<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">504</other_info_on_meta>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="subfamily">Cypripedioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cypripedium</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="1805" rank="species">candidum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4(1): 142. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily cypripedioideae;genus cypripedium;species candidum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101547</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 11–40 cm.</text>
      <biological_entity id="o34969" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3–4 (–5), on proximal or middle portion of stem, alternate, erect-ascending;</text>
      <biological_entity id="o34970" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="4" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-ascending" value_original="erect-ascending" />
      </biological_entity>
      <biological_entity id="o34971" name="portion" name_original="portion" src="d0_s1" type="structure" />
      <biological_entity id="o34972" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o34970" id="r4691" name="on" negation="false" src="d0_s1" to="o34971" />
      <relation from="o34971" id="r4692" name="part_of" negation="false" src="d0_s1" to="o34972" />
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate or elliptic to oblanceolate, 7–20 × 0.9–5.3 cm.</text>
      <biological_entity id="o34973" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s2" to="5.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 1 (–2);</text>
      <biological_entity id="o34974" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals green to pale brownish yellow, usually spotted and striped with reddish-brown or madder;</text>
      <biological_entity id="o34975" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="pale brownish yellow" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="spotted and striped with reddish-brown or spotted and striped with madder" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal ovate to ovate-lance-acuminate, 15–35 × 7–13 mm;</text>
      <biological_entity constraint="dorsal" id="o34976" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovate-lance-acuminate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals connate;</text>
      <biological_entity constraint="lateral" id="o34977" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>synsepal 13–35 × 7–15 mm;</text>
      <biological_entity id="o34978" name="synsepal" name_original="synsepal" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s7" to="35" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals spreading to somewhat deflexed, same color as sepals, spirally twisted or spiral-undulate, lanceolate to linearlanceolate, 23–46 × 3–5 mm;</text>
      <biological_entity id="o34979" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading to somewhat" value_original="spreading to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s8" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="spirally" name="coloration" notes="" src="d0_s8" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="spiral-undulate" value_original="spiral-undulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s8" to="46" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34980" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <relation from="o34979" id="r4693" name="as" negation="false" src="d0_s8" to="o34980" />
    </statement>
    <statement id="d0_s9">
      <text>lip white, obovoid or oblance-ovoid to oblance-fusiform, 17–27 mm;</text>
      <biological_entity id="o34981" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="oblance-ovoid" name="shape" src="d0_s9" to="oblance-fusiform" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s9" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>orifice basal, 10–15 mm;</text>
      <biological_entity id="o34982" name="orifice" name_original="orifice" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="basal" value_original="basal" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode lanceoloid or oblong-lanceoloid to ellipsoid.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o34983" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong-lanceoloid" name="shape" src="d0_s11" to="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34984" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic to wet prairies and fen meadows, very rarely open wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet prairies" />
        <character name="habitat" value="fen" />
        <character name="habitat" value="mesic to wet prairies" />
        <character name="habitat" value="fen meadows" />
        <character name="habitat" value="open wooded slopes" modifier="very rarely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Sask.; Ala., Ill., Ind., Iowa., Ky., Md., Mich., Minn., Mo., Nebr., N.J., N.Y., N.Dak., Ohio, Pa., S.Dak., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">White lady’s-slipper</other_name>
  <discussion>Cypripedium candidum begins blooming while the shoots are still emerging from the soil; at that time they are small, with the leaves clustered near the base. The plants enlarge greatly during and after anthesis, and the leaves may assume a more median position. The orifice of the lip in C. candidum is distinctive, the apical margin forming an acute angle. This feature is shared with C. montanum and differs from the commonly obtuse margin in C. parviflorum; hence, it can aid determination of discolored herbarium specimens. Hybrids of C. candidum and C. parviflorum have been designated C. ×andrewsii A. M. Fuller. See 11. C. parviflorum for a general discussion of hybridization and variation within and between related species.</discussion>
  
</bio:treatment>