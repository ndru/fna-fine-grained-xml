<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">331</other_info_on_meta>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="genus">brodiaea</taxon_name>
    <taxon_name authority="Lindley" date="1849" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>J. Hort. Soc. London</publication_title>
      <place_in_publication>4: 84. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus brodiaea;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101435</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hookera</taxon_name>
    <taxon_name authority="(Lindley) Greene" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Hookera;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Scape 20–70 cm, stout.</text>
      <biological_entity id="o26974" name="scape" name_original="scape" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers 24–38 mm;</text>
      <biological_entity id="o26975" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s1" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>perianth pale lilac or sometimes white, tube cylindrical, 9–12 mm, translucent, splitting in fruit, lobes ascending, recurved distally, 20–30 × 4–10 mm, usually more than twice length of tube;</text>
      <biological_entity id="o26976" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale lilac" value_original="pale lilac" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26977" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s2" value="translucent" value_original="translucent" />
        <character constraint="in fruit" constraintid="o26978" is_modifier="false" name="architecture_or_dehiscence" src="d0_s2" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o26978" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <biological_entity id="o26979" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character constraint="tube" constraintid="o26980" is_modifier="false" name="length" src="d0_s2" value="2+ times length of tube" />
      </biological_entity>
      <biological_entity id="o26980" name="tube" name_original="tube" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>filaments 7–10 mm, base not triangular, appendages absent or rudimentary;</text>
      <biological_entity id="o26981" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26982" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o26983" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>anthers linear, 9–12 mm, apex rounded;</text>
      <biological_entity id="o26984" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26985" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminodia erect, white to pale lilac, narrowly linear, usually flat, 16–27 mm, margins 1/4–1/2 involute, apex rounded;</text>
      <biological_entity id="o26986" name="staminodium" name_original="staminodia" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="pale lilac" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s5" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26987" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s5" to="1/2" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o26988" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary 5–11 mm;</text>
      <biological_entity id="o26989" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 15–23 mm;</text>
      <biological_entity id="o26990" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 2–10 cm.</text>
      <biological_entity id="o26991" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth lobes 6–10 mm wide; ovary 7–11 mm.</description>
      <determination>2a Brodiaea californica subsp. californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth lobes 4–7 mm wide; ovary 5–7 mm.</description>
      <determination>2b Brodiaea californica subsp. leptandra</determination>
    </key_statement>
  </key>
</bio:treatment>