<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">362</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1904" rank="species">funereum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 387. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species funereum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101903</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, green to ashy olive when dry, to 7.6 dm, strongly glaucous;</text>
      <biological_entity id="o14638" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="ashy olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="7.6" to_unit="dm" />
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots somewhat fleshy-thickened.</text>
      <biological_entity id="o14639" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched, with 1 node, 1–4 mm wide, glabrous, margins white-cartilaginous;</text>
      <biological_entity id="o14640" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14641" name="node" name_original="node" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14642" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="white-cartilaginous" value_original="white-cartilaginous" />
      </biological_entity>
      <relation from="o14640" id="r2011" name="with" negation="false" src="d0_s2" to="o14641" />
    </statement>
    <statement id="d0_s3">
      <text>internode 16–65 cm, longer than leaves, with 2–3 branches.</text>
      <biological_entity id="o14643" name="internode" name_original="internode" src="d0_s3" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s3" to="65" to_unit="cm" />
        <character constraint="than leaves" constraintid="o14644" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14644" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14645" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o14643" id="r2012" name="with" negation="false" src="d0_s3" to="o14645" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o14646" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14647" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o14648" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o14648" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o14649" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green, obviously wider than supporting branch, glabrous, keels entire;</text>
      <biological_entity id="o14650" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than supporting branch" constraintid="o14651" is_modifier="false" name="width" src="d0_s6" value="obviously wider" value_original="obviously wider" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14651" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity id="o14652" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer 13.5–24 mm, 3 mm shorter to 1 mm longer than inner, tapering evenly towards apex, margins basally connate 5–6.5 mm;</text>
      <biological_entity constraint="outer" id="o14653" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="13.5" from_unit="mm" name="some_measurement" src="d0_s7" to="24" to_unit="mm" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o14654" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o14655" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o14654" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o14655" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o14656" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved to straight, hyaline margins 0.6–1.1 mm wide, apex extending as 2 broadly rounded and erose lobes, ending at or slightly beyond green apex.</text>
      <biological_entity constraint="inner" id="o14657" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o14658" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character char_type="range_value" from="evenly curved" name="course" src="d0_s8" to="straight" />
      </biological_entity>
      <biological_entity id="o14659" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14660" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o14661" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o14662" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <relation from="o14657" id="r2013" name="with" negation="false" src="d0_s8" to="o14658" />
      <relation from="o14660" id="r2014" name="extending as" negation="false" src="d0_s8" to="o14661" />
      <relation from="o14660" id="r2015" modifier="slightly" name="beyond" negation="false" src="d0_s8" to="o14662" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals pale blue to light bluish violet, bases yellow;</text>
      <biological_entity id="o14663" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14664" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s9" to="light bluish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o14665" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer tepals 9–15 mm, apex truncate to occasionally rounded, aristate;</text>
      <biological_entity id="o14666" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o14667" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14668" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s10" to="occasionally rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ± entirely, glabrous;</text>
      <biological_entity id="o14669" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14670" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o14671" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14672" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o14673" name="foliage" name_original="foliage" src="d0_s12" type="structure" />
      <relation from="o14672" id="r2016" name="to" negation="false" src="d0_s12" to="o14673" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules beige, globose, 5–6 mm;</text>
      <biological_entity id="o14674" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="beige" value_original="beige" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel erect to ascending.</text>
      <biological_entity id="o14675" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s14" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds globose to obconic, lacking obvious depression, 1.1–1.5 mm, slightly granular.</text>
      <biological_entity id="o14676" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 32.</text>
      <biological_entity id="o14677" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s15" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief_or_texture" src="d0_s15" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14678" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, grassy areas along streams and springs where soil strongly alkaline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" constraint="along streams and springs" />
        <character name="habitat" value="grassy areas" constraint="along streams and springs" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sisyrinchium funereum is endemic to the Death Valley-Ash Meadows region.</discussion>
  
</bio:treatment>