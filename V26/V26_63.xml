<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">veratrum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">album</taxon_name>
    <taxon_name authority="(Turczaninow) Miyabe &amp; Kudô" date="1915" rank="variety">oxysepalum</taxon_name>
    <place_of_publication>
      <publication_title>in K. Miyabe and T. Miyake, Fl. Saghalin,</publication_title>
      <place_in_publication>484. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus veratrum;species album;variety oxysepalum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102322</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veratrum</taxon_name>
    <taxon_name authority="Turczaninow" date="unknown" rank="species">oxysepalum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>13: 79. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Veratrum;species oxysepalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.3–1 m, glabrous proximally, villous in inflorescence.</text>
      <biological_entity id="o30780" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="in inflorescence" constraintid="o30781" is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o30781" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and proximal cauline ovate to elliptic, 10–25 × 5–18 cm, reduced distally, glabrous to sparsely villous at least on veins.</text>
      <biological_entity id="o30782" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="elliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s1" to="18" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s1" value="reduced" value_original="reduced" />
        <character char_type="range_value" constraint="on veins" constraintid="o30783" from="glabrous" name="pubescence" src="d0_s1" to="sparsely villous" />
      </biological_entity>
      <biological_entity id="o30783" name="vein" name_original="veins" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences spiciform-racemose or with few, short, ascending branches proximally, 0–10 cm, sparsely villous;</text>
      <biological_entity id="o30784" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiciform-racemose" value_original="spiciform-racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="with few , short , ascending branches" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o30785" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o30784" id="r4142" name="with" negation="false" src="d0_s2" to="o30785" />
    </statement>
    <statement id="d0_s3">
      <text>bracts lanceolate, 1/2 as long as to equaling flowers, sparsely villous, ciliate.</text>
      <biological_entity id="o30786" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o30787" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Tepals yellowish green, lanceolate to lanceovate, not clawed, 7–12 mm, margins denticulate, sparsely villous at least in proximal 1/2;</text>
      <biological_entity id="o30788" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="lanceovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30789" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character constraint="in proximal 1/2" constraintid="o30790" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30790" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>gland 1, basal, green to yellowish green, V-shaped;</text>
      <biological_entity id="o30791" name="gland" name_original="gland" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary subglabrous.</text>
      <biological_entity id="o30792" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules elliptic to oblong, 1–1.5 cm, subglabrous.</text>
      <biological_entity id="o30793" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds flat, broadly winged, 5–7 mm. 2n = 64.</text>
      <biological_entity id="o30794" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30795" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, marshy soils in mesic tundra and Salix thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy soils" modifier="wet" constraint="in mesic tundra and salix thickets" />
        <character name="habitat" value="mesic tundra" />
        <character name="habitat" value="salix thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; ne Russia, n Japan (Hokkaido).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="ne Russia" establishment_means="native" />
        <character name="distribution" value="n Japan (Hokkaido)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  
</bio:treatment>