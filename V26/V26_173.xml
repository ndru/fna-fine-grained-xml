<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="species">eurycarpus</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>348. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species eurycarpus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101466</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calochortus</taxon_name>
    <taxon_name authority="Douglas" date="unknown" rank="species">nitidus</taxon_name>
    <taxon_name authority="L. F. Henderson" date="unknown" rank="variety">eurycarpus</taxon_name>
    <taxon_hierarchy>genus Calochortus;species nitidus;variety eurycarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems not branching, straight, 1–5 dm.</text>
      <biological_entity id="o18411" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal persistent, subtended by inflorescence, 1–3 dm × 5–25 mm;</text>
      <biological_entity id="o18412" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18413" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s1" to="3" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18414" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <relation from="o18413" id="r2542" name="subtended by" negation="false" src="d0_s1" to="o18414" />
    </statement>
    <statement id="d0_s2">
      <text>blade flat, tapering toward both ends, becoming involute.</text>
      <biological_entity id="o18415" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18416" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character constraint="toward ends" constraintid="o18417" is_modifier="false" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="becoming" name="shape_or_vernation" notes="" src="d0_s2" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o18417" name="end" name_original="ends" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences subumbellate, 1–5-flowered;</text>
      <biological_entity id="o18418" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 2 or more, narrowly lanceolate to linear, long-attenuate, unequal, 1–5 cm.</text>
      <biological_entity id="o18419" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="count" value_original="count" />
        <character is_modifier="false" name="shape" src="d0_s4" value="list" value_original="list" />
        <character name="quantity" src="d0_s4" unit="or morepunct narrowly lanceolate to linear" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-attenuate" value_original="long-attenuate" />
        <character is_modifier="false" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers erect;</text>
      <biological_entity id="o18420" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o18421" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals ovate to lanceolate, usually much shorter than petals, glabrous, apex acute to acuminate;</text>
      <biological_entity id="o18422" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character constraint="than petals" constraintid="o18423" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="usually much shorter" value_original="usually much shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18423" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o18424" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals creamy white to lavender, with conspicuous median red-purple adaxial blotch, obovate, invested near gland with few long, flexuous hairs, base cuneate, apex rounded or acute;</text>
      <biological_entity id="o18425" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s8" to="lavender" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="median adaxial" id="o18426" name="blotch" name_original="blotch" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="red-purple" value_original="red-purple" />
      </biological_entity>
      <biological_entity id="o18427" name="gland" name_original="gland" src="d0_s8" type="structure" />
      <biological_entity id="o18428" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="true" name="course" src="d0_s8" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o18429" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o18430" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o18425" id="r2543" name="with" negation="false" src="d0_s8" to="o18426" />
      <relation from="o18425" id="r2544" name="invested near" negation="false" src="d0_s8" to="o18427" />
      <relation from="o18425" id="r2545" name="with" negation="false" src="d0_s8" to="o18428" />
    </statement>
    <statement id="d0_s9">
      <text>glands triangular-lunate, slightly depressed, bordered proximally by comparatively narrow, deeply fringed membrane, distally often by narrower, crenate membranes, enclosed surface densely covered with long yellowish hairs, which, with membrane fringe, often inconspicuously papillose;</text>
      <biological_entity id="o18431" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular-lunate" value_original="triangular-lunate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="depressed" value_original="depressed" />
        <character constraint="by membrane" constraintid="o18432" is_modifier="false" name="architecture" src="d0_s9" value="bordered" value_original="bordered" />
        <character is_modifier="false" modifier="often inconspicuously" name="relief" notes="" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o18432" name="membrane" name_original="membrane" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="comparatively" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="true" modifier="deeply" name="shape" src="d0_s9" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o18433" name="membrane" name_original="membranes" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="narrower" value_original="narrower" />
        <character is_modifier="true" name="shape" src="d0_s9" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o18434" name="surface" name_original="surface" src="d0_s9" type="structure" />
      <biological_entity id="o18435" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity constraint="membrane" id="o18436" name="fringe" name_original="fringe" src="d0_s9" type="structure" />
      <relation from="o18431" id="r2546" modifier="distally often; often" name="by" negation="false" src="d0_s9" to="o18433" />
      <relation from="o18431" id="r2547" name="enclosed" negation="false" src="d0_s9" to="o18434" />
      <relation from="o18431" id="r2548" modifier="densely" name="covered with" negation="false" src="d0_s9" to="o18435" />
      <relation from="o18431" id="r2549" name="with" negation="false" src="d0_s9" to="o18436" />
    </statement>
    <statement id="d0_s10">
      <text>filaments slightly longer than anthers;</text>
      <biological_entity id="o18437" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="than anthers" constraintid="o18438" is_modifier="false" name="length_or_size" src="d0_s10" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o18438" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers oblong, apex obtuse.</text>
      <biological_entity id="o18439" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o18440" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules erect, 3-winged, ellipsoid-oblong.</text>
      <biological_entity id="o18441" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-winged" value_original="3-winged" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid-oblong" value_original="ellipsoid-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds light beige.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 20.</text>
      <biological_entity id="o18442" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="light beige" value_original="light beige" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18443" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands and open coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Nev., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  
</bio:treatment>