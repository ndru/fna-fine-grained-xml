<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="mention_page">545</other_info_on_meta>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="Sheviak" date="1973" rank="species">magnicamporum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mus. Leafl.</publication_title>
      <place_in_publication>23: 287, plate 22. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species magnicamporum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101958</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 7–60 cm.</text>
      <biological_entity id="o34782" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots few, descending, tuberous, mostly to 0.8 cm diam.</text>
      <biological_entity id="o34783" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="descending" value_original="descending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="mostly" name="diameter" src="d0_s1" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves fugaceous or rarely persisting to anthesis, basal, ascending, linearlanceolate to oblanceolate, to 16 × 1.5 cm.</text>
      <biological_entity id="o34784" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="fugaceous" value_original="fugaceous" />
        <character constraint="to basal leaves" constraintid="o34785" is_modifier="false" modifier="rarely" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="16" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o34785" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="anthesis" value_original="anthesis" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes usually very tightly spiraled, 3–4 flowers per cycle of spiral;</text>
      <biological_entity id="o34786" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually very tightly" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o34787" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o34788" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
      <relation from="o34787" id="r4672" modifier="of spiral" name="per" negation="false" src="d0_s3" to="o34788" />
    </statement>
    <statement id="d0_s4">
      <text>rachis moderately pubescent, some trichomes capitate, glands obviously stalked (longest trichomes 0.2–0.52 mm).</text>
      <biological_entity id="o34789" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o34790" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o34791" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers abruptly nodding from base, white to ivory, gaping, lip not strongly curving from claw, not urceolate;</text>
      <biological_entity id="o34792" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="from base" constraintid="o34793" is_modifier="false" modifier="abruptly" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s5" to="ivory" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="gaping" value_original="gaping" />
      </biological_entity>
      <biological_entity id="o34793" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o34794" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character constraint="from claw" constraintid="o34795" is_modifier="false" modifier="not strongly" name="course" src="d0_s5" value="curving" value_original="curving" />
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s5" value="urceolate" value_original="urceolate" />
      </biological_entity>
      <biological_entity id="o34795" name="claw" name_original="claw" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sepals distinct to base, 5–14 mm;</text>
      <biological_entity id="o34796" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="to base" constraintid="o34797" is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34797" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals widespreading, commonly ascending above flower;</text>
      <biological_entity constraint="lateral" id="o34798" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="widespreading" value_original="widespreading" />
        <character constraint="above flower" constraintid="o34799" is_modifier="false" modifier="commonly" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o34799" name="flower" name_original="flower" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petals linear to lance-oblong, 4.9–13 mm, apex acute to obtuse;</text>
      <biological_entity id="o34800" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lance-oblong" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34801" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip commonly yellow centrally, ovate to oblong, 4.9–12 × 3.3–7 mm, margins crenulate, glabrous;</text>
      <biological_entity id="o34802" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="commonly; centrally" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34803" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>veins several, branches parallel;</text>
      <biological_entity id="o34804" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o34805" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>basal calli short-conic, mostly to 1 mm;</text>
      <biological_entity constraint="basal" id="o34806" name="callus" name_original="calli" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-conic" value_original="short-conic" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidia linearlanceolate;</text>
      <biological_entity id="o34807" name="viscidium" name_original="viscidia" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 4–10 mm.</text>
      <biological_entity id="o34808" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds monoembryonic.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 30.</text>
      <biological_entity id="o34809" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monoembryonic" value_original="monoembryonic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34810" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to wet prairies and fens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to wet prairies" />
        <character name="habitat" value="dry to fens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont.; Ala., Ark., Ill., Ind., Iowa, Kans., Ky., La., Mich., Minn., Miss., Mo., Nebr., N.Mex., N.Dak., Ohio, Okla., Pa., S.Dak., Tex., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Leaves typically senesce some weeks before anthesis, usually before the inflorescence appears. Occasionally at the northern and western range limits of the species, however, especially in wetter habitats, they may persist into anthesis. See notes on gene flow and apomixis under 14. Spiranthes cernua.</discussion>
  
</bio:treatment>