<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="treatment_page">549</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Seguier" date="1754" rank="genus">PSEUDORCHIS</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Veron.</publication_title>
      <place_in_publication>3: 254. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus pseudorchis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudo, false, and the generic name Orchis</other_info_on_name>
    <other_info_on_name type="fna_id">127215</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rather succulent.</text>
      <biological_entity id="o6912" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fasciculate, fleshy, both slender and digitately divided tuberoids.</text>
      <biological_entity id="o6913" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o6914" name="tuberoid" name_original="tuberoids" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="digitately" name="shape" src="d0_s1" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems leafy, leaves gradually reduced apically, generally to bracts proximal to inflorescence.</text>
      <biological_entity id="o6915" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o6916" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="gradually; apically" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6917" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character constraint="to inflorescence" constraintid="o6918" is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o6918" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
      <relation from="o6916" id="r988" modifier="generally" name="to" negation="false" src="d0_s2" to="o6917" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves few to several, ascending-spreading, conduplicate, bases sheathing.</text>
      <biological_entity id="o6919" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending-spreading" value_original="ascending-spreading" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity id="o6920" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, solitary, rather dense spikes;</text>
      <biological_entity id="o6921" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o6922" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="rather" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>floral bracts foliaceous, prominently exserted to reduced and inconspicuous.</text>
      <biological_entity constraint="floral" id="o6923" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="prominently" name="position" src="d0_s5" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers several to many, resupinate;</text>
      <biological_entity id="o6924" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="several" name="quantity" src="d0_s6" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lip deeply 3-lobed, lobes entire, nearly equal, base spurred;</text>
      <biological_entity id="o6925" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s7" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o6926" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o6927" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anther cells parallel;</text>
      <biological_entity constraint="anther" id="o6928" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pollinia 2, pollinaria 2, separated by thick, fleshy, laminar projection of midlobe of rostellum, free viscidia sheltered within pockets formed by auriculate lateral rostellum lobes;</text>
      <biological_entity id="o6929" name="pollinium" name_original="pollinia" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6930" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character constraint="by laminar projection" constraintid="o6931" is_modifier="false" name="arrangement" src="d0_s9" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="laminar" id="o6931" name="projection" name_original="projection" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="thick" value_original="thick" />
        <character is_modifier="true" name="texture" src="d0_s9" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o6932" name="midlobe" name_original="midlobe" src="d0_s9" type="structure" />
      <biological_entity id="o6933" name="rostellum" name_original="rostellum" src="d0_s9" type="structure" />
      <biological_entity id="o6934" name="viscidium" name_original="viscidia" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o6935" name="pocket" name_original="pockets" src="d0_s9" type="structure" />
      <biological_entity constraint="rostellum" id="o6936" name="lobe" name_original="lobes" src="d0_s9" type="structure" constraint_original="lateral rostellum">
        <character is_modifier="true" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <relation from="o6931" id="r989" name="part_of" negation="false" src="d0_s9" to="o6932" />
      <relation from="o6931" id="r990" name="part_of" negation="false" src="d0_s9" to="o6933" />
      <relation from="o6934" id="r991" name="sheltered within" negation="false" src="d0_s9" to="o6935" />
      <relation from="o6934" id="r992" name="sheltered within" negation="false" src="d0_s9" to="o6936" />
    </statement>
    <statement id="d0_s10">
      <text>stigma concave.</text>
      <biological_entity id="o6937" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o6938" name="capsule" name_original="capsules" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>ne North America, Europe, n Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="ne North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Small white orchid</other_name>
  <discussion>Species 2 or 3 (1 in the flora).</discussion>
  
</bio:treatment>