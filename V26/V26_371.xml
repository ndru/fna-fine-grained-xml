<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">asparagus</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="species">aethiopicus</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.,</publication_title>
      <place_in_publication>63. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus asparagus;species aethiopicus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242101423</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asparagus</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="species">sprengeri</taxon_name>
    <taxon_hierarchy>genus Asparagus;species sprengeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, sprawling or scrambling;</text>
      <biological_entity id="o21699" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scrambling" value_original="scrambling" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous and tuberous.</text>
      <biological_entity id="o21700" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems to 2 m, wiry, branches numerous, finely ridged;</text>
      <biological_entity id="o21701" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="2" to_unit="m" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o21702" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s2" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cladophylls solitary or in fascicles of 3 or more per node, linear, flattened, straight or curved, 8–22 × 2 mm, with single prominent vein.</text>
      <biological_entity id="o21703" name="cladophyll" name_original="cladophylls" src="d0_s3" type="structure">
        <character constraint="of node" constraintid="o21704" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="in fascicles" value_original="in fascicles" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21704" name="node" name_original="node" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21705" name="vein" name_original="vein" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o21703" id="r2961" name="with" negation="false" src="d0_s3" to="o21705" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves membranous, 1–2 mm;</text>
      <biological_entity id="o21706" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade diamond-shaped, attached in middle, tapering to slender apex.</text>
      <biological_entity id="o21707" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="diamond--shaped" value_original="diamond--shaped" />
        <character constraint="in slender apex" constraintid="o21708" is_modifier="false" name="fixation" src="d0_s5" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity constraint="slender" id="o21708" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="middle" value_original="middle" />
        <character is_modifier="true" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences in axillary racemes, 5–9 (–17) -flowered.</text>
      <biological_entity id="o21709" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="5-9(-17)-flowered" value_original="5-9(-17)-flowered" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o21710" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <relation from="o21709" id="r2962" name="in" negation="false" src="d0_s6" to="o21710" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o21711" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth rotate to campanulate;</text>
      <biological_entity id="o21712" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="rotate" name="shape" src="d0_s8" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals white to pale-pink, 3–4 × 1.5–2 mm;</text>
      <biological_entity id="o21713" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale-pink" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicel 5–8 mm, jointed 2–3 mm above base.</text>
      <biological_entity id="o21714" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="jointed" value_original="jointed" />
        <character char_type="range_value" constraint="above base" constraintid="o21715" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21715" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Berries red, 5–8 mm.</text>
      <biological_entity id="o21716" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seed 1.2n = 40, 60.</text>
      <biological_entity id="o21717" name="seed" name_original="seed" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21718" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, abandoned gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="abandoned gardens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Sprenger asparagus-fern</other_name>
  <other_name type="common_name">emerald-fern</other_name>
  <discussion>The name Asparagus densiflorus (Kunth) Jessop (Asparagopsis densiflora Kunth) has been misapplied to this species (P. S. Green 1986; W. S. Judd 2001). Asparagus aethiopicus cv. ‘Sprengeri’ is the most common of the asparagus-fern cultivars.</discussion>
  
</bio:treatment>