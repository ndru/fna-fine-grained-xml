<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">635</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="(Willdenow) Poiret" date="1818" rank="species">odontorhiza</taxon_name>
    <taxon_name authority="(Greenman) Freudenstein" date="1997" rank="variety">pringlei</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>1(10): 24. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species odontorhiza;variety pringlei;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102229</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">pringlei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>33: 475. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Corallorhiza;species pringlei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: perianth open;</text>
      <biological_entity id="o26392" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o26393" name="perianth" name_original="perianth" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>lip similar to petals (narrowly obovate-spatulate), 2.7–4.6 × 2–3.7 mm;</text>
      <biological_entity id="o26394" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o26395" name="lip" name_original="lip" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" notes="" src="d0_s1" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s1" to="3.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26396" name="petal" name_original="petals" src="d0_s1" type="structure" />
      <relation from="o26395" id="r3589" name="to" negation="false" src="d0_s1" to="o26396" />
    </statement>
    <statement id="d0_s2">
      <text>column 1.8–2.4 × 0.8–1.7 mm, base with 2 prominent auricles on adaxial surface;</text>
      <biological_entity id="o26397" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o26398" name="column" name_original="column" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s2" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26399" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o26400" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26401" name="surface" name_original="surface" src="d0_s2" type="structure" />
      <relation from="o26399" id="r3590" name="with" negation="false" src="d0_s2" to="o26400" />
      <relation from="o26400" id="r3591" name="on" negation="false" src="d0_s2" to="o26401" />
    </statement>
    <statement id="d0_s3">
      <text>stigmatic surface 0.3–0.5 × 0.7–1 mm.</text>
      <biological_entity id="o26402" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="stigmatic" id="o26403" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s3" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich deciduous woods, mixed woods, and conifer plantations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich deciduous woods" />
        <character name="habitat" value="mixed woods" />
        <character name="habitat" value="conifer plantations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Ga., Ind., Iowa, Mich., N.J., N.Y., N.C., Pa., Tenn., Wis.; Mexico; Central America (El Salvador, Guatemala, Honduras, Nicaragua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (El Salvador)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
        <character name="distribution" value="Central America (Nicaragua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <discussion>In Corallorhiza odontorhiza both chasmogamous and cleistogamous flowers exist; they occur on separate plants, although sometimes in the same populations. The cleistogamous form is by far the more frequent.</discussion>
  
</bio:treatment>