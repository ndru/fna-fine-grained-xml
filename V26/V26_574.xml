<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">299</other_info_on_meta>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Herbert" date="unknown" rank="genus">zephyranthes</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">treatiae</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 300. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus zephyranthes;species treatiae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102092</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atamosco</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">treatiae</taxon_name>
    <taxon_hierarchy>genus Atamosco;species treatiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zephyranthes</taxon_name>
    <taxon_name authority="(Linnaeus) Herbert" date="unknown" rank="species">atamasca</taxon_name>
    <taxon_name authority="(S. Watson) Meerow" date="unknown" rank="variety">treatiae</taxon_name>
    <taxon_hierarchy>genus Zephyranthes;species atamasca;variety treatiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade dull green, to 4 mm wide.</text>
      <biological_entity id="o14386" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spathe (1.8–) 2–3.3 (–3.6) cm.</text>
      <biological_entity id="o14387" name="spathe" name_original="spathe" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3.6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers erect to inclined;</text>
      <biological_entity id="o14388" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="inclined" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth mostly white to pink, color deepening with age, funnelform, (6–) 6.6–9.5 (–11) cm;</text>
      <biological_entity id="o14389" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character char_type="range_value" from="mostly white" name="coloration" src="d0_s3" to="pink" />
        <character constraint="with age" constraintid="o14390" is_modifier="false" name="coloration" src="d0_s3" value="color deepening" value_original="color deepening" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="6.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="6.6" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="9.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14390" name="age" name_original="age" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>perianth-tube green, (1.7–) 2–3 (–3.1) cm, increasing in diam., at least 1/4 perianth length, ± equaling (2/3–11/4) filament length, ca. 2/3–11/3 spathe length;</text>
      <biological_entity id="o14391" name="perianth-tube" name_original="perianth-tube" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="atypical_distance" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s4" to="3.1" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s4" to="3" to_unit="cm" />
        <character modifier="at least" name="quantity" src="d0_s4" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o14392" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s4" value="equaling" value_original="equaling" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s4" to="11/4" />
      </biological_entity>
      <biological_entity id="o14393" name="filament" name_original="filament" src="d0_s4" type="structure">
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s4" to="11/3" />
      </biological_entity>
      <biological_entity id="o14394" name="spathe" name_original="spathe" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tepals usually reflexed;</text>
      <biological_entity id="o14395" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens diverging, appearing equal;</text>
      <biological_entity id="o14396" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments filiform, (1.6–) 2–4 (–4.6) cm;</text>
      <biological_entity id="o14397" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4.6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3–6 (–8) mm;</text>
      <biological_entity id="o14398" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style longer than perianth-tube;</text>
      <biological_entity id="o14399" name="style" name_original="style" src="d0_s9" type="structure">
        <character constraint="than perianth-tube" constraintid="o14400" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14400" name="perianth-tube" name_original="perianth-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma 3-fid, exserted more than 2 mm beyond anthers;</text>
      <biological_entity id="o14401" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" constraint="beyond anthers" constraintid="o14402" from="2" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14402" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel 0–1 (–1.2) cm, shorter than spathe.</text>
      <biological_entity id="o14404" name="spathe" name_original="spathe" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o14403" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s11" to="1" to_unit="cm" />
        <character constraint="than spathe" constraintid="o14404" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14405" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid winter–spring (Jan–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="mid winter" />
        <character name="flowering time" char_type="range_value" to="Apr" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Peaty-sandy soil, usually associated with wet inlands or former pineland sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="peaty-sandy soil" />
        <character name="habitat" value="wet inlands" />
        <character name="habitat" value="former pineland sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>It has generally been thought that Zephyranthes atamasca and Z. treatiae are distinguishable only by differences in leaves and habitat, and that their flowers are not significantly different. However, the greater length of the perianth tube compared with that of the filaments in Z. treatiae readily separates that species from Z. atamasca.</discussion>
  
</bio:treatment>