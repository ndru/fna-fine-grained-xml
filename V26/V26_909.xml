<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Karen H. Clary</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">440</other_info_on_meta>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="(Engelmann) Baker" date="1892" rank="genus">HESPEROYUCCA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Misc. Inform. Kew</publication_title>
      <place_in_publication>1892: 8. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus HESPEROYUCCA</taxon_hierarchy>
    <other_info_on_name type="fna_id">115253</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="unranked">Hesperoyucca</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>497. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Yucca;unranked Hesperoyucca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple or cespitose, erect, acaulescent, scapose, rosulate, monocarpic or polycarpic;</text>
      <biological_entity id="o18662" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes solitary or in colonies.</text>
      <biological_entity id="o18663" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character constraint="in colonies" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="in colonies" value_original="in colonies" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear, rarely narrowly lanceolate, widest at base, tapering to apex, glaucous, somewhat flexible when young, rigid at maturity, margins pale-yellow, mostly denticulate, corneous, apex distinctly spinose.</text>
      <biological_entity id="o18664" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="at base" constraintid="o18665" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character constraint="to apex" constraintid="o18666" is_modifier="false" name="shape" notes="" src="d0_s2" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="when young" name="fragility" src="d0_s2" value="pliable" value_original="flexible" />
        <character constraint="at maturity" is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o18665" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o18666" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o18667" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="texture" src="d0_s2" value="corneous" value_original="corneous" />
      </biological_entity>
      <biological_entity id="o18668" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture_or_shape" src="d0_s2" value="spinose" value_original="spinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape well developed, extending beyond rosettes, usually more than 2.5 cm in diam., glabrous.</text>
      <biological_entity id="o18669" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character char_type="range_value" from="2.5" from_unit="cm" modifier="usually" name="diam" src="d0_s3" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18670" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <relation from="o18669" id="r2584" name="extending beyond" negation="false" src="d0_s3" to="o18670" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences paniculate, cylindrical, bracteate, glabrous;</text>
      <biological_entity id="o18671" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis and peduncle reddish purple;</text>
      <biological_entity id="o18672" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o18673" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts reflexed, deltate, abruptly narrowing to linear, sharp-pointed apex.</text>
      <biological_entity id="o18674" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="abruptly" name="width" src="d0_s6" value="narrowing" value_original="narrowing" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o18675" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o18676" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth campanulate or globose;</text>
      <biological_entity id="o18677" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals distinct, white or creamy white to greenish or purple-tinged, broadly lanceolate, 3.2–4.5 (–6) cm;</text>
      <biological_entity id="o18678" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s9" to="greenish or purple-tinged" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments ca. 1.3 cm, equal to or longer than pistil, papillose;</text>
      <biological_entity id="o18679" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="cm" value="1.3" value_original="1.3" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character constraint="than pistil" constraintid="o18680" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o18680" name="pistil" name_original="pistil" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers reniform;</text>
      <biological_entity id="o18681" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="reniform" value_original="reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistil 1–1.3 × 0.6–1 cm;</text>
      <biological_entity id="o18682" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s12" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s12" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary superior;</text>
      <biological_entity id="o18683" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style white, 2–3 mm;</text>
      <biological_entity id="o18684" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas green, capitate.</text>
      <biological_entity id="o18685" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits erect, capsular, obovoid, 3–5 × 1.5–4 cm, dehiscence loculicidal.</text>
      <biological_entity id="o18686" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="dehiscence" src="d0_s16" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="dehiscence" src="d0_s16" to="4" to_unit="cm" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds many per locule, dull black, thin, flattened, 6–8 mm diam. x = 30.</text>
      <biological_entity id="o18687" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character constraint="per locule" constraintid="o18688" is_modifier="false" name="quantity" src="d0_s17" value="many" value_original="many" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s17" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18688" name="locule" name_original="locule" src="d0_s17" type="structure" />
      <biological_entity constraint="x" id="o18689" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Our Lord’s candle</other_name>
  <other_name type="common_name">Quixote plant</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>DNA evidence (M. A. Hanson 1993; D. J. Bogler 1994; K. H. Clary 1997) supports W. Trelease’s (1893) recognition of Hesperoyucca at the genus level and reinforces S. D. McKelvey’s (1938–1947, vol. 2) statement that several characteristics of Hesperoyucca whipplei are not typical of Yucca and could justify its removal from Yucca.</discussion>
  <discussion>J. M. Webber (1953) believed that the differences in leaf, inflorescence, and capsule that McKelvey used to separate Hesperoyucca newberryi from H. whipplei are weak and within the normal ranges of variation in the latter species. F. Hochstätter (2000) treated H. newberryi as a subspecies of H. whipplei. DNA evidence (K. H. Clary 1997) supports specific status for H. newberryi, H. whipplei, and a third species from Mexico, H. peninsularis (McKelvey) Clary. Conclusive DNA analyses have not been performed on the proposed varieties and subspecies of H. whipplei (K. H. Clary 2001), nor has there been any recent systematic morphological work that would support those earlier proposals. For these reasons, only the species of Hesperoyucca are described. This treatment follows S. D. McKelvey’s and K. H. Clary’s interpretations.</discussion>
  <references>
    <reference>Clary, K. H. 1997. Phylogeny, Character Evolution, and Biogeography of Yucca L. (Agavaceae) As Inferred from Plant Morphology and Sequences of the Internal Transcribed Spacer (ITS) Region of the Nuclear Ribosomal DNA. Ph.D. dissertation. University of Texas.  Mc</reference>
    <reference>Kelvey, S. D. 1938–1947. Yuccas of the Southwestern United States. Jamaica Plain.  </reference>
    <reference>Trelease, W. 1893. Further studies of Yucca and their pollination. Rep. (Annual) Missouri Bot. Gard. 4: 181–225.  </reference>
    <reference>Webber, J. M. 1953. Yuccas of the Southwest. Washington. [U.S.D.A. Agric. Monogr. 17.]</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature capsules with conspicuous placental wings; plants monocarpic and rosettes single, or plants polycarpic and rosettes cespitose; sw California</description>
      <determination>1 Hesperoyucca whipplei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature capsules with slight or inconspicuous placental wings; plants monocarpic, rosettes single; Arizona.</description>
      <determination>2 Hesperoyucca newberryi</determination>
    </key_statement>
  </key>
</bio:treatment>