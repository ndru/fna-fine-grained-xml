<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lawrence K. Magrath,Ronald A. Coleman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">591</other_info_on_meta>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Neottieae</taxon_name>
    <taxon_name authority="Bentham" date="" rank="subtribe">Limodorinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">ListerA</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>5: 201. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe neottieae;subtribe limodorinae;genus listera;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Martin Lister (1638–1711), noted English physician and naturalist</other_info_on_name>
    <other_info_on_name type="fna_id">118722</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial.</text>
      <biological_entity id="o33210" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots slender, fibrous.</text>
      <biological_entity id="o33211" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems slender to stout, glabrous proximal to leaves;</text>
      <biological_entity id="o33212" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s2" to="stout" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="to leaves" constraintid="o33213" is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o33213" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline bracts 2 (–3), enclosing base of stem.</text>
      <biological_entity constraint="stem" id="o33214" name="bract" name_original="bracts" src="d0_s3" type="structure" constraint_original="stem cauline; stem">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o33215" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o33216" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o33214" id="r4470" name="enclosing" negation="false" src="d0_s3" to="o33215" />
      <relation from="o33214" id="r4471" name="part_of" negation="false" src="d0_s3" to="o33216" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves 2 (–3, rarely), at apex of stem, opposite or subopposite, sessile, glabrous.</text>
      <biological_entity id="o33217" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33218" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o33219" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <relation from="o33217" id="r4472" name="at" negation="false" src="d0_s4" to="o33218" />
      <relation from="o33218" id="r4473" name="part_of" negation="false" src="d0_s4" to="o33219" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, 2–100-flowered racemes;</text>
      <biological_entity id="o33220" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o33221" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="2-100-flowered" value_original="2-100-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle and rachis densely glandular-pubescent or glabrate;</text>
      <biological_entity id="o33222" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o33223" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bracts inconspicuous, lanceolate, elliptic, suborbiculate-ovate, rhombic-ovate, or oblong.</text>
      <biological_entity constraint="floral" id="o33224" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="suborbiculate-ovate" value_original="suborbiculate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers resupinate, maroon-purple, yellowish green to dark green, blue-green, or pinkish tan;</text>
      <biological_entity id="o33225" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="maroon-purple" value_original="maroon-purple" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s8" to="dark green blue-green or pinkish tan" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s8" to="dark green blue-green or pinkish tan" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s8" to="dark green blue-green or pinkish tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal ovate-elliptic, elliptic-obovate, linear-elliptic, or lanceolate;</text>
      <biological_entity constraint="dorsal" id="o33226" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic-obovate" value_original="elliptic-obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals semiorbiculate-elliptic, ovate, linear-elliptic, or oblong-lanceolate, often falcate-recurved;</text>
      <biological_entity constraint="lateral" id="o33227" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="semiorbiculate-elliptic" value_original="semiorbiculate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s10" value="falcate-recurved" value_original="falcate-recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals reflexed, spreading, or connivent, linear, linear-oblong to lanceolate, or elliptic, falcate;</text>
      <biological_entity id="o33228" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear" value_original="linear" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s11" to="lanceolate or elliptic" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s11" to="lanceolate or elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip prominently deflexed or not, sessile or clawed, linear-oblong to obovate, suborbiculate-ovate, or ovate-reniform, base of lip with or without prominent auricles or lobes, apex deeply 2-lobed, dilated to rounded, apiculate;</text>
      <biological_entity id="o33229" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="prominently" name="orientation" src="d0_s12" value="deflexed" value_original="deflexed" />
        <character name="orientation" src="d0_s12" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s12" to="obovate suborbiculate-ovate or ovate-reniform" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s12" to="obovate suborbiculate-ovate or ovate-reniform" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s12" to="obovate suborbiculate-ovate or ovate-reniform" />
      </biological_entity>
      <biological_entity id="o33230" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o33231" name="lip" name_original="lip" src="d0_s12" type="structure" />
      <biological_entity id="o33232" name="auricle" name_original="auricles" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o33233" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <relation from="o33230" id="r4474" name="part_of" negation="false" src="d0_s12" to="o33231" />
      <relation from="o33230" id="r4475" name="with or without" negation="false" src="d0_s12" to="o33232" />
      <relation from="o33230" id="r4476" name="with or without" negation="false" src="d0_s12" to="o33233" />
    </statement>
    <statement id="d0_s13">
      <text>calli various, papillose, pair of horns, or with 1–2 lamellae;</text>
      <biological_entity id="o33234" name="callus" name_original="calli" src="d0_s13" type="structure" constraint="horn" constraint_original="horn; horn">
        <character is_modifier="false" name="variability" src="d0_s13" value="various" value_original="various" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o33235" name="horn" name_original="horns" src="d0_s13" type="structure" />
      <biological_entity id="o33236" name="lamella" name_original="lamellae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s13" to="2" />
      </biological_entity>
      <relation from="o33234" id="r4477" name="part_of" negation="false" src="d0_s13" to="o33235" />
      <relation from="o33234" id="r4478" name="with" negation="false" src="d0_s13" to="o33236" />
    </statement>
    <statement id="d0_s14">
      <text>column arcuate, thick, short, apex expanded or not;</text>
      <biological_entity id="o33237" name="column" name_original="column" src="d0_s14" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s14" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="width" src="d0_s14" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o33238" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="expanded" value_original="expanded" />
        <character name="size" src="d0_s14" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anther on adaxial side of column near apex;</text>
      <biological_entity id="o33239" name="anther" name_original="anther" src="d0_s15" type="structure" />
      <biological_entity constraint="adaxial" id="o33240" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity id="o33241" name="column" name_original="column" src="d0_s15" type="structure" />
      <biological_entity id="o33242" name="apex" name_original="apex" src="d0_s15" type="structure" />
      <relation from="o33239" id="r4479" name="on" negation="false" src="d0_s15" to="o33240" />
      <relation from="o33240" id="r4480" name="part_of" negation="false" src="d0_s15" to="o33241" />
      <relation from="o33240" id="r4481" name="near" negation="false" src="d0_s15" to="o33242" />
    </statement>
    <statement id="d0_s16">
      <text>pollinia 2, yellow, soft;</text>
      <biological_entity id="o33243" name="pollinium" name_original="pollinia" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s16" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovaries pedicellate.</text>
      <biological_entity id="o33244" name="ovary" name_original="ovaries" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsules, horizontal to semierect, ellipsoid, ovoid, or subglobose, glabrous or glabrate to glandular-pubescent.</text>
      <biological_entity constraint="fruits" id="o33245" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s18" to="semierect" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s18" to="glandular-pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Cool temperate regions, Northern and Southern hemispheres.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Cool temperate regions" establishment_means="native" />
        <character name="distribution" value="Northern and Southern hemispheres" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <discussion>Species 25 (8 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip cleft 1/2–2/3 its length into 2 pointed, linear-lanceolate apical lobes; column 0.5–1 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lip expanded at apex or if cleft, then less than 1/2 its length and apical lobes rounded; column 1.5–4 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip with basal auricles rounded and projecting distally, partially surrounding column; flowers smaller, dorsal and lateral sepals 1.5 mm.</description>
      <determination>1 Listera australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip with basal auricles or lobes pointed and projecting outward like horns, not partially surrounding column; flowers larger, dorsal and lateral sepals 2–3 mm.</description>
      <determination>2 Listera cordata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Lip ± as broad at apex as at base, with basal auricles or lobes extending beyond column.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Lip broader at apex than at base, without basal auricles or lobes extending beyond column.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Auricles curving around and clasping base of column; pedicels glabrous, rarely glabrate; dorsal sepal 3–3.5 mm, lateral sepals 3–4 mm.</description>
      <determination>3 Listera auriculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Auricles divergent from column; pedicels glandular-pubescent; dorsal sepal 4–6 mm, lateral sepals 4.5–7 mm.</description>
      <determination>4 Listera borealis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lip with short claw, not sessile.</description>
      <determination>5 Listera convallarioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lip sessile, without claw.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lip deflexed, base of lip without teeth or lobules; pedicels stout, 3–4 mm; capsules 10 mm.</description>
      <determination>6 Listera ovata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lip not deflexed, base of lip with teeth or lobules; pedicels slender, 4–12 mm; capsules 5 mm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lip rounded or retuse at apex, base with 2 small, pointed auricles; pedicels glandular-puberulent.</description>
      <determination>7 Listera caurina</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lip deeply notched at apex into 2 broadly rounded lobes, base with rounded lobules on each side; pedicels glabrous.</description>
      <determination>8 Listera smallii</determination>
    </key_statement>
  </key>
</bio:treatment>