<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">552</other_info_on_meta>
    <other_info_on_meta type="mention_page">570</other_info_on_meta>
    <other_info_on_meta type="treatment_page">571</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">platanthera</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray ex L. C. Beck" date="1848" rank="species">integra</taxon_name>
    <place_of_publication>
      <publication_title>Bot. North. Middle States ed.</publication_title>
      <place_in_publication>2, 348. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus platanthera;species integra;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101840</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orchis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">integra</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 188. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Orchis;species integra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Habenaria</taxon_name>
    <taxon_name authority="(Nuttall) Sprengel" date="unknown" rank="species">integra</taxon_name>
    <taxon_hierarchy>genus Habenaria;species integra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–75 cm.</text>
      <biological_entity id="o34892" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1–3, ascending to spreading, rather abruptly or gradually reduced to bracts distally;</text>
      <biological_entity id="o34893" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="spreading" />
        <character constraint="to bracts" constraintid="o34894" is_modifier="false" modifier="rather abruptly; abruptly; gradually" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o34894" name="bract" name_original="bracts" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade lance-oblong to linearlanceolate, 4.5–32 × 1–3 cm.</text>
      <biological_entity id="o34895" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lance-oblong" value_original="lance-oblong" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s2" to="32" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikes dense.</text>
      <biological_entity id="o34896" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers resupinate, showy, pale yellow-orange to pale orange;</text>
      <biological_entity id="o34897" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="showy" value_original="showy" />
        <character char_type="range_value" from="pale yellow-orange" name="coloration" src="d0_s4" to="pale orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal entire or rarely apically dentate;</text>
      <biological_entity constraint="dorsal" id="o34898" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely apically" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals spreading;</text>
      <biological_entity constraint="lateral" id="o34899" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals elliptic to linear-oblong, margins entire;</text>
      <biological_entity id="o34900" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="linear-oblong" />
      </biological_entity>
      <biological_entity id="o34901" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip descending, ovate-elliptic to obovate or oblong, 3–5 × 1.5–4 mm, margins eroded to lacerate or rarely entire, with basal pair of fleshy ridges on adaxial surface;</text>
      <biological_entity id="o34902" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="descending" value_original="descending" />
        <character char_type="range_value" from="ovate-elliptic" name="shape" src="d0_s8" to="obovate or oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34903" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s8" value="eroded" value_original="eroded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="basal" id="o34904" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o34905" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34906" name="surface" name_original="surface" src="d0_s8" type="structure" />
      <relation from="o34903" id="r4684" name="with" negation="false" src="d0_s8" to="o34904" />
      <relation from="o34904" id="r4685" name="part_of" negation="false" src="d0_s8" to="o34905" />
      <relation from="o34904" id="r4686" name="on" negation="false" src="d0_s8" to="o34906" />
    </statement>
    <statement id="d0_s9">
      <text>spur tapering from broad base to slender tube, 5–10 mm;</text>
      <biological_entity id="o34907" name="spur" name_original="spur" src="d0_s9" type="structure">
        <character constraint="from base" constraintid="o34908" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34908" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity constraint="slender" id="o34909" name="tube" name_original="tube" src="d0_s9" type="structure" />
      <relation from="o34908" id="r4687" name="to" negation="false" src="d0_s9" to="o34909" />
    </statement>
    <statement id="d0_s10">
      <text>rostellum lobes curved downward, short, rounded;</text>
      <biological_entity constraint="rostellum" id="o34910" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="downward" value_original="downward" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinaria essentially straight;</text>
      <biological_entity id="o34911" name="pollinarium" name_original="pollinaria" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="essentially" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>large pollinia protruding forward;</text>
      <biological_entity id="o34912" name="pollinium" name_original="pollinia" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="false" modifier="forward" name="prominence" src="d0_s12" value="protruding" value_original="protruding" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>viscidia orbiculate to suborbiculate;</text>
      <biological_entity id="o34913" name="viscidium" name_original="viscidia" src="d0_s13" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s13" to="suborbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary slender to somewhat stout, 5–11 mm.</text>
      <biological_entity id="o34914" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="slender to somewhat" value_original="slender to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet pine barrens, peaty depressions in pine savannas, wet sandy woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet pine barrens" />
        <character name="habitat" value="peaty depressions" constraint="in pine savannas" />
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="wet sandy woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., La., Miss., N.J., N.C., S.C., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion>As in Platanthera clavellata and P. nivea, the column of P. integra bears two pairs of lateral processes. In P. integra, the distal structures are essentially sessile and cushionlike, as in P. nivea, but the proximal are short, stout, clublike, and bear several stout horns. The tuberoids of P. integra are abruptly swollen into oblong-cylinders, somewhat like those of P. nivea. These three species evidently form a group apart from Platanthera. See note under 30. P. nivea.</discussion>
  
</bio:treatment>