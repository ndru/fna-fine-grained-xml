<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="Rafinesque" date="1840" rank="species">cuneatum</taxon_name>
    <place_of_publication>
      <publication_title>Autik. Bot.,</publication_title>
      <place_in_publication>133. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species cuneatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101987</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes horizontal, brownish, short, thick, praemorse, not brittle.</text>
      <biological_entity id="o4488" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s0" value="praemorse" value_original="praemorse" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s0" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–5, 1.6–4.5 dm, smooth to rough near bract attachment.</text>
      <biological_entity id="o4489" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="1.6" from_unit="dm" name="some_measurement" src="d0_s1" to="4.5" to_unit="dm" />
        <character constraint="near bract attachment" constraintid="o4490" is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="smooth to rough" value_original="smooth to rough" />
      </biological_entity>
      <biological_entity constraint="bract" id="o4490" name="attachment" name_original="attachment" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts held well above ground, sessile;</text>
      <biological_entity id="o4491" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o4492" name="ground" name_original="ground" src="d0_s2" type="structure" />
      <relation from="o4491" id="r658" name="held" negation="false" src="d0_s2" to="o4492" />
    </statement>
    <statement id="d0_s3">
      <text>blade green to purplish green, weakly to strongly mottled, mottling becoming obscure with age, ovate, ovate-elliptic, occasionally ovate-orbicular with margins overlapping, 7–18.5 × 7–13 cm, usually widest below middle, not glossy, base ± rounded, margins of distal 1/3 convex-curved to apex, apex acuminate to acute.</text>
      <biological_entity id="o4493" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="purplish green" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="purplish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o4494" is_modifier="false" modifier="becoming" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character constraint="with margins" constraintid="o4495" is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="ovate-orbicular" value_original="ovate-orbicular" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" notes="" src="d0_s3" to="18.5" to_unit="cm" />
        <character constraint="below middle blade" constraintid="o4496" is_modifier="false" modifier="usually" name="width" src="d0_s3" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o4494" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o4495" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" notes="" src="d0_s3" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4496" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s3" value="middle" value_original="middle" />
        <character is_modifier="false" modifier="not" name="reflectance" notes="" src="d0_s3" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o4497" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4498" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o4499" name="convex-curved" name_original="convex-curved" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o4500" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o4501" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <relation from="o4498" id="r659" name="consist_of" negation="false" src="d0_s3" to="o4499" />
      <relation from="o4499" id="r660" name="to" negation="false" src="d0_s3" to="o4500" />
    </statement>
    <statement id="d0_s4">
      <text>Flower borne upon bracts, erect, odor usually pleasant, faint, spicy, reminiscent of odor of bruised sweetshrub (Calycanthus) leaves, occasionally musty or unpleasant;</text>
      <biological_entity id="o4502" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="odor" src="d0_s4" value="pleasant" value_original="pleasant" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="faint" value_original="faint" />
        <character is_modifier="false" name="odor_or_taste" src="d0_s4" value="spicy" value_original="spicy" />
        <character is_modifier="false" modifier="occasionally" name="odor" src="d0_s4" value="unpleasant" value_original="unpleasant" />
      </biological_entity>
      <biological_entity id="o4503" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity constraint="sweetshrub" id="o4504" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="character" src="d0_s4" value="odor" value_original="odor" />
        <character is_modifier="true" name="condition" src="d0_s4" value="bruised" value_original="bruised" />
      </biological_entity>
      <relation from="o4502" id="r661" name="borne upon" negation="false" src="d0_s4" to="o4503" />
      <relation from="o4502" id="r662" name="part_of" negation="false" src="d0_s4" to="o4504" />
    </statement>
    <statement id="d0_s5">
      <text>sepals widely spreading, variably green, purple-streaked to all purple, oblong-lanceolate, 27–60 × 7–13 mm, margins entire, apex rounded to acute;</text>
      <biological_entity id="o4505" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="variably" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple-streaked" value_original="purple-streaked" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="27" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4506" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4507" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals long-lasting, erect, ± connivent, ± concealing stamens and ovary, maroon, maroon-purple, brownish purple, bronze, greenish purple, clear green, yellowish green, pale lemon yellow, or 2-colored, yellow distally with purple base, in occasional clones, flowers open or quickly fade to bright copper bronze, the particular color-pattern consistent from year to year, fading to browner tones with age except in yellow or green forms, not spirally twisted, shape quite variable across range, elliptic-obovate to oblanceolate, 4–7 × 0.9–2.7 cm, thick-textured, with widest portion at or above middle, narrowed to usually cuneate basally, not clawed, margins flat, entire, apex acute, rounded-acute to obtuse;</text>
      <biological_entity id="o4508" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="long-lasting" value_original="long-lasting" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o4509" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="maroon-purple" value_original="maroon-purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="clear green" value_original="clear green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale lemon" value_original="pale lemon" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character constraint="with base" constraintid="o4511" is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o4510" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="maroon-purple" value_original="maroon-purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="clear green" value_original="clear green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale lemon" value_original="pale lemon" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character constraint="with base" constraintid="o4511" is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o4511" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o4512" name="clone" name_original="clones" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="occasional" value_original="occasional" />
      </biological_entity>
      <biological_entity id="o4513" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character name="architecture" src="d0_s6" value="quickly" value_original="quickly" />
        <character constraint="with age" constraintid="o4516" is_modifier="false" name="coloration" notes="" src="d0_s6" value="fading to browner tones" value_original="fading to browner tones" />
        <character is_modifier="false" modifier="not spirally" name="architecture" notes="" src="d0_s6" value="twisted" value_original="twisted" />
        <character constraint="across range" is_modifier="false" modifier="quite" name="shape" src="d0_s6" value="variable" value_original="variable" />
        <character char_type="range_value" from="elliptic-obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s6" to="2.7" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thick-textured" value_original="thick-textured" />
        <character char_type="range_value" from="narrowed" name="shape" notes="" src="d0_s6" to="usually cuneate basally" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o4514" name="year" name_original="year" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="bright copper" value_original="bright copper" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="bronze" value_original="bronze" />
        <character is_modifier="true" name="character" src="d0_s6" value="color-pattern" value_original="color-pattern" />
        <character is_modifier="true" name="variability" src="d0_s6" value="consistent" value_original="consistent" />
      </biological_entity>
      <biological_entity id="o4515" name="year" name_original="year" src="d0_s6" type="structure" />
      <biological_entity id="o4516" name="age" name_original="age" src="d0_s6" type="structure" />
      <biological_entity id="o4517" name="form" name_original="forms" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="widest" id="o4518" name="portion" name_original="portion" src="d0_s6" type="structure" />
      <biological_entity constraint="middle" id="o4519" name="portion" name_original="portion" src="d0_s6" type="structure" />
      <biological_entity id="o4520" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4521" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character char_type="range_value" from="rounded-acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <relation from="o4509" id="r663" name="in" negation="false" src="d0_s6" to="o4512" />
      <relation from="o4510" id="r664" name="in" negation="false" src="d0_s6" to="o4512" />
      <relation from="o4513" id="r665" name="fade to" negation="false" src="d0_s6" to="o4514" />
      <relation from="o4513" id="r666" name="to" negation="false" src="d0_s6" to="o4515" />
      <relation from="o4516" id="r667" name="except in" negation="false" src="d0_s6" to="o4517" />
      <relation from="o4513" id="r668" name="with" negation="false" src="d0_s6" to="o4518" />
      <relation from="o4518" id="r669" name="at or above" negation="false" src="d0_s6" to="o4519" />
    </statement>
    <statement id="d0_s7">
      <text>stamens erect, straight, brownish purple-green, 11–18 mm;</text>
      <biological_entity id="o4522" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brownish purple-green" value_original="brownish purple-green" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments brownish purple, 1.5–3.5 mm, widest at base;</text>
      <biological_entity id="o4523" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brownish purple" value_original="brownish purple" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character constraint="at base" constraintid="o4524" is_modifier="false" name="width" src="d0_s8" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o4524" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>anthers erect, straight, brownish gray, 7–14 mm, dehiscence latrorse or occasionally introrse;</text>
      <biological_entity id="o4525" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish gray" value_original="brownish gray" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="latrorse" value_original="latrorse" />
        <character is_modifier="false" modifier="occasionally" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>connectives straight, scarcely (0.5 mm or less) if at all extended beyond anther sacs;</text>
      <biological_entity id="o4526" name="connectif" name_original="connectives" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="beyond anther sacs" constraintid="o4527" is_modifier="false" modifier="at" name="size" src="d0_s10" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="anther" id="o4527" name="sac" name_original="sacs" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary maroon, ovoid to vase-shaped, weakly 6-angled or ridged when mature, 12–15 mm;</text>
      <biological_entity id="o4528" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="vase-shaped weakly 6-angled or ridged" />
        <character char_type="range_value" from="ovoid" modifier="when mature" name="shape" src="d0_s11" to="vase-shaped weakly 6-angled or ridged" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas erect, slightly diverging to spreading, distinct, purplish gray, linear-subulate to thickly subulate, 4–15 mm, fleshy.</text>
      <biological_entity id="o4529" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="slightly diverging" name="orientation" src="d0_s12" to="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish gray" value_original="purplish gray" />
        <character char_type="range_value" from="linear-subulate" name="shape" src="d0_s12" to="thickly subulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits green or with purple streaks, ovoid, very obscurely angled or angles no longer apparent, 2 × 1–1.5 cm, mealy or pulpy, fleshy, not juicy.</text>
      <biological_entity id="o4530" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="with purple streaks" value_original="with purple streaks" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="very obscurely" name="shape" src="d0_s13" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o4531" name="streak" name_original="streaks" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o4530" id="r670" name="with" negation="false" src="d0_s13" to="o4531" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 10.</text>
      <biological_entity id="o4532" name="angle" name_original="angles" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="no" value_original="no" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="apparent" value_original="apparent" />
        <character name="length" src="d0_s13" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s13" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="mealy" value_original="mealy" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="pulpy" value_original="pulpy" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s13" value="juicy" value_original="juicy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4533" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flower late winter–mid spring (early Mar–mid Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, mostly upland woods, especially limestone soils, also less calcareous sites, occasionally found in old fields, ditches, or coal-mine tailings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich" />
        <character name="habitat" value="upland woods" modifier="mostly" />
        <character name="habitat" value="limestone soils" modifier="especially" />
        <character name="habitat" value="less calcareous" modifier="also" />
        <character name="habitat" value="sites" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="coal-mine tailings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Miss., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Whip-poor-will flower</other_name>
  <other_name type="common_name">cuneate trillium</other_name>
  <other_name type="common_name">large toadshade</other_name>
  <other_name type="common_name">purple toadshade</other_name>
  <other_name type="common_name">bloody butcher</other_name>
  <other_name type="common_name">sweet betsy</other_name>
  <discussion>Trillium cuneatum has escaped locally and become established in Michigan and other states. It is most frequent on the Ordovician limestone-derived soils of southern Kentucky and Tennessee, and is perhaps the most vigorous and certainly the largest of the eastern sessile trilliums. Numerous, mostly unnamed color forms occur. Plants from the lower piedmont of Georgia, Alabama, and Mississippi, considered by most botanists to be of this species, have smaller, narrower petals than specimens from northeastern Alabama northward to Kentucky, and they are therefore sometimes difficult to place with certainty.</discussion>
  
</bio:treatment>