<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey ex Durand" date="unknown" rank="genus">schoenolirion</taxon_name>
    <taxon_name authority="Sherman" date="1979" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>24: 125. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus schoenolirion;species wrightii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101886</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with prominent bulbs at tops of vertical rootstocks;</text>
      <biological_entity id="o26050" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26051" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o26052" name="top" name_original="tops" src="d0_s0" type="structure" />
      <biological_entity id="o26053" name="rootstock" name_original="rootstocks" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o26050" id="r3546" name="with" negation="false" src="d0_s0" to="o26051" />
      <relation from="o26051" id="r3547" name="at" negation="false" src="d0_s0" to="o26052" />
      <relation from="o26052" id="r3548" name="part_of" negation="false" src="d0_s0" to="o26053" />
    </statement>
    <statement id="d0_s1">
      <text>bulbs ovoid to elongate, to 17 mm diam.</text>
      <biological_entity id="o26054" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s1" to="elongate" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s1" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–7, 14–34 cm × 2–6 mm, not withering to persistent fibers;</text>
      <biological_entity id="o26055" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s2" to="34" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o26056" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade flattened or somewhat keeled, usually shorter than scape and inflorescence, base fleshy.</text>
      <biological_entity id="o26057" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character constraint="than scape and inflorescence" constraintid="o26058, o26059" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o26058" name="scape" name_original="scape" src="d0_s3" type="structure" />
      <biological_entity id="o26059" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <biological_entity id="o26060" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences seldom branched;</text>
      <biological_entity id="o26061" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts mostly lanceolate-acuminate, sometimes ovate-obtuse.</text>
      <biological_entity id="o26062" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovate-obtuse" value_original="ovate-obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals seldom strongly recurved, white, each with green abaxial stripe, mostly 3 (–5) -veined, ovate to ovatelanceolate, 4.4–6.5 mm, apex obtuse;</text>
      <biological_entity id="o26063" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26064" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="seldom strongly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="mostly" name="architecture" notes="" src="d0_s6" value="3(-5)-veined" value_original="3(-5)-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="ovatelanceolate" />
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s6" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26065" name="stripe" name_original="stripe" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o26066" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o26064" id="r3549" name="with" negation="false" src="d0_s6" to="o26065" />
    </statement>
    <statement id="d0_s7">
      <text>ovary and style green.</text>
      <biological_entity id="o26067" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 24.</text>
      <biological_entity id="o26068" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o26069" name="style" name_original="style" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26070" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–mid Apr in Texas, Arkansas; mid Apr–early May in Alabama; dormant by mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in Texas, Arkans" to="mid Apr" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone outcrops, moist pinelands, or boggy areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="moist pinelands" />
        <character name="habitat" value="boggy areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., La., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Schoenolirion wrightii occurs in habitats similar to those of S. croceum in the Alabama plateau region and in the western part of its range.</discussion>
  <discussion>Treatment of these white-flowered plants as a species distinct from Schoenolirion croceum is very tenuous; tepal color seems to be the only consistent difference between the two. At the time of the last systematic study of the genus (H. L. Sherman 1969), there seemed to be a significant difference in chromosome number between the two “color forms” (2n = 24 for S. wrightii and 2n = 30 or 32 for S. croceum). Since that time, one population of S. croceum with 2n = 24 has been found. In future treatments of the genus, S. wrightii may be considered to be a color form or variety of S. croceum.</discussion>
  
</bio:treatment>