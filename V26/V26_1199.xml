<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">589</other_info_on_meta>
    <other_info_on_meta type="treatment_page">588</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Neottieae</taxon_name>
    <taxon_name authority="Bentham" date="" rank="subtribe">Limodorinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">listera</taxon_name>
    <taxon_name authority="(Linnaeus) R. Brown" date="1813" rank="species">cordata</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>5: 201. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe neottieae;subtribe limodorinae;genus listera;species cordata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101755</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophrys</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cordata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 946. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ophrys;species cordata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bifolium</taxon_name>
    <taxon_name authority="(Linnaeus) Nieuwland" date="unknown" rank="species">cordatum</taxon_name>
    <taxon_hierarchy>genus Bifolium;species cordatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diphryllum</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">cordatum</taxon_name>
    <taxon_hierarchy>genus Diphryllum;species cordatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Distomaea</taxon_name>
    <taxon_name authority="(Linnaeus) Spenner" date="unknown" rank="species">cordata</taxon_name>
    <taxon_hierarchy>genus Distomaea;species cordata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neottia</taxon_name>
    <taxon_name authority="(Linnaeus) Richard" date="unknown" rank="species">cordata</taxon_name>
    <taxon_hierarchy>genus Neottia;species cordata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pollinirhiza</taxon_name>
    <taxon_name authority="(Linnaeus) Dulac" date="unknown" rank="species">cordata</taxon_name>
    <taxon_hierarchy>genus Pollinirhiza;species cordata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–33 cm.</text>
      <biological_entity id="o23775" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="33" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green to reddish purple, succulent, glabrous.</text>
      <biological_entity id="o23776" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="reddish purple" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade broadly to narrowly ovate-cordate or deltate, 0.9–2 (–4) × 0.7–2 (–3.8) cm, apex mucronate.</text>
      <biological_entity id="o23777" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23778" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="ovate-cordate" value_original="ovate-cordate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="3.8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23779" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 5–25-flowered, lax to dense, 20–100 mm, slender;</text>
      <biological_entity id="o23780" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="5-25-flowered" value_original="5-25-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="100" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral bracts ovate, 1–1.5 × 1 mm;</text>
      <biological_entity constraint="floral" id="o23781" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="1.5" to_unit="mm" />
        <character name="width" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle and rachis slightly glandular-puberulent or glabrate;</text>
      <biological_entity id="o23782" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s5" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o23783" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s5" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts, pedicel, and ovary glabrous.</text>
      <biological_entity id="o23784" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23785" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23786" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers yellow-green, green, or reddish purple;</text>
      <biological_entity id="o23787" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel slender, 2–3 mm;</text>
      <biological_entity id="o23788" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal ovate-oblong to oblongelliptic, 2–3 × 1 mm, apex obtuse;</text>
      <biological_entity constraint="dorsal" id="o23789" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s9" to="oblongelliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23790" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals ovate-oblong to oblongelliptic, slightly falcate, 2–3 × 0.5–1.5 mm, apex obtuse;</text>
      <biological_entity constraint="lateral" id="o23791" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s10" to="oblongelliptic" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23792" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals elliptic to oblong-linear, 1.5–2.5 × 0.5–1 mm, apex obtuse;</text>
      <biological_entity id="o23793" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="elliptic to oblong-linear" value_original="elliptic to oblong-linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23794" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip linear-oblong, cleft 1/2 –2/3 its length into 2 linearlanceolate lobes, 3–4 × 1–1.5 mm;</text>
      <biological_entity id="o23795" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="into lobes" constraintid="o23796" from="1/2" name="length" src="d0_s12" to="2/3" />
      </biological_entity>
      <biological_entity id="o23796" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" notes="" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>disc with pair of spreading, linear lobes, apices acute;</text>
      <biological_entity id="o23797" name="disc" name_original="disc" src="d0_s13" type="structure" />
      <biological_entity id="o23798" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity id="o23799" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23800" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o23797" id="r3228" name="with" negation="false" src="d0_s13" to="o23798" />
      <relation from="o23798" id="r3229" name="part_of" negation="false" src="d0_s13" to="o23799" />
    </statement>
    <statement id="d0_s14">
      <text>column 0.5 × 0.5 mm.</text>
      <biological_entity id="o23801" name="column" name_original="column" src="d0_s14" type="structure">
        <character name="length" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
        <character name="width" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules semierect, subglobose, 5 × 4 mm.</text>
      <biological_entity id="o23802" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="semierect" value_original="semierect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character name="length" src="d0_s15" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s15" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>n North America, Europe, Asia (Japan), Iceland.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="n North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Iceland" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Heart-leaved twayblade</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <references>
    <reference>  Ackerman, J. D. and M. R. Mesler. 1979. Pollination biology of Listera cordata (Orchidaceae). Amer. J. Bot. 66: 820–824.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 0.7–2 cm wide; lip 3–4 mm; flowers yellow-green, green, or reddish purple.</description>
      <determination>2a Listera cordata var. cordata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 1.8–3.8 cm wide; lip 5–6 mm; flowers green to yellow-green.</description>
      <determination>2b Listera cordata var. nephrophylla</determination>
    </key_statement>
  </key>
</bio:treatment>