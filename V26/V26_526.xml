<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>T. D. Jacobsen,Dale W. McNeal Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Kunth" date="1843" rank="genus">NOTHOSCORDUM</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>4: 457. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus NOTHOSCORDUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek nothos, false, and scordon, garlic</other_info_on_name>
    <other_info_on_name type="fna_id">122440</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, from bulbs.</text>
      <biological_entity id="o10204" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10205" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <relation from="o10204" id="r1456" name="from" negation="false" src="d0_s0" to="o10205" />
    </statement>
    <statement id="d0_s1">
      <text>Bulbs in general appearance very similar to some species of Allium, without alliaceous odor;</text>
      <biological_entity id="o10206" name="bulb" name_original="bulbs" src="d0_s1" type="structure" />
      <biological_entity id="o10207" name="appearance" name_original="appearance" src="d0_s1" type="structure" />
      <biological_entity id="o10208" name="species" name_original="species" src="d0_s1" type="taxon_name" />
      <biological_entity id="o10209" name="allium" name_original="allium" src="d0_s1" type="taxon_name" />
      <relation from="o10206" id="r1457" name="in" negation="false" src="d0_s1" to="o10207" />
      <relation from="o10207" id="r1458" name="to" negation="false" src="d0_s1" to="o10208" />
      <relation from="o10208" id="r1459" name="part_of" negation="false" src="d0_s1" to="o10209" />
    </statement>
    <statement id="d0_s2">
      <text>outer coats membranous.</text>
      <biological_entity constraint="outer" id="o10210" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually present at flowering time, basal, imbricate, sheathing basally;</text>
      <biological_entity id="o10211" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at time" constraintid="o10212" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10212" name="time" name_original="time" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10213" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade filiform to linear.</text>
      <biological_entity id="o10214" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s4" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences umbellate, subtended by spathe bracts;</text>
      <biological_entity id="o10215" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
      </biological_entity>
      <biological_entity constraint="spathe" id="o10216" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o10215" id="r1460" name="subtended by" negation="false" src="d0_s5" to="o10216" />
    </statement>
    <statement id="d0_s6">
      <text>bracts 2, membranous.</text>
      <biological_entity id="o10217" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers fastigiate, withering-persistent;</text>
      <biological_entity id="o10218" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="fastigiate" value_original="fastigiate" />
        <character is_modifier="false" name="duration" src="d0_s7" value="withering-persistent" value_original="withering-persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals 6, 2-whorled, connate proximal 1/3, 1-veined, subequal;</text>
      <biological_entity id="o10219" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="2-whorled" value_original="2-whorled" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s8" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 6, adnate to tepal bases, included;</text>
      <biological_entity id="o10220" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character constraint="to tepal bases" constraintid="o10221" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="position" notes="" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity constraint="tepal" id="o10221" name="base" name_original="bases" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>filaments distinct, ± dilated basally, subulate and entire apically;</text>
      <biological_entity id="o10222" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less; basally" name="shape" src="d0_s10" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers dorsifixed, oblong, introse;</text>
      <biological_entity id="o10223" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, sessile, 3-locular;</text>
      <biological_entity id="o10224" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules several (–12);</text>
      <biological_entity id="o10225" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="several" value_original="several" />
        <character name="quantity" src="d0_s13" value="12]" value_original="12]" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style filiform;</text>
      <biological_entity id="o10226" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma terminal, small.</text>
      <biological_entity id="o10227" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s15" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular, 3-lobed, membranous, dehiscence loculicidal.</text>
      <biological_entity id="o10228" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds black, angled [compressed or almost flat].</text>
      <biological_entity id="o10229" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <other_name type="common_name">False onion</other_name>
  <other_name type="common_name">false garlic</other_name>
  <discussion>Species ca. 19 (2 in the flora).</discussion>
  <discussion>All species of Nothoscordum are native to the Americas. Nothoscordum gracile has become naturalized in Europe, Africa, Asia, and Australia; it spreads rapidly by seeds and bulblets.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 4–12 mm wide; flowers fragrant; tepals connate to 1/3 their length.</description>
      <determination>1 Nothoscordum gracile</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 1–4(–5) mm wide; flowers not fragrant; tepals distinct or nearly so.</description>
      <determination>2 Nothoscordum bivalve</determination>
    </key_statement>
  </key>
</bio:treatment>