<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">527</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="treatment_page">528</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">sacoila</taxon_name>
    <taxon_name authority="(Aublet) Garay" date="1982" rank="species">lanceolata</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mus. Leafl.</publication_title>
      <place_in_publication>28: 352. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus sacoila;species lanceolata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101877</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="Aublet" date="unknown" rank="species">lanceolatum</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Pl. Guiane</publication_title>
      <place_in_publication>2: 821. 1775</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Limodorum;species lanceolatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Satyrium</taxon_name>
    <taxon_name authority="(Aublet) León" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_hierarchy>genus Satyrium;species lanceolata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="(Aublet) León" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species lanceolata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 23–60 cm.</text>
      <biological_entity id="o6542" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="23" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: sheaths 5–9, cauline, bladeless.</text>
      <biological_entity id="o6543" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o6544" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="9" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o6545" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="bladeless" value_original="bladeless" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–7;</text>
      <biological_entity id="o6546" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole not distinct;</text>
      <biological_entity id="o6547" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic, 5–25 (–35) × 2–8 cm.</text>
      <biological_entity id="o6548" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–18 cm;</text>
      <biological_entity id="o6549" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis pubescent with bubble-shaped, glandular-capitate, and transitional hairs;</text>
      <biological_entity id="o6550" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character constraint="with hairs" constraintid="o6551" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6551" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="bubble--shaped" value_original="bubble--shaped" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="glandular-capitate" value_original="glandular-capitate" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="transitional" value_original="transitional" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bracts lanceolate.</text>
      <biological_entity constraint="floral" id="o6552" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers usually orange-red, occasionally pale green or golden bronze;</text>
      <biological_entity id="o6553" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s8" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden bronze" value_original="golden bronze" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal free, lanceolate, ascending at apex;</text>
      <biological_entity constraint="dorsal" id="o6554" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character constraint="at apex" constraintid="o6555" is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o6555" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals basally connate, lanceolate, decurrent along lateral ribs of ovary;</text>
      <biological_entity constraint="lateral" id="o6556" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character constraint="along lateral ribs" constraintid="o6557" is_modifier="false" name="shape" src="d0_s10" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6557" name="rib" name_original="ribs" src="d0_s10" type="structure" />
      <biological_entity id="o6558" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <relation from="o6557" id="r934" name="part_of" negation="false" src="d0_s10" to="o6558" />
    </statement>
    <statement id="d0_s11">
      <text>petals lanceolate, falcate, slightly decurrent basally;</text>
      <biological_entity id="o6559" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s11" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip lanceolate, base sessile, grooved, apex acute to acuminate, with linear, nearly marginal, pubescent calli near base;</text>
      <biological_entity id="o6560" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o6561" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o6562" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
      </biological_entity>
      <biological_entity id="o6563" name="callus" name_original="calli" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="nearly" name="position" src="d0_s12" value="marginal" value_original="marginal" />
        <character is_modifier="true" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6564" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o6562" id="r935" name="with" negation="false" src="d0_s12" to="o6563" />
      <relation from="o6563" id="r936" name="near" negation="false" src="d0_s12" to="o6564" />
    </statement>
    <statement id="d0_s13">
      <text>anther cap 6–7 mm.</text>
      <biological_entity constraint="anther" id="o6565" name="cap" name_original="cap" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ascending, ovoid, expanded portion 9–16 × 6–10 mm.</text>
      <biological_entity id="o6566" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o6567" name="portion" name_original="portion" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s14" to="16" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.5–1.4 mm.</text>
      <biological_entity id="o6568" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>In Florida the two varieties differ in their breeding systems, Sacoila lanceolata var. lanceolata producing seed by adventitious embryony and var. paludicola by auto-pollination (P. M. Catling 1987). Plants of this species from Florida, referred to Spiranthes lanceolata var. luteoalba (Reichenbach) Luer (C. A. Luer 1972), represent only a very restricted clone with more ascending flowers whose characters intergrade with those of plants from other parts of the range. Because all green-flowered plants addressed by Luer do not consistently possess the distinctive features he noted and do not conform to the description of var. luteoalba (Reichenbach f.) Luer, they are best recognized at the rank of forma as forma albidaviridis Catling &amp; Sheviak (P. M. Catling and C. J. Sheviak 1993).</discussion>
  <discussion>Sacoila lanceolata is widespread in tropical and subtropical America. Synonyms for Sacoila lanceolata in the restricted sense are given by L. A. Garay (1980[1982]) and in the very broad sense by C. A. Luer (1972). Two color forms associated with var. lanceolata have been named: forma albidaviridis Catling &amp; Sheviak, with green and white flowers, and forma folsomii P. M. Brown with golden bronze flowers.</discussion>
  <references>
    <reference>Brown, P. M. 1999. A striking new color form of Sacoila lanceolata. N. Amer. Native Orchid J. 5: 169–173.  </reference>
    <reference>Catling, P. M. 1987. Notes on the breeding systems of Sacoila lanceolata (Aublet) Garay (Orchidacae). Ann. Missouri Bot. Gard. 74: 58–68.  </reference>
    <reference>Luer, C. A. 1971. A variety of Spiranthes lanceolata in Florida. Florida Orchidist 14: 19.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Dorsal sepal 19–23 mm; leaves usually absent at flowering.</description>
      <determination>1a Sacoila lanceolata var. lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Dorsal sepal 9–11 mm; leaves present at flowering.</description>
      <determination>1b Sacoila lanceolata var. paludicola</determination>
    </key_statement>
  </key>
</bio:treatment>