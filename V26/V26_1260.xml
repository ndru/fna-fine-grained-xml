<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>C. A. Luer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">617</other_info_on_meta>
    <other_info_on_meta type="treatment_page">616</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Kunth" date="1815" rank="tribe">Epidendreae</taxon_name>
    <taxon_name authority="Lindley" date="1830" rank="subtribe">PLEUROTHALLIDINAE</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">PLEUROTHALLIS</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>5: 211. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe epidendreae;subtribe pleurothallidinae;genus pleurothallis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pleuron, rib, and thallos, branch, referring to cespitose, slender, aerial shoots</other_info_on_name>
    <other_info_on_name type="fna_id">126055</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs epiphytic.</text>
      <biological_entity id="o23803" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous.</text>
      <biological_entity id="o23804" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, stout;</text>
      <biological_entity id="o23805" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths tubular, variously enclosing stems.</text>
      <biological_entity id="o23806" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o23807" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <relation from="o23806" id="r3230" modifier="variously" name="enclosing" negation="false" src="d0_s3" to="o23807" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves 1, erect, sessile or petiolate;</text>
      <biological_entity id="o23808" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade leathery.</text>
      <biological_entity id="o23809" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal [axillary], 1–several lax or clustered racemes [solitary flower].</text>
      <biological_entity id="o23810" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="1-several" value_original="1-several" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="lax" value_original="lax" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o23811" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers sometimes distichous, resupinate, opening simultaneously;</text>
      <biological_entity id="o23812" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s7" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals distinct [variously connate], subequal, membranous [thickly fleshy], pubescent [smooth or verrucose];</text>
      <biological_entity id="o23813" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals glabrous [verrucose, papillose, pubescent, ciliate, or fringed];</text>
      <biological_entity id="o23814" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip simple [2–5-lobed], membranous [thickly fleshy], callous, base loosely articulate to base of column or apex of column-foot [sometimes inflexibly adnate];</text>
      <biological_entity id="o23815" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s10" value="callous" value_original="callous" />
      </biological_entity>
      <biological_entity id="o23816" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="to base" constraintid="o23817" is_modifier="false" modifier="loosely" name="architecture" src="d0_s10" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o23817" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o23818" name="column" name_original="column" src="d0_s10" type="structure" />
      <biological_entity id="o23819" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o23820" name="column-foot" name_original="column-foot" src="d0_s10" type="structure" />
      <relation from="o23817" id="r3231" name="part_of" negation="false" src="d0_s10" to="o23818" />
      <relation from="o23817" id="r3232" name="part_of" negation="false" src="d0_s10" to="o23819" />
      <relation from="o23817" id="r3233" name="part_of" negation="false" src="d0_s10" to="o23820" />
    </statement>
    <statement id="d0_s11">
      <text>column semiterete, stout [slender], winged [wingless], with or without column-foot, apex with anther exposed or hooded;</text>
      <biological_entity id="o23821" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="semiterete" value_original="semiterete" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s11" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o23822" name="column-foot" name_original="column-foot" src="d0_s11" type="structure" />
      <biological_entity id="o23823" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o23824" name="anther" name_original="anther" src="d0_s11" type="structure" />
      <relation from="o23821" id="r3234" modifier="with or without column-foot , apex with anther exposed or hooded" name="with or without" negation="false" src="d0_s11" to="o23822" />
      <relation from="o23821" id="r3235" modifier="with or without column-foot , apex with anther exposed or hooded" name="with or without" negation="false" src="d0_s11" to="o23823" />
      <relation from="o23821" id="r3236" name="with" negation="false" src="d0_s11" to="o23824" />
    </statement>
    <statement id="d0_s12">
      <text>anther apical or adaxial;</text>
      <biological_entity id="o23825" name="anther" name_original="anther" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="apical" value_original="apical" />
        <character is_modifier="false" name="position" src="d0_s12" value="adaxial" value_original="adaxial" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary articulate between ovary and minute curved pedicel;</text>
      <biological_entity id="o23826" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character constraint="between ovary, pedicel" constraintid="o23827, o23828" is_modifier="false" name="architecture" src="d0_s13" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o23827" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="minute" value_original="minute" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o23828" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="minute" value_original="minute" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollinia 2, pyriform with viscidium [spheric without viscidium] and caudicles [naked, with or without viscidium];</text>
      <biological_entity id="o23829" name="pollinium" name_original="pollinia" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character constraint="with caudicles" constraintid="o23831" is_modifier="false" name="shape" src="d0_s14" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o23830" name="viscidium" name_original="viscidium" src="d0_s14" type="structure" />
      <biological_entity id="o23831" name="caudicle" name_original="caudicles" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>stigma adaxial [apical], 1–2-lobed;</text>
      <biological_entity id="o23832" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="shape" src="d0_s15" value="1-2-lobed" value_original="1-2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>rostellum apical or adaxial.</text>
      <biological_entity id="o23833" name="rostellum" name_original="rostellum" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="apical" value_original="apical" />
        <character is_modifier="false" name="position" src="d0_s16" value="adaxial" value_original="adaxial" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, deciduous at pedicel (ca. 1 mm), ellipsoid, 3-valved.</text>
      <biological_entity constraint="fruits" id="o23834" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character constraint="at pedicel" constraintid="o23835" is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-valved" value_original="3-valved" />
      </biological_entity>
      <biological_entity id="o23835" name="pedicel" name_original="pedicel" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropics, s Fla., s Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropics" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="s Fla." establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <discussion>Species ca. 2500 (1 in the flora).</discussion>
  <discussion>Pleurothallis is the largest genus in the subtribe Pleurothallidinae, and perhaps in the Orchidaceae.</discussion>
  
</bio:treatment>