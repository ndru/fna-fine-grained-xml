<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Californicae</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="species">macrosiphon</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 144. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series californicae;species macrosiphon;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101709</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="species">elata</taxon_name>
    <taxon_hierarchy>genus Iris;species elata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes many-branched, forming tufts, with fibrous remains of old leaves at nodes, slender, 0.6–0.8 cm diam.;</text>
      <biological_entity id="o4127" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="many-branched" value_original="many-branched" />
        <character is_modifier="false" name="size" notes="" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="diameter" src="d0_s0" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4128" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o4129" name="remains" name_original="remains" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o4130" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o4131" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <relation from="o4127" id="r598" name="forming" negation="false" src="d0_s0" to="o4128" />
      <relation from="o4127" id="r599" name="with" negation="false" src="d0_s0" to="o4129" />
      <relation from="o4129" id="r600" name="part_of" negation="false" src="d0_s0" to="o4130" />
      <relation from="o4129" id="r601" name="at" negation="false" src="d0_s0" to="o4131" />
    </statement>
    <statement id="d0_s1">
      <text>roots few, fibrous.</text>
      <biological_entity id="o4132" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, solid, almost absent or to 2.5 dm.</text>
      <biological_entity id="o4133" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="almost" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s2" value="0-2.5 dm" value_original="0-2.5 dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal longer than stem, blade light green, finely veined, narrowly linear, 3–4 dm × 0.4–0.6 cm, glaucous, margins not thickened, apex acute;</text>
      <biological_entity id="o4134" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o4135" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="than stem" constraintid="o4136" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4136" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o4137" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="finely" name="architecture" src="d0_s3" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s3" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o4138" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o4139" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 1–2, spreading, sheathing for about 1/2 length, foliaceous, blade not inflated, 0.7–1 dm.</text>
      <biological_entity id="o4140" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o4141" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o4142" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s4" to="1" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence units (1–) 2-flowered;</text>
      <biological_entity constraint="inflorescence" id="o4143" name="unit" name_original="units" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)2-flowered" value_original="(1-)2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes nearly opposite, connivent, linearlanceolate, 4–9.5 cm × 4–6 mm, subequal or outer longer.</text>
      <biological_entity id="o4144" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="position" src="d0_s6" value="outer" value_original="outer" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth indigo, purple, lavender, white, cream, or yellow;</text>
      <biological_entity id="o4145" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4146" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="indigo" value_original="indigo" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube linear, gradually dilating apically, 6 cm;</text>
      <biological_entity id="o4147" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4148" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character modifier="gradually; apically" name="some_measurement" src="d0_s8" unit="cm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals usually with fine, dark veins basally, becoming coarser on claw, oblanceolate or obovate, 3.9–7 × 2 cm, base abruptly attenuate into claw;</text>
      <biological_entity id="o4149" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4150" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="on claw" constraintid="o4152" is_modifier="false" modifier="becoming" name="relief" notes="" src="d0_s9" value="coarser" value_original="coarser" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.9" from_unit="cm" name="length" src="d0_s9" to="7" to_unit="cm" />
        <character name="width" src="d0_s9" unit="cm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4151" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="fine" value_original="fine" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o4152" name="claw" name_original="claw" src="d0_s9" type="structure" />
      <biological_entity id="o4153" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="into claw" constraintid="o4154" is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o4154" name="claw" name_original="claw" src="d0_s9" type="structure" />
      <relation from="o4150" id="r602" name="with" negation="false" src="d0_s9" to="o4151" />
    </statement>
    <statement id="d0_s10">
      <text>petals oblanceolate, 3.5–6 × 0.5–1.6 cm, base gradually attenuate;</text>
      <biological_entity id="o4155" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4156" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s10" to="1.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4157" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s10" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary ovoid, ca. 1 cm;</text>
      <biological_entity id="o4158" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4159" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s11" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 2–3.5 cm, crests overlapping, reflexed, semiovate, 0.8–1.8 cm, margins denticulate;</text>
      <biological_entity id="o4160" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4161" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4162" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="semiovate" value_original="semiovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s12" to="1.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4163" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas triangular, margins entire;</text>
      <biological_entity id="o4164" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4165" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o4166" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 1.5–2 cm.</text>
      <biological_entity id="o4167" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o4168" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s14" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong to ovoid, somewhat 3-angled in cross-section, 2.5–3 cm.</text>
      <biological_entity id="o4169" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="ovoid" />
        <character constraint="in cross-section" constraintid="o4170" is_modifier="false" modifier="somewhat" name="shape" src="d0_s15" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s15" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4170" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, angular, finely wrinkled.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o4171" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="angular" value_original="angular" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4172" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sunny hillsides, meadows, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sunny hillsides" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Ground iris</other_name>
  <discussion>Iris macrosiphon hybridizes with I. chrysophylla, I. douglasiana, I. fernaldii, I. hartwegii, I. innominata, I. munzii, I. purdyi, I. tenax, and I. tenuissima.</discussion>
  <discussion>The invalid name “Iris californica” Leichtlin has sometimes been applied to a portion of this species.</discussion>
  
</bio:treatment>