<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="treatment_page">275</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Douglas ex S. Watson" date="1879" rank="species">scilloides</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 229. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species scilloides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101399</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–5, not clustered on stout, primary rhizome, increase bulbs ± equaling parent bulbs, never appearing as basal cluster, rhizomes absent, globose to ovoid, 1–2 cm × 8–20 mm;</text>
      <biological_entity id="o12576" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="5" />
        <character constraint="on primary rhizome" constraintid="o12577" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity constraint="primary" id="o12577" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o12578" name="bulb" name_original="bulbs" src="d0_s0" type="structure" />
      <biological_entity constraint="parent" id="o12579" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s0" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12580" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="never" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o12581" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s0" to="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="2" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s0" to="20" to_unit="mm" />
      </biological_entity>
      <relation from="o12576" id="r1748" name="increase" negation="false" src="d0_s0" to="o12578" />
      <relation from="o12576" id="r1749" name="appearing as" negation="false" src="d0_s0" to="o12580" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing renewal bulbs, reddish or brownish, membranous, lacking cellular reticulation or cells arranged in only 2–3 rows distal to roots, ± quadrate, without fibers;</text>
      <biological_entity constraint="outer" id="o12582" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="renewal" id="o12583" name="bulb" name_original="bulbs" src="d0_s1" type="structure" />
      <biological_entity id="o12584" name="reticulation" name_original="reticulation" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="cellular" value_original="cellular" />
        <character constraint="in rows" constraintid="o12586" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o12585" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="cellular" value_original="cellular" />
        <character constraint="in rows" constraintid="o12586" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o12586" name="row" name_original="rows" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" modifier="only" name="quantity" src="d0_s1" to="3" />
        <character constraint="to roots" constraintid="o12587" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o12587" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o12588" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o12582" id="r1750" name="enclosing" negation="false" src="d0_s1" to="o12583" />
      <relation from="o12584" id="r1751" name="without" negation="false" src="d0_s1" to="o12588" />
      <relation from="o12585" id="r1752" name="without" negation="false" src="d0_s1" to="o12588" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white or light-brown, cells absent or obscure and ± quadrate.</text>
      <biological_entity constraint="inner" id="o12589" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity id="o12590" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous with scape after seeds mature, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o12591" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with scape" constraintid="o12592" is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o12592" name="scape" name_original="scape" src="d0_s3" type="structure" />
      <biological_entity id="o12593" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12594" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o12595" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o12596" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o12592" id="r1753" name="after" negation="false" src="d0_s3" to="o12593" />
      <relation from="o12594" id="r1754" name="extending" negation="false" src="d0_s3" to="o12595" />
      <relation from="o12594" id="r1755" name="extending" negation="false" src="d0_s3" to="o12596" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, falcate, 6–15 cm × 2–4 mm, margins entire or obscurely papillose.</text>
      <biological_entity id="o12597" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12598" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape usually forming abcission layer and deciduous with leaves after seeds mature, frequently breaking at this level after pressing, solitary, erect, solid, strongly flattened, 2-edged or winged distally, wings obscurely papillose, 4–8 cm × 1–3 mm.</text>
      <biological_entity id="o12599" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character constraint="with leaves" constraintid="o12602" is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="frequently" name="architecture_or_arrangement_or_growth_form" notes="" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-edged" value_original="2-edged" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o12600" name="abcission" name_original="abcission" src="d0_s5" type="structure" />
      <biological_entity id="o12601" name="layer" name_original="layer" src="d0_s5" type="structure" />
      <biological_entity id="o12602" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12603" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o12604" name="pressing" name_original="pressing" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s5" value="level" value_original="level" />
      </biological_entity>
      <biological_entity id="o12605" name="wing" name_original="wings" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o12599" id="r1756" name="forming" negation="false" src="d0_s5" to="o12600" />
      <relation from="o12599" id="r1757" name="forming" negation="false" src="d0_s5" to="o12601" />
      <relation from="o12602" id="r1758" name="after" negation="false" src="d0_s5" to="o12603" />
      <relation from="o12599" id="r1759" name="breaking at" negation="false" src="d0_s5" to="o12604" />
    </statement>
    <statement id="d0_s6">
      <text>Umbel decisuous with scape, erect, compact, 5–12-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o12606" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-12-flowered" value_original="5-12-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o12607" name="scape" name_original="scape" src="d0_s6" type="structure" />
      <biological_entity id="o12608" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
      <relation from="o12606" id="r1760" name="with" negation="false" src="d0_s6" to="o12607" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, 3-veined, broadly ovate, ± equal, apex acute.</text>
      <biological_entity constraint="spathe" id="o12609" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o12610" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate, 6–8 mm;</text>
      <biological_entity id="o12611" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, white or pinkish with green midribs, becoming reddish purple, broadly elliptic-oblong, ± equal, becoming rigid, papery and ± connivent over fruit, margins entire, apex obtuse, not involute at tip;</text>
      <biological_entity id="o12612" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character constraint="with midribs" constraintid="o12613" is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" modifier="becoming" name="coloration" notes="" src="d0_s9" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="texture" src="d0_s9" value="papery" value_original="papery" />
        <character constraint="over fruit" constraintid="o12614" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s9" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o12613" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12614" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o12615" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12616" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character constraint="at tip" constraintid="o12617" is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o12617" name="tip" name_original="tip" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o12618" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers purple;</text>
      <biological_entity id="o12619" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen white to gray;</text>
      <biological_entity id="o12620" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crestless or obscurely crested;</text>
      <biological_entity id="o12621" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="crestless" value_original="crestless" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 3, central, rounded, margins entire;</text>
      <biological_entity id="o12622" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o12623" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o12624" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o12625" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, unlobed or minutely 3-lobed;</text>
      <biological_entity id="o12626" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 2–10.5 (–18.5 in fruit) mm.</text>
      <biological_entity id="o12627" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o12628" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o12629" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12630" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Barren, gravelly soils, arid interior slopes and ridges well back from Columbia River</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="interior slopes" modifier="arid" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>95.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Allium scilloides is known only from east of the Cascades.</discussion>
  
</bio:treatment>