<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">fimbriatum</taxon_name>
    <taxon_name authority="(Eastwood) Ownbey ex McNeal" date="1992" rank="variety">purdyi</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>13: 423. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species fimbriatum;variety purdyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102148</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">purdyi</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 110. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Allium;species purdyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Scape 10–35 cm × 1.5–3.5 mm.</text>
      <biological_entity id="o31545" name="scape" name_original="scape" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s0" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Umbel 20–75-flowered.</text>
      <biological_entity id="o31546" name="umbel" name_original="umbel" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="20-75-flowered" value_original="20-75-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: tepals white to pale lavender with darker midvein, apex not spreading or recurved at tip;</text>
      <biological_entity id="o31547" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o31548" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="with midvein" constraintid="o31549" from="white" name="coloration" src="d0_s2" to="pale lavender" />
      </biological_entity>
      <biological_entity id="o31549" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o31550" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character constraint="at tip" constraintid="o31551" is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o31551" name="tip" name_original="tip" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>ovarian crest process margins denticulate to laciniate (crests rarely absent).</text>
      <biological_entity id="o31552" name="flower" name_original="flowers" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 14.</text>
      <biological_entity constraint="process" id="o31553" name="margin" name_original="margins" src="d0_s3" type="structure" constraint_original="ovarian crest process">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s3" to="laciniate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31554" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine clay</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine clay" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45c.</number>
  <discussion>Allium fimbriatum var. purdyi is known only from the vicinity of Clear Lake.</discussion>
  
</bio:treatment>