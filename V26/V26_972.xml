<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="treatment_page">464</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1866" rank="genus">manfreda</taxon_name>
    <taxon_name authority="Verhoek" date="1978" rank="species">sileri</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 168, figs. 4–6. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus manfreda;species sileri</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101771</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes globose.</text>
      <biological_entity id="o16740" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves spreading, succulent, (14–) 25–39 × 2.2–4.8 cm;</text>
      <biological_entity id="o16741" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
        <character char_type="range_value" from="14" from_unit="cm" name="atypical_length" src="d0_s1" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s1" to="39" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s1" to="4.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade light green, glaucous, spotted with green or brown round to oval markings up to 1 × 0.5 cm, channeled and undulate, or flat, broadly lanceolate, margins with teeth of several sizes, irregularly spaced, often retrorse.</text>
      <biological_entity id="o16742" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light green" value_original="light green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character constraint="with markings" constraintid="o16743" is_modifier="false" name="coloration" src="d0_s2" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="shape" src="d0_s2" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o16743" name="marking" name_original="markings" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="1" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16744" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="of several sizes; irregularly" name="arrangement" notes="" src="d0_s2" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity id="o16745" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <relation from="o16744" id="r2296" name="with" negation="false" src="d0_s2" to="o16745" />
    </statement>
    <statement id="d0_s3">
      <text>Scape 21.4–21.95 dm.</text>
      <biological_entity id="o16746" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="21.4" from_unit="dm" name="some_measurement" src="d0_s3" to="21.95" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences crowded, 2.8–3.95 dm, bearing 27–46 (–81) flowers.</text>
      <biological_entity id="o16747" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.8" from_unit="dm" name="some_measurement" src="d0_s4" to="3.95" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o16748" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="46" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="81" />
        <character char_type="range_value" from="27" is_modifier="true" name="quantity" src="d0_s4" to="46" />
      </biological_entity>
      <relation from="o16747" id="r2297" name="bearing" negation="false" src="d0_s4" to="o16748" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers nearly erect, with cooked onion odor;</text>
      <biological_entity id="o16749" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals golden green adaxially, glaucous and green abaxially;</text>
      <biological_entity id="o16750" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="golden green" value_original="golden green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube broadly funnelform-campanulate, 0.7–1.5 (–2.2) cm;</text>
      <biological_entity id="o16751" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="funnelform-campanulate" value_original="funnelform-campanulate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s7" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="distance" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>limb lobes recurved, usually longer than tube, 0.7–2.1 cm;</text>
      <biological_entity constraint="limb" id="o16752" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character constraint="than tube" constraintid="o16753" is_modifier="false" name="length_or_size" src="d0_s8" value="usually longer" value_original="usually longer" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="2.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16753" name="tube" name_original="tube" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>filaments nearly erect at maturity, exserted by 4–6.6 (–9.7) cm, bent near middle in bud;</text>
      <biological_entity id="o16754" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="by at cm" constraintid="o16755" is_modifier="false" modifier="nearly" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16755" name="cm" name_original="cm" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o16756" name="bud" name_original="bud" src="d0_s9" type="structure" />
      <relation from="o16755" id="r2298" name="in" negation="false" src="d0_s9" to="o16756" />
    </statement>
    <statement id="d0_s10">
      <text>ovary 10–20 mm;</text>
      <biological_entity id="o16757" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style slightly longer than stamens;</text>
      <biological_entity id="o16758" name="style" name_original="style" src="d0_s11" type="structure">
        <character constraint="than stamens" constraintid="o16759" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o16759" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stigma pale green, clavate, 3-angled, furrowed apically, lobes not reflexed.</text>
      <biological_entity id="o16760" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s12" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity id="o16761" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cylindrical, 2.3–3.1 × 1.6–1.9 cm.</text>
      <biological_entity id="o16762" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="length" src="d0_s13" to="3.1" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s13" to="1.9" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Growing separately or in colonies, in open areas, on clay soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="colonies" modifier="growing separately or in" constraint="in open areas" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="clay soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>