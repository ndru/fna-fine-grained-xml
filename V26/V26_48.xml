<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HELONIAS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 342. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 159. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus HELONIAS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek helos, marsh, alluding to the habitat</other_info_on_name>
    <other_info_on_name type="fna_id">114956</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, subscapose, glabrous, from stout rhizomes;</text>
      <biological_entity id="o13956" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o13957" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o13956" id="r1940" name="from" negation="false" src="d0_s0" to="o13957" />
    </statement>
    <statement id="d0_s1">
      <text>roots contractile, fibrous.</text>
      <biological_entity id="o13958" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="contractile" value_original="contractile" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, simple, hollow, glabrous.</text>
      <biological_entity id="o13959" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evergreen, in basal rosettes, reduced distally to bractlike leaves;</text>
      <biological_entity id="o13960" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="evergreen" value_original="evergreen" />
        <character constraint="to leaves" constraintid="o13962" is_modifier="false" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13961" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity id="o13962" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <relation from="o13960" id="r1941" name="in" negation="false" src="d0_s3" to="o13961" />
    </statement>
    <statement id="d0_s4">
      <text>blade simple, oblong-spatulate to oblanceolate, margins entire, apex acute, glabrous.</text>
      <biological_entity id="o13963" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="oblong-spatulate" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o13964" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13965" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemose, dense, ebracteate, pedicellate.</text>
      <biological_entity id="o13966" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers spreading, fragrant, funnel-shaped;</text>
      <biological_entity id="o13967" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="odor" src="d0_s6" value="fragrant" value_original="fragrant" />
        <character is_modifier="false" name="shape" src="d0_s6" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals persistent, 6, distinct to barely connate basally, purplish-pink, becoming green, spatulate to oblong, nectary proximal, adaxial, weakly sulcate;</text>
      <biological_entity id="o13968" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character char_type="range_value" from="distinct" modifier="basally" name="fusion" src="d0_s7" to="barely connate" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish-pink" value_original="purplish-pink" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s7" to="oblong" />
      </biological_entity>
      <biological_entity id="o13969" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s7" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sulcate" value_original="sulcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens persistent, 6, equaling tepals;</text>
      <biological_entity id="o13970" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o13971" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments distinct, inner 3 proximally adnate to ovary;</text>
      <biological_entity id="o13972" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character constraint="to ovary" constraintid="o13973" is_modifier="false" modifier="proximally" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o13973" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers dorsifixed, versatile, 1-locular, extrorse, pollen-sacs apically confluent;</text>
      <biological_entity id="o13974" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s10" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="fixation" src="d0_s10" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s10" value="extrorse" value_original="extrorse" />
      </biological_entity>
      <biological_entity id="o13975" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="apically" name="arrangement" src="d0_s10" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 3-locular proximally, 1-locular distally;</text>
      <biological_entity id="o13976" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="distally" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>septal nectaries absent;</text>
      <biological_entity constraint="septal" id="o13977" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 3, depressed apically into ovary apex, ascending to arching, distinct, sessile;</text>
      <biological_entity id="o13978" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character constraint="into ovary apex" constraintid="o13979" is_modifier="false" name="shape" src="d0_s13" value="depressed" value_original="depressed" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s13" to="arching" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o13979" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stigmas not papillate along adaxial surface.</text>
      <biological_entity id="o13980" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character constraint="along adaxial surface" constraintid="o13981" is_modifier="false" modifier="not" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13981" name="surface" name_original="surface" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsular, deeply 3-lobed, papery, dehiscence loculicidal.</text>
      <biological_entity id="o13982" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s15" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="texture" src="d0_s15" value="papery" value_original="papery" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 16 per locule, linear-fusiform, caudate at both ends.</text>
      <biological_entity id="o13984" name="locule" name_original="locule" src="d0_s16" type="structure" />
      <biological_entity id="o13985" name="end" name_original="ends" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>x = 17.</text>
      <biological_entity id="o13983" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character constraint="per locule" constraintid="o13984" name="quantity" src="d0_s16" value="16" value_original="16" />
        <character is_modifier="false" name="shape" notes="" src="d0_s16" value="linear-fusiform" value_original="linear-fusiform" />
        <character constraint="at ends" constraintid="o13985" is_modifier="false" name="shape" src="d0_s16" value="caudate" value_original="caudate" />
      </biological_entity>
      <biological_entity constraint="x" id="o13986" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Species 1.</discussion>
  <discussion>The close relationships among Helonias and the east Asian Heloniopsis A. Gray and Ypsilandra Franchet have been noted repeatedly. They exhibit many similarities in morphology and anatomy (F. Buxbaum 1925, 1927; W. Schulze 1978b; C. Sterling 1980; F. H. Utech 1978b; F. H. Utech and S. Kawano 1981; N. Tanaka 1997, 1997b, 1997c, 1997d, 1997e, 1998), palynology (M. Takahashi and S. Kawano 1989), ecology (S. Kawano and J. Masuda 1980; H. Takahashi 1988), and karyology (F. H. Utech 1980). Based on these similarities, Tanaka merged the five species of Heloniopsis and the three species of Ypsilandra into Helonias. However, molecular evidence indicates that, although the genera are related, they are distinct (S. Fuse and M. N. Tamura 2000). This group has been recognized as a separate tribe, Heloniadeae Fries, within the Melanthiaceae sensu stricto (M. N. Tamura 1998; W. B. Zomlefer 1997b), or placed in a separate family, Heloniadaceae J. Agardh (A. L. Takhtajan 1997).</discussion>
  <discussion>Helonias in eastern North America and the Ypsilandra–Heloniopsis lineage in eastern Asia comprise a paired Arcto-Tertiary element in which Ypsilandra (western China to the Himalayas) and Heloniopis (Taiwan, Korea to Japan) have differentiated from each other in eastern Asia (N. Tanaka 1997e).</discussion>
  <references>
    <reference>Sutter, R. D. 1984. The status of Helonias bullata L. (Liliaceae) in the southern Appalachians. Castanea 49: 9–16.  </reference>
    <reference>Tanaka, N. 1997. Taxonomic significance of some floral characters in Helonias and Ypsilandra (Liliaceae). J. Jap. Bot. 72: 110–116.  </reference>
    <reference>Tanaka, N. 1997b. Evolutionary significance of the variation of the floral structure of Heloniopsis. J. Jap. Bot. 72: 131–138.  </reference>
    <reference>Tanaka, N. 1997c. Phylogenetic and taxonomic studies on Helonias, Ypsilandra and Heloniopsis. I. Comparison of character states (1). J. Jap. Bot. 72: 221–228.  </reference>
    <reference>Tanaka, N. 1997d. Phylogenetic and taxonomic studies on Helonias, Ypsilandra and Heloniopsis. I. Comparison of character states (2). J. Jap. Bot. 72: 286–292.  </reference>
    <reference>Tanaka, N. 1997e. Phylogenetic and taxonomic studies on Helonias, Ypsilandra and Heloniopsis. II. Evolution and geographical distribution. J. Jap. Bot. 72: 329–336.  </reference>
    <reference>Tanaka, N. 1998. Phylogenetic and taxonomic studies on Helonias, Ypsilandra and Heloniopsis. III. Taxonomic revision. J. Jap. Bot. 73: 102–115.  </reference>
    <reference>Utech, F. H. 1978b. Vascular floral anatomy of Helonias bullata (Liliaceae–Helonieae), with a comparison to the Asian Heloniopsis orientalis. Ann. Carnegie Mus. 47: 169–191.</reference>
  </references>
  
</bio:treatment>