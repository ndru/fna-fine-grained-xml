<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak,Paul M. Catling</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="treatment_page">632</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="(Nuttall) Torrey" date="1818" rank="genus">APLECTRUM</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 197. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus aplectrum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a, without, and plectron, spur</other_info_on_name>
    <other_info_on_name type="fna_id">102263</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o31284" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots in fascicles at base of pseudobulb, slender, fibrous.</text>
      <biological_entity id="o31285" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o31286" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o31287" name="pseudobulb" name_original="pseudobulb" src="d0_s1" type="structure" />
      <relation from="o31285" id="r4207" modifier="in fascicles" name="at" negation="false" src="d0_s1" to="o31286" />
      <relation from="o31286" id="r4208" name="part_of" negation="false" src="d0_s1" to="o31287" />
    </statement>
    <statement id="d0_s2">
      <text>Corms connected by slender rhizomes.</text>
      <biological_entity id="o31288" name="corm" name_original="corms" src="d0_s2" type="structure">
        <character constraint="by slender rhizomes" constraintid="o31289" is_modifier="false" name="fusion" src="d0_s2" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity constraint="slender" id="o31289" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems scapes, cormose.</text>
      <biological_entity constraint="stems" id="o31290" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="cormose" value_original="cormose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves produced in late summer–fall, withering in spring, solitary, borne at distal end of corm, plicate.</text>
      <biological_entity id="o31291" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="in spring" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o31292" name="summer-fall" name_original="summer-fall" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o31293" name="end" name_original="end" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o31294" name="corm" name_original="corm" src="d0_s4" type="structure" />
      <relation from="o31291" id="r4209" name="produced in late" negation="false" src="d0_s4" to="o31292" />
      <relation from="o31291" id="r4210" name="borne at" negation="false" src="d0_s4" to="o31293" />
      <relation from="o31291" id="r4211" name="borne at" negation="false" src="d0_s4" to="o31294" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary, lax racemes from lateral nodes on corm;</text>
      <biological_entity id="o31295" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o31296" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o31297" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o31298" name="corm" name_original="corm" src="d0_s5" type="structure" />
      <relation from="o31296" id="r4212" name="from" negation="false" src="d0_s5" to="o31297" />
      <relation from="o31297" id="r4213" name="on" negation="false" src="d0_s5" to="o31298" />
    </statement>
    <statement id="d0_s6">
      <text>floral bracts inconspicuous;</text>
      <biological_entity constraint="floral" id="o31299" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline bracts sheathing, large.</text>
      <biological_entity constraint="cauline" id="o31300" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="size" src="d0_s7" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers several, resupinate;</text>
      <biological_entity id="o31301" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals to 15 mm;</text>
      <biological_entity id="o31302" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip 3-lobed, with 3 parallel lamellae;</text>
      <biological_entity id="o31303" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o31304" name="lamella" name_original="lamellae" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
      <relation from="o31303" id="r4214" name="with" negation="false" src="d0_s10" to="o31304" />
    </statement>
    <statement id="d0_s11">
      <text>column compressed;</text>
      <biological_entity id="o31305" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollinarium 1;</text>
    </statement>
    <statement id="d0_s13">
      <text>pollinea 4, attached to viscidium by short stipe;</text>
      <biological_entity id="o31306" name="pollinarium" name_original="pollinarium" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
        <character is_modifier="false" name="fixation" src="d0_s13" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o31307" name="viscidium" name_original="viscidium" src="d0_s13" type="structure" />
      <biological_entity id="o31308" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
      </biological_entity>
      <relation from="o31307" id="r4215" name="by" negation="false" src="d0_s13" to="o31308" />
    </statement>
    <statement id="d0_s14">
      <text>stigma deeply concave.</text>
      <biological_entity id="o31309" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s14" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, pendent, straight, 7 mm.</text>
      <biological_entity constraint="fruits" id="o31310" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, e Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>55.</number>
  <other_name type="common_name">Putty-root</other_name>
  <other_name type="common_name">Adam-and-Eve</other_name>
  <other_name type="common_name">aplectrum d’hiver</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  
</bio:treatment>