<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">166</other_info_on_meta>
    <other_info_on_meta type="treatment_page">168</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fritillaria</taxon_name>
    <taxon_name authority="(Linnaeus) Ker Gawler" date="1809" rank="species">camschatcensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>30: under plate 1216. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus fritillaria;species camschatcensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101618</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">camschatcense</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 303. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lilium;species camschatcense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb scales: large 6–15;</text>
      <biological_entity constraint="bulb" id="o26820" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s0" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>small 30–190.</text>
      <biological_entity constraint="bulb" id="o26821" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s1" to="190" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem 2–6 dm.</text>
      <biological_entity id="o26822" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in 1–3 whorls of 5–9 leaves per node proximally, alternate distally, 4–10 cm, usually shorter than inflorescence;</text>
      <biological_entity id="o26823" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character constraint="than inflorescence" constraintid="o26827" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o26824" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o26825" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity id="o26826" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o26827" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <relation from="o26823" id="r3655" name="in" negation="false" src="d0_s3" to="o26824" />
      <relation from="o26824" id="r3656" name="part_of" negation="false" src="d0_s3" to="o26825" />
      <relation from="o26824" id="r3657" name="per" negation="false" src="d0_s3" to="o26826" />
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly to broadly lanceolate;</text>
      <biological_entity id="o26828" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal leaves usually ± equaling proximalmost leaf.</text>
      <biological_entity constraint="distal" id="o26829" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximalmost" id="o26830" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="usually more or less" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers spreading to nodding, odor unpleasant;</text>
      <biological_entity id="o26831" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s6" to="nodding" />
        <character is_modifier="false" name="odor" src="d0_s6" value="unpleasant" value_original="unpleasant" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals dark greenish brown to brownish purple, sometimes streaked or spotted with yellow, oblongelliptic to elliptic-obovate, 2–3 cm, apex not recurved;</text>
      <biological_entity id="o26832" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark greenish" value_original="dark greenish" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s7" to="brownish purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="spotted with yellow" value_original="spotted with yellow" />
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s7" to="elliptic-obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26833" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectaries obscure, same color as tepals, linear, ± equaling tepal length;</text>
      <biological_entity id="o26834" name="nectary" name_original="nectaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o26835" name="tepal" name_original="tepals" src="d0_s8" type="structure" />
      <biological_entity id="o26836" name="tepal" name_original="tepal" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o26834" id="r3658" name="as" negation="false" src="d0_s8" to="o26835" />
    </statement>
    <statement id="d0_s9">
      <text>style obviously branched for 2/3 its length, branches longer than 1.5 mm.</text>
      <biological_entity id="o26837" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="obviously" name="length" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o26838" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules cylindric-ovoid.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24, 36.</text>
      <biological_entity id="o26839" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric-ovoid" value_original="cylindric-ovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26840" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist areas from near tideflats to mountain meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" constraint="from near tideflats to mountain meadows" />
        <character name="habitat" value="near tideflats" />
        <character name="habitat" value="mountain meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Oreg., Wash.; Asia (Japan, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Coastal Native Americans used bulbs of this species for food. Often the bulbs were dried and later added to other foods, especially soups and fish dishes.</discussion>
  <references>
    <reference>  Matsura, H. and H. Toyokuni. 1963. A karyological and taxonomical study of Fritillaria camschatcensis. Sci. Rep. Tohoku Imp. Univ., Ser. 4, Biol. 29: 239–245.</reference>
  </references>
  
</bio:treatment>