<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">smilacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">smilax</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>244. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family smilacaceae;genus smilax;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101940</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilax</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">humilis</taxon_name>
    <taxon_hierarchy>genus Smilax;species humilis;</taxon_hierarchy>
    <other_info_on_name>name proposed for rejection</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilax</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">pubera</taxon_name>
    <taxon_hierarchy>genus Smilax;species pubera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilax</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">puberula</taxon_name>
    <taxon_hierarchy>genus Smilax;species puberula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or vines;</text>
      <biological_entity id="o5199" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes black, knotted, 5–6 × 2 cm, often with white to pinkish stolons.</text>
      <biological_entity id="o5201" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotted" value_original="knotted" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character constraint="often with stolons" constraintid="o5202" name="width" src="d0_s1" unit="cm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5202" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s1" to="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems perennial, prostrate to clambering, branching, slender, to 1 m, ± woody, densely woolly-pubescent, usually prickly (especially at base).</text>
      <biological_entity id="o5203" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="clambering" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="1" to_unit="m" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="woolly-pubescent" value_original="woolly-pubescent" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="prickly" value_original="prickly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly evergreen, ± evenly disposed;</text>
      <biological_entity id="o5204" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s3" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s3" value="disposed" value_original="disposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.05–0.25 cm, often longer on sterile shoots;</text>
      <biological_entity id="o5205" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.05" from_unit="cm" name="some_measurement" src="d0_s4" to="0.25" to_unit="cm" />
        <character constraint="on shoots" constraintid="o5206" is_modifier="false" modifier="often" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o5206" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade gray-green, drying to ashy gray-green, obovate to ovatelanceolate, with 3 prominent veins, 6–10.5 × 5–8 cm, glabrous adaxially, densely puberulent abaxially, base cordate to deeply notched, margins entire, apex bluntly pointed.</text>
      <biological_entity id="o5207" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="condition" src="d0_s5" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ashy gray-green" value_original="ashy gray-green" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="ovatelanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" notes="" src="d0_s5" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" notes="" src="d0_s5" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5208" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="true" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o5209" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="deeply notched" />
      </biological_entity>
      <biological_entity id="o5210" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5211" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="bluntly" name="shape" src="d0_s5" value="pointed" value_original="pointed" />
      </biological_entity>
      <relation from="o5207" id="r768" name="with" negation="false" src="d0_s5" to="o5208" />
    </statement>
    <statement id="d0_s6">
      <text>Umbels 1–7, axillary to leaves, 5–16-flowered, loose, spherical;</text>
      <biological_entity id="o5212" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="7" />
        <character constraint="to leaves" constraintid="o5213" is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="5-16-flowered" value_original="5-16-flowered" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spherical" value_original="spherical" />
      </biological_entity>
      <biological_entity id="o5213" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.2–0.8 cm, shorter than to 1.5 as long as petiole of subtending leaf.</text>
      <biological_entity id="o5214" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s7" to="0.8" to_unit="cm" />
        <character constraint="than 0-1.5 as-long-as petiole" constraintid="o5215" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5215" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s7" to="1.5" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o5216" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <relation from="o5215" id="r769" name="part_of" negation="false" src="d0_s7" to="o5216" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth yellowish;</text>
      <biological_entity id="o5217" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5218" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals 3–4 mm;</text>
      <biological_entity id="o5219" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5220" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers much shorter than filaments;</text>
      <biological_entity id="o5221" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5222" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="than filaments" constraintid="o5223" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o5223" name="filament" name_original="filaments" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovule 1 per locule;</text>
      <biological_entity id="o5224" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5225" name="ovule" name_original="ovule" src="d0_s11" type="structure">
        <character constraint="per locule" constraintid="o5226" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5226" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel thin, 0.1–0.4 cm.</text>
      <biological_entity id="o5227" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5228" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s12" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Berries red, ovoid, 5–8 mm, with acute beaks, not glaucous.</text>
      <biological_entity id="o5229" name="berry" name_original="berries" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s13" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o5230" name="beak" name_original="beaks" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o5229" id="r770" name="with" negation="false" src="d0_s13" to="o5230" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods, along streams, sandy soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" constraint="along streams , sandy soil" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="sandy soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Sarsaparilla vine</other_name>
  <discussion>The red, pointed fruits and densely pubescent herbage of Smilax pumila are distinctive. In Louisiana, the dried leaves are used to prepare a tea for upset stomach.</discussion>
  <discussion>The name Smilax humilis Miller, which predates S. pumila by 20 years and recently has been determined to apply also to this species, has been proposed for rejection (J. L. Reveal 2000). If that proposal is not adopted, the correct name will be S. humilis.</discussion>
  
</bio:treatment>