<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">599</other_info_on_meta>
    <other_info_on_meta type="mention_page">600</other_info_on_meta>
    <other_info_on_meta type="treatment_page">598</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">calopogon</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="species">multiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Orchid. Pl.,</publication_title>
      <place_in_publication>425. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus calopogon;species multiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101508</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calopogon</taxon_name>
    <taxon_name authority="(Walter) Ames" date="unknown" rank="species">barbatus</taxon_name>
    <taxon_name authority="(Lindley) Correll" date="unknown" rank="variety">multiflorus</taxon_name>
    <taxon_hierarchy>genus Calopogon;species barbatus;variety multiflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helleborine</taxon_name>
    <taxon_name authority="(Lindley) Kuntze" date="unknown" rank="species">multiflora</taxon_name>
    <taxon_hierarchy>genus Helleborine;species multiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="(Lindley) C. Mohr" date="unknown" rank="species">multiflorum</taxon_name>
    <taxon_hierarchy>genus Limodorum;species multiflorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="Small  " date="unknown" rank="species">pinetorum</taxon_name>
    <taxon_hierarchy>genus Limodorum;species pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 17–33 (–50) cm.</text>
      <biological_entity id="o28785" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="33" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" src="d0_s0" to="33" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms elongate, forked, 18–40 mm.</text>
      <biological_entity id="o28786" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="forked" value_original="forked" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear, 3–15 cm × 3–9 mm at flowering, later elongating to 6–41 cm.</text>
      <biological_entity id="o28787" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o28788" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" constraint="at flowering" from="3" from_unit="mm" name="width" src="d0_s2" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="later" name="length" src="d0_s2" value="elongating" value_original="elongating" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="41" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences dark purple, becoming green after flowering, 17–33 (–50) cm;</text>
      <biological_entity id="o28789" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character constraint="after flowering , 17-33(-50) cm" is_modifier="false" modifier="becoming" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral bracts ovate, ovatelanceolate, or subulate, 3–8 mm.</text>
      <biological_entity constraint="floral" id="o28790" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 2–15, opening nearly simultaneously, crimson, magenta, to rarely light pink, strongly fragrant to pungent;</text>
      <biological_entity id="o28791" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" modifier="nearly simultaneously; simultaneously" name="coloration" src="d0_s5" value="crimson" value_original="crimson" />
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s5" to="rarely light pink" />
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s5" to="rarely light pink" />
        <character char_type="range_value" from="strongly fragrant" name="odor" src="d0_s5" to="pungent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>dorsal sepal obovate to oblanceolate, 9–17 × 5–8 mm, apex acuminate;</text>
      <biological_entity constraint="dorsal" id="o28792" name="sepal" name_original="sepal" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28793" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals reflexed only at tip or straight, ovate to lanceolate, slightly falcate to straight, 10–13.5 × 6–9 mm;</text>
      <biological_entity constraint="lateral" id="o28794" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="at tip" constraintid="o28795" is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="13.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28795" name="tip" name_original="tip" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petals pandurate, rarely obovate, 8–14 × 4–8 mm, apex obtuse;</text>
      <biological_entity id="o28796" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="pandurate" value_original="pandurate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28797" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip 7–12 mm, middle lobe with dilated apex triangular, rounded to truncate, 7–13 mm wide;</text>
      <biological_entity id="o28798" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o28799" name="lobe" name_original="lobe" src="d0_s9" type="structure">
        <character char_type="range_value" from="rounded" name="shape" notes="" src="d0_s9" to="truncate" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28800" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o28799" id="r3888" name="with" negation="false" src="d0_s9" to="o28800" />
    </statement>
    <statement id="d0_s10">
      <text>column 5.5–7.5 × 1–2 mm, distal end 5–7 mm wide;</text>
      <biological_entity id="o28801" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28802" name="end" name_original="end" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rostellum present.</text>
      <biological_entity id="o28803" name="rostellum" name_original="rostellum" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ellipsoid to obconic, 13–22 × 4–7 mm. 2n = 40, 42.</text>
      <biological_entity id="o28804" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s12" to="obconic" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s12" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28805" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr, rarely Nov–Aug (s Fla only), usually several weeks following fires.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="flowering time" char_type="range_value" modifier="rarely;  (s Fla only)" to="Aug" from="Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, relatively dry pine savannas and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="dry pine savannas" modifier="relatively" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Many-flowered grass-pink</other_name>
  <discussion>Calopogon multiflorus is nearly extirpated outside of central Florida.</discussion>
  <discussion>Distinctive features of Calopogon multiflorus are the purple rachis, forked corm, strong floral fragrance, pandurate petals, and a lip usually as wide or wider than long. Although previously considered to be a variety of Calopogon barbatus (D. S. Correll 1940), C. multiflorus is readily distinguished from that species and all other Calopogon species by its features. The biology of C. multiflorus has been discussed in detail by D. H. Goldman and S. L. Orzell (2000).</discussion>
  
</bio:treatment>