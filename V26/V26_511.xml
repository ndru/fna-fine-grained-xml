<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 234. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101370</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="A. Nelson &amp; J. F. Macbride" date="unknown" rank="species">incisum</taxon_name>
    <taxon_hierarchy>genus Allium;species incisum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="A. Nelson &amp; J. F. Macbride" date="unknown" rank="species">scissum</taxon_name>
    <taxon_hierarchy>genus Allium;species scissum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–5+, not clustered on stout primary rhizome, ovoid, 1.5–2.2 × 1–2 cm;</text>
      <biological_entity id="o28891" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="5" upper_restricted="false" />
        <character constraint="on primary rhizome" constraintid="o28892" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s0" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o28892" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, brown, membranous, ± prominently cellular-reticulate, cells in ± regular vertical rows, narrowly rectangular, transversely elongate, without fibers;</text>
      <biological_entity constraint="outer" id="o28893" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s1" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o28894" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" modifier="more or less prominently" name="architecture_or_coloration_or_relief" src="d0_s1" value="cellular-reticulate" value_original="cellular-reticulate" />
      </biological_entity>
      <biological_entity id="o28895" name="row" name_original="rows" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s1" value="regular" value_original="regular" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o28896" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o28893" id="r3898" name="enclosing" negation="false" src="d0_s1" to="o28894" />
      <relation from="o28893" id="r3899" name="in" negation="false" src="d0_s1" to="o28895" />
      <relation from="o28893" id="r3900" name="without" negation="false" src="d0_s1" to="o28896" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white to light-brown, cells ± narrowly rectangular, transversely elongate, or quadrate.</text>
      <biological_entity constraint="inner" id="o28897" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s2" to="light-brown" />
      </biological_entity>
      <biological_entity id="o28898" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less narrowly" name="shape" src="d0_s2" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually deciduous with scape, green or withering only at tip at anthesis, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o28899" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with scape" constraintid="o28900" is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="green" value_original="green" />
        <character constraint="at tip" constraintid="o28901" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o28900" name="scape" name_original="scape" src="d0_s3" type="structure" />
      <biological_entity id="o28901" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character modifier="at anthesis" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28902" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o28903" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o28904" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o28902" id="r3901" name="extending" negation="false" src="d0_s3" to="o28903" />
      <relation from="o28902" id="r3902" name="extending" negation="false" src="d0_s3" to="o28904" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, falcate, 8–30 cm × 3–5 mm, margins entire.</text>
      <biological_entity id="o28905" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28906" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape usually forming abcission layer and deciduous with leaves after seeds mature, frequently breaking at this level after pressing, solitary, erect, solid, flattened, narrowly winged, 15–20 cm × 1–4 mm.</text>
      <biological_entity id="o28907" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character constraint="with leaves" constraintid="o28910" is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="frequently" name="architecture_or_arrangement_or_growth_form" notes="" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s5" value="winged" value_original="winged" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28908" name="abcission" name_original="abcission" src="d0_s5" type="structure" />
      <biological_entity id="o28909" name="layer" name_original="layer" src="d0_s5" type="structure" />
      <biological_entity id="o28910" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o28911" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o28912" name="pressing" name_original="pressing" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s5" value="level" value_original="level" />
      </biological_entity>
      <relation from="o28907" id="r3903" name="forming" negation="false" src="d0_s5" to="o28908" />
      <relation from="o28907" id="r3904" name="forming" negation="false" src="d0_s5" to="o28909" />
      <relation from="o28910" id="r3905" name="after" negation="false" src="d0_s5" to="o28911" />
      <relation from="o28907" id="r3906" name="breaking at" negation="false" src="d0_s5" to="o28912" />
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact to ± loose, 10–40-flowered, hemispheric, bulbils unknown;</text>
      <biological_entity id="o28913" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="compact" name="architecture" src="d0_s6" to="more or less loose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-40-flowered" value_original="10-40-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o28914" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, 8–10-veined, broadly lanceolate to ovate, ± equal, apex long-acuminate.</text>
      <biological_entity constraint="spathe" id="o28915" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="8-10-veined" value_original="8-10-veined" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o28916" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers campanulate, 6–9 mm;</text>
      <biological_entity id="o28917" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, pink to whitish, lanceovate, ± equal, becoming ± rigid in fruit, margins entire, apex acute to acuminate;</text>
      <biological_entity id="o28918" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="whitish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o28919" is_modifier="false" modifier="becoming more or less" name="texture" src="d0_s9" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o28919" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o28920" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28921" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens ± equaling tepals;</text>
      <biological_entity id="o28922" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o28923" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers light purple to yellow;</text>
      <biological_entity id="o28924" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="light purple" name="coloration" src="d0_s11" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o28925" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary obscurely crested;</text>
      <biological_entity id="o28926" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, low, central, 2 per lobe, margins entire;</text>
      <biological_entity id="o28927" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="position" src="d0_s14" value="central" value_original="central" />
        <character constraint="per lobe" constraintid="o28928" name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28928" name="lobe" name_original="lobe" src="d0_s14" type="structure" />
      <biological_entity id="o28929" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style linear, equaling stamens;</text>
      <biological_entity id="o28930" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o28931" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, scarcely thickened, unlobed;</text>
      <biological_entity id="o28932" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 8–16 mm.</text>
      <biological_entity id="o28933" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat dull;</text>
      <biological_entity id="o28934" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o28935" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28936" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Drying, clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay soils" modifier="drying" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>84.</number>
  
</bio:treatment>