<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">nolina</taxon_name>
    <taxon_name authority="Correll" date="1968" rank="species">arenicola</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>19: 187. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus nolina;species arenicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101794</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent, cespitose;</text>
      <biological_entity id="o19610" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes from vertical, subterranean, branched caudices.</text>
      <biological_entity id="o19611" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity id="o19612" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="true" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o19611" id="r2689" name="from" negation="false" src="d0_s1" to="o19612" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades wiry, carinate, flat or concavo-convex, 100–200 cm × 5–10 mm, not glaucous;</text>
      <biological_entity id="o19613" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="shape" src="d0_s2" value="carinate" value_original="carinate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concavo-convex" value_original="concavo-convex" />
        <character char_type="range_value" from="100" from_unit="cm" name="length" src="d0_s2" to="200" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins remotely serrulate, with widely separated, noncartilaginous teeth, to entire;</text>
      <biological_entity id="o19614" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19615" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="widely" name="arrangement" src="d0_s3" value="separated" value_original="separated" />
        <character is_modifier="true" name="texture" src="d0_s3" value="noncartilaginous" value_original="noncartilaginous" />
      </biological_entity>
      <relation from="o19614" id="r2690" name="with" negation="false" src="d0_s3" to="o19615" />
    </statement>
    <statement id="d0_s4">
      <text>apex lacerate.</text>
      <biological_entity id="o19616" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape 1–3 dm.</text>
      <biological_entity id="o19617" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s5" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences paniculate, rarely purple, 4–7 dm × 12–18 cm;</text>
      <biological_entity id="o19618" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s6" to="7" to_unit="dm" />
        <character char_type="range_value" from="12" from_unit="cm" name="width" src="d0_s6" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>main rachis and divisions thick, rigid, lateral branches spreading;</text>
      <biological_entity constraint="main" id="o19619" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity constraint="main" id="o19620" name="division" name_original="divisions" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19621" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, to 45 cm, apices curling;</text>
      <biological_entity id="o19622" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="45" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19623" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="curling" value_original="curling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bractlets slightly erose, 1.5–5 mm, apex aristate.</text>
      <biological_entity id="o19624" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19625" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals 2–2.8 mm;</text>
      <biological_entity id="o19626" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19627" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>infertile stamens: filaments to 1 mm, anthers to 0.4 mm;</text>
      <biological_entity id="o19628" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="infertile" value_original="infertile" />
      </biological_entity>
      <biological_entity id="o19629" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19630" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>fruiting pedicel erect, thick, articulate near base, noticeably dilated into perianth, proximal to joint 2–3.5 mm, distal to joint to 1.5 mm.</text>
      <biological_entity id="o19631" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="infertile" value_original="infertile" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="width" src="d0_s12" value="thick" value_original="thick" />
        <character constraint="near base" constraintid="o19633" is_modifier="false" name="architecture" src="d0_s12" value="articulate" value_original="articulate" />
        <character constraint="into perianth" constraintid="o19634" is_modifier="false" modifier="noticeably" name="shape" notes="" src="d0_s12" value="dilated" value_original="dilated" />
        <character constraint="to joint" constraintid="o19635" is_modifier="false" name="position" notes="" src="d0_s12" value="proximal" value_original="proximal" />
        <character constraint="to joint" constraintid="o19636" is_modifier="false" name="position_or_shape" notes="" src="d0_s12" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o19632" name="pedicel" name_original="pedicel" src="d0_s12" type="structure" />
      <biological_entity id="o19633" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o19634" name="perianth" name_original="perianth" src="d0_s12" type="structure" />
      <biological_entity id="o19635" name="joint" name_original="joint" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19636" name="joint" name_original="joint" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o19631" id="r2691" name="fruiting" negation="false" src="d0_s12" to="o19632" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules firm-walled, inflated, 2.5–3.3 × 5–6.2 mm, broadly rounded distally.</text>
      <biological_entity id="o19637" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s13" to="6.2" to_unit="mm" />
        <character is_modifier="false" modifier="broadly; distally" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds closely invested in capsules, bursting ovary walls, 3 × 4.3 mm diam.</text>
      <biological_entity id="o19638" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="diameter" src="d0_s14" unit="mm" value="3" value_original="3" />
        <character name="diameter" src="d0_s14" unit="mm" value="4.3" value_original="4.3" />
      </biological_entity>
      <biological_entity id="o19639" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
      <biological_entity constraint="ovary" id="o19640" name="wall" name_original="walls" src="d0_s14" type="structure" />
      <relation from="o19638" id="r2692" name="invested in" negation="false" src="d0_s14" to="o19639" />
      <relation from="o19638" id="r2693" name="bursting" negation="false" src="d0_s14" to="o19640" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy soils or dunes in brushlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" modifier="open" constraint="in brushlands" />
        <character name="habitat" value="dunes" constraint="in brushlands" />
        <character name="habitat" value="brushlands" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Sand sacahuista</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Nolina arenicola is endemic to the trans-Pecos region of Texas and is listed as threatened and endangered by the U.S. Fish and Wildlife Service. Nolina arenicola is similar to N. texana except that in N. arenicola the clumps are much more robust, with wider leaves, serrulate near the point of attachment, and are restricted to sandy areas.</discussion>
  
</bio:treatment>