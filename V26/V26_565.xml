<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">leucojum</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">aestivum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 975. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus leucojum;species aestivum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200028052</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 30–60 cm;</text>
      <biological_entity id="o35040" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb 2–4 × 2–3 (–4) cm.</text>
      <biological_entity id="o35041" name="bulb" name_original="bulb" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheaths 3–6 mm, membranous;</text>
      <biological_entity id="o35042" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o35043" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade deep green, linear to ligulate, 30–50 (–70) × 0.5–2 cm, apex obtuse, glossy, well developed at anthesis.</text>
      <biological_entity id="o35044" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o35045" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="depth" src="d0_s3" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="70" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o35046" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="glossy" value_original="glossy" />
        <character constraint="at anthesis" is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Scape stout, hollow, 2-winged, compressed, (20–) 30–60 cm.</text>
      <biological_entity id="o35047" name="scape" name_original="scape" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-winged" value_original="2-winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s4" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spathe bract solitary, fused, green, lanceolate, 3–5 cm.</text>
      <biological_entity constraint="spathe" id="o35048" name="bract" name_original="bract" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="fused" value_original="fused" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals white with yellowish green spot near apex, broadly ovate, (10–) 13–22 × 10–12 mm;</text>
      <biological_entity id="o35049" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o35050" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character constraint="with spot" constraintid="o35051" is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s6" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35051" name="spot" name_original="spot" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity id="o35052" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o35051" id="r4696" name="near" negation="false" src="d0_s6" to="o35052" />
    </statement>
    <statement id="d0_s7">
      <text>filaments white, slender, 2–5 mm;</text>
      <biological_entity id="o35053" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o35054" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 5–6 mm;</text>
      <biological_entity id="o35055" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o35056" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary oblong-ovate, 6–10 × 5–8 mm;</text>
      <biological_entity id="o35057" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35058" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style clavate, 5–10 mm, exceeding stamens;</text>
      <biological_entity id="o35059" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o35060" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35061" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <relation from="o35060" id="r4697" name="exceeding" negation="false" src="d0_s10" to="o35061" />
    </statement>
    <statement id="d0_s11">
      <text>stigma truncate, inconspicuous;</text>
      <biological_entity id="o35062" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o35063" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 2.5–7 cm, longest equaling or exceeding spathe.</text>
      <biological_entity id="o35064" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o35065" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s12" to="7" to_unit="cm" />
        <character is_modifier="false" name="length" src="d0_s12" value="longest" value_original="longest" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s12" value="exceeding spathe" value_original="exceeding spathe" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds black, 5–7 mm;</text>
      <biological_entity id="o35066" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>coat loose, with air pockets at each end.</text>
      <biological_entity id="o35068" name="air" name_original="air" src="d0_s14" type="structure" />
      <biological_entity id="o35069" name="pocket" name_original="pockets" src="d0_s14" type="structure" />
      <biological_entity id="o35070" name="end" name_original="end" src="d0_s14" type="structure" />
      <relation from="o35067" id="r4698" name="with" negation="false" src="d0_s14" to="o35068" />
      <relation from="o35067" id="r4699" name="with" negation="false" src="d0_s14" to="o35069" />
      <relation from="o35068" id="r4700" name="at" negation="false" src="d0_s14" to="o35070" />
      <relation from="o35069" id="r4701" name="at" negation="false" src="d0_s14" to="o35070" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 22, 24.</text>
      <biological_entity id="o35067" name="coat" name_original="coat" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s14" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35071" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="22" value_original="22" />
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.S.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Miss., N.Y., N.C., Ohio, Pa., S.C., Tenn., Tex., Va.; c, s Europe; naturalized throughout temperate areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="naturalized throughout temperate areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Summer snowflake</other_name>
  
</bio:treatment>