<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bryan Ness</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="treatment_page">164</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">FRITILLARIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 303. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 144. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus FRITILLARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, fritillus, checkered, alluding to the markings on the tepals of many species</other_info_on_name>
    <other_info_on_name type="fna_id">113029</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, bulbose;</text>
      <biological_entity id="o16708" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulbs with 1–several large fleshy scales and 0–many small scales (often called rice-grain bulblets).</text>
      <biological_entity id="o16709" name="bulb" name_original="bulbs" src="d0_s1" type="structure" />
      <biological_entity id="o16710" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o16711" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s1" to="many" />
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o16709" id="r2286" name="with" negation="false" src="d0_s1" to="o16710" />
      <relation from="o16709" id="r2287" name="with" negation="false" src="d0_s1" to="o16711" />
    </statement>
    <statement id="d0_s2">
      <text>Stem 1, erect, simple, absent in nonflowering individuals.</text>
      <biological_entity id="o16712" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="in nonflowering individuals" constraintid="o16713" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="nonflowering" id="o16713" name="individual" name_original="individuals" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate or whorled proximally in some species, sessile;</text>
      <biological_entity id="o16714" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character constraint="in species" constraintid="o16715" is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o16715" name="species" name_original="species" src="d0_s3" type="taxon_name" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to ± ovate;</text>
      <biological_entity id="o16716" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="more or less ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nonflowering individuals with single elliptical, ovate, or obovate “bulb-leaf.” Inflorescences loosely racemose, bracteate;</text>
      <biological_entity constraint="nonflowering" id="o16717" name="individual" name_original="individuals" src="d0_s5" type="structure" />
      <biological_entity id="o16718" name="bulb-leaf" name_original="bulb-leaf" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s5" value="elliptical" value_original="elliptical" />
        <character is_modifier="true" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="true" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o16719" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <relation from="o16717" id="r2288" name="with" negation="false" src="d0_s5" to="o16718" />
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike.</text>
      <biological_entity id="o16720" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1–many, usually nodding, 3-merous;</text>
      <biological_entity id="o16721" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s7" to="many" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-merous" value_original="3-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth hypogynous, campanulate or cupulate;</text>
      <biological_entity id="o16722" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cupulate" value_original="cupulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals 6, in 2 similar whorls, distinct, nectaries present on all tepals, but better developed on inner ones;</text>
      <biological_entity id="o16723" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o16724" name="whorl" name_original="whorls" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16725" name="nectary" name_original="nectaries" src="d0_s9" type="structure" />
      <biological_entity id="o16726" name="tepal" name_original="tepals" src="d0_s9" type="structure" />
      <biological_entity constraint="inner" id="o16727" name="tepal" name_original="tepals" src="d0_s9" type="structure" />
      <relation from="o16723" id="r2289" name="in" negation="false" src="d0_s9" to="o16724" />
      <relation from="o16725" id="r2290" name="present on all" negation="false" src="d0_s9" to="o16726" />
      <relation from="o16725" id="r2291" name="developed on" negation="false" src="d0_s9" to="o16727" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, included;</text>
      <biological_entity id="o16728" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers adnate to filaments near middle;</text>
      <biological_entity id="o16729" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character constraint="to filaments" constraintid="o16730" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o16730" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <biological_entity id="o16731" name="middle" name_original="middle" src="d0_s11" type="structure" />
      <relation from="o16730" id="r2292" name="near" negation="false" src="d0_s11" to="o16731" />
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, ± sessile;</text>
      <biological_entity id="o16732" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style unbranched or 3-branched.</text>
      <biological_entity id="o16733" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-branched" value_original="3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsular, 3-locular, 6-angled or winged, thin-walled, ± rounded, dehiscence loculicidal.</text>
      <biological_entity id="o16734" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="6-angled" value_original="6-angled" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds many, in 2 rows per locule, yellowish to brownish, flat.</text>
      <biological_entity id="o16736" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16737" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <relation from="o16735" id="r2293" name="in" negation="false" src="d0_s15" to="o16736" />
      <relation from="o16736" id="r2294" name="per" negation="false" src="d0_s15" to="o16737" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12, 13, often with chromosome fragments.</text>
      <biological_entity id="o16735" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="many" value_original="many" />
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s15" to="brownish" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s15" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="x" id="o16738" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit=",,often" value="12" value_original="12" />
        <character name="quantity" src="d0_s16" unit=",,often" value="13" value_original="13" />
      </biological_entity>
      <biological_entity constraint="chromosome" id="o16739" name="fragment" name_original="fragments" src="d0_s16" type="structure" />
      <relation from="o16735" id="r2295" name="with" negation="false" src="d0_s16" to="o16739" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Northern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Northern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Fritillary</other_name>
  <discussion>Species ca. 100 (20 in the flora).</discussion>
  <discussion>A number of Fritillaria species are grown as ornamentals, although North American species can be difficult to grow and many have not been tried. Most require well-drained soil, full sun, and no summer watering. Native Americans used the bulbs of various species as food, typically roasted, sometimes dried for later use.</discussion>
  <references>
    <reference>Beck, C. H. 1951. Fritillaries: A Gardener’s Introduction to the Genus Fritillaria. London.  </reference>
    <reference>Beetle, D. F. 1944. A monograph of the North American species of Fritillaria. Madroño 7: 133–159.  </reference>
    <reference>Farrens, B. M. 1947. A Taxonomic Study of the North American Species of Fritillaria. M.A. thesis. Stanford University.  </reference>
    <reference>Marchant, C. J. 1981. Fritillaria in British Columbia. Davidsonia 12: 19–25.  </reference>
    <reference>Rix, E. M. and D. Rast. 1975. Nectar sugars and subgeneric classification in Fritillaria. Biochem. Syst. &amp; Ecol. 2: 207–209.  </reference>
    <reference>Santana, D. O. 1984. Morphological and Anatomical Observations on the Bulbs and Tepals of Fritillaria (Liliaceae) Section Liliorhiza (Kellogg) Watson, Their Taxonomic Implications with a Synopsis and the Reproductive Biology of the Section. Ph.D. dissertation. University of California, Davis.  </reference>
    <reference>Stapf, O. 1934. Lilium, Notholirion and Fritillaria. Bull. Misc. Inform. Kew 1934: 94–96.  </reference>
    <reference>Turner, N. J. and H. V. Kuhnlein. 1983. Camas (Camassia spp.) and riceroot (Fritillaria spp.): Two liliaceous “root” foods of the Northwest Coast Indians. Ecol. Food Nutr. 13: 199–219.  </reference>
    <reference>Turrill, W. B. 1950. Character combinations and distribution in the genus Fritillaria and allied genera. Evolution 4: 1–6.  </reference>
    <reference>Turrill, W. B. and J. R. Sealy. 1980. Studies in the genus Fritillaria (Liliaceae). Hooker’s Icon. Pl. 39.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Style unbranched or rarely with branches shorter than 1.5 mm; tepals never scarlet.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Style obviously branched, branches longer than 1.5 mm; tepals sometimes scarlet.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals shorter than 2 cm or, if longer, then yellow to orange.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals 2 cm or longer, white to pink or pinkish purple.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves whorled proximally; tepals pinkish to purplish; s California.</description>
      <determination>5 Fritillaria brandegeei</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves subopposite to scattered; tepals yellow to orange; n California to British Columbia, e to Wyoming.</description>
      <determination>16 Fritillaria pudica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Tepals pinkish purple, obovate, apex rounded to acute, not recurved; flowers not noticeably fragrant; n California.</description>
      <determination>15 Fritillaria pluriflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Tepals white to pink, oblanceolate, apex acute to apiculate, usually recurved; flowers fragrant; s California.</description>
      <determination>19 Fritillaria striata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tepals red or scarlet, sometimes to maroon or purplish, clearly checkered or mottled, apex usually recurved, sometimes only spreading.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tepals sometimes mottled or rarely scarlet, but never both, apex not recurved, only rarely slightly recurved.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perianth slender; tepal apex usually strongly recurved, nectaries ¼ tepal length or less; style branches ± erect; n California, s Oregon, w Nevada.</description>
      <determination>18 Fritillaria recurva</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perianth broadly campanulate; tepal apex spreading, not recurved, nectaries 1/2 tepal length; style branches widely spreading; s Oregon.</description>
      <determination>9 Fritillaria gentneri</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves more than 10 or in whorls of 2–6(–9) per node proximally, blade linear to narrowly to broadly lanceolate to rarely ovate.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves 10 or fewer and/or alternate, blade sometimes sickle-shaped.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Flowers ± erect, occasionally nodding; distal leaves usually ca. 1/3–1/2 length of proximalmost leaf; leaves often longer than inflorescence; only in California.</description>
      <determination>14 Fritillaria pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Flowers nodding or occasionally spreading; distal leaves usually ± equaling proximalmost leaf; leaves usually shorter than inflorescence; more widespread.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Tepals purplish brown, mottled yellow or white; leaves 2–3 per node proximally; 1000–3200 m, especially inland mountains.</description>
      <determination>3 Fritillaria atropurpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Tepals greenish white, greenish yellow to red, purplish, or nearly black, mottled or not; leaves usually more than 3 per node proximally; 0–1800 m, especially in coastal mountains.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Tepals dull greenish yellow, dark-dotted, nectaries widely elliptic to ± diamond-shaped, paler than tepals; s California.</description>
      <determination>13 Fritillaria ojaiensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Tepals greenish white, pale green or greenish yellow to red, purplish, or almost black, sometimes mottled, nectaries lanceolate or linear, variously colored; s California to Alaska.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Tepals pale green to almost black, not mottled, nectaries ca. 1/2 tepal length, green; small bulb scales 0–4; s California.</description>
      <determination>20 Fritillaria viridea</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Tepals greenish white, pale greenish yellow to red, greenish brown to purplish, or pale yellowish green, sometimes mottled, nectaries variously colored, less than 1/2 tepal length (nearly equaling tepal length in F. camschatcensis, but very narrow and obscure; to 2/3 tepal length in F. affinis); small bulb scales usually 10 or more, rarely fewer; c California to Alaska.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Tepals usually 2 cm or longer, often clearly purple- or yellow-mottled.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Tepals usually shorter than 2 cm, mottling absent or faint.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Tepals clearly purple- or yellow-mottled and small bulb scales 20 or fewer, or tepals not mottled and bulb scales 50 or more; nectaries lanceolate, to 2/3 tepal length; flower odor not unpleasant.</description>
      <determination>1 Fritillaria affinis</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Tepals dark greenish brown to brownish purple; small bulb scales 30 or more; nectaries obscure, linear, ± equaling tepal length; flower odor unpleasant.</description>
      <determination>6 Fritillaria camschatcensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Style branches barely recurved; tepals greenish yellow to red, apex usually flared to slightly recurved, nectaries green, gold, or yellow.</description>
      <determination>7 Fritillaria eastwoodiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Style branches strongly recurved; tepals purplish to greenish white, apex not flared or recurved, nectaries greenish white, dotted purple.</description>
      <determination>12 Fritillaria micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Tepals clearly mottled; small bulb scales usually 10 or more, rarely fewer.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Tepals not clearly mottled; small bulb scales fewer than 10.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blade sickle-shaped; flowers erect; San Francisco Bay region and s coastal mountains of California.</description>
      <determination>8 Fritillaria falcata</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blade usually not sickle-shaped; flowers usually horizontal or nodding, sometimes ± erect; not in San Francisco Bay region and s coastal mountains of California.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves 2–10, blade ovate, a few sometimes sickle-shaped; tepals white, spotted purple; small bulb scales 0–3.</description>
      <determination>17 Fritillaria purdyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves usually 4 or more, blade linear to lanceolate; tepals purplish brown, mottled greenish yellow, yellow, or white; small bulb scales 45–50.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Flowers nodding; distal leaves slightly less than or equaling proximalmost leaf, leaves usually shorter than inflorescence.</description>
      <determination>3 Fritillaria atropurpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Flowers ± erect; distal leaves usually 1/3–1/2 length of proximalmost leaf; leaves longer than inflorescence.</description>
      <determination>14 Fritillaria pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves 2–4, blade sickle-shaped.</description>
      <determination>10 Fritillaria glauca</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves usually more than 4, blade not sickle-shaped.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Nectaries obscure, forming narrow band 1/2–2/3 tepal length; tepals white, striped green; flowers odorless or faintly fragrant; coastal.</description>
      <determination>11 Fritillaria liliacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Nectaries prominent, forming narrow band 2/3 to equaling tepal length; tepals brown, purplish brown, or greenish purple at least adaxially; flowers odorless or with unpleasant odor; inland.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Tepals greenish white or yellow abaxially, purplish brown adaxially; flower odor definitely unpleasant; usually in clay depressions.</description>
      <determination>2 Fritillaria agrestis</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Tepals dark brown to greenish purple or yellowish green; flowers odorless or sometimes with unpleasant odor; usually on hillsides and mesas.</description>
      <determination>4 Fritillaria biflora</determination>
    </key_statement>
  </key>
</bio:treatment>