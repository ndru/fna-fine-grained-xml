<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">527</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="treatment_page">529</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">sacoila</taxon_name>
    <taxon_name authority="(Aublet) Garay" date="1982" rank="species">lanceolata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">lanceolata</taxon_name>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus sacoila;species lanceolata;variety lanceolata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242102291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neottia</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">aphylla</taxon_name>
    <taxon_hierarchy>genus Neottia;species aphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Satyrium</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">orchioides</taxon_name>
    <taxon_hierarchy>genus Satyrium;species orchioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">jaliscana</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species jaliscana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="(Swartz) A. Richard" date="unknown" rank="species">orchioides</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species orchioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves usually absent at flowering time.</text>
      <biological_entity id="o23749" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at time" constraintid="o23750" is_modifier="false" modifier="usually" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23750" name="time" name_original="time" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences 4–18 cm;</text>
      <biological_entity id="o23751" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>floral bracts green to reddish-brown, lanceolate, 15–20 (–25) mm.</text>
      <biological_entity constraint="floral" id="o23752" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 9–50, coral to orange-red;</text>
      <biological_entity id="o23753" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s3" to="50" />
        <character char_type="range_value" from="coral" name="coloration" src="d0_s3" to="orange-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>dorsal sepal 19–23 × 7–9 mm;</text>
      <biological_entity constraint="dorsal" id="o23754" name="sepal" name_original="sepal" src="d0_s4" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s4" to="23" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral sepals 23–31 × 3–8 mm, connate for 5–7 mm over basal portion of mentum, free portion of mentum 4–6 mm;</text>
      <biological_entity constraint="lateral" id="o23755" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s5" to="31" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character constraint="over basal portion" constraintid="o23756" is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o23756" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o23757" name="mentum" name_original="mentum" src="d0_s5" type="structure" />
      <biological_entity id="o23758" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="free" value_original="free" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23759" name="mentum" name_original="mentum" src="d0_s5" type="structure" />
      <relation from="o23756" id="r3223" name="part_of" negation="false" src="d0_s5" to="o23757" />
      <relation from="o23758" id="r3224" name="part_of" negation="false" src="d0_s5" to="o23759" />
    </statement>
    <statement id="d0_s6">
      <text>petals 21–26 × 6–8 mm;</text>
      <biological_entity id="o23760" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s6" to="26" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lip pale-red or pinkish white to white, 23–31 × 8–11 mm;</text>
      <biological_entity id="o23761" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character char_type="range_value" from="pinkish white" name="coloration" src="d0_s7" to="white" />
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s7" to="31" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>column 9–10 mm from attachment of dorsal sepal to tip, foot extending back 11–12 mm;</text>
      <biological_entity id="o23762" name="column" name_original="column" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from attachment" constraintid="o23763" from="9" from_unit="mm" name="location" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23763" name="attachment" name_original="attachment" src="d0_s8" type="structure" />
      <biological_entity constraint="dorsal" id="o23764" name="sepal" name_original="sepal" src="d0_s8" type="structure" />
      <biological_entity id="o23765" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o23766" name="foot" name_original="foot" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23767" name="back" name_original="back" src="d0_s8" type="structure" />
      <relation from="o23763" id="r3225" name="part_of" negation="false" src="d0_s8" to="o23764" />
      <relation from="o23763" id="r3226" name="to" negation="false" src="d0_s8" to="o23765" />
      <relation from="o23766" id="r3227" name="extending" negation="false" src="d0_s8" to="o23767" />
    </statement>
    <statement id="d0_s9">
      <text>pollinaria 8–9 mm;</text>
      <biological_entity id="o23768" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>viscidia 4–5 mm;</text>
      <biological_entity id="o23769" name="viscidium" name_original="viscidia" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicellate ovary 10–20 mm;</text>
      <biological_entity id="o23770" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rostellum 4–5 mm.</text>
      <biological_entity id="o23771" name="rostellum" name_original="rostellum" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 14 × 8 mm.</text>
      <biological_entity id="o23772" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character name="length" src="d0_s13" unit="mm" value="14" value_original="14" />
        <character name="width" src="d0_s13" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds largely polyembryonic.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 46.</text>
      <biological_entity id="o23773" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="largely" name="architecture" src="d0_s14" value="polyembryonic" value_original="polyembryonic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23774" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Mar–early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, dry to moist habitats including poorly drained pinelands, roadside ditches, pastures, open woods in sandy, ± acid substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist habitats" />
        <character name="habitat" value="drained pinelands" modifier="poorly" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="open" />
        <character name="habitat" value="dry to moist habitats including drained pinelands" modifier="poorly" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="open woods" constraint="in sandy" />
        <character name="habitat" value="acid substrates" modifier="sandy" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <other_name type="common_name">Leafless beaked orchid</other_name>
  
</bio:treatment>