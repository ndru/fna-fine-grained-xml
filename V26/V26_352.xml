<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">convallaria</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">majalis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 314. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus convallaria;species majalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200027600</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–3 dm.</text>
      <biological_entity id="o10658" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 15–50 × 2.5–13 cm;</text>
      <biological_entity id="o10659" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s1" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 4–30 cm;</text>
      <biological_entity id="o10660" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 9–26 cm, strongly or weakly veined.</text>
      <biological_entity id="o10661" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s3" to="26" to_unit="cm" />
        <character is_modifier="false" modifier="strongly; weakly" name="architecture" src="d0_s3" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescence bracts scarious, 4–20 mm.</text>
      <biological_entity constraint="inflorescence" id="o10662" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals 6, white, midribs prominent, white or green, lobes 1–3 mm, with microscopic glands;</text>
      <biological_entity id="o10663" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o10664" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o10665" name="midrib" name_original="midribs" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o10666" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10667" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="microscopic" value_original="microscopic" />
      </biological_entity>
      <relation from="o10666" id="r1516" name="with" negation="false" src="d0_s5" to="o10667" />
    </statement>
    <statement id="d0_s6">
      <text>pedicel 7–11 mm.</text>
      <biological_entity id="o10668" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10669" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Berries 1–9, 1–5-seeded, 6–12 mm.</text>
      <biological_entity id="o10670" name="berry" name_original="berries" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-5-seeded" value_original="1-5-seeded" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 3–4 mm. 2n = 38.</text>
      <biological_entity id="o10671" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10672" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Deciduous forests of e North America, w Europe, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Deciduous forests of e North America" establishment_means="native" />
        <character name="distribution" value="w Europe" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 3 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants forming dense colonies; leaf blades green until frost; bracts 4–10 mm, shorter than pedicel; tepal midribs white.</description>
      <determination>1a Convallaria majalis var. majalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants scattered or forming small groups; leaf blades yellowing in late summer; bracts 10–20 mm, equaling or longer than pedicel; tepal midribs green.</description>
      <determination>1b Convallaria majalis var. montana</determination>
    </key_statement>
  </key>
</bio:treatment>