<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul M. Catling,Lawrence K. Magrath</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="treatment_page">627</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Malaxideae</taxon_name>
    <taxon_name authority="Solander ex Swartz" date="1788" rank="genus">MALAXIS</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>8, 119. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe malaxideae;genus malaxis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek malaxis, softening, in reference to soft and tender texture of leaves</other_info_on_name>
    <other_info_on_name type="fna_id">119529</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, terrestrial to semiepiphytic, glabrous.</text>
      <biological_entity id="o18690" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots few, fibrous, 0.3–1 mm wide.</text>
      <biological_entity id="o18691" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems swollen at base into pseudobulb, ± globose, glabrous.</text>
      <biological_entity id="o18692" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o18693" is_modifier="false" name="shape" src="d0_s2" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s2" value="globose" value_original="globose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18693" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o18694" name="pseudobulb" name_original="pseudobulb" src="d0_s2" type="structure" />
      <relation from="o18693" id="r2585" name="into" negation="false" src="d0_s2" to="o18694" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves 1–3 (–5), with sheathing base;</text>
      <biological_entity id="o18695" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o18696" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o18695" id="r2586" name="with" negation="false" src="d0_s3" to="o18696" />
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic, ovate, or lanceolate.</text>
      <biological_entity id="o18697" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemes, spicate racemes, corymbose racemes, or subumbellate racemes;</text>
      <biological_entity id="o18698" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o18702" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="corymbose" value_original="corymbose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bracts inconspicuous, lanceolate, subulate, or triangular-acuminate.</text>
      <biological_entity constraint="floral" id="o18703" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular-acuminate" value_original="triangular-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular-acuminate" value_original="triangular-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2–160, resupinate or not, erect or spreading, sessile or minutely to strongly pedicellate;</text>
      <biological_entity id="o18704" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="160" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
        <character name="orientation" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="sessile or" name="architecture" src="d0_s7" to="minutely strongly pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals spreading, distinct or lateral sepals basally connate, ovate, elliptic, or lanceolate, 1–6 mm, margins revolute or not;</text>
      <biological_entity id="o18705" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18706" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18707" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="revolute" value_original="revolute" />
        <character name="shape_or_vernation" src="d0_s8" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals spreading or recurved, filiform to linear, lanceolate, or triangular, usually much narrower than sepals;</text>
      <biological_entity id="o18708" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s9" to="linear lanceolate or triangular" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s9" to="linear lanceolate or triangular" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s9" to="linear lanceolate or triangular" />
        <character constraint="than sepals" constraintid="o18709" is_modifier="false" name="width" src="d0_s9" value="usually much narrower" value_original="usually much narrower" />
      </biological_entity>
      <biological_entity id="o18709" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lip ovate or lanceolate, cordate, unlobed to 3-lobed, concave or saccate, widest proximal to middle, base auriculate or truncate;</text>
      <biological_entity id="o18710" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate cordate unlobed" name="shape" src="d0_s10" to="3-lobed concave or saccate" />
        <character char_type="range_value" from="lanceolate cordate unlobed" name="shape" src="d0_s10" to="3-lobed concave or saccate" />
        <character char_type="range_value" from="lanceolate cordate unlobed" name="shape" src="d0_s10" to="3-lobed concave or saccate" />
        <character char_type="range_value" from="lanceolate cordate unlobed" name="shape" src="d0_s10" to="3-lobed concave or saccate" />
        <character is_modifier="false" name="position" src="d0_s10" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s10" to="middle" />
      </biological_entity>
      <biological_entity id="o18711" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>column free;</text>
      <biological_entity id="o18712" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther terminal;</text>
      <biological_entity id="o18713" name="anther" name_original="anther" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollinaria 4, waxy;</text>
    </statement>
    <statement id="d0_s14">
      <text>1 pollinarium or 2 separate hemipollinaria;</text>
      <biological_entity id="o18714" name="pollinarium" name_original="pollinaria" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
        <character is_modifier="false" name="texture" src="d0_s13" value="ceraceous" value_original="waxy" />
        <character name="quantity" src="d0_s14" unit="pollinarium or  separate hemipollinaria" value="1" value_original="1" />
        <character name="quantity" src="d0_s14" unit="pollinarium or  separate hemipollinaria" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>viscidia yellow or orange.</text>
      <biological_entity id="o18715" name="viscidium" name_original="viscidia" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules;</text>
    </statement>
    <statement id="d0_s17">
      <text>previous year’s fruiting stem and capsules frequently present during current-year’s anthesis.</text>
      <biological_entity id="o18717" name="stem" name_original="stem" src="d0_s17" type="structure">
        <character constraint="during current-year ’" constraintid="o18719" is_modifier="false" modifier="frequently" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18718" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character constraint="during current-year ’" constraintid="o18719" is_modifier="false" modifier="frequently" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="current-year" id="o18719" name="’" name_original="’" src="d0_s17" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s17" value="anthesis" value_original="anthesis" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 14, 15, 18, ca. 20, ca. 21, 22.</text>
      <biological_entity constraint="fruits" id="o18716" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="growth_order" src="d0_s17" value="previous" value_original="previous" />
      </biological_entity>
      <biological_entity constraint="x" id="o18720" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="14" value_original="14" />
        <character name="quantity" src="d0_s18" value="15" value_original="15" />
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
        <character name="quantity" src="d0_s18" value="21" value_original="21" />
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread, mostly in Asia and East Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread" establishment_means="native" />
        <character name="distribution" value="mostly in Asia and East Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Adder’s-mouth</other_name>
  <discussion>Species ca. 250 (10 in the flora).</discussion>
  <references>
    <reference>Ames, O. and C. Schweinfurth. 1935. Nomenclatural studies in Malaxis and Spiranthes. Bot. Mus. Leafl. 3: 113–133.  </reference>
    <reference>Catling, P. M. 1991. Systematics of Malaxis bayardii and M. unifolia. Lindleyana 6: 3–23. </reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 2–3(–5).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 1(–2, rarely).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip 2.5–2.9(–4) mm, base prominently cordate-auriculate.</description>
      <determination>1 Malaxis spicata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip 1.2–2.5 mm, base truncate or cuneate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves basal; base of lip cuneate; flowers not resupinate.</description>
      <determination>2 Malaxis paludosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves cauline; base of lip truncate, 3-lobed, lateral lobes auriculate; flowers resupinate.</description>
      <determination>3b Malaxis monophyllos var. brachypoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lip 3-dentate at apex, middle tooth smallest, sometimes so inconspicuous that apex appears 2-dentate.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lip unlobed (with or without single point) at apex.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers apparently sessile, pedicels 1.3–1.7 mm; flowers not resupinate.</description>
      <determination>4 Malaxis soulei</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers clearly pedicellate, pedicels 3.4–10(–13) mm; flowers resupinate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lip with auricles 0.6 or more times as long as distance from base of lip to apex of middle lobe.</description>
      <determination>5 Malaxis bayardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lip with auricles less than 0.6 times as long as distance from base of lip to apex of middle lobe.</description>
      <determination>6 Malaxis unifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lip base truncate with prominent, forward-directed auricles; flowers not resupinate.</description>
      <determination>3a Malaxis monophyllos var. monophyllos</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lip base cordate or hastate-auriculate; flowers resupinate.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences 1.5–3 cm; pedicels crowded.</description>
      <determination>7 Malaxis corymbosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescence 2.6–25 cm; pedicels not crowded.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sepals 4–6 mm.</description>
      <determination>9 Malaxis abieticola</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sepals 1.8–2.4 mm.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Sepals glabrous, not papillose; apex of lip broadly acuminate, auricles at base narrow and parallel.</description>
      <determination>10 Malaxis porphyrea</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Sepals papillose, not glabrous; apex of lip narrowly acuminate, auricles at base broad and diverging.</description>
      <determination>8 Malaxis wendtii</determination>
    </key_statement>
  </key>
</bio:treatment>