<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="Douglass M. Henderson" date="1976" rank="species">hitchcockii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>28: 170, fig. 4. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species hitchcockii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101907</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, green to dark olive when dry, to 5 dm, not glaucous;</text>
      <biological_entity id="o16114" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="dark olive" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes elongate.</text>
      <biological_entity id="o16115" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly simple, 2.5–4 mm wide, glabrous, margins usually entire, similar in color and texture to stem body.</text>
      <biological_entity id="o16116" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16117" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="stem" id="o16118" name="body" name_original="body" src="d0_s2" type="structure" />
      <relation from="o16117" id="r2196" name="to" negation="false" src="d0_s2" to="o16118" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o16119" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16120" name="base" name_original="bases" src="d0_s3" type="structure">
        <character constraint="in tufts" constraintid="o16121" is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o16121" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o16122" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes green, glabrous, keels entire or denticulate;</text>
      <biological_entity id="o16123" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16124" name="keel" name_original="keels" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer 23–37 mm, 6–17 mm longer than inner, tapering evenly towards apex, margins basally connate 5–6 mm;</text>
      <biological_entity constraint="outer" id="o16125" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s6" to="37" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o16126" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o16127" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16126" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <biological_entity id="o16127" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o16128" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner with keel evenly curved, hyaline margins 0.1–0.2 mm wide, apex obtuse or truncate, ending 0.1–0.3 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o16129" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o16130" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o16131" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16132" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o16133" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o16129" id="r2197" name="with" negation="false" src="d0_s7" to="o16130" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals purple to dark reddish purple, rarely bluish violet, bases darker purple;</text>
      <biological_entity id="o16134" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16135" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple to dark reddish" value_original="purple to dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="bluish violet" value_original="bluish violet" />
      </biological_entity>
      <biological_entity id="o16136" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="darker purple" value_original="darker purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple to dark reddish" value_original="purple to dark reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals 15–19 mm, apex truncate or emarginate, aristate;</text>
      <biological_entity id="o16137" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o16138" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16139" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o16140" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16141" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o16142" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16143" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <biological_entity id="o16144" name="foliage" name_original="foliage" src="d0_s11" type="structure" />
      <relation from="o16143" id="r2198" name="to" negation="false" src="d0_s11" to="o16144" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules beige to light-brown, globose, 6–7 mm;</text>
      <biological_entity id="o16145" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="beige" name="coloration" src="d0_s12" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicel ascending.</text>
      <biological_entity id="o16146" name="pedicel" name_original="pedicel" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds globose to obconic, lacking obvious depression, 1.9–2.5 mm, rugulose.</text>
      <biological_entity id="o16147" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 64.</text>
      <biological_entity id="o16148" name="depression" name_original="depression" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16149" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas, openings in woods, mostly where somewhat dry later in season</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="openings" constraint="in woods , mostly where somewhat dry" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="season" modifier="dry later in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sisyrinchium hitchcockii is endemic to the southern Willamette Valley and the Roseburg area of Douglas County, Oregon, and to Cape Ridge, Humboldt County, California. It often occurs in mixed populations with S. bellum. The lack of yellow tepal bases and the extensive rhizomes are the most distinctive features.</discussion>
  
</bio:treatment>