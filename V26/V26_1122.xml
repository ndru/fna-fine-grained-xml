<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak,Paul M. Catling</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1833" rank="genus">GALEARIS</taxon_name>
    <place_of_publication>
      <publication_title>Herb. Raf.,</publication_title>
      <place_in_publication>71. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus galearis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin galea, helmet</other_info_on_name>
    <other_info_on_name type="fna_id">113161</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose, rather succulent.</text>
      <biological_entity id="o10035" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots not tuberously thickened, fascicled, fleshy.</text>
      <biological_entity id="o10036" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not tuberously" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems bractless proximal to inflorescence, conspicuously angled.</text>
      <biological_entity id="o10037" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="bractless" value_original="bractless" />
        <character constraint="to inflorescence" constraintid="o10038" is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="conspicuously" name="shape" notes="" src="d0_s2" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o10038" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves [1–] 2, basal, spreading, conduplicate, gradually narrowed to sheathing petioles;</text>
      <biological_entity id="o10039" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character char_type="range_value" from="gradually narrowed" name="shape" src="d0_s3" to="sheathing" />
      </biological_entity>
      <biological_entity id="o10040" name="petiole" name_original="petioles" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade oblance-ovate to obovate, elliptic, or suborbiculate, apex rounded-obtuse.</text>
      <biological_entity id="o10041" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblance-ovate" name="shape" src="d0_s4" to="obovate elliptic or suborbiculate" />
        <character char_type="range_value" from="oblance-ovate" name="shape" src="d0_s4" to="obovate elliptic or suborbiculate" />
        <character char_type="range_value" from="oblance-ovate" name="shape" src="d0_s4" to="obovate elliptic or suborbiculate" />
      </biological_entity>
      <biological_entity id="o10042" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded-obtuse" value_original="rounded-obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, solitary, racemose spikes;</text>
      <biological_entity id="o10043" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o10044" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts large, foliaceous.</text>
      <biological_entity id="o10045" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2–15, resupinate, showy;</text>
      <biological_entity id="o10046" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="15" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip protruding downward from hood formed by sepals and enclosing petals, base produced into spur;</text>
      <biological_entity id="o10047" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="protruding" value_original="protruding" />
        <character constraint="from hood" constraintid="o10048" is_modifier="false" name="orientation" src="d0_s8" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o10048" name="hood" name_original="hood" src="d0_s8" type="structure" />
      <biological_entity id="o10049" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o10050" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity id="o10051" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o10052" name="spur" name_original="spur" src="d0_s8" type="structure" />
      <relation from="o10048" id="r1434" name="formed by" negation="false" src="d0_s8" to="o10049" />
      <relation from="o10047" id="r1435" name="enclosing" negation="false" src="d0_s8" to="o10050" />
      <relation from="o10051" id="r1436" name="produced into" negation="false" src="d0_s8" to="o10052" />
    </statement>
    <statement id="d0_s9">
      <text>pollinaria 2;</text>
      <biological_entity id="o10053" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollinia 2;</text>
      <biological_entity id="o10054" name="pollinium" name_original="pollinia" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>viscidia within single 2-lobed bursicle;</text>
      <biological_entity id="o10055" name="viscidium" name_original="viscidia" src="d0_s11" type="structure" />
      <biological_entity id="o10056" name="bursicle" name_original="bursicle" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="single" value_original="single" />
        <character is_modifier="true" name="shape" src="d0_s11" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <relation from="o10055" id="r1437" name="within" negation="false" src="d0_s11" to="o10056" />
    </statement>
    <statement id="d0_s12">
      <text>stigma concave, hidden behind bursicle.</text>
      <biological_entity id="o10057" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o10058" name="bursicle" name_original="bursicle" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="hidden" value_original="hidden" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsules, erect.</text>
      <biological_entity constraint="fruits" id="o10059" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, 1 in e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="1 in e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Showy orchis</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  
</bio:treatment>