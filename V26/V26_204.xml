<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">calochortus</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="species">aureus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Naturalist</publication_title>
      <place_in_publication>7: 303. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus calochortus;species aureus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101458</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calochortus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_name authority="(S. Watson) Ownbey" date="unknown" rank="variety">aureus</taxon_name>
    <taxon_hierarchy>genus Calochortus;species nuttallii;variety aureus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually bulbose;</text>
      <biological_entity id="o16279" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb coat, when present, membranous.</text>
      <biological_entity constraint="bulb" id="o16280" name="coat" name_original="coat" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when present" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually not branching or twisted, 1–3 dm.</text>
      <biological_entity id="o16281" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering, 1–2 dm;</text>
      <biological_entity id="o16282" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o16283" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear.</text>
      <biological_entity id="o16284" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16285" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect, subumbellate, 1–4-flowered;</text>
      <biological_entity id="o16286" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts unequal at base of inflorescence, 1–6 cm.</text>
      <biological_entity id="o16287" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="at base" constraintid="o16288" is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16288" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o16289" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
      <relation from="o16288" id="r2217" name="part_of" negation="false" src="d0_s6" to="o16289" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o16290" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth open, campanulate;</text>
      <biological_entity id="o16291" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals with maroon blotch distal to gland, broadly lanceolate, 1–4 cm, apex acuminate;</text>
      <biological_entity id="o16292" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16293" name="blotch" name_original="blotch" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="maroon" value_original="maroon" />
        <character constraint="to gland" constraintid="o16294" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o16294" name="gland" name_original="gland" src="d0_s9" type="structure" />
      <biological_entity id="o16295" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o16292" id="r2218" name="with" negation="false" src="d0_s9" to="o16293" />
    </statement>
    <statement id="d0_s10">
      <text>petals lemon yellow, with maroon blotch distal to gland, broadly obovate, cuneate, 3–3.5 cm, ± glabrous or with a few lemon-yellow hairs near gland;</text>
      <biological_entity id="o16296" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="lemon yellow" value_original="lemon yellow" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="with a few lemon-yellow hairs" />
      </biological_entity>
      <biological_entity id="o16297" name="blotch" name_original="blotch" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="maroon" value_original="maroon" />
        <character constraint="to gland" constraintid="o16298" is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o16298" name="gland" name_original="gland" src="d0_s10" type="structure" />
      <biological_entity id="o16299" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="lemon-yellow" value_original="lemon-yellow" />
      </biological_entity>
      <biological_entity id="o16300" name="gland" name_original="gland" src="d0_s10" type="structure" />
      <relation from="o16296" id="r2219" name="with" negation="false" src="d0_s10" to="o16297" />
      <relation from="o16296" id="r2220" name="with" negation="false" src="d0_s10" to="o16299" />
      <relation from="o16299" id="r2221" name="near" negation="false" src="d0_s10" to="o16300" />
    </statement>
    <statement id="d0_s11">
      <text>glands round, depressed, surrounded by conspicuously fringed membranes, densely covered with short, unbranched or distally branching hairs;</text>
      <biological_entity id="o16301" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s11" value="depressed" value_original="depressed" />
      </biological_entity>
      <biological_entity id="o16302" name="membrane" name_original="membranes" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="conspicuously" name="shape" src="d0_s11" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o16303" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
        <character is_modifier="true" modifier="distally" name="architecture" src="d0_s11" value="branching" value_original="branching" />
      </biological_entity>
      <relation from="o16301" id="r2222" name="surrounded by" negation="false" src="d0_s11" to="o16302" />
      <relation from="o16301" id="r2223" modifier="densely" name="covered with" negation="false" src="d0_s11" to="o16303" />
    </statement>
    <statement id="d0_s12">
      <text>filaments ca. equaling anthers;</text>
      <biological_entity id="o16304" name="filament" name_original="filaments" src="d0_s12" type="structure" />
      <biological_entity id="o16305" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers yellowish cream, oblong, apex obtuse.</text>
      <biological_entity id="o16306" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish cream" value_original="yellowish cream" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o16307" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, light tan to light-brown, linear-lanceoloid, 3-angled, ca. 2.5–5 cm, apex acuminate.</text>
      <biological_entity id="o16308" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character char_type="range_value" from="light tan" name="coloration" src="d0_s14" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-lanceoloid" value_original="linear-lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s14" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16309" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds light yellow, flat.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 32.</text>
      <biological_entity id="o16310" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light yellow" value_original="light yellow" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s15" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16311" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy–clayey places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey places" modifier="dry sandy" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  
</bio:treatment>