<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Dressler" date="1990" rank="subtribe">Prescottiinae</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="genus">prescottia</taxon_name>
    <taxon_name authority="(Swartz) Lindley" date="1840" rank="species">oligantha</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Orchid. Pl.,</publication_title>
      <place_in_publication>454. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe prescottiinae;genus prescottia;species oligantha;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101865</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cranichis</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">oligantha</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>120. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cranichis;species oligantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cranichis</taxon_name>
    <taxon_name authority="Sprengel" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_hierarchy>genus Cranichis;species micrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prescottia</taxon_name>
    <taxon_name authority="Schlechter" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>genus Prescottia;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prescottia</taxon_name>
    <taxon_name authority="Reichenbach ex Grisebach" date="unknown" rank="species">myosurus</taxon_name>
    <taxon_hierarchy>genus Prescottia;species myosurus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prescottia</taxon_name>
    <taxon_name authority="Schlechter" date="unknown" rank="species">panamensis</taxon_name>
    <taxon_hierarchy>genus Prescottia;species panamensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 40 cm, glabrous (except roots).</text>
      <biological_entity id="o32190" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fasciculate, fingerlike, 3–8 mm diam., fleshy.</text>
      <biological_entity id="o32191" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="fingerlike" value_original="fingerlike" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s1" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–4;</text>
      <biological_entity id="o32192" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–3 cm;</text>
      <biological_entity id="o32193" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to suborbiculate, 1.5–8 × 0.8–3 cm, base cuneate, margins entire, apex acute to rounded.</text>
      <biological_entity id="o32194" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32195" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o32196" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o32197" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences slender;</text>
      <biological_entity id="o32198" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scape 12–31 cm, partially covered by sheathing bracts;</text>
      <biological_entity id="o32199" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s6" to="31" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32200" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o32199" id="r4331" modifier="partially" name="covered by" negation="false" src="d0_s6" to="o32200" />
    </statement>
    <statement id="d0_s7">
      <text>spike to 17 cm, densely many-flowered;</text>
      <biological_entity id="o32201" name="spike" name_original="spike" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="17" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s7" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral bracts ovate to lanceolate, clasping base of ovary, 1.5–4 mm, apex acuminate.</text>
      <biological_entity constraint="floral" id="o32202" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o32203" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s8" value="clasping" value_original="clasping" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32204" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
      <biological_entity id="o32205" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o32203" id="r4332" name="part_of" negation="false" src="d0_s8" to="o32204" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers minute, glabrous;</text>
      <biological_entity id="o32206" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals reflexed-spreading, pinkish, 1-veined, 1–2.2 × 1 mm;</text>
      <biological_entity id="o32207" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed-spreading" value_original="reflexed-spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2.2" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>dorsal sepal reflexed, ovate, curled;</text>
      <biological_entity constraint="dorsal" id="o32208" name="sepal" name_original="sepal" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="curled" value_original="curled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lateral sepals adnate with lip forming short mentum, triangular to deltate, slightly larger than dorsal sepal;</text>
      <biological_entity constraint="lateral" id="o32209" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character constraint="with lip" constraintid="o32210" is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s12" to="deltate" />
        <character constraint="than dorsal sepal" constraintid="o32212" is_modifier="false" name="size" src="d0_s12" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity id="o32210" name="lip" name_original="lip" src="d0_s12" type="structure" />
      <biological_entity id="o32211" name="mentum" name_original="mentum" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o32212" name="sepal" name_original="sepal" src="d0_s12" type="structure" />
      <relation from="o32210" id="r4333" name="forming" negation="false" src="d0_s12" to="o32211" />
    </statement>
    <statement id="d0_s13">
      <text>petals reflexed, white or pink, narrowly obovate to oblong, 1–1.5 × 0.5 mm;</text>
      <biological_entity id="o32213" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character char_type="range_value" from="narrowly obovate" name="shape" src="d0_s13" to="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="1.5" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lip erect, white, basally auriculate, hoodlike, enclosing column, shallowly saccate, 1–2 mm, fleshy, apiculate;</text>
      <biological_entity id="o32214" name="lip" name_original="lip" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s14" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="hoodlike" value_original="hoodlike" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o32215" name="column" name_original="column" src="d0_s14" type="structure" />
      <relation from="o32214" id="r4334" name="enclosing" negation="false" src="d0_s14" to="o32215" />
    </statement>
    <statement id="d0_s15">
      <text>column adaxially adnate to sepal tube, minute (0.3 mm), apex winged;</text>
      <biological_entity id="o32216" name="column" name_original="column" src="d0_s15" type="structure">
        <character constraint="to sepal tube" constraintid="o32217" is_modifier="false" modifier="adaxially" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="size" notes="" src="d0_s15" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o32217" name="tube" name_original="tube" src="d0_s15" type="structure" />
      <biological_entity id="o32218" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary sessile, 1.5–3.5 mm.</text>
      <biological_entity id="o32219" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules erect, ellipsoid, 4 mm.</text>
      <biological_entity id="o32220" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Dec–early Mar.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Mar" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America (Galapagos Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Galapagos Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>