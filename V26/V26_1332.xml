<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="treatment_page">647</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Maxillarieae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Oncidiinae</taxon_name>
    <taxon_name authority="R. Brown" date="1822" rank="genus">macradenia</taxon_name>
    <taxon_name authority="R. Brown" date="1822" rank="species">lutescens</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Reg.</publication_title>
      <place_in_publication>8: plate 612. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe maxillarieae;subtribe oncidiinae;genus macradenia;species lutescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220007992</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 18 cm (excluding inflorescence).</text>
      <biological_entity id="o1481" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade 8–15 × 1–2.5 cm.</text>
      <biological_entity id="o1482" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1483" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 1–2, to 12 cm;</text>
      <biological_entity id="o1484" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts 3–10 mm.</text>
      <biological_entity id="o1485" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers cream to pale orange with redbrown to red-purple spots;</text>
      <biological_entity id="o1486" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with redbrown to red-purple spots" from="cream" name="coloration" src="d0_s4" to="pale orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 6–120 × 3–6 mm, apex acute;</text>
      <biological_entity id="o1487" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="120" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1488" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 6–120 × 3–4 mm, apex acute;</text>
      <biological_entity id="o1489" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="120" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1490" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lip white to pale-yellow with yellow patch near callus, 7–10 × 6–8 mm, lateral lobes nearly circular, middle lobe nearly linear, apex acute and strongly reflexed;</text>
      <biological_entity id="o1491" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pale-yellow" />
        <character constraint="near callus" constraintid="o1492" is_modifier="false" name="coloration" src="d0_s7" value="yellow patch" value_original="yellow patch" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1492" name="callus" name_original="callus" src="d0_s7" type="structure" />
      <biological_entity constraint="lateral" id="o1493" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement_or_shape" src="d0_s7" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity constraint="middle" id="o1494" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o1495" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>callus of 3 shallow keels;</text>
      <biological_entity id="o1496" name="callus" name_original="callus" src="d0_s8" type="structure" />
      <biological_entity id="o1497" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="true" name="depth" src="d0_s8" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o1496" id="r195" name="consist_of" negation="false" src="d0_s8" to="o1497" />
    </statement>
    <statement id="d0_s9">
      <text>column white, often with 2 redbrown streaks, 8 mm.</text>
      <biological_entity id="o1498" name="column" name_original="column" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character name="some_measurement" notes="" src="d0_s9" unit="mm" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o1499" name="streak" name_original="streaks" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <relation from="o1498" id="r196" modifier="often" name="with" negation="false" src="d0_s9" to="o1499" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 15–20 mm.</text>
      <biological_entity id="o1500" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic in shaded sites on small branches and twigs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="epiphytic" constraint="in shaded sites on small branches and twigs" />
        <character name="habitat" value="shaded sites" constraint="on small branches and twigs" />
        <character name="habitat" value="small branches" />
        <character name="habitat" value="twigs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10[–500] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies; n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>