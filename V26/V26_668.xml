<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">331</other_info_on_meta>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Kunth" date="1843" rank="genus">dichelostemma</taxon_name>
    <taxon_name authority="(Smith) Kunth" date="1843" rank="species">congestum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>4: 470. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus dichelostemma;species congestum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220004030</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brodiaea</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">congesta</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>10: 3, plate 1. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brodiaea;species congesta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hookera</taxon_name>
    <taxon_name authority="(Smith) Jepson" date="unknown" rank="species">congesta</taxon_name>
    <taxon_hierarchy>genus Hookera;species congesta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 3–4, 4–35 cm;</text>
      <biological_entity id="o21504" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s0" to="4" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade strongly keeled, ± glaucous.</text>
      <biological_entity id="o21505" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Scape self-supporting, with occasional bends, 30–80 (–90) cm, ± scabrous.</text>
      <biological_entity id="o21506" name="scape" name_original="scape" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="self-supporting" value_original="self-supporting" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="90" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21507" name="bend" name_original="bends" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="occasional" value_original="occasional" />
      </biological_entity>
      <relation from="o21506" id="r2938" name="with" negation="false" src="d0_s2" to="o21507" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemose, very dense, 6–15-flowered;</text>
      <biological_entity id="o21508" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="very" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="6-15-flowered" value_original="6-15-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts pale-purple to green, widely lanceolate, 6–12 mm.</text>
      <biological_entity id="o21509" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="pale-purple" name="coloration" src="d0_s4" to="green" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers horizontal or erect;</text>
      <biological_entity id="o21510" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth bluish purple, tube unangled, narrowly ovoid, slightly constricted above ovary, 8–10 mm, lobes ascending, 8–10 mm;</text>
      <biological_entity id="o21511" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bluish purple" value_original="bluish purple" />
      </biological_entity>
      <biological_entity id="o21512" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unangled" value_original="unangled" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character constraint="above ovary" constraintid="o21513" is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21513" name="ovary" name_original="ovary" src="d0_s6" type="structure" />
      <biological_entity id="o21514" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth appendages 1 per stamen, each coalescent to an inner and outer tepal, leaning away from inner anthers to form corona, erect, purplish, narrowly lanceolate, 5–6 mm, apex deeply 2-fid into 2 wings;</text>
      <biological_entity constraint="perianth" id="o21515" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="per stamen" constraintid="o21516" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character constraint="to inner outer tepal" constraintid="o21517" is_modifier="false" name="fusion" notes="" src="d0_s7" value="coalescent" value_original="coalescent" />
        <character constraint="away-from inner anthers" constraintid="o21518" is_modifier="false" name="orientation" notes="" src="d0_s7" value="leaning" value_original="leaning" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21516" name="stamen" name_original="stamen" src="d0_s7" type="structure" />
      <biological_entity constraint="inner and outer" id="o21517" name="tepal" name_original="tepal" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o21518" name="anther" name_original="anthers" src="d0_s7" type="structure" />
      <biological_entity id="o21519" name="form" name_original="form" src="d0_s7" type="structure" />
      <biological_entity id="o21520" name="corona" name_original="corona" src="d0_s7" type="structure" />
      <biological_entity id="o21521" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character constraint="into wings" constraintid="o21522" is_modifier="false" modifier="deeply" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o21522" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o21518" id="r2939" name="to" negation="false" src="d0_s7" to="o21519" />
      <relation from="o21518" id="r2940" name="to" negation="false" src="d0_s7" to="o21520" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 3, equal;</text>
      <biological_entity id="o21523" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 4–5 mm;</text>
      <biological_entity id="o21524" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary sessile, 4–6 mm;</text>
      <biological_entity id="o21525" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 5–6 mm;</text>
      <biological_entity id="o21526" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 1–6 mm. 2n = 18, 36.</text>
      <biological_entity id="o21527" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21528" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (late Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="late Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woodlands, grasslands near coast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Fork-toothed ookow</other_name>
  <discussion>Dichelostemma congestum can be recognized by its congested racemose inflorescence and deeply bifid perianth appendages that stand away from the anthers to form a corona.</discussion>
  
</bio:treatment>