<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="genus">camassia</taxon_name>
    <taxon_name authority="(Engelmann &amp; A. Gray) Blankinship" date="1907" rank="species">angusta</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>18: 195. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus camassia;species angusta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101513</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scilla</taxon_name>
    <taxon_name authority="Engelmann &amp; A. Gray" date="unknown" rank="species">angusta</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>5: 237. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scilla;species angusta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camassia</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">fraseri</taxon_name>
    <taxon_name authority="(Engelmann &amp; A. Gray) Torrey" date="unknown" rank="variety">angusta</taxon_name>
    <taxon_hierarchy>genus Camassia;species fraseri;variety angusta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quamasia</taxon_name>
    <taxon_name authority="(Engelmann &amp; A. Gray) Piper" date="unknown" rank="species">angusta</taxon_name>
    <taxon_hierarchy>genus Quamasia;species angusta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs sometimes clustered, globose, 1–3 cm diam.</text>
      <biological_entity id="o20928" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" src="d0_s0" value="globose" value_original="globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3–11, 2–6 dm × 5–20 mm.</text>
      <biological_entity id="o20929" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="11" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s1" to="6" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 27–87 cm;</text>
      <biological_entity id="o20930" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="27" from_unit="cm" name="some_measurement" src="d0_s2" to="87" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sterile bracts 3–19 (–28), bracts subtending flowers shorter than or equaling pedicel.</text>
      <biological_entity id="o20931" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="19" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="28" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="19" />
      </biological_entity>
      <biological_entity id="o20932" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character constraint="than or equaling pedicel" constraintid="o20934" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20933" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o20934" name="pedicel" name_original="pedicel" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o20932" id="r2858" name="subtending" negation="false" src="d0_s3" to="o20933" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers actinomorphic;</text>
      <biological_entity id="o20935" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tepals withering separately, sometimes connivent over capsules, light blue to lavender, each 3-veined or 5-veined, 6–10 × 2.2–3.6 mm;</text>
      <biological_entity id="o20936" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="separately" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
        <character constraint="over capsules" constraintid="o20937" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s5" value="connivent" value_original="connivent" />
        <character char_type="range_value" from="light blue" name="coloration" notes="" src="d0_s5" to="lavender" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-veined" value_original="5-veined" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s5" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20937" name="capsule" name_original="capsules" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>anthers bright-yellow, 1.5–2 mm;</text>
    </statement>
    <statement id="d0_s7">
      <text>fruiting pedicel incurving-erect, 6–12 mm.</text>
      <biological_entity id="o20938" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="incurving-erect" value_original="incurving-erect" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20939" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <relation from="o20938" id="r2859" name="fruiting" negation="false" src="d0_s7" to="o20939" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules deciduous, pale green to light-brown, ovoid-ellipsoid, 6–10 mm.</text>
      <biological_entity id="o20940" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s8" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 2–5 per locule.</text>
      <biological_entity id="o20942" name="locule" name_original="locule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 30.</text>
      <biological_entity id="o20941" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o20942" from="2" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20943" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Ind., Iowa, Kans., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Prairie camas</other_name>
  <discussion>Camassia angusta flowers two to three weeks later than sympatric populations of C. scilloides.</discussion>
  
</bio:treatment>