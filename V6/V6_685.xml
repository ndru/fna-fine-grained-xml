<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 21. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species wrightii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101203</other_info_on_name>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Wright’s globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o11329" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, gray-green, 2–5 (–7.5) dm, canescent to tomentose.</text>
      <biological_entity id="o11330" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="7.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character char_type="range_value" from="canescent" name="pubescence" src="d0_s1" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades gray-green, widely ovate or triangular to orbiculate, all but proximalmost deeply pedately divided, (1–) 2–4 cm, not rugose, base truncate to cordate, margins entire, surfaces stellate-pubescent.</text>
      <biological_entity id="o11331" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s2" to="orbiculate" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" modifier="deeply pedately" name="shape" src="d0_s2" value="divided" value_original="divided" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_distance" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o11332" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cordate" />
      </biological_entity>
      <biological_entity id="o11333" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11334" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemose or paniculate, narrow, open, few–many-flowered, tip not leafy;</text>
      <biological_entity id="o11335" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="few-many-flowered" value_original="few-many-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets green to tan.</text>
      <biological_entity id="o11336" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o11337" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 6–7 mm, not forming beak;</text>
      <biological_entity id="o11338" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11339" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11340" name="beak" name_original="beak" src="d0_s5" type="structure" />
      <relation from="o11339" id="r1244" name="forming" negation="true" src="d0_s5" to="o11340" />
    </statement>
    <statement id="d0_s6">
      <text>petals lavender, red-orange, or pink, 10–13.5 (–18) mm;</text>
      <biological_entity id="o11341" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11342" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character char_type="range_value" from="13.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="18" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="13.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow or purple.</text>
      <biological_entity id="o11343" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o11344" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps hemispheric to truncate-conic;</text>
      <biological_entity id="o11345" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s8" to="truncate-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 12–15, 4–7 × 2.5–3 mm, chartaceous, nonreticulate dehiscent part 55–65% of height, with prominent ventral beak, tip acutish, cuspidate, indehiscent part not wider than dehiscent part, sides prominently reticulate.</text>
      <biological_entity id="o11346" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="15" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o11347" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acutish" value_original="acutish" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity constraint="ventral" id="o11348" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="ventral" id="o11349" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o11350" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="than dehiscent part" constraintid="o11351" is_modifier="false" name="width" src="d0_s9" value="not wider" value_original="not wider" />
      </biological_entity>
      <biological_entity id="o11351" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o11352" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_coloration_or_relief" src="d0_s9" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o11347" id="r1245" modifier="55-65%" name="part_of" negation="false" src="d0_s9" to="o11348" />
      <relation from="o11347" id="r1246" modifier="55-65%" name="part_of" negation="false" src="d0_s9" to="o11349" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds 2 per mericarp, black, pubescent.</text>
      <biological_entity id="o11354" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 20.</text>
      <biological_entity id="o11353" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character constraint="per mericarp" constraintid="o11354" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11355" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky areas" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>