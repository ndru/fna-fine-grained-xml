<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">408</other_info_on_meta>
    <other_info_on_meta type="illustration_page">403</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Spach" date="1836" rank="genus">crocanthemum</taxon_name>
    <taxon_name authority="(Nuttall) Millspaugh" date="unknown" rank="species">scoparium</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>5: 175. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus crocanthemum;species scoparium</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101255</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthemum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">scoparium</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 152. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthemum;species scoparium</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Broom or peak rushrose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs.</text>
      <biological_entity id="o21265" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect-fastigiate, 10–45 cm, usually sparsely stellate-pubescent to glabrate, sometimes densely lanate.</text>
      <biological_entity id="o21266" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="erect-fastigiate" value_original="erect-fastigiate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character char_type="range_value" from="usually sparsely stellate-pubescent" name="pubescence" src="d0_s1" to="glabrate" />
        <character is_modifier="false" modifier="sometimes densely" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, tending to deciduous in summer;</text>
      <biological_entity id="o21267" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character constraint="in summer" is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–2 mm;</text>
      <biological_entity id="o21268" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 5–11 × 0.5–2 (–3.5) mm, surfaces stellate-pubescent to glabrate abaxially, sparsely stellate-pubescent to glabrate adaxially, lateral-veins obscure abaxially.</text>
      <biological_entity id="o21269" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21270" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="stellate-pubescent" modifier="adaxially" name="pubescence" src="d0_s4" to="glabrate abaxially" />
      </biological_entity>
      <biological_entity id="o21271" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, panicles or racemes;</text>
      <biological_entity id="o21272" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21273" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o21274" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>chasmogamous flowers 1–18 per panicle or raceme, cleistogamous 0.</text>
      <biological_entity id="o21275" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" constraint="per raceme" constraintid="o21277" from="1" name="quantity" src="d0_s6" to="18" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21276" name="panicle" name_original="panicle" src="d0_s6" type="structure" />
      <biological_entity id="o21277" name="raceme" name_original="raceme" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 2–6 mm, sparsely or not glandular-hairy;</text>
      <biological_entity id="o21278" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; not" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 2–4 × 0.3–0.5 mm.</text>
      <biological_entity id="o21279" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Chasmogamous flowers: outer sepals linear, 1.5–3.5 × 0.3 mm, inner sepals 3.5–5 (–7.5) × 2–3 mm, apex acute to acuminate;</text>
      <biological_entity id="o21280" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21281" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21282" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21283" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx stellate-pubescent, hairs to 1 mm;</text>
      <biological_entity id="o21284" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o21285" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <biological_entity id="o21286" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals obovate, 3–6 × 3–5 mm;</text>
      <biological_entity id="o21287" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o21288" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>capsules 2.8–3.8 × 2–2.5 mm, glabrous.</text>
      <biological_entity id="o21289" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o21290" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s12" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Even with the recognition of Crocanthemum aldersonii and C. suffrutescens as separate species, morphological diversity within C. scoparium still remains considerable. The two varieties here recognized show differences in habit, average plant height, number of flowers, and distribution. Another variant occurs sporadically along the coast and on Santa Cruz Island, from Monterey to San Diego counties; vegetative parts (at least distal branches, pedicels, and sepals) are covered with white, lanate hairs. This variant has never been formally named. Another form from coastal Mendocino County was called “Helianthemum mendocinensis” by Alice Eastwood on a specimen (H. E. Brown 785, JEPS); the name was never published. These plants have densely stellate-pubescent stems and exceptionally elongate sepal tips.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems mostly 10–30 cm, divaricate and spreading or curved proximally and erect distally; mostly near-coastal.</description>
      <determination>14a Crocanthemum scoparium var. scoparium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually 30–45 cm, usually erect-fastigiate, sometimes curved proximally and erect distally; mostly inland.</description>
      <determination>14b Crocanthemum scoparium var. vulgare</determination>
    </key_statement>
  </key>
</bio:treatment>