<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">419</other_info_on_meta>
    <other_info_on_meta type="mention_page">420</other_info_on_meta>
    <other_info_on_meta type="illustration_page">417</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">droseraceae</taxon_name>
    <taxon_name authority="Solander ex J. Ellis" date="1768" rank="genus">dionaea</taxon_name>
    <taxon_name authority="J. Ellis" date="unknown" rank="species">muscipula</taxon_name>
    <place_of_publication>
      <publication_title>St. James’s Chron. Brit. Eve. Post</publication_title>
      <place_in_publication>1172: [4]. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family droseraceae;genus dionaea;species muscipula</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220004158</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rosettes often clumping in older specimens;</text>
      <biological_entity id="o18864" name="specimen" name_original="specimens" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>whole plant black upon drying.</text>
      <biological_entity id="o18863" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character constraint="in specimens" constraintid="o18864" is_modifier="false" modifier="often" name="arrangement_or_growth_form" src="d0_s0" value="clumping" value_original="clumping" />
      </biological_entity>
      <biological_entity id="o18865" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="upon drying" is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 2–7 (–10) × 0.2–1 (–2) cm, longer in shade-grown plants;</text>
      <biological_entity id="o18866" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18867" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
        <character constraint="in plants" constraintid="o18868" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o18868" name="plant" name_original="plants" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade lobes 1.2–2.5 (–3) × 1–2 cm, marginal bristles 0.3–0.8 cm, rarely larger in all respects.</text>
      <biological_entity id="o18869" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="blade" id="o18870" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o18871" name="bristle" name_original="bristles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s3" to="0.8" to_unit="cm" />
        <character constraint="in respects" constraintid="o18872" is_modifier="false" modifier="rarely" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o18872" name="respect" name_original="respects" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: scape (15–) 18–26 (–43) cm;</text>
      <biological_entity id="o18873" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18874" name="scape" name_original="scape" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="18" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="43" to_unit="cm" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s4" to="26" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts subtending pedicels 5 or 6, linear to lanceolate, 0.3–0.7 × 0.1–0.2 cm.</text>
      <biological_entity id="o18875" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o18876" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="count" value_original="count" />
        <character is_modifier="false" name="shape" src="d0_s5" value="list" value_original="list" />
        <character name="quantity" src="d0_s5" unit="or punct linear to lanceolate" value="5" value_original="5" />
        <character name="quantity" src="d0_s5" unit="or punct linear to lanceolate" value="6" value_original="6" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s5" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s5" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18877" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o18876" id="r1989" name="subtending" negation="false" src="d0_s5" to="o18877" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals green, lanceolate, 5–6 (–10) × 2–3 mm;</text>
      <biological_entity id="o18878" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18879" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals obovate to obspatulate, 10–12 × 5–6 mm, base cuneate;</text>
      <biological_entity id="o18880" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18881" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="obspatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18882" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigma papillate.</text>
      <biological_entity id="o18883" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18884" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 3–4 × 4–6 mm.</text>
      <biological_entity id="o18885" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds shiny black, obovoid, 1–1.5 mm. 2n = 32.</text>
      <biological_entity id="o18886" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18887" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, sandy-peaty soil and sphagnum mats of roadsides, ditch banks, borrow-pits, longleaf pine savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" constraint="of roadsides , ditch banks , borrow-pits , longleaf pine savannas" />
        <character name="habitat" value="sandy-peaty soil" constraint="of roadsides , ditch banks , borrow-pits , longleaf pine savannas" />
        <character name="habitat" value="sphagnum mats" constraint="of roadsides , ditch banks , borrow-pits , longleaf pine savannas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="ditch banks" />
        <character name="habitat" value="borrow-pits" />
        <character name="habitat" value="longleaf pine savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Calif., Fla., N.J., N.C., Pa., S.C., Va., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dionaea muscipula has occurred historically in the outer Coastal Plain from Beaufort County, North Carolina, to Charleston County, South Carolina, and inland as far as Moore County, North Carolina. The present native range of the species is much reduced due to drainage, habitat conversion, fire exclusion, and development. It has been introduced into southern Alabama, northern California, eastern Pennsylvania, and Caroline County, Virginia, where it is doubtfully truly naturalized; it is reportedly naturalized in the Apalachicola region of Florida, southern New Jersey, and Skagit County, Washington.</discussion>
  <discussion>Dionaea muscipula is one of the world’s most famous plants. C. Darwin (1875) studied its actions extensively. The trigger hairs must be touched twice in close succession for the traps to snap shut (in less than a second), the bristles helping to keep perky prey from escaping. This double stimulation helps prevent inorganic debris and casual rain drops from triggering trap closure. When lightly closed, the two trap-lobes come together and appear convex, opening again the next day if no prey has been caught. If a protein meal is detected, the lobes will tighten completely, become slightly concave, squeeze the victim, and secrete digestive enzymes. After the prey is digested, the traps grow themselves open again, expose the indigestible carcass, and remain viable for some time. New leaf-traps are constantly formed (they curl open in a somewhat circinate fashion) as old ones die and turn black. Much research has been conducted on the mechanisms of stimulation and trap closure (B. E. Juniper et al. 1989).</discussion>
  <discussion>There are many interesting accounts of encounters and explanations for flytraps in the older literature (F. E. Lloyd 1942) and the traps continue to be fascinating to old and young alike. Not the least of their mysterious features is their narrow distribution, which may be related to limited seed dispersal, unusual soil type (nutrient deficient), relatively high annual rainfall, and periodic fire.</discussion>
  <discussion>In southeastern North Carolina and adjacent South Carolina Dionaea muscipula grows in association with no fewer than 16 other species of carnivorous plants, the highest diversity of carnivorous plants anywhere in the world. While usually locally abundant, the Venus’s-flytrap is very sensitive to shading and drought and may go dormant during dry periods or if its habitat does not burn every five years or so. Dionaea requires sites with saturated but not inundated soils and strong sunlight, especially ecotones between pocosins and longleaf pine-dominated savannas or sandhills. Roadside populations of thousands of plants are managed for seed collection for nursery-grown specimens.</discussion>
  <discussion>It is curious that the leaf petioles can vary from relatively long to short, apparently under genetic as well as environmental influences, and the adaxial surface of the traps may be bright red or greenish. Strong sunlight sometimes brings out the red color, but not always. Variants have been found in cultivation in which the entire plant is a rich red-burgundy color. There are also cultivars with variations in the lengths of the marginal bristles. The plants are easy to grow as long as fresh water low in solutes is available to keep the soil moist, and there is abundant bright light. They do not make good windowsill house plants.</discussion>
  
</bio:treatment>