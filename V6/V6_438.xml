<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Nuttall" date="1821" rank="genus">callirhoë</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">bushii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>11: 51. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus callirhoë;species bushii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416214</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Callirhoë</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="species">involucrata</taxon_name>
    <taxon_name authority="(Fernald) R. F. Martin" date="unknown" rank="variety">bushii</taxon_name>
    <taxon_hierarchy>genus Callirhoë;species involucrata;variety bushii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="(Cavanilles) A. Gray" date="unknown" rank="species">papaver</taxon_name>
    <taxon_name authority="(Fernald) Waterfall" date="unknown" rank="variety">bushii</taxon_name>
    <taxon_hierarchy>genus C.;species papaver;variety bushii</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Bush’s poppy mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o16288" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–9, weakly erect, ascending, or decumbent, 4.8–14 dm, hairy, hairs 4-rayed, stellate, and often simple, spreading or retrorse, sometimes glabrate.</text>
      <biological_entity id="o16289" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="9" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="4.8" from_unit="dm" name="some_measurement" src="d0_s1" to="14" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16290" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, ovate, somewhat auriculate, 8–16 (–21) × 3.5–10 (–13) mm;</text>
      <biological_entity id="o16291" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16292" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="21" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="16" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="13" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–27 (–37) cm;</text>
      <biological_entity id="o16293" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16294" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="37" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="27" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade suborbiculate to ovate, (3–) 5–7-lobed, 4–14 (–19) × (2.3–) 5–15 cm, surfaces hairy, hairs stellate and simple, lobes broad, oblong or obovate.</text>
      <biological_entity id="o16295" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16296" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(3-)5-7-lobed" value_original="(3-)5-7-lobed" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="19" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="atypical_width" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16297" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16298" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o16299" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose;</text>
    </statement>
    <statement id="d0_s6">
      <text>involucellar bractlets 3, lanceolate or ovate, 8–22 × 1–4 mm.</text>
      <biological_entity id="o16300" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o16301" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o16302" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyx lobes valvate in bud, forming apiculate or acuminate point;</text>
      <biological_entity constraint="calyx" id="o16303" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character constraint="in bud" constraintid="o16304" is_modifier="false" name="arrangement_or_dehiscence" src="d0_s8" value="valvate" value_original="valvate" />
      </biological_entity>
      <biological_entity id="o16304" name="bud" name_original="bud" src="d0_s8" type="structure" />
      <biological_entity id="o16305" name="point" name_original="point" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="apiculate" value_original="apiculate" />
        <character is_modifier="true" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o16303" id="r1753" name="forming" negation="false" src="d0_s8" to="o16305" />
    </statement>
    <statement id="d0_s9">
      <text>petals red or pale-red without white basal spot, 2–3.2 cm.</text>
      <biological_entity id="o16306" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character constraint="without basal spot" constraintid="o16307" is_modifier="false" name="coloration" src="d0_s9" value="pale-red" value_original="pale-red" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s9" to="3.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16307" name="spot" name_original="spot" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Schizocarps 8.5–11.5 mm diam.;</text>
      <biological_entity id="o16308" name="schizocarp" name_original="schizocarps" src="d0_s10" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="diameter" src="d0_s10" to="11.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mericarps 15–20, 4–4.6 × 2–3.5 mm, sparsely hairy, indehiscent;</text>
      <biological_entity id="o16309" name="mericarp" name_original="mericarps" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="20" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>beaks not prominent, 0.7–2 mm;</text>
      <biological_entity id="o16310" name="beak" name_original="beaks" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>collars well developed, 2-lobed.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 56.</text>
      <biological_entity id="o16311" name="collar" name_original="collars" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s13" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16312" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer(–early fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="atypical_range" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky woods, limestone glades, glade margins, meadows, disturbed, open areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky woods" />
        <character name="habitat" value="limestone glades" />
        <character name="habitat" value="glade margins" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="disturbed" />
        <character name="habitat" value="open areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Iowa, Kans., Mo., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Callirhoë bushii is found in the Ozark Plateaus, Ouachita Mountains, and Cherokee Plains. Adventive populations have also been found north of the Missouri River in Iowa and Missouri.</discussion>
  <discussion>Callirhoë bushii is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>