<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">160</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">146</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="M. S. Baker &amp; J. C. Clausen" date="unknown" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>5: 142. 1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species tomentosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100967</other_info_on_name>
  </taxon_identification>
  <number>65</number>
  <other_name type="common_name">Woolly or felt-leaved or feltleaf violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 7–10 cm.</text>
      <biological_entity id="o8596" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–5), prostrate or decumbent to erect, leafy proximally and distally, densely white-tomentose, from usually vertical, subligneous rhizome.</text>
      <biological_entity id="o8597" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character name="growth_form_or_orientation" src="d0_s1" value="decumbent to erect" value_original="decumbent to erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="distally; densely" name="pubescence" src="d0_s1" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o8598" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="usually" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
      </biological_entity>
      <relation from="o8597" id="r903" name="from" negation="false" src="d0_s1" to="o8598" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o8599" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 1–6;</text>
      <biological_entity constraint="basal" id="o8600" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules linear to broadly ovate-oblong, margins entire, sometimes with scattered glandular-hairs, apex acute to obtuse;</text>
      <biological_entity constraint="basal" id="o8601" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o8602" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="broadly ovate-oblong" />
      </biological_entity>
      <biological_entity id="o8603" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8604" name="glandular-hair" name_original="glandular-hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o8605" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <relation from="o8603" id="r904" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o8604" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 2–6 cm, densely white-tomentose;</text>
      <biological_entity constraint="basal" id="o8606" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o8607" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ± oblanceolate to elliptic, 1.5–5 × 1.4–2.1 cm, base attenuate, usually oblique, margins usually entire, rarely crenate distally, ciliate, apex acute to usually obtuse, mucronulate, surfaces densely white-tomentose abaxially, strigose adaxially;</text>
      <biological_entity constraint="basal" id="o8608" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o8609" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="less oblanceolate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s6" to="2.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8610" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="orientation_or_shape" src="d0_s6" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o8611" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely; distally" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8612" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="usually obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o8613" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: stipules ovate, lanceolate, oblanceolate, or oblong, margins entire or toothed, densely ciliate with white hairs;</text>
      <biological_entity constraint="cauline" id="o8614" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8615" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o8616" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8617" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character constraint="with hairs" constraintid="o8618" is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8618" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <relation from="o8614" id="r905" name="to" negation="false" src="d0_s7" to="o8615" />
      <relation from="o8615" id="r906" name="except" negation="false" src="d0_s7" to="o8616" />
    </statement>
    <statement id="d0_s8">
      <text>petiole 1.5–3.5 cm;</text>
      <biological_entity constraint="cauline" id="o8619" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o8620" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o8621" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3.5" to_unit="cm" />
      </biological_entity>
      <relation from="o8619" id="r907" name="to" negation="false" src="d0_s8" to="o8620" />
      <relation from="o8620" id="r908" name="except" negation="false" src="d0_s8" to="o8621" />
    </statement>
    <statement id="d0_s9">
      <text>blade 1.8–4 × 0.6–1.1 cm.</text>
      <biological_entity constraint="cauline" id="o8622" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o8623" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o8624" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s9" to="1.1" to_unit="cm" />
      </biological_entity>
      <relation from="o8622" id="r909" name="to" negation="false" src="d0_s9" to="o8623" />
      <relation from="o8623" id="r910" name="except" negation="false" src="d0_s9" to="o8624" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 1–4 cm, densely white-tomentose.</text>
      <biological_entity id="o8625" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals lanceolate, margins ciliate-tomentose, auricles 0.5–1 mm;</text>
      <biological_entity id="o8626" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8627" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o8628" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="ciliate-tomentose" value_original="ciliate-tomentose" />
      </biological_entity>
      <biological_entity id="o8629" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals deep lemon-yellow adaxially, upper 2 often brownish purple abaxially, lower 3 dark-brown to brownish purple-veined, lateral 2 bearded, lowest 6–11 mm, spur yellow, gibbous, 0.5–1.5 mm;</text>
      <biological_entity id="o8630" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8631" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s12" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character modifier="often" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s12" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" name="position" src="d0_s12" value="lower" value_original="lower" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown to brownish purple-veined" value_original="dark-brown to brownish purple-veined" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown to brownish purple-veined" value_original="dark-brown to brownish purple-veined" />
      </biological_entity>
      <biological_entity id="o8632" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style head bearded;</text>
      <biological_entity id="o8633" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="style" id="o8634" name="head" name_original="head" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>cleistogamous flowers absent.</text>
      <biological_entity id="o8635" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8636" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ± spherical, 4–5 mm, densely white-tomentose.</text>
      <biological_entity id="o8637" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="spherical" value_original="spherical" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown with lighter brown mottling, 2.5–2.8 mm. 2n = 12.</text>
      <biological_entity id="o8638" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown with lighter brown mottling" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8639" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, gravelly places, open ponderosa, Jeffrey, lodgepole pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly places" modifier="dry" />
        <character name="habitat" value="open ponderosa" />
        <character name="habitat" value="lodgepole pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Viola tomentosa occurs in El Dorado, Nevada, Placer, Plumas, and Sierra counties. M. S. Baker (1949) reported that nearly every leaf axil of V. tomentosa produces a flower bud and that these buds produce chasmogamous flowers late in season instead of cleistogamous flowers, as do other members of the V. nuttallii complex.</discussion>
  <discussion>Viola tomentosa hybridizes with V. purpurea; the hybrids appeared sterile (M. S. Baker 1949). J. Clausen (1964) reported a putative hybrid between V. tomentosa and V. sheltonii from one location in Sierra County.</discussion>
  
</bio:treatment>