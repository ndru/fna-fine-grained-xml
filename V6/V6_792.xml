<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>T. Lawrence Mellichamp</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
    <other_info_on_meta type="mention_page">419</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">DROSERACEAE</taxon_name>
    <taxon_hierarchy>family DROSERACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10290</other_info_on_name>
  </taxon_identification>
  <number>0</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, carnivorous, scapose.</text>
      <biological_entity id="o15636" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="carnivorous" value_original="carnivorous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves in basal rosettes (alternate-cauline in Drosera intermedia);</text>
      <biological_entity constraint="basal" id="o15638" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o15637" id="r1674" name="in" negation="false" src="d0_s1" to="o15638" />
    </statement>
    <statement id="d0_s2">
      <text>stipulate or estipulate;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o15637" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="estipulate" value_original="estipulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade infolded or circinate in vernation, modified as hinged, jawlike trap (Dionaea) [Aldrovanda] or bearing mucilage-tipped, irritable, multicelled hairs (Drosera) [Drosophyllum].</text>
      <biological_entity id="o15639" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="vernation" src="d0_s4" value="infolded" value_original="infolded" />
        <character is_modifier="false" name="vernation" src="d0_s4" value="circinate" value_original="circinate" />
        <character constraint="as " constraintid="o15641" is_modifier="false" name="development" src="d0_s4" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity id="o15640" name="trap" name_original="trap" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="jawlike" value_original="jawlike" />
      </biological_entity>
      <biological_entity id="o15641" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="mucilage-tipped" value_original="mucilage-tipped" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="multicelled" value_original="multicelled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, umbellike cymes (Dionaea) or lateral, circinate or scorpioid-cymes (Drosera), multiflowered (rarely 1-flowered).</text>
      <biological_entity id="o15642" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o15643" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="umbel-like" value_original="umbellike" />
        <character is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="circinate" value_original="circinate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="multiflowered" value_original="multiflowered" />
      </biological_entity>
      <biological_entity id="o15644" name="scorpioid-cyme" name_original="scorpioid-cymes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth and androecium hypogynous;</text>
      <biological_entity id="o15645" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15646" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o15647" name="androecium" name_original="androecium" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 5, distinct or connate basally;</text>
      <biological_entity id="o15648" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15649" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5, distinct;</text>
      <biological_entity id="o15650" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15651" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens usually 5 (Drosera) or (10–) 15 (–20) (Dionaea), distinct or sometimes connate basally;</text>
      <biological_entity id="o15652" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15653" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s9" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="20" />
        <character name="quantity" src="d0_s9" value="15" value_original="15" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes; basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistils 1, compound, 3–5-carpellate;</text>
      <biological_entity id="o15654" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15655" name="pistil" name_original="pistils" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-5-carpellate" value_original="3-5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 1-locular;</text>
      <biological_entity id="o15656" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15657" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>placentation basal (Dionaea) or parietal (Drosera);</text>
      <biological_entity id="o15658" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s12" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 1 and undivided (Dionaea) or 3 [5] and bifid (Drosera);</text>
      <biological_entity id="o15659" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15660" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="undivided" value_original="undivided" />
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="bifid" value_original="bifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma plumose (Dionaea) or capitate (Drosera).</text>
      <biological_entity id="o15661" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15662" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="plumose" value_original="plumose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsular.</text>
      <biological_entity id="o15663" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 4, species ca. 175 (2 genera, 9 species in the flora).</discussion>
  <discussion>Droseraceae comprise carnivorous plants with an unusual, worldwide distribution. They live mostly in sunny, low-nutrient, moist-to-wet acidic sands, clays, seeps, and peat bogs, often subjected to periodic fires. They catch rather small prey in jawlike traps (Aldrovanda Linnaeus and Dionaea) or on sticky glandular hairs (Drosera and Drosophyllum Link). All genera, except Drosera, are monotypic and sometimes have been placed in separate families for various reasons. F. Rivadavia et al. (2003) indicated that Droseraceae are monophyletic including these three genera but excluding Drosophyllum; K. M. Cameron et al. (2002) believed that the Old World Aldrovanda is more closely related to Dionaea, and both genera are relicts of a more widespread distribution based on fossil pollen records.</discussion>
  <discussion>Some species of Droseraceae are grown worldwide as ornamental bog garden or terrarium specimens and as such have been given formal or informal cultivar names. Species of Drosera have been artificially hybridized for horticultural purposes; in the wild the rare hybrids that do occur are normally sterile.</discussion>
  <references>
    <reference>Williams, S. E., V. A. Albert, and M. W. Chase. 1994. Relations of the Droseraceae: A cladistic analysis of rbcL sequence and morphological data. Amer. J. Bot. 81: 1027–1037.</reference>
    <reference>Wood, C. E. Jr. 1960. The genera of Sarraceniaceae and Droseraceae in the southeastern United States. J. Arnold Arbor. 41: 152–163.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades of 2 subreniform, hinged lobes that act as jawlike traps, adaxial surface with 3 trigger hairs; inflorescences umbel-like cymes; stamens 15–20.</description>
      <determination>1 Dionaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades unlobed, surfaces covered with glandular trichomes; inflorescences circinate or scorpioid cymes; stamens 5.</description>
      <determination>2 Drosera</determination>
    </key_statement>
  </key>
</bio:treatment>