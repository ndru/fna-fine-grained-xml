<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="illustration_page">362</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="unknown" rank="species">coccinea</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>40: 58. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species coccinea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101174</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">coccinea</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Upper Louisiana, no.</publication_title>
      <place_in_publication>51. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malva;species coccinea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malvastrum</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">coccineum</taxon_name>
    <taxon_hierarchy>genus Malvastrum;species coccineum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="unknown" rank="species">coccinea</taxon_name>
    <taxon_hierarchy>genus Sida;species coccinea</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Scarlet or common globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, rhizomatous.</text>
      <biological_entity id="o1164" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–6, ascending or decumbent, light green to grayish, 1–3 (–5) dm, stellate green.</text>
      <biological_entity id="o1165" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="6" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="light green" name="coloration" src="d0_s1" to="grayish" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 5–10 mm;</text>
      <biological_entity id="o1166" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o1167" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals red-orange, 5–20 mm;</text>
      <biological_entity id="o1168" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1169" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="red-orange" value_original="red-orange" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>anthers yellow.</text>
      <biological_entity id="o1170" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1171" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Schizocarps flattened spheric-conic;</text>
      <biological_entity id="o1172" name="schizocarp" name_original="schizocarps" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spheric-conic" value_original="spheric-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>mericarps 10–14, 3–3.5 × 2.5–3 mm, thick, coriaceous, nonreticulate dehiscent part 10–35% of height, muticous, indehiscent part usually wider than dehiscent part.</text>
      <biological_entity id="o1173" name="mericarp" name_original="mericarps" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="14" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s6" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o1174" name="part" name_original="part" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s6" value="dehiscent" value_original="dehiscent" />
        <character constraint="than dehiscent part" constraintid="o1176" is_modifier="false" name="width" src="d0_s6" value="usually wider" value_original="usually wider" />
      </biological_entity>
      <biological_entity id="o1175" name="part" name_original="part" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="height" value_original="height" />
        <character is_modifier="true" name="shape" src="d0_s6" value="muticous" value_original="muticous" />
        <character is_modifier="true" name="dehiscence" src="d0_s6" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o1176" name="part" name_original="part" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s6" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <relation from="o1174" id="r128" modifier="10-35%" name="part_of" negation="false" src="d0_s6" to="o1175" />
    </statement>
    <statement id="d0_s7">
      <text>Seeds 1 per mericarp, gray to black, ± glabrous.</text>
      <biological_entity id="o1177" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character constraint="per mericarp" constraintid="o1178" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character char_type="range_value" from="gray" name="coloration" notes="" src="d0_s7" to="black" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1178" name="mericarp" name_original="mericarp" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Ariz., Colo., Idaho, Iowa, Kans., Mont., N.Dak., N.Mex., Nebr., Okla., S.Dak., Tex., Utah, Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Sphaeralcea coccinea is variable; it is one of the first sphaeralceas to bloom and is commonly found on roadsides.</discussion>
  <references>
    <reference>La Duke, J. C. and D. K. Northington. 1978. The systematics of Sphaeralcea coccinea (Nutt.) Rydberg. SouthW. Naturalist 23: 651–660.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades: midlobe ± equaling secondary lobes.</description>
      <determination>4a Sphaeralcea coccinea var. coccinea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades: midlobe longer than secondary lobes.</description>
      <determination>4b Sphaeralcea coccinea var. elata</determination>
    </key_statement>
  </key>
</bio:treatment>