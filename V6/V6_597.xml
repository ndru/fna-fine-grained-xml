<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Cybele Columb.</publication_title>
      <place_in_publication>1: 35. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species covillei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101119</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sidalcea</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_name authority="(Greene) Roush" date="unknown" rank="variety">covillei</taxon_name>
    <taxon_hierarchy>genus Sidalcea;species neomexicana;variety covillei</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Owens Valley checkerbloom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.2–0.6 m, often glaucous, with fleshy, simple to clustered roots, without caudex or rhizomes.</text>
      <biological_entity id="o4902" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4903" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o4904" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o4905" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o4902" id="r573" name="with" negation="false" src="d0_s0" to="o4903" />
      <relation from="o4902" id="r574" name="without" negation="false" src="d0_s0" to="o4904" />
      <relation from="o4902" id="r575" name="without" negation="false" src="d0_s0" to="o4905" />
    </statement>
    <statement id="d0_s1">
      <text>Stems several, clustered, erect, solid, often glaucous proximally, base sparsely, finely to coarsely stellate-hairy or hispid, hairs smaller distally.</text>
      <biological_entity id="o4906" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="several" value_original="several" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="often; proximally" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o4907" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="finely to coarsely; coarsely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o4908" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s1" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–5 per stem, mostly basal;</text>
      <biological_entity id="o4909" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o4910" from="2" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <biological_entity id="o4910" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o4911" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linearlanceolate, 3–6 × 1 mm;</text>
      <biological_entity id="o4912" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="6" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (4–) 5–10 cm, reduced on cauline leaves, proximal 1–4 times as long as blade, distal 1/2 times to as long as blade;</text>
      <biological_entity id="o4913" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character constraint="on cauline leaves" constraintid="o4914" is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4914" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o4915" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="blade" constraintid="o4916" is_modifier="false" name="length" src="d0_s4" value="1-4 times as long as blade" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character constraint="blade" constraintid="o4917" is_modifier="false" name="length" src="d0_s4" value="1/2 times to as long as blade" />
      </biological_entity>
      <biological_entity id="o4916" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o4917" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade fleshy, glaucous, rather densely stellate-hairy, proximalmost usually shallowly to deeply, ternately, palmately 5–7-lobed, 1.5–3 × 1.5–4 cm, lobes obovate, margins crenate-dentate, distal deeply 3–7-lobed, lobes linear, distalmost 2–4 cm wide.</text>
      <biological_entity id="o4918" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="rather densely" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" modifier="usually shallowly; shallowly to deeply" name="position" src="d0_s5" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" modifier="deeply; ternately; palmately" name="shape" src="d0_s5" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4919" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o4920" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="crenate-dentate" value_original="crenate-dentate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4921" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="3-7-lobed" value_original="3-7-lobed" />
      </biological_entity>
      <biological_entity id="o4922" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o4923" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences erect, open, calyces not conspicuously overlapping except sometimes in bud, branched or unbranched, nearly scapose, often 20+-flowered, slender, elongate, 1-sided or not, 6–30 cm;</text>
      <biological_entity id="o4924" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4925" name="calyx" name_original="calyces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not conspicuously" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s6" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="20+-flowered" value_original="20+-flowered" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="6" from_unit="cm" modifier="not" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4926" name="bud" name_original="bud" src="d0_s6" type="structure" />
      <relation from="o4925" id="r576" name="except" negation="false" src="d0_s6" to="o4926" />
    </statement>
    <statement id="d0_s7">
      <text>bracts inconspicuous, linear, 2-fid, 2–4 mm, shorter than calyx and pedicels.</text>
      <biological_entity id="o4927" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character constraint="than calyx and pedicels" constraintid="o4928, o4929" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4928" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o4929" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–8 (–10) mm;</text>
    </statement>
    <statement id="d0_s9">
      <text>involucellar bractlets absent.</text>
      <biological_entity id="o4930" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4931" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual;</text>
      <biological_entity id="o4932" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx 5–8 mm, uniformly, densely stellate-puberulent or few with longer rays;</text>
      <biological_entity id="o4933" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="uniformly; densely" name="pubescence" src="d0_s11" value="stellate-puberulent" value_original="stellate-puberulent" />
        <character constraint="with longer rays" constraintid="o4934" is_modifier="false" modifier="uniformly" name="quantity" src="d0_s11" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4934" name="ray" name_original="rays" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals pale pink-lavender, veins paler, 10–15 mm;</text>
      <biological_entity id="o4935" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale pink-lavender" value_original="pale pink-lavender" />
      </biological_entity>
      <biological_entity id="o4936" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="paler" value_original="paler" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminal column 4–5 mm, hairy;</text>
      <biological_entity constraint="staminal" id="o4937" name="column" name_original="column" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers white;</text>
      <biological_entity id="o4938" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 5 or 6.</text>
      <biological_entity id="o4939" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="5" value_original="5" />
        <character name="quantity" src="d0_s15" unit="or" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Schizocarps 5 mm diam.;</text>
      <biological_entity id="o4940" name="schizocarp" name_original="schizocarps" src="d0_s16" type="structure">
        <character name="diameter" src="d0_s16" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>mericarps 5 or 6, 2.5 mm, sparsely glandularpuberulent, roughened, back reticulate-veined, sides strongly so, not pitted, mucro 0.1–0.3 mm.</text>
      <biological_entity id="o4941" name="mericarp" name_original="mericarps" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="5" value_original="5" />
        <character name="some_measurement" src="d0_s17" value="6 mm" value_original="6 mm" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" modifier="sparsely" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
        <character is_modifier="false" modifier="back" name="architecture" src="d0_s17" value="reticulate-veined" value_original="reticulate-veined" />
      </biological_entity>
      <biological_entity id="o4942" name="side" name_original="sides" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s17" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o4943" name="mucro" name_original="mucro" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1 mm. 2n = 20.</text>
      <biological_entity id="o4944" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="some_measurement" src="d0_s18" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4945" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline flats, springs, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sidalcea covillei is one of two species of Sidalcea (along with S. neomexicana) with fleshy roots and adapted to alkaline conditions on flats. Its range (Owens Valley in Inyo County) and specialized habitat have made it vulnerable to any lowering of the water table and to grazing; it is listed as endangered in California. Most individuals of it were destroyed by construction of the Haiwee Reservoir. Once thought to have been extirpated, it was subsequently rediscovered.</discussion>
  
</bio:treatment>