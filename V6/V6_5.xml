<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
    <other_info_on_meta type="illustration_page">9</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">momordica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">charantia</taxon_name>
    <taxon_name authority="[F I]" date="unknown" rank="subspecies">charantia</taxon_name>
    <taxon_hierarchy>family cucurbitaceae;genus momordica;species charantia;subspecies charantia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100801</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Momordica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">charantia</taxon_name>
    <taxon_name authority="Seringe" date="unknown" rank="variety">abbreviata</taxon_name>
    <taxon_hierarchy>genus Momordica;species charantia;variety abbreviata</taxon_hierarchy>
  </taxon_identification>
  <number>2a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous or hairy.</text>
      <biological_entity id="o20982" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1.5–3.5 (–6) cm;</text>
      <biological_entity id="o20983" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o20984" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade suborbiculate to orbiculate, deeply palmately 5–7-lobed, 5–10 (–12) cm, base cordate, lobes ovate-oblong or ovate-elliptic, sinuses 50–90% to base, margins coarsely and widely dentate to crenate-dentate, leaf lobes and teeth sometimes submucronate, not apiculate, surfaces glabrous or sparsely hairy.</text>
      <biological_entity id="o20985" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20986" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s2" to="orbiculate" />
        <character is_modifier="false" modifier="deeply palmately" name="shape" src="d0_s2" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20987" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o20988" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
      </biological_entity>
      <biological_entity id="o20989" name="sinuse" name_original="sinuses" src="d0_s2" type="structure" />
      <biological_entity id="o20990" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o20991" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="widely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s2" value="dentate to crenate-dentate" value_original="dentate to crenate-dentate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o20992" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="submucronate" value_original="submucronate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o20993" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="submucronate" value_original="submucronate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o20994" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o20989" id="r2239" modifier="50-90%" name="to" negation="false" src="d0_s2" to="o20990" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: staminate peduncles bracteate at or proximal to middle, bracts sessile, reniform or orbiculate-cordate, margins entire;</text>
      <biological_entity id="o20995" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o20996" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s3" to="middle" />
      </biological_entity>
      <biological_entity id="o20997" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate-cordate" value_original="orbiculate-cordate" />
      </biological_entity>
      <biological_entity id="o20998" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate peduncles ebracteate.</text>
      <biological_entity id="o20999" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o21000" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Petals bright-yellow, obovate to oblong-obovate, 7–25 mm.</text>
      <biological_entity id="o21001" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblong-obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits red or orange to yellow-orange, cylindric or ellipsoidal to oblong-fusiform or ovoid, 7–25 cm, beak becoming less prominent at maturity, surface usually minutely tuberculate to muricate and muriculate, sometimes irregularly smooth-ridged.</text>
      <biological_entity id="o21002" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s6" to="yellow-orange" />
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s6" to="oblong-fusiform or ovoid" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21003" name="beak" name_original="beak" src="d0_s6" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="becoming less" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o21004" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually minutely tuberculate" name="relief" src="d0_s6" to="muricate" />
        <character is_modifier="false" name="relief" src="d0_s6" value="muriculate" value_original="muriculate" />
        <character is_modifier="false" modifier="sometimes irregularly" name="shape" src="d0_s6" value="smooth-ridged" value_original="smooth-ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds oblong, 10–15 mm. 2n = 22.</text>
      <biological_entity id="o21005" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21006" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides, fencerows, thickets, hammock edges, fields, ditches, canal banks, shell mounds, orange groves, sand pine scrub, flatwoods, floodplain woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="hammock edges" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="canal banks" />
        <character name="habitat" value="shell mounds" />
        <character name="habitat" value="orange groves" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="scrub" modifier="pine" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="floodplain woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Conn., Fla., Ga., La., Pa.; Africa; introduced also in Mexico, West Indies, South America, Europe, se Asia, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="se Asia" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>No documentation has been found for a report of Momordica charantia in Texas. The species is apparently becoming common in Florida, where it is known to be naturalized in many counties. It is grown as a crop in many countries for the young fruits, which are edible before an extreme bitterness develops in the mature fruits.</discussion>
  <discussion>Two forms of Momordica charantia in the New World can be distinguished: the ubiquitous weed of the humid lowland tropics and subtropics, growing spontaneously and not cultivated or eaten except occasionally and opportunistically for the pulp; and the domesticated forms, which are seen in Asian markets. The cultivated forms do not escape (M. Nee, pers. comm.).</discussion>
  <discussion>Momordica charantia subsp. macroloba Achigan-Dako &amp; Blattner is endemic to the Dahomey Gap and Sudano-Guinean phytoregion of Benin and Togo in west-central Africa.</discussion>
  
</bio:treatment>