<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="(Linnaeus) Webb &amp; Berthelot" date="unknown" rank="species">arborea</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Nat. Îles Canaries</publication_title>
      <place_in_publication>3(2,1): 30. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species arborea;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101088</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lavatera</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arborea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 690. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lavatera;species arborea</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Tree mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, biennial or perennial, or subshrubs, 1–3 m, stellate-tomentose.</text>
      <biological_entity id="o23006" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" notes="" src="d0_s0" to="3" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stellate-tomentose" value_original="stellate-tomentose" />
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, base usually woody.</text>
      <biological_entity id="o23008" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o23009" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules deciduous, ovate, 4–5 × 2–3 mm, papery, apex acute to obtuse, sparsely stellate-hairy and ciliate;</text>
      <biological_entity id="o23010" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23011" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o23012" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole longer than blade;</text>
      <biological_entity id="o23013" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23014" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="than blade" constraintid="o23015" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o23015" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade rounded, shallowly and unequally 5–7 (–9) -lobed (lobes obtuse), 5–20 × 5–20 cm, base cordate, margins crenate, apex obtuse to rounded, surfaces densely soft stellate-hairy especially abaxially.</text>
      <biological_entity id="o23016" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23017" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly; unequally" name="shape" src="d0_s4" value="5-7(-9)-lobed" value_original="5-7(-9)-lobed" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23018" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o23019" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o23020" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o23021" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="especially abaxially; abaxially" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers in fascicles.</text>
      <biological_entity id="o23022" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o23023" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels jointed distally, 0.5–1 cm, not much longer in fruit;</text>
      <biological_entity id="o23025" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets connate in proximal 1/3, adnate to calyx, lobes broadly ovate to round, 8 × 5–6 mm, longer than calyx, margins entire, apex acute or obtuse, surfaces stellate-hairy.</text>
      <biological_entity id="o23024" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character constraint="in fruit" constraintid="o23025" is_modifier="false" modifier="not much" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o23026" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character constraint="in proximal 1/3" constraintid="o23027" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character constraint="to calyx" constraintid="o23028" is_modifier="false" name="fusion" notes="" src="d0_s7" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23027" name="1/3" name_original="1/3" src="d0_s7" type="structure" />
      <biological_entity id="o23028" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o23029" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s7" to="round" />
        <character name="length" src="d0_s7" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="than calyx" constraintid="o23030" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o23030" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o23031" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23032" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o23033" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 3–4 mm, not much enlarged in fruit, densely stellate-canescent;</text>
      <biological_entity id="o23034" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23035" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character constraint="in fruit" constraintid="o23036" is_modifier="false" modifier="not much" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s8" value="stellate-canescent" value_original="stellate-canescent" />
      </biological_entity>
      <biological_entity id="o23036" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals rose to lavender with 5 darker veins, dark purple basally, 15–20 mm, length 4–5 times calyx, apex emarginate;</text>
      <biological_entity id="o23037" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23038" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="with veins" constraintid="o23039" from="rose" name="coloration" src="d0_s9" to="lavender" />
        <character is_modifier="false" modifier="basally" name="coloration" notes="" src="d0_s9" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character constraint="calyx" constraintid="o23040" is_modifier="false" name="length" src="d0_s9" value="4-5 times calyx" value_original="4-5 times calyx" />
      </biological_entity>
      <biological_entity id="o23039" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o23040" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o23041" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminal column 8–10 mm, glabrous proximally, stellate-hairy distally;</text>
      <biological_entity id="o23042" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o23043" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers purplish;</text>
      <biological_entity id="o23044" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23045" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style (6–) 8 (or 9) -branched;</text>
      <biological_entity id="o23046" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23047" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="(6-)8-branched" value_original="(6-)8-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas (6–) 8 (or 9).</text>
      <biological_entity id="o23048" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23049" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s13" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Schizocarps 8–10 mm diam.;</text>
      <biological_entity id="o23050" name="schizocarp" name_original="schizocarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>mericarps (6–) 8 (or 9), 4–5 mm, margins sharp-angled, apical surface and sides ridged, surfaces glabrous or hairy.</text>
      <biological_entity id="o23051" name="mericarp" name_original="mericarps" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s15" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23052" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="sharp-angled" value_original="sharp-angled" />
      </biological_entity>
      <biological_entity constraint="apical" id="o23053" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o23054" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity id="o23055" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, 3 mm. 2n = 36, 40, 42, 44.</text>
      <biological_entity id="o23056" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23057" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
        <character name="quantity" src="d0_s16" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, coastal bluffs, dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; Europe; introduced also in Mexico (Baja California), Africa (Libya), Atlantic Islands (Canary Islands); Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Mexico (Baja California)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Libya)" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva arborea is infrequently cultivated as a garden ornamental. It is traditionally placed in Lavatera and has three prominent, spreading, rounded, earlike involucellar bractlets and inconspicuous sepals.</discussion>
  
</bio:treatment>