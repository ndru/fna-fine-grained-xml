<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">asprella</taxon_name>
    <taxon_name authority="(Jepson) S. R. Hill" date="unknown" rank="subspecies">nana</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>56: 105. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species asprella;subspecies nana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101112</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">reptans</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">nana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>2: 489. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sidalcea;species reptans;variety nana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="unknown" rank="species">malviflora</taxon_name>
    <taxon_name authority="(Jepson) C. L. Hitchcock" date="unknown" rank="subspecies">nana</taxon_name>
    <taxon_hierarchy>genus S.;species malviflora;subspecies nana</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <other_name type="common_name">Dwarf harsh checkerbloom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–0.3 (–0.4) m, with caudex or not, with rhizomes freely rooting, 5–20 cm × 2 (–3) mm.</text>
      <biological_entity id="o28" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.4" to_unit="m" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.3" to_unit="m" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s0" to="3" to_unit="mm" />
        <character name="width" notes="" src="d0_s0" unit="mm" value="2" value_original="2" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o30" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o31" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o32" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <relation from="o28" id="r5" name="with" negation="false" src="d0_s0" to="o29" />
      <relation from="o28" id="r6" name="with" negation="false" src="d0_s0" to="o30" />
      <relation from="o30" id="r7" name="with" negation="false" src="d0_s0" to="o31" />
      <relation from="o30" id="r8" name="with" negation="false" src="d0_s0" to="o32" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent-ascending to erect, sometimes proximally prostrate, rooting, not brittle, densely, harshly stellate-puberulent to glabrate, hairs 0.1–0.5 (–0.7) mm, usually less dense distally.</text>
      <biological_entity id="o33" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent-ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="sometimes proximally" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character char_type="range_value" from="harshly stellate-puberulent" modifier="densely" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
      <biological_entity id="o34" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s1" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually less; distally" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal, cauline 1–3+, gradually reduced distally;</text>
      <biological_entity id="o35" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o36" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o37" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles of proximal leaves 5–10 cm, 1–4 times as long as blade, those of distal leaves 1/2 times to as long as blade;</text>
      <biological_entity id="o38" name="petiole" name_original="petioles" src="d0_s3" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character constraint="blade" constraintid="o40" is_modifier="false" name="length" src="d0_s3" value="1-4 times as long as blade" />
        <character constraint="blade" constraintid="o42" is_modifier="false" name="length" src="d0_s3" value="1/2 times to as long as blade" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o39" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o40" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o41" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o42" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o38" id="r9" name="part_of" negation="false" src="d0_s3" to="o39" />
      <relation from="o38" id="r10" name="part_of" negation="false" src="d0_s3" to="o41" />
    </statement>
    <statement id="d0_s4">
      <text>blades: basal usually palmately 7-lobed, or deeply crenate, 2.2–2.7 (–4) × 2–2.3 (–4) cm, base cordate or sinus wide to narrow, apex rounded, cauline deeply 3–7-lobed, 1.5–3 cm, lobes usually apically 3-toothed or distalmost entire, surfaces hairy, more densely stellate-puberulent adaxially, hairs usually 2–4-rayed (simple), bristly.</text>
      <biological_entity id="o43" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o44" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually palmately" name="shape" src="d0_s4" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="usually palmately" name="shape" src="d0_s4" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s4" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o45" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o46" name="sinus" name_original="sinus" src="d0_s4" type="structure">
        <character char_type="range_value" from="wide" name="width" src="d0_s4" to="narrow" />
      </biological_entity>
      <biological_entity id="o47" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o48" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="3-7-lobed" value_original="3-7-lobed" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o49" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually apically" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o50" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o51" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s4" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
      <biological_entity id="o52" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences ascending, usually unbranched, subscapose, usually 1-sided, pistillate usually 9–14-flowered, bisexual 2–9 (–19) -flowered;</text>
      <biological_entity id="o53" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subscapose" value_original="subscapose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="9-14-flowered" value_original="9-14-flowered" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-9(-19)-flowered" value_original="2-9(-19)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts linear to lanceolate, stipulelike, 2–2.5 (–4) mm, shorter than to as long as pedicel.</text>
      <biological_entity id="o54" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="stipule-like" value_original="stipulelike" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character constraint="than to as-long-as pedicel" constraintid="o55" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o55" name="pedicel" name_original="pedicel" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 3–4 mm.</text>
      <biological_entity id="o56" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 5–7 (–12) mm;</text>
      <biological_entity id="o57" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o58" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pink, pistillate 9–11 mm, bisexual 15–20 (–28) mm;</text>
      <biological_entity id="o59" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o60" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="28" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 7 or 8.</text>
      <biological_entity id="o61" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o62" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Schizocarps 7–8 mm diam.;</text>
      <biological_entity id="o63" name="schizocarp" name_original="schizocarps" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>mericarps 7 or 8, 4 mm, reticulate-rugose-veined, sides and back pitted, mucro 1 mm, with few minute bristles.</text>
      <biological_entity id="o64" name="mericarp" name_original="mericarps" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="7" value_original="7" />
        <character name="some_measurement" src="d0_s12" value="8 mm" value_original="8 mm" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="reticulate-rugose-veined" value_original="reticulate-rugose-veined" />
      </biological_entity>
      <biological_entity id="o65" name="side" name_original="sides" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="back" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o66" name="mucro" name_original="mucro" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o67" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s12" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o66" id="r11" name="with" negation="false" src="d0_s12" to="o67" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2.8 mm.</text>
      <biological_entity id="o68" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="2.8" value_original="2.8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woodlands, grassy margins, yellow pine-Douglas fir forests, usually serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="grassy margins" />
        <character name="habitat" value="yellow pine-douglas fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies nana has been confused with subsp. asprella, Sidalcea celata, S. elegans, and S. glaucescens and has been placed within S. malviflora. The type specimen (Jepson 14061) has small individuals with only two or three flowers and long rhizomes, and it does superficially resemble S. reptans, as Jepson suggested, but more robust individuals have more flowers and clearly show vegetative and reproductive similarity to subsp. asprella.</discussion>
  <discussion>Subspecies nana occurs from California in the northern Sierra Nevada to southwestern Oregon.</discussion>
  
</bio:treatment>