<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="unknown" rank="species">adunca</taxon_name>
    <taxon_name authority="(Greene) H. D. Harrington" date="unknown" rank="variety">bellidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Man. Pl. Colorado,</publication_title>
      <place_in_publication>641. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species adunca;variety bellidifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100896</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">bellidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 292. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species bellidifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, usually appearing small and tufted, 1.8–4.5 (–6.5) cm.</text>
      <biological_entity id="o15151" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s0" to="4.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blade ovate to ovate-orbiculate, 0.5–1.7 × 0.4–1.4 cm, base subcordate, truncate, or attenuate, apex usually obtuse, surfaces usually glabrous.</text>
      <biological_entity constraint="basal" id="o15152" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15153" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="ovate-orbiculate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s1" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s1" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15154" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o15155" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o15156" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: petiole 0.5–3.8 cm;</text>
      <biological_entity constraint="cauline" id="o15157" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15158" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 0.6–1.5 × 0.4–1.4 cm.</text>
      <biological_entity constraint="cauline" id="o15159" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15160" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1.7–5 cm, bracteoles usually opposite.</text>
      <biological_entity id="o15161" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15162" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepal margins usually eciliate;</text>
      <biological_entity id="o15163" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="sepal" id="o15164" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lowest petal 7–13 (–14) mm;</text>
      <biological_entity id="o15165" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="lowest" id="o15166" name="petal" name_original="petal" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style head sparsely bearded.</text>
      <biological_entity id="o15167" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="style" id="o15168" name="head" name_original="head" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1.5 mm. 2n = 20.</text>
      <biological_entity id="o15169" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15170" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine areas, wet meadows, lake margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine areas" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="lake margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500–3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Colo., Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety bellidifolia is found in the Rocky Mountains. Although forms transitional with var. adunca occur, the diminutive var. bellidifolia is quite distinct. V. B. Baird (1942) reported that Viola bellidifolia occurs in the Siskiyou Mountains of California; we have seen no supporting specimens.</discussion>
  
</bio:treatment>