<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Medikus" date="1787" rank="genus">WISSADULA</taxon_name>
    <place_of_publication>
      <publication_title>Malvenfam.,</publication_title>
      <place_in_publication>24. 1787</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus wissadula;</taxon_hierarchy>
    <other_info_on_name type="etymology">Presumably Sinhalese wissa, poison, and duvili, dust or powder; common name wissaduli used for plants of Centipeda minima (Linnaeus) A. Braun &amp; Ascherson and misapplied here</other_info_on_name>
    <other_info_on_name type="fna_id">134943</other_info_on_name>
  </taxon_identification>
  <number>52.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs [perennial herbs].</text>
      <biological_entity id="o7220" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, hairy [glabrate], not viscid.</text>
      <biological_entity id="o7221" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves distalmost sometimes subsessile;</text>
      <biological_entity id="o7222" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="distalmost" value_original="distalmost" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules usually persistent, filiform, subulate, or minute;</text>
      <biological_entity id="o7223" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="size" src="d0_s3" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate to ovate-triangular [narrowly triangular], unlobed, base cordate, margins entire [crenate-dentate], surfaces usually stellate-hairy [sometimes glabrate].</text>
      <biological_entity id="o7224" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o7225" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o7226" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7227" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal panicles or racemes;</text>
      <biological_entity id="o7228" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o7229" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o7230" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>involucel absent.</text>
      <biological_entity id="o7231" name="involucel" name_original="involucel" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx not accrescent, not inflated, shorter than mature fruits, lobes not ribbed, triangular;</text>
      <biological_entity id="o7232" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7233" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="than mature fruits" constraintid="o7234" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7234" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o7235" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla usually yellowish, sometimes white, rotate;</text>
      <biological_entity id="o7236" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7237" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rotate" value_original="rotate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminal column exserted;</text>
      <biological_entity id="o7238" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="staminal" id="o7239" name="column" name_original="column" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 3–6-branched;</text>
      <biological_entity id="o7240" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7241" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-6-branched" value_original="3-6-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas capitate.</text>
      <biological_entity id="o7242" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7243" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits schizocarps, erect, not inflated, obovoid, not indurate;</text>
      <biological_entity constraint="fruits" id="o7244" name="schizocarp" name_original="schizocarps" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mericarps 3–6, 2-celled, apex bulbous-apiculate, proximal cell indehiscent, distal cell dehiscent.</text>
      <biological_entity id="o7245" name="mericarp" name_original="mericarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-celled" value_original="2-celled" />
      </biological_entity>
      <biological_entity id="o7246" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="bulbous-apiculate" value_original="bulbous-apiculate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7247" name="cell" name_original="cell" src="d0_s13" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7248" name="cell" name_original="cell" src="d0_s13" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds (1–) 3 per mericarp, lower cell 1-seeded, upper cell usually 2-seeded, hairy, proximal seed relatively more densely hairy.</text>
      <biological_entity id="o7249" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s14" to="3" to_inclusive="false" />
        <character constraint="per mericarp" constraintid="o7250" name="quantity" src="d0_s14" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o7250" name="mericarp" name_original="mericarp" src="d0_s14" type="structure" />
      <biological_entity constraint="lower" id="o7251" name="cell" name_original="cell" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-seeded" value_original="1-seeded" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7252" name="cell" name_original="cell" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s14" value="2-seeded" value_original="2-seeded" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>x = 7.</text>
      <biological_entity constraint="proximal" id="o7253" name="seed" name_original="seed" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="x" id="o7254" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc United States, Mexico, West Indies, South America, s Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="s Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 25 (3 in the flora).</discussion>
  <references>
    <reference>Fries, R. E. 1908. Entwurf einer Monographie der Gattungen Wissadula und Pseudabutilon. Kongl. Svenska Vetensk. Acad. Handl., n. s. 43(4): 1–114.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 2.5–3.5 cm, apex acute to subobtuse; stipules minute; petals yellow, fading to orange.</description>
      <determination>2 Wissadula parvifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 4–11 cm, apex acute or acuminate; stipules 4–12 mm; petals usually yellowish or white, sometimes with dark red basal spot</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades broadly ovate, base deeply cordate (except uppermost), margins curved; petals yellowish; stipules 7–12 mm.</description>
      <determination>1 Wissadula hernandioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades ovate-triangular, base shallowly cordate, margins straight; petals yellowish or white, sometimes with dark red basal spot; stipules 4–5 mm.</description>
      <determination>3 Wissadula periplocifolia</determination>
    </key_statement>
  </key>
</bio:treatment>