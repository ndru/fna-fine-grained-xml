<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">abutilon</taxon_name>
    <taxon_name authority="(Lamarck) Sweet" date="unknown" rank="species">hirtum</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Brit.,</publication_title>
      <place_in_publication>53. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus abutilon;species hirtum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200013674</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">hirta</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>1: 7. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sida;species hirta</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 1 m.</text>
      <biological_entity id="o7802" name="whole_organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s0" unit="m" value="1" value_original="1" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, puberulent and with simple hairs 2–5 mm, viscid.</text>
      <biological_entity id="o7803" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="coating" notes="" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o7804" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o7803" id="r844" name="with" negation="false" src="d0_s1" to="o7804" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules recurved, lanceolate, 7–9 mm;</text>
      <biological_entity id="o7805" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7806" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole as long as or longer than blade;</text>
      <biological_entity id="o7807" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7808" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o7810" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7809" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o7808" id="r845" name="as long as" negation="false" src="d0_s3" to="o7810" />
    </statement>
    <statement id="d0_s4">
      <text>blade somewhat discolorous, ovate to suborbiculate, 5–7 cm, base cordate, margins finely serrate, apex acuminate, surfaces softly tomentose.</text>
      <biological_entity id="o7811" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7812" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="coloration" src="d0_s4" value="discolorous" value_original="discolorous" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7813" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o7814" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o7815" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o7816" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers or terminal panicles.</text>
      <biological_entity id="o7817" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o7818" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o7819" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 12–17 mm, lobes not overlapping, erect, ovate;</text>
      <biological_entity id="o7820" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7821" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7822" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla orange-yellow with dark red center, petals 18–20 mm;</text>
      <biological_entity id="o7823" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7824" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character constraint="with center" constraintid="o7825" is_modifier="false" name="coloration" src="d0_s7" value="orange-yellow" value_original="orange-yellow" />
      </biological_entity>
      <biological_entity id="o7825" name="center" name_original="center" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="dark red" value_original="dark red" />
      </biological_entity>
      <biological_entity id="o7826" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column pubescent;</text>
      <biological_entity id="o7827" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o7828" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 20–25-branched.</text>
      <biological_entity id="o7829" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7830" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="20-25-branched" value_original="20-25-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Schizocarps oblate, 12–14 × 20 mm;</text>
      <biological_entity id="o7831" name="schizocarp" name_original="schizocarps" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mericarps: apex obtuse to acute, surface stellate-hirsute.</text>
      <biological_entity id="o7832" name="mericarp" name_original="mericarps" src="d0_s11" type="structure" />
      <biological_entity id="o7833" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute" />
      </biological_entity>
      <biological_entity id="o7834" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hirsute" value_original="stellate-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 3 per mericarp, 2.4–2.8 mm, minutely scabridulous.</text>
      <biological_entity id="o7836" name="mericarp" name_original="mericarp" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 42.</text>
      <biological_entity id="o7835" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character constraint="per mericarp" constraintid="o7836" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="2.8" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7837" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Asia; Africa; Australia; introduced also in Mexico, West Indies (Cuba, Lesser Antilles, Puerto Rico), Central America, South America (Peru, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="introduced" />
        <character name="distribution" value="West Indies (Lesser Antilles)" establishment_means="introduced" />
        <character name="distribution" value="West Indies (Puerto Rico)" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America (Peru)" establishment_means="introduced" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Abutilon hirtum has been found in Alachua, Lake, Miami-Dade, and Monroe counties, primarily in the Florida Keys. It is native in tropical parts of Africa, Asia, and Australia.</discussion>
  
</bio:treatment>