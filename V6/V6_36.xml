<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">SICYOS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1013. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 443. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus SICYOS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek sikyos, cucumber or gourd</other_info_on_name>
    <other_info_on_name type="fna_id">130305</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Bur cucumber</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, monoecious, climbing or trailing;</text>
      <biological_entity id="o2219" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems glabrous or hairy, often viscid-pubescent when young;</text>
      <biological_entity id="o2220" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="viscid-pubescent" value_original="viscid-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots fibrous;</text>
      <biological_entity id="o2221" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tendrils 2–5-branched from a common point.</text>
      <biological_entity id="o2222" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character constraint="from point" constraintid="o2223" is_modifier="false" name="architecture" src="d0_s3" value="2-5-branched" value_original="2-5-branched" />
      </biological_entity>
      <biological_entity id="o2223" name="point" name_original="point" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="common" value_original="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves sessile or subsessile to petiolate;</text>
      <biological_entity id="o2224" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s4" to="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate or orbiculate to suborbiculate or reniform, deeply to shallowly palmately 3–5-angular-lobed, lobes triangular to deltate, margins usually serrate to denticulate, surfaces eglandular.</text>
      <biological_entity id="o2225" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="suborbiculate or reniform" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="suborbiculate or reniform" />
      </biological_entity>
      <biological_entity id="o2226" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="deltate" />
      </biological_entity>
      <biological_entity id="o2227" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually serrate" name="shape" src="d0_s5" to="denticulate" />
      </biological_entity>
      <biological_entity id="o2228" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: staminate flowers 3–22 (–34) in axillary racemes or panicles;</text>
      <biological_entity id="o2229" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o2230" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="22" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="34" />
        <character char_type="range_value" constraint="in panicles" constraintid="o2232" from="3" name="quantity" src="d0_s6" to="22" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o2231" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o2232" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers 4–16, sessile to subsessile in umbelliform clusters at peduncle apex, from same axils as staminate, peduncles erect at apex;</text>
      <biological_entity id="o2233" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o2234" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="16" />
        <character char_type="range_value" constraint="at peduncle apex" constraintid="o2235" from="sessile" name="architecture" src="d0_s7" to="subsessile" />
      </biological_entity>
      <biological_entity constraint="peduncle" id="o2235" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o2236" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <biological_entity id="o2237" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character constraint="at apex" constraintid="o2238" is_modifier="false" modifier="as staminate" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o2238" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o2234" id="r275" name="from" negation="false" src="d0_s7" to="o2236" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o2239" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o2240" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium cupulate to shallowly campanulate;</text>
      <biological_entity id="o2241" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2242" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="cupulate" name="shape" src="d0_s9" to="shallowly campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, ovate to deltate or subulate, linear, linear-triangular, or narrowly triangular;</text>
      <biological_entity id="o2243" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2244" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="deltate or subulate linear linear-triangular or narrowly triangular" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="deltate or subulate linear linear-triangular or narrowly triangular" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="deltate or subulate linear linear-triangular or narrowly triangular" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="deltate or subulate linear linear-triangular or narrowly triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, connate 1/4–1/2 length, white to greenish white, yellowish green, or yellow, triangular to lanceolate or narrowly lanceolate, 0.5–1.5 mm, glabrous abaxially, often glandular adaxially, corolla campanulate to cupulate.</text>
      <biological_entity id="o2245" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2246" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s11" to="1/2" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish white yellowish green or yellow" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish white yellowish green or yellow" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish white yellowish green or yellow" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s11" to="lanceolate or narrowly lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often; adaxially" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2247" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s11" to="cupulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: stamens (2–) 3 (–5);</text>
      <biological_entity id="o2248" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2249" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="5" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted at base of hypanthium, connate 1 mm;</text>
      <biological_entity id="o2250" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2251" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2252" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o2253" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure" />
      <relation from="o2251" id="r276" name="inserted at" negation="false" src="d0_s13" to="o2252" />
      <relation from="o2251" id="r277" name="part_of" negation="false" src="d0_s13" to="o2253" />
    </statement>
    <statement id="d0_s14">
      <text>thecae connate into head, horseshoe-shaped, connective slightly broadened;</text>
      <biological_entity id="o2254" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2255" name="theca" name_original="thecae" src="d0_s14" type="structure">
        <character constraint="into head" constraintid="o2256" is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="horseshoe--shaped" value_original="horseshoe--shaped" />
      </biological_entity>
      <biological_entity id="o2256" name="head" name_original="head" src="d0_s14" type="structure" />
      <biological_entity id="o2257" name="connective" name_original="connective" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s14" value="broadened" value_original="broadened" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistillodes absent.</text>
      <biological_entity id="o2258" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2259" name="pistillode" name_original="pistillodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary 1-locular, ovoid-fusiform;</text>
      <biological_entity id="o2260" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2261" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid-fusiform" value_original="ovoid-fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 1 per locule;</text>
      <biological_entity id="o2262" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2263" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character constraint="per locule" constraintid="o2264" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2264" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1, narrow;</text>
      <biological_entity id="o2265" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2266" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" name="size_or_width" src="d0_s18" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 1, obscurely 2–3-lobed;</text>
      <biological_entity id="o2267" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2268" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s19" value="2-3-lobed" value_original="2-3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>staminodes absent.</text>
      <biological_entity id="o2269" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2270" name="staminode" name_original="staminodes" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits pepos, dark green to gray at maturity, fusiform to ovoid, beaked or not, dry, thin-walled, echinate or spinulose, usually also with shorter hairs, rarely glabrous, indehiscent.</text>
      <biological_entity constraint="fruits" id="o2271" name="pepo" name_original="pepos" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="at maturity" from="dark green" name="coloration" src="d0_s21" to="gray" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s21" to="ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="beaked" value_original="beaked" />
        <character name="architecture_or_shape" src="d0_s21" value="not" value_original="not" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s21" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture" src="d0_s21" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="architecture" src="d0_s21" value="echinate" value_original="echinate" />
        <character is_modifier="false" name="architecture" src="d0_s21" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s21" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="dehiscence" src="d0_s21" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o2272" name="hair" name_original="hairs" src="d0_s21" type="structure" />
      <relation from="o2271" id="r278" modifier="usually" name="with" negation="false" src="d0_s21" to="o2272" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds 1, ovoid, compressed, not arillate, margins not differentiated but sometimes with 2 small swellings at base, surface smooth.</text>
      <biological_entity id="o2273" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o2274" name="margin" name_original="margins" src="d0_s22" type="structure">
        <character constraint="sometimes with swellings" constraintid="o2275" is_modifier="false" modifier="not" name="variability" src="d0_s22" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o2275" name="swelling" name_original="swellings" src="d0_s22" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s22" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s22" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o2276" name="base" name_original="base" src="d0_s22" type="structure" />
      <relation from="o2275" id="r279" name="at" negation="false" src="d0_s22" to="o2276" />
    </statement>
    <statement id="d0_s23">
      <text>x = 12.</text>
      <biological_entity id="o2277" name="surface" name_original="surface" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s22" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o2278" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Pacific Islands, Australia; introduced in Europe, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 50 (4 in the flora).</discussion>
  <discussion>Attribution of Sicyos parviflorus A. Gray ex Naudin to the United States has been based on misapplication of the name. The type of S. parviflorus was collected from the vicinity of Mexico City, and the species occurs from central Mexico through Central America into South America (R. Lira 2001).</discussion>
  <references>
    <reference>Nesom, G. L. 2011. Taxonomy of Sicyos (Cucurbitaceae) in the USA. Phytoneuron 2011-15: 1–11.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pepos ovoid-beaked, 9–15 mm; stigmas 3-lobed; e, c North America, including Texas.</description>
      <determination>1 Sicyos angulatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pepos ovoid, 4–8 mm; stigmas 2- or 3-lobed; Arizona, New Mexico, Texas</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pepos not echinate.</description>
      <determination>2 Sicyos glaber</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pepos echinate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate inflorescences 10–16-flowered, peduncle plus floral axis 40–140 mm; stigmas 3-lobed; mature stems glabrate to sparsely minutely stipitate-glandular; leaf blades deeply lobed, sinuses (1/3–)1/2–2/3 to base, margins not dentate, evenly sharply indurate-apiculate, proximal pair of lateral veins divergent from edge of basal sinus.</description>
      <determination>3 Sicyos microphyllus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate inflorescences 3–10-flowered, peduncle plus floral axis 3–25 mm; stigmas 2-lobed; mature stems glabrescent but remaining villous and stipitate-glandular; leaf blades shallowly lobed to angulate, sinuses 1/5–1/4 to base, margins evenly and shallowly dentate, teeth sharply indurate-apiculate, proximal pair of lateral veins closely bordering edge of basal sinus.</description>
      <determination>4 Sicyos laciniatus</determination>
    </key_statement>
  </key>
</bio:treatment>