<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David E. Lemke</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LECHEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 90. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 40. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus LECHEA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Johan Leche, 1704 – 1764, Swedish botanist</other_info_on_name>
    <other_info_on_name type="fna_id">117809</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Spach" date="unknown" rank="genus">Lechidium</taxon_name>
    <taxon_hierarchy>genus Lechidium</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Pinweed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial or biennial, or subshrubs, (0.7–) 1.5–9 dm, sericeous, villous, or rarely glabrous;</text>
      <biological_entity id="o13148" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s0" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="9" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems dimorphic: basal stems produced late in growing season, prostrate to procumbent or ascending, overwintering;</text>
      <biological_entity id="o13150" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13151" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="growth_form" src="d0_s1" to="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="duration" src="d0_s1" value="overwintering" value_original="overwintering" />
      </biological_entity>
      <biological_entity id="o13152" name="season" name_original="season" src="d0_s1" type="structure" />
      <relation from="o13151" id="r1424" name="produced late in" negation="false" src="d0_s1" to="o13152" />
    </statement>
    <statement id="d0_s2">
      <text>flowering-stems produced in spring, erect to spreading-ascending.</text>
      <biological_entity id="o13153" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o13154" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" modifier="in spring" name="orientation" src="d0_s2" to="spreading-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, subopposite, opposite, or whorled, petiolate or sessile;</text>
      <biological_entity id="o13155" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined from base, linear to elliptic or ovate, oblanceolate, or orbiculate, margins revolute.</text>
      <biological_entity id="o13156" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="from base" constraintid="o13157" is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
      <biological_entity id="o13157" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o13158" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences paniculate or racemose, loose or congested, axils 1–3-flowered, bracteolate.</text>
      <biological_entity id="o13159" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o13160" name="axil" name_original="axils" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels present.</text>
      <biological_entity id="o13161" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: chasmogamous;</text>
      <biological_entity id="o13162" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5, outer 2 linear to linearlanceolate, inner 3 ovate to obovate, sometimes indurate in fruit;</text>
      <biological_entity id="o13163" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13164" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13165" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character constraint="to inner sepals" constraintid="o13166" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="obovate" />
        <character constraint="in fruit" constraintid="o13167" is_modifier="false" modifier="sometimes" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13166" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o13167" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals 3, maroon or green, usually shorter than sepals;</text>
      <biological_entity id="o13168" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13169" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character constraint="than sepals" constraintid="o13170" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o13170" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens (3–) 5–15 (–25);</text>
      <biological_entity id="o13171" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13172" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="25" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels 3;</text>
      <biological_entity id="o13173" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13174" name="carpel" name_original="carpels" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 0–0.5 mm;</text>
      <biological_entity id="o13175" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13176" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 3, dark red, fimbriate-plumose, sometimes persistent in fruit.</text>
      <biological_entity id="o13177" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o13178" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="fimbriate-plumose" value_original="fimbriate-plumose" />
        <character constraint="in fruit" constraintid="o13179" is_modifier="false" modifier="sometimes" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o13179" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules 3-valved, exserted beyond calyx or not.</text>
      <biological_entity id="o13180" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o13181" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–6 per capsule, ovoid, often compressed laterally, with or without easily separating membranous coat.</text>
      <biological_entity id="o13183" name="capsule" name_original="capsule" src="d0_s15" type="structure" />
      <biological_entity id="o13184" name="coat" name_original="coat" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="easily" name="arrangement" src="d0_s15" value="separating" value_original="separating" />
        <character is_modifier="true" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o13182" id="r1425" name="without" negation="false" src="d0_s15" to="o13184" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9 or unknown.</text>
      <biological_entity id="o13182" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per capsule" constraintid="o13183" from="1" name="quantity" src="d0_s15" to="6" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="often; laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="x" id="o13185" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit=",unknown" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies (Cuba), Central America (Belize).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The flowers of Lechea species are at anthesis only early in the day and most collections comprise fruiting individuals. In the key here, specimens lacking fruit and seeds can be difficult to determine with certainty. Regional treatments (for example, K. Barringer 2004; D. D. Spaulding 2013) can be very useful for identification.</discussion>
  <discussion>Species 18 (16 in the flora).</discussion>
  <references>
    <reference>Barringer, K. 2004. New Jersey pinweeds (Lechea, Cistaceae). J. Torrey Bot. Soc. 131: 261–276.</reference>
    <reference>Britton, N. L. 1894. A revision of the genus Lechea. Bull. Torrey Bot. Club 21: 244–253.</reference>
    <reference>Hodgdon, A. R. 1938. A taxonomic study of Lechea. Rhodora 40: 29–69, 87–131.</reference>
    <reference>Spaulding, D. D. 2013. Key to the pinweeds (Lechea, Cistaceae) of Alabama and adjacent states. Phytoneuron 2013-99: 1–15.</reference>
    <reference>Wilbur, R. L. 1966. Notes on Rafinesque’s species of Lechea. Rhodora 68: 192–208.</reference>
    <reference>Wilbur, R. L. and H. S. Daoud. 1961. The genus Lechea (Cistaceae) in the southeastern United States. Rhodora 63: 103–118.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems spreading-villous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal stems not produced; leaves of flowering stems: blade 5–8 mm; capsules ellipsoid, distinctly longer than calyx.</description>
      <determination>3 Lechea divaricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal stems produced; leaves of flowering stems: blade 10–30 mm; capsules subglobose, equaling or slightly longer than calyx.</description>
      <determination>9 Lechea mucronata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pedicels 3–5 mm; seeds (4–)6.</description>
      <determination>12 Lechea san-sabeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pedicels 1–2 mm; calyx indurate, shiny, yellow-brown basally in fruit.</description>
      <determination>11 Lechea racemulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Capsules ovoid to broadly ovoid, ± equaling calyx</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves of flowering stems whorled or opposite, blade 4–7 mm wide.</description>
      <determination>8 Lechea minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves of flowering stems alternate or subopposite, blade 0.3–1.5 mm wide.</description>
      <determination>13 Lechea sessiliflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Seeds 1(–2); capsules 1.2–1.3 mm diam.</description>
      <determination>7 Lechea mensalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Seeds 2–3(–5); capsules 1.3–1.5 mm diam.</description>
      <determination>15 Lechea tenuifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems and leaves glabrous.</description>
      <determination>5 Lechea lakelae</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems and leaves hairy, at least in part</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades densely hairy on both surfaces; pedicels 2–3 per axil.</description>
      <determination>1 Lechea cernua</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades glabrous or sparsely hairy adaxially, hairy abaxially at least on midvein and margins; pedicels 1 per axil</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Capsules ellipsoid to ovoid or obovoid, length mostly 2+ times diam</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Stigmas not persistent on capsule; calyx indurate, shiny, yellow-brown basally in fruit.</description>
      <determination>11 Lechea racemulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Stigmas persistent on capsule; calyx not indurate, shiny, yellow-brown basally in fruit.</description>
      <determination>13 Lechea sessiliflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Subshrubs; seeds 1(–2).</description>
      <determination>2 Lechea deckertii</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Herbs; seeds 2–6</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Capsules longer than calyx.</description>
      <determination>10 Lechea pulchella</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Capsules shorter than or ± equaling calyx</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Seeds with membranous gray coat.</description>
      <determination>4 Lechea intermedia</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Seeds without membranous gray coat</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Capsules mostly 2+ mm diam.; c Canada, midwestern United States.</description>
      <determination>14 Lechea stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Capsules mostly to 2 mm diam.; coastal plain</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves of flowering stems: blade narrowly elliptic to oblanceolate, 1.2–4 mm wide; capsules 1.5–1.9 mm diam.; n coastal plain south to ne North Carolina.</description>
      <determination>6 Lechea maritima</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves of flowering stems: blade linear to narrowly elliptic, 1–1.5 mm diam.; s coastal plain north to se North Carolina.</description>
      <determination>16 Lechea torreyi</determination>
    </key_statement>
  </key>
</bio:treatment>