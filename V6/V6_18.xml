<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">16</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Schrader" date="1831" rank="genus">cyclanthera</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Arnott" date="unknown" rank="species">dissecta</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>3: 280. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus cyclanthera;species dissecta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100802</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Discanthera</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">dissecta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 697. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Discanthera;species dissecta</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Cut-leaf or Texas coastal plain cyclanthera</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous except for minutely villosulous nodes;</text>
      <biological_entity id="o23576" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="except-for nodes" constraintid="o23577" is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23577" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s0" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>tendrils 2-branched, rarely (shortest tendrils) unbranched.</text>
      <biological_entity id="o23578" name="tendril" name_original="tendrils" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="2-branched" value_original="2-branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3-foliolate, lateral pair of leaflets deeply divided, petiolules 3–5 mm, terminal leaflet 3–4.5 cm, blade broadly lanceolate, petiolule 7–15 mm, linear, abruptly broadening into leaflet base, leaflet margins coarsely serrate to shallowly or deeply lobed;</text>
      <biological_entity id="o23579" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character constraint="of leaflets" constraintid="o23580" is_modifier="false" name="position" src="d0_s2" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o23580" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o23581" name="petiolule" name_original="petiolules" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o23582" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23583" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o23584" name="petiolule" name_original="petiolule" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character constraint="into leaflet base" constraintid="o23585" is_modifier="false" modifier="abruptly" name="size" src="d0_s2" value="broadening" value_original="broadening" />
      </biological_entity>
      <biological_entity constraint="leaflet" id="o23585" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="leaflet" id="o23586" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="coarsely serrate" name="shape" src="d0_s2" to="shallowly or deeply lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 10–21 mm.</text>
      <biological_entity id="o23587" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate inflorescences 5–11 cm, floriferous portion 3–7 cm, (50–) 65–90 (–130) -flowered, paniculate-racemoid, lateral branches 3–25 mm, longest proximally;</text>
      <biological_entity id="o23588" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23589" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="floriferous" value_original="floriferous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="(50-)65-90(-130)-flowered" value_original="(50-)65-90(-130)-flowered" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23590" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="length" src="d0_s4" value="longest" value_original="longest" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers solitary, in fascicles of 2–4, or along short axes.</text>
      <biological_entity id="o23591" name="flower" name_original="flowers" src="d0_s5" type="structure" constraint="axis" constraint_original="axis; axis">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o23592" name="axis" name_original="axes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <relation from="o23591" id="r2491" modifier="in fascicles" name="part_of" negation="false" src="d0_s5" to="o23592" />
    </statement>
    <statement id="d0_s6">
      <text>Staminate corollas 3.5–4.9 mm diam.</text>
      <biological_entity id="o23593" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s6" to="4.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Anther heads 0.6–0.8 mm diam., subsessile, glabrous.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting peduncles 4–6 mm.</text>
      <biological_entity constraint="anther" id="o23594" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s7" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23595" name="peduncle" name_original="peduncles" src="d0_s8" type="structure" />
      <relation from="o23594" id="r2492" name="fruiting" negation="false" src="d0_s8" to="o23595" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules narrowly ovoid, slightly oblique, short-beaked, 15–25 mm, spinules 3–4 mm.</text>
      <biological_entity id="o23596" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="slightly" name="orientation_or_shape" src="d0_s9" value="oblique" value_original="oblique" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="short-beaked" value_original="short-beaked" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23597" name="spinule" name_original="spinules" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riparian woods and thickets, bottomlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="bottomlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–70 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="70" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cyclanthera dissecta is a rarely collected endemic of the southeastern Texas coastal plain (G. L. Nesom 2014), known only from five counties (Austin, Brazoria, Brazos, Grimes, Victoria). It differs from the plants of central Texas (previously identified as C. dissecta, here as C. naudiniana) in its smaller staminate flowers and anthers, short fruiting peduncles, and especially in its paniculate-racemoid staminate inflorescences with an abounding number of flowers.</discussion>
  
</bio:treatment>