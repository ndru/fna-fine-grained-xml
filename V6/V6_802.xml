<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">droseraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">drosera</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">rotundifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 281. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family droseraceae;genus drosera;species rotundifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">200009769</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Roundleaf sundew</other_name>
  <other_name type="common_name">droséra à feuilles rondes</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming winter hibernaculae, rosettes (2–) 4–10 (–15) cm diam.;</text>
      <biological_entity id="o11367" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11368" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s0" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stem base not bulbous-cormose.</text>
      <biological_entity constraint="stem" id="o11369" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbous-cormose" value_original="bulbous-cormose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect to prostrate;</text>
      <biological_entity id="o11370" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules adnate to petioles their entire length, 4–6 mm, margins fimbriate along distal 1/2;</text>
      <biological_entity id="o11371" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character constraint="to petioles" constraintid="o11372" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11372" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11373" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="along distal 1/2" constraintid="o11374" is_modifier="false" name="shape" src="d0_s3" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11374" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole differentiated from blade, 1.5–5 cm, glandular-pilose;</text>
      <biological_entity id="o11375" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="from blade" constraintid="o11376" is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pilose" value_original="glandular-pilose" />
      </biological_entity>
      <biological_entity id="o11376" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade suborbiculate, 0.4–1 cm × 5–12 mm, broader than long, much shorter than petiole.</text>
      <biological_entity id="o11377" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
        <character constraint="broader than long , much shorter than petiole" constraintid="o11378" is_modifier="false" name="width" src="d0_s5" value="broader than long , much shorter than petiole" />
      </biological_entity>
      <biological_entity id="o11379" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11378" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–15 (–25) -flowered;</text>
      <biological_entity id="o11380" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-15(-25)-flowered" value_original="2-15(-25)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scapes 5–35 cm, glabrous.</text>
      <biological_entity id="o11381" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="35" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 4–7 mm diam.;</text>
      <biological_entity id="o11382" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals connate basally, oblong, 4–5 × 1.5–2 mm, glabrous;</text>
      <biological_entity id="o11383" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white or pink, spatulate, 5–6 × 3 mm.</text>
      <biological_entity id="o11384" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 5 mm.</text>
      <biological_entity id="o11385" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds light-brown, fusiform, 1–1.5 mm, finely and regularly longitudinally striate, with metallic sheen.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20.</text>
      <biological_entity id="o11386" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="finely; regularly longitudinally" name="coloration_or_pubescence_or_relief" src="d0_s12" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11387" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sphagnum bogs, fens, beaver ponds, swamps, peaty gravels, sandy soil, wet sand (for example, disturbed bottoms of old sand pits, emergent sandy shorelines) in the North, lake and stream margins, sphagnous streamheads, and seeps in the South</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="beaver ponds" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="peaty gravels" />
        <character name="habitat" value="sandy soil" />
        <character name="habitat" value="wet sand" />
        <character name="habitat" value="example" />
        <character name="habitat" value="disturbed bottoms" constraint="of old sand pits" />
        <character name="habitat" value="old sand pits" />
        <character name="habitat" value="the north" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="sphagnous streamheads" />
        <character name="habitat" value="seeps" modifier="and" constraint="in the south" />
        <character name="habitat" value="the south" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Colo., Conn., Del., D.C., Ga., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., Mont., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis.; Eurasia; Pacific Islands (New Guinea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Guinea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Drosera rotundifolia was the carnivorous plant most studied by C. Darwin (1875). The species is circumboreal and is widespread across North America, much more common northward and rarer in the South. It is difficult to grow in warmer climates.</discussion>
  <discussion>M. L. Fernald (1950) recognized forma breviscapa (Regel) Domin, found in the Canadian Maritime Provinces, with scapes 1–4 cm long and 1–3 flowers, and var. comosa Fernald, found from Gaspé County, Quebec, to New England and northern New York, with the parts of the flowers modified to green gland-bearing leaves. Dwarf, few-leaved plants found in Alaska have been called var. gracilis Laested.</discussion>
  
</bio:treatment>