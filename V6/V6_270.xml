<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(M. S. Baker &amp; J. C. Clausen) J. T. Howell" date="unknown" rank="variety">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Mentzelia</publication_title>
      <place_in_publication>1: 8. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species purpurea;variety mohavensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100954</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">aurea</taxon_name>
    <taxon_name authority="M. S. Baker &amp; J. C. Clausen" date="unknown" rank="subspecies">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>12: 9, fig. 1. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species aurea;subspecies mohavensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(M. S. Baker &amp; J. C. Clausen) J. C. Clausen" date="unknown" rank="subspecies">mohavensis</taxon_name>
    <taxon_hierarchy>genus V.;species purpurea;subspecies mohavensis</taxon_hierarchy>
  </taxon_identification>
  <number>51f.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–24 cm.</text>
      <biological_entity id="o10679" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="24" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, usually not buried, usually elongated by end of season, ± glabrous or usually puberulent.</text>
      <biological_entity id="o10680" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually not" name="location" src="d0_s1" value="buried" value_original="buried" />
        <character constraint="by end" constraintid="o10681" is_modifier="false" modifier="usually" name="length" src="d0_s1" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o10681" name="end" name_original="end" src="d0_s1" type="structure" />
      <biological_entity id="o10682" name="season" name_original="season" src="d0_s1" type="structure" />
      <relation from="o10681" id="r1173" name="part_of" negation="false" src="d0_s1" to="o10682" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal: 1–5;</text>
      <biological_entity id="o10683" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o10684" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 4.5–14.5 cm, puberulent;</text>
      <biological_entity id="o10685" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10686" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s3" to="14.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade gray-green to purple-tinted abaxially, green or gray-green adaxially, ovate, ± orbiculate, or ± deltate, 1–4 × 1–3.5 cm, base usually attenuate, margins dentate-serrate with 4 or 5 (6) pointed or rounded teeth per side, apex obtuse, abaxial surface glabrous or puberulent, adaxial surface glabrous or finely puberulent, not shiny;</text>
      <biological_entity id="o10687" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10688" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="gray-green" modifier="adaxially" name="coloration" src="d0_s4" to="purple-tinted abaxially" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10689" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10690" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="with " constraintid="o10691" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate-serrate" value_original="dentate-serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pointed" value_original="pointed" />
        <character constraint="with " constraintid="o10692" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o10691" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character name="atypical_quantity" src="d0_s4" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o10692" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10693" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o10694" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10695" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10696" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
      </biological_entity>
      <relation from="o10692" id="r1174" name="per" negation="false" src="d0_s4" to="o10693" />
    </statement>
    <statement id="d0_s5">
      <text>cauline: petiole 0.8–11 cm, glabrous or puberulent;</text>
      <biological_entity constraint="cauline" id="o10697" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o10698" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate, elliptic, or lanceolate, 1.5–3.7 × 0.5–2.5 cm, length 1–3 times width, base attenuate, margins with 3 or 4 (5) pointed or rounded teeth per side, apex acute, abaxial surface puberulent, adaxial surface usually glabrous, sometimes puberulent.</text>
      <biological_entity constraint="cauline" id="o10699" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o10700" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1-3" value_original="1-3" />
      </biological_entity>
      <biological_entity id="o10701" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10702" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pointed" value_original="pointed" />
        <character constraint="with " constraintid="o10704" is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o10703" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="3" value_original="3" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character name="atypical_quantity" src="d0_s6" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o10704" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o10705" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o10706" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10707" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10708" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o10702" id="r1175" name="with" negation="false" src="d0_s6" to="o10703" />
      <relation from="o10704" id="r1176" name="per" negation="false" src="d0_s6" to="o10705" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 1.7–14 cm, glabrous or usually puberulent.</text>
      <biological_entity id="o10709" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s7" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Lowest petal 10–14 mm.</text>
      <biological_entity constraint="lowest" id="o10710" name="petal" name_original="petal" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 5–7 mm.</text>
      <biological_entity id="o10711" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds light-brown, 2.7–3.1 mm.</text>
      <biological_entity id="o10712" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s10" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, sagebrush, dry areas in yellow pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="dry areas" constraint="in yellow pine forests" />
        <character name="habitat" value="yellow pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety mohavensis is variable; cauline leaf margins in some populations are similar to basal blades. The basal leaves of plants from Long Valley, Mono County, California, are more or less truncate or subcordate. In 1953, M. S. Baker changed his previous position and decided to treat Viola aurea as a separate species rather than as a subspecies of V. purpurea, and to treat mohavensis as a subspecies of V. aurea. G. L. Stebbins et al. (1963) stated that additional study may reveal that aurea and mohavensis might be better treated as species separate from each other and from V. purpurea.</discussion>
  <discussion>M. S. Baker (1953) noted that a form of var. mohavensis found in Mono and Inyo counties, California, is much greener in aspect and lacks microscopic pubescence. G. L. Stebbins et al. (1963) wrote that these taxa appeared to be a variable assemblage perhaps of forms transitional between V. aurea and subspecies of V. purpurea other than subsp. purpurea.</discussion>
  
</bio:treatment>