<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Laurence J. Dorr</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Byttnerioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1756" rank="genus">AYENIA</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Svenska Vetensk. Acad. Handl.</publication_title>
      <place_in_publication>17: 23, plate 2. 1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily byttnerioideae;genus ayenia;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Louis de Noailles, 1713 – 1793, first Duc d’Ayen</other_info_on_name>
    <other_info_on_name type="fna_id">103277</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="B. L. Robinson &amp; Greenman" date="unknown" rank="genus">Nephropetalum</taxon_name>
    <taxon_hierarchy>genus Nephropetalum</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs [trees], erect, ascending, or decumbent;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o8645" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unarmed, hairy 3–7-veined from base.</text>
      <biological_entity id="o8647" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character constraint="from base" constraintid="o8648" is_modifier="false" name="architecture" src="d0_s2" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
      <biological_entity id="o8648" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary or terminal, cymose, 1–11-flowered;</text>
      <biological_entity id="o8649" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-11-flowered" value_original="1-11-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>epicalyx absent.</text>
      <biological_entity id="o8650" name="epicalyx" name_original="epicalyx" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual [rarely unisexual or cleistogamous];</text>
      <biological_entity id="o8651" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals basally connate, basally abaxially glandular-hairy;</text>
      <biological_entity id="o8652" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally abaxially" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals clawed, claws filiform, lamina rhombic, deltate, reniform, or ± triangular, base divided into 2 lobes or not, lobe margins entire or erose, apex notched, entire, or with 2 widely spaced teeth, surfaces glabrous or hairy, abaxial surface appendaged or not, appendage ± filiform, cylindric, or clavate, inserted in middle of lamina distally, petals inflexed toward center of flower and connivent to apex of staminal tube such that when viewed from above corolla resembles a disc with staminodes, style, and stigma, if exserted, protruding from center;</text>
      <biological_entity id="o8653" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o8654" name="claw" name_original="claws" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o8655" name="lamina" name_original="lamina" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o8656" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="into " constraintid="o8658" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o8657" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o8658" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o8659" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o8660" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o8661" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8662" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" modifier="widely" name="arrangement" src="d0_s7" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o8663" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8664" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="appendaged" value_original="appendaged" />
        <character name="architecture" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o8665" name="appendage" name_original="appendage" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o8666" name="middle" name_original="middle" src="d0_s7" type="structure" />
      <biological_entity id="o8667" name="lamina" name_original="lamina" src="d0_s7" type="structure" />
      <biological_entity id="o8668" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character constraint="toward center" constraintid="o8669" is_modifier="false" name="orientation" src="d0_s7" value="inflexed" value_original="inflexed" />
        <character constraint="to apex" constraintid="o8671" is_modifier="false" name="arrangement" notes="" src="d0_s7" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o8669" name="center" name_original="center" src="d0_s7" type="structure" />
      <biological_entity id="o8670" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <biological_entity id="o8671" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s7" value="exserted" value_original="exserted" />
        <character constraint="from center" constraintid="o8676" is_modifier="false" name="prominence" src="d0_s7" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity constraint="staminal" id="o8672" name="tube" name_original="tube" src="d0_s7" type="structure" />
      <biological_entity id="o8673" name="staminode" name_original="staminodes" src="d0_s7" type="structure" />
      <biological_entity id="o8674" name="style" name_original="style" src="d0_s7" type="structure" />
      <biological_entity id="o8675" name="stigma" name_original="stigma" src="d0_s7" type="structure" />
      <biological_entity id="o8676" name="center" name_original="center" src="d0_s7" type="structure" />
      <relation from="o8658" id="r911" name="into" negation="false" src="d0_s7" to="o8659" />
      <relation from="o8658" id="r912" name="into" negation="false" src="d0_s7" to="o8660" />
      <relation from="o8661" id="r913" name="with" negation="false" src="d0_s7" to="o8662" />
      <relation from="o8665" id="r914" name="inserted in" negation="false" src="d0_s7" to="o8666" />
      <relation from="o8665" id="r915" name="middle of" negation="false" src="d0_s7" to="o8667" />
      <relation from="o8669" id="r916" name="part_of" negation="false" src="d0_s7" to="o8670" />
      <relation from="o8671" id="r917" name="part_of" negation="false" src="d0_s7" to="o8672" />
      <relation from="o8671" id="r918" modifier="when viewed from above corolla resembles a disc with staminodes" name="with" negation="false" src="d0_s7" to="o8673" />
      <relation from="o8671" id="r919" modifier="when viewed from above corolla resembles a disc with staminodes" name="with" negation="false" src="d0_s7" to="o8674" />
      <relation from="o8671" id="r920" modifier="when viewed from above corolla resembles a disc with staminodes" name="with" negation="false" src="d0_s7" to="o8675" />
    </statement>
    <statement id="d0_s8">
      <text>androgynophore present [absent];</text>
      <biological_entity id="o8677" name="androgynophore" name_original="androgynophore" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminodes 5, connate into cylindric or ± rounded, minute or indistinct.</text>
      <biological_entity id="o8678" name="staminode" name_original="staminodes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits capsules, often pendulous, oblate or subspheric, 5-locular, dehiscence septicidal and then loculicidal, hairy [glabrous], prickled.</text>
      <biological_entity constraint="fruits" id="o8679" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s10" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subspheric" value_original="subspheric" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="loculicidal" value_original="loculicidal" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 1 per locule, ovoid, smooth, wrinkled, or tuberculate;</text>
      <biological_entity id="o8680" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character constraint="per locule" constraintid="o8681" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o8681" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>endosperm absent;</text>
      <biological_entity id="o8682" name="endosperm" name_original="endosperm" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>cotyledons leafy, folded and rolled around hypocotylar axis.</text>
      <biological_entity id="o8684" name="axis" name_original="axis" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>x = 10.</text>
      <biological_entity id="o8683" name="cotyledon" name_original="cotyledons" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="around" name="shape" src="d0_s13" value="rolled" value_original="rolled" />
      </biological_entity>
      <biological_entity constraint="x" id="o8685" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, West Indies, Central America, South America (to Argentina and Paraguay).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (to Argentina and Paraguay)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The name Ayenia pusilla Linnaeus has been misapplied to some species found in the flora area. C. L. Cristóbal (1960) selected the South American element in the Linnaean protologue of this name as lectotype; the name is now correctly applied only to a species in Ecuador and Peru.</discussion>
  <discussion>Molecular data (B. A. Whitlock and A. M. Hale 2011) indicate that a monophyletic Ayenia is embedded within a paraphyletic Byttneria Loefling and that the monospecific, Brazilian endemic Rayleya Cristóbal is sister to a combined clade of Ayenia and Byttneria. The taxonomic implications of these relationships have not been fully resolved. A relatively small proportion of species in this Ayenia/Byttneria/Rayleya clade have been sampled and the African genus Megatritheca Cristóbal, which shows a combination of morphological features usually associated with either Ayenia or Byttneria, has not been included in molecular analyses.</discussion>
  <discussion>The structure of Ayenia flowers is complicated and the terminology applied to the various parts has not been consistent (see L. J. Dorr 1996 and included references). In particular, the terms claw and lamina as applied to petals have been used to describe different structures. termed a gland or ligule) or not. W. Leinfellner (1960), using comparative and developmental data, argued that the rhombic, deltate, or reniform portion of the petal (lamina) is homologous with the distal part of the claw and that the lamina is reduced to an appendage or absent.</discussion>
  <discussion>Species ca. 80 (7 in the flora).</discussion>
  <references>
    <reference>Cristóbal, C. L. 1960. Revisión del género Ayenia (Sterculiaceae). Opera Lilloana 4: 1–230.</reference>
    <reference>Whitlock, B. A. and A. M. Hale. 2011. The phylogeny of Ayenia, Byttneria, and Rayleya (Malvaceae s.l.) and its implications for the evolution of growth forms. Syst. Bot. 36: 129–136.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals: base of lamina lobed, apex entire or with 2 widely spaced teeth (not notched), abaxial appendage absent</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals: base of lamina attenuate on claw, apex notched, abaxial appendage present</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals caducous (not present in young fruit); capsule prickles 0.1–0.3 mm.</description>
      <determination>1 Ayenia compacta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals persistent; capsule prickles 0.3–1.5 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades: proximals and distals similar in shape and size, apex cuspidate; petiole (0.1–)0.2–0.5 cm; Florida.</description>
      <determination>2 Ayenia euphrasiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades: proximals and distals dissimilar in shape and size (sometimes only slightly), apex acute or subacute; petiole 0.4–1(–1.5) cm; Arizona, New Mexico, Texas</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves: blades of proximals ovate to orbiculate, distals oblong to ovate-lanceolate or linear, base rounded to truncate, surfaces usually stellate-puberulent, sometimes glabrescent.</description>
      <determination>3 Ayenia filiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves: blades of proximals broadly ovate to orbiculate, distals ovate to oblong-ovate or oblong-lanceolate, base cordate, surfaces moderately to sparingly hirsute, hairs usually simple, bifurcate, and fasciculate, sometimes also stellate.</description>
      <determination>7 Ayenia pilosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cymes borne on short shoots (brachyblasts); sepals not reflexed at anthesis; petal lamina apex with 2 widely-spaced teeth; stamen filaments present.</description>
      <determination>6 Ayenia microphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cymes not borne on short shoots (brachyblasts); sepals reflexed at anthesis; petal lamina apex entire; stamen filaments absent or nearly so</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades broadly ovate to ovate-lanceolate, slightly 3–5-lobed; petal lobe margins erose; androgynophore 0.5–1 mm; Arizona.</description>
      <determination>4 Ayenia jaliscana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades ovate to ovate-lanceolate, unlobed; petal lobe margins not erose; androgynophore 0.2–0.3 mm; Texas.</description>
      <determination>5 Ayenia limitaris</determination>
    </key_statement>
  </key>
</bio:treatment>