<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Desvaux" date="unknown" rank="family">frankeniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">frankenia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pulverulenta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 332. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family frankeniaceae;genus frankenia;species pulverulenta</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200014280</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">European sea-heath</other_name>
  <other_name type="common_name">wisp-weed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, often prostrate, to 1.5 (–3) dm;</text>
      <biological_entity id="o18012" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches glabrous or puberulous, hairs erect, usually curved, sometimes straight.</text>
      <biological_entity id="o18013" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulous" value_original="puberulous" />
      </biological_entity>
      <biological_entity id="o18014" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0.8–2 mm, markedly tapering toward blade, apex narrower than base of blade;</text>
      <biological_entity id="o18015" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18016" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character constraint="toward blade" constraintid="o18017" is_modifier="false" modifier="markedly" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18017" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o18018" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character constraint="than base" constraintid="o18019" is_modifier="false" name="width" src="d0_s2" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o18019" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o18020" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o18019" id="r1913" name="part_of" negation="false" src="d0_s2" to="o18020" />
    </statement>
    <statement id="d0_s3">
      <text>blade gray-green, usually narrowly obovate or obovate to elliptic or oblongelliptic, sometimes orbiculate, flat, 2–7 × 1–3 mm, margins slightly to loosely revolute, abaxial surface mostly exposed, adaxial surface usually glabrous, sometimes glabrate.</text>
      <biological_entity id="o18021" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18022" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="elliptic or oblongelliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18023" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly to loosely" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18024" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="prominence" src="d0_s3" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18025" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences usually compound, sometimes simple dichasia, sometimes solitary flowers.</text>
      <biological_entity id="o18026" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o18027" name="dichasium" name_original="dichasia" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o18028" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx 2.5–4.5 mm, lobes 5, 0.5–1 mm;</text>
      <biological_entity id="o18029" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18030" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18031" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5, pink to violet, oblong-oblanceolate to spatulate, 2.5–5.2 mm;</text>
      <biological_entity id="o18032" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18033" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s6" to="violet" />
        <character char_type="range_value" from="oblong-oblanceolate" name="shape" src="d0_s6" to="spatulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 6, included to ± exserted, 1.7–3.5 mm;</text>
      <biological_entity id="o18034" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18035" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character char_type="range_value" from="included" name="position" src="d0_s7" to="more or less exserted" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow;</text>
      <biological_entity id="o18036" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18037" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style included to ± exserted, 3-branched;</text>
      <biological_entity id="o18038" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18039" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="included" name="position" src="d0_s9" to="more or less exserted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-branched" value_original="3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 3-carpellate;</text>
      <biological_entity id="o18040" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18041" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 25–60, attached along sutures, funiculi erect.</text>
      <biological_entity id="o18042" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18043" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="60" />
        <character constraint="along sutures" constraintid="o18044" is_modifier="false" name="fixation" src="d0_s11" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o18044" name="suture" name_original="sutures" src="d0_s11" type="structure" />
      <biological_entity id="o18045" name="funiculus" name_original="funiculi" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 20–60 per capsule, oblong-ellipsoid, 0.4–0.7 mm. 2n = 20.</text>
      <biological_entity id="o18046" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per capsule" constraintid="o18047" from="20" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="oblong-ellipsoid" value_original="oblong-ellipsoid" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18047" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o18048" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ballast, moist, saline soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline soil" modifier="ballast moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Mass., N.J., Oreg., Utah; Eurasia; Africa; introduced also in South America and Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in South America and Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the United States, Frankenia pulverulenta is introduced and has been rarely collected on the east and west coasts and near Salt Lake City, Utah, where recent attempts to relocate it were unsuccessful (N. H. Holmgren 2005c).</discussion>
  
</bio:treatment>