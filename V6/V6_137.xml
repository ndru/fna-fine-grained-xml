<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="(Spach) R. Keller in H. G. A. Engler and K. Prantl" date="1893" rank="section">MYRIANDRA</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">ellipticum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 110. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section myriandra;species ellipticum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100861</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brathydium</taxon_name>
    <taxon_name authority="Spach" date="unknown" rank="species">canadense</taxon_name>
    <taxon_hierarchy>genus Brathydium;species canadense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypericum</taxon_name>
    <taxon_name authority="Steudel" date="unknown" rank="species">brathydium</taxon_name>
    <taxon_hierarchy>genus Hypericum;species brathydium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">canadense</taxon_name>
    <taxon_name authority="R. Keller" date="unknown" rank="variety">oviforme</taxon_name>
    <taxon_hierarchy>genus H.;species canadense;variety oviforme</taxon_hierarchy>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Pale St. John’s wort</other_name>
  <other_name type="common_name">millepertuis elliptique</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, erect, with creeping, rhizomatous, ± woody base, usually unbranched, sometimes branched proximally, relatively slender, 1.1–3 (–5) dm.</text>
      <biological_entity id="o20558" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o20559" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="true" modifier="more or less" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o20558" id="r2186" name="with" negation="false" src="d0_s0" to="o20559" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes 4-lined.</text>
      <biological_entity id="o20560" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o20561" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades broadly to narrowly elliptic or oblanceolate to oblongelliptic, 11–35 × 3–13 mm, base not articulated, cuneate to shallowly cordate-amplexicaul, margins plane to subrevolute, apex rounded, midrib with 5–7 pairs of branches.</text>
      <biological_entity id="o20562" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" modifier="narrowly" name="shape" src="d0_s2" to="oblongelliptic" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20563" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s2" value="cordate-amplexicaul" value_original="cordate-amplexicaul" />
      </biological_entity>
      <biological_entity id="o20564" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subrevolute" value_original="subrevolute" />
      </biological_entity>
      <biological_entity id="o20565" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20566" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o20567" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o20568" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <relation from="o20566" id="r2187" name="with" negation="false" src="d0_s2" to="o20567" />
      <relation from="o20567" id="r2188" name="part_of" negation="false" src="d0_s2" to="o20568" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences corymbiform, (1–) 3–15-flowered, narrowly branched, sometimes with branches from 1–2 proximal nodes.</text>
      <biological_entity id="o20569" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="(1-)3-15-flowered" value_original="(1-)3-15-flowered" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o20570" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o20571" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <relation from="o20569" id="r2189" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o20570" />
      <relation from="o20570" id="r2190" name="from" negation="false" src="d0_s3" to="o20571" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 12–15 mm diam.;</text>
      <biological_entity id="o20572" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals persistent, not enclosing capsule, (4–) 5, ± lanceolate to lanceolate-elliptic, ± unequal, 6–7 × 2–3 mm;</text>
      <biological_entity id="o20573" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="less lanceolate" name="shape" src="d0_s5" to="lanceolate-elliptic" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20574" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <relation from="o20573" id="r2191" name="enclosing" negation="true" src="d0_s5" to="o20574" />
    </statement>
    <statement id="d0_s6">
      <text>petals (4–) 5, pale-yellow, sometimes tinged red, obovate to oblanceolate, 6–8 mm;</text>
      <biological_entity id="o20575" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="tinged red" value_original="tinged red" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens persistent, 70–95;</text>
      <biological_entity id="o20576" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="70" name="quantity" src="d0_s7" to="95" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary 3-merous, placentation parietal.</text>
      <biological_entity id="o20577" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-merous" value_original="3-merous" />
        <character is_modifier="false" name="placentation" src="d0_s8" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules ellipsoid to globose, 4–7 × 3.5–5 mm.</text>
      <biological_entity id="o20578" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s9" to="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds carinate, 0.6–0.7 mm;</text>
      <biological_entity id="o20579" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>testa scalariform-reticulate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16, 18.</text>
      <biological_entity id="o20580" name="testa" name_original="testa" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s11" value="scalariform-reticulate" value_original="scalariform-reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20581" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream, lake, and pond margins, wet meadows, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., Que.; Conn., Ill., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., Ohio., Pa., R.I., Tenn., Vt., Wash., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypericum ellipticum is related to H. sphaerocarpum, differing by the shorter, herbaceous, rhizomatous habit, shorter leaves, and smaller seeds. A submerged aquatic form (forma submersum Fassett) and one with axillary branches developing after fertilization (forma foliosum Marie-Victorin) seem scarcely worth formal recognition. Hypericum ellipticum is introduced in Washington.</discussion>
  
</bio:treatment>