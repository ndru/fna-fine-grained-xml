<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">114</other_info_on_meta>
    <other_info_on_meta type="illustration_page">132</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pedata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 933. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species pedata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242417472</other_info_on_name>
  </taxon_identification>
  <number>43.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, acaulescent, not stoloniferous, 5–30 cm;</text>
      <biological_entity id="o1312" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome thick, fleshy.</text>
      <biological_entity id="o1313" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, 4–10, ascending to erect, deeply divided;</text>
      <biological_entity id="o1314" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linearlanceolate, margins entire, lacerate, or shallowly divided, apex acute;</text>
      <biological_entity id="o1315" name="stipule" name_original="stipules" src="d0_s3" type="structure" />
      <biological_entity id="o1316" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o1317" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–12 cm, glabrous or sparsely puberulent;</text>
      <biological_entity id="o1318" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 3–9 (–10) -lobed, lobes similar in width and shape, spatulate, lanceolate ± linear, deltate, or ovate, 1–4 × 1–4 cm, base attenuate or broadly cordate to cuneate, margins entire, ciliate or eciliate, apex rounded to usually acute, surfaces usually glabrous, sometimes pubescent on abaxial veins.</text>
      <biological_entity id="o1319" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="3-9(-10)-lobed" value_original="3-9(-10)-lobed" />
      </biological_entity>
      <biological_entity id="o1320" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1321" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly cordate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o1322" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o1323" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="usually acute" />
      </biological_entity>
      <biological_entity id="o1324" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="on abaxial veins" constraintid="o1325" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1325" name="vein" name_original="veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–12 cm, glabrous or pubescent.</text>
      <biological_entity id="o1326" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals lanceolate, margins mostly ciliate, at least proximally, auricles 1–2 mm;</text>
      <biological_entity id="o1327" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1328" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1329" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1330" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" modifier="at-least proximally; proximally" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals uniformly light to dark blue-violet on both surfaces or upper 2 darker adaxially, sometimes white, upper and lateral 2 often darker basally, lowest, seldom others, dark violet-veined, all beardless, lowest white basally, 12–24 mm, spur white, gibbous, 2–3 mm;</text>
      <biological_entity id="o1331" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1332" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="on " constraintid="o1336" from="uniformly light" name="coloration" src="d0_s8" to="dark blue-violet" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="dark violet-veined" value_original="dark violet-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="beardless" value_original="beardless" />
        <character is_modifier="false" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1333" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1334" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1335" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1336" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character constraint="on " constraintid="o1340" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1337" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1338" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1339" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1340" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1341" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1342" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character constraint="on " constraintid="o1346" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1343" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1344" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1345" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="true" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1346" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1347" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1348" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="true" name="position" src="d0_s8" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o1349" name="spur" name_original="spur" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o1340" id="r145" name="on" negation="false" src="d0_s8" to="o1341" />
      <relation from="o1340" id="r146" modifier="on both surfaces or upper darker , sometimes white , upper and lateral often darker , lowest , seldom others" name="on" negation="false" src="d0_s8" to="o1342" />
      <relation from="o1346" id="r147" name="on" negation="false" src="d0_s8" to="o1347" />
      <relation from="o1346" id="r148" modifier="on both surfaces or upper darker , sometimes white , upper and lateral often darker , lowest , seldom others" name="on" negation="false" src="d0_s8" to="o1348" />
    </statement>
    <statement id="d0_s9">
      <text>style head beardless;</text>
      <biological_entity id="o1350" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="style" id="o1351" name="head" name_original="head" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="beardless" value_original="beardless" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cleistogamous flowers absent.</text>
      <biological_entity id="o1352" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1353" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ellipsoid, 6–10 mm, glabrous.</text>
      <biological_entity id="o1354" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds beige, mottled to brown, 1.4–3 mm. 2n = 56.</text>
      <biological_entity id="o1355" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="beige" value_original="beige" />
        <character char_type="range_value" from="mottled" name="coloration" src="d0_s12" to="brown" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1356" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., D.C., Del., Ga., Ill., Ind., Iowa, Kans., Ky., La., Mass., Md., Mich., Minn., Miss., Mo., N.C., N.H., N.J., N.Y., Nebr., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 7–9-lobed, lobes spatulate, lanceolate, or ± linear, sometimes with narrowly deltate to falcate appendages toward apex.</description>
      <determination>43a Viola pedata var. pedata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 3–5(–10)-lobed, lobes deltate or ovate.</description>
      <determination>43b Viola pedata var. ranunculifolia</determination>
    </key_statement>
  </key>
</bio:treatment>