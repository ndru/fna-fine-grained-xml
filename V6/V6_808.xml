<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">purpurea</taxon_name>
    <taxon_hierarchy>family violaceae;genus viola;species purpurea;variety purpurea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100949</other_info_on_name>
  </taxon_identification>
  <number>51a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–25 cm.</text>
      <biological_entity id="o17140" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, usually not buried, usually elongated by end of season, ± glabrous or puberulent.</text>
      <biological_entity id="o17141" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually not" name="location" src="d0_s1" value="buried" value_original="buried" />
        <character constraint="by end" constraintid="o17142" is_modifier="false" modifier="usually" name="length" src="d0_s1" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o17142" name="end" name_original="end" src="d0_s1" type="structure" />
      <biological_entity id="o17143" name="season" name_original="season" src="d0_s1" type="structure" />
      <relation from="o17142" id="r1823" name="part_of" negation="false" src="d0_s1" to="o17143" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal: 1–5;</text>
      <biological_entity id="o17144" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o17145" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 4–11 cm, puberulent;</text>
      <biological_entity id="o17146" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17147" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="11" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade purple-tinted abaxially, green adaxially, often shiny, ± orbiculate to widely ovate, 1.6–4.6 (–5.3) × 1.6–4.1 cm, ± fleshy, base usually attenuate, sometimes subcordate or truncate, margins irregularly crenate, apex acute to obtuse, abaxial surface glabrous or puberulent, hairs sometimes only on veins, adaxial surface usually glabrous, sometimes sparsely puberulent;</text>
      <biological_entity id="o17148" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17149" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="purple-tinted" value_original="purple-tinted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="less orbiculate" name="shape" src="d0_s4" to="widely ovate" />
        <character char_type="range_value" from="4.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="5.3" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s4" to="4.6" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s4" to="4.1" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o17150" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o17151" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o17152" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17153" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o17154" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o17155" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o17156" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o17154" id="r1824" name="on" negation="false" src="d0_s4" to="o17155" />
    </statement>
    <statement id="d0_s5">
      <text>cauline: petiole 1.5–19.7 cm, usually puberulent;</text>
      <biological_entity constraint="cauline" id="o17157" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o17158" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="19.7" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate, deltate, or lanceolate, 1.2–4.5 × 0.8–2.5 cm, length 1–2.3 times width, base usually attenuate, sometimes ± cordate or truncate, margins crenate-serrate, abaxial surface puberulent (or along veins), adaxial surface glabrous or sparsely pubescent.</text>
      <biological_entity constraint="cauline" id="o17159" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o17160" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s6" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1-2.3" value_original="1-2.3" />
      </biological_entity>
      <biological_entity id="o17161" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o17162" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17163" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17164" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 4.5–10 cm, usually puberulent, sometimes glabrous.</text>
      <biological_entity id="o17165" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Lowest petals 10–12 mm.</text>
      <biological_entity constraint="lowest" id="o17166" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 5–6 mm.</text>
      <biological_entity id="o17167" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds dark-brown, 2.1–2.3 mm.</text>
      <biological_entity id="o17168" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In openings or beneath shrubs, usually in yellow pine (Pinus ponderosa) forests or higher</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" modifier="in" />
        <character name="habitat" value="shrubs" modifier="or beneath" />
        <character name="habitat" value="pinus ponderosa" />
        <character name="habitat" value="yellow pine ( pinus ponderosa ) forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Mountain violet</other_name>
  
</bio:treatment>