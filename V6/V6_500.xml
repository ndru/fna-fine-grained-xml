<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Fryxell" date="1978" rank="genus">KRAPOVICKASIA</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 456. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus krapovickasia;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Antonio Krapovickas, b. 1921 Argentinian botanist</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">117269</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Monteiro" date="unknown" rank="genus">Physalastrum</taxon_name>
    <place_of_publication>
      <publication_title>Anais Congr. Soc. Bot. Brasil.</publication_title>
      <place_in_publication>20: 395. 1969</place_in_publication>
      <other_info_on_pub>(not Physaliastrum Makino 1914 [Solanaceae]), based on Sida Linnaeus sect. Physalodes Grisebach ex K. Schumann in C. F. P. von Martius et al., Fl. Bras. 12(3): 280. 1891</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Physalastrum</taxon_hierarchy>
  </taxon_identification>
  <number>30.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o5392" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent [suberect], with fine stellate-pubescence and long simple hairs, not viscid.</text>
      <biological_entity id="o5393" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="not" name="coating" notes="" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o5394" name="stellate-pubescence" name_original="stellate-pubescence" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o5395" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o5393" id="r616" name="with" negation="false" src="d0_s1" to="o5394" />
      <relation from="o5393" id="r617" name="with" negation="false" src="d0_s1" to="o5395" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate and distichous;</text>
      <biological_entity id="o5396" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules persistent, inconspicuous, subulate;</text>
      <biological_entity id="o5397" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate-oblong [lanceolate-ovate], unlobed, base cordate, margins crenate-dentate, surfaces tomentose.</text>
      <biological_entity id="o5398" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o5399" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o5400" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-dentate" value_original="crenate-dentate" />
      </biological_entity>
      <biological_entity id="o5401" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, solitary flowers [fasciculate];</text>
      <biological_entity id="o5402" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o5403" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucel absent.</text>
      <biological_entity id="o5404" name="involucel" name_original="involucel" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx accrescent, inflated in fruit, brownish-membranous at maturity, completely enclosing fruit, lobes unribbed, ovate, stellate-hairy;</text>
      <biological_entity id="o5405" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5406" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="accrescent" value_original="accrescent" />
        <character constraint="in fruit" constraintid="o5407" is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="at maturity" is_modifier="false" name="texture" notes="" src="d0_s7" value="brownish-membranous" value_original="brownish-membranous" />
      </biological_entity>
      <biological_entity id="o5407" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o5408" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o5409" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="unribbed" value_original="unribbed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o5406" id="r618" modifier="completely" name="enclosing" negation="false" src="d0_s7" to="o5408" />
    </statement>
    <statement id="d0_s8">
      <text>corolla white or rose;</text>
      <biological_entity id="o5410" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5411" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose" value_original="rose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminal column included;</text>
      <biological_entity id="o5412" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="staminal" id="o5413" name="column" name_original="column" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 5 [–9] -carpellate;</text>
      <biological_entity id="o5414" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5415" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5[-9]-carpellate" value_original="5[-9]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 5 [–9] -branched;</text>
      <biological_entity id="o5416" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5417" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5[-9]-branched" value_original="5[-9]-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas capitate.</text>
      <biological_entity id="o5418" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5419" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits schizocarps, erect, not inflated, subglobose, not indurate, fragile-walled, glabrous;</text>
      <biological_entity constraint="fruits" id="o5420" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s13" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="fragile-walled" value_original="fragile-walled" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 5, 1-celled, without dorsal spurs, indehiscent.</text>
      <biological_entity id="o5421" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-celled" value_original="1-celled" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s14" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o5422" name="spur" name_original="spurs" src="d0_s14" type="structure" />
      <relation from="o5421" id="r619" name="without" negation="false" src="d0_s14" to="o5422" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1 per mericarp, glabrous.</text>
      <biological_entity id="o5424" name="mericarp" name_original="mericarp" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>x = 8.</text>
      <biological_entity id="o5423" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character constraint="per mericarp" constraintid="o5424" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o5425" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Tex.; n Mexico, South America (Argentina, Bolivia, Brazil, Paraguay, Peru, Uruguay).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Paraguay)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="South America (Uruguay)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (1 in the flora).</discussion>
  
</bio:treatment>