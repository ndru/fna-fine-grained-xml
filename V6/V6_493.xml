<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="genus">iliamna</taxon_name>
    <taxon_name authority="Wiggins" date="unknown" rank="species">latibracteata</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>1: 225, plate 20. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus iliamna;species latibracteata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101068</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphaeralcea</taxon_name>
    <taxon_name authority="(Douglas) Torrey" date="unknown" rank="species">rivularis</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">cismontana</taxon_name>
    <taxon_hierarchy>genus Sphaeralcea;species rivularis;variety cismontana</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">California wild hollyhock</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–2 m;</text>
      <biological_entity id="o10123" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>herbage harshly and moderately stellate-hairy.</text>
      <biological_entity id="o10124" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades deeply (3-), 5-lobed, or 7-lobed, 8–20 cm wide, lobes broadest at middle, base truncate to cordate, margins serrate or irregularly dentate.</text>
      <biological_entity id="o10125" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="[3" value_original="[3" />
        <character is_modifier="false" name="shape" src="d0_s2" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="7-lobed" value_original="7-lobed" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10126" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character constraint="at middle lobes" constraintid="o10127" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity constraint="middle" id="o10127" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity id="o10128" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cordate" />
      </biological_entity>
      <biological_entity id="o10129" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences cymes, forming interrupted spikes or racemes;</text>
      <biological_entity constraint="inflorescences" id="o10130" name="cyme" name_original="cymes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets broadly elliptic-lanceolate to ovate, 10–14 × 4–6 mm, equaling or exceeding calyx.</text>
      <biological_entity id="o10131" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity id="o10132" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity id="o10133" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly elliptic-lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s4" value="exceeding calyx" value_original="exceeding calyx" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx 8–10 mm, lobes triangular-ovate, 5–8 × 4–5 mm, slightly longer than broad, slightly longer than tube;</text>
      <biological_entity id="o10134" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o10135" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10136" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character constraint="slightly longer than broad , slightly longer than tube" constraintid="o10137" is_modifier="false" name="length_or_size" src="d0_s5" value="slightly longer than broad , slightly longer than tube" />
      </biological_entity>
      <biological_entity id="o10138" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10137" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals rose-purple, 2–3 cm.</text>
      <biological_entity id="o10139" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10140" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose-purple" value_original="rose-purple" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Schizocarps 10–15 mm diam.;</text>
      <biological_entity id="o10141" name="schizocarp" name_original="schizocarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mericarps 10–14, 6–8 mm.</text>
      <biological_entity id="o10142" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="14" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 2 or 3, dark-brown, 2 mm, puberulent.</text>
      <biological_entity id="o10143" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s9" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Conifer forests, streamsides, sometimes shaded</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="conifer forests" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="shaded" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Iliamna latibracteata is distinctive in having large involucral bracts, which more or less envelop the calyx and equal or surpass it in length. It occurs in the Klamath and Siskiyou mountains of Del Norte and Humboldt counties in California, north to Coos, Curry, Douglas, Jackson, and Josephine counties in Oregon, with an eastern outlying station reported from Mount Shasta, California.</discussion>
  
</bio:treatment>