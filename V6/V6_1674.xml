<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/17 21:02:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">30</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Rose" date="1912" rank="genus">tumamoca</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 21, plate 17. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus tumamoca</taxon_hierarchy>
    <other_info_on_name type="etymology">Local Native American Tumamoc, name for the hill upon which the Carnegie Institute Desert Laboratory is located</other_info_on_name>
    <other_info_on_name type="fna_id">133986</other_info_on_name>
  </taxon_identification>
  <number>14</number>
  <other_name type="common_name">Globeberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, monoecious and dioecious (developmentally, seasonally), trailing or climbing;</text>
      <biological_entity id="o63" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems perennial proximally, annual distally, woody, glabrous [hispid-hirsute or hispidulous];</text>
      <biological_entity id="o64" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="duration" src="d0_s1" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="distally" name="duration" src="d0_s1" value="annual" value_original="annual" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots tuberous;</text>
      <biological_entity id="o65" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tendrils unbranched.</text>
      <biological_entity id="o66" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiolar region an extension of blade midportion;</text>
      <biological_entity id="o67" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="petiolar" id="o68" name="region" name_original="region" src="d0_s4" type="structure" />
      <biological_entity constraint="petiolar" id="o69" name="extension" name_original="extension" src="d0_s4" type="structure" />
      <biological_entity id="o70" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <relation from="o68" id="r5" name="part_of" negation="false" src="d0_s4" to="o70" />
      <relation from="o69" id="r6" name="part_of" negation="false" src="d0_s4" to="o70" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to orbiculate, deeply 3-lobed, lobes 1–2-lobed, ultimate segments narrow, margins entire, surfaces eglandular at base.</text>
      <biological_entity id="o71" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o72" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="orbiculate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o73" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="1-2-lobed" value_original="1-2-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o74" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o75" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o76" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character constraint="at base" constraintid="o77" is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o77" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: staminate flowers 2–19 in axillary, subsessile racemes;</text>
      <biological_entity id="o78" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o79" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="in racemes" constraintid="o80" from="2" name="quantity" src="d0_s6" to="19" />
      </biological_entity>
      <biological_entity id="o80" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers solitary, from same axils as staminate;</text>
      <biological_entity id="o81" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o82" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o83" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <relation from="o82" id="r7" modifier="as staminate" name="from" negation="false" src="d0_s7" to="o83" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o84" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o85" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium narrowly tubular-funnelform;</text>
      <biological_entity id="o86" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o87" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, deltate to triangular;</text>
      <biological_entity id="o88" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o89" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s10" to="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, distinct, pale-yellow to greenish yellow, narrowly triangular to linear-lanceolate, valvate in bud, 4–6 mm, apex entire, glabrous, corolla salverform.</text>
      <biological_entity id="o90" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o91" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s11" to="greenish yellow" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s11" to="linear-lanceolate" />
        <character constraint="in bud" constraintid="o92" is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o92" name="bud" name_original="bud" src="d0_s11" type="structure" />
      <biological_entity id="o93" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o94" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="salverform" value_original="salverform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: stamens 3;</text>
      <biological_entity id="o95" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o96" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted at hypanthium rim, distinct, nearly vestigial;</text>
      <biological_entity id="o97" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o98" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="nearly" name="prominence" src="d0_s13" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o99" name="rim" name_original="rim" src="d0_s13" type="structure" />
      <relation from="o98" id="r8" name="inserted at" negation="false" src="d0_s13" to="o99" />
    </statement>
    <statement id="d0_s14">
      <text>thecae distinct, oblong, connective narrow, each with glabrous, narrow, exserted, terminal appendage nearly as long as corolla;</text>
      <biological_entity id="o100" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o101" name="theca" name_original="thecae" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o102" name="connective" name_original="connective" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o103" name="appendage" name_original="appendage" src="d0_s14" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o104" name="corolla" name_original="corolla" src="d0_s14" type="structure" />
      <relation from="o102" id="r9" name="with" negation="false" src="d0_s14" to="o103" />
      <relation from="o103" id="r10" name="as long as" negation="false" src="d0_s14" to="o104" />
    </statement>
    <statement id="d0_s15">
      <text>pistillodes absent.</text>
      <biological_entity id="o105" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o106" name="pistillode" name_original="pistillodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary 3-locular, ellipsoid;</text>
      <biological_entity id="o107" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o108" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules ca. 2–6 per locule;</text>
      <biological_entity id="o109" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o110" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o111" from="2" name="quantity" src="d0_s17" to="6" />
      </biological_entity>
      <biological_entity id="o111" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1, narrowly columnar;</text>
      <biological_entity id="o112" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o113" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="columnar" value_original="columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 3, coiled;</text>
      <biological_entity id="o114" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o115" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>staminodes 3.</text>
      <biological_entity id="o116" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o117" name="staminode" name_original="staminodes" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits berrylike, red or yellow, globose, 0.8–1 cm, smooth, glabrous, irregularly dehiscent.</text>
      <biological_entity id="o118" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="berrylike" value_original="berrylike" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s21" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s21" to="1" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s21" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s21" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 8–20, obovoid to ovoid, compressed, arillate, margins obscure, surface tuberculate-rugose.</text>
      <biological_entity id="o119" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s22" to="20" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s22" to="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o120" name="margin" name_original="margins" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o121" name="surface" name_original="surface" src="d0_s22" type="structure">
        <character is_modifier="false" name="relief" src="d0_s22" value="tuberculate-rugose" value_original="tuberculate-rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arizona, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Arizona" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora)</discussion>
  <discussion>Tumamoca is closely related to Ibervillea. Plants of both genera produce orange to red fruits with prominently margined, red-arillate seeds. Tumamoca is distinct in its narrowly funnelform hypanthium (versus narrowly campanulate to cylindric in Ibervillea), three staminodes (versus five), entire petals (versus apices bifid), interior corolla surfaces glabrous (versus densely pubescent), valvate buds (versus buds with infolded apices), and seeds with roughened surfaces and obscure margins (versus corky-pleated surfaces and raised margins).</discussion>
  <discussion>Tumamoca mucronata Kearns apparently is known only from the type locality in northwestern Zacatecas, Mexico.</discussion>
  <references>
    <reference>Kearns, D. M. 1994. A revision of Tumamoca (Cucurbitaceae). Madroño 41: 23–29.</reference>
  </references>
  
</bio:treatment>