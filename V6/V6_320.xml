<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Roussel" date="unknown" rank="family">passifloraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">passiflora</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">ciliata</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>3: 310. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family passifloraceae;genus passiflora;species ciliata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100986</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Passiflora</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">foetida</taxon_name>
    <taxon_name authority="(Aiton) Masters" date="unknown" rank="variety">ciliata</taxon_name>
    <taxon_hierarchy>genus Passiflora;species foetida;variety ciliata</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems terete to longitudinally ridged, glabrous or densely hairy.</text>
      <biological_entity id="o17464" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s0" to="longitudinally ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves not or weakly pungent, glabrous or densely hairy, glandular-ciliate;</text>
      <biological_entity id="o17465" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="weakly" name="odor_or_shape" src="d0_s1" value="pungent" value_original="pungent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules pectinate, 5–6 × 2–4 mm, with glandular bristles or hairs;</text>
      <biological_entity id="o17466" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pectinate" value_original="pectinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o17467" name="bristle" name_original="bristles" src="d0_s2" type="structure" />
      <biological_entity id="o17468" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <relation from="o17466" id="r1858" name="with" negation="false" src="d0_s2" to="o17467" />
      <relation from="o17466" id="r1859" name="with" negation="false" src="d0_s2" to="o17468" />
    </statement>
    <statement id="d0_s3">
      <text>petiole with glandular bristles or hairs;</text>
      <biological_entity id="o17469" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity constraint="glandular" id="o17470" name="bristle" name_original="bristles" src="d0_s3" type="structure" />
      <biological_entity id="o17471" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o17469" id="r1860" name="with" negation="false" src="d0_s3" to="o17470" />
      <relation from="o17469" id="r1861" name="with" negation="false" src="d0_s3" to="o17471" />
    </statement>
    <statement id="d0_s4">
      <text>blade roughly symmetric, 3–10 (–12) × 3–8 (–10) cm, moderately 3 (–5) -lobed, middle lobe much longer than lateral lobes, margins weakly serrulate to nearly entire;</text>
      <biological_entity id="o17472" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="roughly" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s4" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o17473" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="than lateral lobes" constraintid="o17474" is_modifier="false" name="length_or_size" src="d0_s4" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o17474" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o17475" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="weakly serrulate" name="architecture_or_shape" src="d0_s4" to="nearly entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial fine veins weakly raised, abaxial nectaries absent.</text>
      <biological_entity constraint="abaxial" id="o17476" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity id="o17477" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="fine" value_original="fine" />
        <character is_modifier="false" modifier="weakly" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17478" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bracts pinnatifid, 20–30 × 15–25 mm, margins serrate to nearly entire, with glandular bristles or hairs.</text>
      <biological_entity constraint="floral" id="o17479" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s6" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17480" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s6" value="serrate to nearly" value_original="serrate to nearly" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o17481" name="bristle" name_original="bristles" src="d0_s6" type="structure" />
      <biological_entity id="o17482" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o17480" id="r1862" name="with" negation="false" src="d0_s6" to="o17481" />
      <relation from="o17480" id="r1863" name="with" negation="false" src="d0_s6" to="o17482" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: floral-tube cuplike, 4–6 mm deep;</text>
      <biological_entity id="o17483" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17484" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuplike" value_original="cuplike" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals white, 18–25 × 7–9 mm;</text>
      <biological_entity id="o17485" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17486" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals purple to white, 20–25 × 8–10 mm;</text>
      <biological_entity id="o17487" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17488" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s9" to="white" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corona filament whorls 5–6, outer 2 whorls purple basally, white medially, pale-purple apically, linear, terete to transversely compressed, 16–20 mm.</text>
      <biological_entity id="o17489" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="filament" id="o17490" name="whorl" name_original="whorls" src="d0_s10" type="structure" constraint_original="corona filament">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o17491" name="whorl" name_original="whorls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s10" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s10" to="transversely compressed" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries bright red to crimson, ovoid to broadly ellipsoid, 30–35 × 20–25 mm.</text>
      <biological_entity id="o17492" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s11" to="crimson" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="broadly ellipsoid" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Warm-temperate to subtropical woodlands, shrublands, or disturbed areas, in moist to dry, loamy to sandy soil, 10–200 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="warm-temperate to subtropical woodlands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="disturbed areas" modifier="or" />
        <character name="habitat" value="in moist to dry" />
        <character name="habitat" value="loamy to sandy soil" />
        <character name="habitat" value="10–200 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Tex.; Mexico; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Passiflora ciliata is relatively uncommon and apparently introduced in the flora area and probably is spreading. Nearly all plants in Florida (for example, R. P. Wunderlin and B. F. Hansen 2003) and some in Texas identified as P. foetida are actually P. ciliata. E. P. Killip (1938) noted the presence of P. foetida var. riparia (C. Wright ex Grisebach) Killip in Florida, which we include in P. ciliata.</discussion>
  <discussion>Plants of this species within the flora area can be either entirely glabrous or densely hairy throughout; otherwise, they are essentially identical morphologically. The description provided here best reflects its variation within our region, and the species is more variable outside of our range. We treat this species broadly to include many members of sect. Dysosmia that have mature leaves unscented to weakly pungent when bruised and that have bright red to scarlet fruits, characteristics that seem to be consistently associated.</discussion>
  
</bio:treatment>