<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Seringe" date="unknown" rank="genus">LAGENARIA</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Soc. Phys. Genève</publication_title>
      <place_in_publication>3: 25, plate 2. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus LAGENARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lagenos, flask, alluding to shape and use of fruit</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">117477</other_info_on_name>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Bottle gourd</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual [perennial], monoecious [dioecious], scandent or prostrate;</text>
      <biological_entity id="o12448" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scandent" value_original="scandent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems mostly annual, densely villous to puberulent [sparsely hirsute to villous-hirsute];</text>
    </statement>
    <statement id="d0_s2">
      <text>taprooted [roots tuberous];</text>
      <biological_entity id="o12449" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s1" value="annual" value_original="annual" />
        <character char_type="range_value" from="densely villous" name="pubescence" src="d0_s1" to="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="taprooted" value_original="taprooted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tendrils 2-branched.</text>
      <biological_entity id="o12450" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-branched" value_original="2-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole with pair of small conical glands at apex;</text>
      <biological_entity id="o12451" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12452" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o12453" name="pair" name_original="pair" src="d0_s4" type="structure" />
      <biological_entity id="o12454" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s4" value="conical" value_original="conical" />
      </biological_entity>
      <biological_entity id="o12455" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o12452" id="r1320" name="with" negation="false" src="d0_s4" to="o12453" />
      <relation from="o12453" id="r1321" name="part_of" negation="false" src="d0_s4" to="o12454" />
      <relation from="o12453" id="r1322" name="at" negation="false" src="d0_s4" to="o12455" />
    </statement>
    <statement id="d0_s5">
      <text>blade broadly reniform or ovate [suborbiculate], ± palmately 3–5 (–7) -lobed, lobes triangular to widely obovate, base cordate, margins shallowly sinuate-serrate to dentate or sinuate-dentate, surfaces shortly puberulous or pubescent, eglandular.</text>
      <biological_entity id="o12456" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12457" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less palmately" name="shape" src="d0_s5" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
      </biological_entity>
      <biological_entity id="o12458" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="widely obovate" />
      </biological_entity>
      <biological_entity id="o12459" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o12460" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="shallowly sinuate-serrate" name="architecture_or_shape" src="d0_s5" to="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="sinuate-dentate" value_original="sinuate-dentate" />
      </biological_entity>
      <biological_entity id="o12461" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s5" value="puberulous" value_original="puberulous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: staminate flowers solitary [racemose], axillary;</text>
      <biological_entity id="o12462" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o12463" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers solitary, in same axils as staminate;</text>
      <biological_entity id="o12464" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o12465" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o12466" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <relation from="o12465" id="r1323" modifier="as staminate" name="in" negation="false" src="d0_s7" to="o12466" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o12467" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o12468" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium campanulate to funnelform;</text>
      <biological_entity id="o12469" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12470" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character constraint="to funnelform" constraintid="o12471" is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o12471" name="funnelform" name_original="funnelform" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, subulate to triangular or linear;</text>
      <biological_entity id="o12472" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12473" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s10" to="triangular or linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, distinct, white to cream, obovate to oblong-obovate, (15–) 20–greenish yellow, maturing yellowish or pale-brown, commonly mottled or with light green or white longitudinal stripes, subglobose to cylindric, ellipsoid, or lageniform, sometimes 2-ventricose, usually broader distally, smooth, glabrous, indehiscent.</text>
      <biological_entity id="o12474" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12475" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="cream" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="oblong-obovate" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="(15-)20-greenish yellow" value_original="(15-)20-greenish yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s11" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-brown" value_original="pale-brown" />
        <character constraint="with stripes" constraintid="o12476" is_modifier="false" modifier="commonly" name="coloration" src="d0_s11" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="with light green or white longitudinal stripes" />
        <character char_type="range_value" from="subglobose" name="shape" notes="" src="d0_s11" to="cylindric ellipsoid or lageniform" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s11" to="cylindric ellipsoid or lageniform" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s11" to="cylindric ellipsoid or lageniform" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="2-ventricose" value_original="2-ventricose" />
        <character is_modifier="false" modifier="usually; distally" name="width" src="d0_s11" value="broader" value_original="broader" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o12476" name="stripe" name_original="stripes" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="light green" value_original="light green" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s11" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 100–300, oblong to ovoid-oblong, compressed, not arillate, with marginal groove (truncate), surface smooth.</text>
      <biological_entity id="o12477" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s12" to="300" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="ovoid-oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o12478" name="groove" name_original="groove" src="d0_s12" type="structure" />
      <relation from="o12477" id="r1324" name="with" negation="false" src="d0_s12" to="o12478" />
    </statement>
    <statement id="d0_s13">
      <text>x = 11.</text>
      <biological_entity id="o12479" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o12480" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Africa, Indian Ocean Islands (Madagascar); introduced widely.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="widely" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 (1 in the flora).</discussion>
  <references>
    <reference>Clarke, A. C. et al. 2006. Reconstructing the origins and dispersal of the Polynesian bottle gourd (Lagenaria siceraria). Molec. Biol. Evol. 23: 893–900.</reference>
  </references>
  
</bio:treatment>