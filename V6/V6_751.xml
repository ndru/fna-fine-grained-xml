<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="(Dunal) Spach" date="unknown" rank="genus">TUBERARIA</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 6: 364. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus TUBERARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin tuber, swelling, and -aria, possession, alluding to swellings on roots</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">133941</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Helianthemum</taxon_name>
    <taxon_name authority="Dunal" date="unknown" rank="section">Tuberaria</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>1: 270. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthemum;section Tuberaria</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual [perennial], 0.3–2 (–3) [–8] dm.</text>
      <biological_entity id="o2414" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly opposite, basal soon withering, sometimes in rosettes, distal cauline sometimes alternate, stipulate or estipulate, petiolate or sessile;</text>
      <biological_entity id="o2415" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2416" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="soon" name="life_cycle" src="d0_s1" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o2417" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity constraint="distal cauline" id="o2418" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="estipulate" value_original="estipulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="estipulate" value_original="estipulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o2416" id="r310" modifier="sometimes" name="in" negation="false" src="d0_s1" to="o2417" />
    </statement>
    <statement id="d0_s2">
      <text>blade usually 3 [–5] -veined from base, margins sometimes revolute, surfaces hairy [glabrous], hairs sometimes clustered (stellate).</text>
      <biological_entity id="o2419" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o2420" is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="3[-5]-veined" value_original="3[-5]-veined" />
      </biological_entity>
      <biological_entity id="o2420" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o2421" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o2422" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2423" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemiform [helicoid, scorpioid] cymes.</text>
      <biological_entity id="o2424" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o2425" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels present;</text>
      <biological_entity id="o2426" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts [present or] absent.</text>
      <biological_entity id="o2427" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers chasmogamous and cleistogamous, nodding or pendulous in bud.</text>
      <biological_entity id="o2428" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
        <character constraint="in bud" constraintid="o2429" is_modifier="false" name="orientation" src="d0_s6" value="pendulous" value_original="pendulous" />
      </biological_entity>
      <biological_entity id="o2429" name="bud" name_original="bud" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Chasmogamous flowers: sepals persistent, 5, outer smaller than [equaling] inner;</text>
      <biological_entity id="o2430" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2431" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2432" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="than inner sepals" constraintid="o2433" is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="inner" id="o2433" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petals 5, yellow, sometimes purple to brown at or near bases;</text>
      <biological_entity id="o2434" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2435" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" constraint="at or near bases" constraintid="o2436" from="purple" modifier="sometimes" name="coloration" src="d0_s8" to="brown" />
      </biological_entity>
      <biological_entity id="o2436" name="base" name_original="bases" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 10–15+;</text>
      <biological_entity id="o2437" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2438" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments distinct, outer stamens often sterile;</text>
      <biological_entity id="o2439" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2440" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2441" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels 3;</text>
      <biological_entity id="o2442" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2443" name="carpel" name_original="carpels" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 0;</text>
      <biological_entity id="o2444" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2445" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 1, ± sessile, hemispheric [obconic].</text>
      <biological_entity id="o2446" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2447" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s13" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cleistogamous flowers similar;</text>
      <biological_entity id="o2448" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals 0;</text>
      <biological_entity id="o2449" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5–12.</text>
      <biological_entity id="o2450" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s16" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 3-valved.</text>
      <biological_entity id="o2451" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-valved" value_original="3-valved" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 6–50+ per capsule.</text>
      <biological_entity id="o2453" name="capsule" name_original="capsule" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity id="o2452" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per capsule" constraintid="o2453" from="6" name="quantity" src="d0_s18" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="x" id="o2454" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Calif.; c, w Europe, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="w Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tuberaria differs from Crocanthemum and Helianthemum primarily in habit, mostly opposite leaves, and sessile or subsessile stigmas.</discussion>
  <discussion>Species 8–12 (1 in the flora).</discussion>
  
</bio:treatment>