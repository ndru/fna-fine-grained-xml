<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
    <other_info_on_meta type="illustration_page">190</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Sterculioideae</taxon_name>
    <taxon_name authority="Marsili" date="unknown" rank="genus">firmiana</taxon_name>
    <taxon_name authority="(Linnaeus) W. Wight" date="unknown" rank="species">simplex</taxon_name>
    <place_of_publication>
      <publication_title>U.S.D.A. Bur. Pl. Industr. Bull.</publication_title>
      <place_in_publication>142: 67. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily sterculioideae;genus firmiana;species simplex;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242322646</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hibiscus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">simplex</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 2: 977. 1763, name and type proposed for conservation</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hibiscus;species simplex</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Firmiana</taxon_name>
    <taxon_name authority="(Linnaeus f.) Schott &amp; Endlicher" date="unknown" rank="species">platanifolia</taxon_name>
    <taxon_hierarchy>genus Firmiana;species platanifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Chinese parasol or bottle tree</other_name>
  <other_name type="common_name">Japanese varnish tree</other_name>
  <other_name type="common_name">phoenix tree</other_name>
  <other_name type="common_name">sycamore-leaf sterculia</other_name>
  <other_name type="common_name">varnish tree</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees single or multistemmed, 10–15 (–20) m;</text>
      <biological_entity id="o19139" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" value_original="multistemmed" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character char_type="range_value" from="10" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark green with paler greenish white vertical stripes, becoming gray or chalky white, smooth.</text>
      <biological_entity id="o19140" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character constraint="with stripes" constraintid="o19141" is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="chalky white" value_original="chalky white" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19141" name="stripe" name_original="stripes" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="paler greenish" value_original="paler greenish" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 15–30 (–40) cm;</text>
      <biological_entity id="o19142" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19143" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade palmately 3–5-lobed, rarely unlobed, broadly ovate, lobes often constricted at base, (8–) 12–40 × (12–) 20–50 cm, membranous, apex acute or acuminate, surfaces abaxially minutely stellate-puberulent with domatia in axils of primary and secondary-veins, adaxially glabrous.</text>
      <biological_entity id="o19144" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19145" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o19146" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o19147" is_modifier="false" modifier="often" name="size" src="d0_s3" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_length" notes="" src="d0_s3" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" notes="" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="atypical_width" notes="" src="d0_s3" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="width" notes="" src="d0_s3" to="50" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o19147" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o19148" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19149" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="with domatia" constraintid="o19150" is_modifier="false" modifier="abaxially minutely" name="pubescence" src="d0_s3" value="stellate-puberulent" value_original="stellate-puberulent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19150" name="domatium" name_original="domatia" src="d0_s3" type="structure" />
      <biological_entity id="o19151" name="axil" name_original="axils" src="d0_s3" type="structure" />
      <biological_entity constraint="primary" id="o19152" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure" />
      <relation from="o19150" id="r2018" name="in" negation="false" src="d0_s3" to="o19151" />
      <relation from="o19151" id="r2019" name="part_of" negation="false" src="d0_s3" to="o19152" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, erect, paniculate, 20–50 × 20–100 cm, often borne on leafy branches;</text>
      <biological_entity id="o19153" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="width" src="d0_s4" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19154" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o19153" id="r2020" modifier="often" name="borne on" negation="false" src="d0_s4" to="o19154" />
    </statement>
    <statement id="d0_s5">
      <text>bracts caducous.</text>
      <biological_entity id="o19155" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels articulated, 2–3 (–5) mm.</text>
      <biological_entity id="o19156" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="articulated" value_original="articulated" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 7–9 (–10) mm, divided nearly to base, tube cupuliform, lobes reflexed, lanceolate-oblong, abaxially puberulent, adaxially villous in throat proximally, androgynophore exserted, glabrous.</text>
      <biological_entity id="o19157" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19158" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character constraint="to base" constraintid="o19159" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o19159" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o19160" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cupuliform" value_original="cupuliform" />
      </biological_entity>
      <biological_entity id="o19161" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character constraint="in throat" constraintid="o19162" is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o19162" name="throat" name_original="throat" src="d0_s7" type="structure" />
      <biological_entity id="o19163" name="androgynophore" name_original="androgynophore" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: androgynophore to 2 cm;</text>
      <biological_entity id="o19164" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o19165" name="androgynophore" name_original="androgynophore" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers irregularly fascicled;</text>
      <biological_entity id="o19166" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o19167" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_arrangement" src="d0_s9" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillode obscured by anthers.</text>
      <biological_entity id="o19168" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o19169" name="pistillode" name_original="pistillode" src="d0_s10" type="structure">
        <character constraint="by anthers" constraintid="o19170" is_modifier="false" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o19170" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: androgynophore to 0.5 cm;</text>
      <biological_entity id="o19171" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19172" name="androgynophore" name_original="androgynophore" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s11" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary densely stellate-pubescent.</text>
      <biological_entity id="o19173" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19174" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Follicles pendant, ovatelanceolate, 6–11 × 2.5–4.5 cm, abaxially stellate-puberulent or glabrous.</text>
      <biological_entity id="o19175" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendant" value_original="pendant" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s13" to="11" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s13" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="stellate-puberulent" value_original="stellate-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds (1) 2–4, 6–7 mm diam. 2n = 40.</text>
      <biological_entity id="o19176" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s14" to="7" to_unit="mm" unit=",6-7 mm" />
        <character name="diameter" src="d0_s14" unit=",6-7 mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="diameter" src="d0_s14" to="4" unit=",6-7 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19177" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug; fruiting Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, thickets, mixed deciduous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="mixed deciduous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., D.C., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex., Va.; Asia (China); introduced also in Europe, e Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Firmiana simplex has long been cultivated in Japan and was brought to Europe by the mid eighteenth century. André Michaux reportedly introduced the species to North America before 1796 through his garden in Charleston, South Carolina (J. P. F. Deleuze 1804) and it is grown today as an ornamental or street tree in at least 20 states. The species is considered an invasive tree in the southeastern states, but only a few occurrences of its being naturalized have been well documented.</discussion>
  
</bio:treatment>