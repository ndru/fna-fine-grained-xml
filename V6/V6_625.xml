<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">multifida</taxon_name>
    <place_of_publication>
      <publication_title>Cybele Columb.</publication_title>
      <place_in_publication>1: 34. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species multifida;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101147</other_info_on_name>
  </taxon_identification>
  <number>20.</number>
  <other_name type="common_name">Cut-leaf checkerbloom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (0.1–) 0.2–0.4 (–0.6) m, pale-glaucous, with thick, rather woody taproot and simple or branched caudex, without rhizomes.</text>
      <biological_entity id="o4377" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.1" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.2" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.4" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pale-glaucous" value_original="pale-glaucous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4378" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="true" modifier="rather" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o4379" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o4380" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o4377" id="r511" name="with" negation="false" src="d0_s0" to="o4378" />
      <relation from="o4377" id="r512" name="without" negation="false" src="d0_s0" to="o4380" />
    </statement>
    <statement id="d0_s1">
      <text>Stems clustered, usually erect or ascending, sometimes proximally decumbent or prostrate, not rooting, solid, sparsely to densely appressed stellate-hairy.</text>
      <biological_entity id="o4381" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes proximally" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="sparsely to densely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal, persistent, to 10+;</text>
      <biological_entity id="o4382" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" notes="" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4383" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linearlanceolate to elliptic, 5–6 × 2 mm on proximal stem, 4–5 × 0.5 mm on distal stem;</text>
      <biological_entity id="o4384" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="6" to_unit="mm" />
        <character constraint="on proximal stem" constraintid="o4385" name="width" src="d0_s3" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4385" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s3" to="5" to_unit="mm" />
        <character constraint="on distal stem" constraintid="o4386" name="width" notes="" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4386" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 5.5–16 cm, basal 3–5 times longer than blades, reduced distally to 1/2 times as long as blades;</text>
      <biological_entity id="o4387" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s4" to="16" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="blade" constraintid="o4388" is_modifier="false" name="length_or_size" src="d0_s4" value="3-5 times longer than blades" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="blade" constraintid="o4389" is_modifier="false" name="length" src="d0_s4" value="0-1/2 times as long as blades" />
      </biological_entity>
      <biological_entity id="o4388" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o4389" name="blade" name_original="blades" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blades: basal reniform-orbiculate, palmately (5–) 7–9-lobed, deeply incised, (1.5–) 2.5–4 (–6) × (1.5–) 2.5–4 (–6) cm, base cordate, margins entire, apex rounded to acute, lobes linear to oblong, again deeply pinnately or ternately 3–5-lobed;</text>
      <biological_entity id="o4390" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o4391" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform-orbiculate" value_original="reniform-orbiculate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s5" value="(5-)7-9-lobed" value_original="(5-)7-9-lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s5" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s5" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4392" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o4393" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4394" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o4395" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblong" />
        <character is_modifier="false" modifier="again deeply; deeply pinnately; pinnately; ternately" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline ternately lobed, lobes linear to filiform, narrowest on distalmost leaves, somewhat fleshy, surfaces hairy, hairs appressed, stellate;</text>
      <biological_entity id="o4396" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity constraint="cauline" id="o4397" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="ternately" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o4398" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="filiform" />
        <character constraint="on distalmost leaves" constraintid="o4399" is_modifier="false" name="width" src="d0_s6" value="narrowest" value_original="narrowest" />
        <character is_modifier="false" modifier="somewhat" name="texture" notes="" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o4399" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o4400" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4401" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distalmost leaf lobes unlobed.</text>
      <biological_entity id="o4402" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <biological_entity constraint="leaf" id="o4403" name="lobe" name_original="lobes" src="d0_s7" type="structure" constraint_original="distalmost leaf">
        <character is_modifier="false" name="shape" src="d0_s7" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences erect, open, calyces not conspicuously overlapping except sometimes in bud, usually unbranched, usually 3–9 (–15) -flowered, elongate, 1-sided, to 7–25 cm;</text>
      <biological_entity id="o4404" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4405" name="calyx" name_original="calyces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not conspicuously" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="3-9(-15)-flowered" value_original="3-9(-15)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s8" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4406" name="bud" name_original="bud" src="d0_s8" type="structure" />
      <relation from="o4405" id="r513" name="except" negation="false" src="d0_s8" to="o4406" />
    </statement>
    <statement id="d0_s9">
      <text>bracts usually paired, linear or lanceolate to narrowly ovate, proximal divided to base, distalmost 2-fid or simple, 5 mm, shorter to longer than pedicels.</text>
      <biological_entity id="o4407" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s9" value="paired" value_original="paired" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="narrowly ovate" />
        <character is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character constraint="to base" constraintid="o4408" is_modifier="false" name="shape" src="d0_s9" value="divided" value_original="divided" />
        <character constraint="to base" constraintid="o4409" is_modifier="false" name="position" notes="" src="d0_s9" value="distalmost" value_original="distalmost" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="5" value_original="5" />
        <character constraint="than pedicels" constraintid="o4410" is_modifier="false" name="size_or_length" src="d0_s9" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o4408" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o4409" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o4410" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 3–8 (–10) mm;</text>
    </statement>
    <statement id="d0_s11">
      <text>involucellar bractlets absent.</text>
      <biological_entity id="o4411" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4412" name="bractlet" name_original="bractlets" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers usually bisexual or, infrequently, unisexual and pistillate, plants gynodioecious;</text>
      <biological_entity id="o4413" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character name="reproduction" src="d0_s12" value="," value_original="," />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4414" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="gynodioecious" value_original="gynodioecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calyx (6–) 7–10 mm, slightly enlarged in fruit, uniformly minutely stellate-puberulent;</text>
      <biological_entity id="o4415" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="in fruit" constraintid="o4416" is_modifier="false" modifier="slightly" name="size" src="d0_s13" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="uniformly minutely" name="pubescence" notes="" src="d0_s13" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
      <biological_entity id="o4416" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>petals pink to rose, pale-veined at least when dry, 9–20 (–25) mm, pistillate shortest;</text>
      <biological_entity id="o4417" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="rose" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s14" value="pale-veined" value_original="pale-veined" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="25" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="height_or_length" src="d0_s14" value="shortest" value_original="shortest" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>staminal column 3.3–7.5 mm, sparsely stellate-hairy;</text>
      <biological_entity constraint="staminal" id="o4418" name="column" name_original="column" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers pale yellowish to white;</text>
      <biological_entity id="o4419" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="pale yellowish" name="coloration" src="d0_s16" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 6 or 7.</text>
      <biological_entity id="o4420" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" unit="or" value="6" value_original="6" />
        <character name="quantity" src="d0_s17" unit="or" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Schizocarps 5–7 mm diam.;</text>
      <biological_entity id="o4421" name="schizocarp" name_original="schizocarps" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s18" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>mericarps 6 or 7, 3.5–4.3 mm, roughened, back finely glandularpuberulent, sides and back coarsely reticulate-veined, pitted, mucro 0.5–1 mm.</text>
      <biological_entity id="o4422" name="mericarp" name_original="mericarps" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="6" value_original="6" />
        <character name="quantity" src="d0_s19" unit="or" value="7" value_original="7" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s19" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o4423" name="side" name_original="sides" src="d0_s19" type="structure">
        <character is_modifier="true" modifier="back coarsely" name="architecture" src="d0_s19" value="reticulate-veined" value_original="reticulate-veined" />
        <character is_modifier="false" modifier="back finely; finely" name="relief" src="d0_s19" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o4424" name="mucro" name_original="mucro" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1.5–2 mm. 2n = 20.</text>
      <biological_entity id="o4425" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s20" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4426" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry places, sagebrush scrub, pinyon-juniper or pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry places" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1200–)2000–2500(–2900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="2000" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2900" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sidalcea multifida is generally a low-growing plant of high elevations, and usually can be distinguished by its highly glaucous, waxy, generally erect stems and leaves, and the generally basal and persistent seven- to nine-lobed leaf blades with pinnate or ternate lobes. It has been confused with S. glaucescens, to which it appears to be closely related and of which it may be found to be a variant or subspecies; as in S. glaucescens, the narrow inflorescence is often slightly curved between flowers. Sidalcea multifida can generally be distinguished from S. glaucescens by its seven- to nine-lobed leaf blades with more finely divided, ternate lobes, persisting basal leaves, range, and more erect habit. It occurs in Alpine, Mono, and Tulare counties in California, and from Lyon to Washoe counties in Nevada.</discussion>
  
</bio:treatment>