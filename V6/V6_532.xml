<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">sylvestris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 689. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species sylvestris;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">220008088</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">mauritiana</taxon_name>
    <taxon_hierarchy>genus Malva;species mauritiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sylvestris</taxon_name>
    <taxon_name authority="(Linnaeus) Boissier" date="unknown" rank="variety">mauritiana</taxon_name>
    <taxon_hierarchy>genus M.;species sylvestris;variety mauritiana</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">High or garden mallow</other_name>
  <other_name type="common_name">mauve des bois</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, biennial, or perennial, 0.5—1.5 (–3) m, hairy to glabrate, hairs simple and stellate.</text>
      <biological_entity id="o3216" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s0" to="glabrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3217" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, glabrous or sparsely hairy, hairs both simple and stellate.</text>
      <biological_entity id="o3218" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3219" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, lanceolate to ovate-triangular, 3–5 (–8) × 3 mm;</text>
      <biological_entity id="o3220" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3221" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate-triangular" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1/2 to 1 1/2 times as long as blade, pubescent in adaxial groove, otherwise glabrous;</text>
      <biological_entity id="o3222" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3223" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o3224" is_modifier="false" name="length" src="d0_s3" value="1/2-1-1/2 times as long as blade" />
        <character constraint="in adaxial groove" constraintid="o3225" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3224" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o3225" name="groove" name_original="groove" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade reniform to suborbiculate-cordate, unlobed or shallowly 3–7-lobed, sinuses to 1/2 to base, (2–) 5–10 (–14) × (2–) 5–10 (–14) cm, base cordate to ± truncate, lobes semicircular to oblong, margins crenate, apex rounded to wide-acute, surfaces glabrous or sparsely hairy, hairs simple or stellate.</text>
      <biological_entity id="o3226" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3227" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="suborbiculate-cordate unlobed or shallowly 3-7-lobed" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="suborbiculate-cordate unlobed or shallowly 3-7-lobed" />
      </biological_entity>
      <biological_entity id="o3228" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="to base" constraintid="o3229" from="0" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o3229" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" notes="alterIDs:o3229" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o3229" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="alterIDs:o3229" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_width" notes="alterIDs:o3229" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" notes="alterIDs:o3229" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" notes="alterIDs:o3229" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3230" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="more or less truncate" />
      </biological_entity>
      <biological_entity id="o3231" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s4" to="oblong" />
      </biological_entity>
      <biological_entity id="o3232" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o3233" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="wide-acute" />
      </biological_entity>
      <biological_entity id="o3234" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3235" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary or in 2–4-flowered fascicles, long-stalked.</text>
      <biological_entity id="o3236" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o3237" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="in 2-4-flowered fascicles" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="in 2-4-flowered fascicles" value_original="in 2-4-flowered fascicles" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="long-stalked" value_original="long-stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–2.5 cm, 1–4.5 cm in fruit, much longer than calyx, glabrous or sparsely stellate-hairy;</text>
      <biological_entity id="o3239" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o3240" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets distinct, sometimes adnate to calyx in basal 1 mm, oblong-lanceolate to elliptic or narrowly obovate, reticulate-veined, (3–) 4–5 (–7) × 2.5–4 mm, shorter than calyx, margins entire, surfaces glabrous, sparsely ciliate.</text>
      <biological_entity id="o3238" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o3239" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="4.5" to_unit="cm" />
        <character constraint="than calyx" constraintid="o3240" is_modifier="false" name="length_or_size" notes="" src="d0_s6" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o3241" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="to calyx" constraintid="o3242" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="oblong-lanceolate" modifier="in basal 1 mm" name="shape" notes="" src="d0_s7" to="elliptic or narrowly obovate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="reticulate-veined" value_original="reticulate-veined" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s7" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
        <character constraint="than calyx" constraintid="o3243" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3242" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o3243" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o3244" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3245" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx reticulate-veined, 5–6 mm, to 10 mm in fruit, lobes incompletely enclosing mericarps, stellate-puberulent;</text>
      <biological_entity id="o3246" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3247" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="reticulate-veined" value_original="reticulate-veined" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o3248" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3248" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o3249" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
      <biological_entity id="o3250" name="mericarp" name_original="mericarps" src="d0_s8" type="structure" />
      <relation from="o3249" id="r387" modifier="incompletely" name="enclosing" negation="false" src="d0_s8" to="o3250" />
    </statement>
    <statement id="d0_s9">
      <text>petals pink to purple or reddish purple with darker veins, usually drying blue, (12–) 16–30 (–45) mm, length 2 1/2–3 (–4) times calyx;</text>
      <biological_entity id="o3251" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3252" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink to purple" value_original="pink to purple" />
        <character constraint="with veins" constraintid="o3253" is_modifier="false" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" modifier="usually" name="condition" notes="" src="d0_s9" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="45" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
        <character constraint="calyx" constraintid="o3254" is_modifier="false" name="length" src="d0_s9" value="2-1/2-3(-4) times calyx" value_original="2-1/2-3(-4) times calyx" />
      </biological_entity>
      <biological_entity id="o3253" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o3254" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminal column 5 mm, minutely, retrorsely stellate-puberulent;</text>
      <biological_entity id="o3255" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o3256" name="column" name_original="column" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" modifier="minutely; retrorsely" name="pubescence" src="d0_s10" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 10–12-branched;</text>
      <biological_entity id="o3257" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3258" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="10-12-branched" value_original="10-12-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 10–12.</text>
      <biological_entity id="o3259" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3260" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Schizocarps 7 mm diam.;</text>
      <biological_entity id="o3261" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character name="diameter" src="d0_s13" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 10–12, 2–2.5 mm, margins sharp, not winged, sides thin and papery, with radiating veins, surface strongly to obscurely reticulate-wrinkled, usually glabrous, sometimes sparsely hairy.</text>
      <biological_entity id="o3262" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="12" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3263" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="sharp" value_original="sharp" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o3264" name="side" name_original="sides" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s14" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o3265" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="radiating" value_original="radiating" />
      </biological_entity>
      <biological_entity id="o3266" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly to obscurely" name="relief" src="d0_s14" value="reticulate-wrinkled" value_original="reticulate-wrinkled" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o3264" id="r388" name="with" negation="false" src="d0_s14" to="o3265" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5–2.2 mm. 2n = 42.</text>
      <biological_entity id="o3267" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3268" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides, farm yards, pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="farm yards" />
        <character name="habitat" value="pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., N.S., Ont., Que., Sask.; Calif., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; temperate Asia; n Africa; introduced also in Mexico, Central America (Guatemala, Honduras), South America (Argentina, Brazil, Colombia, Ecuador, Peru, Venezuela), Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="temperate Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="introduced" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="South America (Brazil)" establishment_means="introduced" />
        <character name="distribution" value="South America (Colombia)" establishment_means="introduced" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="introduced" />
        <character name="distribution" value="South America (Peru)" establishment_means="introduced" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva sylvestris is native throughout Europe except in the extreme north and into temperate Asia and northern Africa and is widely cultivated for food and ornament. It is the most commonly cultivated Malva in most of the</discussion>
  <discussion>United States. It is not very tolerant of hot, arid conditions. It is variable in habit, indument, leaf shape, and corolla size and color; most variants originated as selections/cultivars. The flowers and fruits indicate its close relationship with M. nicaeensis; the upright habit and much larger flowers allow an easy distinction.</discussion>
  
</bio:treatment>