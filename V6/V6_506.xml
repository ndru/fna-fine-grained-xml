<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="illustration_page">275</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">lavatera</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">thuringiaca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 691. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus lavatera;species thuringiaca;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101076</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Althaea</taxon_name>
    <taxon_name authority="(de Candolle) Alefeld" date="unknown" rank="species">ambigua</taxon_name>
    <taxon_hierarchy>genus Althaea;species ambigua</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="(Schur) Borbás" date="unknown" rank="species">vitifolia</taxon_name>
    <taxon_hierarchy>genus A.;species vitifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="(Linnaeus) Visiani" date="unknown" rank="species">thuringiaca</taxon_name>
    <taxon_hierarchy>genus Malva;species thuringiaca</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, sometimes appearing subshrublike, perennial, usually 1.5–2 m.</text>
      <biological_entity id="o21946" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short stellate-tomentose.</text>
      <biological_entity id="o21947" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-tomentose" value_original="stellate-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves reduced distally;</text>
      <biological_entity id="o21948" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linear to oblanceolate, 6 mm;</text>
      <biological_entity id="o21949" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles equaling blades proximally, progressively reduced distally on stem to 1/4 blade length;</text>
      <biological_entity id="o21950" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <biological_entity id="o21951" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character constraint="on stem" constraintid="o21952" is_modifier="false" modifier="progressively" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21952" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="1/4" />
      </biological_entity>
      <biological_entity id="o21953" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade grayish, cordate-orbiculate, usually palmately 3–5-lobed, usually 7–9 cm, lobes subacute to obtuse, margins crenate, apex acute, surfaces short-hairy.</text>
      <biological_entity id="o21954" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate-orbiculate" value_original="cordate-orbiculate" />
        <character is_modifier="false" modifier="usually palmately" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="7" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s5" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21955" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o21956" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o21957" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21958" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary flowers or racemelike, usually not congested, pedicellate.</text>
      <biological_entity id="o21959" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o21960" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
        <character is_modifier="false" modifier="usually not" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 2–5 cm, not elongating in fruit;</text>
      <biological_entity id="o21962" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>involucellar bractlets connate in lower 1/3, lobes wide-ovate, usually shorter than calyx, apex obtuse to abruptly acute.</text>
      <biological_entity id="o21961" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character constraint="in fruit" constraintid="o21962" is_modifier="false" modifier="not" name="length" src="d0_s7" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o21963" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character constraint="in lower 1/3" constraintid="o21964" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21964" name="1/3" name_original="1/3" src="d0_s8" type="structure" />
      <biological_entity id="o21965" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="wide-ovate" value_original="wide-ovate" />
        <character constraint="than calyx" constraintid="o21966" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o21966" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity id="o21967" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="abruptly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers showy;</text>
      <biological_entity id="o21968" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx divided halfway, campanulate, puberulent, not hispid, lobes narrowly acute to acuminate, ± enclosing fruit;</text>
      <biological_entity id="o21969" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="divided" value_original="divided" />
        <character is_modifier="false" name="position" src="d0_s10" value="halfway" value_original="halfway" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o21970" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <biological_entity id="o21971" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <relation from="o21970" id="r2324" modifier="more or less" name="enclosing" negation="false" src="d0_s10" to="o21971" />
    </statement>
    <statement id="d0_s11">
      <text>corolla 3–7 cm diam., petals pale purplish-pink, only occasionally with somewhat darker-pink veins, 2–3.5 cm, bases not overlapping (calyx surface visible between), apex notched;</text>
      <biological_entity id="o21972" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s11" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21973" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale purplish-pink" value_original="pale purplish-pink" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s11" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21974" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="somewhat" name="coloration" src="d0_s11" value="darker-pink" value_original="darker-pink" />
      </biological_entity>
      <biological_entity id="o21975" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s11" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o21976" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="notched" value_original="notched" />
      </biological_entity>
      <relation from="o21973" id="r2325" modifier="only occasionally; occasionally" name="with" negation="false" src="d0_s11" to="o21974" />
    </statement>
    <statement id="d0_s12">
      <text>staminal column glabrous;</text>
      <biological_entity constraint="staminal" id="o21977" name="column" name_original="column" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers on upper 3/4 of stamen column, pale-pink to white;</text>
      <biological_entity id="o21978" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" notes="" src="d0_s13" to="white" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21979" name="3/4" name_original="3/4" src="d0_s13" type="structure" />
      <biological_entity constraint="stamen" id="o21980" name="column" name_original="column" src="d0_s13" type="structure" />
      <relation from="o21978" id="r2326" name="on" negation="false" src="d0_s13" to="o21979" />
      <relation from="o21979" id="r2327" name="part_of" negation="false" src="d0_s13" to="o21980" />
    </statement>
    <statement id="d0_s14">
      <text>style 20–22-branched (same number as locules).</text>
      <biological_entity id="o21981" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="20-22-branched" value_original="20-22-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Mericarps 20–22, glabrous.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 42.</text>
      <biological_entity id="o21982" name="mericarp" name_original="mericarps" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s15" to="22" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21983" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Man., Sask.; Minn., Wyo.; c, e Europe, c Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e Europe" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lavatera thuringiaca is showy and occasionally cultivated as a garden ornamental. There are only a few records of the species as escaped or naturalized in North America. Old reports have indicated its presence in New Brunswick and Ontario based on escapes from cultivation that did not become established; there was also an apparently erroneous report from Quebec.</discussion>
  
</bio:treatment>