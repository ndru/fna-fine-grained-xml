<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="(Spach) R. Keller in H. G. A. Engler and K. Prantl" date="1893" rank="section">MYRIANDRA</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">apocynifolium</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 616. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section myriandra;species apocynifolium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100857</other_info_on_name>
  </taxon_identification>
  <number>15.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect, branches ascending, 4–7 dm.</text>
      <biological_entity id="o15519" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o15520" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes narrowly 4-winged at first, then 2-lined.</text>
      <biological_entity id="o15521" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o15522" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s1" value="4-winged" value_original="4-winged" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="2-lined" value_original="2-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades oblong to elliptic-oblong, 20–40 × 12–20 mm, base not articulated, broadly cuneate, margins usually plane, rarely recurved, apex rounded to retuse, midrib with 6 pairs of branches.</text>
      <biological_entity id="o15523" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="elliptic-oblong" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15524" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="articulated" value_original="articulated" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o15525" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o15526" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="retuse" />
      </biological_entity>
      <biological_entity id="o15527" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o15528" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o15529" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <relation from="o15527" id="r1668" name="with" negation="false" src="d0_s2" to="o15528" />
      <relation from="o15528" id="r1669" name="part_of" negation="false" src="d0_s2" to="o15529" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal (1–) 3–5 (–8) -flowered, narrowly branched.</text>
      <biological_entity id="o15530" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="(1-)3-5(-8)-flowered" value_original="(1-)3-5(-8)-flowered" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 15 mm diam.;</text>
      <biological_entity id="o15531" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="diameter" src="d0_s4" unit="mm" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals tardily deciduous, not enclosing capsule, 5, spatulate to elliptic or ovate, unequal, 3–5 × 1.5–2.3 mm;</text>
      <biological_entity id="o15532" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="elliptic or ovate" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15533" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <relation from="o15532" id="r1670" name="enclosing" negation="true" src="d0_s5" to="o15533" />
    </statement>
    <statement id="d0_s6">
      <text>petals 5, coppery yellow, oblong, 8–10 mm length 2 times sepals;</text>
      <biological_entity id="o15534" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="coppery yellow" value_original="coppery yellow" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o15535" is_modifier="false" name="length" src="d0_s6" value="2 times sepals" value_original="2 times sepals" />
      </biological_entity>
      <biological_entity id="o15535" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens deciduous, 60–80;</text>
      <biological_entity id="o15536" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="60" name="quantity" src="d0_s7" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary 3-merous, placentation incompletely axile.</text>
      <biological_entity id="o15537" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-merous" value_original="3-merous" />
        <character is_modifier="false" modifier="incompletely" name="placentation" src="d0_s8" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules cylindric-conic, 6–15 × 4.5–8 mm.</text>
      <biological_entity id="o15538" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric-conic" value_original="cylindric-conic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds scarcely carinate, 1.8–2 mm;</text>
      <biological_entity id="o15539" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s10" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>testa finely scalariform-reticulate.</text>
      <biological_entity id="o15540" name="testa" name_original="testa" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s11" value="scalariform-reticulate" value_original="scalariform-reticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks and moist woods, coastal plain and inland valleys</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="coastal plain" />
        <character name="habitat" value="valleys" modifier="inland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Fla., La., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypericum apocynifolium has been included in H. nudiflorum; it can be distinguished from the latter by the fewer, larger flowers with relatively longer, persistent sepals, the larger, thicker-walled capsules, and the seeds, which are ridged and straight rather than carinate and curved.</discussion>
  <discussion>A record from Georgia in the Flint River drainage has not been verified.</discussion>
  
</bio:treatment>