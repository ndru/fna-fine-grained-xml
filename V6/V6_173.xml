<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">99</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Hypericum</taxon_name>
    <taxon_name authority="Crantz" date="unknown" rank="species">maculatum</taxon_name>
    <taxon_name authority="(Tourlet) Hayek" date="unknown" rank="subspecies">obtusiusculum</taxon_name>
    <place_of_publication>
      <publication_title>Sched. Fl. Stiriac.</publication_title>
      <place_in_publication>23 – 24: 27. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section hypericum;species maculatum;subspecies obtusiusculum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100886</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypericum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">quadrangulum</taxon_name>
    <taxon_name authority="Tourlet" date="unknown" rank="subspecies">obtusiusculum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Bot. France</publication_title>
      <place_in_publication>50: 307. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hypericum;species quadrangulum;subspecies obtusiusculum</taxon_hierarchy>
  </taxon_identification>
  <number>48a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs erect, with rooting, not creeping, base, forming clumps, 1.5–10 dm.</text>
      <biological_entity id="o19425" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19426" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="true" modifier="not" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o19427" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <relation from="o19425" id="r2045" name="with" negation="false" src="d0_s0" to="o19426" />
      <relation from="o19425" id="r2046" name="forming" negation="false" src="d0_s0" to="o19427" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes (at least some) 4-lined, with black glands in raised lines.</text>
      <biological_entity id="o19428" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o19429" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
      </biological_entity>
      <biological_entity id="o19430" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o19431" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" value_original="raised" />
      </biological_entity>
      <relation from="o19429" id="r2047" name="with" negation="false" src="d0_s1" to="o19430" />
      <relation from="o19430" id="r2048" name="in" negation="false" src="d0_s1" to="o19431" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading, sessile;</text>
      <biological_entity id="o19432" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate, ovatelanceolate, oblong, or elliptic, 15–50 × 10–20 mm, base cuneate to rounded, margins plane, apex rounded, midrib with 2–3 pairs of branches, tertiary-veins not densely reticulate, black glands intramarginal (spaced), pale glands usually sparse or absent.</text>
      <biological_entity id="o19433" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19434" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o19435" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o19436" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o19437" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <biological_entity id="o19438" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o19439" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o19440" name="tertiary-vein" name_original="tertiary-veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not densely" name="architecture_or_coloration_or_relief" src="d0_s3" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o19441" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character is_modifier="false" name="position" src="d0_s3" value="intramarginal" value_original="intramarginal" />
      </biological_entity>
      <biological_entity id="o19442" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o19437" id="r2049" name="with" negation="false" src="d0_s3" to="o19438" />
      <relation from="o19438" id="r2050" name="part_of" negation="false" src="d0_s3" to="o19439" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences subcorymbiform to broadly pyramidal or cylindric, to 40-flowered.</text>
      <biological_entity id="o19443" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="subcorymbiform" value_original="subcorymbiform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="40-flowered" value_original="40-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 15–25 (–30) mm diam.;</text>
      <biological_entity id="o19444" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals not imbricate, spreading in fruit, broadly ovate to oblong, unequal to subequal, 4–5 × 2–3.5 mm, apex rounded-apiculate to erose-denticulate;</text>
      <biological_entity id="o19445" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character constraint="in fruit" constraintid="o19446" is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" notes="" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="unequal" name="size" src="d0_s6" to="subequal" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19446" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o19447" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded-apiculate" name="shape" src="d0_s6" to="erose-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals golden yellow, obovate-oblong to oblanceolate, 10–15 mm;</text>
      <biological_entity id="o19448" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="obovate-oblong" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 30–70;</text>
      <biological_entity id="o19449" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anther gland black;</text>
      <biological_entity id="o19450" name="anther" name_original="anther" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="gland black" value_original="gland black" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3–4 mm.</text>
      <biological_entity id="o19451" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules broadly ovoid, 7–9 × 5–7 mm, with longitudinal vittae.</text>
      <biological_entity id="o19452" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19453" name="vitta" name_original="vittae" src="d0_s11" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s11" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o19452" id="r2051" name="with" negation="false" src="d0_s11" to="o19453" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds scarcely carinate, 1 mm;</text>
      <biological_entity id="o19454" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s12" value="carinate" value_original="carinate" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>testa linear-reticulate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 32.</text>
      <biological_entity id="o19455" name="testa" name_original="testa" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s13" value="linear-reticulate" value_original="linear-reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19456" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, open sites, dry or damp</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Idaho, Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The western European lowland subspecies of Hypericum maculatum has been recorded from disturbed habitats near Vancouver and Prince Rupert regions in western British Columbia for more than 50 years. There is no evidence as to whether it has persisted over the whole of this period or has been reintroduced. The mainly eastern European and Siberian subsp. maculatum is diploid (2n = 16) and has entire sepals, dotted to shortly streaked (not lined) petals, and a narrower angle of branching (ca. 30° rather than ca. 50°).</discussion>
  
</bio:treatment>