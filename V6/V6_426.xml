<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Fryxell" date="unknown" rank="genus">BATESIMALVA</taxon_name>
    <place_of_publication>
      <publication_title>Bol. Soc. Bot. México</publication_title>
      <place_in_publication>35: 25, fig. 1. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus batesimalva;</taxon_hierarchy>
    <other_info_on_name type="etymology">For David M. Bates, b. 1935 American botanist, and Latin malva, mallow</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">103606</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Bates’s mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [herbs].</text>
      <biological_entity id="o2737" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, hairy, not viscid.</text>
      <biological_entity id="o2738" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, filiform, sometimes absent;</text>
      <biological_entity id="o2739" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2740" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to ovatelanceolate, not dissected or parted, base cordate, margins coarsely crenate.</text>
      <biological_entity id="o2741" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2742" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovatelanceolate not dissected or parted" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovatelanceolate not dissected or parted" />
      </biological_entity>
      <biological_entity id="o2743" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o2744" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, solitary flowers or 2–4-flowered clusters;</text>
      <biological_entity id="o2745" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2746" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucel absent.</text>
      <biological_entity id="o2747" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx not accrescent, not inflated, ca. 1/2-divided, shorter than mature fruit, lobes unribbed, lanceolate to ovate;</text>
      <biological_entity id="o2748" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2749" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character constraint="than mature fruit" constraintid="o2750" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o2750" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o2751" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="unribbed" value_original="unribbed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla blue-violet [bluish lavender, white, or yellow];</text>
      <biological_entity id="o2752" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2753" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue-violet" value_original="blue-violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column included;</text>
      <biological_entity id="o2754" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o2755" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 8–10 [–16] -branched;</text>
      <biological_entity id="o2756" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2757" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-10[-16]-branched" value_original="8-10[-16]-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas capitate.</text>
      <biological_entity id="o2758" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2759" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits schizocarps, erect or semipendent, inflated, disciform, prominently lobed, papery, minutely tomentose;</text>
      <biological_entity constraint="fruits" id="o2760" name="schizocarp" name_original="schizocarps" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="semipendent" value_original="semipendent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="disciform" value_original="disciform" />
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s11" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="texture" src="d0_s11" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>mericarps 8–10 [–16], 2-celled, without dorsal spur, apex rounded, proximal cell indehiscent, enclosing 1 seed (each seed covered by endoglossum), distal cell dehiscent, empty (by ovule abortion), unwinged.</text>
      <biological_entity id="o2761" name="mericarp" name_original="mericarps" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="16" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-celled" value_original="2-celled" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o2762" name="spur" name_original="spur" src="d0_s12" type="structure" />
      <biological_entity id="o2763" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2764" name="cell" name_original="cell" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o2765" name="seed" name_original="seed" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2766" name="cell" name_original="cell" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="empty" value_original="empty" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="unwinged" value_original="unwinged" />
      </biological_entity>
      <relation from="o2761" id="r347" name="without" negation="false" src="d0_s12" to="o2762" />
      <relation from="o2764" id="r348" name="enclosing" negation="false" src="d0_s12" to="o2765" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1 per mericarp, subglabrous to sparsely hairy.</text>
      <biological_entity id="o2768" name="mericarp" name_original="mericarp" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>x = 16.</text>
      <biological_entity id="o2767" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character constraint="per mericarp" constraintid="o2768" name="quantity" src="d0_s13" value="1" value_original="1" />
        <character char_type="range_value" from="subglabrous" name="pubescence" notes="" src="d0_s13" to="sparsely hairy" />
      </biological_entity>
      <biological_entity constraint="x" id="o2769" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico, n South America (w Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="n South America (w Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (1 in the flora).</discussion>
  
</bio:treatment>