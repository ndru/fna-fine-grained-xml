<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">46</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="illustration_page">43</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Seringe" date="unknown" rank="genus">lagenaria</taxon_name>
    <taxon_name authority="(Molina) Standley" date="unknown" rank="species">siceraria</taxon_name>
    <taxon_name authority="[F I]" date="unknown" rank="subspecies">siceraria</taxon_name>
    <taxon_hierarchy>family cucurbitaceae;genus lagenaria;species siceraria;subspecies siceraria</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101237</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cucurbita</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">lagenaria</taxon_name>
    <taxon_hierarchy>genus Cucurbita;species lagenaria</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lagenaria</taxon_name>
    <taxon_name authority="Rusby" date="unknown" rank="species">leucantha</taxon_name>
    <taxon_hierarchy>genus Lagenaria;species leucantha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="Seringe" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_hierarchy>genus L.;species vulgaris</taxon_hierarchy>
  </taxon_identification>
  <number>1a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–5 m, rooting at nodes.</text>
      <biological_entity id="o3101" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character constraint="at nodes" constraintid="o3102" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o3102" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 3–10 (–16) cm;</text>
      <biological_entity id="o3103" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o3104" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 3–25 (–40) × 4–25 (–40) cm, lobes obscure, rounded, apex apiculate.</text>
      <biological_entity id="o3105" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3106" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3107" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3108" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: pistillate peduncles 6–10 cm.</text>
      <biological_entity id="o3109" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o3110" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petal apex apiculate, corolla cream to white with darker veins, pale-yellow at base.</text>
      <biological_entity id="o3111" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="petal" id="o3112" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o3113" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with veins" constraintid="o3114" from="cream" name="coloration" src="d0_s4" to="white" />
        <character constraint="at base" constraintid="o3115" is_modifier="false" name="coloration" notes="" src="d0_s4" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o3114" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o3115" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pepos 10–50 cm (to 200 cm in some cultivated forms), [exocarp woody].</text>
      <biological_entity id="o3116" name="pepo" name_original="pepos" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seeds slightly tapered, slightly 2-horned on shoulders, with 2 flat facial ridges, 12–22 mm. 2n = 22.</text>
      <biological_entity id="o3117" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
        <character constraint="on shoulders" constraintid="o3118" is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="2-horned" value_original="2-horned" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3118" name="shoulder" name_original="shoulders" src="d0_s6" type="structure" />
      <biological_entity constraint="facial" id="o3119" name="ridge" name_original="ridges" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3120" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="22" value_original="22" />
      </biological_entity>
      <relation from="o3117" id="r377" name="with" negation="false" src="d0_s6" to="o3119" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gardens, trash heaps, fields, woods edges, railroad banks, roadsides, ditch banks, stream banks, commonly cultivated in home gardens and commercially, abandoned plantings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gardens" />
        <character name="habitat" value="trash heaps" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="woods edges" />
        <character name="habitat" value="railroad banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="ditch banks" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="home gardens" />
        <character name="habitat" value="abandoned plantings" modifier="commercially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Fla., Ga., Ill., Ky., La., Mass., Miss., Mo., N.Y., N.C., Okla., Pa., S.C., Tex., Va.; Asia; Africa; introduced also in West Indies, South America, Europe, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in West Indies" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>