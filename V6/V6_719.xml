<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">cistus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ladanifer</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 523. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus cistus;species ladanifer</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101212</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs to 25 dm, stems usually viscid.</text>
      <biological_entity id="o9174" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o9175" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="coating" src="d0_s0" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves sessile;</text>
      <biological_entity id="o9176" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 3-veined from base, elliptic, lanceolate-elliptic, lanceolate-linear, or oblong, 40–80 (–120) × 6–25 mm.</text>
      <biological_entity id="o9177" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o9178" is_modifier="false" name="architecture" src="d0_s2" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate-linear" value_original="lanceolate-linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate-linear" value_original="lanceolate-linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9178" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences solitary flowers.</text>
      <biological_entity id="o9179" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o9180" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 3, orbiculate;</text>
      <biological_entity id="o9181" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o9182" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals white, sometimes with red or yellow spot near bases, 30–50 mm;</text>
      <biological_entity id="o9183" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o9184" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9185" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow spot" value_original="yellow spot" />
      </biological_entity>
      <relation from="o9184" id="r980" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o9185" />
    </statement>
    <statement id="d0_s6">
      <text>styles 0–0.5 mm.</text>
      <biological_entity id="o9186" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o9187" name="style" name_original="styles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules 6–12-locular.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 18 (Europe).</text>
      <biological_entity id="o9188" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s7" value="6-12-locular" value_original="6-12-locular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9189" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, chaparral, roadsides, trails, abandoned plantings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="trails" />
        <character name="habitat" value="plantings" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">ladanifera</other_name>
  <discussion>Cistus ladanifer is known from the southern Central Coast Ranges, southern South Coast Ranges, Western Transverse Ranges, and the San Gabriel Mountains.</discussion>
  
</bio:treatment>