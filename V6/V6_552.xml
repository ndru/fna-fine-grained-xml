<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">304</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Moench" date="unknown" rank="genus">modiola</taxon_name>
    <taxon_name authority="(Linnaeus) G. Don" date="unknown" rank="species">caroliniana</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Hist.</publication_title>
      <place_in_publication>1: 466. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus modiola;species caroliniana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242433988</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">caroliniana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 688. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malva;species caroliniana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Modiola</taxon_name>
    <taxon_name authority="(Cavanilles) A. St.-Hilaire" date="unknown" rank="species">prostrata</taxon_name>
    <taxon_hierarchy>genus Modiola;species prostrata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="A. St.-Hilaire" date="unknown" rank="species">reptans</taxon_name>
    <taxon_hierarchy>genus M.;species reptans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="(Kunth) G. Don" date="unknown" rank="species">urticifolia</taxon_name>
    <taxon_hierarchy>genus M.;species urticifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Carolina bristlemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: flowering apices often ascending, branched, usually 0.2–0.5 m, often rooting at nodes.</text>
      <biological_entity id="o14504" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o14505" name="apex" name_original="apices" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.2" from_unit="m" modifier="usually" name="some_measurement" src="d0_s0" to="0.5" to_unit="m" />
        <character constraint="at nodes" constraintid="o14506" is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o14506" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules 3–4 × 1.5–3 mm;</text>
      <biological_entity id="o14507" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o14508" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole length 1–2 times blade;</text>
      <biological_entity id="o14509" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14510" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character constraint="blade" constraintid="o14511" is_modifier="false" name="length" src="d0_s2" value="1-2 times blade" value_original="1-2 times blade" />
      </biological_entity>
      <biological_entity id="o14511" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade 1.5–4 × 1.5–4 cm.</text>
      <biological_entity id="o14512" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14513" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels usually shorter than subtending petioles, hairy;</text>
      <biological_entity constraint="subtending" id="o14515" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>involucellar bractlets lanceolate, 4–5 mm.</text>
      <biological_entity id="o14514" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="than subtending petioles" constraintid="o14515" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14516" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 5–7 mm, hairy, hairs simple, 1–2 mm;</text>
      <biological_entity id="o14517" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o14518" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14519" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla erect, 6–8 mm;</text>
      <biological_entity id="o14520" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14521" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column yellowish;</text>
      <biological_entity id="o14522" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o14523" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers crowded at apex;</text>
      <biological_entity id="o14524" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14525" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character constraint="at apex" constraintid="o14526" is_modifier="false" name="arrangement" src="d0_s9" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o14526" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigmas equaling number of locules.</text>
      <biological_entity id="o14527" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14528" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character constraint="of locules" constraintid="o14529" is_modifier="false" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o14529" name="locule" name_original="locules" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Mericarps drying black, 5–6 mm, apical spines 1.5–3 mm.</text>
      <biological_entity id="o14530" name="mericarp" name_original="mericarps" src="d0_s11" type="structure">
        <character is_modifier="false" name="condition" src="d0_s11" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o14531" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1.5 mm. 2n = 18.</text>
      <biological_entity id="o14532" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14533" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, usually moist habitats, shores of ponds and reservoirs, low sandy areas, lawns, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed" />
        <character name="habitat" value="moist habitats" modifier="usually" />
        <character name="habitat" value="shores" constraint="of ponds and reservoirs" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="reservoirs" />
        <character name="habitat" value="low sandy areas" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ariz., Ark., Calif., Del., Fla., Ga., Ky., La., Mass., Miss., N.C., Okla., Oreg., Pa., S.C., Tenn., Tex., Va.; South America; introduced also in Mexico; Central America, Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Modiola caroliniana is somewhat weedy but not a serious agricultural weed. It has been reported in Delaware, Massachusetts, New Jersey, and Pennsylvania as a waif but doubtfully persists that far north. It is well established in the southeastern United States and is rather common as a lawn weed in some locations and as a garden weed in California. It probably came from southern South America in wool or cotton. Its closest relative, Modiolastrum K. Schumann, is known from southern South America.</discussion>
  
</bio:treatment>