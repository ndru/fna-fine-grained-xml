<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">291</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="illustration_page">289</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Demonstr. Pl.,</publication_title>
      <place_in_publication>18. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species parviflora;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242416823</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Small-flowered mallow</other_name>
  <other_name type="common_name">mauve parviflore</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 0.2–0.8 m.</text>
      <biological_entity id="o8250" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.8" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect or ascending, rarely decumbent, wide-branched, glabrous or sparsely stellate-hairy distally.</text>
      <biological_entity id="o8251" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="wide-branched" value_original="wide-branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, broadly lanceolate, 4–5 × 2–3 mm;</text>
      <biological_entity id="o8252" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8253" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–3 (–4) times as long as blade;</text>
      <biological_entity id="o8254" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8255" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o8256" is_modifier="false" name="length" src="d0_s3" value="2-3(-4) times as long as blade" />
      </biological_entity>
      <biological_entity id="o8256" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade suborbiculate-cordate or reniform, mostly shallowly 5–7-lobed or angled, 2–8 (–10) × 2–8 (–10) cm, base cordate (to nearly truncate), lobes deltate or rounded, margins evenly crenate, apex rounded to broadly acute, surfaces glabrous or hairy, especially at base, hairs simple and stellate.</text>
      <biological_entity id="o8257" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o8258" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate-cordate" value_original="suborbiculate-cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="mostly shallowly" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate-cordate" value_original="suborbiculate-cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="mostly shallowly" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="angled" value_original="angled" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8259" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o8260" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8261" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o8262" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o8263" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8264" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o8265" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o8263" id="r870" modifier="especially" name="at" negation="false" src="d0_s4" to="o8264" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary or in 2–4-flowered fascicles.</text>
      <biological_entity id="o8266" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o8267" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="in 2-4-flowered fascicles" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="in 2-4-flowered fascicles" value_original="in 2-4-flowered fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.2–0.4 cm, usually to 1 cm in fruit, shorter than calyx;</text>
      <biological_entity id="o8269" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o8270" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets distinct, not adnate to calyx, linear to filiform, (1–) 2–3 × 0.3 mm, shorter than calyx, margins entire, surfaces glabrous or slightly ciliate.</text>
      <biological_entity id="o8268" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s6" to="0.4" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8269" from="0" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character constraint="than calyx" constraintid="o8270" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8271" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="to calyx" constraintid="o8272" is_modifier="false" modifier="not" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s7" to="filiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="0.3" value_original="0.3" />
        <character constraint="than calyx" constraintid="o8273" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8272" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o8273" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o8274" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8275" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 3–4.5 mm, to 7–8 mm in fruit, glabrous or stellate-hairy, lobes widespreading outward in fruit, orbiculate-deltate, reticulate-veined, apex often abruptly acuminate, short-ciliate or not, scarious in fruit;</text>
      <biological_entity id="o8276" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8277" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8278" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o8278" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o8279" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o8280" is_modifier="false" name="orientation" src="d0_s8" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="orbiculate-deltate" value_original="orbiculate-deltate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="reticulate-veined" value_original="reticulate-veined" />
      </biological_entity>
      <biological_entity id="o8280" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o8281" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often abruptly" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="short-ciliate" value_original="short-ciliate" />
        <character name="architecture_or_pubescence_or_shape" src="d0_s8" value="not" value_original="not" />
        <character constraint="in fruit" constraintid="o8282" is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8282" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals white to pale lilac, drying pinkish or whitish, or faded, veins not darker, 3–4.5 (–5) mm, subequal to or only slightly longer than calyx, glabrous;</text>
      <biological_entity id="o8283" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8284" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale lilac drying pinkish or whitish" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale lilac drying pinkish or whitish" />
      </biological_entity>
      <biological_entity id="o8285" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="darker" value_original="darker" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" constraint="than calyx" constraintid="o8286" from="subequal" name="size" src="d0_s9" to="or only slightly longer" value="subequal to or only slightly longer" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8286" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminal column 1.5 mm, glabrous;</text>
      <biological_entity id="o8287" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o8288" name="column" name_original="column" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 10-branched or 11-branched;</text>
      <biological_entity id="o8289" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8290" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="10-branched" value_original="10-branched" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="11-branched" value_original="11-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 10 or 11.</text>
      <biological_entity id="o8291" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8292" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="or" value="10" value_original="10" />
        <character name="quantity" src="d0_s12" unit="or" value="11" value_original="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Schizocarps 6–7 mm diam.;</text>
      <biological_entity id="o8293" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 10 or 11, 2–2.5 mm, apical face strongly reticulate-wrinkled, sides appearing strongly, radially ribbed, margins sharpedged, toothed, narrowly winged, surface glabrous or hairy.</text>
      <biological_entity id="o8294" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="10" value_original="10" />
        <character name="quantity" src="d0_s14" unit="or" value="11" value_original="11" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8295" name="face" name_original="face" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s14" value="reticulate-wrinkled" value_original="reticulate-wrinkled" />
      </biological_entity>
      <biological_entity id="o8296" name="side" name_original="sides" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o8297" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="strongly; radially" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o8298" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o8296" id="r871" name="appearing" negation="false" src="d0_s14" to="o8297" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5–2 mm. 2n = 42.</text>
      <biological_entity id="o8299" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8300" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, usually dry, warm sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed" />
        <character name="habitat" value="dry" modifier="usually" />
        <character name="habitat" value="warm sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Ont., P.E.I., Que., Sask.; Ariz., Calif., Colo., Fla., Ga., Idaho, Iowa, Kans., La., Md., Mass., Mo., Mont., Nebr., Nev., N.J., N.Mex., N.Y., N.Dak., Okla., Oreg., S.C., S.Dak., Tex., Utah, Wash., Wyo.; Eurasia (possibly as far east as India); n Africa; introduced also in Mexico, West Indies, Central America, South America, elsewhere in Africa, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia (possibly as far east as India)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="elsewhere in Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva parviflora is native in southwestern Europe and the Mediterranean region to India; it is commonly introduced in many parts of the world. It is distinguished from similar species by its short petals (often equaling the calyx), the lack of darker lines on the petals, and the wide-spreading calyx lobes in fruit. The sharp-edged or winged mericarp with a conspicuously reticulate-pitted surface is likewise distinctive.</discussion>
  <discussion>Malva parviflora is more heat-tolerant than most Malva species. It is especially common as a weed from California to Texas. Northern records should be checked because some may be based upon waifs and others may be based on misidentifications. In some older floras, M. parviflora was confused with M. rotundifolia, a name rejected because of its inconsistent use for this as well as for M. pusilla and other species. It is sometimes cultivated as a forage crop in semi-arid regions.</discussion>
  
</bio:treatment>