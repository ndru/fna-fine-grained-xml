<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">lindheimeri</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 162. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species lindheimeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101191</other_info_on_name>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Woolly globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o12079" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, whitish, 2–7 dm, soft-pubescent.</text>
      <biological_entity id="o12080" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="soft-pubescent" value_original="soft-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades abaxially white to silvery, adaxially green, deltate-ovate, unlobed or 3-lobed, 4 cm, not rugose, base cordate to truncate, margins broadly crenate, surfaces stellate-pubescent.</text>
      <biological_entity id="o12081" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="abaxially white" name="coloration" src="d0_s2" to="silvery" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character name="distance" src="d0_s2" unit="cm" value="4" value_original="4" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o12082" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s2" to="truncate" />
      </biological_entity>
      <biological_entity id="o12083" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o12084" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemose, crowded, few-flowered, tip not leafy;</text>
      <biological_entity id="o12085" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets green to purple.</text>
      <biological_entity id="o12086" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o12087" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 8–15 mm;</text>
      <biological_entity id="o12088" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12089" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals red to red-pink, 15–25 mm;</text>
      <biological_entity id="o12090" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12091" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s6" to="red-pink" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow.</text>
      <biological_entity id="o12092" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12093" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps hemispheric;</text>
      <biological_entity id="o12094" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 18, 4 mm, chartaceous, nonreticulate dehiscent part 60–70% of height, tip acute, indehiscent part not wider than dehiscent part.</text>
      <biological_entity id="o12095" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="18" value_original="18" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o12096" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o12097" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="60-70%" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o12098" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="than dehiscent part" constraintid="o12099" is_modifier="false" name="width" src="d0_s9" value="not wider" value_original="not wider" />
      </biological_entity>
      <biological_entity id="o12099" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 2 or 3 per mericarp, brown to black, slightly pubescent.</text>
      <biological_entity id="o12100" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or per" value="2" value_original="2" />
        <character name="quantity" src="d0_s10" unit="or per" value="3" value_original="3" />
        <character char_type="range_value" from="brown" name="coloration" notes="" src="d0_s10" to="black" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12101" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil, open thickets or roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" />
        <character name="habitat" value="open thickets" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphaeralcea lindheimeri has unusually long and soft hairs and is found usually near the coast.</discussion>
  
</bio:treatment>