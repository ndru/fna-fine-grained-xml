<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lorin I. Nevling Jr.,Kerry Barringer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">thymelaeaceae</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="genus">THYMELAEA</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 3. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thymelaeaceae;genus THYMELAEA</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">132930</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, to 6 m.</text>
      <biological_entity id="o10616" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched, not jointed, slender, glabrous or sparsely pilose.</text>
      <biological_entity id="o10617" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves scattered along new growth, not clustered distally, often appressed to stem when young, sessile or subsessile;</text>
      <biological_entity id="o10618" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="along growth" constraintid="o10619" is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" modifier="not; distally" name="arrangement_or_growth_form" notes="" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="often" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o10619" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o10620" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when young" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to linearlanceolate, surfaces glabrous.</text>
      <biological_entity id="o10621" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o10622" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, solitary flowers or cymose clusters, sessile [shortly pedicellate];</text>
      <biological_entity id="o10623" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o10624" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, [linear] lanceolate, often with tuft of white hairs.</text>
      <biological_entity id="o10625" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o10626" name="tuft" name_original="tuft" src="d0_s5" type="structure" />
      <biological_entity id="o10627" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <relation from="o10625" id="r1167" modifier="often" name="with" negation="false" src="d0_s5" to="o10626" />
      <relation from="o10626" id="r1168" name="part_of" negation="false" src="d0_s5" to="o10627" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium tubular or urceolate;</text>
      <biological_entity id="o10628" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10629" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx 4-lobed, lobes spreading;</text>
      <biological_entity id="o10630" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10631" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
      <biological_entity id="o10632" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals absent;</text>
      <biological_entity id="o10633" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10634" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>disc minute or absent;</text>
      <biological_entity id="o10635" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10636" name="disc" name_original="disc" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 8, included or distal whorl partially exserted, adnate to hypanthium;</text>
      <biological_entity id="o10637" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10638" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o10639" name="whorl" name_original="whorl" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="partially" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character constraint="to hypanthium" constraintid="o10640" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o10640" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>style slightly exserted;</text>
      <biological_entity id="o10641" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10642" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma capitate.</text>
      <biological_entity id="o10643" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10644" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, green, becoming black, hard, enclosed by persistent hypanthium.</text>
      <biological_entity id="o10645" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="texture" src="d0_s13" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o10646" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="true" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o10645" id="r1169" name="enclosed by" negation="false" src="d0_s13" to="o10646" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, w Asia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 30 (1 in the flora).</discussion>
  <references>
    <reference>Pohl, R. W. 1955. Thymelaea passerina, new weed in the United States. Proc. Iowa Acad. Sci. 62: 152–154.</reference>
    <reference>Tan, K. 1980. Studies in the Thymelaeaceae: 2. A revision of the genus Thymelaea. Notes Roy. Bot. Gard. Edinburgh 38: 189–246.</reference>
  </references>
  
</bio:treatment>