<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">381</other_info_on_meta>
    <other_info_on_meta type="illustration_page">380</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">calenduleae</taxon_name>
    <taxon_name authority="Moench" date="1794" rank="genus">dimorphotheca</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">sinuata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>6: 72. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe calenduleae;genus dimorphotheca;species sinuata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242412953</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–30+ cm.</text>
      <biological_entity id="o4222" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades narrowly oblong or oblanceolate to linear, 10–50 (–100+) × 2–20 (–30+) mm, margins usually sinuately denticulate, sometimes serrate or entire, rarely pinnatifid.</text>
      <biological_entity id="o4223" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="linear" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="100" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4224" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually sinuately" name="shape" src="d0_s1" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles (2–) 5–15+ cm.</text>
      <biological_entity id="o4225" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 6–12+ mm.</text>
      <biological_entity id="o4226" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray corolla laminae abaxially yellow to orange (often marked with purple), adaxially mostly yellow to orange (sometimes purplish at bases and/or apices), 15–20 (–30+) mm.</text>
      <biological_entity constraint="corolla" id="o4227" name="lamina" name_original="laminae" src="d0_s4" type="structure" constraint_original="ray corolla">
        <character char_type="range_value" from="abaxially yellow" name="coloration" src="d0_s4" to="orange adaxially mostly yellow" />
        <character char_type="range_value" from="abaxially yellow" name="coloration" src="d0_s4" to="orange adaxially mostly yellow" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc-florets 15–50+;</text>
      <biological_entity id="o4228" name="disc-floret" name_original="disc-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s5" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas yellow to orange, usually purplish distally, 4–5 mm (lobe apices acute, terete, or dilated).</text>
      <biological_entity id="o4229" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="orange" />
        <character is_modifier="false" modifier="usually; distally" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray cypselae 4–5 mm;</text>
      <biological_entity constraint="ray" id="o4230" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>disc cypselae 6–8 mm. 2n = 18.</text>
      <biological_entity constraint="disc" id="o4231" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4232" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Dec–)Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ariz., Calif.; South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Plants treated here (and in horticultural trade) as Dimorphotheca sinuata are sometimes called “D. aurantiaca Hort., non de Candolle” and/or “D. calendulacea Harvey.” But for corolla colors, plants of D. sinuata are not easily distinguished from plants called D. pluvialis. They may prove to be better considered a color form of D. pluvialis.</discussion>
  
</bio:treatment>