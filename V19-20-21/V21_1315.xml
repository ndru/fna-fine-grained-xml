<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Ness) K. Schumann" date="1901" rank="species">cymosa</taxon_name>
    <place_of_publication>
      <publication_title>Just’s Bot. Jahresber.</publication_title>
      <place_in_publication>27(1): 528. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species cymosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067096</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Ness" date="unknown" rank="species">cymosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 21, plate 351. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lacinaria;species cymosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–75 cm.</text>
      <biological_entity id="o3078" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose to elongate-globose.</text>
      <biological_entity id="o3079" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="elongate-globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems strigoso-puberulent.</text>
      <biological_entity id="o3080" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigoso-puberulent" value_original="strigoso-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 1-nerved, linear to narrowly oblanceolate, 80–120 × 2–5 mm, gradually reduced distally or abruptly reduced on distal 1/2 of stems, essentially glabrous (margins proximally piloso-ciliate).</text>
      <biological_entity id="o3081" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="or" value_original="or" />
        <character constraint="on distal 1/2" constraintid="o3082" is_modifier="false" modifier="abruptly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3082" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o3083" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <relation from="o3082" id="r234" name="part_of" negation="false" src="d0_s3" to="o3083" />
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–20+) in open, cymiform arrays.</text>
      <biological_entity id="o3084" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o3085" from="2" name="atypical_quantity" src="d0_s4" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3085" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 20–70 mm (bracteate).</text>
      <biological_entity id="o3086" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindro-campanulate, 14–17 × 8–11 mm.</text>
      <biological_entity id="o3087" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 5–7 series, broadly oblong-obovate (outer) to broadly oblong, strongly unequal, sparsely short-pilose, margins without hyaline borders, ciliolate, apices rounded, sometimes mucronate.</text>
      <biological_entity id="o3088" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly oblong-obovate" name="shape" notes="" src="d0_s7" to="broadly oblong" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
      <biological_entity id="o3089" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <biological_entity id="o3090" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o3091" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o3092" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <relation from="o3088" id="r235" name="in" negation="false" src="d0_s7" to="o3089" />
      <relation from="o3090" id="r236" name="without" negation="false" src="d0_s7" to="o3091" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 20–25;</text>
      <biological_entity id="o3093" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous inside.</text>
      <biological_entity id="o3094" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 6–7 mm;</text>
      <biological_entity id="o3095" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles plumose.</text>
      <biological_entity id="o3096" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o3097" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o3098" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Post oak woodlands, fields, fencerows, openings, edges, clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak woodlands" modifier="post" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="openings" />
        <character name="habitat" value="edges" />
        <character name="habitat" value="clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Aggie-land or branched gayfeather</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>