<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">doellingeria</taxon_name>
    <taxon_name authority="Small" date="1898" rank="species">sericocarpoides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club.</publication_title>
      <place_in_publication>25: 620. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus doellingeria;species sericocarpoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066484</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Small) K. Schumann" date="unknown" rank="species">sericocarpoides</taxon_name>
    <taxon_hierarchy>genus Aster;species sericocarpoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">umbellatus</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">brevisquamus</taxon_name>
    <taxon_hierarchy>genus Aster;species umbellatus;variety brevisquamus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Miller) Nees" date="unknown" rank="species">umbellatus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">latifolius</taxon_name>
    <taxon_hierarchy>genus Aster;species umbellatus;variety latifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Doellingeria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbellata</taxon_name>
    <taxon_name authority="(A. Gray) House" date="unknown" rank="variety">latifolia</taxon_name>
    <taxon_hierarchy>genus Doellingeria;species umbellata;variety latifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–150 cm (short to long-rhizomatous).</text>
      <biological_entity id="o19615" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, ascending to erect, striate, glabrous proximal to heads.</text>
      <biological_entity id="o19616" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s1" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="to heads" constraintid="o19617" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o19617" name="head" name_original="heads" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: mid and distal crowded, blades lanceolate (proximal) to ovate (distal), 30–110 × 15–40 mm, reduced distally, stiff, bases cuneate, margins involute to weakly revolute, finely ciliate, apices acuminate, faces glabrous or sparsely hairy.</text>
      <biological_entity constraint="cauline" id="o19618" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="mid and distal" id="o19619" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o19620" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="110" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o19621" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19622" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="vernation_or_shape" src="d0_s2" value="involute to weakly" value_original="involute to weakly" />
        <character is_modifier="false" modifier="weakly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="finely" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19623" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19624" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads (8–) 30–130 (–200).</text>
      <biological_entity id="o19625" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s3" to="30" to_inclusive="false" />
        <character char_type="range_value" from="130" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="200" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s3" to="130" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–10 mm, sparsely to moderately canescent;</text>
      <biological_entity id="o19626" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts linear-lanceolate to broadly lanceolate.</text>
      <biological_entity id="o19627" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="broadly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3.2–6 mm.</text>
      <biological_entity id="o19628" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, midveins often swollen, translucent apically, apices broadly rounded, glabrate.</text>
      <biological_entity id="o19629" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o19630" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o19631" name="midvein" name_original="midveins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="apically" name="coloration_or_reflectance" src="d0_s7" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o19632" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o19629" id="r1820" name="in" negation="false" src="d0_s7" to="o19630" />
    </statement>
    <statement id="d0_s8">
      <text>Rays 2–7;</text>
      <biological_entity id="o19633" name="ray" name_original="rays" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae (6–) 8–12 (–14.5) × 1–3 mm.</text>
      <biological_entity id="o19634" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="14.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 4–13 (–20);</text>
      <biological_entity id="o19635" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="20" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 4–7 mm, lobes 2–4.2 mm, 60–75% of limbs.</text>
      <biological_entity id="o19636" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19637" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19638" name="limb" name_original="limbs" src="d0_s11" type="structure" />
      <relation from="o19637" id="r1821" modifier="60-75%" name="part_of" negation="false" src="d0_s11" to="o19638" />
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.5–3.7 mm, 6–8-ribbed, sparsely strigose;</text>
      <biological_entity id="o19639" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="6-8-ribbed" value_original="6-8-ribbed" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer 0.4–1.1 mm, inner 4–7 mm. 2n = 18.</text>
      <biological_entity id="o19640" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="outer" value_original="outer" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s13" value="inner" value_original="inner" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19641" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs, wet thickets and woods, coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., Md., N.J., N.C., Okla., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Southern tall flat-topped or southern whitetop aster</other_name>
  <discussion>Doellingeria sericocarpoides is uncommon in eastern Texas and southeastern Oklahoma.</discussion>
  
</bio:treatment>