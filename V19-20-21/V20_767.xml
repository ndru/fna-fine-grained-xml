<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="species">cervinus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 163. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species cervinus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066571</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">delicatus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species delicatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (10–) 15–30 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices or rhizomes sometimes with relatively short or elongate, slightly woody branches.</text>
      <biological_entity id="o6515" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o6516" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
      </biological_entity>
      <biological_entity id="o6517" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
      </biological_entity>
      <biological_entity id="o6518" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="true" modifier="slightly" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o6516" id="r588" name="with" negation="false" src="d0_s1" to="o6518" />
      <relation from="o6517" id="r589" name="with" negation="false" src="d0_s1" to="o6518" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending-erect, glabrous, sometimes minutely glandular distally.</text>
      <biological_entity id="o6519" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes minutely; distally" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (often persistent) and cauline;</text>
      <biological_entity id="o6520" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades spatulate to obovate or oblanceolate, (10–) 40–80 (–120) × 4–12 (–15) mm, cauline equal-sized or gradually reduced distally (bases sometimes weakly subclasping), margins entire, faces glabrous, eglandular.</text>
      <biological_entity constraint="basal" id="o6521" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="obovate or oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o6522" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="equal-sized" value_original="equal-sized" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6523" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6524" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–4).</text>
      <biological_entity id="o6525" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–7 × 9–14 mm.</text>
      <biological_entity id="o6526" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series, glabrous, densely minutely glandular.</text>
      <biological_entity id="o6527" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o6528" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o6527" id="r590" name="in" negation="false" src="d0_s7" to="o6528" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–45;</text>
      <biological_entity id="o6529" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue to purplish, 7–10 mm (mostly 1.5–3 mm wide), laminae coiling.</text>
      <biological_entity id="o6530" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="purplish" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6531" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.5–4.9 mm.</text>
      <biological_entity constraint="disc" id="o6532" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.5–1.7 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o6533" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o6534" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 12–15 bristles.</text>
      <biological_entity id="o6535" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o6536" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o6537" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
      <relation from="o6535" id="r591" name="outer of" negation="false" src="d0_s12" to="o6536" />
      <relation from="o6535" id="r592" name="inner of" negation="false" src="d0_s12" to="o6537" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky slopes and streamsides, seeps, crevices in walls, meadows, pine to fir woods, chaparral, sometimes over serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="open" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="crevices" constraint="in walls , meadows , pine to fir woods , chaparral , sometimes over serpentine" />
        <character name="habitat" value="walls" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="fir woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(50–)900–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="900" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>148.</number>
  <other_name type="common_name">Siskiyou fleabane</other_name>
  <discussion>Plants with relatively elongate basal leaves and cauline leaves little reduced distally (identified as Erigeron delicatus) are much different in general aspect; intermediates bridge the morphologic extremes.</discussion>
  
</bio:treatment>