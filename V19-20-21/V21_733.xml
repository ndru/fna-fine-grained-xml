<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">ANISOCARPUS</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 388. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus ANISOCARPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek anisos, unequal or dissimilar, and karpos, fruit, alluding to contrasting ray (fertile) and disc (sterile) ovaries in type species</other_info_on_name>
    <other_info_on_name type="fna_id">101819</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–80 cm.</text>
      <biological_entity id="o22332" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched from bases or throughout.</text>
      <biological_entity id="o22333" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o22334" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o22334" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o22335" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite (sometimes rosettes), distal alternate;</text>
      <biological_entity constraint="proximal" id="o22336" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>± sessile;</text>
      <biological_entity constraint="distal" id="o22337" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblong to linear, lance-linear, or oblanceolate, margins entire or toothed, faces hirsute to strigose or pubescent and (distal leaves) stipitate-glandular.</text>
      <biological_entity id="o22338" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong to linear" value_original="oblong to linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o22339" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o22340" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s5" to="strigose or pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly or in corymbiform or racemiform arrays.</text>
      <biological_entity id="o22341" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in arrays" constraintid="o22342" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in corymbiform or racemiform arrays" />
      </biological_entity>
      <biological_entity id="o22342" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0 at tips.</text>
      <biological_entity constraint="peduncular" id="o22343" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o22344" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o22345" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o22346" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character constraint="at tips" constraintid="o22347" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22347" name="tip" name_original="tips" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± globose or broadly ellipsoid to campanulate, 4–6+ mm diam.</text>
      <biological_entity id="o22348" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="less globose or broadly ellipsoid" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s8" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 0 (see paleae at receptacles) or falling, 1–3 or 7–15 in 1 series (lanceolate to lance-attenuate or oblanceolate, herbaceous, each 1/2 or fully enveloping subtended ray-floret proximally, ciliolate, abaxially stipitate-glandular, sometimes hirtellous).</text>
      <biological_entity id="o22349" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
        <character char_type="range_value" constraint="in series" constraintid="o22350" from="7" name="quantity" src="d0_s9" to="15" />
      </biological_entity>
      <biological_entity id="o22350" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, glabrous or setulose, paleate (paleae falling, in 1 series, between rays and discs, usually connate, sometimes distinct, phyllary-like, more scarious; in discoid heads, functionally an “involucre”).</text>
      <biological_entity id="o22351" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0, 1–3, or 8–15, pistillate, fertile;</text>
      <biological_entity id="o22352" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="3" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow.</text>
      <biological_entity id="o22353" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 5–30, bisexual and fertile, or functionally staminate;</text>
      <biological_entity id="o22354" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s13" to="30" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes shorter than funnelform throats, lobes 5, deltate (styles glabrous proximal to branches; anthers yellow).</text>
      <biological_entity id="o22355" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o22356" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o22357" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22357" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o22358" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ray cypselae (black or grayish) compressed or ± obcompressed, clavate, ± arcuate (basal attachments centered, apices beaked, beaks offset adaxially, 0.2–0.3 mm, faces glabrous or hairy);</text>
      <biological_entity constraint="ray" id="o22359" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
        <character is_modifier="false" modifier="more or less" name="course_or_shape" src="d0_s15" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 or coroniform.</text>
      <biological_entity id="o22360" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Disc cypselae (black or grayish) ± terete, clavate (± straight, faces hairy);</text>
      <biological_entity constraint="disc" id="o22361" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s17" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi of 5–8 or 11–21 lanceolate, linear, quadrate, or subulate, ciliolate-plumose, erose, or fimbrillate scales.</text>
      <biological_entity id="o22363" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbrillate" value_original="fimbrillate" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s18" to="8" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="ciliolate-plumose" value_original="ciliolate-plumose" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s18" value="erose" value_original="erose" />
        <character char_type="range_value" constraint="of " constraintid="o22364" from="11" name="quantity" src="d0_s18" to="21" />
      </biological_entity>
      <biological_entity id="o22364" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbrillate" value_original="fimbrillate" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s18" to="8" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="ciliolate-plumose" value_original="ciliolate-plumose" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s18" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="quadrate" value_original="quadrate" />
        <character constraint="of " constraintid="o22365" is_modifier="false" name="shape" src="d0_s18" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o22365" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbrillate" value_original="fimbrillate" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s18" to="8" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="ciliolate-plumose" value_original="ciliolate-plumose" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s18" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="quadrate" value_original="quadrate" />
        <character constraint="of " constraintid="o22366" is_modifier="false" name="shape" src="d0_s18" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o22366" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbrillate" value_original="fimbrillate" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s18" to="8" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="ciliolate-plumose" value_original="ciliolate-plumose" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s18" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o22362" id="r1539" name="consist_of" negation="false" src="d0_s18" to="o22363" />
    </statement>
    <statement id="d0_s19">
      <text>x = 7.</text>
      <biological_entity id="o22362" name="pappus" name_original="pappi" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o22367" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>347.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Following B. G. Baldwin (1999b), Anisocarpus comprises two species that have resided in different genera and different tribes. Anisocarpus scabridus was placed in Raillardella or Raillardiopsis—both regarded as members of Senecioneae until S. Carlquist’s (1959) anatomic studies. Anisocarpus madioides was treated in Madia and included in D. D. Keck’s (1959) informal “section Anisocarpus,” along with the other pappose species of Madia in the sense of Keck (now treated in Harmonia, Jensia, and Kyhosia). Molecular phylogenetic data support a sister-group relationship between A. madioides and A. scabridus (Baldwin 1996).</discussion>
  <discussion>Anisocarpus madioides and A. scabridus are the only perennial herbaceous tarweeds that combine non-scapiform capitulescences, radiate heads (at least in part), ellipsoid or spheric involucres, and yellow anthers, and are the only perennials in Madiinae with 2n = 14. The two species are highly distinct ecologically: A. madioides occurs in low- to mid-elevation, forest and woodland understories; A. scabridus occurs on high-elevation, exposed scree slopes and ridges.</discussion>
  <discussion>Artificial hybrids between the two species are vigorous, easily produced (with Anisocarpus madioides as seed parent), and largely pollen-sterile, except for large, diploid grains. B. G. Baldwin used diploid pollen from an F1 hybrid between A. madioides and A. scabridus in an artificial hybridization with a member of the closely-related Hawaiian silversword alliance, Dubautia knudsenii Hillebrand, a wet-forest tree from Kaua‘i (see M. Barrier et al. 1999).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves dark green, 40–130 mm, margins entire or toothed, apices acute; involucres ± globose, 4–6 mm; ray florets 8–15; disc florets functionally staminate; cypselae compressed; disc pappi of 5–8 linear, lanceolate, or quadrate, fimbrillate or erose scales 0.2–1.5 mm</description>
      <determination>1 Anisocarpus madioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves blue-green to grayish blue-green, 10–30 mm, margins entire, apices acute or obtuse; involucres broadly ellipsoid to campanulate, 6–12 mm; ray florets 0 or 1–3; disc florets bisexual, fertile; ray cypselae ± compressed; disc pappi of 11–21 subulate, ciliate-plumose scales 4–7 mm</description>
      <determination>2 Anisocarpus scabridus</determination>
    </key_statement>
  </key>
</bio:treatment>