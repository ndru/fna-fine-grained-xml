<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
    <other_info_on_meta type="illustration_page">329</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="(M. E. Jones) J. F. Macbride" date="1918" rank="species">gilensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">gilensis</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species gilensis;variety gilensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068634</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants upright to spreading, 22–40 cm.</text>
      <biological_entity id="o22855" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="upright" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="22" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 9–25 (–30) mm;</text>
      <biological_entity id="o22856" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22857" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lobes of mid and distal leaves elliptic to ovate, to 1 × 1 cm. 2n = 68–72.</text>
      <biological_entity id="o22858" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22859" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="1" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o22860" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="2n" id="o22861" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character char_type="range_value" from="68" name="quantity" src="d0_s2" to="72" />
      </biological_entity>
      <relation from="o22859" id="r1566" name="part_of" negation="false" src="d0_s2" to="o22860" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices igneous bluffs and boulders especially canyon walls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices igneous bluffs" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="canyon walls" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26a.</number>
  <other_name type="common_name">Gila rock daisy</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Proximal leaves of vars. gilensis and salensis may appear similar; lobes of the mid and distal leaves are elliptic to ovate in var. gilensis and linear to long-spatulate in var. salensis.</discussion>
  
</bio:treatment>