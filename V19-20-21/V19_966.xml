<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="treatment_page">554</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Vogt &amp; Oberprieler" date="1995" rank="genus">MAURANTHEMUM</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>44: 377. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus MAURANTHEMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin Mauros, a native of North Africa, and Greek anthemon, flower</other_info_on_name>
    <other_info_on_name type="fna_id">316902</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="B. H. Wilcox, K. Bremer &amp; Humphries," date="unknown" rank="genus">Leucoglossum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Nat. Hist. Mus. London, Bot.</publication_title>
      <place_in_publication>23: 142. 1993,</place_in_publication>
      <other_info_on_pub>not S. Imai 1942</other_info_on_pub>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>44: 377. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leucoglossum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals [perennials], 2–25 (–40) cm.</text>
      <biological_entity id="o13354" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5+, erect, usually branched near bases, glabrous.</text>
      <biological_entity id="o13355" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="near bases" constraintid="o13356" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13356" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o13357" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obovate or oblong to lanceolate or linear (bases sometimes clasping), usually irregularly 1-pinnately lobed or toothed, ultimate margins toothed or entire, faces glabrous (minutely gland-dotted abaxially).</text>
      <biological_entity id="o13358" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="lanceolate or linear" />
        <character is_modifier="false" modifier="usually irregularly 1-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13359" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13360" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in open, corymbiform arrays.</text>
      <biological_entity id="o13361" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in open , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o13362" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o13361" id="r1217" name="in" negation="false" src="d0_s6" to="o13362" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric, 8–12 (–15+) mm diam.</text>
      <biological_entity id="o13363" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 25–40+ in 3–4 series, distinct, ovate or oblong to lance-deltate or lanceolate (not carinate), unequal, margins and apices (pale-brown to black) scarious.</text>
      <biological_entity id="o13364" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o13365" from="25" name="quantity" src="d0_s8" to="40" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="lance-deltate or lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o13365" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o13366" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o13367" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles ± conic, epaleate.</text>
      <biological_entity id="o13368" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 10–21+, pistillate and fertile, or styliferous and sterile;</text>
      <biological_entity id="o13369" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="21" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas mostly white (usually yellowish at bases, drying pinkish), laminae oblong to ovate.</text>
      <biological_entity id="o13370" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o13371" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 60–100+, bisexual, fertile;</text>
      <biological_entity id="o13372" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas ± yellow, tubes ± cylindric (basally dilated, not gland-dotted), throats ± campanulate, lobes (2–) 5, deltate (without resin sacs).</text>
      <biological_entity id="o13373" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13374" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o13375" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o13376" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s13" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae mostly columnar to obovoid or fusiform, outer sometimes 3-angled, ribs 7–10, faces glabrous (pericarps with myxogenic cells on ribs and resin sacs between ribs);</text>
      <biological_entity id="o13377" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="mostly columnar" name="shape" src="d0_s14" to="obovoid or fusiform" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13378" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity id="o13379" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
      <biological_entity id="o13380" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0 (sterile ovaries of rays usually with apical coronas).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o13381" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o13382" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; sw Europe, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw Europe" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>133.</number>
  <discussion>Species ca. 4 (1 in the flora).</discussion>
  
</bio:treatment>