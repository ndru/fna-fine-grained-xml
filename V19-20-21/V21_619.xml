<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
    <other_info_on_meta type="illustration_page">249</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="subtribe">flaveriinae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">flaveria</taxon_name>
    <taxon_name authority="(Sprengel) C. Mohr" date="1901" rank="species">trinervia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>6: 810. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe flaveriinae;genus flaveria;species trinervia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066777</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Odera</taxon_name>
    <taxon_name authority="Sprengel" date="unknown" rank="species">trinervia</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gart. Halle,</publication_title>
      <place_in_publication>63. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Odera;species trinervia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 200+ cm (delicate or robust, glabrate or glabrous).</text>
      <biological_entity id="o24167" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o24168" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves petiolate (proximal, petioles 10–20 mm) or sessile (distal);</text>
      <biological_entity id="o24169" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades lanceolate or oblanceolate to elliptic or subovate, 30–150 × (7–) 10–40 mm, bases (distal) connate, margins serrate, serrate-dentate, or spinulose-serrate.</text>
      <biological_entity id="o24170" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic or subovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="150" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24171" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o24172" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate-dentate" value_original="serrate-dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinulose-serrate" value_original="spinulose-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 30–300+, in tight, axillary, sessile glomerules (receptacles of glomerules setose).</text>
      <biological_entity id="o24173" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s4" to="300" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24174" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s4" value="tight" value_original="tight" />
        <character is_modifier="true" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o24173" id="r1657" name="in" negation="false" src="d0_s4" to="o24174" />
    </statement>
    <statement id="d0_s5">
      <text>Calyculi 0.</text>
      <biological_entity id="o24175" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres oblong and cylindric or angular, 3.8–4.5 mm.</text>
      <biological_entity id="o24176" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="angular" value_original="angular" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually 2, oblong (closely investing and falling with mature cypselae).</text>
      <biological_entity id="o24177" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0–1;</text>
      <biological_entity id="o24178" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae pale-yellow or whitish, oblique or suborbiculate, 0.5–1 mm.</text>
      <biological_entity id="o24179" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblique" value_original="oblique" />
        <character is_modifier="false" name="shape" src="d0_s9" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 0–1 (–2);</text>
      <biological_entity id="o24180" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="2" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla-tubes 0.5–1.4 mm, throats campanulate, 0.5–0.8 mm.</text>
      <biological_entity id="o24181" name="corolla-tube" name_original="corolla-tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24182" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae oblanceoloid to subclavate, 2–2.6 mm (rays longer);</text>
      <biological_entity id="o24183" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblanceoloid" name="shape" src="d0_s12" to="subclavate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0.2n = 36.</text>
      <biological_entity id="o24184" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24185" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Near water, saline and gypseous areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="near water" />
        <character name="habitat" value="saline" />
        <character name="habitat" value="gypseous areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Fla., Mass., Mo., N.Mex., Tex., Va.; West Indies; Central America (British Honduras); South America (Brazil, Ecuador, Peru, Venezuela); probably introduced in Asia (India, Middle East); Africa; Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America (British Honduras)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
        <character name="distribution" value="probably  in Asia (India)" establishment_means="introduced" />
        <character name="distribution" value="probably  in Asia (Middle East)" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Clustered yellowtops</other_name>
  <discussion>Flaveria trinervia is widespread and weedy; it often occurs in saline, gypseous, disturbed areas near permanent or ephemeral water sources in southern Florida and from Texas to southern California. It occurs also in scattered locations in some eastern states and has been reported from Alabama.</discussion>
  <discussion>The heads of Flaveria trinervia, which usually contain just one floret, are either radiate or discoid; radiate heads tend to occur on the periphery of setose glomerules. Reduction of some of the floral features, including number of florets [0–1(–2)], phyllaries per head (2), and size of ray laminae, suggest that F. trinervia may be the most derived species in the genus.</discussion>
  
</bio:treatment>