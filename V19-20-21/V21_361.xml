<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1821" rank="species">petiolaris</taxon_name>
    <taxon_name authority="Heiser" date="1958" rank="subspecies">fallax</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>60: 279, fig. 3. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species petiolaris;subspecies fallax</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068445</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually ± hispid, rarely ± hirsute to glabrate.</text>
      <biological_entity id="o24594" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
        <character char_type="range_value" from="less hirsute" name="pubescence" src="d0_s0" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: abaxial faces sparsely, if at all, glanddotted.</text>
      <biological_entity id="o24595" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="abaxial" id="o24596" name="face" name_original="faces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles usually with leafy bracts subtending heads.</text>
      <biological_entity id="o24597" name="peduncle" name_original="peduncles" src="d0_s2" type="structure" />
      <biological_entity id="o24598" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o24599" name="head" name_original="heads" src="d0_s2" type="structure" />
      <relation from="o24597" id="r1675" name="with" negation="false" src="d0_s2" to="o24598" />
      <relation from="o24598" id="r1676" name="subtending" negation="false" src="d0_s2" to="o24599" />
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 2–3.5 mm wide.</text>
      <biological_entity id="o24600" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas: throats gradually narrowed distal to slight, not densely hairy basal bulges.</text>
      <biological_entity constraint="disc" id="o24601" name="corolla" name_original="corollas" src="d0_s4" type="structure" />
      <biological_entity id="o24602" name="throat" name_original="throats" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="slight" value_original="slight" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2n = 34.</text>
      <biological_entity constraint="basal" id="o24603" name="bulge" name_original="bulges" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="not densely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24604" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  
</bio:treatment>