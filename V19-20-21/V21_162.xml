<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
    <other_info_on_meta type="illustration_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chrysogonum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 920. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus chrysogonum;species virginianum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220002876</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 2.5–10 cm, faces minutely strigoso-hirsutulous to villoso-hirsute and stipitate-glandular (hairs ca. 0.1 mm).</text>
      <biological_entity id="o2155" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="distance" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2156" name="face" name_original="faces" src="d0_s0" type="structure">
        <character char_type="range_value" from="minutely strigoso-hirsutulous" name="pubescence" src="d0_s0" to="villoso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 2–22 cm.</text>
      <biological_entity id="o2157" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Paleae ca. 4 mm.</text>
      <biological_entity id="o2158" name="palea" name_original="paleae" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray corollas 6–17 mm, laminae broadly elliptic, apices 3-toothed.</text>
      <biological_entity constraint="ray" id="o2159" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2160" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o2161" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas 2.5–2.7 mm.</text>
      <biological_entity constraint="disc" id="o2162" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 3–4.5 × 2–2.5 mm.</text>
      <biological_entity id="o2163" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., D.C., Fla., Ga., Ky., La., Md., Miss., N.C., N.Y., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Green and gold</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants not stoloniferous; earliest flowering stems leafless, later ones leafy and mostly 15–35(–50) cm</description>
      <determination>1a Chrysogonum virginianum var. virginianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants stoloniferous (colonial, mat-forming); flowering stems leafless and/or leafy, leafy stems (2–)15–25 cm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Longest stolon internodes 2–6 cm; earliest flowering stems leafless, mostly 2–25 cm (later flowering stems leafy, 15–25 cm)</description>
      <determination>1b Chrysogonum virginianum var. brevistolon</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Longest stolon internodes 12–60 cm; earliest (and all other) flowering stems leafless, 2–10 cm</description>
      <determination>1c Chrysogonum virginianum var. australe</determination>
    </key_statement>
  </key>
</bio:treatment>