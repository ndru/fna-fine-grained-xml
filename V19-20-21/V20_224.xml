<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Rydberg) Semple" date="2003" rank="subsection">humiles</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="species">simplex</taxon_name>
    <taxon_name authority="(Porter) G. S. Ringius" date="1991" rank="variety">monticola</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 397. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection humiles;species simplex;variety monticola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068791</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">puberula</taxon_name>
    <taxon_name authority="Porter" date="unknown" rank="variety">monticola</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>19: 129. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;species puberula;variety monticola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="(Porter) Britton" date="unknown" rank="species">randii</taxon_name>
    <taxon_name authority="(Porter) Fernald" date="unknown" rank="variety">monticola</taxon_name>
    <taxon_hierarchy>genus Solidago;species randii;variety monticola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">virgaurea</taxon_name>
    <taxon_name authority="Porter" date="unknown" rank="variety">deanei</taxon_name>
    <taxon_hierarchy>genus Solidago;species virgaurea;variety deanei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virgaurea</taxon_name>
    <taxon_name authority="Porter" date="unknown" rank="variety">redfieldii</taxon_name>
    <taxon_hierarchy>genus Solidago;species virgaurea;variety redfieldii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (10–) 18–53 (–83) cm.</text>
      <biological_entity id="o5452" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="18" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="53" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="83" to_unit="cm" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s0" to="53" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal obovate to spatulate, margins crenate to sharply serrate, acute or somewhat obtuse;</text>
      <biological_entity id="o5453" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o5454" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s1" to="spatulate" />
      </biological_entity>
      <biological_entity id="o5455" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s1" to="sharply serrate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline (3–) 9–26 (–35), proximal (42–) 62–113 (–159) × (5–) 10–22 (–31) mm, margins crenate to serrate;</text>
      <biological_entity id="o5456" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o5457" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s2" to="9" to_inclusive="false" />
        <character char_type="range_value" from="26" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="35" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s2" to="26" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5458" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="42" from_unit="mm" name="atypical_length" src="d0_s2" to="62" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="113" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="159" to_unit="mm" />
        <character char_type="range_value" from="62" from_unit="mm" name="length" src="d0_s2" to="113" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="31" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5459" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s2" to="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid to distal 11.5–39.5 (–76) × (1.3–) 2–8.4 (–14.5) mm.</text>
      <biological_entity id="o5460" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5461" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="39.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="76" to_unit="mm" />
        <character char_type="range_value" from="11.5" from_unit="mm" name="length" src="d0_s3" to="39.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_width" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="14.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="8.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in wandlike racemiform or wandlike to sometimes open paniculiform arrays.</text>
      <biological_entity id="o5462" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o5463" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="shape" src="d0_s4" value="wand-like" value_original="wandlike" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="shape" src="d0_s4" value="wand-like" value_original="wandlike" />
      </biological_entity>
      <relation from="o5462" id="r492" name="in" negation="false" src="d0_s4" to="o5463" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles: bracteoles (0–) 1–4 (–6).</text>
      <biological_entity id="o5464" name="peduncle" name_original="peduncles" src="d0_s5" type="structure" />
      <biological_entity id="o5465" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="1" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries: mid less than 0.9 mm wide 1 mm below apices.</text>
      <biological_entity id="o5466" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity constraint="mid" id="o5467" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="0.9" to_unit="mm" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5468" name="apex" name_original="apices" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Cypselae sparsely to moderately strigose.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 36.</text>
      <biological_entity id="o5469" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5470" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry serpentine soils, granitic rocky barren uplands of mountains below alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry serpentine soils" />
        <character name="habitat" value="granitic rocky barren uplands" constraint="of mountains below alpine slopes" />
        <character name="habitat" value="mountains" constraint="below alpine slopes" />
        <character name="habitat" value="alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–800+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800+" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Que.; Maine, Mass., N.H., N.Y., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.1.</number>
  <other_name type="common_name">Rand’s goldenrod</other_name>
  <other_name type="common_name">verge d’or simple de la serpentine</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety monticola is found as rare disjunct populations throughout its range.</discussion>
  
</bio:treatment>