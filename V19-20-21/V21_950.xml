<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="genus">PEUCEPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 74. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus PEUCEPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek peuke, pine or fir, and phyllon, leaf</other_info_on_name>
    <other_info_on_name type="fna_id">124752</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or treelets, mostly 100–300 cm.</text>
      <biological_entity id="o4629" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o4630" name="treelet" name_original="treelets" src="d0_s0" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–5+), branched.</text>
      <biological_entity id="o4631" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o4632" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear-filiform, rarely with 1–2 lateral lobes, ultimate margins entire, faces glabrous, glanddotted.</text>
      <biological_entity id="o4633" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-filiform" value_original="linear-filiform" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4634" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o4635" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4636" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4633" id="r359" modifier="rarely" name="with" negation="false" src="d0_s5" to="o4634" />
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly.</text>
      <biological_entity id="o4637" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate to campanulate, 6–12 mm diam.</text>
      <biological_entity id="o4638" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s7" to="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent or tardily falling, 8–18 in ± 2 series (linear to lanceolate, outer intergrading with subtending leaves).</text>
      <biological_entity id="o4639" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o4640" from="8" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
      <biological_entity id="o4640" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, pitted or knobby, epaleate.</text>
      <biological_entity id="o4641" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="shape" src="d0_s9" value="knobby" value_original="knobby" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0.</text>
      <biological_entity id="o4642" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 12–21, bisexual, fertile;</text>
      <biological_entity id="o4643" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="21" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas creamy yellow, distally purplish (stipitate-glandular), tubes shorter than cylindric throats, lobes 5, erect, deltate (style-branch appendages rounded-truncate, papillate).</text>
      <biological_entity id="o4644" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="creamy yellow" value_original="creamy yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o4645" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character constraint="than cylindric throats" constraintid="o4646" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4646" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o4647" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae obconic to obpyramidal, hirsute (hairs tawny to reddish);</text>
      <biological_entity id="o4648" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s13" to="obpyramidal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of 30–60 bristles subtending 15–20 subulate-aristate scales, or of ca. 120 bristles.</text>
      <biological_entity id="o4650" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s14" to="60" />
        <character is_modifier="false" name="position" src="d0_s14" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o4651" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="subulate-aristate" value_original="subulate-aristate" />
      </biological_entity>
      <biological_entity id="o4652" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="120" value_original="120" />
      </biological_entity>
      <relation from="o4649" id="r360" name="consist_of" negation="false" src="d0_s14" to="o4650" />
      <relation from="o4649" id="r361" name="consist_of" negation="false" src="d0_s14" to="o4652" />
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o4649" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
      <biological_entity constraint="x" id="o4653" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>363.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>