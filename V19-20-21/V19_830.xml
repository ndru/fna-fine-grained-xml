<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">496</other_info_on_meta>
    <other_info_on_meta type="illustration_page">497</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">chamaemelum</taxon_name>
    <taxon_name authority="(Linnaeus) Allioni" date="1785" rank="species">nobile</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Pedem.</publication_title>
      <place_in_publication>1: 185. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus chamaemelum;species nobile</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200023646</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anthemis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nobilis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 894. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anthemis;species nobilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–20 (–30) cm across.</text>
      <biological_entity id="o3040" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly prostrate (much branched, often forming mats), ± strigoso-sericeous to villous.</text>
      <biological_entity id="o3041" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="less strigoso-sericeous" name="pubescence" src="d0_s1" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o3042" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblong, 1–3 (–5) cm, 2–3-pinnately lobed.</text>
      <biological_entity id="o3043" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="2-3-pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4–6 × 7–10+ mm.</text>
      <biological_entity id="o3044" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: margins and apices greenish or lacking pigment, abaxial faces ± villous.</text>
      <biological_entity id="o3045" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity id="o3046" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pigment" value_original="pigment" />
      </biological_entity>
      <biological_entity id="o3047" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pigment" value_original="pigment" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3048" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae 3–4+ mm, margins greenish or lacking pigment.</text>
      <biological_entity id="o3049" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3050" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pigment" value_original="pigment" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets usually 13–21+, rarely 0;</text>
      <biological_entity id="o3051" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="21" upper_restricted="false" />
        <character modifier="rarely" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae 7–10+ mm.</text>
      <biological_entity id="o3052" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 2–3 mm.</text>
      <biological_entity constraint="disc" id="o3053" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1–1.5 mm. 2n = 18.</text>
      <biological_entity id="o3054" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3055" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Conn., Del., Ill., Ind., Md., N.J., N.Y., N.C., Ohio, Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>