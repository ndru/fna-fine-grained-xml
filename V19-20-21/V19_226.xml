<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">193</other_info_on_meta>
    <other_info_on_meta type="illustration_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">centaurea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">solstitialis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 917. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus centaurea;species solstitialis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416255</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–100 cm.</text>
      <biological_entity id="o1284" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or often branched from base, forming rounded bushy plants, gray-tomentose.</text>
      <biological_entity id="o1285" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o1286" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity id="o1286" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o1287" name="plant" name_original="plants" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="growth_form" src="d0_s1" value="bushy" value_original="bushy" />
      </biological_entity>
      <relation from="o1285" id="r122" name="forming" negation="false" src="d0_s1" to="o1287" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves gray-tomentose and scabrous to short-bristly;</text>
      <biological_entity id="o1288" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s2" to="short-bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline petiolate or tapered to base, usually absent at anthesis, blades 5–15 cm, margins pinnately lobed or dissected;</text>
      <biological_entity constraint="basal and proximal cauline" id="o1289" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character constraint="to base" constraintid="o1290" is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" notes="" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1290" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o1291" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1292" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline long-decurrent, blades linear to oblong, 1–10 cm, entire.</text>
      <biological_entity constraint="cauline" id="o1293" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
      <biological_entity id="o1294" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads disciform, borne singly or in open leafy arrays, long-pedunculate.</text>
      <biological_entity id="o1295" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="disciform" value_original="disciform" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in open leafy arrays" value_original="in open leafy arrays" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="long-pedunculate" value_original="long-pedunculate" />
      </biological_entity>
      <biological_entity id="o1296" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o1295" id="r123" name="in" negation="false" src="d0_s5" to="o1296" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres ovoid, 13–17 mm, loosely cobwebby-tomentose or becoming glabrous.</text>
      <biological_entity id="o1297" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s6" value="cobwebby-tomentose" value_original="cobwebby-tomentose" />
        <character is_modifier="false" modifier="becoming; becoming" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Principal phyllaries: bodies pale green, ovate, appendages stramineous to brown, each with palmately radiating cluster of spines, and stout central spine 10–25 mm.</text>
      <biological_entity constraint="principal" id="o1298" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o1299" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o1300" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s7" to="brown" />
      </biological_entity>
      <biological_entity id="o1301" name="spine" name_original="spines" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o1302" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s7" value="stout" value_original="stout" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <relation from="o1300" id="r124" modifier="with palmately radiating cluster" name="part_of" negation="false" src="d0_s7" to="o1301" />
    </statement>
    <statement id="d0_s8">
      <text>Inner phyllaries: appendages scarious, obtuse or abruptly spine-tipped.</text>
      <biological_entity constraint="inner" id="o1303" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o1304" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets many;</text>
      <biological_entity id="o1305" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s9" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, all ± equal, 13–20 mm;</text>
      <biological_entity id="o1306" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sterile florets slender, inconspicuous.</text>
      <biological_entity id="o1307" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae dimorphic, 2–3 mm, glabrous, outer dark-brown, without pappi, inner white or light-brown, mottled;</text>
      <biological_entity id="o1308" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1309" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o1310" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o1311" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="mottled" value_original="mottled" />
      </biological_entity>
      <relation from="o1309" id="r125" name="without" negation="false" src="d0_s12" to="o1310" />
    </statement>
    <statement id="d0_s13">
      <text>pappi of many white, unequal bristles 2–4 mm, fine.</text>
      <biological_entity id="o1313" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="many" value_original="many" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o1312" id="r126" name="consist_of" negation="false" src="d0_s13" to="o1313" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o1312" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s13" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1314" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly summer–autumn (Jun–Oct), sometimes year-round in frostfree coastal habitats.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="autumn" from="summer" />
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Oct" from="Jun" />
        <character name="flowering time" char_type="range_value" modifier="sometimes;  in frostfree coastal habitats" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, pastures, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widely introduced; Alta., Man., Ont., Sask.; Ariz., Calif., Colo., Conn., Del., Fla., Idaho, Ill., Ind., Iowa, Kans., Ky., Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Wash., W.Va., Wis., Wyo.; s Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widely" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Yellow or Barnaby star-thistle</other_name>
  <other_name type="common_name">St. Barnaby’s thistle</other_name>
  <other_name type="common_name">centauré du solstice</other_name>
  <discussion>Centaurea solstitialis is a serious weed pest, especially in the western United States, where it has invaded millions of acres of rangelands, and it is listed as a noxious weed in eleven western states and two Canadian provinces. It is a strong competitor in infested areas, often forming dense colonies. It is very difficult to control or eradicate once it becomes established. In addition, yellow star-thistle is poisonous to horses; when ingested over a prolonged period it causes a neurological disorder called equine nigropallidal encephalomalacia, or “chewing disease.” Although its bitter taste and spiny heads usually deter grazing animals, horses sometimes will seek it out. Yellow star-thistle tends to spread in rangelands when more palatable plants are consumed.</discussion>
  
</bio:treatment>