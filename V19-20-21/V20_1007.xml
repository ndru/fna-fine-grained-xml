<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">440</other_info_on_meta>
    <other_info_on_meta type="mention_page">445</other_info_on_meta>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1894" rank="species">acradenia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">acradenia</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species acradenia;variety acradenia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068532</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 20–30 mm, margins usually entire, proximal rarely toothed or lobed (in Arizona).</text>
      <biological_entity id="o14260" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14261" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14262" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s0" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s0" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 5–7 mm.</text>
      <biological_entity id="o14263" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllary apices usually rounded, not spinulose, sometimes with weakly developed aristae.</text>
      <biological_entity constraint="phyllary" id="o14264" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s2" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o14265" name="arista" name_original="aristae" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="weakly" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o14264" id="r1308" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o14265" />
    </statement>
    <statement id="d0_s3">
      <text>Florets 18–24.2n = 24.</text>
      <biological_entity id="o14266" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s3" to="24" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14267" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Salt shrub, often with Larrea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="salt shrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <other_name type="common_name">Pale-leaf goldenweed</other_name>
  
</bio:treatment>