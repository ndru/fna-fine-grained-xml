<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="genus">helianthella</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1857" rank="species">californica</taxon_name>
    <taxon_name authority="(Greene) W. A. Weber" date="1999" rank="subspecies">nevadensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>85: 20. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus helianthella;species californica;subspecies nevadensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068430</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthella</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">nevadensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 89. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthella;species nevadensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">nevadensis</taxon_name>
    <taxon_hierarchy>genus Helianthella;species californica;variety nevadensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems not branched.</text>
      <biological_entity id="o15549" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves mostly alternate;</text>
      <biological_entity constraint="cauline" id="o15550" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades oblong-lanceolate to lance-linear, (10–) 20–45 mm wide.</text>
      <biological_entity id="o15551" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="lance-linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually 2–3+, seldom borne singly.</text>
      <biological_entity id="o15552" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" upper_restricted="false" />
        <character is_modifier="false" modifier="seldom" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae winged;</text>
      <biological_entity id="o15553" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi of 2 ± subulate scales plus 0–4 shorter scales.</text>
      <biological_entity id="o15554" name="pappus" name_original="pappi" src="d0_s5" type="structure" />
      <biological_entity id="o15555" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o15556" name="scale" name_original="scales" src="d0_s5" type="structure" />
      <relation from="o15554" id="r1071" name="consist_of" negation="false" src="d0_s5" to="o15555" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, open forest, chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(200–)600–2300+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300+" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2300+" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <discussion>Subspecies nevadensis grows in the Sierra Nevada and in the Cascade Range.</discussion>
  
</bio:treatment>