<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Leila M. Shultz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="treatment_page">498</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">PICROTHAMNUS</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos Soc., n. s.</publication_title>
      <place_in_publication>7: 417. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus PICROTHAMNUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek picro- , bitter, and thamnos, bush, alluding to bitterness of the plants</other_info_on_name>
    <other_info_on_name type="fna_id">125403</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 5–30 (–50) cm (strongly aromatic).</text>
      <biological_entity id="o21343" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, usually erect, diffusely branched from bases and throughout (some laterals persistent, forming thorns), villous to arachnose (hairs medifixed).</text>
      <biological_entity id="o21345" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o21346" is_modifier="false" modifier="diffusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="villous" modifier="throughout" name="pubescence" notes="" src="d0_s1" to="arachnose" />
      </biological_entity>
      <biological_entity id="o21346" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o21348" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o21347" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ± orbiculate to flabellate, simple or 1–2-pedately lobed (lobes orbiculate to spatulate or linear), ultimate margins entire, faces ± villous and gland-dotted.</text>
      <biological_entity id="o21349" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="less orbiculate" name="shape" src="d0_s5" to="flabellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="1-2-pedately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o21350" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21351" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform, usually (2–12+) in ± leafy, racemiform to spiciform arrays, rarely borne singly.</text>
      <biological_entity id="o21352" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21353" from="2" modifier="usually" name="atypical_quantity" src="d0_s6" to="12" upper_restricted="false" />
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o21353" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s6" to="spiciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± obconic, 2–3 (–5) mm diam.</text>
      <biological_entity id="o21354" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 5–8 in ± 2 series, distinct, ± obovate, subequal, margins and apices (hyaline) narrowly scarious.</text>
      <biological_entity id="o21355" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o21356" from="5" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o21356" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21357" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o21358" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex (glabrous), epaleate.</text>
      <biological_entity id="o21359" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 (peripheral pistillate florets 2–8; corollas pale-yellow, ± filiform, ± villous).</text>
      <biological_entity id="o21360" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 5–13 (–15), functionally staminate;</text>
      <biological_entity id="o21361" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="13" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pale-yellow (± villous), tubes ± cylindric, throats campanulate, lobes 5, ± deltate.</text>
      <biological_entity id="o21362" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o21363" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o21364" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o21365" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (brown) obovoid to ellipsoid, ribs 0, faces ± villous and obscurely nerved (pericarps without myxogenic cells or resin sacs);</text>
      <biological_entity id="o21366" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s13" to="ellipsoid" />
      </biological_entity>
      <biological_entity id="o21367" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21368" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s13" value="nerved" value_original="nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o21369" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o21370" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>117.</number>
  <other_name type="common_name">Budsage</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Separation of Picrothamnus from Artemisia calls attention to differing views of generic circumscription within Anthemideae (see discussion under Artemisia). Distinguished by its spinescent branches and relatively large heads held among the leaves, Picrothamnus is among the more distinct of proposed segregates. The diffuse-porous woods (S. Carlquist 1966) correspond to the early spring-blooming phenology of the plants and provide an anatomic feature that helps to distinguish Picrothamnus from Artemisia.</discussion>
  
</bio:treatment>