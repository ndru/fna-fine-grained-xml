<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="mention_page">408</other_info_on_meta>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
    <other_info_on_meta type="illustration_page">404</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">xylorhiza</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Greene" date="1896" rank="species">tortifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 48. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus xylorhiza;species tortifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067840</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">tortifolius</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>5: 109. 1845 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species tortifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Cronquist &amp; D. D. Keck" date="unknown" rank="species">tortifolia</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species tortifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 20–60 (–80) cm.</text>
      <biological_entity id="o9966" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched mostly in proximal 3/4, ± piloso-villous, stipitate-glandular.</text>
      <biological_entity id="o9967" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="in proximal 3/4" constraintid="o9968" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" notes="" src="d0_s1" value="piloso-villous" value_original="piloso-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9968" name="3/4" name_original="3/4" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate to elliptic-oblong or oblanceolate, 3–20 (–25) mm wide, bases often subclasping or sometimes attenuate and not clasping, margins flat, sharply spinulose-toothed, faces loosely villous or glabrous, stipitate-glandular.</text>
      <biological_entity id="o9969" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic-oblong or oblanceolate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9970" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o9971" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s2" value="spinulose-toothed" value_original="spinulose-toothed" />
      </biological_entity>
      <biological_entity id="o9972" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 5–25 cm.</text>
      <biological_entity id="o9973" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 12–20 × 15–20 (–30) mm.</text>
      <biological_entity id="o9974" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets (15–) 18–60 (–85);</text>
      <biological_entity id="o9975" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s5" to="18" to_inclusive="false" />
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="85" />
        <character char_type="range_value" from="18" name="quantity" src="d0_s5" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas usually lavender, seldom white.</text>
      <biological_entity id="o9976" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_odor" src="d0_s6" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="seldom" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Style-branch appendages shorter than stigmatic lines.</text>
      <biological_entity constraint="style-branch" id="o9977" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic lines" constraintid="o9978" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o9978" name="line" name_original="lines" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="past_name">Xylorrhiza</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems, leaves, and involucres glabrous, stipitate-glandular; leaf margins sparsely piloso-villous to puberulent</description>
      <determination>8a Xylorhiza tortifolia var. imberbis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems, leaves, and involucres piloso-villous to puberulent, finely stipitate-glandular</description>
      <determination>8b Xylorhiza tortifolia var. tortifolia</determination>
    </key_statement>
  </key>
</bio:treatment>