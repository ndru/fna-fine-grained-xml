<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="illustration_page">178</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">centaurea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cyanus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 911. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus centaurea;species cyanus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023634</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucacantha</taxon_name>
    <taxon_name authority="(Linnaeus) Nieuwland &amp; Lunell" date="unknown" rank="species">cyanus</taxon_name>
    <taxon_hierarchy>genus Leucacantha;species cyanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–100 cm.</text>
      <biological_entity id="o16314" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect, ± openly branched distally, loosely tomentose.</text>
      <biological_entity id="o16315" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less openly; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ± loosely gray-tomentose;</text>
      <biological_entity id="o16316" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less loosely" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaf-blades linear-lanceolate, 3–10 cm, margins entire or with remote linear lobes, apices acute;</text>
      <biological_entity constraint="basal" id="o16317" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="distance" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16318" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="with remote linear lobes" value_original="with remote linear lobes" />
      </biological_entity>
      <biological_entity id="o16319" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s3" value="remote" value_original="remote" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o16318" id="r1478" name="with" negation="false" src="d0_s3" to="o16319" />
    </statement>
    <statement id="d0_s4">
      <text>cauline linear, usually not much smaller except among heads, usually entire.</text>
      <biological_entity id="o16320" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o16321" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually not much" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiant, in open, rounded or ± flat-topped cymiform arrays, pedunculate.</text>
      <biological_entity id="o16322" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="radiant" value_original="radiant" />
        <character is_modifier="false" modifier="in open" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate, 12–16 mm.</text>
      <biological_entity id="o16323" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: bodies green, ovate (outer) to oblong (inner), tomentose or becoming glabrous, margins and erect appendages white to dark-brown or black, scarious, fringed with slender teeth ± 1 mm.</text>
      <biological_entity id="o16324" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o16325" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16326" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="dark-brown or black" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character constraint="with slender teeth" constraintid="o16328" is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o16327" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="dark-brown or black" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character constraint="with slender teeth" constraintid="o16328" is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity constraint="slender" id="o16328" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 25–35;</text>
      <biological_entity id="o16329" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue (white to purple), those of sterile florets raylike, enlarged, 20–25 mm, those of fertile florets 10–15 mm.</text>
      <biological_entity id="o16330" name="corolla" name_original="corollas" src="d0_s9" type="structure" constraint="floret" constraint_original="floret; floret">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="raylike" value_original="raylike" />
        <character is_modifier="false" name="size" src="d0_s9" value="enlarged" value_original="enlarged" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16331" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o16332" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o16330" id="r1479" name="part_of" negation="false" src="d0_s9" to="o16331" />
      <relation from="o16330" id="r1480" name="part_of" negation="false" src="d0_s9" to="o16332" />
    </statement>
    <statement id="d0_s10">
      <text>Cypselae stramineous or pale blue, 4–5 mm, finely hairy;</text>
      <biological_entity id="o16333" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale blue" value_original="pale blue" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of many unequal stiff bristles, 2–4 mm. 2n = 24 (Russia).</text>
      <biological_entity id="o16334" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16335" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="many" value_original="many" />
        <character is_modifier="true" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="fragility" src="d0_s11" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16336" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
      <relation from="o16334" id="r1481" name="consist_of" negation="false" src="d0_s11" to="o16335" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, woodlands, forests, roadsides, other disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="other disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; s Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Bachelor’s-button</other_name>
  <other_name type="common_name">garden cornflower</other_name>
  <other_name type="common_name">cornflower</other_name>
  <other_name type="common_name">bluebottle</other_name>
  <other_name type="common_name">bluebonnets</other_name>
  <other_name type="common_name">blaver</other_name>
  <other_name type="common_name">blue-poppy</other_name>
  <other_name type="common_name">thimbles</other_name>
  <other_name type="common_name">brushes</other_name>
  <other_name type="common_name">corn pinks</other_name>
  <other_name type="common_name">witch’s bells</other_name>
  <other_name type="common_name">hurtsickle</other_name>
  <other_name type="common_name">bleuet</other_name>
  <other_name type="common_name">barbeau</other_name>
  <other_name type="common_name">casse lunette</other_name>
  <discussion>Centaurea cyanus is a commonly cultivated garden ornamental. Its cypselae are often included in wildflower seed mixes and it naturalizes readily in many areas.</discussion>
  
</bio:treatment>