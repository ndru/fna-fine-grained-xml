<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">417</other_info_on_meta>
    <other_info_on_meta type="illustration_page">417</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">psathyrotes</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="1853" rank="species">annua</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 100. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus psathyrotes;species annua</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220011038</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bulbostylis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">annua</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 22. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bulbostylis;species annua;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (rarely persisting), 4–15 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular-pubescent, weakly tomentose, furfuraceous.</text>
      <biological_entity id="o12660" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="weakly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="furfuraceous" value_original="furfuraceous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect and spreading, much branched.</text>
      <biological_entity id="o12661" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades rounded-deltate to reniform, 4–18 × 6–26 mm, margins of some or all toothed.</text>
      <biological_entity id="o12662" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s3" to="reniform" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12663" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="of some" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–28 mm.</text>
      <biological_entity id="o12664" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 6–9 mm.</text>
      <biological_entity id="o12665" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 13–19, outer falling or persistent, 5–6, lanceolate to lance-linear, tips erect, inner falling, 8–13, lance-linear, tips erect.</text>
      <biological_entity id="o12666" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s6" to="19" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12667" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="falling" value_original="falling" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o12668" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12669" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="falling" value_original="falling" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="13" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
      <biological_entity id="o12670" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 10–21;</text>
      <biological_entity id="o12671" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, often purplish at tips, 4–5 mm.</text>
      <biological_entity id="o12672" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character constraint="at tips" constraintid="o12673" is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12673" name="tip" name_original="tips" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o12674" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 35–50, subequal bristles in 1 series, 1–4 mm. 2n = 34.</text>
      <biological_entity id="o12675" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12676" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="35" is_modifier="true" name="quantity" src="d0_s10" to="50" />
        <character is_modifier="true" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o12677" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12678" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
      <relation from="o12675" id="r871" name="consist_of" negation="false" src="d0_s10" to="o12676" />
      <relation from="o12676" id="r872" name="in" negation="false" src="d0_s10" to="o12677" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline soils of washes and playas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline soils" constraint="of washes and playas" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="playas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Idaho, Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>