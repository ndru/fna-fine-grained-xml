<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">478</other_info_on_meta>
    <other_info_on_meta type="treatment_page">479</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">conoclinium</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">dissectum</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 88. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus conoclinium;species dissectum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066412</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conoclinium</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="unknown" rank="species">greggii</taxon_name>
    <taxon_hierarchy>genus Conoclinium;species greggii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">greggii</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species greggii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect (often from knotty crowns, sometimes basally lignescent).</text>
      <biological_entity id="o1495" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate-deltate to ovate, 1.5–4 cm, bases attenuate, margins dissected or lobed, apices pointed or rounded.</text>
      <biological_entity id="o1496" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s1" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="distance" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1497" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o1498" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o1499" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="pointed" value_original="pointed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 3.5–5 mm.</text>
      <biological_entity id="o1500" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Corollas blue to lavender or purple, 2.5–3.5 mm.</text>
      <biological_entity id="o1501" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s3" to="lavender or purple" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 1.8–2.5 mm, hispidulous;</text>
      <biological_entity id="o1502" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi: bristle tips not dilated.</text>
      <biological_entity id="o1503" name="pappus" name_original="pappi" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 20.</text>
      <biological_entity constraint="bristle" id="o1504" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1505" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug, Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waterways, depressions and ditches, dry sandy or rocky soil, mesquite, creosote bush-mesquite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waterways" />
        <character name="habitat" value="depressions" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="dry sandy" />
        <character name="habitat" value="rocky soil" />
        <character name="habitat" value="mesquite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sonora, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Palm-leaf mistflower</other_name>
  
</bio:treatment>