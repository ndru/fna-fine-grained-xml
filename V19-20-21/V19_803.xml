<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="treatment_page">478</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="(Cassini ex Dumortier) Anderberg" date="1989" rank="tribe">plucheeae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">PLUCHEA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1817: 31. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe plucheeae;genus PLUCHEA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Abbé N. A. Pluche, 1688–1761, French naturalist</other_info_on_name>
    <other_info_on_name type="fna_id">126103</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, subshrubs, shrubs, or trees (usually fetid-aromatic), (20–) 50–200 (–500) cm;</text>
      <biological_entity id="o18985" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or fibrous-rooted.</text>
      <biological_entity id="o18986" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="500" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o18987" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="500" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="500" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="500" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, simple or branched, seldom winged (see P. sagittalis), usually puberulent to tomentose and stipitate or sessile-glandular, sometimes glabrous.</text>
      <biological_entity id="o18990" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character char_type="range_value" from="usually puberulent" name="pubescence" src="d0_s2" to="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o18991" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly elliptic, lanceolate, oblanceolate, oblong, obovate, or ovate, bases clasping or not, margins entire or dentate, abaxial faces mostly arachnose, puberulent, sericeous, strigose, or villous and/or stipitate or sessile-glandular, adaxial similar or glabrate or glabrous.</text>
      <biological_entity id="o18992" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o18993" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character name="architecture_or_fixation" src="d0_s5" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o18994" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18995" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18996" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform, in corymbiform or paniculiform arrays (flat-topped or ± elongate).</text>
      <biological_entity id="o18997" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o18998" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o18997" id="r1713" name="in" negation="false" src="d0_s6" to="o18998" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres mostly campanulate, cupulate, cylindric, hemispheric, or turbinate, 3–10 (–12) mm diam.</text>
      <biological_entity id="o18999" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent or falling, in 3–6+ series, mostly ovate to lanceolate or linear, unequal.</text>
      <biological_entity id="o19000" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character char_type="range_value" from="mostly ovate" name="shape" notes="" src="d0_s8" to="lanceolate or linear" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o19001" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" upper_restricted="false" />
      </biological_entity>
      <relation from="o19000" id="r1714" name="in" negation="false" src="d0_s8" to="o19001" />
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, epaleate.</text>
      <biological_entity id="o19002" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peripheral (pistillate) florets in 3–10+ series, fertile;</text>
      <biological_entity constraint="peripheral" id="o19003" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o19004" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="10" upper_restricted="false" />
      </biological_entity>
      <relation from="o19003" id="r1715" name="in" negation="false" src="d0_s10" to="o19004" />
    </statement>
    <statement id="d0_s11">
      <text>corollas creamy white, whitish, yellowish, pinkish, lavender, purplish, or rosy.</text>
      <biological_entity id="o19005" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rosy" value_original="rosy" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rosy" value_original="rosy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Inner (functionally staminate) florets 2–40+;</text>
      <biological_entity constraint="inner" id="o19006" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="40" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas creamy white, whitish, yellowish, pinkish, lavender, purplish, or rosy, lobes (4–) 5.</text>
      <biological_entity id="o19007" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rosy" value_original="rosy" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rosy" value_original="rosy" />
      </biological_entity>
      <biological_entity id="o19008" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s13" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae oblong-cylindric, ribs 4–8, faces strigillose and/or minutely sessile-glandular or glabrous (in the flora, only P. sericea);</text>
      <biological_entity id="o19009" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-cylindric" value_original="oblong-cylindric" />
      </biological_entity>
      <biological_entity id="o19010" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s14" to="8" />
      </biological_entity>
      <biological_entity id="o19011" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s14" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent or tardily falling, of distinct or basally connate, barbellate bristles in 1 series.</text>
      <biological_entity id="o19013" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o19014" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o19012" id="r1716" name="consists_of" negation="false" src="d0_s15" to="o19013" />
      <relation from="o19013" id="r1717" name="in" negation="false" src="d0_s15" to="o19014" />
    </statement>
    <statement id="d0_s16">
      <text>x = 10.</text>
      <biological_entity id="o19012" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o19015" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and warm-temperate regions, North America, West Indies, South America, se Asia, Africa, Australia, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and warm-temperate regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>111.</number>
  <discussion>Species 40–60 (9 in the flora).</discussion>
  <discussion>As currently treated, Pluchea is a heterogeneous group of species, variable in habit (trees and shrubs to herbs) and foliar, floral, and fruit morphology. The American, primarily herbaceous, species are divided into groups (G. L. Nesom 1989): sect. Pluchea, sect. Amplectifolium G. L. Nesom, and sect. Pterocaulis G. L. Nesom. Among the woody species, segregate genera have been recognized (Tessaria Ruiz &amp; Pavón, Berthelotia de Candolle, Eremohylema A. Nelson); boundaries among segregates have not been clearly drawn.</discussion>
  <references>
    <reference>Ariza E., L. 1979. Contribución al conocimiento del género Tessaria (Compositae), I. Consideraciónes sobre los géneros Tessaria y Pluchea. Kurtziana 12–13: 47–62.</reference>
    <reference>Cabrera, A. L. 1939. Las especies Argentinas del género “Tessaria.” Lilloa 4: 181–189.</reference>
    <reference>Godfrey, R. K. 1952. Pluchea, section Stylimnus, in North America. J. Elisha Mitchell Sci. Soc. 68: 238–271.</reference>
    <reference>Keeley, S. C. and R. K. Jansen. 1991. Evidence from chloroplast DNA for the recognition of a new tribe, the Tarchonantheae, and the tribal placement of Pluchea (Asteraceae). Syst. Bot. 16: 173–181. King-</reference>
    <reference>Robinson, H. and J. Cuatrecasas. 1973. The generic limits of Pluchea and Tessaria. Phytologia 27: 277–285.</reference>
    <reference>Jones, S. 2001. Revision of Pluchea Cass. (Compositae, Plucheeae) in the Old World. Englera 23.</reference>
    <reference>Nesom, G. L. 1989. New species, new sections, and a taxonomic overview of American Pluchea (Compositae: Inuleae). Phytologia 67: 158–167.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees; leaves and stems sericeous, not glandular</description>
      <determination>1 Pluchea sericea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals, perennials, or subshrubs; leaves and stems not sericeous, usually glandular</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems (winged by decurrent leaf bases)</description>
      <determination>2 Pluchea sagittalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems (not winged, leaf bases sometimes clasping, not decurrent)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves petiolate, blades mostly elliptic, lanceolate, oblanceolate, oblong-elliptic, oblong-ovate, or ovate (bases not clasping)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves sessile, blades mostly elliptic, lanceolate, oblong, or ovate (bases clasping to subclasping)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Subshrubs, 100–400 cm; leaf margins entire or denticulate (teeth callous-tipped).</description>
      <determination>3 Pluchea carolinensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals or perennials, 50–200+ cm; leaf margins serrate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Involucres 4–6 × 3–4 mm; phyllaries usually cream, sometimes purplish, usually minutely sessile-glandular, sometimes glabrate (the outermost puberulent); stems usually closely arachose (hairs appressed); arrays of heads paniculiform (of rounded-convex corymbiform clusters terminating branches from distal nodes, arrays usually resulting from strongly ascending, bracteate branches, the central axis longest, first to flower, and, rarely, the only component of an array); leaves petiolate; inland, non-saline habitats</description>
      <determination>4 Pluchea camphorata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Involucres 5–6 × 4–8(–10) mm; phyllaries usually cream, sometimes purplish, minutely sessile-glandular (outer also puberulent, hairs multicellular, viscid), sometimes glabrate; stems not arachnose; arrays of heads corymbiform (flat-topped to rounded, often layered, sometimes incorporating relatively long, leafy, lateral branches, clusters of heads terminal on leafy branches, some lateral branches nearly equaling or surpassing central portion); leaves sessile or petiolate; primarily coastal salt marshes, also inland habitats west of Mississippi River</description>
      <determination>5 Pluchea odorata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves mostly 8–20 × 3–7 cm; involucres cylindro-campanulate, 9–12 mm (mid phyllaries 2.5–3 mm wide); phyllaries and corollas creamy white.</description>
      <determination>9 Pluchea longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves mostly 3–10 × 1–3 cm; involucres turbinate-campanulate to cylindro-campanulate, 5–8 mm (mid phyllaries 1–1.5 mm wide); phyllaries and corollas yellowish or creamy white to lavender, pale pink, pinkish, purplish, or rosy</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems and leaves (slightly succulent, shiny) glandular, otherwise mostly glabrous; involucres 5–6 × 4–5 mm; phyllaries and corollas pink to lavender or cream or pinkish to rosy</description>
      <determination>8 Pluchea yucatanensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems and leaves at least puberulent or arachnose as well as glandular; involucres 4–10 × 5–12 mm; phyllaries and corollas rose-pink to purplish, rose-purple, greenish, cream, or creamy white to yellowish, or pale pink</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries and corollas usually creamy white, sometimes cream, greenish, rose-purple, purplish, pale pink, or yellowish; involucres 5–10 × 6–9(–12) mm (bases rounded to impressed); phyllaries thinly arachnose and sessile-glandular</description>
      <determination>6 Pluchea foetida</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries and corollas rose-pink to purplish; involucres 4–6 × 5–9 mm (bases obtuse to barely acute); phyllaries usually arachnose (sometimes also with viscid hairs)</description>
      <determination>7 Pluchea baccharis</determination>
    </key_statement>
  </key>
</bio:treatment>