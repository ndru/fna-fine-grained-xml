<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
    <other_info_on_meta type="illustration_page">527</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rydberg) G. L. Nesom" date="1995" rank="section">occidentales</taxon_name>
    <taxon_name authority="(Lindley ex de Candolle) G. L. Nesom" date="1995" rank="species">foliaceum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 282. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section occidentales;species foliaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067647</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lindley ex de Candolle" date="unknown" rank="species">foliaceus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 228. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species foliaceus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 10–60 cm, colonial;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous.</text>
      <biological_entity id="o30621" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, ascending to erect, glabrous or sparsely puberulent.</text>
      <biological_entity id="o30622" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves thin, margins entire or sometimes serrate, apices acute to obtuse, faces usually glabrous, sometimes sparsely hairy;</text>
      <biological_entity id="o30623" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o30624" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30625" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o30626" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal usually persistent, petiolate to subpetiolate, blades broadly elliptic to obovate, 30–200 × 8–25 (–30) mm, bases attenuate, margins entire or sometimes serrate, apices acute to obtuse;</text>
      <biological_entity constraint="basal" id="o30627" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="petiolate" name="architecture" src="d0_s4" to="subpetiolate" />
      </biological_entity>
      <biological_entity id="o30628" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="200" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30629" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o30630" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30631" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline sessile or subpetiolate, blades elliptic to obovate, 35–120 × 8–25 mm, bases attenuate or cuneate to rounded, sometimes ± clasping, apices acute;</text>
      <biological_entity constraint="proximal cauline" id="o30632" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
      </biological_entity>
      <biological_entity id="o30633" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s5" to="120" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30634" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="false" modifier="sometimes more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o30635" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, reduced distally, bases cuneate, apices acute.</text>
      <biological_entity constraint="distal" id="o30636" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o30637" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30638" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly or in paniculiform arrays, branches ascending, up to 25 cm.</text>
      <biological_entity id="o30639" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="in paniculiform arrays" value_original="in paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o30640" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o30641" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="25" to_unit="cm" />
      </biological_entity>
      <relation from="o30639" id="r2829" name="in" negation="false" src="d0_s7" to="o30640" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles sparsely hairy, bracts 0–3, lanceolate.</text>
      <biological_entity id="o30642" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30643" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 6–16 (–20) mm.</text>
      <biological_entity id="o30644" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–6 series, oblanceolate or oblong (outer) to lanceolate or linear (inner), subequal or unequal (outer exceeding inner), bases outer foliaceous, inner indurate, margins entire, green zones elliptic to lanceolate, apices acute to rounded, faces glabrous or puberulent.</text>
      <biological_entity id="o30645" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s10" to="lanceolate or linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o30646" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o30647" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="position" src="d0_s10" value="inner" value_original="inner" />
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o30648" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30649" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o30650" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="rounded" />
      </biological_entity>
      <biological_entity id="o30651" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o30645" id="r2830" name="in" negation="false" src="d0_s10" to="o30646" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 15–60;</text>
      <biological_entity id="o30652" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas violet to purple, laminae 8–18 (–20) × 1–2 mm.</text>
      <biological_entity id="o30653" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s12" to="purple" />
      </biological_entity>
      <biological_entity id="o30654" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 50–150;</text>
      <biological_entity id="o30655" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" to="150" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, 4–7 mm, lobes triangular, 0.4–1 mm.</text>
      <biological_entity id="o30656" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30657" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae brown, cylindric to obovoid, not compressed, 2.5–4 mm, 3–4-nerved, faces hairy;</text>
      <biological_entity id="o30658" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-4-nerved" value_original="3-4-nerved" />
      </biological_entity>
      <biological_entity id="o30659" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white to tawny, 5–8 mm.</text>
      <biological_entity id="o30660" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="tawny" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>67.</number>
  <other_name type="common_name">Leafy or leafy-bracted or alpine leafybract aster</other_name>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Symphyotrichum foliaceum is extremely variable and is widespread in western montane coniferous forests and subalpine meadows.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 5–30 cm; heads 1–12; phyllaries subequal, narrowly lanceolate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants usually 20–60 cm; heads usually 5–20; outer phyllaries exceeding inner, wider, often foliaceous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 5–20 cm; heads usually borne singly; phyllaries often purple-tinged; alpine or sub- alpine habitats</description>
      <determination>67a Symphyotrichum foliaceum var. apricum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 10–30 cm; heads usually 1–12; phyllaries green; montane to subalpine habitats.</description>
      <determination>67b Symphyotrichum foliaceum var. parryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries obtuse to rounded; cauline leaves often clasping at base</description>
      <determination>67c Symphyotrichum foliaceum var. canbyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries acute; cauline leaves sometimes rounded at base, not clasping</description>
      <determination>67d Symphyotrichum foliaceum var. foliaceum</determination>
    </key_statement>
  </key>
</bio:treatment>