<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="species">racemosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">racemosa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species racemosa;variety racemosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068678</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–70 cm.</text>
      <biological_entity id="o21543" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually glabrous, rarely tomentulose.</text>
      <biological_entity id="o21544" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades oblanceolate to elliptic, 100–250 × 12–25 mm, not thin or succulent, margins entire to denticulate, faces glabrous.</text>
      <biological_entity id="o21545" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o21546" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s2" to="250" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s2" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o21547" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s2" to="denticulate" />
      </biological_entity>
      <biological_entity id="o21548" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in racemiform arrays.</text>
      <biological_entity id="o21549" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o21550" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o21549" id="r1987" name="in" negation="false" src="d0_s3" to="o21550" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric, 10–15 × 12–18 mm.</text>
      <biological_entity id="o21551" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: bases pale, green-tipped, margins hyaline, sometimes ciliate, apices erect, faces glabrous.</text>
      <biological_entity id="o21552" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity id="o21553" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green-tipped" value_original="green-tipped" />
      </biological_entity>
      <biological_entity id="o21554" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21555" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o21556" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypsela faces densely sericeous.</text>
      <biological_entity constraint="cypsela" id="o21557" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal valleys and marshes, neutral or saline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal valleys" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="neutral" />
        <character name="habitat" value="saline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12d.</number>
  <discussion>Variety racemosa is recognized by its robust habit, large heads, racemiform arrays, and occurrence in non-alkaline soils.</discussion>
  
</bio:treatment>