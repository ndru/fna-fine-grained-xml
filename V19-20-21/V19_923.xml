<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">526</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
    <other_info_on_meta type="illustration_page">528</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="Besser" date="1834" rank="species">senjavinensis</taxon_name>
    <place_of_publication>
      <publication_title>Nouv. Mém. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>3: 35. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species senjavinensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066166</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ajania</taxon_name>
    <taxon_name authority="(Besser) Poljakov" date="unknown" rank="species">senjavinensis</taxon_name>
    <taxon_hierarchy>genus Ajania;species senjavinensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Seemann" date="unknown" rank="species">androsacea</taxon_name>
    <taxon_hierarchy>genus Artemisia;species androsacea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–90 cm (densely cespitose), mildly aromatic (caudices branched, woody, taprooted).</text>
      <biological_entity id="o14298" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="mildly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–9, erect, gray-green, lanate.</text>
      <biological_entity id="o14299" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="9" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal (in rosettes, cauline 2–5, scattered on flowering-stems);</text>
      <biological_entity id="o14300" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14301" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (basal) broadly oblanceolate, 0.5–0.8 × 0.5–0.7 cm, relatively deeply lobed (lobes 3–5, acute; cauline blades 0.5–1 cm, entire or pinnately lobed, lobes 3–5), faces densely tomentose to sericeous (hairs 1–2 mm).</text>
      <biological_entity id="o14302" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="false" modifier="relatively deeply" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o14303" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="densely tomentose" name="pubescence" src="d0_s3" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays 0.5–2.5 × 0.5–2.5 cm (subtended by white-sericeous bracts).</text>
      <biological_entity id="o14304" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" modifier="in corymbiform arrays" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" modifier="in corymbiform arrays" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 3–4 × 3–5 mm.</text>
      <biological_entity id="o14305" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate or ovate, hairy.</text>
      <biological_entity id="o14306" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 4–5;</text>
      <biological_entity id="o14307" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 3–4;</text>
      <biological_entity id="o14308" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow or tan, 1.5–2, glandular (style-branches blunt, not fringed).</text>
      <biological_entity id="o14309" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o14310" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="tan" value_original="tan" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (brown) linear-oblong, ca. 2 mm, (apices flat), glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 36, 54.</text>
      <biological_entity id="o14311" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong" value_original="linear-oblong" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14312" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
        <character name="quantity" src="d0_s11" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open calcareous gravelly slopes in tundra or heath, sandy slopes above high tide</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous gravelly slopes" modifier="open" constraint="in tundra or heath , sandy slopes above high tide" />
        <character name="habitat" value="tundra" modifier="in" />
        <character name="habitat" value="heath" />
        <character name="habitat" value="sandy slopes" constraint="above high tide" />
        <character name="habitat" value="high tide" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; e Asia (Russian Far East, Chukotka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (Chukotka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <discussion>Artemisia senjavinensis is known only from western Alaska (Seward Peninsula) and the Chukchi Peninsula of the Russian Far East.</discussion>
  
</bio:treatment>