<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="treatment_page">331</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">agoseris</taxon_name>
    <taxon_name authority="(Lessing) Greene" date="1891" rank="species">apargioides</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 177. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus agoseris;species apargioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Troximon</taxon_name>
    <taxon_name authority="Lessing" date="unknown" rank="species">apargioides</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>6: 501. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Troximon;species apargioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0 or 1–5+ (becoming buried by drifting sand and appearing pseudorhizomatous).</text>
      <biological_entity id="o11927" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually reclining to decumbent, sometimes erect;</text>
      <biological_entity id="o11928" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="usually reclining" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades mostly oblanceolate to spatulate, sometimes nearly linear, 3–15 cm, margins usually dentate to lobed or pinnatifid, rarely entire, lobes 3–5 (–7) pairs, filiform to spatulate, spreading to antrorse, lobules mostly 0, faces glabrous or densely hairy.</text>
      <biological_entity id="o11929" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly oblanceolate" name="shape" src="d0_s2" to="spatulate" />
        <character is_modifier="false" modifier="sometimes nearly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11930" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually dentate" name="shape" src="d0_s2" to="lobed or pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11931" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s2" to="spatulate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="antrorse" />
      </biological_entity>
      <biological_entity id="o11932" name="lobule" name_original="lobules" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11933" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles ± elongating after flowering, 7–45 cm in fruit, glabrous or glabrate to hairy, often villous basally, sometimes villous to tomentose apically, sometimes stipitate-glandular.</text>
      <biological_entity id="o11934" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character constraint="in fruit" constraintid="o11935" is_modifier="false" modifier="more or less" name="length" src="d0_s3" value="elongating" value_original="elongating" />
        <character char_type="range_value" from="glabrate" name="pubescence" notes="" src="d0_s3" to="hairy" />
        <character is_modifier="false" modifier="often; basally" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character char_type="range_value" from="villous" modifier="sometimes; apically" name="pubescence" src="d0_s3" to="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o11935" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres obconic to hemispheric, 1.5–2.5 cm in fruit.</text>
      <biological_entity id="o11936" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s4" to="hemispheric" />
        <character char_type="range_value" constraint="in fruit" constraintid="o11937" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11937" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries imbricate (sometimes subequal) in 2–3 series, green or medially rosy purple, often spotted and/or with purple-black midstripes, margins ciliate to tomentose, faces usually ± villous, sometimes glabrous, sometimes stipitate-glandular;</text>
      <biological_entity id="o11938" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character constraint="in series" constraintid="o11939" is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s5" value="rosy purple" value_original="rosy purple" />
        <character constraint="with midstripes" constraintid="o11940" is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="spotted" value_original="spotted" />
      </biological_entity>
      <biological_entity id="o11939" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o11940" name="midstripe" name_original="midstripes" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="purple-black" value_original="purple-black" />
      </biological_entity>
      <biological_entity id="o11941" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="ciliate" name="pubescence" src="d0_s5" to="tomentose" />
      </biological_entity>
      <biological_entity id="o11942" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer mostly spreading, adaxially usually ± tomentose, rarely glabrous;</text>
      <biological_entity constraint="outer" id="o11943" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="adaxially usually more or less" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner erect, elongating after flowering.</text>
      <biological_entity constraint="inner" id="o11944" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="after flowering" is_modifier="false" name="length" src="d0_s7" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles epaleate.</text>
      <biological_entity id="o11945" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 25–200;</text>
      <biological_entity id="o11946" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, tubes 2–5.5 mm, ligules 3–16 × 1–3 mm;</text>
      <biological_entity id="o11947" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o11948" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11949" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.5–4.5 mm.</text>
      <biological_entity id="o11950" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 5–12 mm;</text>
      <biological_entity id="o11951" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bodies fusiform to obconic, 3–5 mm, beaks (1–) 3–8 mm, lengths mostly 1–2 times bodies;</text>
      <biological_entity id="o11952" name="body" name_original="bodies" src="d0_s13" type="structure">
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s13" to="obconic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11953" name="beak" name_original="beaks" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character constraint="body" constraintid="o11954" is_modifier="false" modifier="mostly" name="length" src="d0_s13" value="1-2 times bodies" value_original="1-2 times bodies" />
      </biological_entity>
      <biological_entity id="o11954" name="body" name_original="bodies" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pappus bristles in 2–3 series, 4–9 mm. 2n = 36.</text>
      <biological_entity constraint="pappus" id="o11955" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11956" name="series" name_original="series" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s14" to="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11957" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
      <relation from="o11955" id="r1102" name="in" negation="false" src="d0_s14" to="o11956" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Seaside agoseris</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>A misinterpretation of the type description of Agoseris apargioides resulted in its confusion with A. hirsuta during the latter half of the twentieth century; the two species are not conspecific. Agoseris apargioides (in the strict sense) here includes what most authors of recent floras have called A. apargioides subsp. maritima and/or var. eastwoodiae. It occurs on coastal dunes along the Pacific coast from central California to Washington. A unique feature of A. apargioides is that its stems become progressively buried by drifting sand, leaving a terminal rosette of leaves exposed, the plants thus appearing pseudorhizomatous.</discussion>
  <discussion>Agoseris apargioides is part of a close alliance that includes A. heterophylla, A. hirsuta, and A. coronopifolia from South America. Exact relationships within this group are not clear. Putative hybrids between A. apargioides and A. heterophylla var. cryptopleura, A. hirsuta, and A. grandiflora var. grandiflora have been identified.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ligules 3–6 mm; anthers 1.5–2.5 mm; phyllaries glabrous or villous, eglandular</description>
      <determination>7c Agoseris apargioides var. maritima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ligules 8–16 mm; anthers 3.5–4.5 mm; phyllaries glabrous or tomentose, ± stipitate-glandular</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants often densely villous; leaf blades mostly oblanceolate to spatulate, margins dentate to lobed, lobes oblanceolate to spatulate; phyl- laries densely villous to tomentose</description>
      <determination>7b Agoseris apargioides var. eastwoodiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants mostly glabrous or sparsely villous; leaf blades usually oblanceolate, sometimes linear, rarely spatulate, margins entire or laciniately pinnatifid, lobes filiform to lanceolate; phyl- laries glabrous or villous</description>
      <determination>7a Agoseris apargioides var. apargioides</determination>
    </key_statement>
  </key>
</bio:treatment>