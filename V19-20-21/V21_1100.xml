<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="treatment_page">440</other_info_on_meta>
    <other_info_on_meta type="illustration_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">hymenoxys</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) K. F. Parker" date="1950" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>10: 159. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus hymenoxys;species grandiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066988</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actinella</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>5: 109. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Actinella;species grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tetraneuris</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) K. F. Parker" date="unknown" rank="species">grandiflora</taxon_name>
    <taxon_hierarchy>genus Tetraneuris;species grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 8–30 cm (polycarpic, often with sparingly branched, woody caudices).</text>
      <biological_entity id="o15295" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10, green throughout to purple-red-tinted proximally or distally to purple-red-tinted throughout, usually unbranched distally, ± hairy.</text>
      <biological_entity id="o15296" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" />
        <character constraint="throughout" is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purple-red-tinted" value_original="purple-red-tinted" />
        <character char_type="range_value" from="proximally or" modifier="throughout" name="coloration" src="d0_s1" to="distally purple-red-tinted" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades simple or lobed (lobes 3–15), ± hairy, glanddotted (basal leaf-bases, sparsely, if at all, long-villous-woolly);</text>
      <biological_entity id="o15297" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15298" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid leaves simple or lobed (lobes 3–7, terminal lobes 1–2.5 mm wide).</text>
      <biological_entity id="o15299" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o15300" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–10 per plant, usually borne singly.</text>
      <biological_entity id="o15301" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o15302" from="1" name="quantity" src="d0_s4" to="10" />
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o15302" name="plant" name_original="plant" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–10 cm, ± hairy, tomentose distally near involucres.</text>
      <biological_entity id="o15303" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character constraint="near involucres" constraintid="o15304" is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o15304" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric to subglobose, 15–25 × 18–30 mm.</text>
      <biological_entity id="o15305" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="subglobose" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, subequal;</text>
      <biological_entity id="o15306" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o15307" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o15306" id="r1047" name="in" negation="false" src="d0_s7" to="o15307" />
    </statement>
    <statement id="d0_s8">
      <text>outer 16–24, basally connate 1/5–1/4 their lengths, lanceolate, 9–15 mm, apices acute;</text>
      <biological_entity constraint="outer" id="o15308" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="24" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/5" name="lengths" src="d0_s8" to="1/4" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner 16–24+, lanceolate to oblanceolate, 8–12 mm, apices acuminate to acute.</text>
      <biological_entity id="o15309" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o15310" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s9" to="24" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15311" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 15–34 (–44);</text>
      <biological_entity id="o15312" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="34" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="44" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="34" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 16–30 × 4–8 mm.</text>
      <biological_entity id="o15313" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 150–400+;</text>
      <biological_entity id="o15314" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s12" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 5–6 mm.</text>
      <biological_entity id="o15315" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obpyramidal to narrowly obpyramidal, 3.3–3.7 mm;</text>
      <biological_entity id="o15316" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="obpyramidal" name="shape" src="d0_s14" to="narrowly obpyramidal" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s14" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 5–7 lanceolate, aristate scales 4.5–5.3 mm. 2n = 30.</text>
      <biological_entity id="o15317" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o15318" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="7" />
        <character is_modifier="true" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15319" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
      <relation from="o15317" id="r1048" name="consist_of" negation="false" src="d0_s15" to="o15318" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, often above timberline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="timberline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600–4300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4300" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Graylocks rubberweed</other_name>
  <other_name type="common_name">four-nerved daisy</other_name>
  
</bio:treatment>