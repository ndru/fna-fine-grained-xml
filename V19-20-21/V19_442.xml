<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">HEDYPNOIS</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 2. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus HEDYPNOIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Ancient name for an endive-like plant, attributed to Pliny</other_info_on_name>
    <other_info_on_name type="fna_id">114829</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (5–) 10–60+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o8650" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect, branched distally, ± hispid to setose (hair tips often forked).</text>
      <biological_entity id="o8651" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="less hispid" name="pubescence" src="d0_s2" to="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o8652" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal ± petiolate, distal sessile;</text>
      <biological_entity constraint="basal" id="o8653" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8654" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate, linear, oblanceolate, oblong, or ovate, margins entire or dentate to pinnately lobed (faces ± hispid).</text>
      <biological_entity id="o8655" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8656" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="dentate to pinnately lobed" value_original="dentate to pinnately lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or in loose, corymbiform arrays.</text>
      <biological_entity id="o8657" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o8658" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o8657" id="r794" name="in" negation="false" src="d0_s6" to="o8658" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ± inflated distally, not bracteate.</text>
      <biological_entity id="o8659" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 3–10+, deltate to lanceolate or lance-linear bractlets.</text>
      <biological_entity id="o8660" name="calyculus" name_original="calyculi" src="d0_s8" type="structure" />
      <biological_entity id="o8661" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="10" upper_restricted="false" />
        <character char_type="range_value" from="deltate" is_modifier="true" name="shape" src="d0_s8" to="lanceolate or lance-linear" />
      </biological_entity>
      <relation from="o8660" id="r795" name="consist_of" negation="false" src="d0_s8" to="o8661" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate to cylindric, 3–12 mm diam.</text>
    </statement>
    <statement id="d0_s10">
      <text>(larger, ± globose in fruit).</text>
      <biological_entity id="o8662" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s9" to="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 5–13+ in 1 series, linear-navicular (± keeled, each ± enfolding subtended ovary or cypsela), subequal, margins little, if at all, scarious, apices acuminate.</text>
      <biological_entity id="o8663" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o8664" from="5" name="quantity" src="d0_s11" to="13" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="linear-navicular" value_original="linear-navicular" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o8664" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8665" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="at all" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8666" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles flat, ± pitted, glabrous, epaleate.</text>
      <biological_entity id="o8667" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Florets 8–30+;</text>
      <biological_entity id="o8668" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s13" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow (often reddish proximally, greenish abaxially).</text>
      <biological_entity id="o8669" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae dark-brown to black, cylindric to fusiform (usually ± arcuate), not beaked, ribs 12–15, faces ± scabrous or barbed;</text>
      <biological_entity id="o8670" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s15" to="black" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="fusiform" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o8671" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s15" to="15" />
      </biological_entity>
      <biological_entity id="o8672" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="barbed" value_original="barbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, whitish;</text>
    </statement>
    <statement id="d0_s17">
      <text>on outer cypselae often coroniform (distinct or connate, erose to fimbriate scales);</text>
    </statement>
    <statement id="d0_s18">
      <text>on inner cypselae 0–5+, cuneate to lanceolate or subulate outer scales plus 5+, lance-aristate to subulate-aristate, inner scales.</text>
      <biological_entity id="o8673" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s17" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s18" to="5" upper_restricted="false" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s18" to="lanceolate or subulate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o8674" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s18" upper_restricted="false" />
        <character char_type="range_value" from="lance-aristate" name="shape" src="d0_s18" to="subulate-aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity constraint="inner" id="o8675" name="scale" name_original="scales" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o8676" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  
</bio:treatment>