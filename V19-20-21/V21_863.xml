<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="R. Chan" date="2001" rank="section">ornduffia</taxon_name>
    <taxon_name authority="(Torrey ex A. Gray) Greene" date="1894" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. San Francisco,</publication_title>
      <place_in_publication>204. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section ornduffia;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067049</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dichaeta</taxon_name>
    <taxon_name authority="Torrey ex A. Gray" date="unknown" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 102. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dichaeta;species fremontii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(Torrey ex A. Gray) A. Gray" date="unknown" rank="species">fremontii</taxon_name>
    <taxon_hierarchy>genus Baeria;species fremontii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 35 cm.</text>
      <biological_entity id="o14949" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched proximally, glabrous proximally, ± hairy distally.</text>
      <biological_entity id="o14950" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less; distally" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear, 10–60 × 1–2+ mm (simple blades or single lobes), margins entire or with 1–3 pairs of linear lobes, faces glabrous or sparsely hairy.</text>
      <biological_entity id="o14951" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14952" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with 1-3 pairs" value_original="with 1-3 pairs" />
      </biological_entity>
      <biological_entity id="o14953" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o14954" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o14955" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o14952" id="r1026" name="with" negation="false" src="d0_s2" to="o14953" />
      <relation from="o14953" id="r1027" name="part_of" negation="false" src="d0_s2" to="o14954" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres hemispheric or obconic, 4–7.5 mm.</text>
      <biological_entity id="o14956" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 8–16 (distinct), ovate, hairy.</text>
      <biological_entity id="o14957" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="16" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles dome-shaped, muricate, hairy.</text>
      <biological_entity id="o14958" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="dome--shaped" value_original="dome--shaped" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 6–13;</text>
      <biological_entity id="o14959" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae oblong to oval, 5–7 mm.</text>
      <biological_entity id="o14960" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="oval" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages linear to narrowly ovate.</text>
      <biological_entity constraint="anther" id="o14961" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae black or gray, clavate, to 1.5 mm, usually scabrous, rarely glabrous;</text>
      <biological_entity id="o14962" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi usually of (3–) 4 (–5) subulate, aristate scales plus 3–5+ shorter, ± subulate scales or teeth, rarely of aristate scales only, or 0.2n = 12.</text>
      <biological_entity id="o14963" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14964" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14965" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o14966" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14967" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14968" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
      <relation from="o14963" id="r1028" name="consist_of" negation="false" src="d0_s10" to="o14964" />
      <relation from="o14965" id="r1029" modifier="rarely" name="part_of" negation="false" src="d0_s10" to="o14967" />
      <relation from="o14966" id="r1030" modifier="rarely" name="part_of" negation="false" src="d0_s10" to="o14967" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernal pools and wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="past_name">fremonti</other_name>
  <other_name type="common_name">Fremont’s goldfields</other_name>
  <discussion>Pappose and epappose plants of Lasthenia fremontii sometimes occur together. The ranges of L. fremontii and L. conjugens overlap slightly; the two species sometimes occur together.</discussion>
  
</bio:treatment>