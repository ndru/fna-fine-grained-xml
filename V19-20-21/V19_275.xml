<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
    <other_info_on_meta type="illustration_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">acuminata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 437. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species acuminata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066438</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">acuminata</taxon_name>
    <taxon_name authority="Babcock &amp; Stebbins" date="unknown" rank="subspecies">pluriflora</taxon_name>
    <taxon_hierarchy>genus Crepis;species acuminata;subspecies pluriflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">angustata</taxon_name>
    <taxon_hierarchy>genus Crepis;species angustata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">seselifolia</taxon_name>
    <taxon_hierarchy>genus Crepis;species seselifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–65 cm (taproots deep, woody, caudices swollen, branched, often covered by old leaf-bases).</text>
      <biological_entity id="o3189" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, erect, stout, branched near or beyond middles, tomentulose (at least proximally).</text>
      <biological_entity id="o3190" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="beyond middles" value_original="beyond middles" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o3191" name="middle" name_original="middles" src="d0_s1" type="structure" />
      <relation from="o3190" id="r313" name="beyond" negation="false" src="d0_s1" to="o3191" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o3192" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic to lanceolate, 8–40 × 0.5–6 (–11) cm, margins deeply pinnately lobed, lobes 5–10 pairs, usually lobed (± halfway to midveins, lobes entire), apices long-acuminate, faces ± tomentulose.</text>
      <biological_entity id="o3193" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="11" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3194" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o3195" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="10" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o3196" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o3197" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 30–70 (–100+), in compound, corymbiform arrays.</text>
      <biological_entity id="o3198" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="100" upper_restricted="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s5" to="70" />
      </biological_entity>
      <biological_entity id="o3199" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o3198" id="r314" name="in" negation="false" src="d0_s5" to="o3199" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 5–7, triangular, tomentulose bractlets 1–2 mm.</text>
      <biological_entity id="o3200" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o3201" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="true" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="tomentulose" value_original="tomentulose" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o3200" id="r315" name="consist_of" negation="false" src="d0_s6" to="o3201" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, 8–16 × 2–3 mm.</text>
      <biological_entity id="o3202" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 5–8, (medially green) lanceolate, 8–12 mm, (margins yellowish, often scarious), apices acute (ciliate), abaxial faces usually glabrous, sometimes sparsely tomentulose, adaxial glabrous.</text>
      <biological_entity id="o3203" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3204" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3205" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3206" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 5–10 (–15);</text>
      <biological_entity id="o3207" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 10–18 mm.</text>
      <biological_entity id="o3208" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae pale yellowish-brown, subcylindric, 6–9 mm, apices ± narrowed (not beaked), ribs 12;</text>
      <biological_entity id="o3209" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale yellowish-brown" value_original="pale yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3210" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o3211" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi white, 6–9 mm. 2n = 22, 33, 44, 55, 88.</text>
      <biological_entity id="o3212" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3213" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="22" value_original="22" />
        <character name="quantity" src="d0_s12" value="33" value_original="33" />
        <character name="quantity" src="d0_s12" value="44" value_original="44" />
        <character name="quantity" src="d0_s12" value="55" value_original="55" />
        <character name="quantity" src="d0_s12" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky hillsides, ridges, grassy flats, open pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky hillsides" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="grassy flats" />
        <character name="habitat" value="open pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Iowa, Mont., Nebr., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Longleaf or tapertip hawksbeard</other_name>
  <discussion>Crepis acuminata is identified by the narrow, pinnately lobed leaves cleft about half way to the midrib and with long-acuminate apices, heads with relatively few florets, relatively small involucres, and glabrous phyllaries. The fertile diploid form of this species is most widespread (E. B. Babcock 1947). In addition, there are apomictic, polyploid populations. The latter often are more variable in leaf size, shape, and indument, and can be difficult to distinguish from C. pleurocarpa and C. intermedia.</discussion>
  
</bio:treatment>