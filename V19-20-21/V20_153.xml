<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="treatment_page">81</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1903" rank="genus">oclemena</taxon_name>
    <taxon_name authority="(Aiton) Greene" date="1903" rank="species">nemoralis</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 5. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus oclemena;species nemoralis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067216</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">nemoralis</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>3: 198. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species nemoralis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucephalus</taxon_name>
    <taxon_name authority="(Aiton) Greene" date="unknown" rank="species">nemoralis</taxon_name>
    <taxon_hierarchy>genus Eucephalus;species nemoralis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Galatella</taxon_name>
    <taxon_name authority="(Aiton) Nees" date="unknown" rank="species">nemoralis</taxon_name>
    <taxon_hierarchy>genus Galatella;species nemoralis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–70 (–90) cm (± loosely clonal, sometimes clustered; rhizomes subsuperficial, elongate, herbaceous or woody, slender, sometimes branched).</text>
      <biological_entity id="o15447" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect, thin (1–2 mm diam.), straight, sparsely strigillose.</text>
      <biological_entity id="o15448" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 30–100+, crowded, proximal sometimes withering by flowering;</text>
      <biological_entity id="o15449" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s2" to="100" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity constraint="proximal" id="o15450" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="sometimes" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to linear-lanceolate, 10–60 × 1–8 mm, bases rounded, margins recurved, appearing entire but remotely serrulate, scabrous, teeth minute, apices obtuse to acute, abaxial faces sparsely hispid, densely stipitate-glandular, adaxial scabrous;</text>
      <biological_entity id="o15451" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15452" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15453" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o15454" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="true" modifier="remotely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o15455" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15456" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15457" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o15453" id="r1408" name="appearing" negation="false" src="d0_s4" to="o15454" />
    </statement>
    <statement id="d0_s5">
      <text>distal slightly reduced, rapidly becoming bracts in arrays.</text>
      <biological_entity constraint="distal" id="o15458" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o15459" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o15460" name="array" name_original="arrays" src="d0_s5" type="structure" />
      <relation from="o15459" id="r1409" name="in" negation="false" src="d0_s5" to="o15460" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–15+, usually borne singly, sometimes in loose corymbiform arrays, branches ascending, at acute angles with stems.</text>
      <biological_entity id="o15461" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o15462" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity id="o15463" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o15464" name="angle" name_original="angles" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15465" name="stem" name_original="stems" src="d0_s6" type="structure" />
      <relation from="o15461" id="r1410" modifier="sometimes" name="in" negation="false" src="d0_s6" to="o15462" />
      <relation from="o15463" id="r1411" name="at" negation="false" src="d0_s6" to="o15464" />
      <relation from="o15464" id="r1412" name="with" negation="false" src="d0_s6" to="o15465" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles filiform, 3–7 cm, ± strigillose, eglandular;</text>
      <biological_entity id="o15466" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3–8, small, linear, scabrous.</text>
      <biological_entity id="o15467" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="small" value_original="small" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres (5–) 6–7.5 mm.</text>
      <biological_entity id="o15468" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries lance-linear to linear, margins often pinkish distally, sparsely to moderately strigillose, sometimes sparsely glandular.</text>
      <biological_entity id="o15469" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" src="d0_s10" to="linear" />
      </biological_entity>
      <biological_entity id="o15470" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s10" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 13–25;</text>
      <biological_entity id="o15471" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s11" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pale to deep pink, seldom white, 11–18 × 1.2–2.6 mm.</text>
      <biological_entity id="o15472" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s12" to="deep pink" />
        <character is_modifier="false" modifier="seldom" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 20–35;</text>
      <biological_entity id="o15473" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas abruptly ampliate, 5–7.5 mm, glabrous;</text>
      <biological_entity id="o15474" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s14" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tubes shorter than or equal to tubular throats, glabrous, lobes erect, triangular, 0.5–1 mm.</text>
      <biological_entity id="o15475" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character constraint="than or equal to tubular throats" constraintid="o15476" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15476" name="throat" name_original="throats" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equal" value_original="equal" />
        <character is_modifier="true" name="shape" src="d0_s15" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o15477" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae tan, fusiform-obconic, ± compressed, 1.9–3 mm, ribs 5–8 (paler than bodies), faces sparsely strigillose or glabrous, gland-dotted;</text>
      <biological_entity id="o15478" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform-obconic" value_original="fusiform-obconic" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15479" name="rib" name_original="ribs" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s16" to="8" />
      </biological_entity>
      <biological_entity id="o15480" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of yellowish or pale salmon-colored bristles in 2 (–3) series, ± equal to disc corollas (outer shorter and very sparse, innermost apically attenuate).</text>
      <biological_entity id="o15482" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="pale salmon-colored" value_original="pale salmon-colored" />
      </biological_entity>
      <biological_entity id="o15483" name="series" name_original="series" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="disc" id="o15484" name="corolla" name_original="corollas" src="d0_s17" type="structure" />
      <relation from="o15481" id="r1413" name="part_of" negation="false" src="d0_s17" to="o15482" />
      <relation from="o15481" id="r1414" name="in" negation="false" src="d0_s17" to="o15483" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity id="o15481" name="pappus" name_original="pappi" src="d0_s17" type="structure" constraint="bristle" constraint_original="bristle; bristle">
        <character constraint="to disc corollas" constraintid="o15484" is_modifier="false" modifier="more or less" name="variability" notes="" src="d0_s17" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15485" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sphagnum bogs and very poor fens, edges of floating bogs, damp sandy shores, acidic or peaty lakeshores, pond margins, cracks in acidic, barren rocks in perhumid areas (Nfld.)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="poor fens" modifier="very" />
        <character name="habitat" value="edges" constraint="of floating bogs" />
        <character name="habitat" value="floating bogs" />
        <character name="habitat" value="sandy shores" modifier="damp" />
        <character name="habitat" value="acidic" />
        <character name="habitat" value="peaty lakeshores" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="cracks" constraint="in acidic , barren rocks in perhumid areas ( nfld . )" />
        <character name="habitat" value="acidic" constraint="in perhumid areas ( nfld . )" />
        <character name="habitat" value="barren rocks" constraint="in perhumid areas ( nfld . )" />
        <character name="habitat" value="perhumid areas" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Conn., Del., Maine, Md., Mass., Mich., N.H., N.J., N.Y., Pa., R.I., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Bog aster</other_name>
  <other_name type="common_name">aster des tourbières</other_name>
  <discussion>Oclemena nemoralis is known only from boreal, northeastern North America, reaching its western limit in the eastern Lake Superior area. It is considered extirpated from Delaware and is endangered in Connecticut and Pennsylvania. It is adapted mainly to acidic Sphagnum bogs and is a wetland indicator. Its small, ericoid, crowded leaves suggest adaptation to open, nutritionally-deficient habitats. The ecogeography and autecology of this species was discussed by L. Brouillet and J.-P. Simon (1979, 1980, 1981). D. E. Moerman (1998) reported that the Chippewa used a decoction of the root (probably rhizome) as drops or on a compress for sore ear. Occasional white-rayed clones occur within O. nemoralis; those have been named Aster nemoralis forma albiflorus Fernald, forma nemoralis designating the more common pink variant. White variants also may represent introgressive clones in areas of hybridization with O. acuminata (see under O. ×blakei).</discussion>
  
</bio:treatment>