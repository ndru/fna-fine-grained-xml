<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">504</other_info_on_meta>
    <other_info_on_meta type="treatment_page">505</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="Besser" date="1829" rank="subgenus">DRANCUNCULUS</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>1: 223. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus DRANCUNCULUS</taxon_hierarchy>
    <other_info_on_name type="fna_id">316930</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, perennials, or subshrubs (shrubs in A. filifolia);</text>
      <biological_entity id="o6403" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted or taprooted, caudices woody, rhizomes absent.</text>
      <biological_entity id="o6404" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o6405" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o6406" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o6407" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems wandlike (new stems may sprout from caudices).</text>
      <biological_entity id="o6408" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="wand-like" value_original="wandlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous (persistent in A. aleutica and A. borealis), usually cauline, sometimes basal, not in fascicles.</text>
      <biological_entity id="o6409" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o6410" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o6411" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads disciform.</text>
      <biological_entity id="o6412" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="disciform" value_original="disciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles epaleate, glabrous.</text>
      <biological_entity id="o6413" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Florets: peripheral 1–25 pistillate and fertile;</text>
      <biological_entity id="o6414" name="floret" name_original="florets" src="d0_s6" type="structure" />
      <biological_entity constraint="peripheral" id="o6415" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="25" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>central 3–32 functionally staminate (not setting fruits);</text>
      <biological_entity id="o6416" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o6417" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="32" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas subglobose.</text>
      <biological_entity id="o6418" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o6419" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>119a.</number>
  <other_name type="past_name">Dracunculi</other_name>
  <discussion>Species ca. 80 (8 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 5–30(–80+) cm (often cespitose and/or mounded)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants (10–)50–180 cm (not cespitose)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials; leaves 2–3-palmately or -pinnately lobed</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials or subshrubs; leaves 1–2-pinnately or -ternately lobed</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 2-palmately lobed; corollas purplish red; Aleutian Islands</description>
      <determination>1 Artemisia aleutica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 2–3-pinnately or -ternately lobed; corollas (at least lobes) usually yellow- orange or deep red; n latitudes and w mountains</description>
      <determination>2 Artemisia borealis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves gray-green, lobes 1–2 mm wide; involucres 3–4 × 3–4 mm; corollas yellow, usually red-tinged, glabrous</description>
      <determination>6 Artemisia pedatifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves silver-green, lobes mostly 2–3 mm wide; involucres 4–5(–7) × 2–3 mm wide; corollas pale yellow, glandular</description>
      <determination>7 Artemisia porteri</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants tarragon-scented or not aromatic; leaves mostly entire, sometimes (basal) irregularly lobed, faces usually glabrous, sometimes glabrescent (deserts)</description>
      <determination>4 Artemisia dracunculus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants faintly to strongly aromatic (not tarragon-scented); leaves lobed, faces hairy</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Shrubs, 60–180 cm (rounded, stems wandlike); involucres 1.5–2 mm diam</description>
      <determination>5 Artemisia filifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Biennials or perennials, (10–)30–80(–150) cm; involucres 2–4.5(–7) mm diam</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems usually 1–5; heads in (mostly leafless) paniculiform arrays</description>
      <determination>3 Artemisia campestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems usually 10+; heads (clustered in glomerules) in (densely leafy) paniculiform to spiciform arrays</description>
      <determination>8 Artemisia pycnocephala</determination>
    </key_statement>
  </key>
</bio:treatment>