<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="section">eublepharis</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">rosea</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 179. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section eublepharis;species rosea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066433</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30 (–60) cm.</text>
      <biological_entity id="o19887" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Internodes (± mid-stem) 1–4 (–5+) cm.</text>
      <biological_entity id="o19888" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o19890" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
      <biological_entity id="o19889" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 0–1 mm, ciliate or not;</text>
      <biological_entity id="o19891" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character name="architecture_or_pubescence_or_shape" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lance-linear to linear or filiform, 20–45 (–60) × 1–2 (–3+) mm, rarely with 1–2 lateral lobes.</text>
      <biological_entity id="o19892" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="to lateral lobes" constraintid="o19893" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19893" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s5" to="3" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" is_modifier="true" modifier="rarely" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–4 (–6+) cm.</text>
      <biological_entity id="o19894" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="6" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of oblong to linear bractlets 1.5–2+ mm.</text>
      <biological_entity id="o19895" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o19896" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" is_modifier="true" name="shape" src="d0_s7" to="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o19895" id="r1349" name="consists_of" negation="false" src="d0_s7" to="o19896" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries deltate-ovate, 4.5–5.5 mm.</text>
      <biological_entity id="o19897" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray laminae pinkish to white, 9–15+ mm.</text>
      <biological_entity constraint="ray" id="o19898" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s9" to="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 40–60+;</text>
      <biological_entity id="o19899" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas ochroleucous to yellow, 2.5–3 mm.</text>
      <biological_entity id="o19900" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="ochroleucous" name="coloration" src="d0_s11" to="yellow" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae narrowly oblong, 1.3–1.8 mm, not winged;</text>
      <biological_entity id="o19901" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0.2n = 26.</text>
      <biological_entity id="o19902" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19903" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy shores, marsh edges, etc.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy shores" />
        <character name="habitat" value="marsh edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S.; Del., Mass., N.J., Pa., R.I., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion>Occurrence of Coreopsis rosea in South Carolina may represent a human-mediated disjunction; the collection came from a “lime sink” near a trailer park close to a freeway.</discussion>
  
</bio:treatment>