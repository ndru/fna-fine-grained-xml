<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="(Nuttall) S. F. Blake" date="1913" rank="section">Tuckermannia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>49: 340. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section Tuckermannia</taxon_hierarchy>
    <other_info_on_name type="fna_id">316935</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="section">Tuckermannia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 363. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Tuckermannia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, subshrubs, or shrubs (often ± fleshy).</text>
      <biological_entity id="o5715" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o5716" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>alternate;</text>
      <biological_entity id="o5718" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (1–) 2–3-pinnately lobed.</text>
      <biological_entity id="o5719" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="(1-)2-3-pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 4–12 bractlets.</text>
      <biological_entity id="o5720" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o5721" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
      <relation from="o5720" id="r424" name="consist_of" negation="false" src="d0_s4" to="o5721" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 8–14+.</text>
      <biological_entity id="o5722" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="14" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae lanceolate to linear-oblong.</text>
      <biological_entity id="o5723" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="linear-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 11–21+, pistillate, fertile;</text>
      <biological_entity id="o5724" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s7" to="21" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae yellow, ± elliptic to oblong, each broadest at or near middle, apices entire or 2–3-toothed.</text>
      <biological_entity id="o5725" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="less elliptic" name="shape" src="d0_s8" to="oblong" />
        <character constraint="at or near middle" is_modifier="false" name="width" src="d0_s8" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o5726" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="2-3-toothed" value_original="2-3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 80–150+;</text>
      <biological_entity id="o5727" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s9" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, lobes 5;</text>
      <biological_entity id="o5728" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o5729" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style-branch apices ± truncate.</text>
      <biological_entity constraint="style-branch" id="o5730" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae ± oblong, wings chartaceous to membranous, entire, not ciliolate, faces glabrous, smooth;</text>
      <biological_entity id="o5731" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o5732" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character char_type="range_value" from="chartaceous" name="texture" src="d0_s12" to="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s12" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o5733" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0, or (1–) 2 cusps 0.1–0.2+ mm (shoulders of wings sometimes bristly).</text>
      <biological_entity id="o5734" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5735" name="cusp" name_original="cusps" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Coastal Calif., adjacent islands; nw Mexico (including Guadalupe Island).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Coastal Calif" establishment_means="native" />
        <character name="distribution" value="adjacent islands" establishment_means="native" />
        <character name="distribution" value="nw Mexico (including Guadalupe Island)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>308a.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Subshrubs or shrubs, (10–)30–250+ cm; heads (1–)8–12+ per stem; peduncles 4–12(–18+) cm; ray florets 8–13+</description>
      <determination>1 Coreopsis gigantea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials, 1–4(–8) dm; heads 1–2(–4+) per stem; peduncles (8–)15–30+ cm; ray florets 16–21+</description>
      <determination>2 Coreopsis maritima</determination>
    </key_statement>
  </key>
</bio:treatment>