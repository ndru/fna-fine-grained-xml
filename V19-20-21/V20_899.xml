<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">machaeranthera</taxon_name>
    <taxon_name authority="Greene" date="1899" rank="species">tagetina</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 71. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus machaeranthera;species tagetina</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067135</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Greene) S. F. Blake" date="unknown" rank="species">tagetinus</taxon_name>
    <taxon_hierarchy>genus Aster;species tagetinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(A. Gray) Standley" date="unknown" rank="species">humilis</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species humilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">tanacetifolia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">humilis</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species tanacetifolia;variety humilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–50 cm.</text>
      <biological_entity id="o18162" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5–40 × 3–17 mm.</text>
      <biological_entity id="o18163" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres broadly turbinate, 5–10 mm.</text>
      <biological_entity id="o18164" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 24–44 in 3–5 series, lanceolate to linear-lanceolate, 4–9 × 0.7–1.5 mm, apices usually appressed, sometimes spreading, acute to acuminate.</text>
      <biological_entity id="o18165" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18166" from="24" name="quantity" src="d0_s3" to="44" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s3" to="linear-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18166" name="series" name_original="series" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o18167" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Receptacles convex, 2–3.5 mm diam.</text>
      <biological_entity id="o18168" name="receptacle" name_original="receptacles" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 8–16;</text>
      <biological_entity id="o18169" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae 6–15 × 1–3 mm.</text>
      <biological_entity id="o18170" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 14–40 (–45);</text>
      <biological_entity id="o18171" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="45" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 4–7 mm, glabrous or glabrate;</text>
      <biological_entity id="o18172" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lobes 0.7–1 mm, hairy (hairs multicellular), 0.2–0.5 mm.</text>
      <biological_entity id="o18173" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2–3.5 (–4) mm;</text>
      <biological_entity id="o18174" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 2–8 mm. 2n = 8.</text>
      <biological_entity id="o18175" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18176" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, Larrea-dominated desert scrub, pine-oak woodlands, roadsides, streambeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="larrea-dominated desert scrub" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1700(–2200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="700" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Mesa tansyaster</other_name>
  <discussion>In the flora area, Machaeranthera tagetina occurs in southern and central Arizona and southwestern New Mexico, in the Chihuahuan and Sonoran desert regions. Much of its range in those states overlaps with that of M. tanacetifolia. In southern and central Arizona, the two species occasionally hybridize.</discussion>
  
</bio:treatment>