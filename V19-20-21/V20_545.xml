<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Pursh) Shinners" date="1951" rank="species">villosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">villosa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species villosa;variety villosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068498</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">mollis</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species mollis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent to erect, 16–39 cm, moderately to densely strigose, sparsely to abundantly long-hirsute, eglandular or sparsely stipitate-glandular.</text>
      <biological_entity id="o6997" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="39" to_unit="cm" />
        <character char_type="range_value" from="strigose" modifier="moderately to densely; densely" name="pubescence" src="d0_s0" to="sparsely abundantly long-hirsute" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s0" to="sparsely abundantly long-hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal cauline leaf-blades oblanceolate, 21–35 × 3.5–6 mm, bases narrowly cuneate to attenuate, margins flat, apices acute, faces moderately strigose (20–60 hairs/mm2), eglandular or sparsely stipitate-glandular.</text>
      <biological_entity constraint="distal cauline" id="o6998" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s1" to="35" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6999" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly cuneate" name="shape" src="d0_s1" to="attenuate" />
      </biological_entity>
      <biological_entity id="o7000" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o7001" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7002" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads 1–12 (–25).</text>
      <biological_entity id="o7003" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="25" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles (5–) 12–47 (–67) mm, sparsely to densely hispido-strigose, eglandular;</text>
      <biological_entity id="o7004" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="47" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="67" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="47" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts subtending heads usually none, rarely linear-oblanceolate, leaflike.</text>
      <biological_entity id="o7005" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s4" value="0" value_original="none" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o7006" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o7005" id="r635" name="subtending" negation="false" src="d0_s4" to="o7006" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres narrowly cylindric to campanulate (fresh), (5–) 6–9 (–11) mm.</text>
      <biological_entity id="o7007" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly cylindric" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries narrowly triangular-lanceolate, margins usually reddish purple distally, faces moderately to densely strigose, eglandular.</text>
      <biological_entity id="o7008" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular-lanceolate" value_original="triangular-lanceolate" />
      </biological_entity>
      <biological_entity id="o7009" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s6" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o7010" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 10–27 (–38), laminae (6.5–) 8.5–15 (–20) mm.</text>
      <biological_entity id="o7011" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="27" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="38" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 18, 36.</text>
      <biological_entity id="o7012" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="8.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7013" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
        <character name="quantity" src="d0_s8" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Oct(–Nov, south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="south" to="Oct" from="Jun" constraint=" (-Nov" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Silty, sandy loam or clay soils, chalk, or granitic soils, gravel soils, dry shale-limestone soils, red sandstone soils, travertine soils of sandhill prairies, pastures, grasslands, roadsides, railroad rights-of-way</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="silty" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="chalk" />
        <character name="habitat" value="granitic soils" modifier="or" />
        <character name="habitat" value="gravel soils" />
        <character name="habitat" value="dry shale-limestone soils" />
        <character name="habitat" value="red sandstone soils" />
        <character name="habitat" value="travertine soils" constraint="of sandhill prairies" />
        <character name="habitat" value="sandhill prairies" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad rights-of-way" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2300(–2900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man. , Ont., Sask.; Colo., Idaho, Ill., Kans., Mich., Mont., Nebr., N.Dak., Oreg., S.Dak., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15a.</number>
  <discussion>Variety villosa grows primarily in the Great Plains region; it is rare in Colorado, Idaho, Illinois, Kansas, and Oregon, and represented in Michigan and southern Ontario by chance introduction.</discussion>
  
</bio:treatment>