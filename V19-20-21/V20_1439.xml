<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">638</other_info_on_meta>
    <other_info_on_meta type="mention_page">640</other_info_on_meta>
    <other_info_on_meta type="treatment_page">639</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">petasites</taxon_name>
    <taxon_name authority="(Linnaeus) Fries" date="1845" rank="species">frigidus</taxon_name>
    <taxon_name authority="(Greene) Cherniawsky" date="1999" rank="variety">×vitifolius</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>76: 2072. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus petasites;species frigidus;variety ×vitifolius</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068950</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Petasites</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">vitifolius</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 180. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Petasites;species vitifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Petasites</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">nivalis</taxon_name>
    <taxon_name authority="(Greene) J. Toman" date="unknown" rank="subspecies">vitifolius</taxon_name>
    <taxon_hierarchy>genus Petasites;species nivalis;subspecies vitifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Petasites</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">trigonophyllus</taxon_name>
    <taxon_hierarchy>genus Petasites;species trigonophyllus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Petasites</taxon_name>
    <taxon_name authority="H. St. John" date="unknown" rank="species">warrenii</taxon_name>
    <taxon_hierarchy>genus Petasites;species warrenii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves: blades palmately to palmati-pinnately nerved, deltate or broadly cordate to reniform, 2–43 × 3.5–47 cm (bases sometimes convex-curved), irregularly palmately to palmati-pinnately lobed (primary lobes 4–14, triangular, sinuses usually less than halfway to bases, lobes subentire to sinuate-dentate, secondary lobes 0–11, entire or dentate, teeth to 44 per side), abaxial and adaxial faces glabrous or woolly to tomentose, sometimes glabrescent.</text>
      <biological_entity constraint="basal" id="o7263" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o7264" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="palmately to palmati-pinnately" name="architecture" src="d0_s0" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="broadly cordate" name="shape" src="d0_s0" to="reniform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s0" to="43" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s0" to="47" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o7265" name="face" name_original="faces" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="palmati-pinnately" name="shape" src="d0_s0" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="woolly" name="pubescence" src="d0_s0" to="tomentose" />
      </biological_entity>
      <relation from="o7264" id="r657" modifier="irregularly palmately; palmately" name="to" negation="false" src="d0_s0" to="o7265" />
    </statement>
    <statement id="d0_s1">
      <text>Staminate heads 3–32;</text>
      <biological_entity id="o7266" name="head" name_original="heads" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="32" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ray-florets 2–27, corolla laminae 2.1–7.8 mm;</text>
      <biological_entity id="o7267" name="ray-floret" name_original="ray-florets" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="27" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o7268" name="lamina" name_original="laminae" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s2" to="7.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>disc-floret style-branches 0.4–1.4 mm, papillate or hairy.</text>
      <biological_entity constraint="disc-floret" id="o7269" name="branch-style" name_original="style-branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="distance" src="d0_s3" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate heads 9–34;</text>
      <biological_entity id="o7270" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s4" to="34" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ray-florets 48–89, corolla laminae 0.8–2.5 mm;</text>
      <biological_entity id="o7271" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="48" name="quantity" src="d0_s5" to="89" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o7272" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>disc-florets: corolla lobes 0.8–2 mm, style-branches 0.3–1.2 mm, papillate or hairy.</text>
      <biological_entity id="o7273" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
      <biological_entity constraint="corolla" id="o7274" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7275" name="branch-style" name_original="style-branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="distance" src="d0_s6" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pappi (pistillate) to 15 mm. 2n = 60.</text>
      <biological_entity id="o7276" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7277" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet marshy sites, moist woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet marshy sites" />
        <character name="habitat" value="moist woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Ont., Que., Sask., Yukon; Mich., Minn., Wash., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1d.</number>
  <other_name type="common_name">Pétasite à feuilles de vigne</other_name>
  <discussion>Petasites frigidus var. ×vitifolius often grows in association with one or both putative parents (P. frigidus var. palmatus and P. frigidus var. sagittatus).</discussion>
  <references>
    <reference>Bogle, A. L. 1968. Evidence for the hybrid origin of Petasites warrenii and P. vitifolius. Rhodora 70: 533–551.</reference>
  </references>
  
</bio:treatment>