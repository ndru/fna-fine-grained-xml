<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">228</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="(Linnaeus) Wallroth" date="1840" rank="species">capillaris</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>14: 657. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species capillaris</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242416375</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lapsana</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">capillaris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 812. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lapsana;species capillaris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">cooperi</taxon_name>
    <taxon_hierarchy>genus Crepis;species cooperi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">virens</taxon_name>
    <taxon_hierarchy>genus Crepis;species virens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials, 10–90 cm (taproots shallow).</text>
      <biological_entity id="o633" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (–6+), erect to ± procumbent, usually simple (usually with single stout leader, sometimes multiple with slender laterals), hispid proximally or throughout.</text>
      <biological_entity id="o635" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="6" upper_restricted="false" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and cauline;</text>
      <biological_entity id="o636" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (petiole bases clasping);</text>
      <biological_entity id="o637" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades lanceolate or oblanceolate, runcinate or lyrate, 5–30 × 1–4.5 cm, margins pinnately divided to sharply dentate (lobes remote, unequal), apices obtuse or acute, mucronate, faces glabrous or sparsely hispid (hairs yellow; proximal cauline auriculate and clasping).</text>
      <biological_entity id="o638" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o639" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="runcinate" value_original="runcinate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="runcinate" value_original="runcinate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o640" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="pinnately divided" name="shape" src="d0_s4" to="sharply dentate" />
      </biological_entity>
      <biological_entity id="o641" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o642" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 10–15 (–30+), in corymbiform arrays.</text>
      <biological_entity id="o643" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="30" upper_restricted="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o644" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o643" id="r69" name="in" negation="false" src="d0_s5" to="o644" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 8, linear, tomentulose or stipitate-glandular bractlets 2–4 mm.</text>
      <biological_entity id="o645" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="8" value_original="8" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o646" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to turbinate, 5–8 × 3–6 mm.</text>
      <biological_entity id="o647" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 8–16, lanceolate, 6–7 mm (margins scarious), apices acute, abaxial faces stipitate-glandular and glandular setose (setae black, usually in 2 rows), adaxial glabrous.</text>
      <biological_entity id="o648" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="16" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o649" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="adaxial abaxial" id="o651" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="true" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="true" name="pubescence" src="d0_s8" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 20–60;</text>
      <biological_entity id="o652" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas deep yellow (reddish abaxially), 8–12 mm (hairy).</text>
      <biological_entity id="o653" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="depth" src="d0_s10" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae brownish yellow, fusiform, 1.5–2.5 mm, apices narrowed (not beaked), ribs 10 (glabrous or scabrous);</text>
      <biological_entity id="o654" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o655" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o656" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi white (fluffy), 3–4 mm (scarcely surpassing phyllaries).</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 6.</text>
      <biological_entity id="o657" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o658" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, pastures, lawns, roadsides, fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., N.B., N.S., Ont., Que.; Alaska, Ark., Calif., Colo., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Miss., Mo., Mont., Nev., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Smooth hawksbeard</other_name>
  <other_name type="common_name">crépis capillaire</other_name>
  <discussion>Crepis capillaris is recognized by its shallow root system, dense rosettes of coarsely dentate or pinnately lobed leaves, erect slender stems, auriculate-based cauline leaves, relatively small heads, phyllaries with double rows of black setae, and fluffy white pappi. It is weedy and can become a serious lawn pest. It is one of only three species of Crepis with 2n = 6; E. B. Babcock (1947) considered it to be advanced in the genus.</discussion>
  
</bio:treatment>