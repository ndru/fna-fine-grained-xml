<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="treatment_page">515</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Elliott) G. L. Nesom" date="1995" rank="species">racemosum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 290. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species racemosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067680</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="species">racemosus</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>2: 348. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species racemosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">brachypholis</taxon_name>
    <taxon_hierarchy>genus Aster;species brachypholis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–90 (–100) cm, colonial or cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous or with woody caudices.</text>
      <biological_entity id="o21640" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="with woody caudices" value_original="with woody caudices" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o21641" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3+, erect (straight), glabrous or glabrate.</text>
      <biological_entity id="o21642" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves thin, margins often ± revolute, scabrous, apices mucronate to mucronulate, faces glabrous or abaxial minutely pilosulous, cauline with clusters of smaller leaves in most axils;</text>
      <biological_entity id="o21643" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o21644" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often more or less" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21645" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="mucronate" name="shape" src="d0_s3" to="mucronulate" />
      </biological_entity>
      <biological_entity id="o21646" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21647" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s3" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o21648" name="face" name_original="faces" src="d0_s3" type="structure" />
      <biological_entity constraint="smaller" id="o21649" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21650" name="axil" name_original="axils" src="d0_s3" type="structure" />
      <relation from="o21648" id="r1999" modifier="with clusters" name="part_of" negation="false" src="d0_s3" to="o21649" />
      <relation from="o21648" id="r2000" name="in" negation="false" src="d0_s3" to="o21650" />
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering (new vernal rosettes often present), petiolate (petioles narrowly winged, sheathing, strigoso-ciliate), blades spatulate to oblanceolate, 5–40 × 5–15 mm, bases attenuate or cuneate to rounded, margins crenate-serrate, apices obtuse to acuminate;</text>
      <biological_entity constraint="basal" id="o21651" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins, apices" constraintid="o21652, o21653, o21654, o21655" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o21652" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o21653" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o21654" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o21655" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, petiolate or subpetiolate (proximalmost) or sessile (petioles winged, sparsely long strigoso-ciliate), blades elliptic or elliptic-lanceolate to lanceolate or linear-lanceolate, 20–70 × 3–20 mm, progressively reduced distally, bases clasping, margins becoming short-ciliate distally;</text>
      <biological_entity constraint="proximal cauline" id="o21656" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades" constraintid="o21657" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s5" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21657" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s5" to="lanceolate or linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o21658" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o21659" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="becoming; distally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal (ascending or spreading) usually sessile, sometimes subpetiolate, blades linear-lanceolate to linear, 5–60+ × 1–8 mm, notably unequal in size, reduced distally, abruptly so on branches, bases cuneate to attenuate, margins serrulate or entire.</text>
      <biological_entity constraint="distal" id="o21660" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="subpetiolate" value_original="subpetiolate" />
      </biological_entity>
      <biological_entity id="o21661" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="notably" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21662" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity id="o21663" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="attenuate" />
      </biological_entity>
      <biological_entity id="o21664" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o21661" id="r2001" modifier="abruptly" name="on" negation="false" src="d0_s6" to="o21662" />
    </statement>
    <statement id="d0_s7">
      <text>Heads in diffuse, ± pyramidal, paniculiform arrays, branches ± lax, spreading horizontally or arching, racemiform, subtended by patent to reflexed branch leaves, often crowded but not (or barely) secund.</text>
      <biological_entity id="o21665" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o21666" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="density" src="d0_s7" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o21667" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="arching" value_original="arching" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity constraint="branch" id="o21668" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="patent" is_modifier="true" name="orientation" src="d0_s7" to="reflexed" />
      </biological_entity>
      <relation from="o21665" id="r2002" name="in" negation="false" src="d0_s7" to="o21666" />
      <relation from="o21667" id="r2003" name="subtended by" negation="false" src="d0_s7" to="o21668" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles slender, 0.2–3+ cm or subsessile, hairy in lines, bracts 5–15, linear-elliptic to acicular, 1–2 mm, glabrous, grading into phyllaries.</text>
      <biological_entity id="o21669" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
        <character constraint="in lines" constraintid="o21670" is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21670" name="line" name_original="lines" src="d0_s8" type="structure" />
      <biological_entity id="o21671" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="15" />
        <character char_type="range_value" from="linear-elliptic" name="shape" src="d0_s8" to="acicular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21672" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o21671" id="r2004" name="into" negation="false" src="d0_s8" to="o21672" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric, (2.5–) 3.5–4.5 (–5.5) mm.</text>
      <biological_entity id="o21673" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–6 series, appressed or outer ± spreading, oblong-lanceolate to linear (innermost), unequal, bases indurate 1/4–1/2, margins narrowly scarious, hyaline, ciliolate, green zones oblanceolate to linear-oblanceolate, apices acute to acuminate, mucronate, sometimes lightly purple-tinged, faces glabrous.</text>
      <biological_entity id="o21674" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity id="o21675" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21676" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s10" to="linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21677" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s10" to="1/2" />
      </biological_entity>
      <biological_entity id="o21678" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o21679" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s10" to="linear-oblanceolate" />
      </biological_entity>
      <biological_entity id="o21680" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="sometimes lightly" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o21681" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21674" id="r2005" name="in" negation="false" src="d0_s10" to="o21675" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (12–) 16–20;</text>
      <biological_entity id="o21682" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s11" to="16" to_inclusive="false" />
        <character char_type="range_value" from="16" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas usually white, rarely pink, laminae 5–8 × 0.5–1.2 mm.</text>
      <biological_entity id="o21683" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o21684" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 10–20 (–25);</text>
      <biological_entity id="o21685" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="25" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas cream or pale-yellow becoming pink or red, (2.5–) 3–4.5 mm, tubes shorter than narrowly funnelform throats, lobes recurved to erect, lanceolate, 0.5–1 mm.</text>
      <biological_entity id="o21686" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="becoming; becoming" name="coloration" src="d0_s14" value="pale-yellow becoming pink or red" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21687" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o21688" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21688" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o21689" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="recurved" name="orientation" src="d0_s14" to="erect" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae gray to tan, obovoid, ± compressed, 1–1.8 mm, 4–5-nerved (faint), faces sparsely strigillose or sericeus;</text>
      <biological_entity id="o21690" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s15" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="4-5-nerved" value_original="4-5-nerved" />
      </biological_entity>
      <biological_entity id="o21691" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
        <character name="pubescence" src="d0_s15" value="sericeus" value_original="sericeus" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white, 2.5–3.5 mm. 2n = 16.</text>
      <biological_entity id="o21692" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21693" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet, often alluvial soils, often brackish, marshes, savannas, bogs, wet meadows, prairie swales, swamps, borders of swamps, open bottomwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet" />
        <character name="habitat" value="moist to alluvial soils" modifier="often" />
        <character name="habitat" value="marshes" modifier="brackish" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="prairie swales" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="borders" constraint="of swamps" />
        <character name="habitat" value="open bottomwoods" />
        <character name="habitat" value="brackish" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., La., Maine, Md., Mass., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <other_name type="common_name">Small white or smooth white oldfield aster</other_name>
  <discussion>Symphyotrichum racemosum is introduced in Canada. The species is cultivated commercially under the name Aster ericoides cv. ‘Spray’. A. G. Jones (1989) reported hybridization with S. dumosum, S. lateriflorum, S. lanceolatum var. interior, and S. ontarionis. The name Aster vimineus Lamarck has been misapplied to this taxon.</discussion>
  
</bio:treatment>