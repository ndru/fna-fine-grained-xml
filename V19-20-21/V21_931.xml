<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="illustration_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arnica</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="species">mollis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 331. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus arnica;species mollis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066122</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–70 cm.</text>
      <biological_entity id="o13915" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (forming clumps) simple or branched among heads.</text>
      <biological_entity id="o13916" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="among heads" constraintid="o13917" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o13917" name="head" name_original="heads" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves (2–) 3 (–4) pairs, mostly cauline (basal sometimes present);</text>
      <biological_entity id="o13918" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="4" />
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (petioles relatively short, broad-winged) or subsessile;</text>
      <biological_entity id="o13919" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades broadly elliptic, lance-elliptic, or narrowly to broadly lanceolate, 4–20 × 1–4 cm, margins entire or irregularly denticulate, apices acute, faces sparsely to moderately hairy (hairs relatively short to long, stipitate-glands or soft, silky).</text>
      <biological_entity id="o13920" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="lance-elliptic or" name="shape" src="d0_s4" to="narrowly broadly lanceolate" />
        <character char_type="range_value" from="lance-elliptic or" name="shape" src="d0_s4" to="narrowly broadly lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13921" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o13922" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13923" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 or 3–7.</text>
      <biological_entity id="o13924" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric to campanulate.</text>
      <biological_entity id="o13925" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 10–22, usually broadly lanceolate, rarely narrowly lanceolate or oblanceolate.</text>
      <biological_entity id="o13926" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="22" />
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely narrowly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 10–22;</text>
      <biological_entity id="o13927" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o13928" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets: corollas yellow;</text>
      <biological_entity id="o13929" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure" />
      <biological_entity id="o13930" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow.</text>
      <biological_entity id="o13931" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure" />
      <biological_entity id="o13932" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae grayish brown to black, 4–8 mm, mostly stipitate-glandular, sparsely hirsutulous (hairs white to brownish, simple or bifid);</text>
      <biological_entity id="o13933" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="grayish brown" name="coloration" src="d0_s12" to="black" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tawny, bristles plumose (with deep, amberlike deposits).</text>
      <biological_entity id="o13934" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38, 57, 76, 95, 114, 133, 152.</text>
      <biological_entity id="o13935" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13936" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
        <character name="quantity" src="d0_s14" value="57" value_original="57" />
        <character name="quantity" src="d0_s14" value="76" value_original="76" />
        <character name="quantity" src="d0_s14" value="95" value_original="95" />
        <character name="quantity" src="d0_s14" value="114" value_original="114" />
        <character name="quantity" src="d0_s14" value="133" value_original="133" />
        <character name="quantity" src="d0_s14" value="152" value_original="152" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows and conifer forests, stream banks, late snow-melt areas, montane to subalpine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="conifer forests" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="late snow-melt areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Hairy arnica</other_name>
  
</bio:treatment>