<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="species">filifolius</taxon_name>
    <taxon_name authority="(A. Nelson) B. L. Turner" date="1956" rank="variety">eriopodus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>58: 225. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species filifolius;variety eriopodus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068510</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenopappus</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">eriopodus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>37: 274. 1904 (as eripoda)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenopappus;species eriopodus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 40–80 cm.</text>
      <biological_entity id="o15684" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal axils ± densely tomentose, terminal lobes 6–40 mm;</text>
      <biological_entity id="o15685" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o15686" name="axil" name_original="axils" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o15687" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 2–7.</text>
      <biological_entity id="o15688" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o15689" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 3–8.</text>
      <biological_entity id="o15690" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 8–16 cm.</text>
      <biological_entity id="o15691" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s4" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 7–10 mm.</text>
      <biological_entity id="o15692" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Florets 30–60;</text>
      <biological_entity id="o15693" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s6" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas whitish, 4–5 mm, throats 2–3 mm, lengths 3–5 times lobes;</text>
      <biological_entity id="o15694" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15695" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="lobe" constraintid="o15696" is_modifier="false" name="length" src="d0_s7" value="3-5 times lobes" value_original="3-5 times lobes" />
      </biological_entity>
      <biological_entity id="o15696" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 3–3.5 mm.</text>
      <biological_entity id="o15697" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 5–6 mm, hairs 0.5–1.5 mm;</text>
      <biological_entity id="o15698" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15699" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 1.5–2 mm.</text>
      <biological_entity id="o15700" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4e.</number>
  
</bio:treatment>