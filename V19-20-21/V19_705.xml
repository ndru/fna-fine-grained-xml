<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">59</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">427</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Tzvelev" date="1990" rank="genus">XEROCHRYSUM</taxon_name>
    <place_of_publication>
      <publication_title>Novosti Sist. Vyssh. Rast.</publication_title>
      <place_in_publication>27: 151. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus XEROCHRYSUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek xeros, dry, and chrysos, gold, perhaps alluding to phyllaries</other_info_on_name>
    <other_info_on_name type="fna_id">316901</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Anderberg &amp; Haegi" date="unknown" rank="genus">Bracteantha</taxon_name>
    <taxon_hierarchy>genus Bracteantha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, 20–90+ cm;</text>
      <biological_entity id="o2262" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (rhizomatous, not stoloniferous).</text>
      <biological_entity id="o2263" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect (rarely 1–2 or more times branched; usually arachnose and ± stipitate-glandular).</text>
      <biological_entity id="o2265" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s5">
      <text>sessile (or nearly so);</text>
      <biological_entity id="o2266" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades elliptic or spatulate to oblanceolate, lanceolate, or linear, bases cuneate, margins entire, faces concolor, usually arachnose and ± stipitate-glandular.</text>
      <biological_entity id="o2267" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s6" to="oblanceolate lanceolate or linear" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s6" to="oblanceolate lanceolate or linear" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s6" to="oblanceolate lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o2268" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o2269" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2270" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="concolor" value_original="concolor" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads disciform, borne singly or (2–3) in loose, corymbiform arrays.</text>
      <biological_entity id="o2271" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="disciform" value_original="disciform" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o2272" from="2" name="atypical_quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o2272" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s7" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± hemispheric, 10–30 mm.</text>
      <biological_entity id="o2273" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–8+ series, usually yellow or brown to purple, sometimes white or pinkish (opaque, stereomes not glandular), unequal, usually chartaceous toward tips (spreading at flowering, deflexed in age).</text>
      <biological_entity id="o2274" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="brown" name="coloration" notes="" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character constraint="toward tips" constraintid="o2276" is_modifier="false" modifier="usually" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o2275" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2276" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <relation from="o2274" id="r219" name="in" negation="false" src="d0_s9" to="o2275" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat, glabrous, epaleate.</text>
      <biological_entity id="o2277" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Peripheral (pistillate) florets (0–) 25–50 (fewer than bisexual);</text>
      <biological_entity constraint="peripheral" id="o2278" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s11" to="25" to_inclusive="false" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow.</text>
      <biological_entity id="o2279" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Inner (bisexual) florets 200–400;</text>
      <biological_entity constraint="inner" id="o2280" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s13" to="400" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow (lobes erect).</text>
      <biological_entity id="o2281" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae columnar to ± prismatic (4-angled), faces smooth, glabrous;</text>
      <biological_entity id="o2282" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="columnar" name="shape" src="d0_s15" to="more or less prismatic" />
      </biological_entity>
      <biological_entity id="o2283" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi readily falling, of 25–35+, distinct or loosely basally ± coherent, subplumose to barbellate bristles in 1 series (falling separately or in groups or rings).</text>
      <biological_entity id="o2285" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s16" to="35" upper_restricted="false" />
        <character is_modifier="true" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="loosely basally more or less" name="fusion" src="d0_s16" value="coherent" value_original="coherent" />
        <character char_type="range_value" from="subplumose" is_modifier="true" name="architecture" src="d0_s16" to="barbellate" />
      </biological_entity>
      <biological_entity id="o2286" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2284" id="r220" name="consist_of" negation="false" src="d0_s16" to="o2285" />
      <relation from="o2285" id="r221" name="in" negation="false" src="d0_s16" to="o2286" />
    </statement>
    <statement id="d0_s17">
      <text>x = 12, 13, 14, 15.</text>
      <biological_entity id="o2284" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o2287" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
        <character name="quantity" src="d0_s17" value="13" value_original="13" />
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
        <character name="quantity" src="d0_s17" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Australia; cultivated and escaping in many areas elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="cultivated and escaping in many areas elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>91.</number>
  <discussion>Species 6 (1 in the flora).</discussion>
  
</bio:treatment>