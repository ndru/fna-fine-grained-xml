<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="treatment_page">206</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">CENTRATHERUM</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1817: 31. 1817</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 7: 383. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus CENTRATHERUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin centrum, center, and atherum, prickle or awn, perhaps alluding to spine-tipped middle phyllaries of original species</other_info_on_name>
    <other_info_on_name type="fna_id">106036</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (or functionally annuals), 1–3 (–8+) dm (stems sometimes rooting at proximal nodes).</text>
      <biological_entity id="o16640" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="8" to_unit="dm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>sessile or petiolate, petioles ± winged;</text>
      <biological_entity id="o16641" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o16642" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s2" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate to obovate, lanceolate, or linear, bases ± cuneate, margins toothed, apices acute, abaxial faces usually ± hirtellous to strigillose or tomentose, sometimes nearly glabrous, adaxial faces sparsely scabrellous or glabrate, both usually resin-gland-dotted.</text>
      <biological_entity id="o16643" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="obovate lanceolate or linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="obovate lanceolate or linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="obovate lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o16644" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16645" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16646" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16647" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="less hirtellous" name="pubescence" src="d0_s3" to="strigillose or tomentose" />
        <character is_modifier="false" modifier="sometimes nearly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16648" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads discoid (pedunculate, each subtended by 3–8+, ± foliaceous bracts), borne singly.</text>
      <biological_entity id="o16649" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric, 6–12 (–18+) mm diam.</text>
      <biological_entity id="o16650" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="18" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 24–50+ in 4–8+ series, each proximally firm, distally ± scarious, the outer ovate to deltate or lanceolate, inner oblong to lanceolate, margins entire, tips rounded to acute, usually apiculate to seta-tipped or attenuate-spinose, abaxial faces glabrous or sparsely strigillose to tomentose and usually ± resin-gland-dotted distally.</text>
      <biological_entity id="o16651" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16652" from="24" name="quantity" src="d0_s6" to="50" upper_restricted="false" />
        <character is_modifier="false" modifier="proximally" name="texture" notes="" src="d0_s6" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="distally more or less" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o16652" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o16653" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="deltate or lanceolate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16654" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o16655" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16656" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="acute" />
        <character char_type="range_value" from="usually apiculate" name="architecture" src="d0_s6" to="seta-tipped or attenuate-spinose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16657" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s6" to="tomentose" />
        <character is_modifier="false" modifier="usually more or less; distally" name="pubescence" src="d0_s6" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 30–50 (–100+);</text>
      <biological_entity id="o16658" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="100" upper_restricted="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas usually lavender to purplish (rarely white), tubes longer than funnelform throats, lobes 5, lance-linear, ± equal.</text>
      <biological_entity id="o16659" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually lavender" name="coloration" src="d0_s8" to="purplish" />
      </biological_entity>
      <biological_entity id="o16660" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character constraint="than funnelform throats" constraintid="o16661" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o16661" name="throat" name_original="throats" src="d0_s8" type="structure" />
      <biological_entity id="o16662" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ± columnar to plumply clavate, 8–10-ribbed, glabrous, often resin-gland-dotted;</text>
      <biological_entity id="o16663" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="less columnar" name="shape" src="d0_s9" to="plumply clavate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="8-10-ribbed" value_original="8-10-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s9" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi caducous, of 20–40 lance-linear to subulate scales.</text>
      <biological_entity id="o16665" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s10" to="40" />
        <character char_type="range_value" from="lance-linear" is_modifier="true" name="shape" src="d0_s10" to="subulate" />
      </biological_entity>
      <relation from="o16664" id="r1497" name="consist_of" negation="false" src="d0_s10" to="o16665" />
    </statement>
    <statement id="d0_s11">
      <text>x = 16.</text>
      <biological_entity id="o16664" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity constraint="x" id="o16666" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Neotropics, Pacific Islands (Philippines), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Neotropics" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Philippines)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Larkdaisy</other_name>
  <discussion>Species 2–4 (1 in the flora).</discussion>
  <references>
    <reference>Gleason, H. A. 1922. Centratherum. In: N. L. Britton et al., eds. 1905+. North American Flora…. 47+ vols. New York. Vol. 33, pp. 49–50.</reference>
    <reference>Kirkman, L. K. 1981. Taxonomic revision of Centratherum and Phyllocephalum (Compositae: Vernonieae). Rhodora 83: 1–24.</reference>
  </references>
  
</bio:treatment>