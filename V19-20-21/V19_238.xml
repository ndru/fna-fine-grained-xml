<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
    <other_info_on_meta type="illustration_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="L’Héritier" date="1789" rank="genus">stokesia</taxon_name>
    <taxon_name authority="(Hill) Greene" date="1893" rank="species">laevis</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>1: 3. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus stokesia;species laevis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242414412</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carthamus</taxon_name>
    <taxon_name authority="Hill" date="unknown" rank="species">laevis</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.,</publication_title>
      <place_in_publication>57, plate 5. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carthamus;species laevis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems tomentulose, glabrescent.</text>
      <biological_entity id="o18717" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal with petioles 3–12 cm, narrowly winged, blades 8–15 × 1–5 cm;</text>
      <biological_entity id="o18718" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18719" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" notes="" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o18720" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18721" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <relation from="o18719" id="r1692" name="with" negation="false" src="d0_s1" to="o18720" />
    </statement>
    <statement id="d0_s2">
      <text>cauline sessile, ± clasping, blades 5–12 × 1–3 cm.</text>
      <biological_entity id="o18722" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o18723" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o18724" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 25–45 × 25–45 mm.</text>
      <biological_entity id="o18725" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 15–35+ mm, foliaceous portions elliptic to spatulate or linear, margins ± spiny, inner oblong to linear, 10–15+ mm, margins mostly entire, tips spiny.</text>
      <biological_entity id="o18726" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o18727" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="35" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o18728" name="portion" name_original="portions" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="spatulate or linear" />
      </biological_entity>
      <biological_entity id="o18729" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18730" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o18731" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18732" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas 15–25+ mm (outer) or 12–15+ mm (inner).</text>
      <biological_entity id="o18733" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" upper_restricted="false" />
        <character name="some_measurement" src="d0_s5" value="12-15+ mm" value_original="12-15+ mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 5–8 mm;</text>
      <biological_entity id="o18734" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 8–12 mm. 2n = 14.</text>
      <biological_entity id="o18735" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18736" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in woodlands, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in woodlands , bogs" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>