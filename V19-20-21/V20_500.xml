<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
    <other_info_on_meta type="illustration_page">227</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">pityopsis</taxon_name>
    <taxon_name authority="(Michaux) Nuttall" date="1840" rank="species">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 318. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pityopsis;species graminifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067351</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Inula</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 122. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Inula;species graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Michaux) Elliott" date="unknown" rank="species">graminifolia</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Michaux) Shinners" date="unknown" rank="species">graminifolia</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–80 cm;</text>
      <biological_entity id="o27310" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 3–20 cm.</text>
      <biological_entity id="o27311" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, erect, green to brown beneath hairs, simple, silvery-sericeous (hairs irregularly anastomosing-cohering).</text>
      <biological_entity id="o27312" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="beneath hairs" constraintid="o27313" from="green" name="coloration" src="d0_s2" to="brown" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="silvery-sericeous" value_original="silvery-sericeous" />
      </biological_entity>
      <biological_entity id="o27313" name="hair" name_original="hairs" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal sessile, blades linear to lanceolate, grasslike, longer or shorter than cauline, 80–250 (–400) × 2–20 mm, faces densely silvery-sericeous (hairs irregularly anastomosing-cohering);</text>
      <biological_entity id="o27314" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o27315" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o27316" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" name="growth_form" src="d0_s3" value="grasslike" value_original="grasslike" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character constraint="than cauline leaves" constraintid="o27317" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="longer or shorter" value_original="longer or shorter" />
        <character char_type="range_value" from="250" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="400" to_unit="mm" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s3" to="250" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o27317" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o27318" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="silvery-sericeous" value_original="silvery-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 20–60, sometimes crowded, spreading to ascending, linear or lanceolate to ovate, usually reduced distally, apices acute, faces silvery-sericeous;</text>
      <biological_entity id="o27319" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o27320" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s4" to="60" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="ascending" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="usually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o27321" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27322" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery-sericeous" value_original="silvery-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distalmost sometimes greatly reduced.</text>
      <biological_entity id="o27323" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="distalmost" id="o27324" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (2–) 10–100+, in corymbiform to sometimes paniculiform arrays.</text>
      <biological_entity id="o27325" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="100" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o27326" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform to sometimes" value_original="corymbiform to sometimes" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o27325" id="r2524" name="in" negation="false" src="d0_s6" to="o27326" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 1–10 cm, sericeous;</text>
      <biological_entity id="o27327" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts and bracteoles 3–20, appressed, often grading into phyllaries.</text>
      <biological_entity id="o27328" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o27329" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="20" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o27330" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o27328" id="r2525" modifier="often" name="into" negation="false" src="d0_s8" to="o27330" />
      <relation from="o27329" id="r2526" modifier="often" name="into" negation="false" src="d0_s8" to="o27330" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres turbino-campanulate, 5–13 mm (usually shorter than pappi).</text>
      <biological_entity id="o27331" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="turbino-campanulate" value_original="turbino-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–6 series, margins fimbriate, piloso-ciliate, faces sparsely to moderately pilose (hairs often twisted), often more densely so distally, sometimes stipitate-glandular.</text>
      <biological_entity id="o27332" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity id="o27333" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o27334" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="piloso-ciliate" value_original="piloso-ciliate" />
      </biological_entity>
      <biological_entity id="o27335" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="often; densely; distally; sometimes" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o27332" id="r2527" name="in" negation="false" src="d0_s10" to="o27333" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 9–13;</text>
      <biological_entity id="o27336" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 4–14 mm.</text>
      <biological_entity id="o27337" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 15–50;</text>
      <biological_entity id="o27338" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s13" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 4–9 mm, limb bases glabrate to sparsely pilose or rarely limbs moderately long-pilose;</text>
      <biological_entity id="o27339" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o27340" name="base" name_original="bases" src="d0_s14" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s14" to="sparsely pilose" />
      </biological_entity>
      <biological_entity id="o27341" name="limb" name_original="limbs" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="long-pilose" value_original="long-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lobes 0.5–0.8 mm, glabrous to sparsely pilose.</text>
      <biological_entity id="o27342" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s15" to="sparsely pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae fusiform, 2.5–4.5 mm, strigose;</text>
      <biological_entity id="o27343" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi: outer scales 0.4–0.9 mm, inner 25–45 bristles 5–9 mm.</text>
      <biological_entity id="o27344" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity constraint="outer" id="o27345" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o27346" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s17" to="45" />
      </biological_entity>
      <biological_entity id="o27347" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ky., La., Md., Miss., N.C., Ohio, Okla., S.C., Tenn., Tex., Va.; se Mexico, Bahamas, Central America (Belize, Guatemala, Honduras).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="se Mexico" establishment_means="native" />
        <character name="distribution" value="Bahamas" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Grass-leaved goldenaster</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>The varieties can be difficult to distinguish in this highly variable species. The infraspecific classification presented by J. C. Semple and F. D. Bowers (1985) is followed here. Involucre height increases somewhat with age, making assignment of post flowering specimens of var. tenuifolia (diploid), var. latifolia (tetraploid), and var. tracyi (hexaploid) more difficult. Data on the distribution of diploids, tetraploids, and hexaploids (Semple and Bowers 1987; subsequent reports) indicate that only diploids occur west of the Mississippi River, while only tetraploids are known in the more northern parts of the range (n Alabama, Tennessee, North Carolina, Virginia). All three ploidy levels occur in Florida, where the three varieties are most distinct. Ploidy level correlates with involucre height in the limited sample of cytovouchers. Additional study is needed to sort out more fully biogeographic patterns of variation in the tenuifolia-latifolia-tracyi complex of Pityopsis graminifolia. An alternative treatment would be to combine all three in a single variety including a polyploid series, under the name var. tenuifolia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres (8–)9–14 mm; disc florets 30+ (tetraploids and hexaploids)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 5–8 mm (Arkansas, Texas, Louisiana; involucres of var. tenuifolia can be 9–10 mm); disc florets 15–29 (diploids)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres 8–12 mm; ray florets 10–16; disc corolla throats and lobes glabrous or proximal throats sparsely short-pilose</description>
      <determination>7d Pityopsis graminifolia var. latifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres 12–14 mm; ray florets 13–25; disc corolla throats and lobes sometimes sparsely to moderately long-pilose</description>
      <determination>7e Pityopsis graminifolia var. tracyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Distal cauline leaves, greatly overlapping, lanceolate to ovate, little reduced distally</description>
      <determination>7b Pityopsis graminifolia var. aequilifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Distal cauline leaves slightly overlapping, linear-lanceolate, reduced distally</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inner phyllaries densely stipitate-glandular apically, sparsely to moderately sericeous proximally (outer coastal plain, Louisiana to North Carolina)</description>
      <determination>7a Pityopsis graminifolia var. graminifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inner phyllaries sometimes sparsely stipitate-glandular, usually sparsely to moderately, rarely densely sericeous throughout (Arkansas and Texas to North Carolina and s Florida)</description>
      <determination>7c Pityopsis graminifolia var. tenuifolia</determination>
    </key_statement>
  </key>
</bio:treatment>