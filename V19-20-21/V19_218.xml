<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">centaurea</taxon_name>
    <taxon_name authority="Lamarck in J. Lamarck et al." date="1785" rank="species">diffusa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>1: 675. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus centaurea;species diffusa</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023635</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acosta</taxon_name>
    <taxon_name authority="(Lamarck) Soják" date="unknown" rank="species">diffusa</taxon_name>
    <taxon_hierarchy>genus Acosta;species diffusa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 20–80 cm.</text>
      <biological_entity id="o5177" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–several, much-branched throughout, puberulent and ± gray tomentose.</text>
      <biological_entity id="o5179" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves hispidulous and ± short-tomentose;</text>
      <biological_entity id="o5180" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="short-tomentose" value_original="short-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline petiolate, often absent at anthesis, blades 10–20 cm, margins bipinnately dissected into narrow lobes;</text>
      <biological_entity constraint="basal and proximal cauline" id="o5181" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character constraint="at anthesis" is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o5182" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5184" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid cauline sessile, bipinnately dissected;</text>
      <biological_entity id="o5183" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="into lobes" constraintid="o5184" is_modifier="false" modifier="bipinnately" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="bipinnately" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal much smaller, entire or pinnately lobed.</text>
      <biological_entity constraint="distal" id="o5185" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s5" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform, in open paniculiform arrays.</text>
      <biological_entity id="o5186" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o5187" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o5186" id="r505" name="in" negation="false" src="d0_s6" to="o5187" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres narrowly ovoid or cylindric, 10–13 × 3–5 mm.</text>
      <biological_entity id="o5188" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Principal phyllaries: bodies pale green, ovate to lanceolate, glabrous or finely tomentose, with a few prominent parallel veins, margins and erect appendages fringed with slender stramineous spines, each phyllary tipped by spine 1–3 mm.</text>
      <biological_entity constraint="principal" id="o5189" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o5190" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o5191" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o5192" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character constraint="with slender spines" constraintid="o5194" is_modifier="false" name="shape" src="d0_s8" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o5193" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="with slender spines" constraintid="o5194" is_modifier="false" name="shape" src="d0_s8" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity constraint="slender" id="o5194" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o5195" name="phyllary" name_original="phyllary" src="d0_s8" type="structure">
        <character constraint="by spine" constraintid="o5196" is_modifier="false" name="architecture" src="d0_s8" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o5196" name="spine" name_original="spine" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o5190" id="r506" name="with" negation="false" src="d0_s8" to="o5191" />
    </statement>
    <statement id="d0_s9">
      <text>Inner phyllaries lanceolate, ± acute, appendage lacerate or spine-tipped.</text>
      <biological_entity constraint="inner" id="o5197" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5198" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 25–35;</text>
      <biological_entity id="o5199" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s10" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas cream white (rarely pink or pale-purple), those of sterile florets 12–13 mm, slender, inconspicuous, those of fertile florets 12–13 mm.</text>
      <biological_entity id="o5200" name="corolla" name_original="corollas" src="d0_s11" type="structure" constraint="floret" constraint_original="floret; floret">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream white" value_original="cream white" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5201" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o5202" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o5200" id="r507" name="part_of" negation="false" src="d0_s11" to="o5201" />
      <relation from="o5200" id="r508" name="part_of" negation="false" src="d0_s11" to="o5202" />
    </statement>
    <statement id="d0_s12">
      <text>Cypselae dark-brown, ca. 2–3 mm;</text>
      <biological_entity id="o5203" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0 or less than 0.5 mm, only rudimentary.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 18, 36.</text>
      <biological_entity id="o5204" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" unit="or lessthan" value="absent" value_original="absent" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="only" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5205" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites in grasslands, woodlands, open coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" constraint="in grasslands , woodlands , open coniferous forests" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="open coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Ont., Que., Sask., Yukon; Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Ky., Mass., Mich., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., Oreg., R.I., Tenn., Utah, Wash., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Diffuse or tumble or white knapweed</other_name>
  <other_name type="common_name">centaurée diffuse</other_name>
  <discussion>Centaurea diffusa is native to southeastern Europe and casually adventive in central and western Europe.</discussion>
  <discussion>Centaurea diffusa readily hybridizes with C. stoebe subsp. micranthos and is often confused with their fertile hybrid (C. ×psammogena G. Gáyer); the latter can be recognized by its cypselae bearing pappi and having conspicuously radiant heads. Morphologically the hybrids are extremely variable; they may be intermediate or may closely resemble one or the other of the parents. Conspicuously radiant heads and pappi are always present; appendages of the phyllaries are brown to black, or rarely stramineous; spines are absent or short and 2n = 18. Centaurea ×psammogena is known from waste places, roadsides, railway tracks; 50–2500 m; B.C., Ont., Que.; Colo., Mass., Mich., Mo., N.C., Oreg., Tenn., Wash. It may occur spontaneously where the ranges of the parent species overlap; they may also be distributed separately. In mixed stands it replaces C. diffusa by introgression. Hybrids are often misidentified as C. diffusa.</discussion>
  
</bio:treatment>