<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="illustration_page">102</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 901. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species virginica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417427</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Verbesina</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">laciniata</taxon_name>
    <taxon_hierarchy>genus Verbesina;species laciniata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Verbesina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginica</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">laciniata</taxon_name>
    <taxon_hierarchy>genus Verbesina;species virginica;variety laciniata;</taxon_hierarchy>
    <other_info_on_name>(based on V. laciniata Nuttall, not Walter)</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (50–) 100–250+ cm (perennating bases ± erect, internodes winged, at least proximal).</text>
      <biological_entity id="o9247" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly alternate (proximal sometimes opposite);</text>
      <biological_entity id="o9248" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades lanceovate or lance-elliptic to lance-linear, 5–12 (–22+) × 1–6 (–12+) cm, sometimes pinnately lobed, bases ± cuneate, ultimate margins usually coarsely toothed to subentire, sometimes sinuate, apices acute, faces ± scabrellous to strigillose.</text>
      <biological_entity id="o9249" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="lance-elliptic to lance-linear" value_original="lance-elliptic to lance-linear" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="22" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o9250" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o9251" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually coarsely toothed" name="shape" src="d0_s2" to="subentire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o9252" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9253" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads (20–) 60–100+ in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o9254" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s3" to="60" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o9255" from="60" name="quantity" src="d0_s3" to="100" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9255" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± obconic to turbinate, 3–5 mm diam.</text>
      <biological_entity id="o9256" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="less obconic" name="shape" src="d0_s4" to="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 8–12+ in 1–2 series, ± erect, spatulate to oblanceolate, 2.5–5 (–7) mm.</text>
      <biological_entity id="o9257" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9258" from="8" name="quantity" src="d0_s5" to="12" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9258" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets (1–) 2–3 (–7);</text>
      <biological_entity id="o9259" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 3–4 (–7+) mm.</text>
      <biological_entity id="o9260" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="7" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 8–12 (–15);</text>
      <biological_entity id="o9261" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="15" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas ochroleucous or white.</text>
      <biological_entity id="o9262" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae dark-brown to blackish, oblanceolate, 3.5–5+ mm, faces ± scabrellous;</text>
      <biological_entity id="o9263" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s10" to="blackish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9264" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s10" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 1.5–3+ mm.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 32, 34.</text>
      <biological_entity id="o9265" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9266" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Sep–Oct(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bottomlands, flood plains, thickets, borders of woodlands, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="borders" constraint="of woodlands , disturbed sites" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., D.C., Fla., Ga., Ill., Kans., Ky., La., Md., Miss., Mo., N.C., Ohio, Okla., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Plants of Verbesina virginica from near the Atlantic Coast with margins of some or most leaf blades sinuate to pinnately 3–5(–7+)-lobed have been called V. laciniata or V. virginica var. laciniata.</discussion>
  
</bio:treatment>