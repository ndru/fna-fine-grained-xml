<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="treatment_page">373</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Cassini) Cassini in F. Cuvier" date="1820" rank="genus">eurybia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1995" rank="species">mirabilis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 261. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eurybia;species mirabilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066755</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">mirabilis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 165. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species mirabilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–120 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>not strongly colonial or in clumps, not or barely stipitate-glandular;</text>
      <biological_entity id="o8665" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" modifier="not strongly" name="growth_form" src="d0_s1" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="in clumps" value_original="in clumps" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8666" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not; barely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes coarse, short, woody.</text>
      <biological_entity id="o8667" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems 1–3+, erect, slightly flexuous, simple, sparsely strigose proximally or glabrescent, densely strigose distally.</text>
      <biological_entity id="o8668" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline, margins crenate-serrate, teeth mucronate, margins scabrous to strigoso-ciliate, apices acute to obtuse, ± acuminate, mucronate, abaxial faces scabrous (short strigose), adaxial ± densely strigose;</text>
      <biological_entity id="o8669" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o8670" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o8671" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o8672" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous to strigoso-ciliate" value_original="scabrous to strigoso-ciliate" />
      </biological_entity>
      <biological_entity id="o8673" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8674" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal and proximal cauline long-petiolate, petioles not to narrowly winged, sheathing, blades ovate, 50–200 × 30–120 mm, bases shallowly cordate to rounded;</text>
      <biological_entity constraint="adaxial" id="o8675" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o8676" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="long-petiolate" value_original="long-petiolate" />
      </biological_entity>
      <biological_entity id="o8677" name="petiole" name_original="petioles" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" from_unit="mm" name="length" notes="" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" notes="" src="d0_s5" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8678" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="architecture" src="d0_s5" value="winged" value_original="winged" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8679" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="shallowly cordate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <relation from="o8677" id="r792" name="to" negation="false" src="d0_s5" to="o8678" />
    </statement>
    <statement id="d0_s6">
      <text>cauline shortly, broadly winged-petiolate to subpetiolate or sessile, ovate or elliptic to lanceolate or oblanceolate, 13–122 × 6–55 mm, gradually reduced distally, bases auriculate, not clasping, or cuneate to winged-attenuate;</text>
      <biological_entity constraint="cauline" id="o8680" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly winged-petiolate" name="architecture" src="d0_s6" to="subpetiolate or sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="lanceolate or oblanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="122" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="55" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o8681" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="winged-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal (arrays) sessile, obovate or oblong to lanceolate, 10–32 × 4–10 mm, bases rounded to attenuate, apices obtuse to rounded or acute.</text>
      <biological_entity constraint="distal" id="o8682" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="32" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8683" name="base" name_original="bases" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="attenuate" />
      </biological_entity>
      <biological_entity id="o8684" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="rounded or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 3–10+ in loose, flat-topped, corymbiform arrays.</text>
      <biological_entity id="o8685" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o8686" from="3" name="quantity" src="d0_s8" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o8686" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character is_modifier="true" name="shape" src="d0_s8" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles densely strigose, sometimes sparsely stipitate-glandular distally;</text>
      <biological_entity id="o8687" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 0–2, broad, densely strigose.</text>
      <biological_entity id="o8688" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres campanulate, 7–12 mm, shorter than pappi.</text>
      <biological_entity id="o8689" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character constraint="than pappi" constraintid="o8690" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8690" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Phyllaries 46–75 (–90) in 5–7 series, oblong-lanceolate (outer) to linear-lanceolate (inner), strongly unequal, membranous, bases indurate, rounded (outer), green zones in distal 1/3–1/2 foliaceous (outer) or 0 (innermost), margins hyaline, narrowly scarious, erose, ciliate, apices ± squarrose or reflexed, obtuse to acute and mucronulate (outer) or acuminate (inner), faces strigoso-villous to strigillose, eglandular.</text>
      <biological_entity id="o8691" name="phyllarie" name_original="phyllaries" src="d0_s12" type="structure">
        <character char_type="range_value" from="75" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="90" />
        <character char_type="range_value" constraint="in series" constraintid="o8692" from="46" name="quantity" src="d0_s12" to="75" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" notes="" src="d0_s12" to="linear-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o8692" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s12" to="7" />
      </biological_entity>
      <biological_entity id="o8693" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8694" name="zone" name_original="zones" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="texture" notes="" src="d0_s12" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8695" name="foliaceou" name_original="foliaceous" src="d0_s12" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s12" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s12" to="1/2" />
      </biological_entity>
      <biological_entity id="o8696" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character constraint="in " constraintid="o8698" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8697" name="foliaceou" name_original="foliaceous" src="d0_s12" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s12" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s12" to="1/2" />
      </biological_entity>
      <biological_entity id="o8698" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8699" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s12" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o8700" name="face" name_original="faces" src="d0_s12" type="structure">
        <character char_type="range_value" from="strigoso-villous" name="pubescence" src="d0_s12" to="strigillose" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o8694" id="r793" name="in" negation="false" src="d0_s12" to="o8695" />
      <relation from="o8694" id="r794" name="in" negation="false" src="d0_s12" to="o8696" />
    </statement>
    <statement id="d0_s13">
      <text>Ray-florets (7–) 16–20 (–30);</text>
      <biological_entity id="o8701" name="ray-floret" name_original="ray-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s13" to="16" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="30" />
        <character char_type="range_value" from="16" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas white to lavender, 10–15 × 1.4–2.1 mm.</text>
      <biological_entity id="o8702" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="lavender" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s14" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s14" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o8703" name="disc-floret" name_original="disc-florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s15" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>corollas pale-yellow, becoming purplish-tinged on lobes, funnelform, slightly ampliate, 6–7.5 mm, tubes longer than throats, lobes spreading, lanceolate, 1–1.5 mm.</text>
      <biological_entity id="o8704" name="corolla" name_original="corollas" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale-yellow" value_original="pale-yellow" />
        <character constraint="on lobes" constraintid="o8705" is_modifier="false" modifier="becoming" name="coloration" src="d0_s16" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" modifier="slightly" name="size" notes="" src="d0_s16" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s16" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8705" name="lobe" name_original="lobes" src="d0_s16" type="structure" />
      <biological_entity id="o8706" name="tube" name_original="tubes" src="d0_s16" type="structure">
        <character constraint="than throats" constraintid="o8707" is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8707" name="throat" name_original="throats" src="d0_s16" type="structure" />
      <biological_entity id="o8708" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae reddish-brown to brown, fusiform, compressed, 3–4 mm, ribs 7–12, faces sparsely strigillose;</text>
      <biological_entity id="o8709" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8710" name="rib" name_original="ribs" src="d0_s17" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s17" to="12" />
      </biological_entity>
      <biological_entity id="o8711" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi of cinnamon (apically clavate) bristles, equaling disc corollas.</text>
      <biological_entity id="o8712" name="pappus" name_original="pappi" src="d0_s18" type="structure" constraint="bristle" constraint_original="bristle; bristle" />
      <biological_entity id="o8713" name="bristle" name_original="bristles" src="d0_s18" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s18" value="cinnamon" value_original="cinnamon" />
      </biological_entity>
      <relation from="o8712" id="r795" name="part_of" negation="false" src="d0_s18" to="o8713" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 18.</text>
      <biological_entity constraint="disc" id="o8714" name="corolla" name_original="corollas" src="d0_s18" type="structure">
        <character is_modifier="true" name="variability" src="d0_s18" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8715" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous or mixed deciduous woods, on slopes or alluvial plains, usually on basic or circumneutral soils, Piedmont Plateau</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="mixed deciduous woods" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="alluvial plains" />
        <character name="habitat" value="basic" />
        <character name="habitat" value="circumneutral soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Bouquet aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eurybia mirabilis is infrequent throughout its range. It has been reported for Alabama and Georgia but no specimens were seen. Such reports may be based on specimens of E. jonesiae identified as Aster commixtus. Both species have been so identified.</discussion>
  
</bio:treatment>