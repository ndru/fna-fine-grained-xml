<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">563</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Greene" date="1888" rank="species">aphanactis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 220. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species aphanactis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067471</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–20+ cm (taproots relatively short and thin).</text>
      <biological_entity id="o19512" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage glabrous or sparsely tomentose (especially distally).</text>
      <biological_entity id="o19513" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1 (relatively thin, delicate).</text>
      <biological_entity id="o19514" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evenly distributed;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o19515" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate to lance-linear, 2–4 × 0.5–1 cm, bases sometimes weakly clasping, margins usually subpinnate to dentate, sometimes subentire (distal leaves bractlike).</text>
      <biological_entity id="o19516" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="lance-linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19517" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes weakly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o19518" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="subpinnate" value_original="subpinnate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subpinnate to dentate" value_original="subpinnate to dentate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 4–10+ in open, cymiform arrays.</text>
      <biological_entity id="o19519" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o19520" from="4" name="quantity" src="d0_s6" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19520" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0 or of 1–3+ lance-deltate bractlets.</text>
      <biological_entity id="o19521" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="of 1-3+ lance-deltate bractlets" value_original="of 1-3+ lance-deltate bractlets" />
      </biological_entity>
      <biological_entity id="o19522" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" upper_restricted="false" />
        <character is_modifier="true" name="shape" src="d0_s7" value="lance-deltate" value_original="lance-deltate" />
      </biological_entity>
      <relation from="o19521" id="r1812" name="consist_of" negation="false" src="d0_s7" to="o19522" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 8 or ± 13, 5–6 mm, tips greenish.</text>
      <biological_entity id="o19523" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
        <character name="quantity" src="d0_s8" value="more or less" value_original="more or less" />
        <character name="quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19524" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0 or 1–5;</text>
      <biological_entity id="o19525" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 0.5–1+ mm (barely surpassing phyllaries, heads perhaps technically disciform).</text>
      <biological_entity constraint="corolla" id="o19526" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae densely hairy.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity id="o19527" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19528" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open ground, especially alkaline flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open ground" />
        <character name="habitat" value="alkaline flats" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>