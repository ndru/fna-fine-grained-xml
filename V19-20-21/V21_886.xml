<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">eriophyllum</taxon_name>
    <taxon_name authority="(I. M. Johnston) Jepson" date="1925" rank="species">mohavense</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1117. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eriophyllum;species mohavense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066713</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eremonanus</taxon_name>
    <taxon_name authority="I. M. Johnston" date="unknown" rank="species">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>68: 101. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eremonanus;species mohavensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–2.5 cm.</text>
      <biological_entity id="o3412" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading.</text>
      <biological_entity id="o3413" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades spatulate to cuneate, 3.5–10 mm, sometimes 1–3-lobed (each lobe terminating in short, sharp cusp), ultimate margins entire, weakly, if at all, revolute (apices acute), faces loosely woolly.</text>
      <biological_entity id="o3414" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3415" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="cuneate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="1-3-lobed" value_original="1-3-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o3416" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="at all" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o3417" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly or in clusters.</text>
      <biological_entity id="o3418" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="in clusters" is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="in clusters" value_original="in clusters" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles ± 0.1 cm.</text>
      <biological_entity id="o3419" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="cm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindric to obconic, 1–2+ mm diam.</text>
      <biological_entity id="o3420" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s5" to="obconic" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s5" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 3–4, distinct.</text>
      <biological_entity id="o3421" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 0.</text>
      <biological_entity id="o3422" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets ± 3;</text>
      <biological_entity id="o3423" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas ± 2 mm (tubes cylindric, throats broadly cylindric, abruptly dilated, lobes glandular; anther appendages deltate, tapering distally, not glandular).</text>
      <biological_entity id="o3424" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2–2.5 mm;</text>
      <biological_entity id="o3425" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 12–14 linear to spatulate scales ± 1.5 mm.</text>
      <biological_entity id="o3426" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o3427" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s11" to="14" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s11" to="spatulate" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <relation from="o3426" id="r264" name="consist_of" negation="false" src="d0_s11" to="o3427" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly areas, creosote-bush scrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly areas" />
        <character name="habitat" value="creosote-bush scrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Mohave or Barstow woolly sunflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>