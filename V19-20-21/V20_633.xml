<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1992" rank="species">sivinskii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>71: 416. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species sivinskii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066680</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–8 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively short, thick, retaining old leaf-bases.</text>
      <biological_entity id="o20435" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o20436" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o20437" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o20436" id="r1893" name="retaining" negation="false" src="d0_s1" to="o20437" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect (greenish), closely strigose (hairs white, equal), eglandular.</text>
      <biological_entity id="o20438" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="closely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent, persistent portions relatively short and broad) and cauline (petioles erect, greenish, bases broadened or not, not thickened and whitish-indurate);</text>
      <biological_entity id="o20439" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear, 12–30 × 0.5–0.8 mm, cauline unreduced for ± 1/2 stems, margins entire, eciliate, faces sparsely strigose, eglandular.</text>
      <biological_entity id="o20440" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20441" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="unreduced" value_original="unreduced" />
        <character modifier="more or less" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o20442" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o20443" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o20444" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o20445" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–6 × (8–) 10–14 mm.</text>
      <biological_entity id="o20446" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (greenish, relatively thinly herbaceous), sparsely hirsuto-pilose along midribs, minutely glandular.</text>
      <biological_entity id="o20447" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="along midribs" constraintid="o20449" is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s7" value="hirsuto-pilose" value_original="hirsuto-pilose" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" notes="" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o20448" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o20449" name="midrib" name_original="midribs" src="d0_s7" type="structure" />
      <relation from="o20447" id="r1894" name="in" negation="false" src="d0_s7" to="o20448" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 21–33;</text>
      <biological_entity id="o20450" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="21" name="quantity" src="d0_s8" to="33" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, with abaxial lilac midstripe, 7–9 mm, laminae coiling.</text>
      <biological_entity id="o20451" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20452" name="midstripe" name_original="midstripe" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="lilac" value_original="lilac" />
      </biological_entity>
      <biological_entity id="o20453" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
      <relation from="o20451" id="r1895" name="with" negation="false" src="d0_s9" to="o20452" />
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3–3.8 mm.</text>
      <biological_entity constraint="disc" id="o20454" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (narrowly oblong) 2.8–3.1 mm, 2 (–3) –nerved, margins sparsely ciliate, faces glabrous;</text>
      <biological_entity id="o20455" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2(-3)-nerved" value_original="2(-3)-nerved" />
      </biological_entity>
      <biological_entity id="o20456" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20457" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 21–27 bristles.</text>
      <biological_entity id="o20458" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o20459" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o20460" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="21" is_modifier="true" name="quantity" src="d0_s12" to="27" />
      </biological_entity>
      <relation from="o20458" id="r1896" name="outer of" negation="false" src="d0_s12" to="o20459" />
      <relation from="o20458" id="r1897" name="inner of" negation="false" src="d0_s12" to="o20460" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Red clay slopes of Summerville Formation and eroded, shale slopes of the Chinle Formation, desert scrub and pinyon-juniper woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="red clay slopes" constraint="of summerville formation and eroded" />
        <character name="habitat" value="shale slopes" modifier="eroded" constraint="of the chinle formation , desert scrub and pinyon-juniper woodland" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="pinyon-juniper woodland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Sivinski’s fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>