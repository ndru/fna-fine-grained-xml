<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Cassini) Cassini in F. Cuvier" date="1820" rank="genus">eurybia</taxon_name>
    <taxon_name authority="(E. S. Burgess) G. L. Nesom" date="1995" rank="species">chlorolepis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 259. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eurybia;species chlorolepis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066745</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="E. S. Burgess" date="unknown" rank="species">chlorolepis</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1211, 1339. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species chlorolepis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">divaricatus</taxon_name>
    <taxon_name authority="(E. S. Burgess) H. E. Ahles" date="unknown" rank="variety">chlorolepis</taxon_name>
    <taxon_hierarchy>genus Aster;species divaricatus;variety chlorolepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 25–80 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>forming ± dense clones (lacking sterile rosettes);</text>
      <biological_entity id="o17525" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes elongate, thin, woody with age.</text>
      <biological_entity id="o17526" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character constraint="with age" constraintid="o17527" is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o17527" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems 1, erect, simple, flexuous, proximally glabrous or villous, more densely villous distally.</text>
      <biological_entity id="o17528" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline, thin, margins ± sharply serrate, teeth (6–20 per side) mucronulate, strigoso-ciliate, apices acuminate, abaxial faces glabrous or sparsely villous, adaxial sparsely strigose, long-stipitate-glandular (black) along veins;</text>
      <biological_entity id="o17529" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o17530" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o17531" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="strigoso-ciliate" value_original="strigoso-ciliate" />
      </biological_entity>
      <biological_entity id="o17532" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17533" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o17535" name="vein" name_original="veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal and proximal cauline withering by flowering, long-petiolate, blades widely ovate, basal smaller than proximal, bases cordate to subcordate (sinuses narrow);</text>
      <biological_entity constraint="adaxial" id="o17534" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character constraint="along veins" constraintid="o17535" is_modifier="false" name="pubescence" src="d0_s4" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o17536" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17537" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="than proximal blades" constraintid="o17538" is_modifier="false" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17538" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o17539" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="subcordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline long (10–50 mm) petiolate, wingless or increasingly winged distally, blades ovate, 30–110 × 11–64 mm, bases cordate (proximal) to rounded or rounded-cuneate (distal);</text>
      <biological_entity constraint="cauline" id="o17540" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="wingless" value_original="wingless" />
        <character is_modifier="false" modifier="increasingly; distally" name="architecture" src="d0_s6" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o17541" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s6" to="110" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s6" to="64" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17542" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s6" to="rounded or rounded-cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal (arrays) sessile or short (0–11 mm), widely-winged petiolate, blades ovate, 8–38 × 5–23 mm.</text>
      <biological_entity constraint="distal" id="o17543" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="widely-winged" value_original="widely-winged" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o17544" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="38" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 3–25+ in open, corymbiform arrays.</text>
      <biological_entity id="o17545" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o17546" from="3" name="quantity" src="d0_s8" to="25" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17546" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles (subtended by ± reduced distal leaves, longest more than 1.5 cm) villous, eglandular;</text>
      <biological_entity id="o17547" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 0 (–1), abruptly smaller than leaves, 2–2.5 × 1–1.5 mm, sometimes subtending heads.</text>
      <biological_entity id="o17548" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="1" />
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character constraint="than leaves" constraintid="o17549" is_modifier="false" name="size" src="d0_s10" value="abruptly smaller" value_original="abruptly smaller" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17549" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity id="o17550" name="head" name_original="heads" src="d0_s10" type="structure" />
      <relation from="o17548" id="r1630" name="sometimes subtending" negation="false" src="d0_s10" to="o17550" />
    </statement>
    <statement id="d0_s11">
      <text>Involucres campanulate, 6.5–9 (–10) mm, shorter than pappi.</text>
      <biological_entity id="o17551" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character constraint="than pappi" constraintid="o17552" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17552" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Phyllaries ca. 27 in 4–5 series, ovate or oblong (outer) to oblong-lanceolate or lanceolate (inner), strongly unequal, membranous, bases indurate, pale green zones on less than distal 1/4 (outer, a few sometimes more than 1/2) to 1/6 or none (inner), margins not scarious, entire or slightly erose, often purplish distally (inner), densely villoso-ciliate, apices obtuse to acute, abaxial faces glabrous or sparsely villous, eglandular.</text>
      <biological_entity id="o17553" name="phyllarie" name_original="phyllaries" src="d0_s12" type="structure">
        <character constraint="in series" constraintid="o17554" name="quantity" src="d0_s12" value="27" value_original="27" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s12" to="oblong-lanceolate or lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o17554" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
      <biological_entity id="o17555" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o17556" name="zone" name_original="zones" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="position_or_shape" src="d0_s12" value="less than distal" value_original="less than distal" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s12" to="1/6" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="0" value_original="none" />
      </biological_entity>
      <biological_entity id="o17557" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s12" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s12" value="villoso-ciliate" value_original="villoso-ciliate" />
      </biological_entity>
      <biological_entity id="o17558" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17559" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Ray-florets (8–) 12–16 (–20);</text>
      <biological_entity id="o17560" name="ray-floret" name_original="ray-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s13" to="12" to_inclusive="false" />
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="20" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s13" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas usually white, sometimes lilac-tinged, (10–) 17–18 (–20) × 2.6–3.3 mm.</text>
      <biological_entity id="o17561" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="lilac-tinged" value_original="lilac-tinged" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s14" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="20" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s14" to="18" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s14" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Disc-florets (12–) 17–26;</text>
      <biological_entity id="o17562" name="disc-floret" name_original="disc-florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s15" to="17" to_inclusive="false" />
        <character char_type="range_value" from="17" name="quantity" src="d0_s15" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>corollas yellow, 5.6–6.1 mm, abruptly ampliate, tubes (3–3.6 mm) longer than campanulate throats (0.7–1.1 mm), lobes reflexed, lanceolate, 1.5–2 mm.</text>
      <biological_entity id="o17563" name="corolla" name_original="corollas" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5.6" from_unit="mm" name="some_measurement" src="d0_s16" to="6.1" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s16" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o17564" name="tube" name_original="tubes" src="d0_s16" type="structure">
        <character constraint="than campanulate throats" constraintid="o17565" is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o17565" name="throat" name_original="throats" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o17566" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae tan to brown, fusiform to cylindro-obovoid, slightly compressed, 3.3–3.5 mm, ribs 7–10, stramineous, faces glabrate to sparsely strigillose;</text>
      <biological_entity id="o17567" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s17" to="brown" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s17" to="cylindro-obovoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17568" name="rib" name_original="ribs" src="d0_s17" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s17" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o17569" name="face" name_original="faces" src="d0_s17" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s17" to="sparsely strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi of pale cinnamon (fine, barbellulate) bristles 6.2–6.3 mm, equaling to longer than disc-florets.</text>
      <biological_entity id="o17571" name="bristle" name_original="bristles" src="d0_s18" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s18" value="pale cinnamon" value_original="pale cinnamon" />
      </biological_entity>
      <biological_entity id="o17572" name="disc-floret" name_original="disc-florets" src="d0_s18" type="structure" />
      <relation from="o17570" id="r1631" name="part_of" negation="false" src="d0_s18" to="o17571" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 36, 45.</text>
      <biological_entity id="o17570" name="pappus" name_original="pappi" src="d0_s18" type="structure" constraint="bristle" constraint_original="bristle; bristle">
        <character char_type="range_value" from="6.2" from_unit="mm" name="some_measurement" src="d0_s18" to="6.3" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s18" value="equaling" value_original="equaling" />
        <character constraint="than disc-florets" constraintid="o17572" is_modifier="false" name="length_or_size" src="d0_s18" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17573" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
        <character name="quantity" src="d0_s19" value="45" value_original="45" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>High elevation Appalachian red spruce–Fraser fir and cool mixed forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fraser fir" modifier="red spruce" />
        <character name="habitat" value="mixed forests" modifier="and cool" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Mountain wood aster</other_name>
  <discussion>Eurybia chlorolepis is known only from the Blue Ridge physiographic province. It is possibly extirpated at the southern end of its range in Georgia, South Carolina, and Tennessee, and it is vulnerable elsewhere. It was mapped by W. F. Lamboy (1992), who showed its distinctness from E. divaricata using morphometric and cytologic data.</discussion>
  
</bio:treatment>