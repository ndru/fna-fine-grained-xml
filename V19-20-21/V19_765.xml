<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">psilocarphus</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="species">chilensis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1: 448. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus psilocarphus;species chilensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067405</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Micropus</taxon_name>
    <taxon_name authority="Bertero ex de Candolle" date="unknown" rank="species">globiferus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 460. 1836,</place_in_publication>
      <other_info_on_pub>not Psilocarphus globiferus Nuttall 1840</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Micropus;species globiferus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tenellus</taxon_name>
    <taxon_name authority="(Bertero ex de Candolle) Morefield" date="unknown" rank="variety">globiferus</taxon_name>
    <taxon_hierarchy>genus P.;species tenellus;variety globiferus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenellus</taxon_name>
    <taxon_name authority="(Eastwood) Cronquist" date="unknown" rank="variety">tenuis</taxon_name>
    <taxon_hierarchy>genus P.;species tenellus;variety tenuis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly greenish, thinly arachnoid-sericeous (in coastal forms grayish to whitish, ± lanuginose).</text>
      <biological_entity id="o12073" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="arachnoid-sericeous" value_original="arachnoid-sericeous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly (1–) 2–7, ascending to ± prostrate;</text>
      <biological_entity id="o12074" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="7" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="more or less prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal internode lengths (2–) 3–6 times leaf lengths.</text>
      <biological_entity constraint="proximal" id="o12075" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o12076" is_modifier="false" name="length" src="d0_s2" value="(2-)3-6 times leaf lengths" value_original="(2-)3-6 times leaf lengths" />
      </biological_entity>
      <biological_entity id="o12076" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Capitular leaves erect to incurved, appressed to heads, ovate to broadly elliptic, widest in proximal 2/3, longest 5–12 mm, lengths mostly 1.2–1.8 (–2) times widths, 1–2 (–2.5) times head heights.</text>
      <biological_entity constraint="capitular" id="o12077" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="incurved" />
        <character constraint="to heads" constraintid="o12078" is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="broadly elliptic" />
        <character constraint="in proximal 2/3" constraintid="o12079" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character is_modifier="false" name="length" notes="" src="d0_s3" value="longest" value_original="longest" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s3" value="1.2-1.8(-2)" value_original="1.2-1.8(-2)" />
        <character constraint="head" constraintid="o12080" is_modifier="false" name="height" src="d0_s3" value="1-2(-2.5) times head heights" value_original="1-2(-2.5) times head heights" />
      </biological_entity>
      <biological_entity id="o12078" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o12079" name="2/3" name_original="2/3" src="d0_s3" type="structure" />
      <biological_entity id="o12080" name="head" name_original="head" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads ± spheric, largest 3–5.5 mm.</text>
      <biological_entity id="o12081" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles unlobed.</text>
      <biological_entity id="o12082" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate paleae usually individually visible through indument, longest mostly 1.5–2.7 mm.</text>
      <biological_entity id="o12083" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="through indument" constraintid="o12084" is_modifier="false" modifier="usually individually" name="prominence" src="d0_s6" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="mostly" name="length" notes="" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12084" name="indument" name_original="indument" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate corollas 0.8–1.3 mm, lobes mostly 4.</text>
      <biological_entity id="o12085" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12086" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae narrowly obovoid, somewhat compressed, 0.6–1.2 mm.</text>
      <biological_entity id="o12087" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid Mar–early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="mid Mar" />
        <character name="fruiting time" char_type="range_value" to="early Jul" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Saturated to drying vernal pool margins, seasonally inundated sites, coastal interdune areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saturated to drying vernal pool margins" />
        <character name="habitat" value="inundated sites" modifier="seasonally" />
        <character name="habitat" value="coastal interdune areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; South America (Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Round woolly marbles</other_name>
  <discussion>Psilocarphus chilensis occurs mainly in west-central California and central Chile; one recent collection is from southern California (western Riverside County). Ecotypes from coastal interdune areas are more lanuginose with shorter stems and internodes than intergrading populations farther inland; they are indistinguishable from the type of Micropus globiferus from Chile (J. D. Morefield 1992d). Psilocarphus chilensis and P. tenellus are at least as distinct as the other species of Psilocarphus; contrary to suggestions by A. Cronquist (1950), intermediates between the two are at most very uncommon.</discussion>
  <discussion>Psilocarphus berteri I. M. Johnston is a superfluous name for P. chilensis. I. M. Johnston (1938) erroneously applied P. chilensis to a species not including the type of Micropus globiferus; such plants are here included in P. brevissimus var. brevissimus.</discussion>
  
</bio:treatment>