<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="species">racemosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 244. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species racemosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067428</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Homopappus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">racemosus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 332. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Homopappus;species racemosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Nuttall) Torrey" date="unknown" rank="species">racemosus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species racemosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–75 cm.</text>
      <biological_entity id="o29108" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–5, erect or strongly ascending, brownish to red-tinged, bases often curved, sparsely to moderately leafy, usually glabrous, sometimes sparsely tomentulose or villous, eglandular.</text>
      <biological_entity id="o29109" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s1" to="red-tinged" />
      </biological_entity>
      <biological_entity id="o29110" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s1" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal (tufted) petiolate, blades lanceolate or oblanceolate to elliptic, 50–200 (–300) × 4–30 mm, margins spinulose-serrate to entire or undulate, ciliate, apices acute, sometimes recurved;</text>
      <biological_entity id="o29111" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o29112" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o29113" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="300" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s2" to="200" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29114" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="spinulose-serrate" value_original="spinulose-serrate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o29115" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline sessile, blades lanceolate, 10–40 × 2–4 mm, reduced distally;</text>
      <biological_entity id="o29116" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o29117" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29118" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>faces usually glabrous, sometimes villous.</text>
      <biological_entity id="o29119" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o29120" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (1–) 3–15+ in spiciform, racemiform, or narrowly paniculiform arrays.</text>
      <biological_entity id="o29121" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o29122" from="3" name="quantity" src="d0_s5" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29122" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" modifier="narrowly" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0–2 cm.</text>
      <biological_entity id="o29123" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric to campanulate, 5–15 × 5–18 mm.</text>
      <biological_entity id="o29124" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 4–5 series, lanceolate to oblanceolate or oblong, 6–13 mm, strongly unequal, margins sometimes ciliate, apices green, obtuse or acute, sometimes recurved, faces glabrous, sparsely tomentulose, or densely villous.</text>
      <biological_entity id="o29125" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s8" to="oblanceolate or oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o29126" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o29127" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o29128" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o29129" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
      <relation from="o29125" id="r2691" name="in" negation="false" src="d0_s8" to="o29126" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 7–28;</text>
      <biological_entity id="o29130" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s9" to="28" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 5–12 mm.</text>
      <biological_entity id="o29131" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 20–65;</text>
      <biological_entity id="o29132" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="65" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 5–8 mm.</text>
      <biological_entity id="o29133" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae subcylindric, 2.5–5.5 mm, 4-angled, faces often sericeous or nearly glabrous;</text>
      <biological_entity id="o29134" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-angled" value_original="4-angled" />
      </biological_entity>
      <biological_entity id="o29135" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi tawny, 6.5–8.5 mm.</text>
      <biological_entity id="o29136" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Clustered goldenweed</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>Pyrrocoma racemosa is generally recognized by its erect stems, tufted leaves, and few to many heads in elongate, racemiform or spiciform arrays. It is the most variable species of Pyrrocoma, with numerous named varieties and subspecies. R. A. Mayes (1976) suggested that P. racemosa is closely related to the small-headed, racemiform species P. uniflora, P. apargioides, and P. lucida. The varieties are intergrading, making them somewhat difficult to identify.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 10–15 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 5–9 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems villous; basal leaf blades lanceolate; phyllaries densely villous</description>
      <determination>12c Pyrrocoma racemosa var. pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually glabrous, rarely tomentulose; basal leaf blades oblanceolate to elliptic; phyllaries glabrous</description>
      <determination>12d Pyrrocoma racemosa var. racemosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Arrays narrowly paniculiform, racemiform, or spiciform, heads crowded only distally</description>
      <determination>12b Pyrrocoma racemosa var. paniculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Arrays usually glomerate-spiciform, heads crowded</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucre 12–16 mm wide; phyllaries yellowish green, apices erect, faces sparsely tomentose at least proximally</description>
      <determination>12a Pyrrocoma racemosa var. congesta</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucre 5–7 mm wide; phyllaries with pale chartaceous bases, apices recurved, green, faces glabrous</description>
      <determination>12e Pyrrocoma racemosa var. sessiliflora</determination>
    </key_statement>
  </key>
</bio:treatment>