<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Nelson" date="1899" rank="species">engelmannii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 247. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species engelmannii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066591</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–20 (–30) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively short and thick.</text>
      <biological_entity id="o8901" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o8902" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, loosely to closely, sparsely to moderately strigose (hairs 0.1–0.9 mm), usually minutely glandular (glands barely evident), sometimes eglandular.</text>
      <biological_entity id="o8903" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="loosely to closely; closely; sparsely to moderately" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="usually minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (usually persistent) and cauline (petioles prominently ciliate at least on proximal portions, hairs spreading, thick-based);</text>
      <biological_entity id="o8904" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades linear-oblanceolate, 20–100 × 1–4 mm, margins entire, faces strigose, eglandular;</text>
      <biological_entity constraint="basal" id="o8905" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8906" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8907" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline usually restricted to proximal 1/2 of stems, slightly reduced distally.</text>
      <biological_entity constraint="cauline" id="o8908" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o8909" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o8908" id="r807" name="restricted to" negation="false" src="d0_s5" to="o8909" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 1 (–3).</text>
      <biological_entity id="o8910" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 3.5–5 (–6) × 7–12 mm.</text>
      <biological_entity id="o8911" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–3 (–4) series, coarsely hirsuto-villous, sparsely to moderately minutely glandular.</text>
      <biological_entity id="o8912" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" notes="" src="d0_s8" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" modifier="sparsely to moderately minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o8913" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o8912" id="r808" name="in" negation="false" src="d0_s8" to="o8913" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets (35–) 45–100;</text>
      <biological_entity id="o8914" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="35" name="atypical_quantity" src="d0_s9" to="45" to_inclusive="false" />
        <character char_type="range_value" from="45" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, sometimes pink or bluish, 5–10 mm, laminae (0.8–1.1 mm wide) primarily reflexing, sometimes also weakly coiling at the tips.</text>
      <biological_entity id="o8915" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="bluish" value_original="bluish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8916" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="primarily" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
        <character constraint="at tips" constraintid="o8917" is_modifier="false" modifier="sometimes; weakly" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o8917" name="tip" name_original="tips" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas (2.5–) 2.7–4.2 mm (throats indurate and inflated, densely puberulent).</text>
      <biological_entity constraint="disc" id="o8918" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s11" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.4–1.8 mm (oblong), 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o8919" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o8920" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer usually of narrow scales, sometimes 0 or of setae, inner of 12–20 bristles.</text>
      <biological_entity id="o8921" name="pappus" name_original="pappi" src="d0_s13" type="structure" constraint="seta" constraint_original="seta; seta">
        <character constraint="of scales" constraintid="o8922" is_modifier="false" name="position" src="d0_s13" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o8922" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character modifier="sometimes" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s13" value="of setae" value_original="of setae" />
      </biological_entity>
      <biological_entity id="o8923" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o8924" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity id="o8925" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
      <relation from="o8921" id="r809" name="part_of" negation="false" src="d0_s13" to="o8923" />
      <relation from="o8924" id="r810" name="consist_of" negation="false" src="d0_s13" to="o8925" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy or rocky sites, prairies, often with sagebrush, rabbitbrush, juniper, pinyon-juniper, salt desert shrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="dry" />
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="rabbitbrush" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="salt desert shrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="past_name">engelmanni</other_name>
  <other_name type="common_name">Engelmann’s fleabane</other_name>
  <discussion>Plants from Chaffee and Fremont counties, Colorado, have strigose leaves and the compact habit of Erigeron engelmannii but sparsely spreading-hirsute stems; they may be intermediate between E. engelmannii and E. concinnus. Unusual variation in the pappi of E. engelmannii also suggests that the species needs study and better definition.</discussion>
  <discussion>A. Cronquist observed that “Erigeron engelmannii intergrades completely with E. pumilus, yet has two geographic subspecies of its own, and shows no distributional similarity to the [taxa] of E. pumilus,” and that E. engelmannii is “smaller and more delicate, with shorter finer hairs, and [has] smaller heads with usually fewer ligules.”</discussion>
  
</bio:treatment>