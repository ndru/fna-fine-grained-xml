<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Chamisso" date="1829" rank="genus">lessingia</taxon_name>
    <taxon_name authority="A. Gray in G. Bentham" date="1849" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>in G. Bentham, Pl. Hartw.,</publication_title>
      <place_in_publication>315. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus lessingia;species nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067082</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–5 (–25) cm.</text>
      <biological_entity id="o11740" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, tan, woolly.</text>
      <biological_entity id="o11741" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" value_original="tan" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal withering by flowering;</text>
      <biological_entity id="o11742" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o11743" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline margins entire, faces gland-dotted (in pits), abaxial woolly.</text>
      <biological_entity id="o11744" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o11745" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11746" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11747" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in corymbiform arrays, in axils of leaves or at ends of branchlets.</text>
      <biological_entity id="o11748" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o11749" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity id="o11750" name="end" name_original="ends" src="d0_s4" type="structure" />
      <relation from="o11748" id="r1076" name="in" negation="false" src="d0_s4" to="o11749" />
      <relation from="o11748" id="r1077" name="in axils of leaves or at" negation="false" src="d0_s4" to="o11750" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres obconic, 7–10 mm.</text>
      <biological_entity id="o11751" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries green, faces woolly, gland-dotted;</text>
      <biological_entity id="o11752" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o11753" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner cartilaginous (stiff, white).</text>
      <biological_entity constraint="inner" id="o11754" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 10–20;</text>
      <biological_entity id="o11755" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white or pale lavender (color more intense in tubes);</text>
      <biological_entity id="o11756" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale lavender" value_original="pale lavender" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style-branch appendages truncate-penicillate, 0.2–0.4 mm.</text>
      <biological_entity constraint="style-branch" id="o11757" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate-penicillate" value_original="truncate-penicillate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi pink to red, longer than cypselae.</text>
      <biological_entity id="o11759" name="cypsela" name_original="cypselae" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 10.</text>
      <biological_entity id="o11758" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="red" />
        <character constraint="than cypselae" constraintid="o11759" is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11760" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open plains, often clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open plains" />
        <character name="habitat" value="clay soils" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Dwarf lessingia</other_name>
  <discussion>Lessingia nana is known from the northern Great Central Valley, adjacent foothills of the Sierra Nevada, and foothills of the Cascade Range.</discussion>
  
</bio:treatment>