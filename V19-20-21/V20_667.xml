<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="1947" rank="species">maguirei</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 165. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species maguirei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066629</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">maguirei</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">harrisonii</taxon_name>
    <taxon_hierarchy>genus Erigeron;species maguirei;variety harrisonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–15 (–28) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively short and thick.</text>
      <biological_entity id="o12406" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="28" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o12407" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending (greenish proximally), densely hirsute (hairs spreading), densely minutely glandular.</text>
      <biological_entity id="o12408" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o12409" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades oblanceolate-spatulate to spatulate (not folding), 20–50 × 3–8 mm (bases often abruptly contracted), margins entire (apices rounded to obtuse), faces densely hirsute to hispido-hirsute, minutely glandular;</text>
      <biological_entity constraint="basal" id="o12410" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate-spatulate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12411" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12412" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="densely hirsute" name="pubescence" src="d0_s4" to="hispido-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades becoming lanceolate, gradually reduced distally or unreduced until heads.</text>
      <biological_entity constraint="cauline" id="o12413" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="until heads" constraintid="o12414" is_modifier="false" name="size" src="d0_s5" value="unreduced" value_original="unreduced" />
      </biological_entity>
      <biological_entity id="o12414" name="head" name_original="heads" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5 (from distal branches).</text>
      <biological_entity id="o12415" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 5–6.5 × 7–11 mm.</text>
      <biological_entity id="o12416" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–4 series, sparsely hirsute, densely minutely glandular.</text>
      <biological_entity id="o12417" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o12418" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o12417" id="r1135" name="in" negation="false" src="d0_s8" to="o12418" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 12–20;</text>
      <biological_entity id="o12419" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white to pink, 6–8 mm, laminae spreading, not coiling or reflexing.</text>
      <biological_entity id="o12420" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12421" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 2.5–3 mm.</text>
      <biological_entity constraint="disc" id="o12422" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.8–2 mm, 2-nerved, faces hirsuto-villous;</text>
      <biological_entity id="o12423" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o12424" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirsuto-villous" value_original="hirsuto-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae or scales, inner of 13–25 bristles.</text>
      <biological_entity id="o12425" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o12426" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o12427" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity id="o12428" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="13" is_modifier="true" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
      <relation from="o12425" id="r1136" name="outer of" negation="false" src="d0_s13" to="o12426" />
      <relation from="o12425" id="r1137" name="outer of" negation="false" src="d0_s13" to="o12427" />
      <relation from="o12425" id="r1138" name="inner of" negation="false" src="d0_s13" to="o12428" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy canyon bottoms, juniper or pinyon-juniper</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy canyon bottoms" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon-juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>82.</number>
  <other_name type="common_name">Maguire’s fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron maguirei is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>