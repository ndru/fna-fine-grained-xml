<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="(Cassini) A. Gray in A. Gray et al." date="1884" rank="section">Dracopis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 263. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section Dracopis</taxon_hierarchy>
    <other_info_on_name type="fna_id">20734</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Cassini" date="unknown" rank="genus">Obeliscaria</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="subgenus">Dracopis</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 35: 273. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Obeliscaria;subgenus Dracopis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Cassini) Cassini" date="unknown" rank="section">Dracopis</taxon_name>
    <taxon_hierarchy>section Dracopis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 25–60 (–120) cm (taprooted).</text>
      <biological_entity id="o133" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green (glaucous, glabrous).</text>
      <biological_entity id="o134" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves bluish green, glaucous;</text>
      <biological_entity id="o135" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal (seldom persisting to flowering) petiolate or sessile, blades elliptic, lanceolate, oblanceolate, oblong, or ovate, not lobed, bases (cauline) auriculate and clasping, margins crenate, entire or serrate, apices acute or acuminate, faces glabrous;</text>
      <biological_entity constraint="basal" id="o136" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o137" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o138" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o139" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o140" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o141" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile, blades elliptic, lanceolate, oblanceolate, oblong, or ovate, bases auriculate and clasping, margins crenate, entire or serrate, apices acute or acuminate, faces glabrous.</text>
      <biological_entity constraint="cauline" id="o142" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o143" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o144" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o145" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o146" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o147" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in loose, corymbiform arrays or borne singly.</text>
      <biological_entity id="o148" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="in loose , corymbiform arrays or borne" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2 series (lengths of outer 2–5+ times inner, inner similar to and sometimes interpreted as paleae).</text>
      <biological_entity id="o149" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity id="o150" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <relation from="o149" id="r5" name="in" negation="false" src="d0_s6" to="o150" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles ovoid to conic;</text>
      <biological_entity id="o151" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s7" to="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae surpassing cypselae (margins ciliate), apices obtuse to acute, often mucronate, faces hairy subapically, glabrous near apices.</text>
      <biological_entity id="o152" name="palea" name_original="paleae" src="d0_s8" type="structure" />
      <biological_entity id="o153" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity id="o154" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acute" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o155" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="subapically" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character constraint="near apices" constraintid="o156" is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o156" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <relation from="o152" id="r6" name="surpassing" negation="false" src="d0_s8" to="o153" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 6–10+;</text>
      <biological_entity id="o157" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, sometimes partly orange or maroon (laminae often with proximal maroon splotch).</text>
      <biological_entity id="o158" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes partly" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="maroon" value_original="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Discs 15–30 × 8–15 mm.</text>
      <biological_entity id="o159" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets to 400+;</text>
      <biological_entity id="o160" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas proximally greenish yellow, distally purplish;</text>
      <biological_entity id="o161" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles ca. 5 mm, branches ca. 1.7 mm, proximal 1/2 stigmatic, apices subulate.</text>
      <biological_entity id="o162" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o163" name="branch" name_original="branches" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.7" value_original="1.7" />
        <character is_modifier="false" name="position" src="d0_s14" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s14" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s14" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o164" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 1.8–2.5 mm;</text>
      <biological_entity id="o165" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 16.</text>
      <biological_entity id="o166" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o167" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>261a.</number>
  <discussion>Species 1.</discussion>
  <discussion>Rudbeckia sect. Dracopis is sometimes recognized at generic rank as Dracopis. DNA-based phylogenetic analyses support its placement in Rudbeckia, where it was treated by Gray as a section. The single species resembles those in sect. Macrocline; it is often placed there based on DNA evidence; the relationship is not uniformly conclusive.</discussion>
  
</bio:treatment>