<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="treatment_page">338</other_info_on_meta>
    <other_info_on_meta type="illustration_page">340</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">bellidiastrum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 307. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species bellidiastrum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066559</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (or biennials?), 3.5–30 (–50) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o4387" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, hirsutulous (hairs upcurved), usually eglandular, sometimes minutely glandular (var. arenarius).</text>
      <biological_entity id="o4388" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (sometimes persistent) and cauline or mostly cauline;</text>
      <biological_entity id="o4389" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to oblanceolate or spatulate, 10–60 (–80) × 2–6 (–9, or 15) mm, margins entire, lobed, or pinnately dissected, faces sparsely strigose, eglandular.</text>
      <biological_entity id="o4390" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate or spatulate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4391" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o4392" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–12 usually in diffuse arrays (from branches beyond midstems or sometimes clustered distally).</text>
      <biological_entity id="o4393" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o4394" from="1" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity id="o4394" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–5 × 5–7 (–11) mm.</text>
      <biological_entity id="o4395" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series, hispidulous, minutely glandular.</text>
      <biological_entity id="o4396" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o4397" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o4396" id="r405" name="in" negation="false" src="d0_s7" to="o4397" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 22–70 (some positioned among inner phyllaries);</text>
      <biological_entity id="o4398" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s8" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, often with abaxial lilac midstripe, drying white to bluish, 4–7.5 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o4399" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="bluish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4400" name="midstripe" name_original="midstripe" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="lilac" value_original="lilac" />
      </biological_entity>
      <biological_entity id="o4401" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
      <relation from="o4399" id="r406" modifier="often" name="with" negation="false" src="d0_s9" to="o4400" />
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.2–3 mm (throats indurate and inflated).</text>
      <biological_entity constraint="disc" id="o4402" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1–1.6 (–1.8) mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o4403" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o4404" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer cartilaginous crowns, inner of 15–18 bristles.</text>
      <biological_entity id="o4405" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o4406" name="crown" name_original="crowns" src="d0_s12" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s12" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4407" name="crown" name_original="crowns" src="d0_s12" type="structure" />
      <biological_entity id="o4408" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="18" />
      </biological_entity>
      <relation from="o4407" id="r407" name="consist_of" negation="false" src="d0_s12" to="o4408" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., Mont., N.Mex., Nebr., Okla., S.Dak., Tex., Utah, Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>158.</number>
  <other_name type="common_name">Sand fleabane</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Erigeron bellidiastrum is recognized by its annual duration, upcurved hairs of the stem, relatively few rays, 1-seriate pappi, and by some ray florets consistently produced between the phyllaries, the mature cypselae of these held in place as the phyllaries reflex at maturity.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaf margins deeply dentate to pinnately lobed</description>
      <determination>158a Erigeron bellidiastrum var. arenarius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaf margins entire or rarely with pair of shallow teeth</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal stems mostly 1–2(–2.5) mm wide; basal and proximal cauline leaf blades linear to linear-oblanceolate, 10–15(–30) × 1–2.5(–3) mm</description>
      <determination>158b Erigeron bellidiastrum var. bellidiastrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal stems mostly (2–)2.5–5 mm wide; basal and proximal cauline leaf blades oblanceolate, 20–40(–60) × 3–5(–15) mm</description>
      <determination>158c Erigeron bellidiastrum var. robustus</determination>
    </key_statement>
  </key>
</bio:treatment>