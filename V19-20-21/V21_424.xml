<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
    <other_info_on_meta type="illustration_page">174</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="genus">viguiera</taxon_name>
    <taxon_name authority="(Cavanilles) Sprengel" date="1826" rank="species">dentata</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>3: 615. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus viguiera;species dentata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067813</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">dentatus</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>3: 10, plate 220. 1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthus;species dentatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 100–200 cm.</text>
      <biological_entity id="o10663" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite (proximal) or alternate (distal);</text>
      <biological_entity id="o10664" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles 10–55 mm;</text>
      <biological_entity id="o10665" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate or rhombic-ovate to lanceovate or lanceolate, 3.5–12.5 × 1–8 cm, margins serrate or serrulate, faces strigose.</text>
      <biological_entity id="o10666" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="rhombic-ovate" name="shape" src="d0_s3" to="lanceovate or lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s3" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10667" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o10668" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually 3–9+ in ± corymbiform arrays.</text>
      <biological_entity id="o10669" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10670" from="3" name="quantity" src="d0_s4" to="9" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10670" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 3–14 cm.</text>
      <biological_entity id="o10671" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 11–18 × 7–10 mm.</text>
      <biological_entity id="o10672" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s6" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllary apices abruptly narrowed to acuminate or spatulate (phyllary bases broad, indurate, apices herbaceous).</text>
      <biological_entity constraint="phyllary" id="o10673" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="abruptly narrowed" name="shape" src="d0_s7" to="acuminate or spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Paleae 6.5 mm, apices prominently cuspidate.</text>
      <biological_entity id="o10674" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="6.5" value_original="6.5" />
      </biological_entity>
      <biological_entity id="o10675" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s8" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 10–14;</text>
      <biological_entity id="o10676" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tubes 1 mm, laminae 7–15 mm.</text>
      <biological_entity id="o10677" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o10678" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 50+;</text>
      <biological_entity id="o10679" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s11" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 3–4 mm (staminal filaments hairy).</text>
      <biological_entity id="o10680" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 3.5–3.8 mm, ± strigose;</text>
      <biological_entity id="o10681" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.8" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 2 lacerate, aristate scales 2.2–2.8 mm plus 2–4 lacerate scales 0.5–0.7 mm. 2n = 34.</text>
      <biological_entity id="o10682" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="4" />
      </biological_entity>
      <biological_entity id="o10683" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s14" value="lacerate" value_original="lacerate" />
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10684" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="lacerate" value_original="lacerate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10685" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
      <relation from="o10682" id="r732" name="consist_of" negation="false" src="d0_s14" to="o10683" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes and canyons, fields, roadside ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadside ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico; West Indies (Cuba); Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Sunflower goldeneye</other_name>
  <other_name type="common_name">toothleaf</other_name>
  <discussion>Viguiera dentata is widespread and variable. It is unique in Helianthinae in the hairy staminal filaments. The name V. dentata var. lancifolia S. F. Blake has been used for plants from Mexico.</discussion>
  
</bio:treatment>