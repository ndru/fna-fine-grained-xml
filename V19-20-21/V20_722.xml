<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">acris</taxon_name>
    <taxon_name authority="(de Candolle) Herder" date="1865" rank="variety">kamtschaticus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>38: 392. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species acris;variety kamtschaticus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">kamtschaticus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 290. 1836 (as kamtschaticum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species kamtschaticus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">acris</taxon_name>
    <taxon_name authority="(de Candolle) H. Hara" date="unknown" rank="subspecies">kamtschaticus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species acris;subspecies kamtschaticus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="(Linnaeus) Gray" date="unknown" rank="species">yellowstonensis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species yellowstonensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trimorpha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">acris</taxon_name>
    <taxon_name authority="(de Candolle) G. L. Nesom" date="unknown" rank="variety">kamtschatica</taxon_name>
    <taxon_hierarchy>genus Trimorpha;species acris;variety kamtschatica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or short-lived perennials, 20–60 (–80) cm;</text>
      <biological_entity id="o18261" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>usually fibrous-rooted, sometimes apparently taprooted, slender caudices simple or branched.</text>
      <biological_entity id="o18262" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" modifier="sometimes apparently" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="slender" id="o18264" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely to moderately strigose or loosely strigoso-villous or nearly glabrous, often also minutely glandular distally.</text>
      <biological_entity id="o18265" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s2" value="strigoso-villous" value_original="strigoso-villous" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often; minutely; distally" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o18266" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate to spatulate, 20–140 × 3–16 mm, cauline spreading, gradually reduced distally, margins usually entire, rarely serrate-dentate, faces glabrous or sparsely villoso-hirsute, distal sometimes also glandular.</text>
      <biological_entity id="o18267" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="140" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o18268" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o18269" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="serrate-dentate" value_original="serrate-dentate" />
      </biological_entity>
      <biological_entity id="o18270" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villoso-hirsute" value_original="villoso-hirsute" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18271" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 or 3–35 in corymbiform to thyrsiform arrays (peduncles curved-ascending).</text>
      <biological_entity id="o18272" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" constraint="in arrays" constraintid="o18273" from="3" name="quantity" src="d0_s5" to="35" />
      </biological_entity>
      <biological_entity id="o18273" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s5" to="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–11 × 8–22 mm.</text>
      <biological_entity id="o18274" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series (often purplish, inner apices attenuate to caudate), glabrous or sometimes sparsely hirsute, minutely glandular.</text>
      <biological_entity id="o18275" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18276" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o18275" id="r1691" name="in" negation="false" src="d0_s7" to="o18276" />
    </statement>
    <statement id="d0_s8">
      <text>Ray (pistillate) florets in 2 series;</text>
      <biological_entity constraint="ray" id="o18277" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o18278" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o18277" id="r1692" name="in" negation="false" src="d0_s8" to="o18278" />
    </statement>
    <statement id="d0_s9">
      <text>outer 150–250, corollas white to pink or purplish, laminae 3–4.5 mm (filiform), erect;</text>
      <biological_entity constraint="outer" id="o18279" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s9" to="250" />
      </biological_entity>
      <biological_entity id="o18280" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink or purplish" />
      </biological_entity>
      <biological_entity id="o18281" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner fewer than outer, corollas tubular, elaminate.</text>
      <biological_entity constraint="inner" id="o18282" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character constraint="than outer florets" constraintid="o18283" is_modifier="false" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18283" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <biological_entity id="o18284" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="elaminate" value_original="elaminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 4.2–6.2 mm.</text>
      <biological_entity constraint="disc" id="o18285" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s11" to="6.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–2.4 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o18286" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o18287" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of (18–) 25–35 (accrescent) bristles.</text>
      <biological_entity id="o18289" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o18290" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="atypical_quantity" src="d0_s13" to="25" to_inclusive="false" />
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s13" to="35" />
      </biological_entity>
      <relation from="o18288" id="r1693" name="outer of" negation="false" src="d0_s13" to="o18289" />
      <relation from="o18288" id="r1694" name="inner of" negation="false" src="d0_s13" to="o18290" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o18288" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o18291" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky and sandy sites, riverbanks, roadsides, and other disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy sites" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="other disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Colo., Idaho, Maine, Mich., Minn., Mont., S.Dak., Utah, Wash., Wyo.; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>115a.</number>
  <other_name type="common_name">Vergerette du Kamtchatka</other_name>
  <discussion>Erigeron angulosus Gaudin, E. asteroides Andrzejowski ex Besser, E. droebachiensis O. F. Mueller ex Retzius, E. elongatus Ledebour, and E. politus Fries, names previously used for the widespread American plants of var. kamtschaticus, refer to Eurasian endemics (summary in G. L. Nesom 2004e). E. Hultén (1968) restricted the name var. kamtschaticus to a single North American collection made at “Junction Firth R. and Mancha Creek on the Alaska-Yukon boundary in August 1961,” treating all others of the species in North America as subsp. politus. Entire-leaved plants apparently are the common form of the species even in the Kamtschatka area, and var. kamtschaticus appears to be correct at varietal rank for the North American plants. Erigeron kamtschaticus has been treated as a species separate from E. acris in other recent literature. Circumscription and rank of E. acris and closely related taxa are in need of study.</discussion>
  <discussion>Erigeron lapiluteus A. Nelson is not a legitimate name.</discussion>
  
</bio:treatment>