<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
    <other_info_on_meta type="illustration_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">horridulum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 90. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species horridulum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066376</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, (± fleshy), 15–250 cm;</text>
      <biological_entity id="o10043" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stout taproots and a fascicle of fleshy lateral roots, often perennating by root sprouts.</text>
      <biological_entity id="o10045" name="taproot" name_original="taproots" src="d0_s1" type="structure" constraint="root" constraint_original="root; root">
        <character is_modifier="true" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character constraint="by root sprouts" constraintid="o10048" is_modifier="false" modifier="often" name="duration" src="d0_s1" value="perennating" value_original="perennating" />
      </biological_entity>
      <biological_entity id="o10046" name="fascicle" name_original="fascicle" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character constraint="by root sprouts" constraintid="o10048" is_modifier="false" modifier="often" name="duration" src="d0_s1" value="perennating" value_original="perennating" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10047" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="root" id="o10048" name="sprout" name_original="sprouts" src="d0_s1" type="structure" />
      <relation from="o10045" id="r926" name="part_of" negation="false" src="d0_s1" to="o10047" />
      <relation from="o10046" id="r927" name="part_of" negation="false" src="d0_s1" to="o10047" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, usually erect, often stout, glabrous to densely tomentose;</text>
      <biological_entity id="o10049" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="densely tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0–many, spreading to ascending, short, stout.</text>
      <biological_entity id="o10050" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades linear to oblanceolate or oblongelliptic, 10–40 × 2–10 cm, unlobed and spiny-dentate to deeply pinnatifid, lobes spiny-dentate or coarsely lobed, main spines stout, 5–30 mm, abaxial faces subglabrous to loosely tomentose, adaxial glabrous to ± densely villous with septate trichomes;</text>
      <biological_entity id="o10051" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10052" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate or oblongelliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="spiny-dentate" name="shape" src="d0_s4" to="deeply pinnatifid" />
      </biological_entity>
      <biological_entity id="o10053" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spiny-dentate" value_original="spiny-dentate" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="main" id="o10054" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10055" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="subglabrous" name="pubescence" src="d0_s4" to="loosely tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10056" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with trichomes" constraintid="o10057" from="glabrous" name="pubescence" src="d0_s4" to="more or less densely villous" />
      </biological_entity>
      <biological_entity id="o10057" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal present at flowering, spiny winged-petiolate, bases often tapered;</text>
      <biological_entity id="o10058" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o10059" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at bases" constraintid="o10060" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10060" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="spiny" value_original="spiny" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline sessile, well distributed, often not much reduced distally, bases often ± auriculate-clasping;</text>
      <biological_entity id="o10061" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o10062" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="often not much; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10063" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often more or less" name="architecture_or_fixation" src="d0_s6" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cauline often spinier than the proximal.</text>
      <biological_entity id="o10064" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o10065" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="than the proximal leaves" constraintid="o10066" is_modifier="false" name="architecture" src="d0_s7" value="often spinier" value_original="often spinier" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10066" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–20 in subcapitate to corymbiform arrays (each closely subtended by an involucrelike ring of spiny-margined bracts).</text>
      <biological_entity id="o10067" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10068" from="1" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
      <biological_entity id="o10068" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character char_type="range_value" from="subcapitate" is_modifier="true" name="architecture" src="d0_s8" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–5 cm.</text>
      <biological_entity id="o10069" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres hemispheric to campanulate, 3–5 × 3–8 cm.</text>
      <biological_entity id="o10070" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s10" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 5–9 series, subequal to imbricate, light green to stramineous, lanceolate to linear, distally often with reddish margins, abaxial faces without glutinous ridge, often ± thinly tomentose, often scabridulous in submarginal bands;</text>
      <biological_entity id="o10071" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="light green" name="coloration" src="d0_s11" to="stramineous" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o10072" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <biological_entity id="o10073" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o10075" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o10076" name="band" name_original="bands" src="d0_s11" type="structure" />
      <relation from="o10071" id="r928" name="in" negation="false" src="d0_s11" to="o10072" />
      <relation from="o10071" id="r929" modifier="distally often; often" name="with" negation="false" src="d0_s11" to="o10073" />
      <relation from="o10074" id="r930" name="without" negation="false" src="d0_s11" to="o10075" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle appressed-ascending, bodies usually reddish-tinged, margins setulose-ciliolate, apices acuminate, spines 1–2 mm, weak;</text>
      <biological_entity constraint="abaxial" id="o10074" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often more or less thinly" name="pubescence" notes="" src="d0_s11" value="tomentose" value_original="tomentose" />
        <character constraint="in submarginal bands" constraintid="o10076" is_modifier="false" modifier="often" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o10077" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="middle" value_original="middle" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="appressed-ascending" value_original="appressed-ascending" />
      </biological_entity>
      <biological_entity id="o10078" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="reddish-tinged" value_original="reddish-tinged" />
      </biological_entity>
      <biological_entity id="o10079" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="setulose-ciliolate" value_original="setulose-ciliolate" />
      </biological_entity>
      <biological_entity id="o10080" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o10081" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s12" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner straight, flat.</text>
      <biological_entity id="o10082" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="straight; straight">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="inner" id="o10083" name="straight" name_original="straight" src="d0_s13" type="structure" />
      <relation from="o10082" id="r931" name="part_of" negation="false" src="d0_s13" to="o10083" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas white to yellow, pink, purple, or red, 30–47 mm, tubes 11–30 mm, throats 6–10 mm, lobes 7–10 mm;</text>
      <biological_entity id="o10084" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="yellow pink purple or red" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="yellow pink purple or red" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="yellow pink purple or red" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="yellow pink purple or red" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s14" to="47" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10085" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s14" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10086" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10087" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 3–5 mm.</text>
      <biological_entity constraint="style" id="o10088" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae straw-colored to tan, 4–6 mm, apical collars weakly differentiated;</text>
      <biological_entity id="o10089" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="straw-colored" name="coloration" src="d0_s16" to="tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o10090" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 25–35 mm. 2n = 32, 33, 34, 35.</text>
      <biological_entity id="o10091" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s17" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10092" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="32" value_original="32" />
        <character name="quantity" src="d0_s17" value="33" value_original="33" />
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
        <character name="quantity" src="d0_s17" value="35" value_original="35" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., La., Maine, Mass., Md., Miss., N.C., N.H., N.J., N.Y., Okla., Pa., R.I., S.C., Tenn., Tex., Va.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Bristly or horrid or yellow or bull thistle</other_name>
  <discussion>Varieties 4 (3 in the flora).</discussion>
  <discussion>Although several variants have been given taxonomic recognition as species, these seem at most races. Flower color varies greatly, sometimes within populations and sometimes on a populational or regional basis. Herbarium specimens are sometimes difficult to assign to variety.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems densely tomentose; involucres ± densely tomentose</description>
      <determination>11a Cirsium horridulum var. horridulum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous or sparsely tomentose; involucres glabrous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves shallowly to deeply pinnatifid, main spines 10–30 mm</description>
      <determination>11b Cirsium horridulum var. megacanthum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves spinose-dentate to shallowly pinnatifid, main spines mostly 5–10 mm</description>
      <determination>11c Cirsium horridulum var. vittatum</determination>
    </key_statement>
  </key>
</bio:treatment>