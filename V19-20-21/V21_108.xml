<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="treatment_page">52</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rudbeckia</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section Rudbeckia</taxon_hierarchy>
    <other_info_on_name type="fna_id">316936</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, to 100 (–200) cm (rhizomatous, fibrous-rooted, or taprooted).</text>
      <biological_entity id="o18815" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green.</text>
      <biological_entity id="o18818" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green, not glaucous;</text>
      <biological_entity id="o18819" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal (usually withering before flowering) usually petiolate, blades linear, lanceolate to ovate, or elliptic to oblanceolate, sometimes lobed, bases attenuate, cuneate, or rounded, margins entire, dentate, or serrate, apices acute to obtuse, faces glabrous or hairy, sometimes glanddotted;</text>
      <biological_entity constraint="basal" id="o18820" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o18821" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate or elliptic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate or elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o18822" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o18823" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o18824" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18825" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline usually petiolate (at least proximal), distal usually sessile, blades linear or ovate to pandurate, sometimes lobed, lobes 3 (–5), bases auriculate, attenuate, or cordate, margins entire or serrate, apices acute to obtuse, faces glabrous or hairy, sometimes glanddotted.</text>
      <biological_entity constraint="cauline" id="o18826" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18827" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o18828" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="pandurate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o18829" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o18830" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o18831" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o18832" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18833" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or in loose, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o18834" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="in arrays" constraintid="o18835" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in loose , corymbiform to paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o18835" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="loose" value_original="loose" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2 (–3) series.</text>
      <biological_entity id="o18836" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity id="o18837" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <relation from="o18836" id="r1283" name="in" negation="false" src="d0_s6" to="o18837" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles usually conic to hemispheric, rarely columnar;</text>
      <biological_entity id="o18838" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually conic" name="shape" src="d0_s7" to="hemispheric" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="columnar" value_original="columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae surpassing cypselae, margins sometimes ciliate, apices obtuse or rounded to acute or attenuate to apiculate, faces glabrous or hairy abaxially.</text>
      <biological_entity id="o18839" name="palea" name_original="paleae" src="d0_s8" type="structure" />
      <biological_entity id="o18840" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity id="o18841" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18842" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="acute or attenuate" />
      </biological_entity>
      <biological_entity id="o18843" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o18839" id="r1284" name="surpassing" negation="false" src="d0_s8" to="o18840" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 6–25+;</text>
      <biological_entity id="o18844" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas usually yellow-orange proximally, yellow distally, sometimes with basal maroon splotch (orangish red to maroon in R. graminifolia).</text>
      <biological_entity id="o18845" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="coloration" src="d0_s10" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18846" name="maroon-splotch" name_original="maroon splotch" src="d0_s10" type="structure" />
      <relation from="o18845" id="r1285" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o18846" />
    </statement>
    <statement id="d0_s11">
      <text>Discs 8–30 × 5–25 mm.</text>
      <biological_entity id="o18847" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 50–800+;</text>
      <biological_entity id="o18848" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="800" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas proximally yellow to yellowish green, distally usually brown-purple, lobes sometimes yellowish or greenish;</text>
      <biological_entity id="o18849" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="proximally yellow" name="coloration" src="d0_s13" to="yellowish green" />
        <character is_modifier="false" modifier="distally usually" name="coloration_or_density" src="d0_s13" value="brown-purple" value_original="brown-purple" />
      </biological_entity>
      <biological_entity id="o18850" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anther appendages sometimes glanddotted;</text>
      <biological_entity constraint="anther" id="o18851" name="appendage" name_original="appendages" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>styles 3–5+ mm, branches 1–1.8 mm, proximal 1/2–4/5 stigmatic, apices acute to rounded.</text>
      <biological_entity id="o18852" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o18853" name="branch" name_original="branches" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s15" to="4/5" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s15" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o18854" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 1.5–3.5 (–4) mm;</text>
      <biological_entity id="o18855" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi usually coroniform (sometimes cypselae each with glandular-hairs around apices in R. heliopsidis).</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 19.</text>
      <biological_entity id="o18856" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s17" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o18857" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>261c.</number>
  <discussion>Species 9 (9 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 0.2–1 cm wide (grasslike, lengths 10+ times widths); rays orangish red to maroon; Florida panhandle</description>
      <determination>16 Rudbeckia graminifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves (0.4–)1–15 cm wide (if less than 1 cm wide, lengths less than 10 times widths); rays yellow to yellow-orange (at least distally); e North America</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals, biennials, or perennials (taprooted or roots fibrous); pappi 0 or coroniform (to 0.1 mm)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials (usually rhizomatous and/or stoloniferous, roots fibrous, caudices sometimes woody); pappi 0 or coroniform (0.1–0.5 mm; sometimes cypselae each with glandular hairs around apices in R. heliopsidis)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems and leaves coarsely hispid to hirsute; style branch apices subulate; e NorthAmerica</description>
      <determination>19 Rudbeckia hirta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems and leaves softly pilose to woolly; style branch apices acute to obtuse; Alabama, Florida, Georgia, South Carolina</description>
      <determination>21 Rudbeckia mollis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal cauline leaves elliptic or ovate, usually 3(–5)-lobed; paleae cuspidate (tips awnlike, 1.5+ mm)</description>
      <determination>23 Rudbeckia triloba</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal cauline leaves elliptic, linear, spatulate, or ovate, rarely lobed (3–5–lobed in R. subtomentosa); paleae acute, obtuse, or rounded</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems and leaves densely hirsute; proximal cauline leaves 3(–5)–lobed; heads (usually 8–25) in loose, corymbiform to paniculiform arrays</description>
      <determination>22 Rudbeckia subtomentosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems and leaves glabrous or sparsely to moderately hairy; proximal cauline leaves not lobed; heads borne singly or (2–12) in corymbiform arrays</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves: abaxial faces glabrous or hirsute to strigose (not gland-dotted); paleae: abaxial tips usually glabrous, rarely pilose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves: abaxial faces glabrous or strigose and gland-dotted; paleae: abaxial tips canescent or strigose, sometimes gland-dotted as well</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants stoloniferous (rosettes forming at stolon apices; stem branches spreading); basal leaves lanceolate to broadly ovate or elliptic</description>
      <determination>15 Rudbeckia fulgida</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants not stoloniferous (rosettes at bases of aerial stems; stem branches ascending); basal leaves linear to narrowly spatulate</description>
      <determination>20 Rudbeckia missouriensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades elliptic, lanceolate, or ovate (flat); heads usually (4–8) in ± corymbiform arrays, sometimes borne singly; rays 6–12 (± spreading); pappi coroniform, to 1.5 mm (sometimes cypselae each with glandular hairs around apices); Alabama, Georgia, North Carolina, South Carolina, Virginia</description>
      <determination>18 Rudbeckia heliopsidis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades elliptic, lanceolate, or ovate (± conduplicate); heads usually borne singly; rays 12–25 (reflexed); pappi coroniform, to 0.5 mm; midwestern and sc United States</description>
      <determination>17 Rudbeckia grandiflora</determination>
    </key_statement>
  </key>
</bio:treatment>