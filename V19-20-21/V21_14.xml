<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="treatment_page">15</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ambrosia</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">bidentata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 182. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus ambrosia;species bidentata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416041</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–30 (–100+) cm.</text>
      <biological_entity id="o17280" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o17281" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly opposite;</text>
      <biological_entity id="o17282" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 0–0.5 mm;</text>
      <biological_entity id="o17283" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades ± lanceolate to lance-linear, 15–40+ × 3–6 (–10+) mm, bases rounded to cordate, margins entire or with (1–) 2 (–4) basal lobes, abaxial and adaxial faces ± piloso-hispid and glanddotted.</text>
      <biological_entity id="o17284" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="less lanceolate" name="shape" src="d0_s4" to="lance-linear" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17285" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o17286" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with (1-)2(-4) basal lobes" value_original="with (1-)2(-4) basal lobes" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17287" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="4" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o17288" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="piloso-hispid" value_original="piloso-hispid" />
      </biological_entity>
      <relation from="o17286" id="r1190" name="with" negation="false" src="d0_s4" to="o17287" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate heads clustered, proximal to staminates;</text>
      <biological_entity id="o17289" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
        <character constraint="to staminates" constraintid="o17290" is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o17290" name="staminate" name_original="staminates" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>florets 1.</text>
      <biological_entity id="o17291" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate heads: peduncles 0–0.5 mm;</text>
      <biological_entity id="o17292" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17293" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucres obliquely cupshaped (lateral lobe longer than others), 2.5–4 mm diam., piloso-hispid;</text>
      <biological_entity id="o17294" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17295" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s8" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="piloso-hispid" value_original="piloso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>florets 6–8+.</text>
      <biological_entity id="o17296" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17297" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Burs: bodies pyramidal, 5–8 mm, piloso-hispid, spines 4 (–5), ± distal, ± acerose, 0.5–1 mm, tips straight.</text>
      <biological_entity id="o17299" name="body" name_original="bodies" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="pyramidal" value_original="pyramidal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="piloso-hispid" value_original="piloso-hispid" />
      </biological_entity>
      <biological_entity id="o17300" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="5" />
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" modifier="more or less" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17301" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Minn., Miss., Mo., N.C., Ohio, Okla., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Hybrids between Ambrosia bidentata and A. trifida have been recorded.</discussion>
  
</bio:treatment>