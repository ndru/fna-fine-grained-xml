<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="treatment_page">350</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. L. P. de Candolle" date="1838" rank="genus">monolopia</taxon_name>
    <taxon_name authority="(A. Gray) B. G. Baldwin" date="1999" rank="species">congdonii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 460. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus monolopia;species congdonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067191</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eatonella</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">congdonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 20. 1883 (as congdoni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eatonella;species congdonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lembertia</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">congdonii</taxon_name>
    <taxon_hierarchy>genus Lembertia;species congdonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves narrowly oblong (10–45 mm, margins shallowly sinuate-dentate or entire, faces loosely woolly).</text>
      <biological_entity id="o19101" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 2–20 mm.</text>
      <biological_entity id="o19102" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres ± 4.5 mm.</text>
      <biological_entity id="o19103" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="4.5" value_original="4.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 4–7, distinct, elliptic to oblanceolate (reflexed in fruit), apices acute.</text>
      <biological_entity id="o19104" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o19105" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 4–7;</text>
      <biological_entity id="o19106" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminae 0.5 mm, ± equally 3-lobed (inconspicuous).</text>
      <biological_entity id="o19107" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="more or less equally" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 20–40 (corolla lobes 4, with glandular-hairs; anther appendages widest at middles).</text>
      <biological_entity id="o19108" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae obcompressed, ± 3 mm, 3-angled (peripheral), 2-angled (disc), hairy on margins (pappi of 2–7 spatulate, erose to laciniate scales ± 1 mm).</text>
      <biological_entity id="o19110" name="margin" name_original="margins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 22.</text>
      <biological_entity id="o19109" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obcompressed" value_original="obcompressed" />
        <character modifier="more or less" name="some_measurement" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-angled" value_original="2-angled" />
        <character constraint="on margins" constraintid="o19110" is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19111" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy areas in grasslands or on alkali sinks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" constraint="in grasslands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="alkali sinks" modifier="or on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>