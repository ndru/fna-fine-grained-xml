<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="illustration_page">72</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="A. Gray" date="1882" rank="genus">hecastocleis</taxon_name>
    <taxon_name authority="A. Gray" date="1882" rank="species">shockleyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 221. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus hecastocleis;species shockleyi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220006114</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blades 1–4 cm, bases ± clasping, margins thickened, usually with 3–6 spines 1–3 mm near bases, apices mucronate or acute.</text>
      <biological_entity id="o21120" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o21121" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21122" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s0" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o21123" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o21124" name="spine" name_original="spines" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s0" to="6" />
        <character char_type="range_value" constraint="near bases" constraintid="o21125" from="1" from_unit="mm" name="location" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21125" name="base" name_original="bases" src="d0_s0" type="structure" />
      <biological_entity id="o21126" name="apex" name_original="apices" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o21123" id="r1913" modifier="usually" name="with" negation="false" src="d0_s0" to="o21124" />
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries usually lanate on margins.</text>
      <biological_entity id="o21127" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure">
        <character constraint="on margins" constraintid="o21128" is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o21128" name="margin" name_original="margins" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Corollas pinkish purple in bud, white at flowering, ca. 10 mm.</text>
      <biological_entity id="o21129" name="corolla" name_original="corollas" src="d0_s2" type="structure">
        <character constraint="in bud" constraintid="o21130" is_modifier="false" name="coloration" src="d0_s2" value="pinkish purple" value_original="pinkish purple" />
        <character constraint="at flowering" is_modifier="false" name="coloration" notes="" src="d0_s2" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s2" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o21130" name="bud" name_original="bud" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Cypselae brown, 5 mm;</text>
      <biological_entity id="o21131" name="cypsela" name_original="cypselae" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pappi 1–2 mm. 2n = 16.</text>
      <biological_entity id="o21132" name="pappus" name_original="pappi" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21133" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granite, basalt, and caliche soils, low desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granite" />
        <character name="habitat" value="basalt" />
        <character name="habitat" value="caliche soils" />
        <character name="habitat" value="low desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Prickle-leaf</other_name>
  <discussion>Hecastocleis shockleyi is known from southwestern Nevada and adjacent California.</discussion>
  
</bio:treatment>