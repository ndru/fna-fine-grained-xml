<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="illustration_page">369</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1829" rank="genus">lygodesmia</taxon_name>
    <taxon_name authority="(Pursh) D. Don ex Hooker" date="1833" rank="species">juncea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 295. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lygodesmia;species juncea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416809</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prenanthes</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">juncea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 498. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Prenanthes;species juncea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–35 (–70) cm (in bushy clumps);</text>
      <biological_entity id="o11898" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots deep, vertical, rhizomes branched, woody.</text>
      <biological_entity id="o11899" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="depth" src="d0_s1" value="deep" value_original="deep" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o11900" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending or decumbent, green, glaucous, much branched from bases and distally, strongly striate, glabrous (often bearing round galls).</text>
      <biological_entity id="o11901" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending or decumbent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character constraint="from bases" constraintid="o11902" is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally; strongly" name="coloration_or_pubescence_or_relief" notes="" src="d0_s2" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11902" name="base" name_original="bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves (basal not in rosettes, absent at flowering);</text>
      <biological_entity id="o11903" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal blades linear, 5–30 (–60) × 1–2 (–4) mm, margins entire, apices acute, faces glabrous;</text>
      <biological_entity constraint="proximal" id="o11904" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11905" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11906" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11907" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline reduced to subulate scales.</text>
      <biological_entity constraint="cauline" id="o11908" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o11909" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (1–50+) borne singly or in corymbiform arrays.</text>
      <biological_entity id="o11910" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="50" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o11911" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o11910" id="r1101" name="in" negation="false" src="d0_s6" to="o11911" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, 10–16 × 4–6 mm, apices spreading.</text>
      <biological_entity id="o11912" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11913" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 8, ovate to linear bractlets 2–4 mm, margins erose-ciliate (faces glabrous).</text>
      <biological_entity id="o11914" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="linear" />
      </biological_entity>
      <biological_entity id="o11915" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11916" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 5 (–7), linear, 10–15 mm, margins scarious, apices acute or obtuse, not appendaged, faces glabrous.</text>
      <biological_entity id="o11917" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="7" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11918" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o11919" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="appendaged" value_original="appendaged" />
      </biological_entity>
      <biological_entity id="o11920" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets usually 5;</text>
      <biological_entity id="o11921" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas usually light pink to lavender, rarely white, 18–20 mm, ligules 3–4 mm wide.</text>
      <biological_entity id="o11922" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="usually light pink" name="coloration" src="d0_s11" to="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11923" name="ligule" name_original="ligules" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 6–10 mm, weakly striate, glabrous;</text>
      <biological_entity id="o11924" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="weakly" name="coloration_or_pubescence_or_relief" src="d0_s12" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 6–9 mm. 2n = 18.</text>
      <biological_entity id="o11925" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11926" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>High Plains, rolling short-grass prairies, blufftop prairies, loess hills, sandy to silty soils, disturbed sites, railroads, roadsides, barren areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rolling short-grass prairies" />
        <character name="habitat" value="blufftop prairies" />
        <character name="habitat" value="loess hills" />
        <character name="habitat" value="silty soils" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="sandy to silty soils" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="barren areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Ariz., Ark., Colo., Idaho, Ind., Iowa, Kans., Minn., Mo., Mont., Nebr., Nev., N.Mex., N.Dak., Okla., S.Dak., Tex., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Rush skeletonplant</other_name>
  <discussion>Lygodesmia juncea is the most widespread species of the genus, occurring throughout the High Plains region of North America. It is easily distinguished by its bushy habit, greatly reduced cauline leaves, relatively small heads and involucres, and phyllaries lacking appendages. Mature cypselae are rarely found on this species, and the plants are presumably sterile and reproduce mainly by vegetative means. Many specimens have round galls to 10 mm diameter on the stems, produced by solitary wasps and apparently unique to this species.</discussion>
  
</bio:treatment>