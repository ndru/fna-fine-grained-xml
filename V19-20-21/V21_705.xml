<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">holocarpha</taxon_name>
    <taxon_name authority="(Greene) D. D. Keck" date="1958" rank="species">heermannii</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>4: 112. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus holocarpha;species heermannii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066962</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">heermannii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>9: 15. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species heermannii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–120 cm;</text>
      <biological_entity id="o7945" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems notably stipitate-glandular.</text>
      <biological_entity id="o7946" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="notably" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads borne singly (at ends of branches) or in paniculiform or racemiform arrays.</text>
      <biological_entity id="o7947" name="head" name_original="heads" src="d0_s2" type="structure">
        <character constraint="in arrays" constraintid="o7948" is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="in paniculiform or racemiform arrays" />
      </biological_entity>
      <biological_entity id="o7948" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres campanulate to ± globose.</text>
      <biological_entity id="o7949" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s3" to="more or less globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries each bearing 25–50 gland-tipped processes and minutely sessile or stipitate-glandular and puberulent or hispidulous.</text>
      <biological_entity id="o7950" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o7951" name="process" name_original="processes" src="d0_s4" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s4" to="50" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o7950" id="r560" name="bearing" negation="false" src="d0_s4" to="o7951" />
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 3–13.</text>
      <biological_entity id="o7952" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 9–22;</text>
      <biological_entity id="o7953" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow to brownish.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 12.</text>
      <biological_entity id="o7954" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s7" to="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7955" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Holocarpha heermannii occurs mostly in the eastern San Francisco Bay area and in the southern Sierra Nevada foothills and Tehachapi Range; there are fewer records from the San Joaquin Valley, central Sierra Nevada foothills, South Coast Ranges, and western Transverse Ranges. Sometimes, H. heermannii occurs with or near H. obconica or H. virgata. Crossing studies have indicated lack of crossability or intersterility of morphologically similar populations of H. heermannii; artificial hybrids of H. heermannii with some populations of H. virgata were marginally fertile (J. Clausen 1951).</discussion>
  
</bio:treatment>