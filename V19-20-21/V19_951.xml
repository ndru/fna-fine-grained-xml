<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavón" date="1794" rank="genus">soliva</taxon_name>
    <taxon_name authority="(Jussieu) Sweet" date="1826" rank="species">anthemifolia</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Brit.,</publication_title>
      <place_in_publication>243. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus soliva;species anthemifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200024555</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnostyles</taxon_name>
    <taxon_name authority="Jussieu" date="unknown" rank="species">anthemifolia</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>4: 262, plate 61, fig. 1. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnostyles;species anthemifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Soliva</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">mutisii</taxon_name>
    <taxon_hierarchy>genus Soliva;species mutisii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 3–15 (–30+) cm (high or across), ± villous, glabrescent (sometimes stoloniferous, ± mat-forming).</text>
      <biological_entity id="o16935" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o16936" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16937" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± obovate to spatulate, 3–8 (–15) cm, 2–3-pinnati-palmately lobed.</text>
      <biological_entity id="o16938" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less obovate" name="shape" src="d0_s2" to="spatulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="2-3-pinnati-palmately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads mostly clustered in leaf-axils (at ground level), rarely scattered along stems.</text>
      <biological_entity id="o16939" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="in leaf-axils" constraintid="o16940" is_modifier="false" modifier="mostly" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
        <character constraint="along stems" constraintid="o16941" is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s3" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o16940" name="leaf-axil" name_original="leaf-axils" src="d0_s3" type="structure" />
      <biological_entity id="o16941" name="stem" name_original="stems" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4–8+ mm diam.</text>
      <biological_entity id="o16942" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s4" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate florets (20–) 50–100+ in 1–8+ series.</text>
      <biological_entity id="o16943" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s5" to="50" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o16944" from="50" name="quantity" src="d0_s5" to="100" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16944" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 2–4+;</text>
      <biological_entity id="o16945" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas 1.5–2 mm.</text>
      <biological_entity id="o16946" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae: bodies oblanceolate to cuneate-oblong, 1.5–2+ mm, wings transversely rugulose or ribbed on proximal 2/3, shoulders not spinose laterally, faces distally villous to pilose, sometimes glabrescent;</text>
      <biological_entity id="o16947" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity id="o16948" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="cuneate-oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16949" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s8" value="rugulose" value_original="rugulose" />
        <character constraint="on proximal 2/3" constraintid="o16950" is_modifier="false" name="architecture_or_shape" src="d0_s8" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16950" name="2/3" name_original="2/3" src="d0_s8" type="structure" />
      <biological_entity id="o16951" name="shoulder" name_original="shoulders" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not; laterally" name="architecture_or_shape" src="d0_s8" value="spinose" value_original="spinose" />
      </biological_entity>
      <biological_entity id="o16952" name="face" name_original="faces" src="d0_s8" type="structure">
        <character char_type="range_value" from="distally villous" name="pubescence" src="d0_s8" to="pilose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 0 (persistent stylar sheaths indurate, spinelike, 1.5–3 mm, usually inflexed).</text>
      <biological_entity id="o16953" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 118 (Punjab).</text>
      <biological_entity id="o16954" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16955" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="118" value_original="118" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ark., Fla., La., Tex.; South America; introduced also in Mexico, Asia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Button burrweed</other_name>
  
</bio:treatment>