<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Nuttall) Ornduff" date="1966" rank="section">ptilomeris</taxon_name>
    <taxon_name authority="(A. Gray) M. C. Vasey" date="1985" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>32: 139. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section ptilomeris;species maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067054</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Burrielia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts.</publication_title>
      <place_in_publication>7: 358. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Burrielia;species maritima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">maritima</taxon_name>
    <taxon_hierarchy>genus Baeria;species maritima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(de Candolle) Ferris" date="unknown" rank="species">minor</taxon_name>
    <taxon_name authority="(A. Gray) Ferris" date="unknown" rank="subspecies">maritima</taxon_name>
    <taxon_hierarchy>genus Baeria;species minor;subspecies maritima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasthenia</taxon_name>
    <taxon_name authority="(de Candolle) Ornduff" date="unknown" rank="species">minor</taxon_name>
    <taxon_name authority="(A. Gray) Ornduff" date="unknown" rank="subspecies">maritima</taxon_name>
    <taxon_hierarchy>genus Lasthenia;species minor;subspecies maritima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 25 cm (herbage not sweetly scented).</text>
      <biological_entity id="o13082" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually prostrate or decumbent, rarely erect, branched proximally, glabrous or hairy at nodes and distally.</text>
      <biological_entity id="o13083" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="at nodes" constraintid="o13084" is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o13084" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear to oblanceolate, 10–90 × 2–12 mm, (fleshy) margins entire or lobed, faces glabrous.</text>
      <biological_entity id="o13085" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="90" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13086" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13087" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres hemispheric, 4–7 mm.</text>
      <biological_entity id="o13088" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 6–14, lanceolate to ovate, hairy (especially at margins and midribs).</text>
      <biological_entity id="o13089" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles conic, muricate, glabrous.</text>
      <biological_entity id="o13090" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 7–12;</text>
      <biological_entity id="o13091" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(corollas light to golden yellow) laminae oblong, 1–3 mm.</text>
      <biological_entity id="o13092" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages ± oblong, obtuse (style apices ± deltate, glabrous or with apical tufts of hairs and subapical fringes of shorter hairs).</text>
      <biological_entity constraint="anther" id="o13093" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae gray, linear to narrowly clavate, (2–) 2.5–3 mm, ± hairy;</text>
      <biological_entity id="o13094" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="gray" value_original="gray" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="narrowly clavate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi usually of 4–6 (–12) brown, lanceolate or subulate, aristate scales plus 4–5+ shorter, laciniate scales, rarely 0.2n = 8.</text>
      <biological_entity id="o13095" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="of 4-6(-12) brown" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o13096" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" upper_restricted="false" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o13097" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="laciniate" value_original="laciniate" />
        <character modifier="rarely" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13098" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seabird roosting sites, coastal headlands, offshore rocks, islands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seabird roosting sites" />
        <character name="habitat" value="coastal headlands" />
        <character name="habitat" value="offshore rocks" />
        <character name="habitat" value="islands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Maritime or seaside goldfields</other_name>
  <discussion>Lasthenia maritima is a self-pollinating, “guano endemic” of seabird nesting grounds. It is typically found on offshore islands and rocks from the Farallon Islands, California, to the northern tip of Vancouver Island, British Columbia and rarely occurs on the mainland.</discussion>
  
</bio:treatment>