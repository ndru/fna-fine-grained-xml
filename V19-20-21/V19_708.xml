<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">gnaphalium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">palustre</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 403. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus gnaphalium;species palustre</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066802</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Filaginella</taxon_name>
    <taxon_name authority="(Nuttall) Holub" date="unknown" rank="species">palustris</taxon_name>
    <taxon_hierarchy>genus Filaginella;species palustris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Klatt" date="unknown" rank="species">palustre</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">nanum</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;species palustre;variety nanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">heteroides</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;species heteroides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (1–) 3–15 (–30) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or fibrous-rooted.</text>
      <biological_entity id="o17856" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems commonly with decumbent branches produced from bases, densely or loosely and persistently woolly-tomentose.</text>
      <biological_entity id="o17857" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely; loosely; persistently" name="pubescence" src="d0_s2" value="woolly-tomentose" value_original="woolly-tomentose" />
      </biological_entity>
      <biological_entity id="o17858" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o17859" name="base" name_original="bases" src="d0_s2" type="structure" />
      <relation from="o17857" id="r1609" name="with" negation="false" src="d0_s2" to="o17858" />
      <relation from="o17858" id="r1610" name="produced from" negation="false" src="d0_s2" to="o17859" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades spatulate to oblanceolate-oblong, 1–3.5 cm × 3–8 (–10) mm.</text>
      <biological_entity id="o17860" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate-oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads oblanceolate to obovate, 4–12 × 1.5–4 mm, shorter than or surpassing glomerules.</text>
      <biological_entity id="o17861" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character constraint="than or surpassing glomerules" constraintid="o17863" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17862" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o17863" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s4" value="surpassing" value_original="surpassing" />
      </biological_entity>
      <relation from="o17861" id="r1611" name="subtending" negation="false" src="d0_s4" to="o17862" />
    </statement>
    <statement id="d0_s5">
      <text>Heads in capitate glomerules (at stem tips and in distalmost axils).</text>
      <biological_entity id="o17864" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o17865" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="capitate" value_original="capitate" />
      </biological_entity>
      <relation from="o17864" id="r1612" name="in" negation="false" src="d0_s5" to="o17865" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2.5–4 mm.</text>
      <biological_entity id="o17866" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries brownish, bases woolly, the inner narrowly oblong with white (opaque), blunt apices.</text>
      <biological_entity id="o17867" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o17868" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o17870" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s7" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 14.</text>
      <biological_entity constraint="inner" id="o17869" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="with apices" constraintid="o17870" is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17871" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arroyos, sandy streambeds, pond edges, potholes, other moist, open sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="sandy streambeds" />
        <character name="habitat" value="pond edges" />
        <character name="habitat" value="potholes" />
        <character name="habitat" value="other moist" />
        <character name="habitat" value="open sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Ariz., Calif., Colo., Idaho, Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Western marsh cudweed</other_name>
  
</bio:treatment>