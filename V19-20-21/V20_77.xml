<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1894" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. San Francisco,</publication_title>
      <place_in_publication>175. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species arborescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066508</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linosyris</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 79. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Linosyris;species arborescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall" date="unknown" rank="species">arborescens</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species arborescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–500 cm.</text>
      <biological_entity id="o9021" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="500" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, green when young, fastigiately branched, glabrous or sparsely hairy, gland-dotted, resinous.</text>
      <biological_entity id="o9022" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="fastigiately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly ascending to spreading, older deflexed;</text>
      <biological_entity id="o9023" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly ascending" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="deflexed" value_original="deflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades filiform (sulcate to concave), 25–90 × 0.5–3 mm, midnerves obscure to evident, apices acuminate to attenuate, often mucronate, faces glabrous or sparsely hairy, gland-dotted (in deep, circular pits), resinous;</text>
      <biological_entity id="o9024" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9025" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s3" to="evident" />
      </biological_entity>
      <biological_entity id="o9026" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="attenuate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o9027" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary leaf fascicles sometimes present.</text>
      <biological_entity constraint="axillary" id="o9028" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in rounded, cymiform arrays (to 10 cm wide).</text>
      <biological_entity id="o9029" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o9030" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o9029" id="r830" name="in" negation="false" src="d0_s5" to="o9030" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–15 mm (bracts 0–7, scalelike, resembling phyllaries).</text>
      <biological_entity id="o9031" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate to subcampanulate, 4–6 × 3.5–4.5 mm.</text>
      <biological_entity id="o9032" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s7" to="subcampanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–25 in 3–4 series, tan, narrowly triangular to lanceolate, 1.5–5 × 0.5–1 mm, unequal, mostly chartaceous, midnerves evident, raised, mostly uniform in width to slightly dilated apically, (margins membranous, fimbriate) apices erect, acute, abaxial faces resinous.</text>
      <biological_entity id="o9033" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9034" from="20" name="quantity" src="d0_s8" to="25" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o9034" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o9035" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="evident" value_original="evident" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="raised" value_original="raised" />
        <character is_modifier="false" modifier="mostly" name="width" src="d0_s8" value="uniform" value_original="uniform" />
        <character is_modifier="false" modifier="slightly; apically" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o9036" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9037" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o9038" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 10–25;</text>
      <biological_entity id="o9039" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 4–5 mm.</text>
      <biological_entity id="o9040" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan to brown, narrowly turbinate, 2–3 mm (5-ribbed), hairy;</text>
      <biological_entity id="o9041" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tan, 3.5–4.5 mm. 2n = 18.</text>
      <biological_entity id="o9042" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9043" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry foothill slopes, in chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry foothill slopes" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Goldenfleece</other_name>
  
</bio:treatment>