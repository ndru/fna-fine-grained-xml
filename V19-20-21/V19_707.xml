<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="treatment_page">428</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GNAPHALIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 850. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 368. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus GNAPHALIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek gnaphalion, a downy plant, the name anciently applied to these or similar plants</other_info_on_name>
    <other_info_on_name type="fna_id">113778</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Opiz" date="unknown" rank="genus">Filaginella</taxon_name>
    <taxon_hierarchy>genus Filaginella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals [biennials or perennials], (1–) 3–30 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually taprooted, sometimes fibrous-rooted.</text>
      <biological_entity id="o10274" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect (often with decumbent-ascending branches from bases; ± woolly-tomentose, not glandular).</text>
      <biological_entity id="o10275" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o10277" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s5">
      <text>± sessile;</text>
      <biological_entity id="o10276" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades oblanceolate to spatulate or linear, bases ± cuneate, margins entire, faces concolor, gray and tomentose.</text>
      <biological_entity id="o10278" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="spatulate or linear" />
      </biological_entity>
      <biological_entity id="o10279" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10280" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10281" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="concolor" value_original="concolor" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads disciform, usually in ± capitate clusters (in axils of leaves or bracts), sometimes in spiciform glomerules.</text>
      <biological_entity id="o10282" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o10283" name="glomerule" name_original="glomerules" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o10282" id="r956" modifier="in more or less capitate clusters; sometimes" name="in" negation="false" src="d0_s7" to="o10283" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly to broadly campanulate, 2.5–4 mm.</text>
      <biological_entity id="o10284" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–5 series, usually white or tawny to brown (opaque or hyaline, often shiny; stereomes usually glandular distally), ± equal to unequal, chartaceous toward tips (inner phyllaries narrowly oblong, usually white-tipped and protruding distal to outer).</text>
      <biological_entity id="o10285" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="tawny" name="coloration" notes="" src="d0_s9" to="brown" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character constraint="toward tips" constraintid="o10287" is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o10286" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <biological_entity id="o10287" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <relation from="o10285" id="r957" name="in" negation="false" src="d0_s9" to="o10286" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat, smooth, epaleate.</text>
      <biological_entity id="o10288" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Peripheral (pistillate) florets 40–80 (more numerous than bisexual);</text>
      <biological_entity constraint="peripheral" id="o10289" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s11" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas purplish or whitish.</text>
      <biological_entity id="o10290" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Inner (bisexual) florets 4–7;</text>
      <biological_entity constraint="inner" id="o10291" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas purplish or whitish.</text>
      <biological_entity id="o10292" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae oblong, faces usually glabrous, sometimes minutely papillate (hairs ± papilliform, not myxogenic);</text>
      <biological_entity id="o10293" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o10294" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes minutely" name="relief" src="d0_s15" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi readily falling, of 8–12 distinct, barbellate bristles in 1 series.</text>
      <biological_entity id="o10296" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s16" to="12" />
        <character is_modifier="true" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o10297" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o10295" id="r958" name="consist_of" negation="false" src="d0_s16" to="o10296" />
      <relation from="o10296" id="r959" name="in" negation="false" src="d0_s16" to="o10297" />
    </statement>
    <statement id="d0_s17">
      <text>x = 7.</text>
      <biological_entity id="o10295" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o10298" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>92.</number>
  <discussion>Species ca. 38 (3 in the flora).</discussion>
  <discussion>Generic segregations have reduced Gnaphalium from hundreds of species to ca. 38. North American species (north of Mexico) not included here have been segregated to Euchiton, Gamochaeta, Omalotheca, and Pseudognaphalium. Species of Gnaphalium in the strict sense (adopted here) are usually ca. 3–30 cm, loosely tomentose and not glandular, and have loosely glomerulate heads, involucres 2–3(–4) mm diam., white-tipped inner phyllaries, papillate cypselae, and readily falling pappi of distinct bristles, features especially contrasting with Pseudognaphalium, to which most North American species have been transferred. Because of their relatively small stature and tendency to produce loosely spiciform arrays of heads, gnaphaliums sometimes are identified as gamochaetas, which have different cypselar vestitures and different pappi. The lectotype species of Gnaphalium is G. uliginosum Linnaeus; discussion of this choice rather than Pseudognaphalium (Gnaphalium) luteoalbum (Linnaeus) Hilliard &amp; Burtt is given in C. Jeffrey (1979), O. M. Hilliard and B. L. Burtt (1981), and J. McNeill et al. (1987).</discussion>
  <discussion>Gnaphalium polycaulon Persoon is included in the key because it probably will be found in warmer coastal localities in the United States (perhaps Florida or California). It is a cosmopolitan weed (Old World native) and occurs in Mexico. It has sometimes been identified by the misapplied name Gnaphalium indicum Linnaeus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads in relatively elongate, interrupted, spiciform glomerules, not subtended by foliaceous bracts; leaf blades oblanceolate, mostly 2–4 cm; tips of inner phyllaries brownish</description>
      <determination>Gnaphalium polycaulon</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads in terminal glomerules, subtended by foliaceous bracts; leaf blades spatulate to oblanceolate-oblong or linear to narrowly oblanceolate, mostly 0.5–2.5 cm; tips of inner phyllaries white</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades spatulate to oblanceolate-oblong, 3–8(–10) mm wide; bracts subtending heads oblanceolate to obovate, longest 4–12 × 1.5–4 mm, shorter than or equaling to slightly surpassing glomerules; inner phyllaries narrowly oblong, apices blunt</description>
      <determination>1 Gnaphalium palustre</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades linear to narrowly oblanceolate, 0.5–3 mm wide; bracts subtending heads linear, oblanceolate, or obovate, 5–25 × 0.5–2 mm, surpassing glomerules; inner phyllaries narrowly triangular, apices acute</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades linear, the largest 0.4–5 cm; bracts subtending heads linear, 10–25 × 0.5–1 mm; heads in spiciform arrays of spikelike, axillary glomerules</description>
      <determination>2 Gnaphalium exilifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades oblanceolate, the largest 1–5 cm; bracts subtending heads linear, oblanceolate, or obovate, 5–15 × 1–2 mm; heads in terminal, capitate glomerules, some- times in axillary glomerules</description>
      <determination>3 Gnaphalium uliginosum</determination>
    </key_statement>
  </key>
</bio:treatment>