<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">630</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">tetradymia</taxon_name>
    <taxon_name authority="(S. F. Blake) Strother" date="1974" rank="species">tetrameres</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>26: 192. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus tetradymia;species tetrameres</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067725</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tetradymia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">comosa</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="subspecies">tetrameres</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>35: 176. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tetradymia;species comosa;subspecies tetrameres;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–200 cm.</text>
      <biological_entity id="o5164" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, erect (wandlike), unarmed, evenly pannose.</text>
      <biological_entity id="o5165" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s1" value="pannose" value_original="pannose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: primaries linear-filiform, flaccid, 10–40 mm;</text>
      <biological_entity id="o5166" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5167" name="primary" name_original="primaries" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-filiform" value_original="linear-filiform" />
        <character is_modifier="false" name="texture" src="d0_s2" value="flaccid" value_original="flaccid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>secondaries filiform-spatulate, 10–20 mm, sparsely tomentose.</text>
      <biological_entity id="o5168" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5169" name="secondary" name_original="secondaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform-spatulate" value_original="filiform-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 4–6.</text>
      <biological_entity id="o5170" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–3 cm.</text>
      <biological_entity id="o5171" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate, 8–9 mm.</text>
      <biological_entity id="o5172" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 4–5, oval-elliptic.</text>
      <biological_entity id="o5173" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="oval-elliptic" value_original="oval-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 4–5;</text>
      <biological_entity id="o5174" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas pale-yellow, ca. 8 mm.</text>
      <biological_entity id="o5175" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 5–6 mm, densely pilose (hairs 4–7 mm);</text>
      <biological_entity id="o5176" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of ca. 20, coarse bristles or subulate scales 3–5 mm. 2n = 60.</text>
      <biological_entity id="o5177" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o5178" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="20" value_original="20" />
        <character is_modifier="true" name="relief" src="d0_s11" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o5179" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5180" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
      </biological_entity>
      <relation from="o5177" id="r467" name="consist_of" negation="false" src="d0_s11" to="o5178" />
      <relation from="o5177" id="r468" name="consist_of" negation="false" src="d0_s11" to="o5179" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep sands or dunes, sagebrush scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep sands" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="sagebrush scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  
</bio:treatment>