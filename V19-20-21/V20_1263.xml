<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">560</other_info_on_meta>
    <other_info_on_meta type="treatment_page">565</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Richardson in J. Franklin et al." date="1823" rank="species">eremophilus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin et al., Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>759. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species eremophilus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067483</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (20–) 40–80 (–140) cm (caudices branched, fibrous-rooted).</text>
      <biological_entity id="o29671" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="140" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage glabrous or glabrate.</text>
      <biological_entity id="o29672" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems single or loosely clustered.</text>
      <biological_entity id="o29673" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ± evenly distributed (proximal often withering before flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o29674" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ovate or lanceolate to narrowly lanceolate, (3–) 6–12 (–20) × (1–) 1.5–5 (–7) cm, bases tapered, margins usually pinnate to lacerate, sometimes dentate.</text>
      <biological_entity id="o29675" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29676" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o29677" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually pinnate" name="shape" src="d0_s5" to="lacerate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 10–60+ in compound corymbiform arrays.</text>
      <biological_entity id="o29678" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o29679" from="10" name="quantity" src="d0_s6" to="60" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29679" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 3–5+ (prominent or inconspicuous) bractlets (lengths to 3/4 phyllaries).</text>
      <biological_entity id="o29680" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o29681" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
      </biological_entity>
      <relation from="o29680" id="r2741" name="consist_of" negation="false" src="d0_s7" to="o29681" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries (± 8) ± 13, 3–8+ mm, tips green or black.</text>
      <biological_entity id="o29682" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s8" value="8" value_original="8" />
        <character modifier="more or less" name="quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29683" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets ± 8;</text>
      <biological_entity id="o29684" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 5–10 mm.</text>
      <biological_entity constraint="corolla" id="o29685" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae usually glabrous, sometimes hirtellous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 38, 40, 44.</text>
      <biological_entity id="o29686" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29687" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="38" value_original="38" />
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask.; Alaska, Ariz., Colo., Mont., N.Dak., N.Mex., S.Dak., Utah, Wyo.; Mostly n, w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mostly n" establishment_means="native" />
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43.</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Varieties of Senecio eremophilus are distinguished by head size and distribution. Varieties eremophilus and macdougalii are notably different; var. kingii is intermediate in both morphology and distribution.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyculi: bractlets prominent (lengths of at least some to about 3/4 phyllaries); involucres 7–10 mm diam.; phyllaries 6–8 mm, tips green or weakly, if at all, black</description>
      <determination>43a Senecio eremophilus var. eremophilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyculi: bractlets conspicuous or inconspicuous (lengths rarely to 1/2 phyllaries); involucres 5–8 mm diam.; phyllaries 3–6 mm, tips usually black</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyculi: bractlets conspicuous (lengths to 1/2 phyllaries); involucres (5–)6–8 diam.; phyllaries ± 13, (4–)5–6 mm</description>
      <determination>43b Senecio eremophilus var. kingii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyculi: bractlets inconspicuous; involucres 4–5(–6) mm diam.; phyllaries ± 8 or ± 13, 3–5 mm</description>
      <determination>43c Senecio eremophilus var. macdougalii</determination>
    </key_statement>
  </key>
</bio:treatment>