<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">argutum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 447. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species argutum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066936</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">argutum</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="variety">parishii</taxon_name>
    <taxon_hierarchy>genus Hieracium;species argutum;variety parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–60 cm.</text>
      <biological_entity id="o349" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally usually glabrous, sometimes piloso-hirsute (hairs 1–5+ mm) and stellate-pubescent, distally glabrous or stellate-pubescent.</text>
      <biological_entity id="o350" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 0–7 (–12+), cauline (1–) 3–8+;</text>
      <biological_entity id="o351" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o352" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="12" upper_restricted="false" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o353" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate or spatulate to lance-linear, 60–120 (–200) × 8–15 (–45+) mm, lengths 5–10 (–15+) times widths, bases cuneate, margins of some or all sinuately toothed, apices acute, faces piloso-hirsute (hairs 1–5+ mm) and stellate-pubescent.</text>
      <biological_entity id="o354" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o355" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="lance-linear" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="200" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="45" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="5-10(-15+)" value_original="5-10(-15+)" />
      </biological_entity>
      <biological_entity id="o356" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o357" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o358" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sinuately" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o359" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <relation from="o357" id="r35" name="part_of" negation="false" src="d0_s3" to="o358" />
    </statement>
    <statement id="d0_s4">
      <text>Heads 12–25 (–40+) in paniculiform arrays.</text>
      <biological_entity id="o360" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="40" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o361" from="12" name="quantity" src="d0_s4" to="25" />
      </biological_entity>
      <biological_entity id="o361" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles usually stellate-pubescent, sometimes stipitate-glandular as well.</text>
      <biological_entity id="o362" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="sometimes; well" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 8–12+.</text>
      <biological_entity id="o363" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o364" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± campanulate to obconic, 7–9+ (× 5–6) mm.</text>
      <biological_entity id="o365" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s7" to="obconic" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="9+[" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="6]" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 13–21+, apices acute, abaxial faces usually stellate-pubescent and stipitate-glandular, sometimes piloso-hirsute as well (hairs 0.5–1+ mm), rarely glabrous.</text>
      <biological_entity id="o366" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s8" to="21" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o367" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o368" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="well; rarely" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 15–30+;</text>
      <biological_entity id="o369" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 8–10 mm.</text>
      <biological_entity id="o370" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (black) columnar, 2.5–3 mm;</text>
      <biological_entity id="o371" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 40–60+, white bristles in ± 2 series, 3.5–5 mm.</text>
      <biological_entity id="o372" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o373" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character modifier="more or less" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o374" name="series" name_original="series" src="d0_s12" type="structure" />
      <relation from="o372" id="r36" name="consist_of" negation="false" src="d0_s12" to="o373" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine and pine-oak forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine and pine-oak forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1000(–3000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  
</bio:treatment>