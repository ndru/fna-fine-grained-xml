<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">eucephalus</taxon_name>
    <taxon_name authority="Bradshaw" date="1921" rank="species">vialis</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>20: 122. 1921</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eucephalus;species vialis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066727</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Bradshaw) S. F. Blake" date="unknown" rank="species">vialis</taxon_name>
    <taxon_hierarchy>genus Aster;species vialis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 60–120 cm (caudices stout).</text>
      <biological_entity id="o513" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, pilose to glandular-pubescent.</text>
      <biological_entity id="o514" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s1" to="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: middle and distal cauline blades lanceolate-elliptic, 5–9 cm × 15–30 mm, abaxial faces usually glabrous, sometimes sparsely pubescent, adaxial faces glandular-pubescent.</text>
      <biological_entity id="o515" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="middle and distal cauline" id="o516" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o517" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o518" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 5–50 (–120) in racemiform to paniculiform arrays.</text>
      <biological_entity id="o519" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="120" />
        <character char_type="range_value" constraint="in arrays" constraintid="o520" from="5" name="quantity" src="d0_s3" to="50" />
      </biological_entity>
      <biological_entity id="o520" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles stipitate-glandular.</text>
      <biological_entity id="o521" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 8–10 mm.</text>
      <biological_entity id="o522" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 3–6 series (sometimes reddish at margins and apices), linear to linear-oblong (strongly unequal), apices acute to acuminate, abaxial faces stipitate-glandular.</text>
      <biological_entity id="o523" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s6" to="linear-oblong" />
      </biological_entity>
      <biological_entity id="o524" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o525" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o526" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o523" id="r47" name="in" negation="false" src="d0_s6" to="o524" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 0.</text>
      <biological_entity id="o527" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae pilose;</text>
      <biological_entity id="o528" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus bristles in 2 series, smooth or ± barbellate.</text>
      <biological_entity constraint="pappus" id="o529" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o530" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o529" id="r48" name="in" negation="false" src="d0_s9" to="o530" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open oak or coniferous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry open oak or coniferous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Wayside aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eucephalus vialis is only known from Lane and Douglas counties. It is considered threatened. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>