<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">540</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MATRICARIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 890. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 380. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus MATRICARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek matrix, womb, and - aria, pertaining to; alluding to reputed medicinal properties</other_info_on_name>
    <other_info_on_name type="fna_id">119860</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Gray" date="unknown" rank="genus">Chamomilla</taxon_name>
    <taxon_hierarchy>genus Chamomilla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (1–) 10–25 (–80) cm (taprooted; often aromatic).</text>
      <biological_entity id="o8029" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, usually erect or ascending, sometimes decumbent, branched or not, glabrous or glabrate to sparsely hairy (hairs basifixed).</text>
      <biological_entity id="o8030" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character name="architecture" src="d0_s1" value="not" value_original="not" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="sparsely hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (not marcescent) basal (soon withering) and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>subpetiolate or sessile;</text>
      <biological_entity id="o8031" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades spatulate to oblong or ovate (bases sheathing or clasping, often pinnately auriculate), (1–) 2–3-pinnately lobed (lobes linear, often curved distally), ultimate margins entire (± recurved), mucronate, faces glabrous or glabrate to sparsely hairy (hairs basifixed).</text>
      <biological_entity id="o8032" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="oblong or ovate" />
        <character is_modifier="false" modifier="(1-)2-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o8033" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o8034" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="sparsely hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly or in open, corymbiform arrays.</text>
      <biological_entity id="o8035" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in open , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o8036" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o8035" id="r745" name="in" negation="false" src="d0_s6" to="o8036" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres patelliform, 4–14 mm diam.</text>
      <biological_entity id="o8037" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="patelliform" value_original="patelliform" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 25–50 in [2–] 3–4 [–5] series, distinct, oblong or ovate to spatulate or linear-spatulate (membranous, not carinate, bases not indurate), subequal, margins and apices (hyaline) scarious (apices rounded to obtuse).</text>
      <biological_entity id="o8038" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o8039" from="25" name="quantity" src="d0_s8" to="50" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="spatulate or linear-spatulate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o8039" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o8040" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8041" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic to oblong-ovoid [subulate] (hollow), epaleate.</text>
      <biological_entity id="o8042" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="oblong-ovoid" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or 10–22, pistillate, fertile;</text>
      <biological_entity id="o8043" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="22" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, laminae elliptic-ovate.</text>
      <biological_entity id="o8044" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8045" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-ovate" value_original="elliptic-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 120–750+, bisexual, fertile;</text>
      <biological_entity id="o8046" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="120" name="quantity" src="d0_s12" to="750" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas (persisting in fruit, often slightly asymmetric, sometimes with scattered, sessile, golden glands) yellow to greenish yellow or yellowish green, tubes (± dilated), throats urceolate to campanulate, lobes 4–5 (spreading), deltate [with resin sacs].</text>
      <biological_entity id="o8047" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="greenish yellow or yellowish green" />
      </biological_entity>
      <biological_entity id="o8048" name="tube" name_original="tubes" src="d0_s13" type="structure" />
      <biological_entity id="o8049" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="urceolate" name="shape" src="d0_s13" to="campanulate" />
      </biological_entity>
      <biological_entity id="o8050" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obconic, slightly compressed (usually asymmetric, apices oblique), ribs [3–] 5, faces glabrous, smooth between ribs (pericarps sometimes with myxogenic cells abaxially and/or in ribs; embryo-sac development monosporic);</text>
      <biological_entity id="o8051" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o8052" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s14" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o8053" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="between ribs" constraintid="o8054" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8054" name="rib" name_original="ribs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, coroniform, or (sometimes on ray cypselae) adaxial auricles.</text>
      <biological_entity constraint="adaxial" id="o8056" name="auricle" name_original="auricles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o8055" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o8057" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, North Africa, some species widespread weeds in the southern hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="North Africa" establishment_means="native" />
        <character name="distribution" value="some species widespread weeds in the southern hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>124.</number>
  <other_name type="common_name">Mayweed</other_name>
  <other_name type="common_name">chamomile</other_name>
  <other_name type="common_name">matricary</other_name>
  <other_name type="common_name">matricaire</other_name>
  <other_name type="common_name">chamomille</other_name>
  <discussion>Species 7 (3 in the flora).</discussion>
  <discussion>Matricaria has been confused with Tripleurospermum (see discussion under the latter). Typification of Matricaria was discussed by K. Bremer and C. J. Humphries (1993), who rejected the arguments of S. Rauschert (1974) in favor of the use of Chamomilla over Matricaria.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads radiate (ray corollas white); disc corollas yellow to greenish yellow, lobes 5; cypselae 0.75–0.9 mm; pappi usually 0, sometimes coroniform (entire or lobed) or (on ray cypselae) each a toothed auricle</description>
      <determination>3 Matricaria chamomilla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads discoid; disc corollas greenish yellow, lobes 4(–5); cypselae 1–1.5 mm (ribs, at least 2 lateral, each with longitudinal mucilage gland); pappi coroniform, entire or lobed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads (1–)4–50(–300); discs 4–7(–11) × 4–7.5(–10) mm; cypselae: 2 lateral ribs each with mucilage gland along ± entire length; pappi coroniform, entire; plants aromatic (pineapple odor when bruised); stems 1–10+, usually erect or ascending, sometimes decumbent, branched from bases</description>
      <determination>1 Matricaria discoidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads 1–15+; discs 5–12.5 × 6–14 mm; cypselae: 2 lateral ribs each with mucilage gland in distal 1/2 (glands expanding into lobes); pappi coroniform, lobed (lobes 2, abaxio-lateral); plants not notably aromatic; stems usually erect, sometimes ascending, simple or branched mostly distally, sometimes proximally (then ascending)</description>
      <determination>2 Matricaria occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>