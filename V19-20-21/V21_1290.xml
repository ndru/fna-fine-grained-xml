<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="treatment_page">506</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="(Kunth) A. Gray" date="1852" rank="species">veronicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 85. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species veronicifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066273</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">veronicifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 88, plate 341. 1818;  4(qto.): 112. 1820 (as veronicaefolium)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species veronicifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">veronicifolia</taxon_name>
    <taxon_name authority="(B. L. Robinson) B. L. Robinson" date="unknown" rank="variety">petrophila</taxon_name>
    <taxon_hierarchy>genus Brickellia;species veronicifolia;variety petrophila;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 50–90 cm.</text>
      <biological_entity id="o24383" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched from bases, densely pubescent, glanddotted.</text>
      <biological_entity id="o24384" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from bases" constraintid="o24385" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24385" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly opposite (sometimes alternate);</text>
      <biological_entity id="o24386" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 2–5 mm;</text>
      <biological_entity id="o24387" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-nerved from bases, ovate to reniform, 4–15 × 7–25 mm, bases cordate to subcordate, margins crenate, apices obtuse, faces pubescent, glanddotted.</text>
      <biological_entity id="o24388" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o24389" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24389" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o24390" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o24391" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o24392" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24393" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o24394" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o24395" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o24394" id="r1665" name="in" negation="false" src="d0_s5" to="o24395" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0–2 mm, densely pubescent, glanddotted.</text>
      <biological_entity id="o24396" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to broadly campanulate, 9–11 mm.</text>
      <biological_entity id="o24397" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="broadly campanulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 35–40 in 7–9 series, greenish, often purple-tinged, 3–6-striate, unequal, margins scarious (apices obtuse to acute);</text>
      <biological_entity id="o24398" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o24399" from="35" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="3-6-striate" value_original="3-6-striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o24399" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity id="o24400" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer broadly ovate to lanceovate (puberulent; margins ciliate), inner lanceolate (glabrous).</text>
      <biological_entity constraint="outer" id="o24401" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s9" to="lanceovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o24402" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 18–33;</text>
      <biological_entity id="o24403" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s10" to="33" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas cream to pale-yellow, often red or purple-tinged, 6–7.5 mm.</text>
      <biological_entity id="o24404" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s11" to="pale-yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 3–4 mm, sparsely hispidulous;</text>
      <biological_entity id="o24405" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 24–28 white or tawny, barbellate bristles.</text>
      <biological_entity id="o24407" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="24" is_modifier="true" name="quantity" src="d0_s13" to="28" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o24406" id="r1666" name="consist_of" negation="false" src="d0_s13" to="o24407" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o24406" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o24408" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist areas, canyons, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="past_name">veronicaefolia</other_name>
  <discussion>Brickellia veronicifolia occurs in the Chisos Mountains.</discussion>
  
</bio:treatment>