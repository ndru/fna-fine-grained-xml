<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Cassini" date="1818" rank="genus">MANTISALCA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1818: 142. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus MANTISALCA</taxon_hierarchy>
    <other_info_on_name type="etymology">Anagram of specific epithet salmantica</other_info_on_name>
    <other_info_on_name type="fna_id">119642</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, 50–100 cm, herbage not spiny.</text>
      <biological_entity id="o3837" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o3839" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s0" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o3840" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (basal) or sessile (cauline);</text>
      <biological_entity id="o3841" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins ± lobed (proximal) or dentate or lobed (distal), sparsely hirsute (basal and proximal cauline) or glabrous (distal cauline).</text>
      <biological_entity constraint="blade" id="o3842" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiant, borne singly.</text>
      <biological_entity id="o3843" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="radiant" value_original="radiant" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ovoid to spheric, 10–15 mm diam.</text>
      <biological_entity id="o3844" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="spheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries many in 6–8 series, unequal, appressed, ovate, margins entire, apices obtuse to acute, narrowly membranous fringed, each with a short deciduous spine.</text>
      <biological_entity id="o3845" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="in series" constraintid="o3846" is_modifier="false" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o3846" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o3847" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3848" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acute" />
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o3849" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o3848" id="r382" name="with" negation="false" src="d0_s7" to="o3849" />
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat, epaleate, long-bristly.</text>
      <biological_entity id="o3850" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="long-bristly" value_original="long-bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets many;</text>
      <biological_entity id="o3851" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s9" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer neuter, corollas expanded and ± raylike, ± bilateral, staminodes present;</text>
      <biological_entity constraint="outer" id="o3852" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
      <biological_entity id="o3853" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s10" value="raylike" value_original="raylike" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s10" value="bilateral" value_original="bilateral" />
      </biological_entity>
      <biological_entity id="o3854" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner fertile, corollas purple (rarely white), radial, tubes very slender, throats narrowly funnelform, lobes linear;</text>
      <biological_entity constraint="inner" id="o3855" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o3856" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="radial" value_original="radial" />
      </biological_entity>
      <biological_entity id="o3857" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s11" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o3858" name="throat" name_original="throats" src="d0_s11" type="structure" />
      <biological_entity id="o3859" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther bases tailed, apical appendages oblong;</text>
      <biological_entity constraint="anther" id="o3860" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3861" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style-branches: fused portions with minutely hairy nodes, distinct portions minute.</text>
      <biological_entity id="o3862" name="style-branch" name_original="style-branches" src="d0_s13" type="structure" />
      <biological_entity id="o3863" name="portion" name_original="portions" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o3864" name="node" name_original="nodes" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3865" name="portion" name_original="portions" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s13" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o3863" id="r383" name="with" negation="false" src="d0_s13" to="o3864" />
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± barrel-shaped, ± compressed, with elaiosomes ribbed, transversely roughened, apices without prominent collars, smooth, faces glabrous, basal attachment scars oblique or lateral;</text>
      <biological_entity id="o3866" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="barrel--shaped" value_original="barrel--shaped" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="transversely" name="relief_or_texture" notes="" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o3867" name="elaiosome" name_original="elaiosomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o3868" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3869" name="collar" name_original="collars" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o3870" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o3871" name="scar" name_original="scars" src="d0_s14" type="structure" constraint_original="basal attachment">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s14" value="oblique" value_original="oblique" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
      <relation from="o3866" id="r384" name="with" negation="false" src="d0_s14" to="o3867" />
      <relation from="o3868" id="r385" name="without" negation="false" src="d0_s14" to="o3869" />
    </statement>
    <statement id="d0_s15">
      <text>pappi of several series, outer of stiff, scabrous bristles, inner a single abaxial scale.</text>
      <biological_entity id="o3872" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o3873" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3874" name="series" name_original="series" src="d0_s15" type="structure" />
      <biological_entity id="o3875" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s15" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3877" name="scale" name_original="scale" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="single" value_original="single" />
      </biological_entity>
      <relation from="o3872" id="r386" name="consist_of" negation="false" src="d0_s15" to="o3873" />
      <relation from="o3874" id="r387" name="consist_of" negation="false" src="d0_s15" to="o3875" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9, 10, 11.</text>
      <biological_entity constraint="inner" id="o3876" name="series" name_original="series" src="d0_s15" type="structure" />
      <biological_entity constraint="x" id="o3878" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
        <character name="quantity" src="d0_s16" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mediterranean region.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mediterranean region" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Species 4 (1 in the flora).</discussion>
  
</bio:treatment>