<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="mention_page">578</other_info_on_meta>
    <other_info_on_meta type="mention_page">589</other_info_on_meta>
    <other_info_on_meta type="treatment_page">596</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Nuttall) W. A. Weber &amp; Á. Löve" date="1981" rank="species">plattensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 48. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species plattensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416913</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">plattensis</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 413. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species plattensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Mackenzie &amp; Bush" date="unknown" rank="species">pseudotomentosus</taxon_name>
    <taxon_hierarchy>genus Senecio;species pseudotomentosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, 20–60+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous and/or fibrous-rooted (bases erect to suberect), sometimes stoloniferous (mostly eastern populations).</text>
      <biological_entity id="o7924" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–3, clustered, floccose-tomentose proximally and in leaf-axils, otherwise sparsely tomentose or glabrescent.</text>
      <biological_entity id="o7926" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="floccose-tomentose" value_original="floccose-tomentose" />
        <character is_modifier="false" modifier="otherwise sparsely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o7927" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure" />
      <relation from="o7926" id="r716" name="in" negation="false" src="d0_s2" to="o7927" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o7928" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly elliptic to elliptic-ovate or oblanceolate to suborbiculate or sublyrate, 20–70+ × 10–30+ mm, bases tapering to rounded or abruptly contracted, margins subentire to crenate, serrate-dentate, or pinnately lobed (abaxial faces floccose-tomentose, especially along midribs, ± glabrescent).</text>
      <biological_entity id="o7929" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="elliptic-ovate or oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7930" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="tapering" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o7931" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="crenate serrate-dentate or pinnately lobed" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="crenate serrate-dentate or pinnately lobed" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="crenate serrate-dentate or pinnately lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually reduced (petiolate, sublyrate or pinnatisect, abaxial faces sparsely hairy; distals sessile, subentire to irregularly dissected).</text>
      <biological_entity constraint="cauline" id="o7932" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 6–20+ in open or congested, corymbiform arrays.</text>
      <biological_entity id="o7933" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o7934" from="6" name="quantity" src="d0_s6" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7934" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles conspicuously bracteate, sparsely to densely tomentose.</text>
      <biological_entity id="o7935" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi inconspicuous.</text>
      <biological_entity id="o7936" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 13 or 21, green (tips sometimes cyanic), 5–6+ mm, densely tomentose proximally, glabrescent distally.</text>
      <biological_entity id="o7937" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="13" value_original="13" />
        <character name="quantity" src="d0_s9" unit="or" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s9" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–10;</text>
      <biological_entity id="o7938" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 9–10 mm.</text>
      <biological_entity constraint="corolla" id="o7939" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 60–70+;</text>
      <biological_entity id="o7940" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s12" to="70" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2.5–3.5 mm, limbs 3.5–4.5 mm.</text>
      <biological_entity id="o7941" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7942" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.5–2.5 mm, usually hirtellous, sometimes glabrous;</text>
      <biological_entity id="o7943" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 6.5–7.5 mm. 2n = 46, 92.</text>
      <biological_entity id="o7944" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7945" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
        <character name="quantity" src="d0_s15" value="92" value_original="92" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Apr–early Jun(–mid Jul, north).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="north" to="early Jun" from="mid Apr" constraint=" (-mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, meadows, open wooded areas, along highways, railroads, around mining and construction areas, usually on limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="wooded areas" modifier="open" constraint="along highways , railroads ," />
        <character name="habitat" value="highways" modifier="along" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="mining" modifier="around" />
        <character name="habitat" value="construction areas" />
        <character name="habitat" value="limestone" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Sask.; Ark., Colo., Ga., Ill., Ind., Iowa, Kans., La., Mich., Minn., Miss., Mo., Mont., Nebr., N.Mex., N.Dak., Ohio, Okla., Pa., S.Dak., Tenn., Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <other_name type="common_name">Prairie groundsel</other_name>
  <discussion>Packera plattensis is abundant, widespread, and almost weedy. Putative hybrids with other species are known. Plants in mesic, remnant prairies in the east are sometimes stoloniferous.</discussion>
  
</bio:treatment>