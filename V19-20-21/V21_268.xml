<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="B. L. Robinson &amp; Greenman" date="1899" rank="species">lindheimeri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>34: 541. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species lindheimeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067794</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–70+ cm (perennating bases ± erect, internodes not winged).</text>
      <biological_entity id="o7444" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly opposite (distal sometimes alternate);</text>
      <biological_entity id="o7445" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ovate-deltate or ovate to lanceovate, 2–6 × 1–5 cm, bases rounded to cuneate, margins toothed to entire, apices obtuse to acute, faces ± scabrellous.</text>
      <biological_entity id="o7446" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7447" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o7448" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s2" to="entire" />
      </biological_entity>
      <biological_entity id="o7449" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o7450" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly.</text>
      <biological_entity id="o7451" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± hemispheric, 15–22+ mm diam.</text>
      <biological_entity id="o7452" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s4" to="22" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 24–30+ in 3–4 series, ± erect, oblong, orbiculate, or ovate, 5–12+ mm.</text>
      <biological_entity id="o7453" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o7454" from="24" name="quantity" src="d0_s5" to="30" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7454" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets ± 13;</text>
      <biological_entity id="o7455" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 25 mm.</text>
      <biological_entity id="o7456" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="25" value_original="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 80–120+;</text>
      <biological_entity id="o7457" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s8" to="120" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o7458" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae dark-brown, narrowly elliptic, 7 mm, faces glabrous;</text>
      <biological_entity id="o7459" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o7460" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0–0.5 mm. 2n = 34.</text>
      <biological_entity id="o7461" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7462" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded slopes, limestone, juniper scrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded slopes" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="juniper scrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  
</bio:treatment>