<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">latisquamea</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 86. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety latisquamea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068281</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">graveolens</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">latisquamea</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 645. 1873 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species graveolens;variety latisquamea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">latisquameus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies latisquameus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 130–200 cm.</text>
      <biological_entity id="o5330" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="130" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems white, leafy, loosely tomentose.</text>
      <biological_entity id="o5331" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves whitish;</text>
      <biological_entity id="o5332" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, filiform, 15–45 × 1–2 mm, faces loosely tomentose.</text>
      <biological_entity id="o5333" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5334" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 7.5–10.5 mm.</text>
      <biological_entity id="o5335" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s4" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 15–17, apices erect, obtuse, outer abaxial faces densely tomentulose, inner glabrous.</text>
      <biological_entity id="o5336" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s5" to="17" />
      </biological_entity>
      <biological_entity id="o5337" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="outer abaxial" id="o5338" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="position" src="d0_s5" value="inner" value_original="inner" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7–9.5 mm, tubes glabrous or puberulent, lobes 0.6–1 mm, glabrous;</text>
      <biological_entity id="o5339" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5340" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5341" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages equaling or shorter than stigmatic portions.</text>
      <biological_entity constraint="style" id="o5342" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="false" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
        <character constraint="than stigmatic portions" constraintid="o5343" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o5343" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae densely hairy;</text>
      <biological_entity id="o5344" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 4.8–7.6 mm. 2n = 18.</text>
      <biological_entity id="o5345" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.8" from_unit="mm" name="some_measurement" src="d0_s9" to="7.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5346" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry streambeds and arroyos</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry streambeds" />
        <character name="habitat" value="arroyos" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21i.</number>
  <other_name type="common_name">Broadscale rabbitbrush</other_name>
  
</bio:treatment>