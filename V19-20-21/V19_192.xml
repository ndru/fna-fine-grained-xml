<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">174</other_info_on_meta>
    <other_info_on_meta type="illustration_page">174</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Cassini" date="1818" rank="genus">mantisalca</taxon_name>
    <taxon_name authority="(Linnaeus) Briquet &amp; Cavillier" date="1930" rank="species">salmantica</taxon_name>
    <place_of_publication>
      <publication_title>Biblioth. Universelle Rev. Suisse, pér.</publication_title>
      <place_in_publication>5, 5: 111. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus mantisalca;species salmantica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067161</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Centaurea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">salmantica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 918. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Centaurea;species salmantica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage cobwebby-tomentose proximally, glabrous distally.</text>
      <biological_entity id="o19759" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="cobwebby-tomentose" value_original="cobwebby-tomentose" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, leafless distally.</text>
      <biological_entity id="o19760" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="leafless" value_original="leafless" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades oblong, 10–25 cm, margins pinnately lobed;</text>
      <biological_entity id="o19761" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o19762" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19763" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline linear to lanceolate, smaller distally, dentate to pinnately dissected.</text>
      <biological_entity id="o19764" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o19765" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="dentate" name="shape" src="d0_s3" to="pinnately dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads long-pedunculate.</text>
      <biological_entity id="o19766" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-pedunculate" value_original="long-pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ovoid, distally narrowed.</text>
      <biological_entity id="o19767" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries greenish or stramineous, apically blackish, spine tips deciduous, spreading or reflexed, 1–3 mm.</text>
      <biological_entity id="o19768" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s6" value="blackish" value_original="blackish" />
      </biological_entity>
      <biological_entity constraint="spine" id="o19769" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas ± purple (rarely white).</text>
      <biological_entity id="o19770" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae dark-brown, 3–4 mm;</text>
      <biological_entity id="o19771" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus bristles brownish white or reddish, 2–3 mm. 2n = 18 (Italy), 20 (North Africa), 22 (Europe).</text>
      <biological_entity constraint="pappus" id="o19772" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish white" value_original="brownish white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19773" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
        <character name="quantity" src="d0_s9" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ariz., Calif.; Europe; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Dagger-flower</other_name>
  <discussion>Mantisalca salmantica is native to the Mediterranean region. It is considered an uncommon introduction into disturbed sites.</discussion>
  
</bio:treatment>