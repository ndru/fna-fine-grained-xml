<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">foliosus</taxon_name>
    <taxon_name authority="(T. J. Howell) Jepson" date="1925" rank="variety">confinis</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1056. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species foliosus;variety confinis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068336</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">confinis</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 35. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species confinis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (10–) 15–35 (–50) cm.</text>
      <biological_entity id="o16743" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves without prominent 1-directional orientation;</text>
      <biological_entity id="o16744" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blades 20–35 × 0.5–1 (–2) mm, apices acute to obtuse, faces glabrous or strigose to strigoso-hirsute.</text>
      <biological_entity id="o16745" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16746" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
      <biological_entity id="o16747" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s2" to="strigoso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 0.8–1 mm wide, margins narrowly to broadly scarious, midnerves usually not distinct and orange-resinous, abaxial faces glabrous, densely, prominently glandular;</text>
      <biological_entity id="o16748" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16749" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o16750" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coating" src="d0_s3" value="orange-resinous" value_original="orange-resinous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16751" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely; prominently" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>inner phyllaries 4–5 mm.</text>
      <biological_entity constraint="inner" id="o16752" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 19–30;</text>
      <biological_entity id="o16753" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s5" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas 9–12 mm.</text>
      <biological_entity id="o16754" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc corollas 3.5–5 mm.</text>
      <biological_entity constraint="disc" id="o16755" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky sites, chaparral to pine and fir woods, over a variety of soils, often on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="fir" />
        <character name="habitat" value="chaparral to pine" constraint="over a variety of soils" />
        <character name="habitat" value="fir woods" constraint="over a variety of soils" />
        <character name="habitat" value="a variety" constraint="of soils" />
        <character name="habitat" value="soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(50–)100–1800(–2200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>96a.</number>
  
</bio:treatment>