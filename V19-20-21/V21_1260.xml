<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="treatment_page">499</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="(Linnaeus) Shinners" date="1971" rank="species">eupatorioides</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Turner" date="1989" rank="variety">gracillima</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>67: 130. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species eupatorioides;variety gracillima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068109</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kuhnia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">eupatorioides</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">gracillima</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 218. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Kuhnia;species eupatorioides;variety gracillima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="(Scheele) Shinners" date="unknown" rank="species">leptophylla</taxon_name>
    <taxon_hierarchy>genus Brickellia;species leptophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">K.</taxon_name>
    <taxon_name authority="Scheele" date="unknown" rank="species">leptophylla</taxon_name>
    <taxon_hierarchy>genus K.;species leptophylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–90 cm.</text>
      <biological_entity id="o25670" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate;</text>
      <biological_entity id="o25671" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles 0–1 mm;</text>
      <biological_entity id="o25672" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved from bases, lance-linear to linear, 5–80 × 0.5–1 mm, margins entire.</text>
      <biological_entity id="o25673" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character constraint="from bases" constraintid="o25674" is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_shape_or_course" notes="" src="d0_s3" value="lance-linear to linear" value_original="lance-linear to linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25674" name="base" name_original="bases" src="d0_s3" type="structure" />
      <biological_entity id="o25675" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o25676" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o25677" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o25676" id="r1743" name="in" negation="false" src="d0_s4" to="o25677" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 2–20 mm.</text>
      <biological_entity id="o25678" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–10 mm.</text>
      <biological_entity id="o25679" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries green to stramineous.</text>
      <biological_entity id="o25680" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 15–25;</text>
      <biological_entity id="o25681" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas pale-yellow to pinkish lavender, 5–7 mm.</text>
      <biological_entity id="o25682" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s9" to="pinkish lavender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4–5.5 mm. 2n = 18.</text>
      <biological_entity id="o25683" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25684" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov, Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas, wide range of soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" />
        <character name="habitat" value="wide range" constraint="of soils" />
        <character name="habitat" value="soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13e.</number>
  
</bio:treatment>