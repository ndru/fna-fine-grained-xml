<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Linnaeus) Willdenow" date="1803" rank="species">scariosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">scariosa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species scariosa;variety scariosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068572</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="(Linnaeus) Hill" date="unknown" rank="species">scariosa</taxon_name>
    <taxon_name authority="Lunell" date="unknown" rank="variety">borealis</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species scariosa;variety borealis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Nuttall ex J. McNab" date="unknown" rank="species">scariosa</taxon_name>
    <taxon_name authority="(Lunell) Gaiser" date="unknown" rank="variety">virginiana</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species scariosa;variety virginiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">borealis</taxon_name>
    <taxon_hierarchy>genus Liatris;species borealis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scariosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">virginiana</taxon_name>
    <taxon_hierarchy>genus Liatris;species scariosa;variety virginiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–80 (–100) cm.</text>
      <biological_entity id="o14747" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with 8–20 (–25) leaves or leafy bracts proximal to heads.</text>
      <biological_entity id="o14748" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o14749" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="25" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s1" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character constraint="to heads" constraintid="o14751" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o14750" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="25" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s1" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character constraint="to heads" constraintid="o14751" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o14751" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o14748" id="r1017" name="with" negation="false" src="d0_s1" to="o14749" />
      <relation from="o14748" id="r1018" name="with" negation="false" src="d0_s1" to="o14750" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline elliptic to oblanceolate-spatulate, mostly 100–280 × 16–40 (–45) mm, glabrous (weakly, if at all, glanddotted).</text>
      <biological_entity id="o14752" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblanceolate-spatulate" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s2" to="280" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually 19–30.</text>
      <biological_entity id="o14753" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s3" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Florets 19–33.</text>
      <biological_entity id="o14754" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s4" to="33" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock ledges, shale banks and barrens, limestone and sandstone outcrops, road banks, flood plains, dry woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="shale banks" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="road banks" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="dry woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md., N.C., Pa., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35a.</number>
  <discussion>Variety scariosa is an Appalachian entity that might justifiably be treated as distinct from vars. novae-anglieae and nieuwlandii at species level, as done by Shinners. It has shorter stems and smaller heads than vars. nieuwlandii and novae-angliae, and the transition in shape and size from basal to cauline leaves is more abrupt.</discussion>
  
</bio:treatment>