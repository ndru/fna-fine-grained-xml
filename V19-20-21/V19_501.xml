<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">agoseris</taxon_name>
    <taxon_name authority="Greene" date="1899" rank="species">monticola</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 37. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus agoseris;species monticola</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066038</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agoseris</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">covillei</taxon_name>
    <taxon_hierarchy>genus Agoseris;species covillei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agoseris</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">decumbens</taxon_name>
    <taxon_hierarchy>genus Agoseris;species decumbens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agoseris</taxon_name>
    <taxon_name authority="(Pursh) Rafinesque" date="unknown" rank="species">glauca</taxon_name>
    <taxon_name authority="(Greene) Q. Jones" date="unknown" rank="variety">monticola</taxon_name>
    <taxon_hierarchy>genus Agoseris;species glauca;variety monticola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.</text>
      <biological_entity id="o13801" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly decumbent to prostrate;</text>
      <biological_entity id="o13802" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="mostly decumbent" name="growth_form_or_orientation" src="d0_s1" to="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles rarely purplish, margins not ciliate;</text>
      <biological_entity id="o13803" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o13804" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to spatulate, 2–10 (–14) cm, margins usually dentate to lobed or laciniately pinnatifid, rarely entire, lobes 2–3 pairs, linear to oblanceolate, proximal lobes often retrorse, distal often antrorse, lobules often present, faces mostly puberulent to villous, sometimes glabrous and glaucous.</text>
      <biological_entity id="o13805" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13806" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually dentate" name="shape" src="d0_s3" to="lobed or laciniately pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13807" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13808" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s3" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13809" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s3" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity id="o13810" name="lobule" name_original="lobules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13811" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="mostly puberulent" name="pubescence" src="d0_s3" to="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles not elongating after flowering, 2–25 cm in fruit, basally lanate, apically stipitate-glandular.</text>
      <biological_entity id="o13812" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character constraint="after flowering" is_modifier="false" modifier="not" name="length" src="d0_s4" value="elongating" value_original="elongating" />
        <character char_type="range_value" constraint="in fruit" constraintid="o13813" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="basally" name="pubescence" notes="" src="d0_s4" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o13813" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres obconic to campanulate, 1–2 cm in fruit.</text>
      <biological_entity id="o13814" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o13815" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13815" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2–4 (–6) series, usually rosy purple, rarely green, sometimes spotted, often with a purple-black midstripes, unequal, faces ± hairy, stipitate-glandular;</text>
      <biological_entity id="o13816" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s6" value="rosy purple" value_original="rosy purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="size" notes="" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o13817" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o13818" name="midstripe" name_original="midstripes" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="purple-black" value_original="purple-black" />
      </biological_entity>
      <biological_entity id="o13819" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o13816" id="r1267" name="in" negation="false" src="d0_s6" to="o13817" />
      <relation from="o13816" id="r1268" modifier="often" name="with" negation="false" src="d0_s6" to="o13818" />
    </statement>
    <statement id="d0_s7">
      <text>outer usually erect, sometimes spreading apically, adaxially glabrous;</text>
      <biological_entity constraint="outer" id="o13820" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes; apically" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner erect, not elongating after flowering.</text>
      <biological_entity constraint="inner" id="o13821" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="after flowering" is_modifier="false" modifier="not" name="length" src="d0_s8" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles epaleate, rarely paleate (outer florets only).</text>
      <biological_entity id="o13822" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 10–40;</text>
      <biological_entity id="o13823" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 4–10 mm, ligules 5–11 × 2–4 mm;</text>
      <biological_entity id="o13824" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13825" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13826" name="ligule" name_original="ligules" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3–5 mm.</text>
      <biological_entity id="o13827" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 6–10 mm;</text>
      <biological_entity id="o13828" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>bodies fusiform, 6–9 mm, beaks 1–3 mm, lengths to 1/2 times bodies;</text>
      <biological_entity id="o13829" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13830" name="beak" name_original="beaks" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character constraint="body" constraintid="o13831" is_modifier="false" name="length" src="d0_s14" value="0-1/2 times bodies" value_original="0-1/2 times bodies" />
      </biological_entity>
      <biological_entity id="o13831" name="body" name_original="bodies" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ribs ridged to flattened, straight;</text>
      <biological_entity id="o13832" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="ridged" name="shape" src="d0_s15" to="flattened" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappus bristles in 2 series, 8–11 mm. 2n = 18, 36.</text>
      <biological_entity constraint="pappus" id="o13833" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13834" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13835" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
      <relation from="o13833" id="r1269" name="in" negation="false" src="d0_s16" to="o13834" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic subalpine meadows and forests to alpine tundra and rocky slopes, volcanic or pyroclastic soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic subalpine meadows" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="to alpine tundra" />
        <character name="habitat" value="to alpine rocky slopes" />
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="pyroclastic soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Sierra Nevada agoseris</other_name>
  <discussion>Agoseris monticola occurs mainly in the Sierra Nevada and sporadically eastward in the Great Basin (Jarbridge and Ruby Mountains) and northward to the Cascade Range and Blue Mountains of Oregon. It appears to be allied with A. glauca and has been treated as a variety of the latter. Ecologically, it approaches A. glauca var. dasycephala; the two are morphologically and geographically separate from each other. Intermediates between A. monticola and A. aurantiaca, A. glauca, and A. parviflora are known.</discussion>
  
</bio:treatment>