<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="A. M. Powell" date="1983" rank="species">huecoensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>30: 219, fig. 2. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species huecoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067323</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 10–20 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely short-hairy.</text>
      <biological_entity id="o14465" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="short-hairy" value_original="short-hairy" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o14466" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="short-hairy" value_original="short-hairy" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 10–80 mm;</text>
      <biological_entity id="o14467" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14468" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="80" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades broadly ovate, ovate-deltate, or ovate-rhombic, 5–12 (–15) × 4–10 (–15) mm, margins entire or serrate-lobed.</text>
      <biological_entity id="o14469" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14470" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-deltate" value_original="ovate-deltate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-rhombic" value_original="ovate-rhombic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-deltate" value_original="ovate-deltate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-rhombic" value_original="ovate-rhombic" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14471" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="serrate-lobed" value_original="serrate-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in corymbiform arrays, 5.5–6 × 4–6 mm.</text>
      <biological_entity id="o14472" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in corymbiform arrays" value_original="in corymbiform arrays" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" notes="" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14473" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o14472" id="r994" name="in" negation="false" src="d0_s4" to="o14473" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–15 mm.</text>
      <biological_entity id="o14474" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres funnelform.</text>
      <biological_entity id="o14475" name="involucre" name_original="involucres" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 8–10, lanceolate to oblanceolate, 3–4.5 × 0.6–1.2 mm.</text>
      <biological_entity id="o14476" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 3–5;</text>
      <biological_entity id="o14477" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, laminae oblong, 2.5–3 × 1.5–2 mm.</text>
      <biological_entity id="o14478" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14479" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 11–13;</text>
      <biological_entity id="o14480" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s10" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 1.3–1.5 mm, throats broadly campanulate-funnelform, 0.8–1.2 mm, lobes 0.5–0.7 mm.</text>
      <biological_entity id="o14481" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14482" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14483" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14484" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae linear-lanceolate, 2.2–3.2 mm, margins usually thin-calloused, short-hairy;</text>
      <biological_entity id="o14485" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14486" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s12" value="thin-calloused" value_original="thin-calloused" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi usually of 1–3, antrorsely barbellate bristles 1–2.5 mm plus 0–4 shorter bristles, sometimes plus hyaline, laciniate scales.</text>
      <biological_entity id="o14487" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="false" modifier="sometimes" name="coloration" notes="" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o14488" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s13" to="3" />
        <character is_modifier="true" modifier="antrorsely" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o14489" name="bristle" name_original="bristles" src="d0_s13" type="structure" />
      <relation from="o14487" id="r995" name="consist_of" negation="false" src="d0_s13" to="o14488" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o14490" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14491" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <other_name type="common_name">Hueco rock daisy</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Perityle huecoensis is found in the United States only in the Hueco Mountains of El Paso County, generally growing on north or northeast facing slopes. Plants in the nearby Sierra Juarez in Mexico tend to lack bristles in pappi on the disc florets and to be taller with larger leaves compared to plants in the United States populations.</discussion>
  
</bio:treatment>