<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Donald J. Pinkava</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">83</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">BERLANDIERA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 517. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus BERLANDIERA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Jean Louis Berlandier, 1805–1851, Belgian explorer in North America</other_info_on_name>
    <other_info_on_name type="fna_id">103842</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 8–120 cm (stems often arising annually from taproots or woody caudices; herbage usually hirsute, hispid, scabrous, or velvety).</text>
      <biological_entity id="o17194" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o17195" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, usually branched (scapiform or ± leafy throughout).</text>
      <biological_entity id="o17196" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and/or cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o17197" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (± pinnately nerved) elongate-deltate, lanceolate, lyrate, oblanceolate, oblong, obovate, ovate, or spatulate, sometimes pinnately lobed to pinnatifid (usually each with deeper sinuses proximal), bases cordate or truncate to cuneate, ultimate margins usually crenate, dentate, or sinuate, faces hirsute, hispid, scabrous, or velvety.</text>
      <biological_entity id="o17198" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate-deltate" value_original="elongate-deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="sometimes pinnately lobed" name="shape" src="d0_s5" to="pinnatifid" />
      </biological_entity>
      <biological_entity id="o17199" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17200" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o17201" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="velvety" value_original="velvety" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="velvety" value_original="velvety" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in paniculiform to corymbiform arrays.</text>
      <biological_entity id="o17202" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o17203" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in paniculiform to corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o17203" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± campanulate to ± hemispheric or broader, 12–30 mm diam.</text>
    </statement>
    <statement id="d0_s8">
      <text>(broader in age).</text>
      <biological_entity id="o17204" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s7" to="more or less hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent (outer) or falling, usually 14–22 in 2–3+ series (inner broadly obovate to orbiculate, hairy, shed with cypselae).</text>
      <biological_entity id="o17205" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o17206" from="14" modifier="usually" name="quantity" src="d0_s9" to="22" />
      </biological_entity>
      <biological_entity id="o17206" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles ± turbinate (distally dilated, apices flattened to slightly depressed), paleate (paleae ± linear-conduplicate, each wrapped round a disc-floret, distally dilated, hirtellous).</text>
      <biological_entity id="o17207" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (2–) 8 (–13), pistillate, fertile;</text>
      <biological_entity id="o17208" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="13" />
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pale-yellow to orange-yellow (abaxially green or red to maroon or with 9–12, green or red to maroon, anastomosing, veins).</text>
      <biological_entity id="o17209" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s12" to="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 80–200+, functionally staminate;</text>
      <biological_entity id="o17210" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s13" to="200" upper_restricted="false" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow or red to maroon, tubes shorter than throats, lobes 5, ± deltate.</text>
      <biological_entity id="o17211" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s14" to="maroon" />
      </biological_entity>
      <biological_entity id="o17212" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than throats" constraintid="o17213" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17213" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o17214" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (black) obcompressed or obflattened, mostly obovate (thin edged, not truly winged, hairy, at least adaxially; each adhering to and shed with 2 adjacent paleae, 2 disc-florets, and a subtending phyllary);</text>
      <biological_entity id="o17215" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character name="shape" src="d0_s15" value="obflattened" value_original="obflattened" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0, or persistent, ± coroniform (inconspicuous ridges or minute crowns of aristate teeth).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 15.</text>
      <biological_entity id="o17216" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o17217" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>270.</number>
  <discussion>Species 8 (8, including 3 hybrids, in the flora).</discussion>
  <discussion>All Berlandiera species have 2n = 30 and all but perhaps B. monocephala (which was unavailable for study) are intercrossable, resulting in all possible artificial interspecific hybrid combinations. Some combinations are not found in nature, e.g., B. lyrata × B. subacaulis, because distributions of potential parentals do not overlap (D. J. Pinkava 1964, 1967).</discussion>
  <references>
    <reference>Nesom, G. L. and B. L. Turner. 1998. Variation in the Berlandiera pumila (Asteraceae) complex. Sida 18: 493–502.</reference>
    <reference>Pinkava, D. J. 1964. Biosystematic Study of Genus Berlandiera DC. (Compositae). Ph.D. dissertation. Ohio State University.</reference>
    <reference>Pinkava, D. J. 1967. Biosystematic study of Berlandiera (Compositae). Brittonia 19: 285–298.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades lyrate (pinnate to pinnatifid) or oblanceolate (seldom pinnate, ultimate margins mostly crenate), faces velvety; peduncles hairy (some hairs reddish, bulbous-based, wartlike); ray corollas: abaxial veins red to maroon; disc corollas usually red to maroon, rarely yellow</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades deltate, lanceolate, lyrate, oblong, oval, or ovate, sometimes pinnately lobed to pinnatifid, faces hirsute, hispid, ± scabrous, or velvety (leaves not both oblanceolate or lyrate-pinnatifid and velvety); peduncles hairy (velvety to scabrous, and with wartlike hairs in B. subacaulis); ray corollas: abaxial veins green; disc corollas yellow or red to maroon</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves usually lyrate-pinnatifid, rarely not lobed; peduncles hairy (relatively many red- dish wartlike hairs surpassing appressed, white hairs)</description>
      <determination>1 Berlandiera lyrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves mostly oblanceolate, usually some pinnatifid near bases; peduncles hairy (relatively few reddish wartlike hairs enmeshed in matted whitish hairs; plants intergradesbetween B. lyrata and B. monocephala)</description>
      <determination>2 Berlandiera ×macrophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves crowded near bases of stems; heads borne singly or 2–3 together; disc corollas yellow</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves crowded near bases of stems or ± evenly distributed on stems; heads borne singly or 2–20 in paniculiform to corymbiform arrays; disc corollas red to maroon</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades oblanceolate (usually not lobed, sometimes lobed at bases, ultimate margins crenate to remotely dentate), faces velvety; peduncles hairy (hairs whitish,matted, none wartlike); Arizona (Sonora)</description>
      <determination>3 Berlandiera monocephala</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades mostly oblong (usually sinuate-pinnatifid), faces ± scabrous; peduncles hairy (hispid and with wartlike hairs); Florida</description>
      <determination>4 Berlandiera subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves crowded near bases of stems (usually some leaves lobed basally, faces finely hirsute); peduncles hairy (hairs relatively long and fine mixed with shorter, stouter hairs; plants intergrade between B. pumila and B. subacaulis)</description>
      <determination>5 Berlandiera ×humilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves evenly distributed along stems (not lobed, faces sometimes finely hirsute); peduncles hairy (hairs not mixed long and short)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades usually ovate, faces velvety; peduncles hairy (hairs whitish, rela-tively long and fine, matted); South Carolina to Mississippi, e Texas</description>
      <determination>6 Berlandiera pumila</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades elongate-deltate, lanceolate, or ovate, faces finely ± hirsute to ± scabrous; peduncles hairy (hirsute or hairs grayish to reddish, erect or spreading, sometimes curling)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems lax (sometimes suffrutescent); leaf blades (at least mid-stem) petiolate, narrowly to broadly ovate (widths 1/2–1 1/4 times lengths), membranous, margins, usually crenate, faces finely to coarsely hirsute; peduncles hairy (hairs grayish to reddish, erect or spreading, sometimes curling); intergrades to B. pumila and B. texana</description>
      <determination>7 Berlandiera ×betonicifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems (stiff) erect; leaf blades (at least mid-stem) petiolate to sessile, elongate-deltate to lanceolate (widths to 2/3 lengths), chartaceous, margins serrate to dentate, faces hirsute to scabrous; peduncles hairy (densely hirsute)</description>
      <determination>8 Berlandiera texana</determination>
    </key_statement>
  </key>
</bio:treatment>