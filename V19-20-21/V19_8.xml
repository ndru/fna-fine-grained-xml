<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="D. Don" date="1830" rank="genus">acourtia</taxon_name>
    <taxon_name authority="(A. Gray) Reveal &amp; R. M. King" date="1973" rank="species">thurberi</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>27: 231. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus acourtia;species thurberi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066008</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Perezia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">thurberi</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Nov. Thurber.,</publication_title>
      <place_in_publication>324. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Perezia;species thurberi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–150 cm (stems sulcate to striate distally, densely glandular).</text>
      <biological_entity id="o1768" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline and/or basal;</text>
    </statement>
    <statement id="d0_s2">
      <text>sessile;</text>
      <biological_entity id="o1769" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate to ovate-elliptic, 1.5 (cauline) –18 (basal) cm, bases shortly sagittate or clasping, margins acerose-denticulate, faces densely glandular-puberulent.</text>
      <biological_entity id="o1770" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovate-elliptic" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1771" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s3" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o1772" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acerose-denticulate" value_original="acerose-denticulate" />
      </biological_entity>
      <biological_entity id="o1773" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in subcongested corymbiform arrays.</text>
      <biological_entity id="o1774" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o1775" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="subcongested" value_original="subcongested" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o1774" id="r171" name="in" negation="false" src="d0_s4" to="o1775" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres obconic to campanulate, 7–9 mm.</text>
      <biological_entity id="o1776" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2–3 series, oblong-oblanceolate, apices acuminate, abaxial faces densely glandular-hairy.</text>
      <biological_entity id="o1777" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
      </biological_entity>
      <biological_entity id="o1778" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o1779" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1780" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <relation from="o1777" id="r172" name="in" negation="false" src="d0_s6" to="o1778" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles alveolate, glandular.</text>
      <biological_entity id="o1781" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="alveolate" value_original="alveolate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 3–6;</text>
      <biological_entity id="o1782" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas lavender-pink (purple), 7–12 mm.</text>
      <biological_entity id="o1783" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="lavender-pink" value_original="lavender-pink" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae subcylindric to subfusiform, 3–7 mm, glandular;</text>
      <biological_entity id="o1784" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="subcylindric" name="shape" src="d0_s10" to="subfusiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi bright white, 8–9 mm (rigid).</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 54.</text>
      <biological_entity id="o1785" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="bright white" value_original="bright white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1786" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravel and caliche soils in warm Sonoran desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel" constraint="in warm sonoran desert scrub" />
        <character name="habitat" value="caliche soils" constraint="in warm sonoran desert scrub" />
        <character name="habitat" value="warm sonoran desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; N.Mex.; Mexico (Chihuahua, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Thurber’s desertpeony</other_name>
  
</bio:treatment>