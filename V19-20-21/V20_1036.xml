<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="treatment_page">449</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1887" rank="genus">hazardia</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1887" rank="species">detonsa</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 29. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus hazardia;species detonsa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066841</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corethrogyne</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">detonsa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>10: 41. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Corethrogyne;species detonsa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Greene) P. H. Raven" date="unknown" rank="species">detonsus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species detonsus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 60–250 cm.</text>
      <biological_entity id="o22416" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems lanate-tomentose.</text>
      <biological_entity id="o22417" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate-tomentose" value_original="lanate-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves subsessile or subpetiolate;</text>
      <biological_entity id="o22418" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subpetiolate" value_original="subpetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades obovate, 40–140 × 10–50 mm, subcoriaceous, bases not clasping, margins serrulate to subentire, abaxial faces densely lanate-tomentose, adaxial densely short-tomentose.</text>
      <biological_entity id="o22419" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s3" to="140" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o22420" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o22421" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrulate" name="shape" src="d0_s3" to="subentire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22422" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="lanate-tomentose" value_original="lanate-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22423" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="short-tomentose" value_original="short-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in thyrsiform to subcorymbiform heads.</text>
      <biological_entity id="o22424" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o22425" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="thyrsiform" value_original="thyrsiform" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="subcorymbiform" value_original="subcorymbiform" />
      </biological_entity>
      <relation from="o22424" id="r2068" name="in" negation="false" src="d0_s4" to="o22425" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate, 10–13 × 10–13 mm.</text>
      <biological_entity id="o22426" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries erect, oblong, apices acute, faces densely woolly.</text>
      <biological_entity id="o22427" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o22428" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o22429" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 6–14, fertile;</text>
      <biological_entity id="o22430" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="14" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas shorter than involucre, inconspicuous.</text>
      <biological_entity id="o22431" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character constraint="than involucre" constraintid="o22432" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o22432" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 30–40;</text>
      <biological_entity id="o22433" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 8–10 mm.</text>
      <biological_entity id="o22434" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 3–4 mm, canescent.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 10.</text>
      <biological_entity id="o22435" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22436" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky hillsides, canyon walls, often with Pinus, Quercus, Ceanothus, Rhus, Arctostaphylos</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="canyon walls" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Island bristleweed</other_name>
  <discussion>Hazardia detonsa is known from Anacapa, Santa Rosa, and Santa Cruz islands. It is little differentiated from H. cana and clearly its evolutionary sister. In both taxa, the ray and disc florets often change to red-purple with maturity.</discussion>
  
</bio:treatment>