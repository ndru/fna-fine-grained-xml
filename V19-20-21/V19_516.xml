<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kenton L. Chambers</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1886" rank="genus">NOTHOCALAÏS</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 54. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus NOTHOCALAÏS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek notho -, false, and Calaïs, a synonym of Microseris</other_info_on_name>
    <other_info_on_name type="fna_id">122416</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="D. Don" date="unknown" rank="genus">Microseris</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="section">Nothocalaïs</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 420. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Microseris;section Nothocalaïs;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–45 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, with caudices often multicipital (taproots thick, fleshy, with blackish periderm, lateral rootlets often borne in clusters on knoblike projections).</text>
      <biological_entity id="o6227" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o6228" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="multicipital" value_original="multicipital" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, erect, scapiform, naked (rarely with 1–3 bracteate nodes near base in N. troximoides and N. nigrescens), glabrous or white-villous, especially near heads.</text>
      <biological_entity id="o6229" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="naked" value_original="naked" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity id="o6230" name="head" name_original="heads" src="d0_s2" type="structure" />
      <relation from="o6229" id="r596" modifier="especially" name="near" negation="false" src="d0_s2" to="o6230" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (bases attenuate or not);</text>
      <biological_entity id="o6231" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear to oblanceolate, margins usually entire (often pinnately lobed in N. alpestris), sometimes undulate (usually white-ciliolate in N. cuspidata and N. troximoides; faces glabrous or lightly villous).</text>
      <biological_entity id="o6232" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o6233" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly (erect, liguliferous).</text>
      <biological_entity id="o6234" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not inflated distally, not bracteate.</text>
      <biological_entity id="o6235" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0.</text>
      <biological_entity id="o6236" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres broadly to narrowly ovoid, campanulate at flowering, 5–20 mm diam.</text>
      <biological_entity id="o6237" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character constraint="at flowering" is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 8–50 in 2–5 series, (often longitudinally striped or finely dotted with red or purple) lanceolate to ovate, equal or unequal (outer shorter), herbaceous (thin), midnerves inconspicuous, apices acute to acuminate, faces glabrous or white-villous.</text>
      <biological_entity id="o6238" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o6239" from="8" name="quantity" src="d0_s10" to="50" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="ovate" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o6239" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o6240" name="midnerve" name_original="midnerves" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o6241" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <biological_entity id="o6242" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat, pitted, glabrous, epaleate.</text>
      <biological_entity id="o6243" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 13–100;</text>
      <biological_entity id="o6244" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s12" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, often reddish abaxially (much surpassing phyllaries in flowering, tubes hairy).</text>
      <biological_entity id="o6245" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often; abaxially" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae brown or gray, narrowly columnar to fusiform, usually narrowed distally, not beaked, ribs ca. 10, glabrous or distally scabrous;</text>
      <biological_entity id="o6246" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray" value_original="gray" />
        <character char_type="range_value" from="narrowly columnar" name="shape" src="d0_s14" to="fusiform" />
        <character is_modifier="false" modifier="usually; distally" name="shape" src="d0_s14" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o6247" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi fragile, of 10–80, distinct, lustrous, white, ± equal, smooth to barbellulate bristles or aristate scales.</text>
      <biological_entity id="o6249" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
      <biological_entity id="o6250" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o6248" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s15" value="fragile" value_original="fragile" />
        <character is_modifier="false" modifier="of 10-80" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s15" value="equal" value_original="equal" />
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s15" to="barbellulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o6251" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>67.</number>
  <other_name type="common_name">False agoseris or dandelion</other_name>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>Because pappi of Nothocalaïs taxa vary from capillary bristles to aristate scales, the species were earlier assigned to either Agoseris or Microseris. The totality of morphologic evidence supports the unity of the genus, as well as its segregation from Agoseris and Microseris (K. L. Chambers 1955, 1957). According to recent phylogenetic studies based on chloroplast DNA (R. K. Jansen et al. 1991b; J. Whitton et al. 1995), Nothocalaïs is most closely related to Uropappus and Agoseris, the three genera together comprising a clade sister to Microseris.</discussion>
  <references>
    <reference>Chambers, K. L. 1955. A biosystematic study of the annual species of Microseris. Contr. Dudley Herb. 4: 207–312.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi of 30–50 barbellulate bristles; leaf margins usually coarsely toothed or pinnately lobed, sometimes entire; phyllaries glabrous, evenly and minutely purple-dotted; subalpine, Cascade Range, Klamath Mountains, and (rarely) Sierra Nevada</description>
      <determination>1 Nothocalaïs alpestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi of 10–30 aristate scales or of 40–80 intergradent, smooth to barbellulate bristles and ± subulate to setiform scales; leaf margins entire, sometimes undulate; phyllaries glabrous or villous, sometimes purple-dotted; Great Plains, n, mid Rocky Mountains, Columbia-Snake Rivers Plateau, n Great Basin</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi of 40–80 intergradent, smooth to barbellulate bristles and ± subulate to setiform scales; Great Plains</description>
      <determination>2 Nothocalaïs cuspidata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi of 10–30 aristate scales; n, mid Rocky Mountains, Columbia-Snake Rivers Plateau, n Great Basin</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves lanceolate to oblanceolate, margins plane, apices acute; phyllaries broadly lanceolate to ovate, apices acute to acuminate, faces usually glabrous, minutely purple-dotted; n, mid Rocky Mountains</description>
      <determination>3 Nothocalaïs nigrescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves mostly linear to linear-lanceolate, margins often undulate, apices acuminate; phyllaries lanceolate, apices acuminate, faces glabrous or villous especially on midnerves, margins often ciliolate, green or with purple-lined midnerves, sometimes also minutely purple- dotted. n Great Basin, Columbia-Snake Rivers Plateau</description>
      <determination>4 Nothocalaïs troximoides</determination>
    </key_statement>
  </key>
</bio:treatment>