<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Wiegand) G. L. Nesom" date="1995" rank="species">ontarionis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 287. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species ontarionis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067665</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="species">ontarionis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>30: 179. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species ontarionis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">missouriensis</taxon_name>
    <place_of_publication>
      <place_in_publication>1898</place_in_publication>
      <other_info_on_pub>not (Nuttall) Kuntze 1891</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species missouriensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–120 cm, colonial;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous.</text>
      <biological_entity id="o17642" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1– (2–3), ascending to erect (straight), proximally glabrate, distally uniformly villous or hirsute, or glabrous (var. glabratum).</text>
      <biological_entity id="o17643" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally uniformly" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally uniformly" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves thin, margins scabrous, apices mucronate, abaxial faces usually sparsely to densely strigose or strigillose, sometimes glabrous (var. glabrum), adaxial usually strigose or scabrous, sometimes glabrate or glabrous (var. glabrum);</text>
      <biological_entity id="o17644" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o17645" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o17646" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17647" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17648" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, petiolate to subpetiolate (petioles narrowly winged, ciliate, bases sheathing), blades spatulate to oblanceolate-obovate, 10–40 × 5–10 mm, bases attenuate, margins crenate-serrate, apices acute to rounded;</text>
      <biological_entity constraint="basal" id="o17649" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins, apices" constraintid="o17650, o17651, o17652, o17653" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o17650" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="petiolate" is_modifier="true" name="architecture" src="d0_s4" to="subpetiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate-obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o17651" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="petiolate" is_modifier="true" name="architecture" src="d0_s4" to="subpetiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate-obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o17652" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="petiolate" is_modifier="true" name="architecture" src="d0_s4" to="subpetiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate-obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o17653" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="petiolate" is_modifier="true" name="architecture" src="d0_s4" to="subpetiolate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate-obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximalmost cauline usually withering by flowering, petiolate or sessile (petioles narrowly winged, ± clasping), blades ovate or lanceovate to elliptic-lanceolate or oblanceolate, 20–80 (–12) × 5–35 mm, progressively reduced distally, bases attenuate to cuneate, margins serrate (sometimes coarsely) to crenate-serrate, apices acute to acuminate or short-caudate;</text>
      <biological_entity constraint="proximalmost cauline" id="o17654" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades" constraintid="o17655" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="average_length" notes="" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s5" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o17655" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s5" to="elliptic-lanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o17656" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o17657" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="serrate to crenate-serrate" value_original="serrate to crenate-serrate" />
      </biological_entity>
      <biological_entity id="o17658" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate or short-caudate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades elliptic-lanceolate to oblanceolate or lanceolate, (6–) 10–80+ × 2–25 mm, progressively reduced distally, bases cuneate, margins entire to serrulate, apices acute to acuminate.</text>
      <biological_entity constraint="distal" id="o17659" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17660" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s6" to="oblanceolate or lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="80" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o17661" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o17662" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s6" value="entire to serrulate" value_original="entire to serrulate" />
      </biological_entity>
      <biological_entity id="o17663" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in ample, open, paniculiform arrays, branches ± ascending or divaricate to long-arching, ± secund.</text>
      <biological_entity id="o17664" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o17665" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="ample" value_original="ample" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o17666" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="long-arching" value_original="long-arching" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s7" value="secund" value_original="secund" />
      </biological_entity>
      <relation from="o17664" id="r1639" name="in" negation="false" src="d0_s7" to="o17665" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles subsessile or 2–10 (–20) mm, ± pilose, bracts 1–5, linear-lanceolate, pilose, grading into phyllaries.</text>
      <biological_entity id="o17667" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
        <character name="architecture" src="d0_s8" value="2-10(-20) mm" value_original="2-10(-20) mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o17668" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o17669" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o17668" id="r1640" name="into" negation="false" src="d0_s8" to="o17669" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 3–5.5 mm.</text>
      <biological_entity id="o17670" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in (3–) 4–6 series, unequal, appressed or ± spreading, linear-obovate (outer) to oblong-lanceolate to linear (inner), bases indurate 1/3–3/4, margins narrowly scarious to apices, erose, hyaline, ciliate, green zones lanceolate, apices acute to acuminate, mucronulate, faces (outer) sparsely pilose or glabrous (var. glabratum), (inner) glabrous.</text>
      <biological_entity id="o17671" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="linear-obovate" name="shape" src="d0_s10" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o17672" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o17673" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s10" to="3/4" />
      </biological_entity>
      <biological_entity id="o17674" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character constraint="to apices" constraintid="o17675" is_modifier="false" modifier="narrowly" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" notes="" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o17675" name="apex" name_original="apices" src="d0_s10" type="structure" />
      <biological_entity id="o17676" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o17677" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o17678" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o17671" id="r1641" name="in" negation="false" src="d0_s10" to="o17672" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (10–) 15–26;</text>
      <biological_entity id="o17679" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s11" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas usually white, rarely pinkish or light purple to blue, laminae 3.5–5.5 (–8) × 0.5–1.5 mm.</text>
      <biological_entity id="o17680" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="light purple" name="coloration" src="d0_s12" to="blue" />
      </biological_entity>
      <biological_entity id="o17681" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 12–25;</text>
      <biological_entity id="o17682" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas cream or light yellow turning magenta or purple (ampliate), 2.5–4 (–4.5) mm, tubes shorter than funnelform throats, lobes slightly spreading to reflexed, lanceolate, 0.7–1.1 mm.</text>
      <biological_entity id="o17683" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light yellow turning magenta or purple" />
        <character name="coloration" src="d0_s14" value="," value_original="," />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17684" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o17685" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17685" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o17686" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="slightly spreading" name="orientation" src="d0_s14" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae gray or tan, oblong-obovoid, sometimes ± compressed, 1.2–1.8 (–2) mm, 3–5-nerved, faces strigillose;</text>
      <biological_entity id="o17687" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-obovoid" value_original="oblong-obovoid" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5-nerved" value_original="3-5-nerved" />
      </biological_entity>
      <biological_entity id="o17688" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi whitish to white, 3–3.5 mm.</text>
      <biological_entity id="o17689" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s16" to="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ill., Ind., Iowa, Kans., Ky., La., Mich., Minn., Miss., Mo., N.C., N.Y., Nebr., Okla., Pa., S.Dak., Tenn., Tex., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49.</number>
  <other_name type="past_name">ontarione</other_name>
  <other_name type="common_name">Ontario or bottomland aster</other_name>
  <other_name type="common_name">aster du lac Ontario</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Symphyotrichum ontarionis is often confused with S. lateriflorum, from which it can be distinguished by its shorter disc corolla lobes and abaxial leaf faces either moderately to densely hairy or glabrous to glabrate (var. glabratum), and without hairs along midveins (as is typical of S. lateriflorum).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Abaxial leaf faces moderately to densely hairy</description>
      <determination>49a Symphyotrichum ontarionis var. ontarionis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Abaxial leaf faces glabrous or glabrate</description>
      <determination>49b Symphyotrichum ontarionis var. glabratum</determination>
    </key_statement>
  </key>
</bio:treatment>