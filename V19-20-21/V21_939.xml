<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">368</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arnica</taxon_name>
    <taxon_name authority="Howell" date="1900" rank="species">cernua</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>373. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus arnica;species cernua</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066108</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arnica</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">chandleri</taxon_name>
    <taxon_hierarchy>genus Arnica;species chandleri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm.</text>
      <biological_entity id="o25101" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (often reddish purple) mostly simple.</text>
      <biological_entity id="o25102" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–4 pairs, mostly cauline (basal often persistent on sterile rosettes);</text>
      <biological_entity id="o25103" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (petioles usually narrow, proximalmost sometimes broadly winged);</text>
      <biological_entity id="o25104" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (often reddish purple) usually elliptic to ovate, sometimes subcordate, 1.5–8 × 1.5–4 cm (often relatively thick, nearly succulent), margins usually entire or serrate, sometimes crenate or slightly lobed, apices acute to rounded, faces glabrous or scabrous.</text>
      <biological_entity id="o25105" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually elliptic" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25106" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o25107" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o25108" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually 1, sometimes 2–3 (often nodding in bud).</text>
      <biological_entity id="o25109" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="2" modifier="sometimes" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate-turbinate.</text>
      <biological_entity id="o25110" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate-turbinate" value_original="campanulate-turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 8–14, ovate to broadly lanceolate.</text>
      <biological_entity id="o25111" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="14" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 5–10;</text>
      <biological_entity id="o25112" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow to slightly orange.</text>
      <biological_entity id="o25113" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="slightly orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets: corollas yellow;</text>
      <biological_entity id="o25114" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure" />
      <biological_entity id="o25115" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow.</text>
      <biological_entity id="o25116" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure" />
      <biological_entity id="o25117" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae dark gray, 6–8 mm, sparsely to densely hirsute (hairs duplex);</text>
      <biological_entity id="o25118" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark gray" value_original="dark gray" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi white, bristles usually barbellate, sometimes subplumose.</text>
      <biological_entity id="o25119" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38.</text>
      <biological_entity id="o25120" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="subplumose" value_original="subplumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25121" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine soils, conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soils" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="past_name">cernuua</other_name>
  <other_name type="common_name">Serpentine arnica</other_name>
  
</bio:treatment>