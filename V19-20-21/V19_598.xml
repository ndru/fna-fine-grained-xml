<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1829" rank="genus">lygodesmia</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1843" rank="species">grandiflora</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; Goodrich) S. L. Welsh" date="1993" rank="variety">entrada</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>95: 399. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lygodesmia;species grandiflora;variety entrada</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068580</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lygodesmia</taxon_name>
    <taxon_name authority="S. L. Welsh &amp; Goodrich" date="unknown" rank="species">entrada</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>40: 83, fig. 4. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lygodesmia;species entrada;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–45 cm.</text>
      <biological_entity id="o2822" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, stiff, woody, branched from bases and distally.</text>
      <biological_entity id="o2823" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character constraint="from bases" constraintid="o2824" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o2824" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades linear-acicular, 5–30 × 2–4 mm, stiff, spreading;</text>
      <biological_entity id="o2825" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o2826" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-acicular" value_original="linear-acicular" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline reduced distally, not scalelike.</text>
      <biological_entity id="o2827" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o2828" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–10, in loose, corymbiform arrays.</text>
      <biological_entity id="o2829" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
      <biological_entity id="o2830" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o2829" id="r278" name="in" negation="false" src="d0_s4" to="o2830" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindric (V-shaped in fruit), 20–22 × 5–7 mm, apices spreading.</text>
      <biological_entity id="o2831" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2832" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries ca. 6.</text>
      <biological_entity id="o2833" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5, corollas white (or fading to pinkish).</text>
      <biological_entity id="o2834" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o2835" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae seldom formed (ovaries 4–5 mm, abaxial faces smooth, adaxial weakly rugose, bisulcate with ridge separating sulci).</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 27.</text>
      <biological_entity id="o2836" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o2837" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="27" value_original="27" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Juniper-scrub community, in deep sandy soil, Entrada Sandstone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="juniper-scrub community" constraint="in deep sandy soil , entrada sandstone" />
        <character name="habitat" value="deep sandy soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety entrada is distinguished by its relatively large size, stiff stems (woody) and leaves, tomentulose phyllaries, and white corollas. It is similar to var. arizonica. Unlike other varieties, it has involucre apices somewhat spreading. Chromosome counts indicate that these plants are asexual triploids (A. S. Tomb, pers. comm.). Variety entrada is locally abundant in Arches National Park, near Moab.</discussion>
  
</bio:treatment>