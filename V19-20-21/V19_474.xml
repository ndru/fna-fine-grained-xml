<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="(A. Gray ex D. C. Eaton) A. Gray in A. Gray et al." date="1884" rank="species">glabrata</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 422. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species glabrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067149</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="A. Gray ex D. C. Eaton" date="unknown" rank="variety">glabrata</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>201. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malacothrix;species californica;variety glabrata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (5–) 10–40+ cm.</text>
      <biological_entity id="o13616" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–) 3–5+, ascending to erect, usually branched proximally and distally, glabrous or sparsely arachno-puberulent near bases (sometimes glaucous).</text>
      <biological_entity id="o13617" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually; proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="near bases" constraintid="o13618" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="arachno-puberulent" value_original="arachno-puberulent" />
      </biological_entity>
      <biological_entity id="o13618" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal usually pinnately lobed (lobes 3–6+ pairs, usually filiform, subequal to unequal, apices acute), ultimate margins entire, faces glabrous or ± hairy (then usually glabrescent);</text>
      <biological_entity constraint="cauline" id="o13619" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o13620" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13621" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13622" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal reduced (usually pinnately lobed).</text>
      <biological_entity constraint="cauline" id="o13623" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o13624" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 12–20+, lanceolate to linear bractlets, hyaline margins 0.05–0.2 mm wide (abaxial faces often ± densely white-hairy).</text>
      <biological_entity id="o13625" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o13626" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s4" to="20" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s4" to="linear" />
      </biological_entity>
      <biological_entity id="o13627" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o13625" id="r1255" name="consist_of" negation="false" src="d0_s4" to="o13626" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate to hemispheric, 9–17 × 4–7 mm.</text>
      <biological_entity id="o13628" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s5" to="hemispheric" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 20–25+ in 2–3 series, lance-linear to linear, hyaline margins 0.05–0.3 mm wide, faces usually glabrous, abaxial sometimes ± white-hairy.</text>
      <biological_entity id="o13629" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o13630" from="20" name="quantity" src="d0_s6" to="25" upper_restricted="false" />
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" notes="" src="d0_s6" to="linear" />
      </biological_entity>
      <biological_entity id="o13630" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o13631" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13632" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13633" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s6" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles bristly.</text>
      <biological_entity id="o13634" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 31–139;</text>
      <biological_entity id="o13635" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="31" name="quantity" src="d0_s8" to="139" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually pale-yellow, sometimes white, 15–23+ mm;</text>
      <biological_entity id="o13636" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="23" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 9–15+ mm.</text>
      <biological_entity constraint="outer" id="o13637" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± cylindro-fusiform (sometimes weakly 5-angled), 2–3.3 mm, ribs extending to apices, usually ± equal;</text>
      <biological_entity id="o13638" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13639" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o13640" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <relation from="o13639" id="r1256" name="extending to" negation="false" src="d0_s11" to="o13640" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi of 0–12+, blunt to acute teeth plus 1–2 (–5) bristles.</text>
      <biological_entity id="o13641" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o13642" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s12" to="12" upper_restricted="false" />
        <character char_type="range_value" from="blunt" is_modifier="true" name="shape" src="d0_s12" to="acute" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <biological_entity id="o13643" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <relation from="o13641" id="r1257" name="consist_of" negation="false" src="d0_s12" to="o13642" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o13644" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13645" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coarse soils in open areas, or among shrubs, creosote bush scrublands, Amsinckia, Artemisia, and Atriplex-Larrea associations, Joshua tree woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coarse soils" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="shrubs" />
        <character name="habitat" value="bush scrublands" modifier="creosote" />
        <character name="habitat" value="atriplex-larrea associations" />
        <character name="habitat" value="joshua tree woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Idaho, Nev., N.Mex., Oreg., Utah; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Smooth desertdandelion</other_name>
  <discussion>Malacothrix glabrata grows in the Mojave, Great Basin, and Sonoran deserts in California and the Intermountain region in Arizona, Nevada, Oregon, and Utah.</discussion>
  
</bio:treatment>