<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">498</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="(Linnaeus) Shinners" date="1971" rank="species">eupatorioides</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">eupatorioides</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species eupatorioides;variety eupatorioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kuhnia</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="species">glutinosa</taxon_name>
    <taxon_hierarchy>genus Kuhnia;species glutinosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–150 cm.</text>
      <biological_entity id="o9105" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite;</text>
      <biological_entity id="o9106" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles 1–10 mm;</text>
      <biological_entity id="o9107" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved or 3-nerved from bases, broadly rhombic-lanceolate to narrowly lanceolate, 25–100 × 5–40 mm, margins entire or ± dentate.</text>
      <biological_entity id="o9108" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character constraint="from bases" constraintid="o9109" is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="broadly rhombic-lanceolate" name="shape" notes="" src="d0_s3" to="narrowly lanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9109" name="base" name_original="bases" src="d0_s3" type="structure" />
      <biological_entity id="o9110" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o9111" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o9112" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o9111" id="r631" name="in" negation="false" src="d0_s4" to="o9112" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–20 mm.</text>
      <biological_entity id="o9113" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 7–11 mm.</text>
      <biological_entity id="o9114" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries green to stramineous, sometimes purple-tinged (lengths of outer to 1/2 inner, apices acute to acuminate, not contorted).</text>
      <biological_entity id="o9115" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="stramineous" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 6–15;</text>
      <biological_entity id="o9116" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas pale-yellow to maroon, 4.5–6 mm.</text>
      <biological_entity id="o9117" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s9" to="maroon" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4–5.5 mm. 2n = 18.</text>
      <biological_entity id="o9118" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9119" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas, wide range of soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" />
        <character name="habitat" value="wide range" constraint="of soils" />
        <character name="habitat" value="soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ind., Ky., La., Md., Miss., N.J., N.C., Ohio, Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13a.</number>
  
</bio:treatment>