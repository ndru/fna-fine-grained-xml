<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
    <other_info_on_meta type="illustration_page">398</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="(Harvey &amp; A. Gray) A. Gray" date="1874" rank="section">acarphaea</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">carphoclinia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">carphoclinia</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section acarphaea;species carphoclinia;variety carphoclinia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250068135</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaenactis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">carphoclinia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">attenuata</taxon_name>
    <taxon_hierarchy>genus Chaenactis;species carphoclinia;variety attenuata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (5–) 10–30 (–40) cm.</text>
      <biological_entity id="o12120" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal (± withering) and cauline, longest 1–6 (–7) cm;</text>
      <biological_entity id="o12121" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles scarcely dilated proximally, ± herbaceous.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 16.</text>
      <biological_entity id="o12122" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="scarcely; proximally" name="shape" src="d0_s2" value="dilated" value_original="dilated" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12123" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky or gravelly (sometimes sandy) desert slopes and flats, shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" modifier="open" />
        <character name="habitat" value="gravelly ( sandy ) desert slopes" modifier="sometimes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-90–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="90" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Utah; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <discussion>Variety carphoclinia is one of the most abundant spring wildflowers in the Sonoran Desert; it extends to the southern Great Basin and northwestern Chihuahuan Desert. It is reported to be eaten by desert tortoises (Gopherus agassizii Cooper). Some stunted forms have been named var. attenuata; such forms recur in the most arid (or otherwise severe) habitats throughout the range of the species.</discussion>
  
</bio:treatment>