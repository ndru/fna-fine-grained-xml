<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="treatment_page">102</other_info_on_meta>
    <other_info_on_meta type="illustration_page">97</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">wyethia</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">mollis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 544. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus wyethia;species mollis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067823</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–40 (–100) cm.</text>
      <biological_entity id="o12975" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blades lanceolate to oblong-ovate (whitish to gray or green), 20–30 (–40) cm, margins entire, not ciliate, faces sparsely to densely tomentose to tomentulose (usually glanddotted as well), glabrescent (nearly bald in age);</text>
      <biological_entity constraint="basal" id="o12976" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12977" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="oblong-ovate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12978" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12979" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="tomentose" modifier="densely" name="pubescence" src="d0_s1" to="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline similar, smaller distally.</text>
      <biological_entity constraint="basal" id="o12980" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o12981" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 2–3 in racemiform to corymbiform arrays or borne singly.</text>
      <biological_entity id="o12982" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="racemiform" name="architecture" src="d0_s3" to="corymbiform" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres campanulate to hemispheric, 20–25 (–30) mm diam.</text>
      <biological_entity id="o12983" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s4" to="hemispheric" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 12–22, subequal to unequal, herbaceous, margins not ciliate, faces tomentose to tomentulose;</text>
      <biological_entity id="o12984" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s5" to="22" />
        <character char_type="range_value" from="subequal" name="size" src="d0_s5" to="unequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o12985" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12986" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer 20–40 mm (equaling or surpassing inner).</text>
      <biological_entity constraint="outer" id="o12987" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 6–15;</text>
      <biological_entity id="o12988" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae 15–45 mm.</text>
      <biological_entity id="o12989" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 9–10 mm, distally strigillose.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 38.</text>
      <biological_entity id="o12990" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12991" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, dry to wet, open sites, openings in conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="openings" constraint="in conifer" />
        <character name="habitat" value="conifer" />
        <character name="habitat" value="dry to wet" />
        <character name="habitat" value="openings in conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2200(–3000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="900" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Wyethia mollis grows in the Sierra Nevada and the Cascade Range.</discussion>
  
</bio:treatment>