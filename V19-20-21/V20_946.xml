<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Bogler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">417</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="treatment_page">413</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">PYRROCOMA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 306, plate 107. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus PYRROCOMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pyrrhos, reddish or tawny, and kome, hair of the head, alluding to reddish pappi in some species</other_info_on_name>
    <other_info_on_name type="fna_id">127795</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Cassini" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Hooker) H. M. Hall" date="unknown" rank="section">Pyrrocoma</taxon_name>
    <taxon_hierarchy>genus Haplopappus;section Pyrrocoma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–50 (–90) cm, sericeous, tomentose, or glabrous, sometimes sessile or stipitate-glandular (taproots woody, caudices often short, branched).</text>
      <biological_entity id="o2622" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending or erect (leafy or subscapiform, often red-tinged), mostly simple.</text>
      <biological_entity id="o2623" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending or erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
      <biological_entity id="o2624" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal usually short-petiolate, cauline sessile;</text>
      <biological_entity constraint="basal" id="o2625" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o2626" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal blades 1-nerved, oblanceolate to elliptic or nearly linear (3–250 (–450) × 3–30 mm, bases usually attenuate), margins entire or spinulose-dentate or serrate or shallowly laciniate;</text>
      <biological_entity constraint="basal" id="o2627" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character constraint="to bases, margins" constraintid="o2628, o2629" is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o2628" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="nearly" name="arrangement" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="[3" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="250" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinulose-dentate" value_original="spinulose-dentate" />
        <character name="architecture_or_shape" src="d0_s5" value="serrate or shallowly laciniate" value_original="serrate or shallowly laciniate" />
      </biological_entity>
      <biological_entity id="o2629" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="nearly" name="arrangement" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="[3" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="250" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinulose-dentate" value_original="spinulose-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline often lanceolate, reduced distally (bases sometimes clasping).</text>
      <biological_entity constraint="cauline" id="o2630" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads usually radiate or disciform, rarely discoid, borne singly or 2–5 (–15) in racemiform, spiciform, or loose, corymbiform arrays (subtended by leafy bracts in P. carthamoides and P. radiata).</text>
      <biological_entity id="o2631" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s7" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="disciform" value_original="disciform" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s7" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="15" />
        <character char_type="range_value" constraint="in arrays" constraintid="o2632" from="2" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o2632" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric to narrowly campanulate, 5–15 (–30) × 5–60 mm.</text>
      <biological_entity id="o2633" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s8" to="narrowly campanulate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 10–40 in 2–6 series, appressed to loosely spreading, 1-nerved, oblanceolate to oblong or linear, equal or unequal, usually herbaceous and yellow-green or with distinct, green apical patch, sometimes proximal 2/3 white-indurate (apices occasionally squarrose, obtuse or acute, occasionally mucronate), faces glabrous or densely villous to tomentose.</text>
      <biological_entity id="o2634" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2635" from="10" name="quantity" src="d0_s9" to="40" />
        <character char_type="range_value" from="appressed" name="orientation" notes="" src="d0_s9" to="loosely spreading" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="oblong or linear" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_texture" src="d0_s9" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="with distinct , green apical patch" />
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s9" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="texture" src="d0_s9" value="white-indurate" value_original="white-indurate" />
      </biological_entity>
      <biological_entity id="o2635" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity constraint="apical" id="o2636" name="patch" name_original="patch" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o2637" name="face" name_original="faces" src="d0_s9" type="structure">
        <character char_type="range_value" from="densely villous" name="pubescence" src="d0_s9" to="tomentose" />
      </biological_entity>
      <relation from="o2634" id="r242" name="with" negation="false" src="d0_s9" to="o2636" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles convex, pitted, epaleate.</text>
      <biological_entity id="o2638" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–80, pistillate, fertile;</text>
      <biological_entity id="o2639" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="80" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow (usually 10–35 mm, sometimes reduced, not surpassing involucres).</text>
      <biological_entity id="o2640" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 20–100, bisexual, fertile;</text>
      <biological_entity id="o2641" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="100" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes ± equaling tubular-funnelform throats, lobes 5, erect, deltate;</text>
      <biological_entity id="o2642" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o2643" name="tube" name_original="tubes" src="d0_s14" type="structure" />
      <biological_entity id="o2644" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s14" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
      <biological_entity id="o2645" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branch appendages triangular.</text>
      <biological_entity constraint="style-branch" id="o2646" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae subcylindro-fusiform, terete to ± flattened, 3–4-angled, faintly 10–12-nerved, faces sericeous to strigose or glabrous;</text>
      <biological_entity id="o2647" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subcylindro-fusiform" value_original="subcylindro-fusiform" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s16" to="more or less flattened" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-4-angled" value_original="3-4-angled" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s16" value="10-12-nerved" value_original="10-12-nerved" />
      </biological_entity>
      <biological_entity id="o2648" name="face" name_original="faces" src="d0_s16" type="structure">
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s16" to="strigose or glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of 15–60 brownish, rigid, unequal, smooth, apically attenuate bristles in 1 series.</text>
      <biological_entity id="o2650" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s17" to="60" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="texture" src="d0_s17" value="rigid" value_original="rigid" />
        <character is_modifier="true" name="size" src="d0_s17" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s17" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o2651" name="series" name_original="series" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2649" id="r243" name="consist_of" negation="false" src="d0_s17" to="o2650" />
      <relation from="o2650" id="r244" name="in" negation="false" src="d0_s17" to="o2651" />
    </statement>
    <statement id="d0_s18">
      <text>x = 6.</text>
      <biological_entity id="o2649" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o2652" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>201.</number>
  <other_name type="common_name">Goldenweed</other_name>
  <discussion>Species 14 (14 in the flora).</discussion>
  <discussion>Pyrrocoma has often been treated as a section within genus Haplopappus (H. M. Hall 1928; A. Cronquist 1994). Various workers have suggested that Haplopappus be divided into a number of smaller genera such as Pyrrocoma, Tonestus, Stenotus, Chrysothamnus, and others. Pyrrocoma was recognized as a separate genus in the dissertation by R. A. Mayes (1976) and in a floristic treatment by G. K. Brown (1993b). It is characterized by its persistent basal rosettes, yellow-rayed heads on scapiform or few-bracteate peduncles, and obtuse, acute, or mucronate phyllaries. The heads vary greatly in size and commonly are sessile or subsessile and borne singly or in spiciform arrays. The base chromosome number of x = 6 is rare in Haplopappus in the broad sense, and supports recognition of a distinct genus (Mayes). Most species of Pyrrocoma are diploid, some tetraploid or hexaploid. The species are frequently polymorphic, and many subspecies and varieties have been named. Some of these taxa are only weakly characterized and their status needs clarification. The present treatment largely follows the works of Hall and Mayes.</discussion>
  <references>
    <reference>Mayes, R. A. 1976. A Cytotaxonomic and Chemosystematic Study of the Genus Pyrrocoma (Asteraceae: Astereae). Ph.D. dissertation. University of Texas.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads disciform, subtended by leaflike bracts</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads radiate, not subtended by leaflike bracts</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 5–50 cm; stems reddish, villous; basal leaf blades 5–40 mm wide, margins ciliate, faces puberulent; phyllaries in 3–5 series (± loose), unequal, margins ciliate, faces puberulent; British Columbia, nw United States</description>
      <determination>2 Pyrrocoma carthamoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 40–90 cm; stems pale, rarely reddish, glabrous; basal leaf blades 40–200 mm wide, margins eciliate, faces glabrous; phyllaries in 5–6 series, loosely appressed, unequal, margins eciliate, faces glabrous; Snake River canyon, Idaho, Oregon</description>
      <determination>13 Pyrrocoma radiata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves glandular</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves eglandular</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Glands sessile; stems not red-tinged, glabrous, resinous; basal leaf faces glabrous, gland-dotted; heads in crowded, spiciform arrays</description>
      <determination>11 Pyrrocoma lucida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Glands stipitate; stems red-tinged, hairy; basal leaf faces hairy; heads in corymbiform, racemiform, or paniculiform arrays</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems villous, tomentose, or woolly; basal leaf faces villous to tomentose; heads in racemiform arrays</description>
      <determination>5 Pyrrocoma hirta</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems ± tomentulose or glabrous; basal leaf faces tomentulose to glabrate or glabrous; heads in corymbiform or paniculiform arrays</description>
      <determination>8 Pyrrocoma lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads 3–20 (in racemiform, spiciform, corymbiform, or paniculiform arrays)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads 1–6+</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Heads in corymbiform or paniculiform arrays</description>
      <determination>8 Pyrrocoma lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Heads in racemiform or spiciform arrays</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries in 2–3 series, (loose) linear-lanceolate, subequal</description>
      <determination>6 Pyrrocoma insecticruris</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries in 4–5 series, lanceolate to oblanceolate or oblong, unequal</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems sparsely villous to tomentose; basal leaf faces sparsely tomen-tose; cypselae narrowly oblong</description>
      <determination>9 Pyrrocoma liatriformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems usually glabrous, sometimes sparsely tomentulose or villous; basal leaf faces usually glabrous; cypselae subcylindric</description>
      <determination>12 Pyrrocoma racemosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucres 20–60 mm wide; basal leaf blades broadly oblanceolate or spatulate to oblong, elliptic, or lanceolate, margins usually entire or undulate, sometimes dentate</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucres 10–20 mm wide; basal leaf blades usually linear to lanceolate or narrowly oblanceolate, margins usually coarsely dentate to laciniate, rarely entire</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Heads usually in racemiform arrays</description>
      <determination>7 Pyrrocoma integrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Heads usually borne singly, sometimes 2–6</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Plants usually 3–18 cm; stems usually sparsely villous, sometimes tomentose; basal leaf margins sparsely ciliate; phyllaries in 3–4 series, lanceolate to oblanceolate, unequal, margins ciliate, faces villous</description>
      <determination>3 Pyrrocoma clementis</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Plants usually 18–80 cm; stems usually glabrous, often tomentose distally; basal leaf margins eciliate; phyllaries oblong to spatulate, subequal, margins eciliate, faces usually glabrous</description>
      <determination>4 Pyrrocoma crocea</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf faces glabrous; phyllaries in 3–4 series, oblanceolate to oblong, unequal, margins ciliate, faces glabrous; cypselae glabrous</description>
      <determination>1 Pyrrocoma apargioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf faces sericeous or shaggy-tomentose; phyllaries in 2 series, linear to linear-lanceolate, equal, margins ciliate, faces ±villous; cypselae sericeous</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 2–5 mm wide (grasslike), margins usually entire</description>
      <determination>10 Pyrrocoma linearis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 3–20 mm wide, margins dentate to laciniate</description>
      <determination>14 Pyrrocoma uniflora</determination>
    </key_statement>
  </key>
</bio:treatment>