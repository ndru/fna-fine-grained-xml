<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="treatment_page">46</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="section">Macrocline</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 312. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section Macrocline</taxon_hierarchy>
    <other_info_on_name type="fna_id">316946</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) P. B. Cox &amp; Urbatsch" date="unknown" rank="subgenus">Macrocline</taxon_name>
    <taxon_hierarchy>genus Rudbeckia;subgenus Macrocline;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–300 cm (robust, rhizomatous or fibrous-rooted).</text>
      <biological_entity id="o19778" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green, bluish green, or purplish (usually glaucous).</text>
      <biological_entity id="o19779" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves often bluish green, glaucous;</text>
      <biological_entity id="o19780" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal (usually persistent to flowering) petiolate or sessile;</text>
      <biological_entity constraint="basal" id="o19781" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear, lanceolate to ovate, or oblong to elliptic, usually lobed, bases attenuate, cuneate, or rounded, ultimate margins entire, crenate, dentate, or serrate, apices acute to obtuse, faces glabrous or hairy, sometimes glaucous;</text>
      <biological_entity id="o19782" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate or oblong" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate or oblong" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o19783" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19784" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o19785" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o19786" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline petiolate or sessile, blades linear or ovate to pandurate, sometimes lobed, bases auriculate, rounded, or attenuate, ultimate margins entire or dentate to serrate, apices acute to obtuse, faces glabrous or hairy, sometimes glaucous.</text>
      <biological_entity constraint="cauline" id="o19787" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o19788" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="pandurate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o19789" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19790" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o19791" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o19792" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or in loose, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o19793" name="head" name_original="heads" src="d0_s6" type="structure">
        <character constraint="in arrays" constraintid="o19794" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform to paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o19794" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="loose" value_original="loose" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2 (–3) series.</text>
      <biological_entity id="o19795" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o19796" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o19795" id="r1345" name="in" negation="false" src="d0_s7" to="o19796" />
    </statement>
    <statement id="d0_s8">
      <text>Receptacles usually conic to columnar (hemispheric to ovoid in R. laciniata);</text>
      <biological_entity id="o19797" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually conic" name="shape" src="d0_s8" to="columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>paleae not surpassing cypselae (except in R. laciniata), margins ciliate, apices acute to obtuse or rounded to truncate, attenuate to apiculate, usually hairy.</text>
      <biological_entity id="o19798" name="palea" name_original="paleae" src="d0_s9" type="structure" />
      <biological_entity id="o19799" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
      <biological_entity id="o19800" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19801" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="obtuse or rounded" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="obtuse or rounded" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o19798" id="r1346" name="surpassing" negation="true" src="d0_s9" to="o19799" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or 8–15+;</text>
      <biological_entity id="o19802" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas bright-yellow.</text>
      <biological_entity id="o19803" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Discs 12–60 (–80) × 10–30 mm.</text>
      <biological_entity id="o19804" name="disc" name_original="discs" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="80" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s12" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (100–) 150–300 (–600+);</text>
      <biological_entity id="o19805" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="100" name="atypical_quantity" src="d0_s13" to="150" to_inclusive="false" />
        <character char_type="range_value" from="300" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="600" upper_restricted="false" />
        <character char_type="range_value" from="150" name="quantity" src="d0_s13" to="300" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas proximally yellow to yellowish green, distally yellow or greenish to brown-purple;</text>
      <biological_entity id="o19806" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="proximally yellow" name="coloration" src="d0_s14" to="yellowish green distally yellow or greenish" />
        <character char_type="range_value" from="proximally yellow" name="coloration" src="d0_s14" to="yellowish green distally yellow or greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anther appendages sometimes abaxially glanddotted;</text>
      <biological_entity constraint="anther" id="o19807" name="appendage" name_original="appendages" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>styles 3–7 mm, branches 1–2.2 mm, proximal 2/3–4/5 stigmatic, apices acute to rounded.</text>
      <biological_entity id="o19808" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19809" name="branch" name_original="branches" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s16" to="4/5" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s16" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o19810" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s16" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae (3–) 3.5–7.5 mm;</text>
      <biological_entity id="o19811" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi usually coroniform or of 2–6, unequal scales, 0.1–2.5 mm, sometimes 0.</text>
      <biological_entity id="o19813" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s18" to="6" />
        <character is_modifier="true" name="size" src="d0_s18" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o19812" id="r1347" name="consist_of" negation="false" src="d0_s18" to="o19813" />
    </statement>
    <statement id="d0_s19">
      <text>x = 18.</text>
      <biological_entity id="o19812" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s18" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" name="shape" src="d0_s18" value="of 2-6 , unequal scales" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s18" to="2.5" to_unit="mm" />
        <character modifier="sometimes" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o19814" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>261b.</number>
  <discussion>Species 13 (13 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and cauline leaves 0.2–1.5 cm wide</description>
      <determination>9 Rudbeckia mohrii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and cauline leaves 2–15(–45) cm wide</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of basal and proximal cauline leaves elliptic, lanceolate, or ovate, usually 1–2-pinnatifid or -pinnately compound (leaflets/lobes 3–11, distal cauline leaves sometimes lobed as well)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of basal, and proximal and mid, cauline leaves elliptic or lanceolate (not lobed, sometimes coarsely toothed, margins of distal cauline leaves crenate, dentate, entire, or toothed, not lobed)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Blades of all but distalmost leaves usually 1–2-pinnatifid or pinnately lobed (basal leaves not lobed and proximal cauline usually 3-lobed in var. heterophylla, known only from Florida); receptacles hemispheric or globose to ovoid; disc corolla lobes yellow (rhizomes elongate, slender, plants usually colonial); Rocky Mountains and e United States</description>
      <determination>7 Rudbeckia laciniata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Blades of basal leaves ± pinnatifid to pinnately lobed (distal leaves sometimes not lobed); receptacles columnar, conic, cylindric, or ovoid; disc corolla lobes yellowish green, greenish, or purplish (rhizomes stout, plants usually not colonial); w United States (including Rocky Mountains)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves sparsely to densely hairy on abaxial or both faces</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves usually glabrous, sometimes sparsely hairy on veins abaxially</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves moderately to densely hairy (both faces); rays 0; pappi of 4 scales, to 1 mm; Washington</description>
      <determination>2 Rudbeckia alpicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves sparsely hairy (abaxial faces); rays 8–21; pappi coroniform or of scales, 0.5–1.5 mm; California</description>
      <determination>4 Rudbeckia californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Rays 7–15; phyllaries to 1.5 cm (apices acute to rounded); discs 15–35 mm; California</description>
      <determination>6 Rudbeckia klamathensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Rays 0; phyllaries to 4 cm (apices attenuate); discs 20–60 mm; Colorado Rockies, Utah</description>
      <determination>10 Rudbeckia montana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mid (and often proximal) cauline leaves sessile, bases of blades usually auriculate (clasping) to truncate, sometimes rounded, faces (one or both) glabrate or hairy, or glabrous</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mid cauline leaves petiolate, bases of blades attenuate to cuneate (not clasping), faces (one or both) glabrous or hairy</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves bluish green (fresh, heavily glaucous; dried blades white under reflected UV light, at least abaxially); discs 40–80 mm; adjoining areas of Arkansas, Louisiana,Oklahoma, Texas (introduced South Carolina)</description>
      <determination>8 Rudbeckia maxima</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves green (fresh; dried blades dark under reflected UV light, at least abaxially); discs 12–25 mm; Alabama, Florida, Louisiana, Texas</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Cauline leaf bases auriculate, clasping; heads (usually 10+) in paniculiform ar-rays; discs 12–16 mm; paleae 4–6 mm; Alabama, Florida</description>
      <determination>3 Rudbeckia auriculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Cauline leaf bases auriculate or rounded, not clasping; heads (usually to 15) in ± corymbiform arrays; discs 12–25 mm; paleae 6–8 mm (stems often purplish); w Louisiana, e Texas 13. Rudbeckia scabrifolia</description>
      <determination>13 Rudbeckia scabrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf margins serrate or entire, faces usually sparsely to densely hairy (mostly abaxially), rarely glabrous; rays 0; n Rocky Mountains and Pacific states</description>
      <determination>12 Rudbeckia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf margins entire, serrate, serrulate, or toothed, faces glabrous or sparsely hairy; rays 7–16; Pacific states or se United States</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves bluish green (heavily glaucous); California, s Oregon</description>
      <determination>5 Rudbeckia glaucescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves green (sometimes lightly glaucous); se United States</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Basal leaf blades 15–50 × 3–9 cm (lengths ± 5 times widths); discs 20–45 mm; paleae 6–8 mm, apices acute (appressed in young heads); cypselae 5–7.5 mm; w Louisiana, e Texas</description>
      <determination>14 Rudbeckia texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Basal leaf blades 15–60 × 2–8 cm (lengths ± 7 times widths); discs 10–30 mm; paleae 5–6 mm, apices acute to acuminate (erect to spreading in young heads); cypselae 3–5.5 mm; n Florida, s Georgia</description>
      <determination>11 Rudbeckia nitida</determination>
    </key_statement>
  </key>
</bio:treatment>