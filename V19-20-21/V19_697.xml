<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">417</other_info_on_meta>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Kirpicznikov" date="1950" rank="genus">pseudognaphalium</taxon_name>
    <taxon_name authority="(Kunth) Anderberg" date="1991" rank="species">roseum</taxon_name>
    <place_of_publication>
      <publication_title>Opera Bot.</publication_title>
      <place_in_publication>104: 148. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus pseudognaphalium;species roseum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067394</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">roseum</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 63. 1818;  4(qto.): 81. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species roseum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 50–200 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o17479" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems persistently woolly-tomentose, not glandular.</text>
      <biological_entity id="o17481" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s2" value="woolly-tomentose" value_original="woolly-tomentose" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades oblong-lanceolate to oblanceolate, midcauline 3–7 cm × (3–) 6–15 (–20) mm, bases clasping to subclasping, not decurrent, margins usually undulate, faces concolor or weakly bicolor, usually woolly-tomentose, sometimes tardily glabrescent adaxially, stipitate or sessile-glandular beneath tomentum.</text>
      <biological_entity id="o17482" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o17483" name="midcauline" name_original="midcauline" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s3" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17484" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="clasping" name="architecture" src="d0_s3" to="subclasping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o17485" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o17486" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="concolor" value_original="concolor" />
        <character is_modifier="false" modifier="weakly" name="coloration" src="d0_s3" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="woolly-tomentose" value_original="woolly-tomentose" />
        <character is_modifier="false" modifier="sometimes tardily; adaxially" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipitate" value_original="stipitate" />
        <character constraint="beneath tomentum" constraintid="o17487" is_modifier="false" name="architecture" src="d0_s3" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o17487" name="tomentum" name_original="tomentum" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o17488" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o17489" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o17488" id="r1572" name="in" negation="false" src="d0_s4" to="o17489" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate, 4–4.5 mm.</text>
      <biological_entity id="o17490" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 5–6 series, usually white, sometimes pink (opaque or hyaline, dull to shiny), ovate to ovate-oblong, glabrous.</text>
      <biological_entity id="o17491" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="ovate-oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17492" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <relation from="o17491" id="r1573" name="in" negation="false" src="d0_s6" to="o17492" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate florets 45–90 (–110).</text>
      <biological_entity id="o17493" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="90" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="110" />
        <character char_type="range_value" from="45" name="quantity" src="d0_s7" to="90" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Bisexual florets (5–) 6–12 (–18).</text>
      <biological_entity id="o17494" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="6" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="18" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae weakly ridged, smooth.</text>
      <biological_entity id="o17495" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s9" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 [–1000+] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1000+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Rosy rabbit-tobacco</other_name>
  <discussion>Pseudognaphalium roseum usually grows above 1000 m in Mexico; it grows below 50 m in California, where it is probably adventive. The closest collections of the species southward from California are from Sinaloa and southern Chihuahua. It is abundant in Mexico only in the eastern and southern states.</discussion>
  <discussion>Pseudognaphalium roseum is recognized by its persistently tomentose stems and leaves, the leaves clasping to subclasping and non-decurrent, weakly bicolor and sessile-glandular beneath the tomentum, often relatively thick stems, relatively large heads with relatively numerous, white or pink, opaque phyllaries, relatively numerous florets, and smooth-faced cypselae. It has been confused with P. canescens; plants of P. roseum with relatively few bisexual florets can be distinguished from P. canescens by their subclasping leaves commonly with closely wavy margins, broader and more numerous phyllaries, and smooth-faced cypselae. Plants from southern California are atypical in their slightly smaller heads.</discussion>
  
</bio:treatment>