<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Guettard" date="1754" rank="genus">POROPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Acad. Roy. Sci. Mém. Math. Phys. (Paris,</publication_title>
      <place_in_publication>4to) 1750: 377. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus POROPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek poros, hole, and phyllon, leaf, alluding to gland-dotted leaf blades</other_info_on_name>
    <other_info_on_name type="fna_id">126562</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, subshrubs, or shrubs, 10–120 [–200+] cm.</text>
      <biological_entity id="o11056" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o11058" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually strictly branched.</text>
      <biological_entity id="o11060" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually strictly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite or alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o11061" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades orbiculate to linear or filiform, margins crenate or entire, faces usually glabrous (oil-glands scattered and/or at margins).</text>
      <biological_entity id="o11062" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="linear or filiform" />
      </biological_entity>
      <biological_entity id="o11063" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11064" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly or in loose, ± corymbiform arrays.</text>
      <biological_entity id="o11065" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in loose , more or less corymbiform arrays" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , more or less corymbiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o11066" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric to campanulate [turbinate], 3–12 mm diam.</text>
      <biological_entity id="o11067" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 5–10 in ± 2 series (distinct to bases, oblong or lanceolate to linear, bearing oval to linear oil-glands).</text>
      <biological_entity id="o11068" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o11069" from="5" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <biological_entity id="o11069" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles convex to conic, pitted, epaleate.</text>
      <biological_entity id="o11070" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s10" to="conic" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0.</text>
      <biological_entity id="o11071" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets (5–) 10–80 [–100+], bisexual, fertile;</text>
      <biological_entity id="o11072" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s12" to="10" to_inclusive="false" />
        <character char_type="range_value" from="80" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="80" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas usually yellow, sometimes whitish to greenish or purplish [brownish], tubes either very slender, much longer than funnelform throats, or stout, much shorter than narrowly cylindric throats, lobes 5, deltate to lanceolate (often unequal).</text>
      <biological_entity id="o11073" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="whitish" modifier="sometimes" name="coloration" src="d0_s13" to="greenish or purplish" />
      </biological_entity>
      <biological_entity id="o11074" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="either very" name="size" src="d0_s13" value="slender" value_original="slender" />
        <character constraint="than funnelform throats" constraintid="o11075" is_modifier="false" name="length_or_size" src="d0_s13" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s13" value="stout" value_original="stout" />
        <character constraint="than narrowly cylindric throats" constraintid="o11076" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o11075" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o11076" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o11077" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s13" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae cylindric to fusiform, hirtellous to villous [glabrous];</text>
      <biological_entity id="o11078" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="fusiform" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s14" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of 25–50 (–100) coarse to fine bristles in 1–2+ series.</text>
      <biological_entity id="o11080" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="100" />
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s15" to="50" />
        <character is_modifier="true" name="relief" src="d0_s15" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="width" src="d0_s15" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o11081" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="2" upper_restricted="false" />
      </biological_entity>
      <relation from="o11079" id="r761" name="consist_of" negation="false" src="d0_s15" to="o11080" />
      <relation from="o11080" id="r762" name="in" negation="false" src="d0_s15" to="o11081" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12.</text>
      <biological_entity id="o11079" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o11082" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, West Indies (Antilles), Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Antilles)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>320.</number>
  <discussion>Species ca. 25 (5 in the flora).</discussion>
  <references>
    <reference>Johnson, R. R. 1969. Monograph of the plant genus Porophyllum (Compositae: Helenieae). Univ. Kansas Sci. Bull. 48: 225–267.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; leaf blades oval or elliptic to obovate, 8–25+ mm wide</description>
      <determination>5 Porophyllum ruderale</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials, or subshrubs or shrubs; leaf blades linear to filiform, 1–5 mm wide</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas whitish or purplish</description>
      <determination>1 Porophyllum gracile</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas yellow (sometimes tinged reddish)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Subshrubs or shrubs, 20–60+ cm; phyllaries 7–10</description>
      <determination>2 Porophyllum scoparium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perennials (rarely woody at bases), mostly 5–30 cm; phyllaries 5(–8)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Internodes mostly 1–5(–12) mm; leaf blades 8–15 × 2 mm; pappi: longer bristles 6–7 mm</description>
      <determination>3 Porophyllum pygmaeum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Internodes mostly 10–20+ mm; leaf blades 20–60 × 1–2 mm; pappi: longer bristles 8–10 mm</description>
      <determination>4 Porophyllum greggii</determination>
    </key_statement>
  </key>
</bio:treatment>