<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="treatment_page">308</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="(M. E. Jones) A. Nelson" date="1904" rank="species">nauseosus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>37: 270. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species nauseosus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066639</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">caespitosus</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="variety">nauseosus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 696. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species caespitosus;variety nauseosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–15 (–25) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (caudices with relatively short and thick branches).</text>
      <biological_entity id="o2546" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or basally ascending, glabrous, minutely glandular.</text>
      <biological_entity id="o2547" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o2548" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades (usually 3-nerved) oblanceolate to obovate or spatulate, 30–80 (–100) × (2–) 4–15 mm, margins entire, sometimes spreading-ciliate (sometimes with conspicuously raised primary and secondary venation), faces usually glabrous, sometimes sparsely hispidulous to hirsute, minutely glandular;</text>
      <biological_entity constraint="basal" id="o2549" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or spatulate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2550" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s4" value="spreading-ciliate" value_original="spreading-ciliate" />
      </biological_entity>
      <biological_entity id="o2551" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="sometimes sparsely hispidulous" name="pubescence" src="d0_s4" to="hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades oblong-oblanceolate, little reduced distally.</text>
      <biological_entity constraint="cauline" id="o2552" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1 (–2).</text>
      <biological_entity id="o2553" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 5–8 × 8–15 mm.</text>
      <biological_entity id="o2554" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–3 (–4) series, glabrous or sparsely strigose, minutely glandular.</text>
      <biological_entity id="o2555" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2556" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o2555" id="r236" name="in" negation="false" src="d0_s8" to="o2556" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 30–60;</text>
      <biological_entity id="o2557" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white or blue, 6–12 mm, laminae reflexing.</text>
      <biological_entity id="o2558" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue" value_original="blue" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2559" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3–5.2 mm.</text>
      <biological_entity constraint="disc" id="o2560" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–2.3 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o2561" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o2562" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of 12–20 bristles.</text>
      <biological_entity id="o2563" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o2564" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o2565" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
      <relation from="o2563" id="r237" name="outer of" negation="false" src="d0_s13" to="o2564" />
      <relation from="o2563" id="r238" name="inner of" negation="false" src="d0_s13" to="o2565" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky or sandy soils, talus, or cliff ledges and crevices, sagebrush, pinyon-juniper, oak, fir, spruce-aspen</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="cliff ledges" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1600–)2500–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3500" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>84.</number>
  <other_name type="common_name">Marysvale fleabane</other_name>
  
</bio:treatment>