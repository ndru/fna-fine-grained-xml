<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="R. P. Roberts" date="2005" rank="species">arizonica</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 1558, fig. 1. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species arizonica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066509</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–50 cm.</text>
      <biological_entity id="o12117" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, reddish tan, becoming darker, branched, short-stipitate-glandular, usually resinous.</text>
      <biological_entity id="o12118" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish tan" value_original="reddish tan" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="darker" value_original="darker" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="usually" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending to spreading;</text>
      <biological_entity id="o12119" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades elliptic to narrowly oblanceolate (flat or somewhat concave adaxially), 10–35 × 2–5 mm, midnerves evident (often with 1–2 fainter, collateral veins), apices acute, apiculate, faces short-stipitate-glandular, usually gland-dotted;</text>
      <biological_entity id="o12120" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12121" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o12122" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity id="o12123" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in cymiform arrays (to 4 cm wide).</text>
      <biological_entity id="o12124" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o12125" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o12124" id="r1104" name="in" negation="false" src="d0_s5" to="o12125" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–10 mm (bracts 1–5, resembling phyllaries, stipitate-glandular).</text>
      <biological_entity id="o12126" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic, 5.5–7.5 × 2.5–4 mm.</text>
      <biological_entity id="o12127" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 30–40 in 4–5 series, tan, lanceolate to elliptic, 2–7 × 0.5–1.2 mm, strongly unequal, mostly chartaceous (bodies abruptly constricted at bases of appendages), midnerves faint, (margins membranous, sparsely ciliate apically), apices (usually recurved) usually acute to cuspidate, sometimes long-acuminate, abaxial faces glabrate, often gland-dotted.</text>
      <biological_entity id="o12128" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o12129" from="30" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o12129" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o12130" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o12131" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually acute" name="shape" src="d0_s8" to="cuspidate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12132" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 1–8;</text>
      <biological_entity id="o12133" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae elliptic, 3–4 × 0.8–1.3 mm.</text>
      <biological_entity id="o12134" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 5–15;</text>
      <biological_entity id="o12135" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 5–7 mm.</text>
      <biological_entity id="o12136" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae usually tan, sometimes reddish, narrowly obconic, 4–5.5 mm, glabrous or densely sericeous;</text>
      <biological_entity id="o12137" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi whitish tan, 4–5.5 mm.</text>
      <biological_entity id="o12138" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish tan" value_original="whitish tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock faces, cracks, and crevices and stony soils, usually on limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="cracks" modifier="faces" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="stony soils" />
        <character name="habitat" value="limestone" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Arizona goldenbush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ericameria arizonica grows in the Grand Canyon.</discussion>
  
</bio:treatment>