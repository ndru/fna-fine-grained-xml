<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">31</other_info_on_meta>
    <other_info_on_meta type="illustration_page">30</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Strother" date="2001" rank="genus">hedosyne</taxon_name>
    <taxon_name authority="(A. Gray) Strother" date="2001" rank="species">ambrosiifolia</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>47: 204. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus hedosyne;species ambrosiifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066846</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphrosyne</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">ambrosiifolia</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 102. 1852 (as ambrosiaefolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Euphrosyne;species ambrosiifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyclachaena</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">ambrosiifolia</taxon_name>
    <taxon_hierarchy>genus Cyclachaena;species ambrosiifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iva</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">ambrosiifolia</taxon_name>
    <taxon_hierarchy>genus Iva;species ambrosiifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petioles 5–12 (–45) mm;</text>
      <biological_entity id="o12465" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o12466" name="petiole" name_original="petioles" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="45" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades 3–5 (–9) × 4–5 (–8) cm overall, lobes 1–3 mm wide.</text>
      <biological_entity id="o12467" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12468" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12469" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 3–12+ mm.</text>
      <biological_entity id="o12470" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 2–3+ mm.</text>
      <biological_entity id="o12471" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 5 sparsely strigose or glabrous.</text>
      <biological_entity id="o12472" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o12473" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Paleae 1–1.5 mm.</text>
      <biological_entity id="o12474" name="palea" name_original="paleae" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Functionally staminate florets: corollas 1.5–2 mm.</text>
      <biological_entity id="o12475" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12476" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1.4–1.7 mm. 2n = 36.</text>
      <biological_entity id="o12477" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12478" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites (roadsides, washes, etc.), in sandy, gypseous, or calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="sandy" modifier="etc" />
        <character name="habitat" value="gypseous" />
        <character name="habitat" value="calcareous soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sonora, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>