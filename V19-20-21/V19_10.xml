<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="D. Don" date="1830" rank="genus">acourtia</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">microcephala</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 66. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus acourtia;species microcephala</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066005</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Perezia</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="unknown" rank="species">microcephala</taxon_name>
    <taxon_hierarchy>genus Perezia;species microcephala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–120 cm.</text>
      <biological_entity id="o329" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>sessile;</text>
      <biological_entity id="o330" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblong-ovate to elliptic, 5–15 cm, bases cordate, clasping, auriculate, margins spinulose-denticulate, faces densely glandular-scabrid.</text>
      <biological_entity id="o331" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o332" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o333" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spinulose-denticulate" value_original="spinulose-denticulate" />
      </biological_entity>
      <biological_entity id="o334" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence_or_relief" src="d0_s3" value="glandular-scabrid" value_original="glandular-scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o335" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o336" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o335" id="r33" name="in" negation="false" src="d0_s4" to="o336" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate to campanulate, 6–8 (–10+) mm.</text>
      <biological_entity id="o337" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 3–4 series, linear to oblanceolate, apices acute to acuminate or mucronate, margins and abaxial faces glandular-hairy.</text>
      <biological_entity id="o338" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s6" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o339" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o340" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate or mucronate" />
      </biological_entity>
      <biological_entity id="o341" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o342" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <relation from="o338" id="r34" name="in" negation="false" src="d0_s6" to="o339" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles alveolate, glandular-hairy.</text>
      <biological_entity id="o343" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="alveolate" value_original="alveolate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 10–20;</text>
      <biological_entity id="o344" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas lavender-pink, 6–12 mm.</text>
      <biological_entity id="o345" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="lavender-pink" value_original="lavender-pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae cylindric, 1.5–6 mm, bristly glandular-puberulent;</text>
      <biological_entity id="o346" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi tawny, 6–10 mm. 2n = 54.</text>
      <biological_entity id="o347" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o348" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravel and caliche soils in scrub oak, oak woodlands, and chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel" constraint="in scrub oak , oak woodlands , and chaparral" />
        <character name="habitat" value="caliche soils" constraint="in scrub oak , oak woodlands , and chaparral" />
        <character name="habitat" value="scrub oak" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>In the flora, Acourtia microcephala is restricted to the dry areas of California from Monterey to San Diego counties and on the Channel Islands.</discussion>
  
</bio:treatment>