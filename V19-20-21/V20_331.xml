<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="treatment_page">154</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1993" rank="subsection">triplinerviae</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1989" rank="species">juliae</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>67: 445. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection triplinerviae;species juliae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067548</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">canescens</taxon_name>
    <taxon_hierarchy>genus Solidago;species canadensis;variety canescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (50–) 100–250 cm;</text>
      <biological_entity id="o897" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short.</text>
      <biological_entity id="o898" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5, densely, evenly villoso-tomentose (hairs white, thin, crisped).</text>
      <biological_entity id="o899" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="densely; evenly" name="pubescence" src="d0_s2" value="villoso-tomentose" value_original="villoso-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal 0;</text>
      <biological_entity id="o900" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o901" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal cauline lanceolate to narrowly lanceolate, 3-nerved, 50–80 (–120) × 5–10 (–12) mm, reduced distally to 10–20 mm, margins shallowly crenate or serrate to nearly entire, faces moderately to densely short pilose (hairs somewhat ascending).</text>
      <biological_entity id="o902" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o903" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character name="shape" src="d0_s4" value="serrate to nearly entire" value_original="serrate to nearly entire" />
      </biological_entity>
      <biological_entity id="o904" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 80–950, secund, in narrowly pyramidal paniculiform arrays, 3–4 times as long as wide, branches spreading and slightly secund, bracts linear-lanceolate, 3–6 mm.</text>
      <biological_entity id="o905" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s5" to="950" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character is_modifier="false" name="l_w_ratio" notes="" src="d0_s5" value="3-4" value_original="3-4" />
      </biological_entity>
      <biological_entity id="o906" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o907" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s5" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o908" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o905" id="r84" name="in" negation="false" src="d0_s5" to="o906" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles moderately to densely villoso-strigillose, rarely sparsely minutely stipitate-glandular;</text>
      <biological_entity id="o909" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s6" value="villoso-strigillose" value_original="villoso-strigillose" />
        <character is_modifier="false" modifier="rarely sparsely minutely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 1–2 (–4).</text>
      <biological_entity id="o910" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly campanulate, 3–4 mm.</text>
      <biological_entity id="o911" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–4 series, oblong-lanceolate or lanceolate to narrowly elliptic, strongly unequal, margins apically ciliate, glabrous, rarely outer sparsely, minutely stipitate-glandular.</text>
      <biological_entity id="o912" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s9" to="narrowly elliptic" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o913" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o914" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="apically" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o915" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s9" value="outer" value_original="outer" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o912" id="r85" name="in" negation="false" src="d0_s9" to="o913" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 7–15;</text>
      <biological_entity id="o916" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>laminae 1–2 (–3) × 0.1–0.6 mm.</text>
      <biological_entity id="o917" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 5–9;</text>
      <biological_entity id="o918" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 2.8–3 mm, lobes 0.4–1.1 mm.</text>
      <biological_entity id="o919" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o920" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (narrowly obconic) 1.4–1.6 mm, sparsely strigillose;</text>
      <biological_entity id="o921" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi ca. 3 mm. 2n = 18.</text>
      <biological_entity id="o922" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o923" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr and Aug–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" constraint=" -Aug-Oct(-Nov)" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soils along streams and lake edges, in grasslands, oak and oak-pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soils" constraint="along streams and lake edges" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="lake edges" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="oak-pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1000+[–2200] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000+" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="2200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Nuevo Leon).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo Leon)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>58.</number>
  <other_name type="common_name">Julia’s goldenrod</other_name>
  <discussion>Solidago juliae is found on the Edwards Plateau and in Trans-Pecos Texas and adjacent Mexico. G. L. Nesom (1989e) discussed its nomenclatural history and reasons for treating it as a distinct species.</discussion>
  
</bio:treatment>