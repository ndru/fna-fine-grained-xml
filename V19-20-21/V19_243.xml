<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">elephantopus</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="species">nudatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>15: 47. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus elephantopus;species nudatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066494</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (1–) 3–11+ dm.</text>
      <biological_entity id="o17750" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="11" to_unit="dm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal at flowering;</text>
      <biological_entity id="o17751" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17752" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades mostly oblanceolate to spatulate, sometimes elliptic, 7–15 (–20+) cm × 20–35 (–45+) mm (including petioles), both faces strigose or pilose to hirsute.</text>
      <biological_entity id="o17753" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly oblanceolate" name="shape" src="d0_s2" to="spatulate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="20" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="45" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17754" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s2" to="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bracts rounded-deltate to lance-deltate, 6–15+ × 4–9+ mm.</text>
      <biological_entity id="o17755" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s3" to="lance-deltate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="9" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inner phyllaries 6–7.5 mm, sparsely strigose or hispidulous with hairs 0.05–0.3 (–0.5) mm.</text>
      <biological_entity constraint="inner" id="o17756" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="7.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character constraint="with hairs" constraintid="o17757" is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o17757" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2.5–3 mm;</text>
      <biological_entity id="o17758" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi 3–4.5 mm. 2n = 22.</text>
      <biological_entity id="o17759" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17760" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open or shaded, dry to wet places in pine forests and mixed forests, often on sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet places" modifier="open or shaded" constraint="in pine" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="dry to wet places in pine forests" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., La., Md., Miss., N.C., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>