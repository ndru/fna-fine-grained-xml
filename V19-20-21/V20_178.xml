<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
    <other_info_on_meta type="illustration_page">91</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">gutierrezia</taxon_name>
    <taxon_name authority="(Pursh) Britton &amp; Rusby" date="1887" rank="species">sarothrae</taxon_name>
    <place_of_publication>
      <publication_title>Trans. New York Acad. Sci.</publication_title>
      <place_in_publication>7: 10. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus gutierrezia;species sarothrae</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066828</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">sarothrae</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 540. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;species sarothrae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthocephalum</taxon_name>
    <taxon_name authority="(Pursh) Shinners" date="unknown" rank="species">sarothrae</taxon_name>
    <taxon_hierarchy>genus Xanthocephalum;species sarothrae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 10–60 (–100) cm.</text>
      <biological_entity id="o19126" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems minutely hispidulous.</text>
      <biological_entity id="o19127" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s1" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal absent at flowering;</text>
      <biological_entity id="o19128" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline blades 1-nerved or 3-nerved, linear to lanceolate, sometimes filiform and fascicled, 1.5–2 (–3) mm wide, little reduced distally.</text>
      <biological_entity id="o19129" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o19130" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (sessile to subsessile in compact glomerules) in dense, flat-topped, corymbiform arrays.</text>
      <biological_entity id="o19131" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o19132" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="true" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o19131" id="r1770" name="in" negation="false" src="d0_s4" to="o19132" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindric to cuneate-campanulate, 1.5–2 (–3) mm diam.</text>
      <biological_entity id="o19133" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s5" to="cuneate-campanulate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllary apices flat.</text>
      <biological_entity constraint="phyllary" id="o19134" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (2–) 3–8;</text>
      <biological_entity id="o19135" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 3–5.5 mm.</text>
      <biological_entity id="o19136" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets (2–) 3–9 (usually bisexual and fertile, rarely functionally staminate, corollas tubular-funnelform, lobes erect to spreading or recurved, deltate).</text>
      <biological_entity id="o19137" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 0.8–1.6 (–2.2) mm, faces without oil cavities, densely strigoso-sericeous;</text>
      <biological_entity id="o19138" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19139" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s10" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
      <biological_entity constraint="oil" id="o19140" name="cavity" name_original="cavities" src="d0_s10" type="structure" />
      <relation from="o19139" id="r1771" name="without" negation="false" src="d0_s10" to="o19140" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of 1–2 series of narrowly oblong to ovatelanceolate or obovate scales (readily falling, those of discs 1/3–1/2 corollas, shorter on rays).</text>
      <biological_entity id="o19142" name="series" name_original="series" src="d0_s11" type="structure" constraint="scale" constraint_original="scale; scale">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
      <biological_entity id="o19143" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly oblong" is_modifier="true" name="shape" src="d0_s11" to="ovatelanceolate or obovate" />
      </biological_entity>
      <relation from="o19141" id="r1772" name="consist_of" negation="false" src="d0_s11" to="o19142" />
      <relation from="o19142" id="r1773" name="part_of" negation="false" src="d0_s11" to="o19143" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 8, 16, 32.</text>
      <biological_entity id="o19141" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o19144" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Nov(–Jan).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Jan" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, commonly on rocky, open slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" modifier="grasslands" />
        <character name="habitat" value="open slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Ariz., Calif., Colo., Idaho, Kans., Minn., Mont., Nebr., Nev., N.Mex., N.Y., N.Dak., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico (Baja California, Baja California Sur, Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sonora, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Kindlingweed</other_name>
  <discussion>Gutierrezia sarothrae is often abundant in overgrazed pastures; it is naturalized in New York.</discussion>
  
</bio:treatment>