<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Miller) Lessing" date="1832" rank="subgenus">absinthium</taxon_name>
    <taxon_name authority="A. Gray" date="1863" rank="species">scopulorum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>15: 66. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus absinthium;species scopulorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066165</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–25 cm (cespitose), mildly aromatic (caudices relatively slender).</text>
      <biological_entity id="o1912" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="mildly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems gray-green, glabrate.</text>
      <biological_entity id="o1913" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, gray-green;</text>
      <biological_entity id="o1914" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (basal) oblanceolate, 2–7 × 0.1 cm, 2-pinnately lobed (lobes linear or oblanceolate; cauline blades smaller, 1–2-pinnate or entire), faces silky-canescent.</text>
      <biological_entity id="o1915" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character name="width" src="d0_s3" unit="cm" value="0.1" value_original="0.1" />
        <character is_modifier="false" modifier="2-pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o1916" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="silky-canescent" value_original="silky-canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (5–22) in spiciform arrays 5–9 × 1–1.5 cm.</text>
      <biological_entity id="o1917" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in spiciform arrays" from="5" name="atypical_quantity" src="d0_s4" to="22" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres broadly globose or subglobose, 4 × 4–7 mm.</text>
      <biological_entity id="o1918" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subglobose" value_original="subglobose" />
        <character name="length" src="d0_s5" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries green (margins black or dark-brown), densely villous.</text>
      <biological_entity id="o1919" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 6–13;</text>
      <biological_entity id="o1920" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 15–30;</text>
      <biological_entity id="o1921" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 1.5–2.5 mm, hairy (at least on lobes).</text>
      <biological_entity id="o1922" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o1923" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 0.8–1 mm, glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o1924" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1925" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, protected areas, bases of rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="protected areas" />
        <character name="habitat" value="bases" constraint="of rocks" />
        <character name="habitat" value="rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3100–4200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4200" to_unit="m" from="3100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Mont., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  
</bio:treatment>