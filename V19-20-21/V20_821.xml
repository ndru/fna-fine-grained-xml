<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="L’Héritier" date="1789" rank="genus">boltonia</taxon_name>
    <taxon_name authority="(Linnaeus) L’Héritier" date="1789" rank="species">asteroides</taxon_name>
    <taxon_name authority="(A. Gray) Cronquist" date="1947" rank="variety">latisquama</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>74: 149. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus boltonia;species asteroides;variety latisquama</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boltonia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">latisquama</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 33: 238. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Boltonia;species latisquama;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Heads 30–50 (–100);</text>
      <biological_entity id="o17269" name="head" name_original="heads" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s0" to="100" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s0" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bracts linear-oblanceolate to narrowly oblanceolate or linear-subulate, 25–70 × 2–7 mm.</text>
      <biological_entity id="o17270" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s1" to="narrowly oblanceolate or linear-subulate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s1" to="70" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 4–5 × 6–14 mm.</text>
      <biological_entity id="o17271" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries sometimes reflexed, spatulate to obovate-spatulate, subequal, membranous margins broad, (2–) 2.5–6 mm wide, apices cuspidate;</text>
      <biological_entity id="o17272" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="obovate-spatulate" />
        <character is_modifier="false" name="size" src="d0_s3" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o17273" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>outer 2.6–3.8 × 0.5–1.6 mm;</text>
    </statement>
    <statement id="d0_s5">
      <text>inner 3.2–4 × 0.9–1.5 mm.</text>
      <biological_entity id="o17274" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s4" value="outer" value_original="outer" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="inner" value_original="inner" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae obovoid to obcordiform, winged;</text>
      <biological_entity id="o17275" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s6" to="obcordiform" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappus awns 0.7–2 mm, lengths 2/3+ cypselae.</text>
      <biological_entity id="o17277" name="cypsela" name_original="cypselae" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 18.</text>
      <biological_entity constraint="pappus" id="o17276" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17278" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, streams, sloughs, wet ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="sloughs" />
        <character name="habitat" value="wet ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Conn., Iowa, Kans., Maine, Md., Mass., Minn., Miss., Mo., Nebr., N.Dak., Ohio, Okla., R.I., S.Dak., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <discussion>Variety latisquama is considered endangered in Maryland.</discussion>
  
</bio:treatment>