<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">490</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="R. Brown" date="1817" rank="genus">ISOCARPHA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>12: 110. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ISOCARPHA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek iso -, same, and carphos, small dry body, evidently alluding to uniform receptacular paleae</other_info_on_name>
    <other_info_on_name type="fna_id">116567</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs [annuals], 10–150 cm.</text>
      <biological_entity id="o12522" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o12523" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, sometimes basally decumbent [creeping].</text>
      <biological_entity id="o12524" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes basally" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly opposite (distal sometimes alternate) [alternate];</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o12525" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ± 3-nerved, elliptic to narrowly lance-elliptic [ovate or lanceolate to linear], margins entire or serrulate [dentate], faces glabrate to weakly hirsutulous or pilosulous, glanddotted.</text>
      <biological_entity id="o12526" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic to narrowly" value_original="elliptic to narrowly" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="lance-elliptic" value_original="lance-elliptic" />
      </biological_entity>
      <biological_entity id="o12527" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o12528" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="weakly hirsutulous or pilosulous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic to narrowly" value_original="elliptic to narrowly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly or in tight, corymbiform to subcapitate [paniculiform] arrays.</text>
      <biological_entity id="o12529" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in arrays" constraintid="o12530" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in tight , corymbiform to subcapitate arrays" />
      </biological_entity>
      <biological_entity id="o12530" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="tight" value_original="tight" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± obconic, 4–6 mm diam.</text>
      <biological_entity id="o12531" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 10–15+ in 2+ series, [1–] 2–3 [–6] -nerved, ovate or elliptic to lanceolate or linear, ± equal.</text>
      <biological_entity id="o12532" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o12533" from="10" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="[1-]2-3[-6]-nerved" value_original="[1-]2-3[-6]-nerved" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="lanceolate or linear" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o12533" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic to columnar, paleate (paleae similar to inner phyllaries).</text>
      <biological_entity id="o12534" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="columnar" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 60–200+;</text>
      <biological_entity id="o12535" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s10" to="200" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white or pinkish, throats funnelform (lengths ca. 2 times diams.) [cylindric or campanulate], lobes 5, ± deltate;</text>
      <biological_entity id="o12536" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
      </biological_entity>
      <biological_entity id="o12537" name="throat" name_original="throats" src="d0_s11" type="structure" />
      <biological_entity id="o12538" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases enlarged, glabrous [papillose], branches filiform.</text>
      <biological_entity id="o12539" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o12540" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12541" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae prismatic, 5-ribbed, glabrous [pubescent];</text>
      <biological_entity id="o12542" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="5-ribbed" value_original="5-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o12543" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o12544" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s, c United States, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s" establishment_means="native" />
        <character name="distribution" value="c United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>402.</number>
  <other_name type="common_name">Pearlhead</other_name>
  <discussion>Species 5 (1 in the flora).</discussion>
  <references>
    <reference>Keil, D. J. and T. F. Stuessy. 1981. Systematics of Isocarpha (Compositae: Eupatorieae). Syst. Bot. 6: 258–287.</reference>
  </references>
  
</bio:treatment>