<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">foliosus</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="1992" rank="variety">mendocinus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>72: 187. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species foliosus;variety mendocinus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068340</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">mendocinus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>2: 9. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species mendocinus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves without prominent 1-directional orientation;</text>
      <biological_entity id="o30853" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>blades 20–40 × 2–4 mm wide, apices acute to obtuse, faces glabrous or glabrate (margins and midveins ascending-ciliate).</text>
      <biological_entity id="o30854" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s1" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30855" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o30856" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 0.8–1 mm wide, margins usually broadly scarious, midnerves usually not distinct and orange-resinous, abaxial faces moderately to densely strigoso-hirsute, densely, prominently glandular;</text>
      <biological_entity id="o30857" name="phyllarie" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30858" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually broadly" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o30859" name="midnerve" name_original="midnerves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coating" src="d0_s2" value="orange-resinous" value_original="orange-resinous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30860" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="strigoso-hirsute" value_original="strigoso-hirsute" />
        <character is_modifier="false" modifier="densely; prominently" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>inner phyllaries 5–6.5 mm.</text>
      <biological_entity constraint="inner" id="o30861" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 25–48;</text>
      <biological_entity id="o30862" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s4" to="48" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 10–15 mm.</text>
      <biological_entity id="o30863" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corollas 3.8–5 mm.</text>
      <biological_entity constraint="disc" id="o30864" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bars, banks, and ledges along rivers, dry slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bars" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="dry slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>96e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>