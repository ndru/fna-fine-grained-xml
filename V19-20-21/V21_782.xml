<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">tenuifolius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 742. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species tenuifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066980</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, 40–150 cm.</text>
      <biological_entity id="o3394" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2-pinnate, 8–15 cm, lobes (3–) 5–15+ × 0.5–1.5+ mm;</text>
      <biological_entity id="o3395" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3396" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s1" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 8–30.</text>
      <biological_entity constraint="cauline" id="o3397" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 20–200 per stem.</text>
      <biological_entity id="o3398" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o3399" from="20" name="quantity" src="d0_s3" to="200" />
      </biological_entity>
      <biological_entity id="o3399" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–5 cm.</text>
      <biological_entity id="o3400" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries yellowish, 5–8 × 2–4 mm.</text>
      <biological_entity id="o3401" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 0.</text>
      <biological_entity id="o3402" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 25–50;</text>
      <biological_entity id="o3403" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s7" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas whitish, 2.5–3 mm, tubes 1.5–2.2 mm, throats campanulate, 0.8–1.5 mm, lengths 1.5–2 times lobes.</text>
      <biological_entity id="o3404" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3405" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3406" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character constraint="lobe" constraintid="o3407" is_modifier="false" name="length" src="d0_s8" value="1.5-2 times lobes" value_original="1.5-2 times lobes" />
      </biological_entity>
      <biological_entity id="o3407" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 3.5–4.5 mm, ± villous;</text>
      <biological_entity id="o3408" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 16–18 scales 1–1.5 (–2) mm.</text>
      <biological_entity id="o3410" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s10" to="18" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o3409" id="r263" name="consist_of" negation="false" src="d0_s10" to="o3410" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity id="o3409" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o3411" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly, or silty soils, limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="sandy" />
        <character name="habitat" value="silty soils" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., N.Mex., Okla., S.Dak., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  
</bio:treatment>