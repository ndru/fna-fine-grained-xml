<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="illustration_page">114</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="genus">helianthella</taxon_name>
    <taxon_name authority="(Hooker) A. Gray" date="1883" rank="species">quinquenervis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 10. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus helianthella;species quinquenervis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066866</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">quinquenervis</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>6: 247. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthus;species quinquenervis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (30–) 50–150 cm.</text>
      <biological_entity id="o6676" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves: largest proximal to mid-stems;</text>
      <biological_entity constraint="cauline" id="o6677" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
        <character constraint="to mid-stems" constraintid="o6678" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o6678" name="mid-stem" name_original="mid-stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blades usually 3– or 5–nerved, elliptic or ovatelanceolate to lanceolate, 10–50 cm, faces sparsely hirsute or glabrous.</text>
      <biological_entity constraint="cauline" id="o6679" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6680" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="5-nerved" value_original="5-nerved" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6681" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly, ± nodding.</text>
      <biological_entity id="o6682" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s3" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric or broader, (25–) 40–50 mm diam.</text>
      <biological_entity id="o6683" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s4" value="broader" value_original="broader" />
        <character char_type="range_value" from="25" from_unit="mm" name="diameter" src="d0_s4" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="diameter" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries (outer sometimes ± foliaceous) ovate to lanceolate (subequal to unequal, margins ciliate).</text>
      <biological_entity id="o6684" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae scarious, relatively soft.</text>
      <biological_entity id="o6685" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="relatively" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (13–) 21;</text>
      <biological_entity id="o6686" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s7" to="21" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="21" value_original="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>(corollas bright-yellow) laminae 25–30 (–40) mm.</text>
      <biological_entity id="o6687" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="40" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas yellow.</text>
      <biological_entity constraint="disc" id="o6688" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae strongly compressed, narrowly obovate (margins ciliate, faces strigose);</text>
      <biological_entity id="o6689" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 2 ± subulate scales plus 2–4+ shorter scales.</text>
      <biological_entity id="o6690" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o6691" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s11" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o6692" name="scale" name_original="scales" src="d0_s11" type="structure" />
      <relation from="o6690" id="r486" name="consist_of" negation="false" src="d0_s11" to="o6691" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, aspen glades</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="aspen glades" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, Mont., Nev., N.Mex., Oreg., S.Dak., Wyo.; Mexico (Chihuahua, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Helianthella quinquenervis grows primarily in the Rocky Mountains, Great Basin ranges, and Black Hills.</discussion>
  
</bio:treatment>