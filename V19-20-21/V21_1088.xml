<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">microcephalum</taxon_name>
    <taxon_name authority="(A. Gray) Bierner" date="1972" rank="variety">oöclinium</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>5: 47. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species microcephalum;variety oöclinium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068428</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helenium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">oöclinium</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>9: 202. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helenium;species oöclinium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 25–80 cm.</text>
      <biological_entity id="o12124" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal and mid-blades serrate to undulate-serrate to occasionally deeply toothed or laciniate, glabrate;</text>
      <biological_entity id="o12125" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o12126" name="mid-blade" name_original="mid-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s1" to="undulate-serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal blades glabrous or sparsely to moderately hairy.</text>
      <biological_entity id="o12127" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o12128" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s2" to="sparsely moderately hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 20–100+ per plant.</text>
      <biological_entity id="o12129" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o12130" from="20" name="quantity" src="d0_s3" to="100" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12130" name="plant" name_original="plant" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1.5–8 cm, usually projecting terminal heads well beyond foliage.</text>
      <biological_entity id="o12131" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o12132" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="usually" name="orientation" src="d0_s4" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o12133" name="foliage" name_original="foliage" src="d0_s4" type="structure" />
      <relation from="o12132" id="r831" name="beyond" negation="false" src="d0_s4" to="o12133" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 8–12 × 8–12 mm.</text>
      <biological_entity id="o12134" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 9–13;</text>
      <biological_entity id="o12135" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas usually yellow throughout, sometimes reddish proximally and yellow distally, 2.7–9.1 × 1.5–6.7 mm.</text>
      <biological_entity id="o12136" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes; proximally" name="coloration" src="d0_s7" value="reddish proximally and yellow" value_original="reddish proximally and yellow" />
        <character char_type="range_value" from="2.7" from_unit="mm" modifier="distally" name="length" src="d0_s7" to="9.1" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="distally" name="width" src="d0_s7" to="6.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 200–400+;</text>
      <biological_entity id="o12137" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s8" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 1.7–2.4 mm, lobes 5.</text>
      <biological_entity id="o12138" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12139" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pappi 0.4–0.7 mm. 2n = 26.</text>
      <biological_entity id="o12140" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12141" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jun(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or clay soils, ditches, washes, around ponds and lakes, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="washes" constraint="around ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Durango, San Luis Potosí).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.</number>
  
</bio:treatment>