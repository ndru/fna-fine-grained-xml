<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) C. L. Boynton &amp; Beadle" date="1901" rank="species">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 12. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species graminifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067446</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinacea</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall" date="unknown" rank="species">atrorubens</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 306. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinacea;species atrorubens;variety graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, to 80 cm (roots fibrous).</text>
      <biological_entity id="o26060" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems moderately strigose (hairs ascending).</text>
      <biological_entity id="o26061" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades narrowly lanceolate to elliptic (grasslike, lengths 10+ times widths, not lobed), herbaceous, bases attenuate, margins entire, apices acute, faces glabrous or sparsely hirsute;</text>
      <biological_entity id="o26062" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26063" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o26064" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o26065" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26066" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o26067" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal petiolate or sessile, 10–25 × 0.5–1 cm;</text>
      <biological_entity id="o26068" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o26069" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile, 1–25 × 0.2–1 cm (distal smaller), bases attenuate.</text>
      <biological_entity id="o26070" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o26071" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26072" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads mostly borne singly.</text>
      <biological_entity id="o26073" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries to 1 cm (spreading to reflexed).</text>
      <biological_entity id="o26074" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles hemispheric to ovoid;</text>
      <biological_entity id="o26075" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae 4–5 mm, (apical margins glabrous) apices acuminate-cuspidate, awn-tipped, abaxial tips sparsely strigose.</text>
      <biological_entity id="o26076" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26077" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="acuminate-cuspidate" value_original="acuminate-cuspidate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26078" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 8–16;</text>
      <biological_entity id="o26079" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas orangish red to maroon, laminae elliptic to obovate, 10–25 × 3–6 mm, abaxially sparsely strigose.</text>
      <biological_entity id="o26080" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="orangish red" name="coloration" src="d0_s10" to="maroon" />
      </biological_entity>
      <biological_entity id="o26081" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Discs 10–15 × 8–15 mm.</text>
      <biological_entity id="o26082" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 50–300+;</text>
      <biological_entity id="o26083" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="300" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas proximally maroon, distally brown-purple, 3.9–4.4 mm;</text>
      <biological_entity id="o26084" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="maroon" value_original="maroon" />
        <character is_modifier="false" modifier="distally" name="coloration_or_density" src="d0_s13" value="brown-purple" value_original="brown-purple" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s13" to="4.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branches ca. 1.8 mm, apices obtuse to rounded.</text>
      <biological_entity id="o26085" name="branch-style" name_original="style-branches" src="d0_s14" type="structure">
        <character name="distance" src="d0_s14" unit="mm" value="1.8" value_original="1.8" />
      </biological_entity>
      <biological_entity id="o26086" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o26087" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi coroniform, to 0.5 mm. 2n = 38.</text>
      <biological_entity id="o26088" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26089" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, sandy flatwoods sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="sandy flatwoods" />
        <character name="habitat" value="sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Grassleaf coneflower</other_name>
  <discussion>Rudbeckia graminifolia grows in the Apalachicola region.</discussion>
  
</bio:treatment>