<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">160</other_info_on_meta>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="1901" rank="species">andrewsii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. W. Calif.,</publication_title>
      <place_in_publication>506. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species andrewsii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066357</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">andrewsii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 45. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species andrewsii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials (or short–lived monocarpic perennials), 60–200 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o6182" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems several, erect to spreading, thinly arachnoid, soon glabrous;</text>
      <biological_entity id="o6183" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches ± fleshy, usually much branched proximally, spreading to ascending.</text>
      <biological_entity id="o6184" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="usually much; proximally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades ± elliptic, 30–75 × 10–20 cm, shallowly to deeply pinnatifid, lobes oblong to ovate, unlobed or with several prominent secondary lobes or large teeth, obtuse to acute, main spines 2–7 mm, abaxial gray arachnoid-tomentose, adaxial faces thinly arachnoid, glabrate;</text>
      <biological_entity id="o6185" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6186" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s4" to="75" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s4" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o6187" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="with several prominent secondary lobes or large teeth" />
        <character char_type="range_value" from="obtuse" name="shape" notes="" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o6188" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character is_modifier="true" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o6189" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="main" id="o6190" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6191" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6192" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o6187" id="r588" name="with" negation="false" src="d0_s4" to="o6188" />
      <relation from="o6187" id="r589" name="with" negation="false" src="d0_s4" to="o6189" />
    </statement>
    <statement id="d0_s5">
      <text>basal often present at flowering, spiny winged-petiolate;</text>
      <biological_entity id="o6193" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o6194" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline sessile, bases clasping with broad, spiny-margined auricles, reduced distally, spinier than proximal;</text>
      <biological_entity id="o6195" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o6196" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6197" name="base" name_original="bases" src="d0_s6" type="structure">
        <character constraint="with auricles" constraintid="o6198" is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s6" value="reduced" value_original="reduced" />
        <character constraint="than proximal leaves" constraintid="o6199" is_modifier="false" name="architecture" src="d0_s6" value="spinier" value_original="spinier" />
      </biological_entity>
      <biological_entity id="o6198" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="broad" value_original="broad" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="spiny-margined" value_original="spiny-margined" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6199" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>distal much reduced, spines 7–20 mm.</text>
      <biological_entity id="o6200" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o6201" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6202" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads several–many, in congested corymbiform arrays.</text>
      <biological_entity id="o6203" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="several" is_modifier="false" name="quantity" src="d0_s8" to="many" />
      </biological_entity>
      <biological_entity id="o6204" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o6203" id="r590" name="in" negation="false" src="d0_s8" to="o6204" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–7 cm.</text>
      <biological_entity id="o6205" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to hemispheric or campanulate, 1.5–3 × 1.5–5 cm, sparsely to densely arachnoid, finely short-ciliate.</text>
      <biological_entity id="o6206" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="hemispheric or campanulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="finely" name="architecture_or_pubescence_or_shape" src="d0_s10" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in ca. 6 series, dark green or brown or with stramineous margins and a darker central zone, imbricate, linear-lanceolate (outer) to linear (inner), abaxial faces without glutinous ridge;</text>
      <biological_entity id="o6207" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="with stramineous margins and a darker central zone" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o6208" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o6209" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="central" id="o6210" name="zone" name_original="zone" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6211" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o6212" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o6207" id="r591" name="in" negation="false" src="d0_s11" to="o6208" />
      <relation from="o6207" id="r592" name="with" negation="false" src="d0_s11" to="o6209" />
      <relation from="o6207" id="r593" name="with" negation="false" src="d0_s11" to="o6210" />
      <relation from="o6211" id="r594" name="without" negation="false" src="d0_s11" to="o6212" />
    </statement>
    <statement id="d0_s12">
      <text>outer and mid bodies appressed, spiny-ciliate, apices long-spreading to ascending long-acuminate, spines straight, stout, 5–15 mm;</text>
      <biological_entity constraint="outer and mid" id="o6213" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="spiny-ciliate" value_original="spiny-ciliate" />
      </biological_entity>
      <biological_entity id="o6214" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="long-spreading" name="orientation" src="d0_s12" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s12" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o6215" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s12" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner straight or twisted, long, entire, flat or spine-tipped.</text>
      <biological_entity id="o6216" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="body" constraint_original="body; body">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s13" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6217" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <relation from="o6216" id="r595" name="part_of" negation="false" src="d0_s13" to="o6217" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas dark reddish purple, 17–24 mm, tubes 8–11 mm, throats 3.5–6 mm, lobes 5–7 mm;</text>
      <biological_entity id="o6218" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s14" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6219" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6220" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6221" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 3–4 mm.</text>
      <biological_entity constraint="style" id="o6222" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae dark-brown, 4–5 mm, apical collars narrow;</text>
      <biological_entity id="o6223" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6224" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 15 mm. 2n = 32.</text>
      <biological_entity id="o6225" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="15" value_original="15" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6226" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Headlands, ravines, seeps near coast, sometimes on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="headlands" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="coast" modifier="seeps near" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Franciscan thistle</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Cirsium andrewsii occurs along the coast of north-central California from San Mateo to Marin counties. It reportedly hybridizes with C. quercetorum (F. Petrak 1917; J. T. Howell 1960b).</discussion>
  
</bio:treatment>