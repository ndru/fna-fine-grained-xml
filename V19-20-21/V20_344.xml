<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Mackenzie) G. L. Nesom" date="1993" rank="subsection">Nemorales</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 8. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection Nemorales</taxon_hierarchy>
    <other_info_on_name type="fna_id">316971</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Mackenzie" date="unknown" rank="unranked">Nemorales</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Man. S.E. Fl.,</publication_title>
      <place_in_publication>1348. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;unranked Nemorales;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Incanae</taxon_name>
    <taxon_hierarchy>genus Solidago;unranked Incanae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Radulae</taxon_name>
    <taxon_hierarchy>genus Solidago;unranked Radulae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems and leaves usually moderately to densely strigoso-canescent.</text>
      <biological_entity id="o23376" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="strigoso-canescent" value_original="strigoso-canescent" />
      </biological_entity>
      <biological_entity id="o23377" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="strigoso-canescent" value_original="strigoso-canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal often present in rosettes at flowering, proximalmost cauline petiolate to subpetiolate, sometimes present at flowering, largest sometimes with two prominent lateral nerves (3-nerved).</text>
      <biological_entity id="o23378" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o23379" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="in rosettes" constraintid="o23380" is_modifier="false" modifier="often" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character constraint="with lateral nerves" constraintid="o23382" is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o23380" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity constraint="proximalmost cauline" id="o23381" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="petiolate" name="architecture" src="d0_s1" to="subpetiolate" />
        <character constraint="at flowering" is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23382" name="nerve" name_original="nerves" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o23380" id="r2157" name="at" negation="false" src="d0_s1" to="o23381" />
    </statement>
    <statement id="d0_s2">
      <text>Heads in variously thyrsiform-paniculiform, open to usually congested, cone-shaped, ± secund (to reflexed in S. nemoralis) arrays, proximal branches arching, secund;</text>
      <biological_entity id="o23383" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o23384" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="variously" name="architecture" src="d0_s2" value="thyrsiform-paniculiform" value_original="thyrsiform-paniculiform" />
        <character char_type="range_value" from="open" is_modifier="true" name="architecture" src="d0_s2" to="usually congested" />
        <character is_modifier="true" name="shape" src="d0_s2" value="cone--shaped" value_original="cone--shaped" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s2" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23385" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="secund" value_original="secund" />
      </biological_entity>
      <relation from="o23383" id="r2158" name="in" negation="false" src="d0_s2" to="o23384" />
    </statement>
    <statement id="d0_s3">
      <text>phyllaries 1-nerved, not stipitate-glandular, not striate.</text>
      <biological_entity id="o23386" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s3" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pappus bristles usually in 2 series (inner usually weakly to moderately clavate).</text>
      <biological_entity constraint="pappus" id="o23387" name="bristle" name_original="bristles" src="d0_s4" type="structure" />
      <biological_entity id="o23388" name="series" name_original="series" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o23387" id="r2159" name="in" negation="false" src="d0_s4" to="o23388" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>163a.11.</number>
  <discussion>Species 6 (5 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads secund, in rounded, secund, pseudo-corymbiform paniculiform arrays; oftenalkaline meadows and flats, Montana and Idaho s to New Mexico and Arizona</description>
      <determination>69 Solidago nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually in narrowly to broadly pyramidal, paniculiform arrays, branches secund</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants with short-branched caudices; arrays secund to apically recurved; prairies and open ground in e deciduous forests</description>
      <determination>67 Solidago nemoralis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants creeping-rhizomatous; heads in thyrsiform to secund-pyramidal, paniculiform arrays</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves finely scabrous, not or weakly 3-nerved; e United States</description>
      <determination>71 Solidago radula</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves coarsely scabrous, hispid or soft-canescent, sometimes strongly 3-nerved; prairies and w United States</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads in paniculiform arrays, usually compact, branches broadly thyrsiform to somewhat secund pyramidal, proximal branches reflexed-recurved distally,basal leaves withering by flowering; prairies of Great Plains</description>
      <determination>70 Solidago mollis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads in cone-shaped arrays with branches narrowly secund, or open, lax, pyramidal; basal leaves often present at flowering; from near sea to mid montane elevations, Wyoming to s Oregon, s to central Mexico</description>
      <determination>68 Solidago velutina</determination>
    </key_statement>
  </key>
</bio:treatment>