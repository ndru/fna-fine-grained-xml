<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">parryi</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">parryi</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species parryi;variety parryi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068306</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm.</text>
      <biological_entity id="o9874" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually crowded, green;</text>
      <biological_entity id="o9875" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 3-nerved (only midnerves prominent), linear, 30–80 × 2–3 mm, faces glabrous or puberulent, often minutely stipitate-glandular, ± resinous;</text>
      <biological_entity id="o9876" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9877" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="often minutely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="more or less" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distalmost overtopping arrays.</text>
      <biological_entity constraint="distalmost" id="o9878" name="blade" name_original="blades" src="d0_s3" type="structure" />
      <biological_entity id="o9879" name="array" name_original="arrays" src="d0_s3" type="structure" />
      <relation from="o9878" id="r916" name="overtopping" negation="false" src="d0_s3" to="o9879" />
    </statement>
    <statement id="d0_s4">
      <text>Heads usually 5–12+ in racemiform arrays.</text>
      <biological_entity id="o9880" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o9881" from="5" name="quantity" src="d0_s4" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9881" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 9–12 mm.</text>
      <biological_entity id="o9882" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 10–15, mostly chartaceous, outermost sometimes herbaceous-tipped, apices erect, attenuate.</text>
      <biological_entity id="o9883" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="15" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o9884" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="herbaceous-tipped" value_original="herbaceous-tipped" />
      </biological_entity>
      <biological_entity id="o9885" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 8–20;</text>
      <biological_entity id="o9886" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 7.9–10 mm, tubes sparsely hairy, throats gradually dilated, lobes 1.4–2.1 mm. 2n = 18.</text>
      <biological_entity id="o9887" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7.9" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9888" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9889" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o9890" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9891" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, dry hillsides and plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="dry hillsides" />
        <character name="habitat" value="plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27j.</number>
  <other_name type="common_name">Parry’s rabbitbrush</other_name>
  <discussion>Variety parryi is widespread in the Rocky Mountain region.</discussion>
  
</bio:treatment>