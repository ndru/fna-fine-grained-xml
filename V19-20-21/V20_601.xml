<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">pumilus</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 147. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species pumilus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066661</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–30 (–50) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices with relatively short and thick branches.</text>
      <biological_entity id="o1288" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o1289" name="caudex" name_original="caudices" src="d0_s1" type="structure" />
      <biological_entity id="o1290" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o1289" id="r115" name="with" negation="false" src="d0_s1" to="o1290" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, hirsute to hispido-hirsute (often with slightly deflexed hairs), minutely to stipitate-glandular.</text>
      <biological_entity id="o1291" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s2" to="hispido-hirsute" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline (petioles prominently ciliate, hairs thick-based, spreading);</text>
      <biological_entity id="o1292" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades oblanceolate to narrowly oblanceolate, 20–80 × 1–4 (–5) mm, margins entire, faces hispid to hispido-hirsute, little, if at all, glandular;</text>
      <biological_entity constraint="basal" id="o1293" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="narrowly oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1294" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1295" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s4" to="hispido-hirsute" />
        <character is_modifier="false" modifier="at all" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline on distal 1/2–3/4 of stems, blades becoming linear-lanceolate, little reduced distally.</text>
      <biological_entity constraint="cauline" id="o1296" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1297" name="1/2-3/4" name_original="1/2-3/4" src="d0_s5" type="structure" />
      <biological_entity id="o1298" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <biological_entity id="o1299" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o1296" id="r116" name="on" negation="false" src="d0_s5" to="o1297" />
      <relation from="o1297" id="r117" name="part_of" negation="false" src="d0_s5" to="o1298" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5 (–50).</text>
      <biological_entity id="o1300" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="50" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 4–7 × 7–15 mm.</text>
      <biological_entity id="o1301" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–4 series (midvein region orange to yellowish), hirsute to hispido-hirsute, minutely glandular.</text>
      <biological_entity id="o1302" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="hirsute" name="pubescence" notes="" src="d0_s8" to="hispido-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o1303" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o1302" id="r118" name="in" negation="false" src="d0_s8" to="o1303" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 50–100;</text>
      <biological_entity id="o1304" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white to pink, less commonly bluish, 6–15 mm, laminae reflexing.</text>
      <biological_entity id="o1305" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character is_modifier="false" modifier="less commonly" name="coloration" src="d0_s10" value="bluish" value_original="bluish" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1306" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3–5 mm (throats distinctly indurate and inflated, glabrous or sparsely puberulent with glandular-viscid, hairs blunt).</text>
      <biological_entity constraint="disc" id="o1307" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.4–1.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o1308" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o1309" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae or subulate scales (0.1–0.3 mm), inner of 12–27 bristles.</text>
      <biological_entity id="o1310" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o1311" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o1312" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1313" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity id="o1314" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s13" to="27" />
      </biological_entity>
      <relation from="o1310" id="r119" name="outer of" negation="false" src="d0_s13" to="o1311" />
      <relation from="o1310" id="r120" name="outer of" negation="false" src="d0_s13" to="o1312" />
      <relation from="o1313" id="r121" name="consist_of" negation="false" src="d0_s13" to="o1314" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Calif., Colo., Idaho, Kans., Mont., N.Dak., Nebr., Nev., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="past_name">pumilum</other_name>
  <other_name type="common_name">Shaggy fleabane</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi: outer of coarse bristles or setiform scales, inner of 12–20 bristles; rays usually pink, sometimes white; indurate portion of disc corollas relatively dull, glabrous or slightly puberulent</description>
      <determination>29a Erigeron pumilus var. intermedius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi: outer of inconspicuous, well-developed setae, inner of 15–27 bristles; rays white; indurate portion of disc corollas shiny, glabrous</description>
      <determination>29b Erigeron pumilus var. pumilus</determination>
    </key_statement>
  </key>
</bio:treatment>