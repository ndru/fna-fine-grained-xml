<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Cassini" date="1822" rank="genus">logfia</taxon_name>
    <taxon_name authority="(Linnaeus) Holub" date="1975" rank="species">arvensis</taxon_name>
    <place_of_publication>
      <publication_title>Notes Roy. Bot. Gard. Edinburgh</publication_title>
      <place_in_publication>33: 432. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus logfia;species arvensis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242426803</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Filago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arvensis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: add. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Filago;species arvensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oglifa</taxon_name>
    <taxon_name authority="(Linnaeus) Cassini" date="unknown" rank="species">arvensis</taxon_name>
    <taxon_hierarchy>genus Oglifa;species arvensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–50 [–70] cm.</text>
      <biological_entity id="o19133" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect;</text>
      <biological_entity id="o19134" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches leafy between proximal forks, remaining grayish to whitish, lanuginose to sericeous.</text>
      <biological_entity id="o19135" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character constraint="between proximal forks" constraintid="o19136" is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="grayish" name="coloration" notes="" src="d0_s2" to="whitish" />
        <character char_type="range_value" from="lanuginose" name="pubescence" src="d0_s2" to="sericeous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19136" name="fork" name_original="forks" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves oblanceolate to lanceolate, largest 14–20 (–40) × 3–4 (–5) mm, pliant;</text>
      <biological_entity id="o19137" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="fragility_or_texture" src="d0_s3" value="pliant" value_original="pliant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>longest capitular leaves 0.8–1.5 (–2) times head heights, acute.</text>
      <biological_entity constraint="longest capitular" id="o19138" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="head" constraintid="o19139" is_modifier="false" name="height" src="d0_s4" value="0.8-1.5(-2) times head heights" value_original="0.8-1.5(-2) times head heights" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o19139" name="head" name_original="head" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Heads mostly in glomerules of 2–10 (–13) in racemiform to paniculiform arrays, broadly pyriform to ± cylindric, largest 4–6 × 3.5–5 mm.</text>
      <biological_entity id="o19140" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly pyriform" name="shape" notes="" src="d0_s5" to="more or less cylindric" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19141" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" modifier="of" name="atypical_quantity" src="d0_s5" to="13" />
        <character char_type="range_value" constraint="in arrays" constraintid="o19142" from="2" modifier="of" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o19142" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
      <relation from="o19140" id="r1725" name="in" negation="false" src="d0_s5" to="o19141" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 0, vestigial, or 1–4, unequal, ± like paleae.</text>
      <biological_entity id="o19143" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="vestigial" value_original="vestigial" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o19144" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <relation from="o19143" id="r1726" modifier="more or less" name="like" negation="false" src="d0_s6" to="o19144" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles ± fungiform, 0.4–0.7 mm, heights 0.4–0.5 times diams.</text>
      <biological_entity id="o19145" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="fungiform" value_original="fungiform" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="height" src="d0_s7" value="0.4-0.5 times diams" value_original="0.4-0.5 times diams" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pistillate paleae (except innermost) 2–4 (–6) in 1 (–2) series, spirally ranked, loosely saccate, incurved 20–60°, scarcely gibbous, not galeate, longest 3.3–4.5 mm, distal 5–10% of lengths glabrous abaxially;</text>
      <biological_entity id="o19146" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character char_type="range_value" constraint="in series" constraintid="o19147" from="2" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" modifier="spirally" name="arrangement" notes="" src="d0_s8" value="ranked" value_original="ranked" />
        <character is_modifier="false" modifier="loosely" name="architecture_or_shape" src="d0_s8" value="saccate" value_original="saccate" />
        <character is_modifier="false" modifier="20-60°" name="orientation" src="d0_s8" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="galeate" value_original="galeate" />
        <character is_modifier="false" name="length" src="d0_s8" value="longest" value_original="longest" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="5-10%" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character constraint="of lengths" is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19147" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bodies ± cartilaginous, ± terete;</text>
      <biological_entity id="o19148" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_texture" src="d0_s9" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>wings obscured by indument.</text>
      <biological_entity id="o19149" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character constraint="by indument" constraintid="o19150" is_modifier="false" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o19150" name="indument" name_original="indument" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Innermost paleae ± 8, spreading in 2 series, pistillate.</text>
      <biological_entity constraint="innermost" id="o19151" name="palea" name_original="paleae" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
        <character constraint="in series" constraintid="o19152" is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19152" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate florets: outer 2–4 (–6) epappose, inner 15–20 pappose.</text>
      <biological_entity id="o19153" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o19154" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epappose" value_original="epappose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19155" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pappose" value_original="pappose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Bisexual florets 3–4;</text>
      <biological_entity id="o19156" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.3–3 mm, lobes mostly 4, reddish to purplish.</text>
      <biological_entity id="o19157" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19158" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s14" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae: outer nearly straight, ± erect, compressed, 0.9–1.1 mm;</text>
      <biological_entity id="o19159" name="cypsela" name_original="cypselae" src="d0_s15" type="structure" />
      <biological_entity constraint="outer" id="o19160" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>inner papillate;</text>
      <biological_entity id="o19161" name="cypsela" name_original="cypselae" src="d0_s16" type="structure" />
      <biological_entity constraint="inner" id="o19162" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 17–23 bristles falling in complete or partial rings, 2.5–3.5 mm. 2n = 28 (Caucasus, Finland, Germany, Slovakia).</text>
      <biological_entity id="o19163" name="cypsela" name_original="cypselae" src="d0_s17" type="structure" />
      <biological_entity id="o19164" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19165" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="17" is_modifier="true" name="quantity" src="d0_s17" to="23" />
        <character constraint="in rings" constraintid="o19166" is_modifier="false" name="life_cycle" src="d0_s17" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o19166" name="ring" name_original="rings" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="complete" value_original="complete" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="partial" value_original="partial" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19167" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
      <relation from="o19164" id="r1727" name="consist_of" negation="false" src="d0_s17" to="o19165" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid Jun–mid Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Sep" from="mid Jun" />
        <character name="fruiting time" char_type="range_value" to="mid Sep" from="mid Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, sandy to gravelly soils, disturbed sites (road and ditch banks, lakeshores, clear cuts, old fields, dwellings, grazed lands)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="road" />
        <character name="habitat" value="ditch banks" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="clear cuts" />
        <character name="habitat" value="open" />
        <character name="habitat" value="sandy to gravelly soils" />
        <character name="habitat" value="disturbed sites ( road" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="dwellings" />
        <character name="habitat" value="lands" modifier="grazed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Sask.; Alaska, Idaho, Mich., Minn., Mont., Nebr., Oreg., S.Dak., Wash., Wyo.; Eurasia; nw Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="nw Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Field cottonrose</other_name>
  <other_name type="common_name">cotonnière des champs</other_name>
  <discussion>Logfia arvensis appears to be basal or nearly so in Logfia and Filagininae (J. D. Morefield 1992); only 2–4 epappose florets are present in most heads. Reports of L. arvensis from Ontario and New York have not been confirmed by me. A report from the Desert National Wildlife Range in southern Nevada (T. L. Ackerman et al. 2003) was likely based on specimens of L. filaginoides. The earliest specimen confirmed from the flora area was from Bonner County, Idaho, in 1934. The label on one nineteenth-century specimen (mixed with Diaperia verna) identifying it as coming from Dallas, Texas, is probably in error; no other collections of L. arvensis are known from in or near Texas.</discussion>
  
</bio:treatment>