<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="treatment_page">159</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Heiser" date="1964" rank="species">smithii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>66: 346. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species smithii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066902</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="Bernhardi ex Sprengel [not Kunth]" date="unknown" rank="species">parviflorus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">attenuatus</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 278. 1884,</place_in_publication>
      <other_info_on_pub>not H. attenuatus E. Watson 1929</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthus;species parviflorus;variety attenuatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 70–150 cm (rhizomatous).</text>
      <biological_entity id="o12898" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (usually purplish) erect, glabrous (glaucous).</text>
      <biological_entity id="o12899" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or alternate;</text>
      <biological_entity id="o12900" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles to 1.3 cm;</text>
      <biological_entity id="o12901" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (light to dark green, 1-nerved) lance-linear to lanceolate, 7–18 × 1–1.8 cm, bases ± cuneate, margins serrulate, abaxial faces ± hispid, glanddotted.</text>
      <biological_entity id="o12902" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lance-linear" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="1.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12903" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12904" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12905" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5.</text>
      <biological_entity id="o12906" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 3–6 cm.</text>
      <biological_entity id="o12907" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 5–15 mm diam.</text>
      <biological_entity id="o12908" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 15–20, lanceolate, 6–9 × 2–2.7 mm, (margins ciliate) apices short-acuminate, abaxial faces glabrous or glabrate, not glanddotted.</text>
      <biological_entity id="o12909" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="20" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12910" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12911" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 5.5–6.5 mm, 3-toothed (glanddotted).</text>
      <biological_entity id="o12912" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 6–9;</text>
      <biological_entity id="o12913" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 10–15 mm.</text>
      <biological_entity id="o12914" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25+;</text>
      <biological_entity id="o12915" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 5.5–6 mm, lobes yellow;</text>
      <biological_entity id="o12916" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12917" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark, appendages dark.</text>
      <biological_entity id="o12918" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o12919" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 2.8–3 mm, glabrate;</text>
      <biological_entity id="o12920" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 aristate scales 1.5–2 mm. 2n = 68.</text>
      <biological_entity id="o12921" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o12922" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12923" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="68" value_original="68" />
      </biological_entity>
      <relation from="o12921" id="r883" name="consist_of" negation="false" src="d0_s17" to="o12922" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300+" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Smith’s sunflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Helianthus smithii is known from relatively few sites. T. A. Storbeck (1984) concluded that it is a distinct species; he could not rule out the possibility that it might be a hybrid of H. microcephalus and H. strumosus. It may also be an extreme variant of H. strumosus that has relatively narrow leaves and small heads.</discussion>
  
</bio:treatment>