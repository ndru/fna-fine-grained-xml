<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">pycnostachya</taxon_name>
    <taxon_name authority="Shinners" date="1951" rank="variety">lasiophylla</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>19: 74. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species pycnostachya;variety lasiophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068568</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">serotina</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species serotina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Greene) K. Schumann" date="unknown" rank="species">serotina</taxon_name>
    <taxon_hierarchy>genus Liatris;species serotina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Corms globose, sometimes becoming elongate rhizomes.</text>
      <biological_entity id="o19774" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o19775" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="sometimes becoming" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems moderately to densely piloso-puberulent.</text>
      <biological_entity id="o19776" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s1" value="piloso-puberulent" value_original="piloso-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves moderately to densely piloso-puberulent to nearly glabrous.</text>
      <biological_entity id="o19777" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="piloso-puberulent" modifier="densely" name="pubescence" src="d0_s2" to="nearly glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jul–Sep(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="mid Jul" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy woods, pitcher plant and grass bogs, pine savannas, drainages, roadsides, fencerows, sands, sandy clays, clays</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy woods" />
        <character name="habitat" value="pitcher plant" />
        <character name="habitat" value="grass bogs" />
        <character name="habitat" value="savannas" modifier="pine" />
        <character name="habitat" value="drainages" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="sands" />
        <character name="habitat" value="sandy clays" />
        <character name="habitat" value="clays" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15b.</number>
  <discussion>Variety lasiophylla occurs over most of the range of the species in Louisiana and Texas; var. pycnostachya occurs in the northern counties of those states and, apparently, also sporadically southward through the range of var. lasiophylla, at least in Texas, where it grows in drier habitats than var. lasiophylla. In Louisiana and Texas, var. pycnostachya begins flowering in mid-June and continues through July (through August more northward in its range), usually well before the main flowering period of var. lasiophylla. Despite these indications of reproductive isolation, apparent intermediates are commonly encountered (usually these are plants with dense cauline vestiture but sparsely pubescent to glabrate leaves, compared to the densely piloso-puberulent stems and leaves of typical var. lasiophylla). Most of the plants with reduced vestiture in the range of var. lasiophylla also have the later flowering period.</discussion>
  <discussion>In Pearl River and Hancock counties, Mississippi, and St. Tammany and Washington parishes, Louisiana, plants with vestiture of var. lasiophylla have phyllaries with rounded to slightly acute apices, different from the rest of the species. These were named Liatris serotina, and it seems likely that they originated as hybrids between var. lasiophylla and L. spicata var. resinosa, whose ranges meet in that area. Some plants of L. spicata in Illinois, Indiana, and Minnesota also develop cauline vestiture; the tendency apparently is evolutionarily independent of that in var. lasiophylla.</discussion>
  
</bio:treatment>