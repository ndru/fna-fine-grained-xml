<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Linnaeus) Á. Löve &amp; D. Löve" date="1982" rank="species">puniceum</taxon_name>
    <taxon_name authority="(Shinners) G. L. Nesom" date="1995" rank="variety">scabricaule</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 290. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species puniceum;variety scabricaule</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068858</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="species">scabricaulis</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>21: 156. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species scabricaulis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">puniceus</taxon_name>
    <taxon_name authority="(Shinners) A. G. Jones" date="unknown" rank="variety">scabricaulis</taxon_name>
    <taxon_hierarchy>genus Aster;species puniceus;variety scabricaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 100–250 (–300) cm.</text>
      <biological_entity id="o30018" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="250" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually densely and uniformly hirsute, sometimes less so and in lines distally.</text>
      <biological_entity id="o30019" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="uniformly" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o30020" name="line" name_original="lines" src="d0_s1" type="structure" />
      <relation from="o30019" id="r2779" modifier="sometimes less; less" name="in" negation="false" src="d0_s1" to="o30020" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: faces ± concolorous, without dark, distinct reticulum, adaxial with distinctly impressed main veins (giving rough appearance);</text>
      <biological_entity id="o30021" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o30022" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s2" value="concolorous" value_original="concolorous" />
      </biological_entity>
      <biological_entity id="o30023" name="reticulum" name_original="reticulum" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="dark" value_original="dark" />
        <character is_modifier="true" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30024" name="face" name_original="faces" src="d0_s2" type="structure" />
      <biological_entity constraint="main" id="o30025" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="distinctly" name="prominence" src="d0_s2" value="impressed" value_original="impressed" />
      </biological_entity>
      <relation from="o30022" id="r2780" name="without" negation="false" src="d0_s2" to="o30023" />
      <relation from="o30024" id="r2781" name="with" negation="false" src="d0_s2" to="o30025" />
    </statement>
    <statement id="d0_s3">
      <text>array leaves reduced in size relative to mid cauline.</text>
      <biological_entity id="o30026" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid cauline" id="o30028" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 16.</text>
      <biological_entity id="o30027" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="to mid cauline leaves" constraintid="o30028" is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30029" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open seepage sites with mucky, saturated soils, wet, seepy grounds in sandy pinelands, sphagnum bogs, shrubby seepage bogs, marshes, pond margins, open stream banks, roadside and drainage ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepage sites" modifier="open" constraint="with mucky" />
        <character name="habitat" value="mucky" modifier="with" />
        <character name="habitat" value="soils" modifier="saturated" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="seepy grounds" constraint="in sandy pinelands" />
        <character name="habitat" value="sandy pinelands" />
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="shrubby seepage bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="open stream banks" />
        <character name="habitat" value="roadside and drainage ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>56b.</number>
  <other_name type="common_name">Roughstem aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety scabricaule is known from the post-oak belt of eastern Texas and in Louisiana (Natchitoches Parish), Mississippi (Grenada and Lauderdale counties), and Alabama (Penton, Chambers County, Kral 37887, BRIT). It is considered of conservation concern in Texas, where most populations occur. Variety scabricaule is in the Center for Plant Conservation’s National Collection of Endangered Plants. The hybrid Symphyotrichum puniceum var. scabricaule × S. lateriflorum was reported by G. L. Nesom (1997b).</discussion>
  
</bio:treatment>