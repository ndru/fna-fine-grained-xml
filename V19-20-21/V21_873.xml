<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">349</other_info_on_meta>
    <other_info_on_meta type="illustration_page">349</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="genus">eatonella</taxon_name>
    <taxon_name authority="(D. C. Eaton) A. Gray" date="1883" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 19. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eatonella;species nivea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220004538</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Burrielia</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>174, plate 18, figs. 6–14. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Burrielia;species nivea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually crowded in tufts.</text>
      <biological_entity id="o25187" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="in tufts" constraintid="o25188" is_modifier="false" modifier="usually" name="arrangement" src="d0_s0" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o25188" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 10–2 (0–30) × 3–8 (–12) mm.</text>
      <biological_entity id="o25189" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s1" to="30" to_unit="mm" />
        <character name="length" src="d0_s1" unit="mm" value="" value_original="" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 1–35+ mm.</text>
      <biological_entity id="o25190" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="35" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 4–6+ mm.</text>
      <biological_entity id="o25191" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray corollas inconspicuous, laminae ± erect, 2–2.5 mm.</text>
      <biological_entity constraint="ray" id="o25192" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o25193" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 1.5–3 mm.</text>
      <biological_entity constraint="disc" id="o25194" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae black, 2.5–3.5 mm, hairs on margins 0.6–1.2 mm;</text>
      <biological_entity id="o25195" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="black" value_original="black" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25196" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <biological_entity id="o25197" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
      <relation from="o25196" id="r1711" name="on" negation="false" src="d0_s6" to="o25197" />
    </statement>
    <statement id="d0_s7">
      <text>pappus-scales 1.5–2.5 mm. 2n = 38.</text>
      <biological_entity id="o25198" name="pappus-scale" name_original="pappus-scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25199" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, often with sagebrush scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="sagebrush scrub" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>