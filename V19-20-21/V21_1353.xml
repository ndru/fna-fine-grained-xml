<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Linnaeus) Willdenow" date="1803" rank="species">scariosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 1635. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species scariosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416765</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Serratula</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">scariosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 818. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Serratula;species scariosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="(Linnaeus) Hill" date="unknown" rank="species">scariosa</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species scariosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–150 cm.</text>
      <biological_entity id="o23367" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms subglobose.</text>
      <biological_entity id="o23368" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems puberulent (at least distally, sometimes glabrous proximally).</text>
      <biological_entity id="o23369" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 1-nerved, elliptic to oblanceolate-spatulate, 120–300 × 25–50 (–55) mm, abruptly, gradually, or little reduced distally, essentially glabrous, weakly, if at all, glanddotted.</text>
      <biological_entity id="o23370" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblanceolate-spatulate" />
        <character char_type="range_value" from="120" from_unit="mm" name="length" src="d0_s3" to="300" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="55" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in racemiform arrays.</text>
      <biological_entity id="o23371" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o23372" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o23371" id="r1600" name="in" negation="false" src="d0_s4" to="o23372" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles usually (ascending) 10–50 mm.</text>
      <biological_entity id="o23373" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to turbinate-campanulate, 11–15 × (12–) 15–22 (–25) mm.</text>
      <biological_entity id="o23374" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="turbinate-campanulate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_width" src="d0_s6" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in (3–) 4–5 series, (erect or outer sometimes spreading-reflexing) oblong-obovate to broadly obovate (not bullate), unequal, glabrous or minutely puberulent, margins usually with relatively narrow, hyaline borders, apices broadly rounded.</text>
      <biological_entity id="o23375" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" notes="" src="d0_s7" to="broadly obovate" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23376" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o23377" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o23378" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="relatively" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o23379" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o23375" id="r1601" name="in" negation="false" src="d0_s7" to="o23376" />
      <relation from="o23377" id="r1602" name="with" negation="false" src="d0_s7" to="o23378" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 19–80;</text>
      <biological_entity id="o23380" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s8" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes usually pilose inside, sometimes glabrous (Arkansas, Illinois, Missouri).</text>
      <biological_entity id="o23381" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4.5–6 (–6.5) mm;</text>
      <biological_entity id="o23382" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles barbellate.</text>
      <biological_entity id="o23383" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o23384" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o23385" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Conn., Ill., Ind., Maine, Mass., Md., Mich., Mo., N.C., N.H., N.J., N.Y., Ohio, Pa., R.I., Tenn., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <other_name type="common_name">Northern gayfeather</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with 8–20(–25) leaves or leafy bracts proximal to heads; florets 20–40(–50)</description>
      <determination>35a Liatris scariosa var. scariosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with 20–85 leaves or leafy bracts proximal to heads; florets 30–80.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal cauline leaves mostly 25–50(–55) mm wide, hirtello-puberulent (and gland-dotted)</description>
      <determination>35b Liatris scariosa var. nieuwlandii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal cauline leaves mostly 7–20(–26) mm wide, glabrous or hirtello-puberulent (weakly, if at all, gland-dotted)</description>
      <determination>35c Liatris scariosa var. novaeangliae</determination>
    </key_statement>
  </key>
</bio:treatment>