<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">stephanomeria</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">lactucina</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 552. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus stephanomeria;species lactucina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067603</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–60 cm (rhizomes slender).</text>
      <biological_entity id="o2421" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single, branches erect or ascending (from near bases), glabrous or sparsely puberulent.</text>
      <biological_entity id="o2422" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o2423" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green at flowering;</text>
      <biological_entity id="o2424" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear-lanceolate, 3–8 cm, margins entire or toothed (teeth remote, faces glabrous or sparsely puberulent).</text>
      <biological_entity id="o2425" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2426" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly along branches.</text>
      <biological_entity id="o2427" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o2428" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o2427" id="r233" name="borne" negation="false" src="d0_s4" to="o2428" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 10–50 mm (bracteate).</text>
      <biological_entity id="o2429" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of (4–7) appressed bractlets (unequal, lengths to 1/2 phyllaries).</text>
      <biological_entity id="o2430" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o2431" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s6" to="7" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o2430" id="r234" name="consist_of" negation="false" src="d0_s6" to="o2431" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres 12–14 mm (phyllaries 8–12, glabrous).</text>
      <biological_entity id="o2432" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets (7–) 8–10.</text>
      <biological_entity id="o2433" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae light tan, 5–6 mm, faces smooth, grooved;</text>
      <biological_entity id="o2434" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light tan" value_original="light tan" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2435" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 25–30, light tan bristles (sometimes connate in groups of 5–6+, persistent), wholly plumose.</text>
      <biological_entity id="o2437" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s10" to="30" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="light tan" value_original="light tan" />
      </biological_entity>
      <relation from="o2436" id="r235" name="consist_of" negation="false" src="d0_s10" to="o2437" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 16.</text>
      <biological_entity id="o2436" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="wholly" name="shape" notes="" src="d0_s10" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2438" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils in dry, open yellow pine and red fir forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="in dry , open yellow pine and red fir forests" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="open yellow pine" />
        <character name="habitat" value="red fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Woodland wirelettuce</other_name>
  <discussion>Stephanomeria lactucina grows in the Cascade Mountains, Sierra Nevada, and North Coast Ranges. It rarely sets seed. The absence of seed is associated with reduced pollen viability, first reported by A. S. Tomb et al. (1978), who noted that “plants [from Siskiyou County, California] were seed-sterile and produced only 45% stainable pollen.” The ability of pollen to take up acetocarmine dye is a measure of their viability and, consequently, the fertility of the plant. I examined pollen from five randomly selected specimens collected from Oregon and California and found from 2% to 98% stainable pollen, with a mean of 47%. Pollen stainability and seed set have not been studied systematically in this species. Because these traits directly influence fitness and population persistence, further study would likely be rewarding.</discussion>
  
</bio:treatment>