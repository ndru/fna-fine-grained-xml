<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Bogler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="treatment_page">264</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PRENANTHES</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 797. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 349. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus PRENANTHES</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek prenes, drooping, and anthos, flower, alluding to drooping heads</other_info_on_name>
    <other_info_on_name type="fna_id">126693</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–250 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, often producing offshoots connected by slender rhizomes.</text>
      <biological_entity id="o17797" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o17798" name="offshoot" name_original="offshoots" src="d0_s1" type="structure" />
      <biological_entity constraint="slender" id="o17799" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="fusion" src="d0_s1" value="connected" value_original="connected" />
      </biological_entity>
      <relation from="o17797" id="r1599" modifier="often" name="producing" negation="false" src="d0_s1" to="o17798" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5, usually erect, sometimes decumbent (P. bootii), usually simple (leafy), usually glabrous proximally, tomentulose distally.</text>
      <biological_entity id="o17800" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="usually; proximally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o17801" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades deltate to triangular, or ovate to oblanceolate, or oblong to linear, or spatulate, margins often pinnately or palmately lobed (sometimes deeply cleft and appearing compound), ultimate margins entire or coarsely serrate or dentate (apices acute, obtuse, or rounded);</text>
      <biological_entity id="o17802" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="triangular or ovate" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="triangular or ovate" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="triangular or ovate" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="triangular or ovate" />
      </biological_entity>
      <biological_entity id="o17803" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17804" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal leaves reduced in size and lobing.</text>
      <biological_entity constraint="distal" id="o17805" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lobing" value_original="lobing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (usually nodding at flowering) in racemiform, paniculiform, thyrsiform, or corymbiform arrays.</text>
      <biological_entity id="o17806" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o17807" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="thyrsiform" value_original="thyrsiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o17806" id="r1600" name="in" negation="false" src="d0_s7" to="o17807" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles not inflated distally, bracteate.</text>
      <biological_entity id="o17808" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyculi of 2–12, triangular to linear-lanceolate or subulate, unequal bractlets.</text>
      <biological_entity id="o17809" name="calyculus" name_original="calyculi" src="d0_s9" type="structure" />
      <biological_entity id="o17810" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="12" />
        <character char_type="range_value" from="triangular" is_modifier="true" name="shape" src="d0_s9" to="linear-lanceolate or subulate" />
        <character is_modifier="true" name="size" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o17809" id="r1601" name="consist_of" negation="false" src="d0_s9" to="o17810" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres narrowly cylindric to campanulate (bases often attenuate), 2–14 mm diam.</text>
      <biological_entity id="o17811" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly cylindric" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 3–15 in 1 series, (yellow green or green to purple or blackish) subulate or linear to lanceolate or elliptic, equal, margins scarious, apices acute, faces glabrous, tomentulose, hispid, or coarsely setose.</text>
      <biological_entity id="o17812" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17813" from="3" name="quantity" src="d0_s11" to="15" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s11" to="lanceolate or elliptic" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17813" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17814" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o17815" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o17816" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s11" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s11" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles slightly convex, smooth, glabrous, epaleate.</text>
      <biological_entity id="o17817" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Florets 4–38;</text>
      <biological_entity id="o17818" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="38" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas usually creamy white, pink, or lavender, rarely yellow or red (glabrous).</text>
      <biological_entity id="o17819" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae golden brown to light tan, narrowly subcylindric, or fusiform to oblanceoloid, or oblong to linear, subterete or angled, apices truncate, not beaked, faces finely 5–12-ribbed, usually glabrous;</text>
      <biological_entity id="o17820" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="golden brown" name="coloration" src="d0_s15" to="light tan" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="oblanceoloid or oblong" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="oblanceoloid or oblong" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="oblanceoloid or oblong" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="oblanceoloid or oblong" />
      </biological_entity>
      <biological_entity id="o17821" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o17822" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s15" value="5-12-ribbed" value_original="5-12-ribbed" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 30–50, dull white to yellow or tan, rarely reddish-brown, ± equal, barbellulate bristles in 1 series.</text>
      <biological_entity id="o17823" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="of 30-50" name="coloration" src="d0_s16" value="dull" value_original="dull" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="yellow or tan" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s16" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s16" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17825" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17824" id="r1602" name="in" negation="false" src="d0_s16" to="o17825" />
    </statement>
    <statement id="d0_s17">
      <text>x = 8.</text>
      <biological_entity id="o17824" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o17826" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Asia, sc Africa (1 species).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
        <character name="distribution" value="sc Africa (1 species)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>46.</number>
  <other_name type="past_name">Prenantes</other_name>
  <other_name type="common_name">Rattlesnakeroot</other_name>
  <other_name type="common_name">cankerweed</other_name>
  <other_name type="common_name">gall-of-the-earth</other_name>
  <other_name type="common_name">prenanthe</other_name>
  <discussion>Species ca. 26–30 (14 in the flora).</discussion>
  <discussion>Prenanthes is recognized by the erect and simple habit, deeply divided proximal leaves, whitish, yellow, or pinkish corollas in nodding heads, calyculate involucres, and pappi of barbellulate bristles. Leaf shape, size, and degree of lobing are often used for distinguishing species but are sometimes exceptionally variable. The proximal leaves are usually different in size, shape, and lobing from the distal leaves. Other taxonomic characters include size and habit, corolla color, number of florets per head, and phyllary number, color, and indument. The cypselae and pappi tend to be uniform.</discussion>
  <discussion>Molecular ITS studies by S. C. Kim et al. (1996) suggested that Prenanthes, as here circumscribed, may be polyphyletic; additional sampling including North American taxa is needed to confirm the relationships of Prenanthes and recognition of Nabalus Cassini at the genus level.</discussion>
  <references>
    <reference>Johnson, M. F. 1979. The genus Prenanthes L. (Cichorieae–Asteraceae) in Virginia. Castanea 45: 24–30.</reference>
    <reference>Milstead, W. L. 1964. A Revision of the North American Species of Prenanthes. Ph.D. dissertation. Purdue University.</reference>
    <reference>Singhurst, J. R., R. J. O’Kennon, and W. C. Holmes. 2004. The genus Prenanthes (Asteraceae: Lactuceae) in Texas. Sida 21: 181–191.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries glabrous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries tomentulose, hispid, or coarsely setose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Florets and phyllaries (4–)5(–6)</description>
      <determination>3 Prenanthes altissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Florets and phyllaries 6–20</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads in elongate, slender, spikelike, narrowly racemiform arrays; proximal leaves oblong to linear, pinnately lobed, lobes narrow and at right angles; corollas pinkish.</description>
      <determination>5 Prenanthes autumnalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads in racemiform, paniculiform, or narrowly thyrsiform arrays; proximal leaves ovate to deltate, palmately lobed, irregularly dentate or entire; corollas whitish, pinkish, or pale yellow</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries ± purplish or maroon (dark on old specimens); corollas whitish to pinkish; pappi usually reddish brown (rarely yellowish)</description>
      <determination>2 Prenanthes alba</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries green to tan, dark green, or blackish; corollas white or pale yellow; pappi pale yellow</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal leaves palmately 3(–5)-lobed (often deeply cleft, compound, lobes lanceolate, sinuses angular); involucres campanulate (attenuate at bases); calyculus bractlets triangular; corollas pale yellow</description>
      <determination>14 Prenanthes trifoliolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal leaves entire or shallowly lobed (bases hastate or sagittate to truncate or rounded); involucres cylindro-campanulate or narrowly campanulate (rounded, not attentuate at bases); calyculus bractlets subulate to narrowly lanceolate; corollas white</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants 5–25 cm; stems decumbent; alpine regions, n Appalachians.</description>
      <determination>7 Prenanthes boottii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants 8–75 cm; stems erect; n Rocky Mountains</description>
      <determination>12 Prenanthes sagittata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal leaves petiolate, petioles ± winged, blades spatulate, usually unlobed, apices usually rounded or obtuse; mid cauline leaves sessile, often clasping; heads ± ascending</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal leaves petiolate, petioles winged, blades deltate or triangular to elliptic or ovate (not spatulate), sometimes lobed, apices acute or obtuse; mid cauline leaves sessile or petiolate; heads nodding</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Midstems roughly hispid or coarsely setose; proximalmost leaves withered or deciduous by flowering; leaves ± hispid abaxially; corollas pale yellow to creamy white</description>
      <determination>4 Prenanthes aspera</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Midstems glabrous; proximalmost leaves persistent to flowering; leaves glabrous; corollas usually pinkish (sometimes white or lavender)</description>
      <determination>10 Prenanthes racemosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Proximal leaves sagittate, lobed; phyllaries hispid or tomentulose</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Proximal leaves deltate, ovate, or elliptic, lobed or unlobed; phyllaries coarsely setose</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants 80–150 cm; proximal leaves sagittate, bases sagittate; phyllaries hispid along midveins; Edwards Plateau, Texas</description>
      <determination>8 Prenanthes carrii</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants 15–80 cm; proximal leaves triangular or irregularly elliptic, bases truncate or hastate; phyllaries finely tomentulose; Pacific Northwest to Alaska</description>
      <determination>1 Prenanthes alata</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Proximal leaves deltate, ovate, or hastate, margins entire or coarsely serrate; phyllaries (9–)12–15, dark green to blackish; florets 15–38</description>
      <determination>9 Prenanthes crepidinea</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Proximal leaves deltate to ovate or elliptic, margins palmately or pinnately lobed or unlobed, dentate or entire; phyllaries 5–10, green or purple to lavender; florets 5–15</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Proximal leaves coarsely dentate or serrate (unlobed); phyllaries purple or lavender</description>
      <determination>6 Prenanthes barbata</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Proximal leaves 3–5-lobed; phyllaries usually green (sometimes purple in P. serpentaria)</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves about twice as long as wide, pinnately lobed, lobes and sinuses rounded; heads in broad paniculiform or corymbiform arrays, at least some branches elongate; phyllaries green or often purple, with appressed, green or tan, coarse setae, sometimes reduced to a single seta per phyllary; florets 8–14; widespread in e, s United States.</description>
      <determination>13 Prenanthes serpentaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves about as long as wide, palmately lobed, lobes angular; heads and sinuses angular; heads in narrow paniculiform or thyrsiform arrays, branches short; phyllaries dark green, with erect, coarse setae; florets 5–7(–13); mid to high elevations in Blue Ridge Mountains of North Carolina, Tennessee, Virginia</description>
      <determination>11 Prenanthes roanensis</determination>
    </key_statement>
  </key>
</bio:treatment>