<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">Hymenopappinae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 43. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe Hymenopappinae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20727</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, (5–) 20–150 cm.</text>
      <biological_entity id="o12437" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal or basal and cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o12440" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades deltate to lanceolate overall, usually 1–2-pinnately or palmati-pinnately lobed, lobes usually filiform, ultimate margins entire or toothed, faces glabrous or hairy, often tomentose, usually glanddotted.</text>
      <biological_entity id="o12441" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s4" to="lanceolate" />
        <character is_modifier="false" modifier="usually 1-2-pinnately; 1-2-pinnately; palmati-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o12442" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12443" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o12444" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually discoid, rarely radiate, usually in ± corymbiform arrays.</text>
      <biological_entity id="o12445" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_arrangement" src="d0_s5" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o12446" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o12445" id="r856" modifier="usually" name="in" negation="false" src="d0_s5" to="o12446" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi 0.</text>
      <biological_entity id="o12447" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic to hemispheric.</text>
      <biological_entity id="o12448" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s7" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent or tardily falling, 5–13+ in 2+ series (distinct, often yellowish, whitish, or purplish, orbiculate to lance-linear, subequal, often ± petaloid, margins usually notably membranous or scarious).</text>
      <biological_entity id="o12449" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o12450" from="5" name="quantity" src="d0_s8" to="13" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12450" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat or convex, usually epaleate (paleae scarious, conduplicate, each ± investing subtended floret in H. newberryi).</text>
      <biological_entity id="o12451" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets usually 0, sometimes 8, pistillate, fertile;</text>
      <biological_entity id="o12452" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character modifier="sometimes" name="quantity" src="d0_s10" value="8" value_original="8" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white or whitish.</text>
      <biological_entity id="o12453" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 12–70+, bisexual, fertile;</text>
      <biological_entity id="o12454" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s12" to="70" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow or whitish to purplish, tubes longer than or about equaling funnelform or abruptly dilated, campanulate throats, lobes 5, ± deltate (reflexed at anthesis);</text>
      <biological_entity id="o12455" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s13" to="purplish" />
      </biological_entity>
      <biological_entity id="o12456" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than or about equaling funnelform or abruptly dilated , campanulate throats" constraintid="o12457" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12457" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" modifier="abruptly" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o12458" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anther thecae pale;</text>
      <biological_entity constraint="anther" id="o12459" name="theca" name_original="thecae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmatic papillae in 2 lines.</text>
      <biological_entity constraint="stigmatic" id="o12460" name="papilla" name_original="papillae" src="d0_s15" type="structure" />
      <biological_entity id="o12461" name="line" name_original="lines" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <relation from="o12460" id="r857" name="in" negation="false" src="d0_s15" to="o12461" />
    </statement>
    <statement id="d0_s16">
      <text>Cypselae obconic to obpyramidal, usually 4-angled and 12–16-ribbed, glabrous or hairy;</text>
      <biological_entity id="o12462" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s16" to="obpyramidal" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s16" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="12-16-ribbed" value_original="12-16-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 0 or of 12–22 orbiculate to spatulate scales.</text>
      <biological_entity id="o12463" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character constraint="of scales" constraintid="o12464" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 12" is_modifier="false" name="quantity" src="d0_s17" to="22 orbiculate to spatulate scales" />
      </biological_entity>
      <biological_entity id="o12464" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s17" to="22" />
        <character char_type="range_value" from="orbiculate" is_modifier="true" name="shape" src="d0_s17" to="spatulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico. H. Robinson (1981) suggested that Hymenopappinae is related to Gaillardiinae and other "core" groups of traditional Helenieae.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico. H. Robinson (1981) suggested that Hymenopappinae is related to Gaillardiinae and other &quot;core&quot; groups of traditional Helenieae" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>187m.19.</number>
  <other_name type="past_name">Hymenopappanae</other_name>
  <discussion>Genera 5, species 28 (1 genus, 10 species in the flora).</discussion>
  <discussion>H. Robinson (1981) suggested that Hymenopappinae is related to Gaillardiinae and other “core” groups of traditional Helenieae.</discussion>
  
</bio:treatment>