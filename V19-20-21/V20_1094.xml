<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Alexander) G. L. Nesom" date="1995" rank="species">fontinale</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 287. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species fontinale</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067648</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Alexander" date="unknown" rank="species">fontialis</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Man. S.E. Fl.,</publication_title>
      <place_in_publication>1382, 1509. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species fontialis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">patens</taxon_name>
    <taxon_name authority="R. W. Long" date="unknown" rank="variety">floridanus</taxon_name>
    <taxon_hierarchy>genus Aster;species patens;variety floridanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–90 cm, colonial;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous.</text>
      <biological_entity id="o28463" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, erect (light to reddish-brown, straight, sometimes stout), proximally moderately to densely hispidulo-strigillose, sometimes glabrescent, distally moderately to densely hispidulo-strigillose, sometimes sparsely stipitate-glandular.</text>
      <biological_entity id="o28464" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally moderately; moderately to densely" name="pubescence" src="d0_s2" value="hispidulo-strigillose" value_original="hispidulo-strigillose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="distally moderately; moderately to densely" name="pubescence" src="d0_s2" value="hispidulo-strigillose" value_original="hispidulo-strigillose" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (green to dark green) firm, margins entire, faces sparsely to moderately strigoso-scabrous, sometimes sparsely stipitate-glandular;</text>
      <biological_entity id="o28465" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o28466" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28467" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s3" value="strigoso-scabrous" value_original="strigoso-scabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal absent at flowering, sessile, blades (3-nerved) oblanceolate to obovate, 25–40 × 12–15 mm, bases attenuate, margins entire (remotely serrate), scabrous, apices obtuse, short-mucronate;</text>
      <biological_entity constraint="basal" id="o28468" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="at blades, bases, margins, apices" constraintid="o28469, o28470, o28471, o28472" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="short-mucronate" value_original="short-mucronate" />
      </biological_entity>
      <biological_entity id="o28469" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28470" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28471" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28472" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline usually withering by flowering, sessile, blades obovate to oblanceolate, 25–80 × 5–18 mm, bases rounded or narrowly auriculate-clasping, margins distally shallowly serrate to subentire, scabrellous, apices spinulose-mucronate;</text>
      <biological_entity constraint="proximal cauline" id="o28473" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by margins, apices" constraintid="o28476, o28477" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o28474" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="distally shallowly serrate" name="shape" src="d0_s5" to="subentire" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
      <biological_entity id="o28475" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="distally shallowly serrate" name="shape" src="d0_s5" to="subentire" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
      <biological_entity id="o28476" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="architecture_or_fixation" src="d0_s5" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character char_type="range_value" from="distally shallowly serrate" name="shape" src="d0_s5" to="subentire" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spinulose-mucronate" value_original="spinulose-mucronate" />
      </biological_entity>
      <biological_entity id="o28477" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="architecture_or_fixation" src="d0_s5" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character char_type="range_value" from="distally shallowly serrate" name="shape" src="d0_s5" to="subentire" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spinulose-mucronate" value_original="spinulose-mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades lanceolate or oblanceolate to oblong-lanceolate, 5–50 × 2–10 mm, reduced distally and becoming bractlike (arrays), bases subclasping to (distally) rounded, margins scabrous, apices acute to white-mucronulate or subspinulose, faces sometimes shiny, minutely gland-dotted.</text>
      <biological_entity constraint="distal" id="o28478" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o28479" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="oblong-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s6" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity id="o28480" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28481" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o28482" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="white-mucronulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subspinulose" value_original="subspinulose" />
      </biological_entity>
      <biological_entity id="o28483" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="reflectance" src="d0_s6" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="minutely" name="coloration" src="d0_s6" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (4–400) in wide to narrow, paniculiform arrays, branches ascending to widely spreading (well-developed), densely small-leaved.</text>
      <biological_entity id="o28484" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o28485" from="4" name="atypical_quantity" src="d0_s7" to="400" />
      </biological_entity>
      <biological_entity id="o28485" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character char_type="range_value" from="wide" is_modifier="true" name="width" src="d0_s7" to="narrow" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o28486" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="widely spreading" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s7" value="small-leaved" value_original="small-leaved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles ascending (secund in well-developed arrays?), 0.3–4.5 cm, densely strigoso-hirsute, sometimes sparsely stipitate-glandular, bracts dense, spreading or reflexed (rarely ascending), narrowly oblong to linear-lanceolate, mostly ca. 3 × 1 mm, spinulose, sparsely to moderately strigilloso-scabrous, sometimes sparsely minutely stipitate-glandular, grading into phyllaries.</text>
      <biological_entity id="o28487" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s8" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="strigoso-hirsute" value_original="strigoso-hirsute" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o28488" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s8" to="linear-lanceolate" />
        <character name="length" src="d0_s8" unit="mm" value="3" value_original="3" />
        <character name="width" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s8" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence_or_relief" src="d0_s8" value="strigilloso-scabrous" value_original="strigilloso-scabrous" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o28489" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o28488" id="r2637" name="into" negation="false" src="d0_s8" to="o28489" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindro-campanulate, 6–7.6 mm.</text>
      <biological_entity id="o28490" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–5 series (appressed), oblong or oblanceolate (outer) to linear-oblanceolate (innermost), unequal, bases indurate, margins hyaline, scarious, erose, ciliate or ciliolate, and/or sometimes stipitate-glandular, often reddish distally, green zones elliptic (outer) to lanceolate, apices erect, acute to acuminate, mucronulate to apiculate (inner), often tinged red-purplish, faces glabrous or glabrate.</text>
      <biological_entity id="o28491" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" notes="" src="d0_s10" to="linear-oblanceolate" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o28492" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o28493" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o28494" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="ciliolate" value_original="ciliolate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o28495" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o28496" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate mucronulate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate mucronulate" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="tinged red-purplish" value_original="tinged red-purplish" />
      </biological_entity>
      <biological_entity id="o28497" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o28491" id="r2638" name="in" negation="false" src="d0_s10" to="o28492" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 15–30;</text>
      <biological_entity id="o28498" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas purplish-blue to lavender, laminae 7–13 × 0.8–2 mm.</text>
      <biological_entity id="o28499" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="purplish-blue" name="coloration" src="d0_s12" to="lavender" />
      </biological_entity>
      <biological_entity id="o28500" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 19-25;</text>
      <biological_entity id="o28501" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas cream to pale-yellow turning reddish purple, sometimes also brownish, 5.2–6 mm, tubes slightly shorter than funnelform throats, lobes lanceolate to triangular, 0.8–1.2 mm.</text>
      <biological_entity id="o28502" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s14" to="pale-yellow turning reddish purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28503" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o28504" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o28504" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o28505" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s14" to="triangular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae tan, obovoid-fusiform, slightly compressed, 1.9–2.3 mm, 4–5-nerved (golden bronze), faces sparsely strigillose to glabrescent;</text>
      <biological_entity id="o28506" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid-fusiform" value_original="obovoid-fusiform" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="4-5-nerved" value_original="4-5-nerved" />
      </biological_entity>
      <biological_entity id="o28507" name="face" name_original="faces" src="d0_s15" type="structure">
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s15" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi sordid, 5–6.2 mm.</text>
      <biological_entity id="o28508" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="sordid" value_original="sordid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="6.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Nov–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, sometimes peaty soils, marshes, sandhills, hammocks, flood plains, ditch banks, rocky bluffs along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="peaty soils" modifier="moist sometimes" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="ditch banks" />
        <character name="habitat" value="rocky bluffs" constraint="along streams" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Florida water aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Symphyotrichum fontinale has been considered to be conspecific with S. patens or S. dumosum of subg. Symphyotrichum (A. Cronquist 1980). It fits well into sect. Grandiflori of subg. Virgulus.</discussion>
  
</bio:treatment>