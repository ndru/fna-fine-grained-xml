<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="1870" rank="species">atractyloides</taxon_name>
    <taxon_name authority="(B. L. Robinson) Jepson" date="1925" rank="variety">odontolepis</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1016. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species atractyloides;variety odontolepis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068102</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">arguta</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">odontolepis</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Gray Herb.</publication_title>
      <place_in_publication>1: 103, fig. 79ß. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brickellia;species arguta;variety odontolepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades ovate, 10–20 × 5–15 mm, margins dentate-serrate, faces strigose, often glandular-puberulent as well.</text>
      <biological_entity id="o12043" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s0" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12044" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="dentate-serrate" value_original="dentate-serrate" />
      </biological_entity>
      <biological_entity id="o12045" name="face" name_original="faces" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="often; well" name="pubescence" src="d0_s0" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 10–15 mm.</text>
      <biological_entity id="o12046" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries: outer 5–12-striate, lanceovate to lance-linear, margins dentate, inner linear (± chartaceous).</text>
      <biological_entity id="o12047" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure" />
      <biological_entity constraint="outer" id="o12048" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s2" value="5-12-striate" value_original="5-12-striate" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s2" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o12049" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12050" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Florets 40–55;</text>
      <biological_entity id="o12051" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s3" to="55" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corollas cream, often purple-tinged.</text>
      <biological_entity id="o12052" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert mountain slopes, washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert mountain slopes" />
        <character name="habitat" value="washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2c.</number>
  
</bio:treatment>