<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">bidens</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1884" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 297. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus bidens;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066231</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (10–) 15–25 (–30+) cm.</text>
      <biological_entity id="o15077" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 10–20 mm;</text>
      <biological_entity id="o15078" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15079" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades either oblanceolate to linear, 5–15+ × 1–2+ mm, or rounded-deltate overall, 10–25 (–60+) × 15–25+ mm, (1–) 2–3-pinnatisect, ultimate lobes oblanceolate to spatulate or linear, 5–15+ × 0.5–5 mm, bases ± cuneate, ultimate margins entire, sometimes ciliolate, apices obtuse to acute, faces glabrous.</text>
      <biological_entity id="o15080" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15081" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded-deltate" value_original="rounded-deltate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="(1-)2-3-pinnatisect" value_original="(1-)2-3-pinnatisect" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15082" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="spatulate or linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15083" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15084" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o15085" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o15086" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly, sometimes in open, ± corymbiform arrays.</text>
      <biological_entity id="o15087" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 10–20 (–90) mm.</text>
      <biological_entity id="o15088" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of (1–) 3–4 appressed to spreading, spatulate to linear bractlets or bracts 3–10 (–25+) mm (sometimes foliaceous: pinnate, lobes 3–5+, linear), margins ciliolate, abaxial faces usually glabrous, sometimes sparsely hispidulous.</text>
      <biological_entity id="o15089" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" modifier="of" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" modifier="of" name="quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s5" to="spreading" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o15090" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15091" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15092" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15093" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s5" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± campanulate to cylindric, 2–3 (–8) × 2 (–3) [–4] mm.</text>
      <biological_entity id="o15094" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s6" to="cylindric" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="3" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries (3–) 5, oblong to lanceolate orlinear, (2–) 3–8 mm.</text>
      <biological_entity id="o15095" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0 or 1 (–3+);</text>
      <biological_entity id="o15096" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" upper_restricted="false" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae whitish, 1–1.5 (–3+) mm.</text>
      <biological_entity id="o15097" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets (3–) 5–9;</text>
      <biological_entity id="o15098" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas whitish to yellowish, 2–2.5 mm.</text>
      <biological_entity id="o15099" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s11" to="yellowish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae: outer redbrown (sometimes with lighter blotches), ± equally 4-angled, linear-fusiform, 5–6 (–8) mm, margins not ciliate, apices ± attenuate, faces 2-grooved, usually glabrous;</text>
      <biological_entity id="o15100" name="cypsela" name_original="cypselae" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o15101" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="more or less equally" name="shape" src="d0_s12" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-fusiform" value_original="linear-fusiform" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15102" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15103" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o15104" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-grooved" value_original="2-grooved" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner similar, 10–14 mm;</text>
      <biological_entity id="o15105" name="cypsela" name_original="cypselae" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o15106" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 2–3 erect, retrorsely barbed awns 1–2 (–3) mm.</text>
      <biological_entity id="o15107" name="cypsela" name_original="cypselae" src="d0_s14" type="structure" />
      <biological_entity id="o15108" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o15109" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s14" to="3" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="true" modifier="retrorsely" name="architecture_or_shape" src="d0_s14" value="barbed" value_original="barbed" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o15108" id="r1037" name="consist_of" negation="false" src="d0_s14" to="o15109" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wettish spots on rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wettish spots" constraint="on rocky slopes" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">lemmoni</other_name>
  
</bio:treatment>