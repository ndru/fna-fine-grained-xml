<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">eriophyllum</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1883" rank="species">ambiguum</taxon_name>
    <taxon_name authority="(Brandegee) Ferris" date="1958" rank="variety">paleaceum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>5: 100. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eriophyllum;species ambiguum;variety paleaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068380</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriophyllum</taxon_name>
    <taxon_name authority="Brandegee" date="unknown" rank="species">paleaceum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 450. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriophyllum;species paleaceum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Disc-florets 15–30;</text>
      <biological_entity id="o26090" name="disc-floret" name_original="disc-florets" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s0" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>corolla lobes hairy (hairs 1-celled).</text>
      <biological_entity constraint="corolla" id="o26091" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pappi 0 or of 6–10 entire scales 0.1–0.2 mm. 2n = 14.</text>
      <biological_entity id="o26092" name="pappus" name_original="pappi" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 6" is_modifier="false" name="quantity" src="d0_s2" to="10 entire scales" />
      </biological_entity>
      <biological_entity id="o26093" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26094" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="14" value_original="14" />
      </biological_entity>
      <relation from="o26092" id="r1769" name="consist_of" negation="false" src="d0_s2" to="o26093" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or rocky openings, creosote-bush scrublands, or Joshua tree or pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky openings" />
        <character name="habitat" value="creosote-bush scrublands" />
        <character name="habitat" value="joshua tree" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6b.</number>
  
</bio:treatment>