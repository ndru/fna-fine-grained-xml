<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CARDUUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 820. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 358. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus CARDUUS</taxon_hierarchy>
    <other_info_on_name type="etymology">From ancient name of thistlelike plant</other_info_on_name>
    <other_info_on_name type="fna_id">105640</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials [perennials], 30–200 (–400) cm, spiny, ± tomentose, sometimes glabrate.</text>
      <biological_entity id="o21029" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="400" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="spiny" value_original="spiny" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="400" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="spiny" value_original="spiny" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple to much branched, (spiny-winged).</text>
      <biological_entity id="o21031" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple to much" value_original="simple to much" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o21032" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins spiny dentate, often 1–2-pinnately lobed, faces glabrous or hairy, eglandular.</text>
      <biological_entity constraint="blade" id="o21033" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="often 1-2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o21034" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads discoid, borne singly or 2–20 in dense clusters or corymbiform arrays.</text>
      <biological_entity id="o21035" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in dense clusters or corymbiform arrays" from="2" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>(Peduncles naked or leafy-bracteate, spiny-winged or not winged.) Involucres cylindric to spheric.</text>
      <biological_entity id="o21036" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s6" to="spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries many in 7–10+ series, linear to broadly ovate, bases appressed, margins entire, apices ascending to spreading or reflexed, acute, spine-tipped.</text>
      <biological_entity id="o21037" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="in series" constraintid="o21038" is_modifier="false" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s7" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o21038" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s7" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o21039" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o21040" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21041" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="spreading or reflexed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat, epaleate, bearing setiform scales (“flattened bristles”).</text>
      <biological_entity id="o21042" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
      <biological_entity id="o21043" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="setiform" value_original="setiform" />
      </biological_entity>
      <relation from="o21042" id="r1902" name="bearing" negation="false" src="d0_s8" to="o21043" />
    </statement>
    <statement id="d0_s9">
      <text>Florets several–many;</text>
      <biological_entity id="o21044" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="several" is_modifier="false" name="quantity" src="d0_s9" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white to pink or purple, ± bilateral, tubes long, slender, throats short, campanulate, abruptly expanded from tubes, lobes linear;</text>
      <biological_entity id="o21045" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink or purple" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s10" value="bilateral" value_original="bilateral" />
      </biological_entity>
      <biological_entity id="o21046" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o21047" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character constraint="from tubes" constraintid="o21048" is_modifier="false" modifier="abruptly" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o21048" name="tube" name_original="tubes" src="d0_s10" type="structure" />
      <biological_entity id="o21049" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anther bases sharply short-tailed, apical appendages oblong;</text>
      <biological_entity constraint="anther" id="o21050" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s11" value="short-tailed" value_original="short-tailed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21051" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style-branches: fused portions with slightly, minutely puberulent, swollen basal nodes, distally papillate or glabrous, distinct portions very short.</text>
      <biological_entity id="o21052" name="style-branch" name_original="style-branches" src="d0_s12" type="structure" />
      <biological_entity id="o21053" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21054" name="node" name_original="nodes" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="slightly; minutely" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
        <character is_modifier="true" name="shape" src="d0_s12" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o21055" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
      <relation from="o21053" id="r1903" name="with" negation="false" src="d0_s12" to="o21054" />
      <relation from="o21053" id="r1904" name="with" negation="false" src="d0_s12" to="o21055" />
    </statement>
    <statement id="d0_s13">
      <text>Cypselae ovoid, slightly compressed, faces smooth, glabrous, attachment scars slightly lateral;</text>
      <biological_entity id="o21056" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o21057" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o21058" name="scar" name_original="scars" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s13" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent or falling in rings, of many minutely barbed, basally connate bristles or setiform, minutely barbed scales (“minutely flattened bristles”).</text>
      <biological_entity id="o21060" name="ring" name_original="rings" src="d0_s14" type="structure" />
      <biological_entity id="o21061" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="many" value_original="many" />
        <character is_modifier="true" modifier="minutely" name="architecture_or_shape" src="d0_s14" value="barbed" value_original="barbed" />
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o21062" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="setiform" value_original="setiform" />
        <character is_modifier="true" modifier="minutely" name="architecture_or_shape" src="d0_s14" value="barbed" value_original="barbed" />
      </biological_entity>
      <relation from="o21059" id="r1905" name="consist_of" negation="false" src="d0_s14" to="o21061" />
      <relation from="o21059" id="r1906" name="consist_of" negation="false" src="d0_s14" to="o21062" />
    </statement>
    <statement id="d0_s15">
      <text>x = 8, 9, 10, 11, 13.</text>
      <biological_entity id="o21059" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character constraint="in rings" constraintid="o21060" is_modifier="false" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o21063" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character name="quantity" src="d0_s15" value="11" value_original="11" />
        <character name="quantity" src="d0_s15" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Plumeless thistle</other_name>
  <other_name type="common_name">chardon</other_name>
  <discussion>Species ca. 90 (5 in the flora).</discussion>
  <references>
    <reference>Batra, S. W. T., J. R. Coulson, P. H. Dunn, and P. E. Boldt. 1981. Insects and Fungi Associated with Carduus Thistles (Compositae). Washington. [U.S.D.A. Techn. Bull. 1616.]</reference>
    <reference>Desrochers, A. M., J. F. Bain, and S. I. Warwick. 1988b. The biology of Canadian weeds. 89. Carduus nutans L. and Carduus acanthoides L. Canad. J. Plant Sci. 68: 1053–1068.</reference>
    <reference>Franco, J. do A. 1976. Carduus. In: T. G. Tutin et al., eds. 1964–1980. Flora Europaea. 5 vols. Cambridge. Vol. 4, pp. 220–232.</reference>
    <reference>Kazmi, S. M. A. 1964. Revision der Gattung Carduus (Compositae). Teil II. Mitt. Bot. Staatssaml. München 5: 279–550.</reference>
    <reference>Desrochers, A. M. , J. F. Bain, and S. I. Warwick. 1988. A biosystematic study of the Carduus nutans complex in Canada. Canad. J. Bot. 66: 1621–1631.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllary appendages 2–7 mm wide, usually wider than appressed bases; peduncles often elongate, distally wingless; heads often nodding, usually borne singly or in leafy corymbiform arrays; involucres 20–70 mm diam</description>
      <determination>3 Carduus nutans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllary appendages 0.5–1.5 mm wide, usually narrower than appressed bases; peduncles short, if present, usually winged throughout or wingless only near tip; heads erect, 1–many, often clustered at branch tips; involucres 7–30 mm diam</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres spheric or hemispheric</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres cylindric or narrowly ellipsoid</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas 13–20 mm; heads 18–25 mm; involucres 14–20 mm; abaxial leaf faces glabrate except for long, curled, septate hairs along veins</description>
      <determination>1 Carduus acanthoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas 11–16 mm; heads 15–18 mm; involucres 12–17 mm; abaxial leaf faces sparsely to densely tomentose with fine, nonseptate hairs and often with curled, septate hairs along veins as well</description>
      <determination>2 Carduus crispus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads 1–5 at ends of branches; phyllaries not scarious-margined, ± persistently tomentose, distally scabrous on margins and faces</description>
      <determination>4 Carduus pycnocephalus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads 5–20 at ends of branches; phyllaries scarious-margined, glabrous or spar- ingly tomentose, distally ciliolate or glabrous</description>
      <determination>5 Carduus tenuiflorus</determination>
    </key_statement>
  </key>
</bio:treatment>