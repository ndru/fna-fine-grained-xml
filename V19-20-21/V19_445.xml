<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">303</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">picris</taxon_name>
    <taxon_name authority="(Linnaeus) Desfontaines" date="1804" rank="species">rhagadioloides</taxon_name>
    <place_of_publication>
      <publication_title>Tabl. École Bot.,</publication_title>
      <place_in_publication>89. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus picris;species rhagadioloides</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067346</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rhagadioloides</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>12, 2: 527. 1767</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Mant. Pl.,</publication_title>
      <place_in_publication>108. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crepis;species rhagadioloides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o16594" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 20–60 (–90) × 5–20+ mm.</text>
      <biological_entity id="o16595" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 8–12+ × 6–9+ mm, larger in fruit.</text>
      <biological_entity id="o16596" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="9" to_unit="mm" upper_restricted="false" />
        <character constraint="in fruit" constraintid="o16597" is_modifier="false" name="size" src="d0_s2" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o16597" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries proximally navicular, each enfolding its subtended floret, usually bristly and/or tomentulose abaxially.</text>
      <biological_entity id="o16598" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="position" src="d0_s3" value="enfolding" value_original="enfolding" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="bristly" value_original="bristly" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o16599" name="floret" name_original="floret" src="d0_s3" type="structure" />
      <relation from="o16598" id="r1493" name="subtended" negation="false" src="d0_s3" to="o16599" />
    </statement>
    <statement id="d0_s4">
      <text>Cypselae (outer often arcuate) 2.5–3 mm;</text>
      <biological_entity id="o16600" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 1–1.5 (outer cypselae) or 5–6 mm (inner cypselae).</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 10.</text>
      <biological_entity id="o16601" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="1.5" />
        <character name="quantity" src="d0_s5" value="5-6 mm" value_original="5-6 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16602" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">rhagadialoides</other_name>
  <discussion>The single report of Picris rhagadioloides for the flora area (as P. sprengerana from St. Louis; G. Yatskievych, pers. comm.) was confirmed for this study. Picris sprengerana (Linnaeus) Poiret has been misapplied to plants here called P. rhagadioloides (see W. Greuter 2003).</discussion>
  
</bio:treatment>