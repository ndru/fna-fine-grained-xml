<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="treatment_page">193</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">coreopsis</taxon_name>
    <taxon_name authority="(A. Dietrich) S. F. Blake" date="1916" rank="species">basalis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>51: 525. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section coreopsis;species basalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242412738</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliopsis</taxon_name>
    <taxon_name authority="A. Dietrich" date="unknown" rank="species">basalis</taxon_name>
    <place_of_publication>
      <publication_title>Allg. Gartenzeitung</publication_title>
      <place_in_publication>3: 329. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calliopsis;species basalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Parker ex E. B. Smith" date="unknown" rank="species">basalis</taxon_name>
    <taxon_name authority="(A. Gray) S. F. Blake" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>genus Coreopsis;species basalis;variety wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus Coreopsis;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–50+ cm.</text>
      <biological_entity id="o7698" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial nodes proximal to first peduncle usually 5–10+, distalmost 1–3 internodes 4–7 (–10) cm.</text>
      <biological_entity id="o7699" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character constraint="to peduncle" constraintid="o7700" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" notes="" src="d0_s1" value="distalmost" value_original="distalmost" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o7700" name="peduncle" name_original="peduncle" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7701" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and cauline on proximal 3/4–7/8 of plant heights;</text>
      <biological_entity id="o7702" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character constraint="on proximal 3/4-7/8" constraintid="o7703" is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7703" name="3/4-7/8" name_original="3/4-7/8" src="d0_s2" type="structure" />
      <biological_entity id="o7704" name="plant" name_original="plant" src="d0_s2" type="structure" />
      <relation from="o7703" id="r542" name="part_of" negation="false" src="d0_s2" to="o7704" />
    </statement>
    <statement id="d0_s3">
      <text>petioles 8–35 (–120) mm;</text>
      <biological_entity id="o7705" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7706" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades simple or 1 (–2) -pinnately lobed with 3–9+ lobes, simple blades or terminal lobes elliptic or lanceolate to oblanceolate or linear, 25–55+ × (1–) 2–9 (–20) mm.</text>
      <biological_entity id="o7707" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7708" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character constraint="with lobes, blades, lobes" constraintid="o7710, o7711" is_modifier="false" modifier="1(-2)-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" notes="" src="d0_s4" to="55" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7710" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="9" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate or linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" notes="alterIDs:o7710" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" notes="alterIDs:o7710" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="alterIDs:o7710" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7711" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="9" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate or linear" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o7712" name="blade" name_original="blades" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 6–15+ cm.</text>
      <biological_entity id="o7713" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of lance-deltate to linear bractlets 6–9 (–12+) mm.</text>
      <biological_entity id="o7714" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o7715" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="lance-deltate" is_modifier="true" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <relation from="o7714" id="r543" name="consists_of" negation="false" src="d0_s6" to="o7715" />
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries lanceovate, 7–9+ mm.</text>
      <biological_entity id="o7716" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceovate" value_original="lanceovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray laminae yellow, usually each with a proximal, redbrown to purple spot or band, 15–20+ mm.</text>
      <biological_entity constraint="ray" id="o7717" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="redbrown to purple spot" value_original="redbrown to purple spot" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7718" name="proximal" name_original="proximal" src="d0_s8" type="structure" />
      <biological_entity id="o7719" name="band" name_original="band" src="d0_s8" type="structure" />
      <relation from="o7717" id="r544" modifier="usually" name="with" negation="false" src="d0_s8" to="o7718" />
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 3–4 mm, apices redbrown to purple.</text>
      <biological_entity constraint="disc" id="o7720" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7721" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s9" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1.2–1.8 mm, wingless (margins ± inrolled adaxially, ± corky).</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 26.</text>
      <biological_entity id="o7722" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="wingless" value_original="wingless" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7723" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils in open, often disturbed, places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="in open , often disturbed , places" />
        <character name="habitat" value="open" />
        <character name="habitat" value="disturbed" modifier="often" />
        <character name="habitat" value="places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., Okla., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Plants in the western part of the distribution of Coreopsis basalis usually have narrower lobes of leaf blades and narrower outer phyllaries; such plants have been treated as C. wrightii or as C. basalis var. wrightii.</discussion>
  
</bio:treatment>