<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="subtribe">Flaveriinae</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Gen. Compos.,</publication_title>
      <place_in_publication>235. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe Flaveriinae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20731</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or subshrubs, 10–80 cm (often ± succulent).</text>
      <biological_entity id="o7906" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o7908" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o7909" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-nerved or 3-nerved) oblong, oblong-ovate, lanceolate, linear, or filiform, margins entire or toothed, faces glabrous, glabrescent, or puberulent.</text>
      <biological_entity id="o7910" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o7911" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o7912" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiate or discoid, in glomerules or corymbiform to paniculiform arrays.</text>
      <biological_entity id="o7913" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="discoid" value_original="discoid" />
        <character char_type="range_value" from="corymbiform" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
      <biological_entity id="o7914" name="glomerule" name_original="glomerules" src="d0_s5" type="structure" />
      <relation from="o7913" id="r554" name="in" negation="false" src="d0_s5" to="o7914" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi 0 or of 1–2 bractlets.</text>
      <biological_entity id="o7915" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 1" is_modifier="false" name="quantity" src="d0_s6" to="2 bractlets" />
      </biological_entity>
      <biological_entity id="o7916" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <relation from="o7915" id="r555" name="consist_of" negation="false" src="d0_s6" to="o7916" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, cylindric, obconic, turbinate, or urceolate.</text>
      <biological_entity id="o7917" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 2–8 in ± 1 series (distinct, linear to oblong, subequal, ± succulent to membranous or scarious).</text>
      <biological_entity id="o7918" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o7919" from="2" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o7919" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, epaleate.</text>
      <biological_entity id="o7920" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0, or 1–6, pistillate, fertile;</text>
      <biological_entity id="o7921" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o7922" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 1–60 [–100+], bisexual, fertile;</text>
      <biological_entity id="o7923" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than or about equaling the funnelform to campanulate throats, lobes 5, deltate;</text>
      <biological_entity id="o7924" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o7925" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than or about equaling the funnelform to campanulate throats" constraintid="o7926" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7926" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o7927" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anther thecae pale;</text>
      <biological_entity constraint="anther" id="o7928" name="theca" name_original="thecae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmatic papillae in 2 lines.</text>
      <biological_entity constraint="stigmatic" id="o7929" name="papilla" name_original="papillae" src="d0_s15" type="structure" />
      <biological_entity id="o7930" name="line" name_original="lines" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7929" id="r556" name="in" negation="false" src="d0_s15" to="o7930" />
    </statement>
    <statement id="d0_s16">
      <text>Cypselae cylindric to clavate or ± compresssed, linear-oblong to oblanceolate, 10–15-ribbed, glabrous or hairy;</text>
      <biological_entity id="o7931" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s16" to="clavate or more or less compresssed linear-oblong" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s16" to="clavate or more or less compresssed linear-oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="10-15-ribbed" value_original="10-15-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 0, or persistent, coroniform, or of 2–5 ovate, scarious scales, or of 20–25+ unequal bristles, or of 5 scales plus 5 bristles (all in 1 series, sometimes connate).</text>
      <biological_entity id="o7932" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s17" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity id="o7933" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="true" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="true" name="texture" src="d0_s17" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o7934" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s17" to="25" upper_restricted="false" />
        <character is_modifier="true" name="size" src="d0_s17" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o7935" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="5" value_original="5" />
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o7936" name="bristle" name_original="bristles" src="d0_s17" type="structure" />
      <relation from="o7932" id="r557" name="consist_of" negation="false" src="d0_s17" to="o7933" />
      <relation from="o7932" id="r558" name="consist_of" negation="false" src="d0_s17" to="o7934" />
      <relation from="o7932" id="r559" name="consist_of" negation="false" src="d0_s17" to="o7935" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly subtropical, tropical, and warm-temperate New World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly subtropical" establishment_means="native" />
        <character name="distribution" value="tropical" establishment_means="native" />
        <character name="distribution" value="and warm-temperate New World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>187m.15.</number>
  <other_name type="past_name">Flaverieae</other_name>
  <discussion>Genera 3, species 27 (3 genera, 10 species in the flora).</discussion>
  <discussion>As noted by H. Robinson (1981), traditionally, Flaveria and Sartwellia were treated in Helenieae, Haploësthes in Senecioneae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets 18–30(–60); pappi of 20–25+ bristles</description>
      <determination>325 Haploësthes</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets 1–15; pappi 0, or coroniform, or wholly or partly of scales</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets 3–5; pappi of 5 erose scales alternating with 5 setiform scales or bristles (sometimes all 10 elements basally connate)</description>
      <determination>326 Sartwellia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets 0 or 1; pappi 0, or coroniform, or of 2–4 hyaline scales</description>
      <determination>327 Flaveria</determination>
    </key_statement>
  </key>
</bio:treatment>