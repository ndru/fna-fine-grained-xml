<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">281</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="species">eatonii</taxon_name>
    <taxon_name authority="(Greene) Cronquist in C. L. Hitchcock et al." date="1955" rank="variety">plantagineus</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>5: 175. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species eatonii;variety plantagineus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068333</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">plantagineus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 292. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species plantagineus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">eatonii</taxon_name>
    <taxon_name authority="(Greene) Cronquist" date="unknown" rank="subspecies">plantagineus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species eatonii;subspecies plantagineus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">robertianus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species robertianus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–23 cm.</text>
      <biological_entity id="o24410" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="23" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads 1–4, held just beyond to well beyond basal leaves.</text>
      <biological_entity id="o24411" name="head" name_original="heads" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24412" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o24411" id="r2246" modifier="beyond; well" name="beyond" negation="false" src="d0_s1" to="o24412" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres 5–7 × (9–) 11–12 (–14) mm.</text>
      <biological_entity id="o24413" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_width" src="d0_s2" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="14" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries sparsely to moderately hirtellous (hairs mostly 0.3–0.8 mm), sometimes sparsely minutely glandular.</text>
      <biological_entity id="o24414" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray laminae 5.5–8 (–9).</text>
      <biological_entity constraint="ray" id="o24415" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="5.5" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 3–4 mm.</text>
      <biological_entity constraint="disc" id="o24416" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 1.8–2.3 mm;</text>
      <biological_entity id="o24417" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi of 16–20 bristles 3–3.5 mm. 2n = 18.</text>
      <biological_entity id="o24418" name="pappus" name_original="pappi" src="d0_s7" type="structure" />
      <biological_entity id="o24419" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24420" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
      <relation from="o24418" id="r2247" name="consist_of" negation="false" src="d0_s7" to="o24419" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, grassy sites, often with sagebrush scrub, usually over volcanic substrate, less commonly over serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="grassy sites" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="volcanic substrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1000–)1500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10d.</number>
  
</bio:treatment>