<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">500</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">floribunda</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 73. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species floribunda</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066256</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 100–200 cm (bases woody).</text>
      <biological_entity id="o23560" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, stipitate-glandular.</text>
      <biological_entity id="o23561" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o23562" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 20–35 mm;</text>
      <biological_entity id="o23563" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-nerved from bases, deltate-ovate or rhombic-ovate, 20–140 × 10–100 mm, bases truncate to cordate, margins irregularly dentate, apices acute, faces glanddotted or glandular-pubescent.</text>
      <biological_entity id="o23564" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o23565" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="140" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="100" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23565" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o23566" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o23567" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o23568" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o23569" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o23570" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o23571" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o23570" id="r1616" name="in" negation="false" src="d0_s5" to="o23571" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3–18 mm, stipitate-glandular.</text>
      <biological_entity id="o23572" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to campanulate, 7.5–8.5 mm.</text>
      <biological_entity id="o23573" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="campanulate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s7" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 22–26 in 5–6 series, greenish, often purple-tinged, 3–4-striate, unequal, margins narrowly scarious (apices obtuse to acute);</text>
      <biological_entity id="o23574" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o23575" from="22" name="quantity" src="d0_s8" to="26" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="3-4-striate" value_original="3-4-striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o23575" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o23576" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ovate to lanceolate (glandular-hirtellous, shorter than inner), inner linear-lanceolate (glabrous).</text>
      <biological_entity constraint="outer" id="o23577" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o23578" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 15–24;</text>
      <biological_entity id="o23579" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pale yellow-green or greenish white, 5–6.5 mm.</text>
      <biological_entity id="o23580" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale yellow-green" value_original="pale yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–3 mm, strigose;</text>
      <biological_entity id="o23581" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 30–35 white, barbellulate bristles.</text>
      <biological_entity id="o23583" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s13" to="35" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o23582" id="r1617" name="consist_of" negation="false" src="d0_s13" to="o23583" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o23582" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o23584" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Near streams, canyon bottoms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="near streams" />
        <character name="habitat" value="canyon bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  
</bio:treatment>