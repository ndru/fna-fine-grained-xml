<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">parryi</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">vulcanica</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 89. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species parryi;variety vulcanica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068308</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">vulcanicus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 80. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species vulcanicus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">vulcanicus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species parryi;subspecies vulcanicus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 cm (branches slender).</text>
      <biological_entity id="o15543" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves moderately crowded, green;</text>
      <biological_entity id="o15544" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 1 (–3) -nerved, linear, 30–50 × 0.5–2 mm, faces glabrous, minutely gland-dotted;</text>
      <biological_entity id="o15545" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1(-3)-nerved" value_original="1(-3)-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15546" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="coloration" src="d0_s2" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distalmost about equaling arrays.</text>
      <biological_entity constraint="distalmost" id="o15547" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 6–20+ in lax (much-branched), racemiform to thyrsiform arrays.</text>
      <biological_entity id="o15548" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o15549" from="6" name="quantity" src="d0_s4" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o15549" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="lax" value_original="lax" />
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s4" to="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 12.5–15 mm.</text>
      <biological_entity id="o15550" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="12.5" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 9–13, chartaceous, apices erect (tan or greenish), attenuate.</text>
      <biological_entity id="o15551" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="13" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o15552" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5–7;</text>
      <biological_entity id="o15553" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 9–11 mm, tubes glabrous or proximally puberulent, throats gradually dilated, lobes 1.5–1.9 mm. 2n = 18.</text>
      <biological_entity id="o15554" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15555" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o15556" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o15557" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15558" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, rocky slopes and sagebrush flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="sagebrush flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>271.</number>
  <other_name type="common_name">Vulcan rabbitbrush</other_name>
  <discussion>Variety vulcanica grows in the southern Sierra Nevada.</discussion>
  
</bio:treatment>