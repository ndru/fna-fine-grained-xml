<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">440</other_info_on_meta>
    <other_info_on_meta type="treatment_page">444</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1894" rank="species">coronopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 111. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species coronopifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linosyris</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">coronopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 96. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Linosyris;species coronopifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isocoma</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">coronopifolia</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="unknown" rank="variety">pedicellata</taxon_name>
    <taxon_hierarchy>genus Isocoma;species coronopifolia;variety pedicellata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isocoma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pedicellata</taxon_name>
    <taxon_hierarchy>genus Isocoma;species pedicellata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage usually glabrous, leaves never stipitate-glandular, almost always resinous.</text>
      <biological_entity id="o10139" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10140" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="never" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="almost always" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblanceolate, 15–30 (–50) mm, margins usually pinnatifid (lobes in 1–3 pairs, spreading, linear, apically spinescent), sometimes entire or only proximal leaves pinnatifid.</text>
      <biological_entity id="o10141" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s1" to="50" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="distance" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10142" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10143" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="only" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 5–6 (–7) × 2.5–4 mm.</text>
      <biological_entity id="o10144" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary apices green to greenish yellow or yellowish (markedly thickened), not aristate, slightly or not gland-dotted, without resin pockets.</text>
      <biological_entity constraint="phyllary" id="o10145" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="greenish yellow or yellowish" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="aristate" value_original="aristate" />
        <character is_modifier="false" modifier="slightly; not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity constraint="resin" id="o10146" name="pocket" name_original="pockets" src="d0_s3" type="structure" />
      <relation from="o10145" id="r937" name="without" negation="false" src="d0_s3" to="o10146" />
    </statement>
    <statement id="d0_s4">
      <text>Florets 12–15;</text>
      <biological_entity id="o10147" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 4.5–6 mm.</text>
      <biological_entity id="o10148" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypsela ribs not forming hornlike extensions.</text>
      <biological_entity id="o10150" name="extension" name_original="extensions" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="hornlike" value_original="hornlike" />
      </biological_entity>
      <relation from="o10149" id="r938" name="forming" negation="false" src="d0_s6" to="o10150" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 12.</text>
      <biological_entity constraint="cypsela" id="o10149" name="rib" name_original="ribs" src="d0_s6" type="structure" />
      <biological_entity constraint="2n" id="o10151" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline sandy flats, matorral, shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline sandy flats" />
        <character name="habitat" value="matorral" />
        <character name="habitat" value="shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Common jimmyweed</other_name>
  <discussion>Populations with at least the distal leaves entire have been called Isocoma pedicellata, and these apparently segregate geographically, at least to some extent (G. L. Nesom 1991c). Some plants have both entire and pinnatifid leaves; the distinction does not appear to warrant formal recognition.</discussion>
  
</bio:treatment>