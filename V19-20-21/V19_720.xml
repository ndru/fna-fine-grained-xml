<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="treatment_page">436</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Weddell" date="1856" rank="genus">gamochaeta</taxon_name>
    <taxon_name authority="(Urban) Anderberg" date="1991" rank="species">antillana</taxon_name>
    <place_of_publication>
      <publication_title>Opera Bot.</publication_title>
      <place_in_publication>104: 157. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus gamochaeta;species antillana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066791</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Urban" date="unknown" rank="species">antillanum</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>13: 482. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species antillanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gamochaeta</taxon_name>
    <taxon_name authority="(Cabrera) Cabrera" date="unknown" rank="species">subfalcata</taxon_name>
    <taxon_hierarchy>genus Gamochaeta;species subfalcata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Cabrera" date="unknown" rank="species">subfalcatum</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;species subfalcatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 6–40 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o771" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to decumbent-ascending, loosely arachnose-tomentose.</text>
      <biological_entity id="o772" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent-ascending" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s2" value="arachnose-tomentose" value_original="arachnose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, basal usually withering before flowering, blades spatulate to oblanceolate, narrowly lanceolate, linear-oblanceolate, or linear, 2–3 (–4) cm × 2–3.5 (–5) mm (distal rarely folded along midveins), faces concolor, loosely tomentose.</text>
      <biological_entity id="o773" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="before blades, faces" constraintid="o774, o775" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o774" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate narrowly lanceolate linear-oblanceolate or linear" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="concolor" value_original="concolor" />
      </biological_entity>
      <biological_entity id="o775" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate narrowly lanceolate linear-oblanceolate or linear" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="concolor" value_original="concolor" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads initially in uninterrupted, cylindro-spiciform arrays (1–) 3–4 (–10) cm × 8–12 mm (pressed), usually becoming glomerulate-interrupted in late flowering (equally leafy-bracted throughout, bracts linear to narrowly lanceolate, smaller distally).</text>
      <biological_entity id="o776" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
        <character constraint="in late flowering" is_modifier="false" modifier="in uninterrupted , cylindro-spiciform arrays; usually becoming" name="architecture" src="d0_s4" value="glomerulate-interrupted" value_original="glomerulate-interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate, 2.5–3 mm, bases sparsely arachnose.</text>
      <biological_entity id="o777" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o778" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 3–4 (–5) series, outer ovatelanceolate, lengths 1/2–2/3 inner, apices (sometimes purplish-tinged) narrowly to broadly acute, inner usually purple (immediately beyond stereome and along proximal margins), oblong, laminae usually purple (at stereome and along proximal margins), apices (whitish, tinged with brown) rounded-obtuse.</text>
      <biological_entity id="o779" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity id="o780" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o781" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="2/3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o782" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity id="o783" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="lengths" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o784" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o785" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o786" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-obtuse" value_original="rounded-obtuse" />
      </biological_entity>
      <relation from="o779" id="r74" name="in" negation="false" src="d0_s6" to="o780" />
    </statement>
    <statement id="d0_s7">
      <text>Florets: bisexual 3–5;</text>
      <biological_entity id="o787" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>all corollas usually purple distally.</text>
      <biological_entity id="o788" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o789" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae (tan) 0.4–0.5 mm.</text>
      <biological_entity id="o790" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–May, sometimes later with moisture.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="sometimes later with moisture" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites in sandy soils, commonly in roadsides and other disturbed sites, stream and pond banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="open" constraint="in sandy soils" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="roadsides" modifier="commonly in" />
        <character name="habitat" value="other disturbed sites" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="pond banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., Okla., S.C., Tenn., Tex., Va.; South America; Europe; New Zealand.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="New Zealand" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="past_name">antillarum</other_name>
  <other_name type="common_name">Delicate everlasting</other_name>
  <discussion>Gamochaeta antillana and G. calviceps have been combined in concept and often misidentified as Gamochaeta falcata (Lamarck) Cabrera; the latter name applies to a South American species that has not been recorded from the flora area. Gamochaeta subfalcata, which has been attributed to the United States (e.g., S. E. Freire and L. Iharlegui 1997), almost certainly applies to the same species as G. antillana.</discussion>
  
</bio:treatment>