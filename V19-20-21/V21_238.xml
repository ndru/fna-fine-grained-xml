<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">wyethia</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">glabra</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 543. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus wyethia;species glabra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067820</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–40 (–60) cm.</text>
      <biological_entity id="o22160" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blades oblong-lanceolate to elliptic-ovate, 20–30 (–40) cm, margins entire or ± serrate-dentate (often undulate), faces glabrous or finely stipitate-glandular, sometimes sparsely pilosulous as well (usually shining);</text>
      <biological_entity constraint="basal" id="o22161" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22162" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s1" to="elliptic-ovate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22163" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s1" value="serrate-dentate" value_original="serrate-dentate" />
      </biological_entity>
      <biological_entity id="o22164" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes sparsely; well" name="pubescence" src="d0_s1" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves similar, smaller.</text>
      <biological_entity constraint="basal" id="o22165" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o22166" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly (–2+).</text>
      <biological_entity id="o22167" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character name="quantity" src="d0_s3" value="2+]" value_original="2+]" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric or broader, 35–60+ mm diam.</text>
      <biological_entity id="o22168" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s4" value="broader" value_original="broader" />
        <character char_type="range_value" from="35" from_unit="mm" name="diameter" src="d0_s4" to="60" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 22–24+, unequal, herbaceous, margins not ciliate, faces glabrous or abaxial finely stipitate-glandular;</text>
      <biological_entity id="o22169" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s5" to="24" upper_restricted="false" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o22170" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22171" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22172" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer (30–) 40–70 mm (foliaceous, much surpassing discs).</text>
      <biological_entity constraint="outer" id="o22173" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (8–) 12–27;</text>
      <biological_entity id="o22174" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s7" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae 15–25 (–35) mm.</text>
      <biological_entity id="o22175" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 10–13 mm, puberulent and/or finely stipitate-glandular (at least distally).</text>
      <biological_entity id="o22176" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady sites, dry foothills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shady sites" />
        <character name="habitat" value="dry foothills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Wyethia glabra grows in the Coast Ranges, often in the fog belt.</discussion>
  
</bio:treatment>