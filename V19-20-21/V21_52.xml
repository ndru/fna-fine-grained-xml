<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
    <other_info_on_meta type="illustration_page">30</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">oxytenia</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="species">acerosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 20. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus oxytenia;species acerosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067228</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphrosyne</taxon_name>
    <taxon_name authority="(Nuttall) Panero" date="unknown" rank="species">acerosa</taxon_name>
    <taxon_hierarchy>genus Euphrosyne;species acerosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iva</taxon_name>
    <taxon_name authority="(Nuttall) R. C. Jackson" date="unknown" rank="species">acerosa</taxon_name>
    <taxon_hierarchy>genus Iva;species acerosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petioles 0–20+ mm;</text>
      <biological_entity id="o6537" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o6538" name="petiole" name_original="petioles" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades or lobes mostly 20–150 × 1–1.5 (–3) mm.</text>
      <biological_entity id="o6539" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6540" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="150" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6541" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="150" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 1–3+ mm.</text>
      <biological_entity id="o6542" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 2–3+ mm.</text>
      <biological_entity id="o6543" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 5–7 ± herbaceous, sericeous to strigillose, inner scarious to membranous, usually villous.</text>
      <biological_entity id="o6544" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o6545" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s4" to="strigillose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6546" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="scarious" name="texture" src="d0_s4" to="membranous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Functionally staminate florets: corollas 2.5–3 mm.</text>
      <biological_entity id="o6547" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6548" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae 1.8–2+ mm.</text>
      <biological_entity id="o6549" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1.5–2.5 mm. 2n = 36.</text>
      <biological_entity id="o6550" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6551" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or seasonally wet, alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline soils" modifier="wet or seasonally wet" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>