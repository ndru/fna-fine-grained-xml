<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="mention_page">570</other_info_on_meta>
    <other_info_on_meta type="treatment_page">568</other_info_on_meta>
    <other_info_on_meta type="illustration_page">569</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">jacobaea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 870. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species jacobaea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242348344</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Jacobaea</taxon_name>
    <taxon_name authority="Gaertner" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_hierarchy>genus Jacobaea;species vulgaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–80 (–100) cm (taprooted or branched caudices surmounting taproots).</text>
      <biological_entity id="o20461" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage sparsely and unevenly tomentose, glabrescent except in leaf-axils and among heads.</text>
      <biological_entity id="o20462" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character constraint="except-in leaf-axils and among heads" constraintid="o20463" is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o20463" name="head" name_original="heads" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems (often purplish-tinged) usually single, sometimes loosely clustered.</text>
      <biological_entity id="o20464" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="sometimes loosely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ± evenly distributed (basal often withering before flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (sometimes obscurely);</text>
      <biological_entity id="o20465" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ovate to broadly ovate (usually 1–3-pinnate, lobes mostly obovate to spatulate), (4–) 7–20 (–30) × (1–) 2–5 (–12) cm, bases usually tapered, ultimate margins dentate (distal leaves similar, smaller).</text>
      <biological_entity id="o20466" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="broadly ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s5" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20467" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o20468" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (10–) 20–60+ in corymbiform arrays.</text>
      <biological_entity id="o20469" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s6" to="20" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o20470" from="20" name="quantity" src="d0_s6" to="60" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o20470" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 2–6 (inconspicuous) bractlets (less than 2 mm).</text>
      <biological_entity id="o20471" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o20472" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <relation from="o20471" id="r1898" name="consist_of" negation="false" src="d0_s7" to="o20472" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 13, 3–4 (–5) mm, tips black or greenish.</text>
      <biological_entity id="o20473" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20474" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets ± 13;</text>
      <biological_entity id="o20475" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 8–12 mm.</text>
      <biological_entity constraint="corolla" id="o20476" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae all sparsely hairy or ray cypselae glabrous.</text>
      <biological_entity id="o20477" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity constraint="ray" id="o20478" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20479" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer(–fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, pastures, roadsides, and waste grounds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste grounds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Calif., Idaho, Ill., Maine, Mass., Mich., Mont., N.J., N.Y., Oreg., Pa., Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="common_name">Tansy ragwort</other_name>
  <other_name type="common_name">stinking Willie</other_name>
  <discussion>Senecio jacobaea is a weed introduced from Europe and now well established in places of cool, damp summers. It is toxic to livestock and legally noxious in most states and provinces where it occurs.</discussion>
  <discussion>The Russian botanist E. Wiebe (2000) resuscitated Jacobaea for plants that are treated here as Senecio jacobaea, S. erucifolius, and S. cannabifolius. Phylogenetic studies may confirm the utility of recognizing Jacobaea as a distinct genus; to do so here would be premature.</discussion>
  
</bio:treatment>