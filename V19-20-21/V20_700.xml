<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Greene" date="1888" rank="species">petrophilus</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="1992" rank="variety">viscidulus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>72: 198. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species petrophilus;variety viscidulus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068356</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">inornatus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">viscidulus</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 215. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species inornatus;variety viscidulus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems and leaves eglandular or obscurely glandular, nonglandular hairs mostly stiff, straight or curved.</text>
      <biological_entity id="o18749" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18750" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18751" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s0" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Phyllary apices not purplish or differently colored from proximal portion.</text>
      <biological_entity constraint="phyllary" id="o18752" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character constraint="from proximal portion" constraintid="o18753" is_modifier="false" modifier="differently" name="coloration" src="d0_s1" value="colored" value_original="colored" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18753" name="portion" name_original="portion" src="d0_s1" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open rocky slopes, ledges, talus, sometimes on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open rocky slopes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1200–)1500–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2700" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>102c.</number>
  <discussion>The cauline vestiture of var. viscidulus is variable, of stiff hairs primarily spreading to slightly deflexed, varying to antrorsely appressed (perhaps suggesting introgression from Erigeron inornatus). Apparent intergrades with E. reductus var. reductus occur in Trinity County.</discussion>
  
</bio:treatment>