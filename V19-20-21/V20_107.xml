<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(L. C. Anderson) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">iridis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 86. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety iridis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068279</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="L. C. Anderson" date="unknown" rank="subspecies">iridis</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>41: 311. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies iridis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(L. C. Anderson) S. L. Welsh" date="unknown" rank="variety">iridis</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;variety iridis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 cm.</text>
      <biological_entity id="o5249" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems grayish green (at least when young), leafy, faces ± compactly tomentose.</text>
      <biological_entity id="o5250" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o5251" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less compactly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green;</text>
      <biological_entity id="o5252" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, linear, 15–40 × 0.8–1 mm, compactly tomentose.</text>
      <biological_entity id="o5253" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="compactly" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 12–16 mm.</text>
      <biological_entity id="o5254" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 20–26, apices erect, acute to acuminate, abaxial faces glabrous.</text>
      <biological_entity id="o5255" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s5" to="26" />
      </biological_entity>
      <biological_entity id="o5256" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5257" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7.8–8.7 mm, tubes glabrous, lobes 1.5–2 mm, glabrous;</text>
      <biological_entity id="o5258" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.8" from_unit="mm" name="some_measurement" src="d0_s6" to="8.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5259" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5260" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o5261" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o5262" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o5262" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae glabrous;</text>
      <biological_entity id="o5263" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 5.2–5.8 mm. 2n = 18.</text>
      <biological_entity id="o5264" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s9" to="5.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5265" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep slopes on ± barren, gypsiferous, Arapien shale</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep slopes" constraint="on ± barren , gypsiferous ," />
        <character name="habitat" value="gypsiferous" modifier="barren" />
        <character name="habitat" value="shale" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21g.</number>
  <other_name type="common_name">Rainbow Hills rabbitbrush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety iridis is known only from Sevier County.</discussion>
  
</bio:treatment>