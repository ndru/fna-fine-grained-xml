<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">449</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="genus">tetraneuris</taxon_name>
    <taxon_name authority="(de Candolle) Greene" date="1898" rank="species">scaposa</taxon_name>
    <taxon_name authority="(K. F. Parker) K. F. Parker" date="1980" rank="variety">argyrocaulon</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>45: 467. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus tetraneuris;species scaposa;variety argyrocaulon</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068877</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenoxys</taxon_name>
    <taxon_name authority="(de Candolle) K. F. Parker" date="unknown" rank="species">scaposa</taxon_name>
    <taxon_name authority="K. F. Parker" date="unknown" rank="variety">argyrocaulon</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>20: 192. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenoxys;species scaposa;variety argyrocaulon;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 17–40+ cm.</text>
      <biological_entity id="o21169" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–20, densely woolly among proximal leaves.</text>
      <biological_entity id="o21170" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" />
        <character constraint="among proximal leaves" constraintid="o21171" is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21171" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves not crowded (internodes evident);</text>
      <biological_entity id="o21172" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear, linear-oblanceolate, or oblanceolate, ± glanddotted.</text>
      <biological_entity id="o21173" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 6–10 × 9–12 mm.</text>
      <biological_entity id="o21174" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray corollas (7.4–) 9.5–14 mm.</text>
      <biological_entity constraint="ray" id="o21175" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="7.4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="9.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corollas 2.5–3 mm. 2n = 30.</text>
      <biological_entity constraint="disc" id="o21176" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21177" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr(–Jun; also Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" constraint=" (-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  
</bio:treatment>