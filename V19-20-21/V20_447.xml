<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="L. M. Shultz &amp; A. H. Holmgren" date="1980" rank="species">smithii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>32: 144, fig. 1. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species smithii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067779</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–8 (–12+) cm (usually ± pulvinate).</text>
      <biological_entity id="o7735" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o7736" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–1 mm, ± scabrellous.</text>
      <biological_entity id="o7737" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, ± spatulate to oblanceolate, 15–50 (–80) × 2–6 (–11) mm, little, if at all, fleshy, faces ± scabrellous.</text>
      <biological_entity id="o7738" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="less spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" constraint="at all" from="2" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o7739" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads on scapiform peduncles 30–80 (–120) mm.</text>
      <biological_entity id="o7740" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o7741" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="scapiform" value_original="scapiform" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="80" to_unit="mm" />
      </biological_entity>
      <relation from="o7740" id="r705" name="on" negation="false" src="d0_s4" to="o7741" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric to campanulate, 10–15 mm diam.</text>
      <biological_entity id="o7742" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="less hemispheric" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 14–26 in 2–3+ series, the longer lanceovate to lanceolate, 9–11 mm (l/w = 3–5), apices attenuate, abaxial faces piloso-strigose to strigose.</text>
      <biological_entity id="o7743" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o7744" from="14" name="quantity" src="d0_s6" to="26" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7744" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7745" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7746" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="piloso-strigose" name="pubescence" src="d0_s6" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 12–21;</text>
      <biological_entity id="o7747" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white adaxially, laminae 6–12+ mm, glabrous abaxially.</text>
      <biological_entity id="o7748" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7749" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 60–80+;</text>
      <biological_entity id="o7750" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s9" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–5 mm.</text>
      <biological_entity id="o7751" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–3.5 mm, faces hairy, hair tips entire;</text>
      <biological_entity id="o7752" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7753" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o7754" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent, 16–20+ lanceolate to subulate scales 0.2–0.8 mm (± connate).</text>
      <biological_entity id="o7755" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="16" name="quantity" src="d0_s12" to="20" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o7756" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7757" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic soils, pinyon or yellow pines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic soils" />
        <character name="habitat" value="pinyon" />
        <character name="habitat" value="yellow pines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>