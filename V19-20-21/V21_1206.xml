<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="treatment_page">480</other_info_on_meta>
    <other_info_on_meta type="illustration_page">480</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">conoclinium</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">coelestinum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 135. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus conoclinium;species coelestinum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220003245</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">coelestinum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 838. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species coelestinum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conoclinium</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">dichotomum</taxon_name>
    <taxon_hierarchy>genus Conoclinium;species dichotomum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually erect, sometimes decumbent or procumbent (rooting at nodes).</text>
      <biological_entity id="o1380" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades triangular to deltate or ovate, 2–7 (–13) cm, bases usually cuneate to truncate, rarely subcordate, margins serrate to serrate-dentate or crenate, apices acute.</text>
      <biological_entity id="o1381" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s1" to="deltate or ovate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s1" to="13" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1382" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="usually cuneate" name="shape" src="d0_s1" to="truncate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o1383" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="serrate-dentate" value_original="serrate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o1384" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 3.5–4 mm.</text>
      <biological_entity id="o1385" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Corollas blue to blue-violet or rosy-violet, (1.6–) 2–2.5 mm.</text>
      <biological_entity id="o1386" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s3" to="blue-violet or rosy-violet" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 1–1.5 mm, glabrous;</text>
      <biological_entity id="o1387" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi: bristle tips not dilated.</text>
      <biological_entity id="o1388" name="pappus" name_original="pappi" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 20.</text>
      <biological_entity constraint="bristle" id="o1389" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1390" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, lakeshores, coastal and inland sands, pine-oak woodlands, low woods, flood plains, ditches, ravines, bogs, wet slopes, pine savannas, disturbed sites, roadsides, along railroads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="coastal" />
        <character name="habitat" value="sands" modifier="inland" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="low woods" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet slopes" />
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroads" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Del., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Mich., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Blue mistflower</other_name>
  
</bio:treatment>