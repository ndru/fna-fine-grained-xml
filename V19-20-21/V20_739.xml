<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">327</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">pulchellus</taxon_name>
    <taxon_name authority="Fernald" date="1936" rank="variety">brauniae</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>38: 235. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species pulchellus;variety brauniae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068363</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous.</text>
      <biological_entity id="o13863" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves margins ciliate, faces glabrous.</text>
      <biological_entity constraint="leaves" id="o13864" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13865" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ray corollas blue to pinkish.</text>
      <biological_entity constraint="ray" id="o13866" name="corolla" name_original="corollas" src="d0_s2" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s2" to="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Disc corollas 4.5–6 mm.</text>
      <biological_entity constraint="disc" id="o13867" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae glabrous or glabrate.</text>
      <biological_entity id="o13868" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic slopes and alluvial forests, sandstone soils on or near the western margin of the Appalachian Plateau (most abundant in the Cliff Section of the Cumberland Plateau)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic slopes" />
        <character name="habitat" value="alluvial forests" />
        <character name="habitat" value="soils" modifier="sandstone" />
        <character name="habitat" value="the western margin" modifier="on or near" constraint="of the appalachian plateau ( most abundant in the cliff section of the cumberland plateau )" />
        <character name="habitat" value="cliff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., Ohio, W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>130a.</number>
  <discussion>Stems and leaves both are glabrous in the weakly delimited var. brauniae. Plants with glabrous leaf faces occur sporadically in various parts of the range of var. pulchellus (Georgia, Maryland, Pennsylvania, South Carolina, and West Virginia); in those plants, stems usually are sparsely hirsuto-villous.</discussion>
  
</bio:treatment>