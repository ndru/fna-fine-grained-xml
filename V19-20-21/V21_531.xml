<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Justin W. Allison</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Cavanilles" date="1795" rank="genus">HETEROSPERMA</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>3: 34. 1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus HETEROSPERMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, heteros, differing, and sperma, seed; probably alluding to the contrasting outer and inner cypselae</other_info_on_name>
    <other_info_on_name type="fna_id">115357</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–40 (–70+) cm (taprooted).</text>
      <biological_entity id="o24063" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (bases relatively thick), ascending to erect, branched distally or ± throughout (striate).</text>
      <biological_entity id="o24064" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="distally; distally; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o24066" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>obscurely petiolate or sessile;</text>
      <biological_entity id="o24065" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly pinnately lobed [undivided] (lobes 3–5, usually linear to filiform, sometimes lanceolate), ultimate margins usually entire, sometimes denticulate (often ciliate, at least proximally, apices acute), faces glabrous [hairy].</text>
      <biological_entity id="o24067" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o24068" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o24069" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, cymiform arrays.</text>
      <biological_entity id="o24070" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , cymiform arrays" />
      </biological_entity>
      <biological_entity id="o24071" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o24070" id="r1650" name="in" negation="false" src="d0_s6" to="o24071" />
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of (1–) 3–5+ erect, narrowly spatulate or linear to filiform, herbaceous bractlets (often surpassing phyllaries, margins usually hispid-ciliate).</text>
      <biological_entity id="o24072" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o24073" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s7" to="filiform" />
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <relation from="o24072" id="r1651" name="consist_of" negation="false" src="d0_s7" to="o24073" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric to obconic, 2–5 mm diam.</text>
    </statement>
    <statement id="d0_s9">
      <text>(larger in fruit).</text>
      <biological_entity id="o24074" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="obconic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 3–5+ in ± 2 series, distinct, erect, mostly oblong or ovate, equal, membranous.</text>
      <biological_entity id="o24075" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o24076" from="3" name="quantity" src="d0_s10" to="5" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24076" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles concave to flat, paleate;</text>
      <biological_entity id="o24077" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="concave" name="shape" src="d0_s11" to="flat" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleae similar to phyllaries, oblong or oval to lanceolate or linear (± embracing cypselae, ± hyaline with brown striae, apices obtuse, apiculate).</text>
      <biological_entity id="o24078" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character char_type="range_value" from="oval" name="shape" notes="" src="d0_s12" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o24079" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure" />
      <relation from="o24078" id="r1652" name="to" negation="false" src="d0_s12" to="o24079" />
    </statement>
    <statement id="d0_s13">
      <text>Ray-florets 1–3 [–8], pistillate, fertile;</text>
      <biological_entity id="o24080" name="ray-floret" name_original="ray-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="8" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pale-yellow [orange].</text>
      <biological_entity id="o24081" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Disc-florets 3–10 [–20+], bisexual, fertile;</text>
      <biological_entity id="o24082" name="disc-floret" name_original="disc-florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="20" upper_restricted="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="10" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>corollas ± yellow (at least distally), tubes shorter than funnelform throats, lobes 5, deltate.</text>
      <biological_entity id="o24083" name="corolla" name_original="corollas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o24084" name="tube" name_original="tubes" src="d0_s16" type="structure">
        <character constraint="than funnelform throats" constraintid="o24085" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24085" name="throat" name_original="throats" src="d0_s16" type="structure" />
      <biological_entity id="o24086" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s16" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae (± dimorphic) outer obcompressed, ellipsoid or obovoid, winged (wings ± corky, often cucullate and/or pectinate), faces often corky-tuberculate, glabrous, inner ± obovoid, usually some (innermost) tapered to ± barbellate beaks;</text>
      <biological_entity id="o24087" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="outer" value_original="outer" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o24088" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="often" name="relief" src="d0_s17" value="corky-tuberculate" value_original="corky-tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s17" value="inner" value_original="inner" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s17" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o24089" name="beak" name_original="beaks" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi 0, or persistent or tardily falling, of (1–) 2–3 spreading to reflexed, retrorsely barbellate awns.</text>
      <biological_entity id="o24091" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s18" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s18" to="3" />
        <character char_type="range_value" from="spreading" is_modifier="true" name="orientation" src="d0_s18" to="reflexed" />
        <character is_modifier="true" modifier="retrorsely" name="architecture" src="d0_s18" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o24090" id="r1653" name="consist_of" negation="false" src="d0_s18" to="o24091" />
    </statement>
    <statement id="d0_s19">
      <text>x = 25.</text>
      <biological_entity id="o24090" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character name="presence" src="d0_s18" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s18" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o24092" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="25" value_original="25" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America, South America; introduced in West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in West Indies" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>313.</number>
  <discussion>Species 5–10 (1 in the flora).</discussion>
  
</bio:treatment>