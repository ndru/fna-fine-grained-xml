<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="(de Candolle) O. Hoffmann in H. G. A. Engler and K. Prantl" date="1891" rank="section">leptosyne</taxon_name>
    <taxon_name authority="(A. Gray) S. F. Blake" date="1913" rank="species">stillmanii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>49: 342. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section leptosyne;species stillmanii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066434</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptosyne</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">stillmanii</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>3: 91. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leptosyne;species stillmanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–20+ cm.</text>
      <biological_entity id="o19062" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades simple or 1 (–2) -pinnately lobed, terminal lobes spatulate to narrowly oblanceolate, 1–3 mm wide.</text>
      <biological_entity id="o19063" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="1(-2)-pinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o19064" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s1" to="narrowly oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 5–12 (–20+) cm.</text>
      <biological_entity id="o19065" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="20" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calyculi of 5–7+ oblong to linear bractlets 4–8 (–10) mm, ciliolate near bases.</text>
      <biological_entity id="o19066" name="calyculus" name_original="calyculi" src="d0_s3" type="structure">
        <character constraint="near bases" constraintid="o19068" is_modifier="false" name="pubescence" notes="" src="d0_s3" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o19067" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="7" upper_restricted="false" />
        <character char_type="range_value" from="oblong" is_modifier="true" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19068" name="base" name_original="bases" src="d0_s3" type="structure" />
      <relation from="o19066" id="r1296" name="consist_of" negation="false" src="d0_s3" to="o19067" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries (5–) 8, ovate to lanceovate, 4–5 (–7) mm.</text>
      <biological_entity id="o19069" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s4" value="8" value_original="8" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceovate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 5–8;</text>
      <biological_entity id="o19070" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae 8–12 (–15+) mm.</text>
      <biological_entity id="o19071" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 12–30+;</text>
      <biological_entity id="o19072" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 2.5–3.5 mm.</text>
      <biological_entity id="o19073" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ± obovate, 4–5 mm, not marked adaxially with red, wings corky-thickened, faces smooth or ± tuberculate, not hirtellous.</text>
      <biological_entity id="o19075" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s9" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o19076" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s9" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <relation from="o19074" id="r1297" modifier="adaxially" name="with" negation="true" src="d0_s9" to="o19075" />
      <relation from="o19074" id="r1298" modifier="adaxially" name="with" negation="false" src="d0_s9" to="o19076" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o19074" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s9" value="corky-thickened" value_original="corky-thickened" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s9" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19077" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, serpentine ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="serpentine ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>