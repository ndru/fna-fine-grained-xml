<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">70</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(J. T. Howell) G. L. Nesom" date="1990" rank="species">ophitidis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 153. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species ophitidis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066529</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(A. Gray) J. F. Macbride" date="unknown" rank="species">bloomeri</taxon_name>
    <taxon_name authority="J. T. Howell" date="unknown" rank="variety">ophitidis</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>6: 85. 1950 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species bloomeri;variety ophitidis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(J. T. Howell) D. D. Keck" date="unknown" rank="species">ophitidis</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species ophitidis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm.</text>
      <biological_entity id="o12926" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, green when young, becoming reddish-brown, intricately branched, glabrous or sparsely hairy, sometimes resinous.</text>
      <biological_entity id="o12927" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending to spreading;</text>
      <biological_entity id="o12928" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear (slightly concave), usually recurved, 5–15 × 0.5–1.5 mm, midnerves evident abaxially, apices acute, usually mucronate, faces glabrous or sparsely hairy, gland-dotted (not in deep pits), ± resinous;</text>
      <biological_entity id="o12929" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12930" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o12931" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles of 4–7 leaves usually present.</text>
      <biological_entity id="o12932" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="more or less" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o12933" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or (2–7) in cymiform arrays (clusters 3–20 mm).</text>
      <biological_entity id="o12934" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o12935" from="2" name="atypical_quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o12935" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–10 mm (glabrous or with conic hairs, resinous).</text>
      <biological_entity id="o12936" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres subcylindric, 10–15 × 3–7 mm.</text>
      <biological_entity id="o12937" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 18–30 in 5–7 series, tan, ovate to elliptic, 2–12 × 1–2.5 mm, unequal, mostly chartaceous, occasionally herbaceous-tipped (mid bodies apically obtuse to truncate), appendages usually herbaceous, often spreading to recurved, midnerves faint, (margins membranous, fimbriate distally, otherwise sometimes ciliolate) apices acute to cuspidate, abaxial faces mostly resinous.</text>
      <biological_entity id="o12938" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o12939" from="18" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s8" value="herbaceous-tipped" value_original="herbaceous-tipped" />
      </biological_entity>
      <biological_entity id="o12939" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o12940" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character char_type="range_value" from="spreading" modifier="often" name="orientation" src="d0_s8" to="recurved" />
      </biological_entity>
      <biological_entity id="o12941" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o12942" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="cuspidate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12943" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o12944" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 5–6;</text>
      <biological_entity id="o12945" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 9–10.5 mm.</text>
      <biological_entity id="o12946" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan to brown, turbinate to narrowly oblanceolate, 5–7 mm (ribs 5–7), moderately hairy distally;</text>
      <biological_entity id="o12947" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="brown" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s12" to="narrowly oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="moderately; distally" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi off-white to brown, 9–10.5 mm.</text>
      <biological_entity id="o12948" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s13" to="brown" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous forest, usually on serpentine soil, Arid Transition Zone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forest" modifier="open" />
        <character name="habitat" value="serpentine soil" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Serpentine goldenbush</other_name>
  <discussion>Ericameria ophitidisis is known from northern California.</discussion>
  
</bio:treatment>