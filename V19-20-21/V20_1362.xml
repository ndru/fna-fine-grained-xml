<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">607</other_info_on_meta>
    <other_info_on_meta type="illustration_page">606</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="D. Don in R. Sweet" date="1834" rank="genus">pericallis</taxon_name>
    <taxon_name authority="(Regel) B. Nordenstam" date="1978" rank="species">hybrida</taxon_name>
    <place_of_publication>
      <publication_title>Opera Bot.</publication_title>
      <place_in_publication>44: 21. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus pericallis;species hybrida</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067306</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="species">hybridus</taxon_name>
    <place_of_publication>
      <publication_title>Gartenflora</publication_title>
      <place_in_publication>8: 310. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species hybridus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cineraria</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">hybrida</taxon_name>
    <place_of_publication>
      <place_in_publication>1809</place_in_publication>
      <other_info_on_pub>not Bernhardi 1800</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Cineraria;species hybrida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Short-lived perennials or facultative annuals, 20–80 cm.</text>
      <biological_entity id="o28164" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="true" name="growth_form" src="d0_s0" value="facultative" value_original="facultative" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles ± lengths of blades, often with expanded and clasping bases;</text>
      <biological_entity id="o28166" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o28167" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <biological_entity id="o28168" name="blade" name_original="blades" src="d0_s1" type="structure" />
      <biological_entity id="o28169" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="more_or_les_length" src="d0_s1" value="expanded" value_original="expanded" />
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
      </biological_entity>
      <relation from="o28167" id="r2612" modifier="often" name="with" negation="false" src="d0_s1" to="o28169" />
    </statement>
    <statement id="d0_s2">
      <text>blades 6–12 × 6–12 cm, margins wavy to dentate (distal leaves smaller, bractlike).</text>
      <biological_entity id="o28170" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o28171" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s2" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28172" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="wavy" name="shape" src="d0_s2" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 4–6 mm.</text>
      <biological_entity id="o28173" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets: corollas white or bicolored, proximal 1/3–2/3 of laminae white, the rest purplish to reddish, pinkish, or bluish.</text>
      <biological_entity id="o28174" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure" />
      <biological_entity id="o28175" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of laminae" constraintid="o28176" from="1/3" name="quantity" src="d0_s4" to="2/3" />
        <character char_type="range_value" from="purplish" name="coloration" notes="" src="d0_s4" to="reddish pinkish or bluish" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s4" to="reddish pinkish or bluish" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s4" to="reddish pinkish or bluish" />
      </biological_entity>
      <biological_entity id="o28176" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 2.5–3.5 mm.</text>
      <biological_entity constraint="disc" id="o28177" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae glabrous or puberulent between ridges;</text>
      <biological_entity id="o28178" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="between ridges" constraintid="o28179" is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o28179" name="ridge" name_original="ridges" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ray cypselae slightly larger and lighter colored than the brown to black disc cypselae (pappi of ray-florets 0 or 2 subulate or setiform scales).</text>
      <biological_entity constraint="disc" id="o28181" name="cypsela" name_original="cypselae" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 60.</text>
      <biological_entity constraint="ray" id="o28180" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="larger" value_original="larger" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lighter colored" value_original="lighter colored" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s7" to="black" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28182" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Atlantic Islands (Canary Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Florists’ cineraria</other_name>
  <discussion>Pericallis hybrida has escaped from cultivation and is established in the cool, damp climate of the San Francisco Bay region. It is a complex of true-breeding cultivars derived from wild progenitors native to the Canary Islands (T. M. Barkley 1966); it is not a naturally occurring species. The cultivars are widely grown in glass houses and are sold as cool-season pot-plants in the horticultural trade.</discussion>
  <discussion>The name Senecio cruentus (L’Heretier) de Candolle has been widely misapplied to Pericallis hybrida in the horticultural trade.</discussion>
  
</bio:treatment>