<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="mention_page">404</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="treatment_page">405</other_info_on_meta>
    <other_info_on_meta type="illustration_page">406</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="section">macrocarphus</taxon_name>
    <taxon_name authority="(Hooker) Hooker &amp; Arnott" date="1839" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>354. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section macrocarphus;species douglasii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenopappus</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 316. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenopappus;species douglasii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Macrocarphus</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_hierarchy>genus Macrocarphus;species douglasii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, (2–) 5–50 (–60) cm (rarely slightly woody or flowering first-year, sometimes cespitose or ± matted);</text>
      <biological_entity id="o9618" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal indument thinning with age, grayish, mostly arachnoid-sericeous to thinly lanuginose.</text>
      <biological_entity constraint="proximal" id="o9620" name="indument" name_original="indument" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="mostly arachnoid-sericeous" name="pubescence" src="d0_s1" to="thinly lanuginose" />
      </biological_entity>
      <biological_entity id="o9621" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o9620" id="r666" name="thinning with" negation="false" src="d0_s1" to="o9621" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–25+, erect to spreading.</text>
      <biological_entity id="o9622" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="25" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, or basal (sometimes withering) and ± cauline, (1–) 2–12 (–15) cm;</text>
      <biological_entity id="o9623" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest blades ± elliptic or slightly lanceolate to ovate, ± 3-dimensional, usually 2-pinnately lobed;</text>
      <biological_entity constraint="largest" id="o9624" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="less elliptic or slightly lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="more or less; usually 2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lobes (4–) 5–9 (–12) pairs, ± congested, scarcely imbricate, ultimate lobes ± involute and/or twisted.</text>
      <biological_entity constraint="primary" id="o9625" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="12" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o9626" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–25+ per stem.</text>
      <biological_entity id="o9627" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o9628" from="1" name="quantity" src="d0_s6" to="25" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9628" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles mostly ascending to erect, 1–10 cm.</text>
      <biological_entity id="o9629" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="mostly ascending" name="orientation" src="d0_s7" to="erect" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres obconic to ± hemispheric.</text>
      <biological_entity id="o9630" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s8" to="more or less hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries: longest 9–15 (–17) mm;</text>
      <biological_entity id="o9631" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="longest" value_original="longest" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer usually stipitate-glandular (sometimes sparsely or obscurely, rarely eglandular) and, often, arachnoid to lanuginose and, sometimes, sparsely villous, apices usually ± squarrose, pliant.</text>
      <biological_entity id="o9632" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o9633" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character char_type="range_value" from="arachnoid" modifier="often" name="pubescence" src="d0_s10" to="lanuginose" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o9634" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="orientation" src="d0_s10" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="fragility_or_texture" src="d0_s10" value="pliant" value_original="pliant" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Corollas 5–8 mm.</text>
      <biological_entity id="o9635" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 5–8 mm (usually sparsely glandular amidst other indument);</text>
      <biological_entity id="o9636" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: longest scales 3–6 mm.</text>
      <biological_entity id="o9637" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="longest" id="o9638" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Dak., N.Mex., Nev., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Hoary pincushion</other_name>
  <other_name type="common_name">Douglas’s dustymaiden</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Chaenactis douglasii is widespread and variable (see discussion under var. douglasii).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves basal (sometimes withering) and ± cauline; plants not or scarcely cespitose, not matted; stems usually 1–5(–12); heads (1–)2–25+ per stem</description>
      <determination>6a Chaenactis douglasii var. douglasii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves strictly basal; plants cespitose or ± matted; stems (1–)10–25+; heads 1(–2) per stem</description>
      <determination>6b Chaenactis douglasii var. alpina</determination>
    </key_statement>
  </key>
</bio:treatment>