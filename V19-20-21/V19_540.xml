<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kenton L. Chambers</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="K. L. Chambers" date="1991" rank="genus">STEBBINSOSERIS</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>78: 1024. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus STEBBINSOSERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">For G. Ledyard Stebbins, 1906–2000, California botanist</other_info_on_name>
    <other_info_on_name type="fna_id">315041</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="D. Don" date="unknown" rank="genus">Microseris</taxon_name>
    <taxon_name authority="(Nuttall) K. L. Chambers" date="unknown" rank="section">Brachycarpa</taxon_name>
    <taxon_hierarchy>genus Microseris;section Brachycarpa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Uropappus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="section">Brachycarpa</taxon_name>
    <taxon_hierarchy>genus Uropappus;section Brachycarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–10 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o18345" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0, or erect, mostly unbranched, glabrous or lightly scurfy-puberulent.</text>
      <biological_entity id="o18346" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="lightly" name="pubescence" src="d0_s2" value="scurfy-puberulent" value_original="scurfy-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually all basal;</text>
      <biological_entity constraint="basal" id="o18348" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles narrowly attenuate, usually scurfy-puberulent, especially proximally);</text>
      <biological_entity id="o18347" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear to narrowly oblanceolate, bases slightly clasping, margins entire or irregularly dentate or lobed (teeth and lobes narrow, acute, straight or arcuate, faces glabrous or minutely scurfy-puberulent).</text>
      <biological_entity id="o18349" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly oblanceolate" />
      </biological_entity>
      <biological_entity id="o18350" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o18351" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="irregularly dentate or lobed" value_original="irregularly dentate or lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly (often inclined in bud, erect in flowering and fruit).</text>
      <biological_entity id="o18352" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not notably inflated, usually ebracteate (glabrous or ± scurfy-puberulent, especially distally).</text>
      <biological_entity id="o18353" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not notably" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of (3–) 4–14, deltate or ovate to lanceolate bractlets.</text>
      <biological_entity id="o18354" name="calyculus" name_original="calyculi" src="d0_s8" type="structure" />
      <biological_entity id="o18355" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="14" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <relation from="o18354" id="r1655" name="consist_of" negation="false" src="d0_s8" to="o18355" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, (3–) 5–35 mm diam.</text>
    </statement>
    <statement id="d0_s10">
      <text>(fusiform to ovoid in fruit).</text>
      <biological_entity id="o18356" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries (4–) 5–18 in ± 2 series, (green or purple) mostly lanceolate, subequal to equal, herbaceous, apices acute, faces glabrous.</text>
      <biological_entity id="o18357" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o18358" from="5" name="quantity" src="d0_s11" to="18" />
        <character is_modifier="false" modifier="mostly" name="shape" notes="" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s11" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o18358" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18359" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o18360" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles flat, ± pitted, glabrous, epaleate.</text>
      <biological_entity id="o18361" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Florets (10–) 30–125;</text>
      <biological_entity id="o18362" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s13" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="125" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow or white, outer often purplish abaxially (equaling or surpassing phyllaries by 1–3 mm).</text>
      <biological_entity id="o18363" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="position" src="d0_s14" value="outer" value_original="outer" />
        <character is_modifier="false" modifier="often; abaxially" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae brown, purplish gray, stramineous, or violet, sometimes purple-spotted, columnar or truncate-fusiform, not beaked, ribs 10, ± scabrellous or spiculate, faces glabrous or (on outer) strigose;</text>
      <biological_entity id="o18364" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish gray" value_original="purplish gray" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="violet" value_original="violet" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s15" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" name="shape" src="d0_s15" value="columnar" value_original="columnar" />
        <character is_modifier="false" name="shape" src="d0_s15" value="truncate-fusiform" value_original="truncate-fusiform" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o18365" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s15" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="relief" src="d0_s15" value="spiculate" value_original="spiculate" />
      </biological_entity>
      <biological_entity id="o18366" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 5, usually yellowish or brownish, rarely white, aristate scales (bodies straight or arcuate, lanceolate, usually glabrous, margins plane or involute, apices erose or notched, aristae shorter than to equaling bodies, barbellulate).</text>
      <biological_entity id="o18367" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s16" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>x = 18.</text>
      <biological_entity id="o18368" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity constraint="x" id="o18369" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>69.</number>
  <other_name type="common_name">Silverpuffs</other_name>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Stebbinsoseris comprises two allotetraploid species derived from hybrids between Microseris and Uropappus. The justification for raising these species to generic rank was given in R. K. Jansen et al. (1991b), where molecular data were presented supporting the separation of Uropappus from Microseris. Stebbinsoseris and Uropappus were ranked as sections of Microseris in earlier taxonomic treatments (K. L. Chambers 1955, 1960). Because of its hybrid origin, Stebbinsoseris is intermediate in critical taxonomic traits of habit, involucre, and fruits (C. Irmler et al. 1982), and it is separated from its parental taxa by rather minor differences. Chloroplast DNA studies show that Microseris douglasii and M. bigelovii were the maternal parents of the tetraploid hybrids (R. S. Wallace and R. K. Jansen 1990). That Uropappus lindleyi was the staminate parent of those crosses is confirmed by nuclear rDNA evidence reported by Wallace and Jansen (1995).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cypselae narrowly truncate-fusiform, brown to purplish, 5–8 mm, apices not widened at bases of pappi; pappus scale bodies 3–5 mm; coastal central California</description>
      <determination>1 Stebbinsoseris decipiens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cypselae narrowly truncate-fusiform to columnar, gray to pale brown or violet (dark purplish in sw California), 4.5–12 mm, apices slightly widened at bases of pappi; pappus scale bodies 4–11 mm; widespread, rarely coastal except in sw California</description>
      <determination>2 Stebbinsoseris heterocarpa</determination>
    </key_statement>
  </key>
</bio:treatment>