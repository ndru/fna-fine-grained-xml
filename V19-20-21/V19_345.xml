<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="treatment_page">258</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1824" rank="genus">MULGEDIUM</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 33: 296. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus MULGEDIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin mulgere, to milk, alluding to milky sap</other_info_on_name>
    <other_info_on_name type="fna_id">121301</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [annuals or biennials], 15–100+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>± rhizomatous.</text>
      <biological_entity id="o4884" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, usually erect, branched distally, glabrous or glabrate.</text>
      <biological_entity id="o4885" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline or mostly cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (basal) or sessile;</text>
      <biological_entity id="o4886" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblong, elliptic, or ovate to lanceolate or linear, margins entire or dentate to pinnately lobed (faces glabrous, often glaucous).</text>
      <biological_entity id="o4887" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate or linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o4888" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="dentate to pinnately lobed" value_original="dentate to pinnately lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o4889" name="head" name_original="heads" src="d0_s6" type="structure">
        <character constraint="in arrays" constraintid="o4890" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in corymbiform to paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o4890" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not inflated distally, usually bracteate.</text>
      <biological_entity id="o4891" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 3–13+, deltate to lanceolate bractlets (sometimes intergrading with phyllaries).</text>
      <biological_entity id="o4892" name="calyculus" name_original="calyculi" src="d0_s8" type="structure" />
      <biological_entity id="o4893" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="13" upper_restricted="false" />
        <character char_type="range_value" from="deltate" is_modifier="true" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <relation from="o4892" id="r479" name="consist_of" negation="false" src="d0_s8" to="o4893" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric, 2–5 [–8+] mm diam.</text>
      <biological_entity id="o4894" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 8–13+ in 1–2 series, lanceolate to linear, subequal to equal, margins little, if at all, scarious, apices acute.</text>
      <biological_entity id="o4895" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4896" from="8" name="quantity" src="d0_s10" to="13" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o4896" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o4897" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="at all" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4898" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat, pitted, glabrous, epaleate.</text>
      <biological_entity id="o4899" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets (10–) 15–50+;</text>
      <biological_entity id="o4900" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s12" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas usually bluish [yellow].</text>
      <biological_entity id="o4901" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="bluish" value_original="bluish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae reddish-brown to brown-mottled or slatey [blackish], bodies ± compressed, lanceoloid, beaks 0 (or gradually set off from and ± concolorous with bodies), ribs 4–6 on each face, faces glabrous [scabrid];</text>
      <biological_entity id="o4902" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s14" to="brown-mottled or slatey" />
      </biological_entity>
      <biological_entity id="o4903" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceoloid" value_original="lanceoloid" />
      </biological_entity>
      <biological_entity id="o4904" name="beak" name_original="beaks" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4905" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="on face" constraintid="o4906" from="4" name="quantity" src="d0_s14" to="6" />
      </biological_entity>
      <biological_entity id="o4906" name="face" name_original="face" src="d0_s14" type="structure" />
      <biological_entity id="o4907" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent (borne on discs at tips of cypselae or beaks), of 80–120+, whitish, ± equal, barbellulate to nearly smooth bristles in 2–3+ series.</text>
      <biological_entity id="o4909" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="80" is_modifier="true" name="quantity" src="d0_s15" to="120" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="whitish" value_original="whitish" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s15" value="equal" value_original="equal" />
        <character char_type="range_value" from="barbellulate" is_modifier="true" name="architecture" src="d0_s15" to="nearly smooth" />
      </biological_entity>
      <biological_entity id="o4910" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="3" upper_restricted="false" />
      </biological_entity>
      <relation from="o4908" id="r480" name="consist_of" negation="false" src="d0_s15" to="o4909" />
      <relation from="o4909" id="r481" name="in" negation="false" src="d0_s15" to="o4910" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o4908" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o4911" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <discussion>Species 15 or so (1 in the flora).</discussion>
  <discussion>In referring a species long included in Lactuca to Mulgedium, I was influenced by K. Bremer (1994).</discussion>
  
</bio:treatment>