<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">astranthium</taxon_name>
    <taxon_name authority="(Michaux) Nuttall" date="1840" rank="species">integrifolium</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 312. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus astranthium;species integrifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220001299</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bellis</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 131. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bellis;species integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (biennials?), usually fibrous-rooted.</text>
      <biological_entity id="o5512" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect to decumbent-ascending.</text>
      <biological_entity id="o5513" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline 30–60 × 7–22 mm.</text>
      <biological_entity id="o5514" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s2" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 3.5–6 mm.</text>
      <biological_entity id="o5515" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets (8–) 16–26;</text>
      <biological_entity id="o5516" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s4" to="16" to_inclusive="false" />
        <character char_type="range_value" from="16" name="quantity" src="d0_s4" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla laminae white, occasionally drying with bluish midstripe abaxially, (6–) 8–17 mm.</text>
      <biological_entity constraint="corolla" id="o5517" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character constraint="with midstripe" constraintid="o5518" is_modifier="false" modifier="occasionally" name="condition" src="d0_s5" value="drying" value_original="drying" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s5" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5518" name="midstripe" name_original="midstripe" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="bluish" value_original="bluish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-floret corollas 2.5–3.7 mm.</text>
      <biological_entity constraint="disc-floret" id="o5519" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae (1.4–) 1.6–2 (–2.2) × 0.9–1.1 mm, faces minutely papillate-pebbly, linear striations barely discernible, usually glabrous, sometimes sparsely glochidiate-hairy distally.</text>
      <biological_entity id="o5520" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="atypical_length" src="d0_s7" to="1.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s7" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5521" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s7" value="papillate-pebbly" value_original="papillate-pebbly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 8.</text>
      <biological_entity id="o5522" name="striation" name_original="striations" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="barely" name="prominence" src="d0_s7" value="discernible" value_original="discernible" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence" src="d0_s7" value="glochidiate-hairy" value_original="glochidiate-hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5523" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky (limestone) banks and ridges, alluvial fields, stream banks and terraces, open juniper woods, glades, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="banks" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="alluvial fields" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="open juniper woods" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Miss., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Eastern western-daisy</other_name>
  <references>
    <reference>Nesom, G. L. 2005c. Taxonomic review of Astranthium integrifolium (Asteraceae: Astereae). Sida 21: 2015–2021.</reference>
  </references>
  
</bio:treatment>