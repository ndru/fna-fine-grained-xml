<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">thymophylla</taxon_name>
    <taxon_name authority="(de Candolle) Small" date="1903" rank="species">tenuiloba</taxon_name>
    <taxon_name authority="(A. Gray) Strother" date="1986" rank="variety">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>11: 378. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus thymophylla;species tenuiloba;variety wrightii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068888</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenatherum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 89. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenatherum;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Robinson" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(de Candolle) B. L. Robinson" date="unknown" rank="species">tenuiloba</taxon_name>
    <taxon_name authority="(A. Gray) Strother" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species tenuiloba;variety wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves linear to spatulate, usually entire, rarely with 1–5 toothlike lobes.</text>
      <biological_entity id="o10686" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s0" to="spatulate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10687" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="5" />
        <character is_modifier="true" name="shape" src="d0_s0" value="toothlike" value_original="toothlike" />
      </biological_entity>
      <relation from="o10686" id="r733" modifier="rarely" name="with" negation="false" src="d0_s0" to="o10687" />
    </statement>
    <statement id="d0_s1">
      <text>Pappi of 10–12 unequal, aristate scales to 3 mm. 2n = 16.</text>
      <biological_entity id="o10688" name="pappus" name_original="pappi" src="d0_s1" type="structure" />
      <biological_entity id="o10689" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s1" to="12" />
        <character is_modifier="true" name="size" src="d0_s1" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="shape" src="d0_s1" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10690" name="chromosome" name_original="" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="16" value_original="16" />
      </biological_entity>
      <relation from="o10688" id="r734" name="consist_of" negation="false" src="d0_s1" to="o10689" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, coastal grasslands, roadways</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="coastal grasslands" />
        <character name="habitat" value="roadways" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6d.</number>
  
</bio:treatment>