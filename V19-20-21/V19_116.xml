<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="treatment_page">138</other_info_on_meta>
    <other_info_on_meta type="illustration_page">138</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Nuttall) Jepson" date="1901" rank="species">occidentale</taxon_name>
    <taxon_name authority="(Harvey &amp; A. Gray) Jepson" date="1901" rank="variety">coulteri</taxon_name>
    <place_of_publication>
      <publication_title>Fl. W. Calif.,</publication_title>
      <place_in_publication>509. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species occidentale;variety coulteri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068195</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Harvey &amp; A. Gray" date="unknown" rank="species">coulteri</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 110. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cirsium;species coulteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect or bushy, usually 30–150+ cm, variably tomentose, sometimes ± glabrate.</text>
      <biological_entity id="o20814" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushy" value_original="bushy" />
        <character char_type="range_value" from="30" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" modifier="variably" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf faces gray-tomentose or adaxial ± glabrate.</text>
      <biological_entity constraint="leaf" id="o20816" name="face" name_original="faces" src="d0_s1" type="structure" constraint_original="adaxial leaf leaf">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="gray-tomentose" value_original="gray-tomentose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads sometimes in tight clusters at ends of peduncles, usually long-pedunculate, usually elevated well above proximal leaves.</text>
      <biological_entity id="o20817" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="long-pedunculate" value_original="long-pedunculate" />
        <character constraint="above proximal leaves" constraintid="o20820" is_modifier="false" modifier="usually" name="prominence" src="d0_s2" value="elevated" value_original="elevated" />
      </biological_entity>
      <biological_entity id="o20818" name="end" name_original="ends" src="d0_s2" type="structure" />
      <biological_entity id="o20819" name="peduncle" name_original="peduncles" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o20820" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o20817" id="r1878" modifier="in tight clusters" name="at" negation="false" src="d0_s2" to="o20818" />
      <relation from="o20818" id="r1879" name="part_of" negation="false" src="d0_s2" to="o20819" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres usually about as wide as tall, 4–5 cm diam., densely arachnoid with fine trichomes connecting tips of adjacent phyllaries.</text>
      <biological_entity id="o20821" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" modifier="as-wide-as tall" name="diameter" src="d0_s3" to="5" to_unit="cm" />
        <character constraint="with trichomes" constraintid="o20822" is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity id="o20822" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o20823" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <biological_entity id="o20824" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o20822" id="r1880" name="connecting" negation="false" src="d0_s3" to="o20823" />
      <relation from="o20821" id="r1881" name="consist_of" negation="false" src="d0_s3" to="o20824" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries subequal, outer ascending to spreading or reflexed, mid apices ascending to stiffly spreading, straight, acicular, usually 1.5–3 × 1–2 mm.</text>
      <biological_entity id="o20825" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o20826" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading or reflexed" />
      </biological_entity>
      <biological_entity constraint="mid" id="o20827" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="stiffly spreading" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acicular" value_original="acicular" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="usually" name="length" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="usually" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas light purple to rich reddish purple, usually 25–33 mm.</text>
      <biological_entity id="o20828" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="light purple" name="coloration_or_density" src="d0_s5" to="rich reddish purple" />
        <character char_type="range_value" from="25" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s5" to="33" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Mar–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal slopes and ridges, dunes, coastal scrub, grassland, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40b.</number>
  <other_name type="common_name">Coulter’s thistle</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety coulteri grows in the coastal zone of central and southern California. The epithet coulteri was for many years misapplied by California botanists. W. L. Jepson ([1923–1925]) used it at the varietal level for a range of plants that included parts of what I am calling vars. venustum and californicum. P. A. Munz (1959) and various other authors recognized Cirsium coulteri as a species, primarily in the context that I recognize as var. venustum. J. T. Howell (1959b) examined the type of C. coulteri and concluded that it and C. occidentale (in the strict sense) are synonymous. I have examined photos of the types of both C. occidentale and C. coulteri and have reached a different conclusion. The type of C. occidentale (BM) has heads with ± imbricate, comparatively short phyllary appendages. The type of C. coulteri (TCD) has long, acicular, subequal phyllaries. D. J. Keil and C. E. Turner (1993) treated the plants with the two head types all as var. occidentale, but I believe it is preferable to recognize separate varieties. Some intermediates are known, but var. coulteri is usually readily separable from var. occidentale.</discussion>
  
</bio:treatment>