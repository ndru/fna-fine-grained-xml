<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="treatment_page">279</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom &amp; T. W. Nelson" date="2004" rank="species">maniopotamicus</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 673, fig. 1. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species maniopotamicus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066631</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–22 (–27) cm, taprooted, caudices usually simple, rarely with branches to 20 cm.</text>
      <biological_entity id="o9831" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="27" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o9832" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o9833" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
      <relation from="o9832" id="r907" modifier="rarely" name="with" negation="false" src="d0_s0" to="o9833" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to decumbent-ascending (usually basally purplish), sparsely to densely strigose (basal-cells inclined, hairs even-width), eglandular.</text>
      <biological_entity id="o9834" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="decumbent-ascending" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o9835" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (strongly to weakly 3-nerved) oblanceolate to spatulate-oblanceolate, 30–100 × (3–) 5–12 (–14) mm, cauline gradually reduced distally or not, usually continuing to near heads, margins entire, faces hirsuto-pilose to weakly pilose, eglandular.</text>
      <biological_entity id="o9836" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="spatulate-oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9837" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually; distally; distally; not" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o9838" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o9839" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9840" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="hirsuto-pilose" name="pubescence" src="d0_s3" to="weakly pilose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o9837" id="r908" name="continuing to" negation="false" src="d0_s3" to="o9838" />
    </statement>
    <statement id="d0_s4">
      <text>Heads 1 (–4) (on peduncles 0.5–5 cm), held well beyond leaves at peak flowering, from branches near midstem (or slightly more distal).</text>
      <biological_entity id="o9841" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9842" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9843" name="peak" name_original="peak" src="d0_s4" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o9844" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <biological_entity id="o9845" name="midstem" name_original="midstem" src="d0_s4" type="structure" />
      <relation from="o9841" id="r909" name="held" negation="false" src="d0_s4" to="o9842" />
      <relation from="o9841" id="r910" name="at" negation="false" src="d0_s4" to="o9843" />
      <relation from="o9841" id="r911" name="from" negation="false" src="d0_s4" to="o9844" />
      <relation from="o9844" id="r912" name="near" negation="false" src="d0_s4" to="o9845" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres (5–) 6–7 × 9–12 (–14) mm.</text>
      <biological_entity id="o9846" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s5" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2–3 (–4) series, (elliptic to oblong-oblanceolate, abruptly acuminate), hirsuto-strigose to pilose, densely villous at bases, usually eglandular, sometimes sparsely glandular.</text>
      <biological_entity id="o9847" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="hirsuto-strigose" name="pubescence" notes="" src="d0_s6" to="pilose" />
        <character constraint="at bases" constraintid="o9849" is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9848" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o9849" name="base" name_original="bases" src="d0_s6" type="structure" />
      <relation from="o9847" id="r913" name="in" negation="false" src="d0_s6" to="o9848" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (16–) 21–33;</text>
      <biological_entity id="o9850" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="16" name="atypical_quantity" src="d0_s7" to="21" to_inclusive="false" />
        <character char_type="range_value" from="21" name="quantity" src="d0_s7" to="33" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white to pinkish or purplish, 10–12 mm, laminae not (or weakly) coiling or reflexing.</text>
      <biological_entity id="o9851" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pinkish or purplish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9852" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 3.2–3.8 mm.</text>
      <biological_entity constraint="disc" id="o9853" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2–2.5 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o9854" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o9855" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: outer of setae, inner of 16–20 bristles.</text>
      <biological_entity id="o9856" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o9857" name="seta" name_original="setae" src="d0_s11" type="structure" />
      <biological_entity id="o9858" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
      <relation from="o9856" id="r914" name="outer of" negation="false" src="d0_s11" to="o9857" />
      <relation from="o9856" id="r915" name="inner of" negation="false" src="d0_s11" to="o9858" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, barren meadows and openings in mixed conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren meadows" modifier="dry" constraint="in mixed conifer woodlands" />
        <character name="habitat" value="openings" constraint="in mixed conifer woodlands" />
        <character name="habitat" value="mixed conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Mad River fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>