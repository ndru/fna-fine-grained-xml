<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">503</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="treatment_page">531</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="S. F. Blake &amp; Cronquist" date="1950" rank="species">papposa</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>6: 43, plate 1. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species papposa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–15 (–20) cm (not cespitose), aromatic.</text>
      <biological_entity id="o15034" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems relatively numerous, erect, gray, simple (annual flowering branches leafy), loosely sericeous.</text>
      <biological_entity id="o15035" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (semideciduous) cauline (sessile), gray-green;</text>
      <biological_entity id="o15036" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate, 0.5–3 × 0.2–1.5 cm (bases attenuate), 3-lobed or irregularly palmatifid (lobes narrow, apices acute), sparsely sericeous-lanate.</text>
      <biological_entity id="o15037" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="palmatifid" value_original="palmatifid" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="sericeous-lanate" value_original="sericeous-lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (mostly erect, peduncles 0 or to 25 mm) in racemiform arrays (4–) 8–12 (–14) × (0.5–) 1–2 (–4) cm.</text>
      <biological_entity id="o15038" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" modifier="in racemiform arrays" name="atypical_length" src="d0_s4" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" modifier="in racemiform arrays" name="atypical_length" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" modifier="in racemiform arrays" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" modifier="in racemiform arrays" name="atypical_width" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" modifier="in racemiform arrays" name="atypical_width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="in racemiform arrays" name="width" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres globose, 3.5–5 × 4–5 mm.</text>
      <biological_entity id="o15039" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries ovate, sparsely sericeous.</text>
      <biological_entity id="o15040" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 8;</text>
      <biological_entity id="o15041" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 20–35;</text>
      <biological_entity id="o15042" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow (tubular with broad throats), ca. 2 mm, glandular.</text>
      <biological_entity id="o15043" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o15044" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (light-brown) oblanceoloid (4–5-angled, broadest at truncate apices), 0.3–0.5 mm, glandular-pubescent (pappi coroniform, 0.3–0.6 mm, irregularly lacerate).</text>
      <biological_entity id="o15045" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky swales, dry meadows, alkaline mud flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky swales" />
        <character name="habitat" value="dry meadows" />
        <character name="habitat" value="mud flats" modifier="alkaline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43.</number>
  <other_name type="common_name">Owyhee sage</other_name>
  <discussion>The pappose cypselae make Artemisia papposa anomalous within Artemisia. Artemisia papposa has capitulescence characteristics that suggest a relationship to Sphaeromeria.</discussion>
  
</bio:treatment>