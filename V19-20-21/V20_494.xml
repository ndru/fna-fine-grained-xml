<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">224</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">pityopsis</taxon_name>
    <taxon_name authority="(Nash) Small" date="1933" rank="species">flexuosa</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>1341. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pityopsis;species flexuosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067350</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="species">flexuosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 107. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species flexuosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Nash) V. L. Harms" date="unknown" rank="species">flexuosa</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species flexuosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–50 cm;</text>
      <biological_entity id="o23292" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1–5 cm.</text>
      <biological_entity id="o23293" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–6, ascending, sometimes reddish-brown, usually simple, flexuous, slender, moderately sericeous, glabrescent.</text>
      <biological_entity id="o23294" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="course" src="d0_s2" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s2" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal equaling or shorter than cauline, persistent through winter, withering by flowering;</text>
      <biological_entity id="o23295" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o23296" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character constraint="than cauline leaves" constraintid="o23297" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
        <character constraint="through winter" is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o23297" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cauline spreading to ascending, sessile, blades linear-lanceolate, 30–70 × 3–7 mm, apices acute, faces moderately sericeous, glabrescent;</text>
      <biological_entity id="o23298" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o23299" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o23300" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23301" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o23302" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal slightly reduced.</text>
      <biological_entity id="o23303" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o23304" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–20 per stem, in corymbiform arrays.</text>
      <biological_entity id="o23305" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o23306" from="3" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o23306" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <biological_entity id="o23307" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o23305" id="r2148" name="in" negation="false" src="d0_s6" to="o23307" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles sparsely bracteolate, 1–11 cm, tomentose, sparsely, minutely glandular;</text>
      <biological_entity id="o23308" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s7" value="bracteolate" value_original="bracteolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="11" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sparsely; minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles reduced distally.</text>
      <biological_entity id="o23309" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly campanulate, (7–) 8–11 mm (equaling pappi).</text>
      <biological_entity id="o23310" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–6 series, margins hyaline, sometimes reddish distally, fimbriate (midnerves pronounced), apices acute, faces sparsely pilose, eglandular or sparsely, minutely stipitate-glandular.</text>
      <biological_entity id="o23311" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity id="o23312" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o23313" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="sometimes; distally" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o23314" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o23315" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
        <character name="architecture" src="d0_s10" value="sparsely" value_original="sparsely" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o23311" id="r2149" name="in" negation="false" src="d0_s10" to="o23312" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 9–13;</text>
      <biological_entity id="o23316" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 5–8 mm.</text>
      <biological_entity id="o23317" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–45;</text>
      <biological_entity id="o23318" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 5.5–7 mm, tubes and proximal throats sparsely puberulent, lobes 0.5 mm, glabrous or glabrate.</text>
      <biological_entity id="o23319" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23320" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23321" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23322" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae fusiform, 3–4 mm, ribbed (dark between ribs), faces sparsely strigose;</text>
      <biological_entity id="o23323" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o23324" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi: outer of linear scales 0.3–1 mm, inner of 30–50 bristles 5.5–7 mm. 2n = 18.</text>
      <biological_entity id="o23325" name="pappus" name_original="pappi" src="d0_s16" type="structure" />
      <biological_entity id="o23326" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o23327" name="scale" name_original="scales" src="d0_s16" type="structure" />
      <biological_entity id="o23328" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s16" to="50" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23329" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
      <relation from="o23325" id="r2150" name="outer of" negation="false" src="d0_s16" to="o23326" />
      <relation from="o23327" id="r2151" name="consist_of" negation="false" src="d0_s16" to="o23328" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, open soils, open pine-oak woods, clearings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open soils" modifier="sandy" />
        <character name="habitat" value="open pine-oak woods" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Pityopsis flexuosa is known only from the vicinity of Tallahassee.</discussion>
  
</bio:treatment>