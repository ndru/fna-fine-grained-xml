<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1894" rank="species">hirta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">hirta</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species hirta;variety hirta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068670</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–40 cm, glabrate or sparsely villous, densely stipitate-glandular.</text>
      <biological_entity id="o9425" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal blades lanceolate, 40–80 (–160) × 10–30 mm, margins coarsely toothed to biserrate, faces villous to tomentose.</text>
      <biological_entity id="o9426" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o9427" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="160" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s1" to="80" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9428" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s1" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="biserrate" value_original="biserrate" />
      </biological_entity>
      <biological_entity id="o9429" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s1" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads 3–6.</text>
      <biological_entity id="o9430" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 7–11 × 8–15 mm.</text>
      <biological_entity id="o9431" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries loose, subequal, green throughout.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 12.</text>
      <biological_entity id="o9432" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="false" name="size" src="d0_s4" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9433" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or wet, rocky meadows, forest openings in pine and mixed hardwoods forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky meadows" modifier="dry or wet" />
        <character name="habitat" value="forest openings" constraint="in pine and mixed hardwoods" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="mixed hardwoods" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  <discussion>Variety hirta intergrades with var. sonchifolia in the northern part of its range.</discussion>
  
</bio:treatment>