<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">338</other_info_on_meta>
    <other_info_on_meta type="illustration_page">336</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Nelson" date="1934" rank="species">lobatus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>21: 580. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species lobatus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066628</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, mostly 10–40 (–50) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o22534" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" modifier="mostly" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely hispido-pilose (hairs spreading, straight, 0.6–2 mm), densely stipitate-glandular.</text>
      <biological_entity id="o22535" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispido-pilose" value_original="hispido-pilose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o22536" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline blades obovate-spatulate, 50–100 (–150) × 4–25 mm, cauline gradually reduced (lobing distally), margins pinnatifid or bipinnatifid with (1–) 2–4 pairs of rounded to acute lobes, faces sparsely hispido-pilose, stipitate-glandular.</text>
      <biological_entity constraint="basal and proximal cauline" id="o22537" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o22538" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o22539" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character constraint="with pairs" constraintid="o22540" is_modifier="false" name="shape" src="d0_s4" value="bipinnatifid" value_original="bipinnatifid" />
      </biological_entity>
      <biological_entity id="o22540" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o22541" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" is_modifier="true" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o22542" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hispido-pilose" value_original="hispido-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o22540" id="r2078" name="part_of" negation="false" src="d0_s4" to="o22541" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–5 (peduncles long, ebracteate).</text>
      <biological_entity id="o22543" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–4 × 6–10 mm.</text>
      <biological_entity id="o22544" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (sometimes basally connate, broad, thin) sparsely hispido-pilose, stipitate-glandular.</text>
      <biological_entity id="o22545" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o22546" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hispido-pilose" value_original="hispido-pilose" />
      </biological_entity>
      <relation from="o22545" id="r2079" name="in" negation="false" src="d0_s7" to="o22546" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 85–110;</text>
      <biological_entity id="o22547" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="85" name="quantity" src="d0_s8" to="110" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, drying dark blue, 6–9 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o22548" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark blue" value_original="dark blue" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22549" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 1.8–2.9 mm.</text>
      <biological_entity constraint="disc" id="o22550" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.2–1.4 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o22551" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o22552" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of scales (sometimes connate, forming crowns), inner of 11–12 bristles.</text>
      <biological_entity id="o22553" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o22554" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o22555" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" is_modifier="true" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <relation from="o22553" id="r2080" name="outer of" negation="false" src="d0_s12" to="o22554" />
      <relation from="o22553" id="r2081" name="inner of" negation="false" src="d0_s12" to="o22555" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan–)Feb–May(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, sandy lake shores, dry washes, desert or riparian scrub, sometime with creosote bush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="sandy lake shores" />
        <character name="habitat" value="dry washes" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="riparian" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="creosote bush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>155.</number>
  <other_name type="common_name">Lobed fleabane</other_name>
  <discussion>Erigeron lobatus is characterized by persistent basal and proximal cauline leaves with rounded to acute lobes, vestiture of stipitate glands and sparse, spreading, hispido-pilose hairs, heads on relatively long, ebracteate peduncles, and broad, thin phyllaries. Erigeron divergens often is similar; its glandularity is not stipitate and its nonglandular hairs are shorter and denser.</discussion>
  
</bio:treatment>