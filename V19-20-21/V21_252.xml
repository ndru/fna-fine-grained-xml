<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">106</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERBESINA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 901. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 384. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus VERBESINA</taxon_hierarchy>
    <other_info_on_name type="etymology">No etymology in protologue; perhaps from genus name Verbena and Latin - ina, resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">134477</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials [shrubs, trees], 7–15+ cm (Verbesina nana) or 30–200 (–400) [–2500+] cm.</text>
      <biological_entity id="o2785" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character name="some_measurement" src="d0_s0" value="30-200(-400)[-2500+] cm" value_original="30-200(-400)[-2500+] cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, usually branched (internodes sometimes winged).</text>
      <biological_entity id="o2787" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and/or cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (sometimes whorled) or alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o2788" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (pinnately nerved or 3-nerved or 5-nerved from at or near bases) mostly rhombic, deltate, ovate, or elliptic to lanceolate or lance-linear (sometimes intermediate shapes), sometimes pinnately or palmately lobed, bases cuneate to rounded or cordate, ultimate margins subentire or toothed, faces glabrous or hairy.</text>
      <biological_entity id="o2789" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="lanceolate or lance-linear" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="lanceolate or lance-linear" />
        <character is_modifier="false" modifier="sometimes pinnately; pinnately; palmately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o2790" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="rounded or cordate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2791" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o2792" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly or in corymbiform, dichasiiform, or paniculiform arrays.</text>
      <biological_entity id="o2793" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in arrays" constraintid="o2794" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in corymbiform , dichasiiform or paniculiform , arrays" />
      </biological_entity>
      <biological_entity id="o2794" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="dichasiiform" value_original="dichasiiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, turbinate, or campanulate to saucerlike, 5–20+ [–30+] mm diam.</text>
      <biological_entity id="o2795" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="saucerlike" />
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="saucerlike" />
        <character char_type="range_value" from="20+" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 9–30 [–50+] in 1–4 [–5+] series (orbiculate, ovate, or oblong to spatulate, lanceolate, or linear, subequal or unequal, outer shorter or longer than inner, herbaceous to chartaceous).</text>
      <biological_entity id="o2796" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="50" upper_restricted="false" />
        <character char_type="range_value" constraint="in series" constraintid="o2797" from="9" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
      <biological_entity id="o2797" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="5" upper_restricted="false" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex or ± conic, paleate (paleae usually navicular, ± conduplicate, herbaceous to scarious, linear to filiform in V. encelioides and V. nana).</text>
      <biological_entity id="o2798" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex or more or less conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or (1–) 5–30, either pistillate and fertile, or styliferous and sterile, or neuter;</text>
      <biological_entity id="o2799" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="30" />
        <character is_modifier="false" modifier="either" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to orange or ochroleucous [reddish].</text>
      <biological_entity id="o2800" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange or ochroleucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 8–150 [–300+], bisexual, fertile;</text>
      <biological_entity id="o2801" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="300" upper_restricted="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="150" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas usually concolorous with rays, tubes much shorter than or ± equaling funnelform or campanulate throats, lobes 5, ± deltate to lance-deltate.</text>
      <biological_entity id="o2802" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character constraint="with rays" constraintid="o2803" is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="concolorous" value_original="concolorous" />
      </biological_entity>
      <biological_entity id="o2803" name="ray" name_original="rays" src="d0_s13" type="structure" />
      <biological_entity id="o2804" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than or more or less equaling funnelform or campanulate throats" constraintid="o2805" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o2805" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o2806" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="less deltate" name="shape" src="d0_s13" to="lance-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± flattened, orbiculate, obovate, or oblanceolate to ± elliptic (usually winged);</text>
      <biological_entity id="o2807" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s14" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s14" to="more or less elliptic" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s14" to="more or less elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent [falling], usually of 2, ± subulate scales or awns, sometimes 0.</text>
      <biological_entity id="o2808" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>x = 17, 18?</text>
      <biological_entity id="o2809" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o2810" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
        <character modifier="usually; sometimes" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o2811" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="17" value_original="17" />
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly subtropical, tropical, and warm-temperate North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly subtropical" establishment_means="native" />
        <character name="distribution" value="tropical" establishment_means="native" />
        <character name="distribution" value="and warm-temperate North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>277.</number>
  <discussion>Species 200 or more (16 in the flora).</discussion>
  <references>
    <reference>Coleman, J. R. 1966. A taxonomic revision of section Ximenesia of the genus Verbesina (Compositae). Amer. Midl. Naturalist 76: 475–481.</reference>
    <reference>Coleman, J. R. 1966b. A taxonomic revision of section Sonoricola of the genus Verbesina (Compositae). Madroño 18: 129–137.</reference>
    <reference>Olsen, J. S. 1979. Taxonomy of the Verbesina virginica complex (Asteraceae). Sida 8: 128–134.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves all or mostly alternate (proximalmost sometimes opposite)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves all or mostly opposite (distal sometimes alternate)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas usually ochroleucous, sometimes white</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas yellow</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets 0</description>
      <determination>1 Verbesina walteri</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets (1–)2–12+</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ray florets (1–)2–3(–7); disc florets 8–12(–15)</description>
      <determination>2 Verbesina virginica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ray florets (9–)10–12+; disc florets 20–25+</description>
      <determination>3 Verbesina microptera</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants 10–50(–120+)cm (annuals); internodes not winged; disc florets 80–150+</description>
      <determination>4 Verbesina encelioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants 7–15 or 30–200+ cm (perennating bases ± erect or horizontal); internodes (at least proximal) winged; disc florets 40–80+</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads (3–)8–25(–50+) in corymbiform to paniculiform arrays; phyllaries 8–12in 1(–2) series, ± spreading to reflexed; ray florets (2–)6–8+</description>
      <determination>5 Verbesina alternifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads 2–5(–10+) in ± corymbiform arrays; phyllaries 16–21+ in 2–3 series,± erect; ray florets 8–13+</description>
      <determination>6 Verbesina helianthoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Internodes winged</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Internodes not winged</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries 8–12+ in 2 series; ray florets (0–)1–3(–5); disc florets 8–15+; pappi 3–4 mm</description>
      <determination>7 Verbesina occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries 18–20+ in 2–3 series; ray florets (5–)8; disc florets 20–60+; pappi 0–0.3mm</description>
      <determination>8 Verbesina heterophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants mostly 7–15+ cm; leaves: abaxial faces mostly strigoso-sericeous; cypselae± strigillose</description>
      <determination>9 Verbesina nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants mostly 30–100(–150+) cm; leaves: faces (at least abaxial) scabrellous to hirtellous, hirsutulous, or hispidulous (not strigoso-sericeous); cypselae glabrous or sparsely hirtellous</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Perennating bases horizontal; disc florets 40–60(–80+); cypselae purplish black; Florida</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Perennating bases ± erect; disc florets 60–120+; cypselae dark brown; Arizona, California, New Mexico, Texas</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Phyllaries 2–3+ mm; ray florets (5–)11–13; cypselae 4–5 mm; pappi 0.5–1mm</description>
      <determination>10 Verbesina aristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Phyllaries 5–9+ mm; ray florets 0; cypselae 5–7 mm; pappi 0–0.3 mm</description>
      <determination>11 Verbesina chapmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades ± lance-linear (lengths 10–15 times widths)</description>
      <determination>12 Verbesina longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades lance-elliptic, lance-ovate, ovate, ovate-deltate, or rhombic (lengths mostly 1.5–2.5 times widths)</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pappi 3–4 mm</description>
      <determination>13 Verbesina dissita</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pappi 0–0.5 mm</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries 12–16 in ± 2 series, lance-linear, lance-ovate, or linear, 4–7 mm</description>
      <determination>14 Verbesina oreophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries 18–30+ in 3–4 series, elliptic, oblong, orbiculate, or ovate, 5–12+ mm</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Involucres 10–15+ mm diam.; phyllaries elliptic to oblong; cypselae 10 mm</description>
      <determination>15 Verbesina rothrockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Involucres 15–22+ mm diam.; phyllaries oblong, orbiculate, or ovate; cypselae 7 mm</description>
      <determination>16 Verbesina lindheimeri</determination>
    </key_statement>
  </key>
</bio:treatment>