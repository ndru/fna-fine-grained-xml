<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">561</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="T. M. Barkley in N. L. Britton et al." date="1978" rank="species">ertterae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl., ser.</publication_title>
      <place_in_publication>2, 10: 124. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species ertterae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067484</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–50 (–60+) cm (taproots relatively thin, twisted).</text>
      <biological_entity id="o325" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage (± turgid or subsucculent) unevenly villous-tomentose, glabrescent.</text>
      <biological_entity id="o326" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="villous-tomentose" value_original="villous-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually single, rarely clustered.</text>
      <biological_entity id="o327" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves progressively reduced distally (basal usually withering before flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles winged);</text>
      <biological_entity id="o328" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate to spatulate, 4–7 × 1.5–3 cm, bases tapered, margins incised (distal leaves sessile, bractlike).</text>
      <biological_entity id="o329" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="spatulate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o330" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o331" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 6–10 (–20) in cymiform arrays.</text>
      <biological_entity id="o332" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="20" />
        <character char_type="range_value" constraint="in arrays" constraintid="o333" from="6" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o333" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 1–5+ linear to subulate bractlets (to 4 mm).</text>
      <biological_entity id="o334" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o335" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s7" to="subulate" />
      </biological_entity>
      <relation from="o334" id="r30" name="consist_of" negation="false" src="d0_s7" to="o335" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 13, 6–7 mm, tips green.</text>
      <biological_entity id="o336" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o337" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets ± 8 (± 13?);</text>
      <biological_entity id="o338" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="8" value_original="8" />
        <character modifier="more or less" name="quantity" src="d0_s9" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 5–6 mm.</text>
      <biological_entity constraint="corolla" id="o339" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae usually hairy (especially on angles), sometimes glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity id="o340" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o341" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus slopes of greenish yellow ash tuff</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" constraint="of greenish yellow ash tuff" />
        <character name="habitat" value="greenish yellow ash tuff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Senecio erterrae is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>