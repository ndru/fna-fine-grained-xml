<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="treatment_page">437</other_info_on_meta>
    <other_info_on_meta type="illustration_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">hymenoxys</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">odorata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 661. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus hymenoxys;species odorata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066993</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–80 cm (robust).</text>
      <biological_entity id="o25818" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–25, usually purple-red-tinted proximally, sometimes throughout, often branched throughout, ± hairy.</text>
      <biological_entity id="o25819" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="25" />
        <character is_modifier="false" modifier="usually; proximally" name="coloration" src="d0_s1" value="purple-red-tinted" value_original="purple-red-tinted" />
        <character is_modifier="false" modifier="sometimes throughout; throughout; often; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades simple or lobed (lobes 3–19+), ± hairy, glanddotted;</text>
      <biological_entity id="o25820" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25821" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid leaves lobed (lobes 5–11+, terminal lobes 0.3–1 mm wide).</text>
      <biological_entity id="o25822" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o25823" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 15–350+ per plant in paniculiform arrays.</text>
      <biological_entity id="o25824" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o25825" from="15" name="quantity" src="d0_s4" to="350" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o25825" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <biological_entity id="o25826" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o25825" id="r1754" name="in" negation="false" src="d0_s4" to="o25826" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 2–12 cm, sparsely hairy.</text>
      <biological_entity id="o25827" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres subhemispheric to campanulate to ± urceolate, 6–10 × 7–12.5 mm.</text>
      <biological_entity id="o25828" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="subhemispheric" name="shape" src="d0_s6" to="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2 series, unequal;</text>
      <biological_entity id="o25829" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o25830" name="series" name_original="series" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o25829" id="r1755" name="in" negation="false" src="d0_s7" to="o25830" />
    </statement>
    <statement id="d0_s8">
      <text>outer 8–13, basally connate 1/4–1/3 their lengths, obovate to oblanceolate, 3.5–5.2 mm, apices acuminate;</text>
      <biological_entity constraint="outer" id="o25831" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="13" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="lengths" src="d0_s8" to="1/3" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner 8–13, obovate, 3.8–5.9 mm, apices acuminate to acute.</text>
      <biological_entity id="o25832" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o25833" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="13" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s9" to="5.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25834" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–13;</text>
      <biological_entity id="o25835" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 8.5–11 × 3–5.5 mm.</text>
      <biological_entity id="o25836" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="length" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 50–150+;</text>
      <biological_entity id="o25837" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 2.6–4.1 mm.</text>
      <biological_entity id="o25838" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s13" to="4.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae narrowly obpyramidal, 1.7–2.5 mm;</text>
      <biological_entity id="o25839" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 5–6 obovate, aristate scales 1.6–2.3 mm. 2n = 22, 24, 28, 30.</text>
      <biological_entity id="o25840" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o25841" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="6" />
        <character is_modifier="true" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25842" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="22" value_original="22" />
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
      <relation from="o25840" id="r1756" name="consist_of" negation="false" src="d0_s15" to="o25841" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Apr–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, open flats, mesquite and creosote-bush flats, ditches and drainage areas, stream banks and bottoms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open flats" />
        <character name="habitat" value="mesquite" />
        <character name="habitat" value="creosote-bush flats" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="drainage areas" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Kans., N.Mex., Okla., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sonora, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Western bitterweed</other_name>
  <other_name type="common_name">bitter rubberweed</other_name>
  
</bio:treatment>