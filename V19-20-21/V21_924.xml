<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arnica</taxon_name>
    <taxon_name authority="Greene" date="1910" rank="species">sororia</taxon_name>
    <place_of_publication>
      <publication_title>Ottawa Naturalist</publication_title>
      <place_in_publication>23: 213. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus arnica;species sororia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066127</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arnica</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">fulgens</taxon_name>
    <taxon_name authority="(Greene) G. W. Douglas &amp; Ruyle-Douglas" date="unknown" rank="variety">sororia</taxon_name>
    <taxon_hierarchy>genus Arnica;species fulgens;variety sororia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–50 cm.</text>
      <biological_entity id="o112" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1 or relatively few) simple or branched.</text>
      <biological_entity id="o113" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–6 pairs, crowded toward stem-bases (axils lacking tufts of brown wool);</text>
      <biological_entity id="o115" name="stem-base" name_original="stem-bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiolate (petioles, at least basal, narrowly winged);</text>
      <biological_entity id="o114" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" />
        <character constraint="toward stem-bases" constraintid="o115" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (with 3 or 5, prominent, subparallel veins) oblanceolate to narrowly oblong, 3.5–14.5 × 0.6–2.4 cm, margins usually entire, rarely denticulate, apices obtuse, faces uniformly hairy, stipitate-glandular.</text>
      <biological_entity id="o116" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="narrowly oblong" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s4" to="14.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s4" to="2.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o117" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o118" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o119" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="uniformly" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–5.</text>
      <biological_entity id="o120" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric.</text>
      <biological_entity id="o121" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 13–20, usually narrowly, sometimes broadly, lanceolate.</text>
      <biological_entity id="o122" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="false" modifier="usually narrowly; narrowly; sometimes broadly; broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 9–17;</text>
      <biological_entity id="o123" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s8" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow-orange.</text>
      <biological_entity id="o124" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow-orange" value_original="yellow-orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets: corollas yellow;</text>
      <biological_entity id="o125" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure" />
      <biological_entity id="o126" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow.</text>
      <biological_entity id="o127" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure" />
      <biological_entity id="o128" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae brown, 3.5–5 mm, densely hirsute, sometimes sparingly glandular as well;</text>
      <biological_entity id="o129" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes sparingly; well" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi white, bristles barbellate.</text>
      <biological_entity id="o130" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38.</text>
      <biological_entity id="o131" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o132" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies and grasslands to montane conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="montane conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Calif., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Twin arnica</other_name>
  
</bio:treatment>