<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">vernonia</taxon_name>
    <taxon_name authority="(Walter) Gleason" date="1906" rank="species">acaulis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. New York Bot. Gard.</publication_title>
      <place_in_publication>4: 222. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus vernonia;species acaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067801</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysocoma</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">acaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>196. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysocoma;species acaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–6 (–10) dm.</text>
      <biological_entity id="o9400" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± puberulent, glabrescent.</text>
      <biological_entity id="o9401" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o9402" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9403" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblongelliptic to spatulate (basal, the distal narrower), 11–25 (–30) cm × (21–) 60–75+ mm, l/w = 2–4 (–5+), abaxially usually glabrous or glabrate, sometimes hirsutulous on veins, usually resin-gland-dotted, adaxially ± scabrellous or glabrate.</text>
      <biological_entity id="o9404" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="21" from_unit="mm" name="atypical_width" src="d0_s3" to="60" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="width" src="d0_s3" to="75" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_l/w" src="d0_s3" to="5" upper_restricted="false" />
        <character char_type="range_value" from="2" name="l/w" src="d0_s3" to="4" />
        <character is_modifier="false" modifier="abaxially usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character constraint="on veins" constraintid="o9405" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s3" value="resin-gland-dotted" value_original="resin-gland-dotted" />
        <character is_modifier="false" modifier="adaxially more or less" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o9405" name="vein" name_original="veins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, corymbiform arrays.</text>
      <biological_entity id="o9406" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o9407" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o9406" id="r875" name="in" negation="false" src="d0_s4" to="o9407" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–50 mm.</text>
      <biological_entity id="o9408" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to hemispheric, 5–10 × 6–7 mm.</text>
      <biological_entity id="o9409" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 40–60 in 5–6 series, glabrate, margins ciliolate, the outer lance-linear, 2–4 mm, inner lanceovate to lance-subulate, 5–9 mm, tips subulate to filiform.</text>
      <biological_entity id="o9410" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9411" from="40" name="quantity" src="d0_s7" to="60" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o9411" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <biological_entity id="o9412" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9413" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="lance-linear" value_original="lance-linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9414" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s7" to="lance-subulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9415" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s7" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 30–40 (–55+).</text>
      <biological_entity id="o9416" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="55" upper_restricted="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 2.5–3+ mm;</text>
      <biological_entity id="o9417" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi whitish to stramineous, outer scales ca. 20, 0.6–1 mm, contrasting with 25–40, 5–7+ mm inner bristles.</text>
      <biological_entity id="o9418" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s10" to="stramineous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9420" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s10" to="40" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o9419" id="r876" name="contrasting with" negation="false" src="d0_s10" to="o9420" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity constraint="outer" id="o9419" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9421" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods, pine savannas, bogs, disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>