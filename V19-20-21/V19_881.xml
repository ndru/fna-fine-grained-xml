<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
    <other_info_on_meta type="illustration_page">514</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Rydberg) McArthur" date="1981" rank="subgenus">tridentatae</taxon_name>
    <taxon_name authority="Rydberg" date="1900" rank="species">tripartita</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 432. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus tridentatae;species tripartita</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066172</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">trifida</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 398. 1841,</place_in_publication>
      <other_info_on_pub>not Turczaninow 1832</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Artemisia;species trifida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tridentata</taxon_name>
    <taxon_name authority="H. M. Hall &amp; Clements" date="unknown" rank="subspecies">trifida</taxon_name>
    <taxon_hierarchy>genus Artemisia;species tridentata;subspecies trifida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Seriphidium</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber" date="unknown" rank="species">tripartitum</taxon_name>
    <taxon_hierarchy>genus Seriphidium;species tripartitum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–15 or 20–150 (–200) cm, aromatic;</text>
    </statement>
    <statement id="d0_s1">
      <text>root-sprouting (caudices with adventitious-buds, fibrous-rooted).</text>
      <biological_entity id="o8090" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s0" to="15" />
        <character name="quantity" src="d0_s0" value="20-150(-200) cm" value_original="20-150(-200) cm" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="root-sprouting" value_original="root-sprouting" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems pale gray, glabrous.</text>
      <biological_entity id="o8091" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale gray" value_original="pale gray" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, gray-green;</text>
      <biological_entity id="o8092" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades broadly cuneate, 1.5–4 × 0.5–2 cm, deeply 3-lobed (lobes 1–1.4 mm wide, acute; cauline leaves smaller, mostly 3-lobed).</text>
      <biological_entity id="o8093" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform or spiciform arrays (5–) 8–15 (–35) × (0.5–) 1–5 cm.</text>
      <biological_entity id="o8094" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spiciform" value_original="spiciform" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s5" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres globose or turbinate, 2–4 × 1.5–3 mm.</text>
      <biological_entity id="o8095" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries broadly lanceolate (margins scarious, obscured by indument), canescent.</text>
      <biological_entity id="o8096" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 3–11;</text>
      <biological_entity id="o8097" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 2–2.5 mm, glandular (style-branches included).</text>
      <biological_entity id="o8098" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (columnar, unequally ribbed) 1.8–2.3 mm, glabrous or resinous.</text>
      <biological_entity id="o8099" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s10" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Nev., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Three-tipped sagebrush</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs 20–150(–200) cm; lobes of leaves linear, to 0.5 mm wide; loamy soils, w of continental divide</description>
      <determination>18a Artemisia tripartita subsp. tripartita</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs 5–15 cm; lobes of leaves lanceolate, 1–1.5 mm wide; stony grasslands, e Wyoming</description>
      <determination>18b Artemisia tripartita subsp. rupicola</determination>
    </key_statement>
  </key>
</bio:treatment>