<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
    <other_info_on_meta type="illustration_page">124</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Rohr" date="1792" rank="genus">melanthera</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="1903" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1251, 1340. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus melanthera;species nivea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220008297</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bidens</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 833. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bidens;species nivea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calea</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">aspera</taxon_name>
    <taxon_hierarchy>genus Calea;species aspera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="(Jacquin) Small" date="unknown" rank="species">aspera</taxon_name>
    <taxon_hierarchy>genus Melanthera;species aspera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">carpenteri</taxon_name>
    <taxon_hierarchy>genus Melanthera;species carpenteri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">deltoidea</taxon_name>
    <taxon_hierarchy>genus Melanthera;species deltoidea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">hastata</taxon_name>
    <taxon_hierarchy>genus Melanthera;species hastata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">ligulata</taxon_name>
    <taxon_hierarchy>genus Melanthera;species ligulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–220 cm.</text>
      <biological_entity id="o20231" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="220" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, sometimes sprawling or scandent, usually scabro-hispid, sometimes glabrescent.</text>
      <biological_entity id="o20232" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="scandent" value_original="scandent" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="scabro-hispid" value_original="scabro-hispid" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades usually ovate, sometimes deltate or 3-lobed, rarely lance-elliptic, 5–12 × 3–8 cm (often triplinerved), bases truncate to cuneate, margins crenate to serrate (often irregularly), faces usually strigose to hispid, sometimes glabrescent.</text>
      <biological_entity id="o20233" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_shape" src="d0_s2" value="lance-elliptic" value_original="lance-elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20234" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o20235" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s2" to="serrate" />
      </biological_entity>
      <biological_entity id="o20236" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually strigose" name="pubescence" src="d0_s2" to="hispid" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne in axils of reduced distal leaves (sometimes forming loose, corymbiform arrays).</text>
      <biological_entity id="o20237" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o20238" name="axil" name_original="axils" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o20239" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o20237" id="r1388" name="borne in" negation="false" src="d0_s3" to="o20238" />
      <relation from="o20237" id="r1389" name="borne in" negation="false" src="d0_s3" to="o20239" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 5–14 cm, hispid to strigose.</text>
      <biological_entity id="o20240" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s4" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 12–20 mm diam.</text>
      <biological_entity id="o20241" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries broadly ovate to lanceolate, 3–10 × 3–4 mm.</text>
      <biological_entity id="o20242" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae 4–7 × 1.5–2.5 mm, apical mucros 0.5–1.4+ mm, usually recurved.</text>
      <biological_entity id="o20243" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20244" name="mucro" name_original="mucros" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.4" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 30–100+;</text>
      <biological_entity id="o20245" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 5–10 mm;</text>
      <biological_entity id="o20246" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anther sacs 2+ mm.</text>
      <biological_entity constraint="anther" id="o20247" name="sac" name_original="sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2–3 × 1–2 mm. 2n = 30.</text>
      <biological_entity id="o20248" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20249" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist forests, forest borders, strands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist forests" />
        <character name="habitat" value="forest borders" />
        <character name="habitat" value="strands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10(–200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Ill., Ky., La., Miss., S.C.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In the flora area, most populations of Melanthera nivea occur along the Atlantic and Gulf coastal plains; populations are also known from southern Illinois and Kentucky.</discussion>
  <discussion>Melanthera nivea ranges widely geographically and varies ecologically and morphologically. Apparent ecogeographic races or ecotypes are often distinct; I have delimited them taxonomically (J. C. Parks 1973). I took the northern, erect, forest-dwelling plants with sulcate, purple-mottled stems, hastate leaves, and lanceolate, long-acuminate, recurved phyllaries and paleae to be M. nivea in the restricted sense. I referred sprawling, bushlike, highly scabrous plants with moderately long phyllaries and paleae from open, weedy habitats to M. aspera var. aspera. A strand form from the West Indies and southern Florida, called M. aspera var. glabriuscula (Kuntze) J. C. Parks, was distinguished by its less hispid, deltate leaves, ovate, obtuse phyllaries, paleae with apical mucros only slightly longer than unopened disc florets, and cypselae frequently obovate with relatively numerous short hairs on the apex rather than obpyramidal with relatively few hairs on a truncate-concave apex. Field observations by floristic botanists have yielded plants of intermediate morphology. Healthy F1 offspring were recovered from 88 genetic crosses among elements of M. nivea in the broad sense (i.e., including M. aspera; unpubl.). On the basis of results from crosses and field observations, I concluded that it is better taxonomically to treat the ecotypes as variants of one wide-ranging, morphologically variable, possibly diverging species.</discussion>
  
</bio:treatment>