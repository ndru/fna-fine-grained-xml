<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">432</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1857" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 107. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066848</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–130 cm.</text>
      <biological_entity id="o17780" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–10), unbranched or sparingly branched distally, weakly to moderately winged, glabrous proximally, glabrous or sparsely hairy distally.</text>
      <biological_entity id="o17781" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="sparingly branched distally" name="architecture" src="d0_s1" to="weakly moderately winged" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves glabrous or sparsely hairy;</text>
      <biological_entity id="o17782" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades oblanceolate to oblongelliptic, entire;</text>
      <biological_entity constraint="basal" id="o17783" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblongelliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal and mid-blades lanceolate to oblongelliptic, entire;</text>
      <biological_entity constraint="proximal" id="o17784" name="mid-blade" name_original="mid-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblongelliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal blades linear to lanceolate, entire.</text>
      <biological_entity constraint="distal" id="o17785" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–20 per plant, borne singly or in paniculiform arrays.</text>
      <biological_entity id="o17786" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o17787" from="1" name="quantity" src="d0_s6" to="20" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in paniculiform arrays" value_original="in paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o17787" name="plant" name_original="plant" src="d0_s6" type="structure" />
      <biological_entity id="o17788" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o17786" id="r1225" name="in" negation="false" src="d0_s6" to="o17788" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles (6–) 10–30 cm, sparsely to moderately hairy.</text>
      <biological_entity id="o17789" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s7" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric to globoid, 12–20 × (14–) 17–22 (–25) mm.</text>
      <biological_entity id="o17790" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_width" src="d0_s8" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (connate proximally) moderately to densely hairy.</text>
      <biological_entity id="o17791" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 14–20, pistillate, fertile;</text>
      <biological_entity id="o17792" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s10" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 13–25 × 5–10 (–12) mm.</text>
      <biological_entity id="o17793" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s11" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 250–500 (–800+);</text>
      <biological_entity id="o17794" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="500" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="800" upper_restricted="false" />
        <character char_type="range_value" from="250" name="quantity" src="d0_s12" to="500" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow proximally, yellow to brown to purple distally, 3–4.4 (–4.8) mm, lobes 5.</text>
      <biological_entity id="o17795" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="yellow" modifier="distally" name="coloration" src="d0_s13" to="brown" />
        <character char_type="range_value" from="4.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17796" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.8–2.4 mm, moderately hairy;</text>
      <biological_entity id="o17797" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 6–8 entire, aristate scales 1.3–2.2 (–2.7) mm.</text>
      <biological_entity id="o17799" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s15" to="8" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
      </biological_entity>
      <relation from="o17798" id="r1226" name="consist_of" negation="false" src="d0_s15" to="o17799" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 32.</text>
      <biological_entity id="o17798" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity constraint="2n" id="o17800" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jul–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs and swamps, around ponds and lakes, along streams, in moist meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" constraint="around ponds and lakes , along streams , in moist meadows" />
        <character name="habitat" value="swamps" constraint="around ponds and lakes , along streams , in moist meadows" />
        <character name="habitat" value="ponds" constraint="along streams" />
        <character name="habitat" value="lakes" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="moist meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Bigelow’s sneezeweed</other_name>
  
</bio:treatment>