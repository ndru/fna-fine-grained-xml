<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
    <other_info_on_meta type="illustration_page">198</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="(Hooker) A. Gray" date="1880" rank="species">florifera</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 84. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species florifera</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067763</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">florifer</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 20. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species florifer;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Townsendia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">florifera</taxon_name>
    <taxon_name authority="(A. Gray) Cronquist" date="unknown" rank="variety">watsonii</taxon_name>
    <taxon_hierarchy>genus Townsendia;species florifera;variety watsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials (perhaps flowering first-year, sometimes persisting), 3–12 (–15+) cm.</text>
      <biological_entity id="o22126" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect;</text>
      <biological_entity id="o22127" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 3–12 (–15+) mm, piloso-strigose.</text>
      <biological_entity id="o22128" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="piloso-strigose" value_original="piloso-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, blades spatulate to linear, 10–25 (–50+) × 2–3 (–8+) mm, not fleshy, faces ± strigose.</text>
      <biological_entity id="o22129" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o22130" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o22131" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads at tips of stems.</text>
      <biological_entity id="o22132" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o22133" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity id="o22134" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <relation from="o22132" id="r2044" name="at" negation="false" src="d0_s4" to="o22133" />
      <relation from="o22133" id="r2045" name="part_of" negation="false" src="d0_s4" to="o22134" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric or broader, 16–20 (–30) mm diam.</text>
      <biological_entity id="o22135" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="true" name="width" src="d0_s5" value="broader" value_original="broader" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 24–30+ in 3–4+ series, the longer ± lanceolate, (6–) 9–11+ mm (l/w = 2.5–5), apices acute, abaxial faces strigose.</text>
      <biological_entity id="o22136" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o22137" from="24" name="quantity" src="d0_s6" to="30" upper_restricted="false" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22137" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22138" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22139" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 13–34+;</text>
      <biological_entity id="o22140" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="34" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white or pinkish adaxially, laminae (8–) 10–18+ mm, abaxially usually glandular-puberulent, rarely glabrous.</text>
      <biological_entity id="o22141" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="pinkish" value_original="pinkish" />
      </biological_entity>
      <biological_entity id="o22142" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially usually" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets (80–) 100–150+;</text>
      <biological_entity id="o22143" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="80" name="atypical_quantity" src="d0_s9" to="100" to_inclusive="false" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s9" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas (4–) 5.5–6+ mm.</text>
      <biological_entity id="o22144" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (3.5–) 4–5+ mm, faces hairy, hair tips entire or forked;</text>
      <biological_entity id="o22145" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22146" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o22147" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae 20–30 subulate to setiform scales 2–6 mm;</text>
      <biological_entity id="o22148" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae 20–30+ subulate to setiform scales (4–) 5–7+ mm.</text>
      <biological_entity id="o22149" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s13" to="setiform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o22150" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="setiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22151" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly flats with junipers and sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly flats" constraint="with junipers and sagebrush" />
        <character name="habitat" value="junipers" />
        <character name="habitat" value="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>J. H. Beaman (1957) considered plants intermediate between Townsendia florifera and T. parryi to be hybrids. Such intermediates occur in Montana (e.g., Jones in 1905 from Gallatin Co. and Suksdorf 282 from Park Co.), outside the known distribution of T. florifera.</discussion>
  
</bio:treatment>