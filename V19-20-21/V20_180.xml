<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">gutierrezia</taxon_name>
    <taxon_name authority="(S. L. Welsh) S. L. Welsh" date="1983" rank="species">pomariensis</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>43: 288. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus gutierrezia;species pomariensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066827</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gutierrezia</taxon_name>
    <taxon_name authority="(Pursh) Britton &amp; Rusby" date="unknown" rank="species">sarothrae</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">pomariensis</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>30: 19. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gutierrezia;species sarothrae;variety pomariensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 20–50 cm.</text>
      <biological_entity id="o8011" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely scabrous or glandular-scabrous to glabrate.</text>
      <biological_entity id="o8012" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character name="pubescence_or_relief" src="d0_s1" value="glandular-scabrous to glabrate" value_original="glandular-scabrous to glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal absent at flowering;</text>
      <biological_entity id="o8013" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o8014" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline blades 1-nerved, linear, 0.5–2.5 mm wide, slightly reduced distally.</text>
      <biological_entity id="o8015" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o8016" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in pairs on bracteate peduncles, or some almost sessile, in loose arrays.</text>
      <biological_entity id="o8017" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="almost" name="architecture" notes="" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o8018" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o8019" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o8020" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o8017" id="r724" name="borne" negation="false" src="d0_s4" to="o8018" />
      <relation from="o8017" id="r725" name="on" negation="false" src="d0_s4" to="o8019" />
      <relation from="o8017" id="r726" name="in" negation="false" src="d0_s4" to="o8020" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindro-turbinate to turbinate-campanulate, 3–5 mm diam.</text>
      <biological_entity id="o8021" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="cylindro-turbinate" name="shape" src="d0_s5" to="turbinate-campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllary apices (green, broadly rounded), thickened, (prominently gland-dotted).</text>
      <biological_entity constraint="phyllary" id="o8022" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (4–) 5–7 (–8);</text>
      <biological_entity id="o8023" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="8" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 2–5 mm.</text>
      <biological_entity id="o8024" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 5–15.</text>
      <biological_entity id="o8025" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1–2 mm, faces without oil cavities, loosely strigose;</text>
      <biological_entity id="o8026" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8027" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="oil" id="o8028" name="cavity" name_original="cavities" src="d0_s10" type="structure" />
      <relation from="o8027" id="r727" name="without" negation="false" src="d0_s10" to="o8028" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of 1–2 series of oblong-lanceolate scales 1–2 mm. 2n = 32.</text>
      <biological_entity id="o8029" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o8030" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8031" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8032" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="32" value_original="32" />
      </biological_entity>
      <relation from="o8029" id="r728" name="consist_of" negation="false" src="d0_s11" to="o8030" />
      <relation from="o8030" id="r729" name="part_of" negation="false" src="d0_s11" to="o8031" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open, rocky sites, mixed desert shrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="dry" />
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="mixed desert" />
        <character name="habitat" value="shrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Orchard snakeweed</other_name>
  <discussion>Gutierrezia pomariensis is similar to G. sarothrae and sympatric with it; the two are ecologically distinct, with G. pomariensis occupying drier habitats. “Intermediates in a populational sense are few and apparently confined to the ecotone in places where the two taxa grow nearby” (A. Cronquist 1994).</discussion>
  
</bio:treatment>