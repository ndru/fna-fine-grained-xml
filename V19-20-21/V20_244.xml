<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="treatment_page">122</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1884" rank="subsection">squarrosae</taxon_name>
    <taxon_name authority="LeBlond" date="2000" rank="species">villosicarpa</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>19: 292, figs. 1–6. 2000</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection squarrosae;species villosicarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067584</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, 45–150 cm;</text>
      <biological_entity id="o21764" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices short, woody, or short rhizomes.</text>
      <biological_entity id="o21765" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o21766" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually single, erect (proximally medium to dark-brown distally lighter, sometimes cyanotic, usually rounded, shallowly many ribbed), sparsely finely hispido-strigose proximally to densely so in arrays.</text>
      <biological_entity id="o21767" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="densely in arrays" constraintid="o21768" is_modifier="false" modifier="sparsely finely; proximally to densely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
      </biological_entity>
      <biological_entity id="o21768" name="array" name_original="arrays" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: rosettes often present at flowering, early leaves smaller;</text>
      <biological_entity id="o21769" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21770" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character constraint="at leaves" constraintid="o21771" is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21771" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline gradually to abruptly tapering to winged petioles, blades elliptic to ovate-elliptic, 90–210 (including petioles) × 40–70 mm, margins serrate-serrulate, ciliate, apices obtuse to broadly acute, faces abaxially glabrous or sparsely strigose, adaxially glabrous or sparsely strigose, mostly on nerves;</text>
      <biological_entity id="o21772" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o21773" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o21774" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate-elliptic" />
        <character char_type="range_value" from="90" from_unit="mm" name="length" src="d0_s4" to="210" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s4" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21775" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate-serrulate" value_original="serrate-serrulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21776" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o21777" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o21778" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
      <relation from="o21777" id="r2008" modifier="mostly" name="on" negation="false" src="d0_s4" to="o21778" />
    </statement>
    <statement id="d0_s5">
      <text>mid to distal cauline sessile, blades lanceolate or elliptic, 20–650 × 10–30 mm, gradually reduced distally, becoming entire, apices acute to acuminate.</text>
      <biological_entity id="o21779" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o21780" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21781" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="650" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21782" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 50–100+ (1–10 per branch, more on much elongated proximal branches), in elongate to thyrsiform-paniculiform arrays 7–22 × 3–6 cm;</text>
      <biological_entity id="o21783" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s6" to="100" upper_restricted="false" />
        <character char_type="range_value" from="7" from_unit="cm" modifier="in elongate to thyrsiform-paniculiform arrays" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="in elongate to thyrsiform-paniculiform arrays" name="width" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches ascending, racemiform or paniculiform, not secund, longest to 1/2 length of arrays.</text>
      <biological_entity id="o21784" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="secund" value_original="secund" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="0 length of arrays" name="length" src="d0_s7" to="1/2 length of arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.5–11 mm, densely short-hispido-strigose;</text>
      <biological_entity id="o21785" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="short-hispido-strigose" value_original="short-hispido-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 5–10+, grading into phyllaries, sparsely short-hispido-strigose and sparsely to moderately glandular.</text>
      <biological_entity id="o21786" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s9" value="short-hispido-strigose" value_original="short-hispido-strigose" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o21787" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure" />
      <relation from="o21786" id="r2009" name="into" negation="false" src="d0_s9" to="o21787" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres campanulate, 5–8 mm.</text>
      <biological_entity id="o21788" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 4–5 series, appressed, strongly unequal, outer ovate, acute, mid and inner broadly oblong (appressed), obtuse or rounded, sparsely strigose and moderately finely stipitate-glandular.</text>
      <biological_entity id="o21789" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s11" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21790" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21791" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="mid and inner" id="o21792" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="moderately finely" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o21789" id="r2010" name="in" negation="false" src="d0_s11" to="o21790" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 4–8;</text>
      <biological_entity id="o21793" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>laminae 5–7 × 1–2 mm.</text>
      <biological_entity id="o21794" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 10–18;</text>
      <biological_entity id="o21795" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas 5–7 mm, lobes 1.5–2.2 mm.</text>
      <biological_entity id="o21796" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21797" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae (fusiform to obconic) 2.5–3 mm, moderately long-strigose;</text>
      <biological_entity id="o21798" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s16" value="long-strigose" value_original="long-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 4–6 mm (some clavate).</text>
      <biological_entity id="o21799" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, live oak scrub on dunes, roadsides, open pine-oak woods, Atlantic coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="live oak" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open pine-oak woods" />
        <character name="habitat" value="atlantic coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Glandular wand goldenrod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Solidago villosicarpa is known only from New Hanover, Onslow, and Pender counties. It has a habit similar to those of S. hispida and S. squarrosa, leaves similar to those of S. erecta, and stem pubescence similar to that of S. puberula. The large, glandular involucres are unique within subsect. Squarrosae. It was compared to S. sciaphila when first described; it is not very similar to that Midwestern species. It is likely either a large-headed diploid like S. squarrosa or possibly an allopolyploid.</discussion>
  
</bio:treatment>