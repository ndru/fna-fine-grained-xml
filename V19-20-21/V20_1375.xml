<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">612</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">doronicum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">plantagineum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 885. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus doronicum;species plantagineum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066487</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–80 cm, Stems glabrate proximally, sparsely pubescent distally.</text>
      <biological_entity id="o9294" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9295" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blades of basal leaves ovate to elliptic or lanceolate, 5–11 × 3–5 (–6) cm, bases cuneate, margins weakly dentate or entire, faces (and petioles) glandular-pubescent;</text>
      <biological_entity id="o9296" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o9297" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="elliptic or lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="11" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9298" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o9299" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o9300" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9301" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <relation from="o9297" id="r862" name="part_of" negation="false" src="d0_s1" to="o9298" />
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves 5–7 (–10), petiolate (proximal) or sessile (distal), blades ovate to lanceolate, bases clasping or not.</text>
      <biological_entity id="o9302" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o9303" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o9304" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9305" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
        <character name="architecture_or_fixation" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly, 3–5 (–6) cm diam.</text>
      <biological_entity id="o9306" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 6–26 cm.</text>
      <biological_entity id="o9307" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="26" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries linear-lanceolate, 14–20 mm, lengths about 2/3 rays, apices filiform.</text>
      <biological_entity id="o9308" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character name="quantity" src="d0_s5" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o9309" name="ray" name_original="rays" src="d0_s5" type="structure" />
      <biological_entity id="o9310" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="lengths" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray corollas 18–27 mm. 2n = 120.</text>
      <biological_entity constraint="ray" id="o9311" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s6" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9312" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed woods and open fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed woods" />
        <character name="habitat" value="open fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Oreg.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Plantain-leaved leopard’s-bane</other_name>
  
</bio:treatment>