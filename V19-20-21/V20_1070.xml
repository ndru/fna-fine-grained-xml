<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="treatment_page">463</other_info_on_meta>
    <other_info_on_meta type="illustration_page">464</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">psilactis</taxon_name>
    <taxon_name authority="S. Watson" date="1891" rank="species">tenuis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>26: 139. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus psilactis;species tenuis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067403</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(S. Watson) B. L. Turner &amp; D. B. Horne" date="unknown" rank="species">tenuis</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species tenuis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials, 25–100 cm.</text>
      <biological_entity id="o7412" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems and branches coarsely antrorse or erect-hairy, often scabrous, usually stipitate-glandular distally, sometimes proximally.</text>
      <biological_entity id="o7414" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="coarsely" name="orientation" src="d0_s1" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="erect-hairy" value_original="erect-hairy" />
        <character is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually; distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o7415" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="coarsely" name="orientation" src="d0_s1" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="erect-hairy" value_original="erect-hairy" />
        <character is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually; distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Distal leaf-blades sessile, linear-lanceolate to narrowly elliptic or linear, smallest 3–4 × 1–1.5 mm.</text>
      <biological_entity constraint="distal" id="o7416" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s2" value="smallest" value_original="smallest" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres broadly turbinate, 4–6 mm.</text>
      <biological_entity id="o7417" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries linear-lanceolate to linear, equal or slightly unequal, 2.5–5 × 0.3–0.6 mm, bases indurate, margins scarious.</text>
      <biological_entity id="o7418" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="linear" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7419" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o7420" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles convex, 1.8–2.5 mm diam.</text>
      <biological_entity id="o7421" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="diameter" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 10–35;</text>
      <biological_entity id="o7422" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 5–8 × 0.7–1 mm.</text>
      <biological_entity id="o7423" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 20–55;</text>
      <biological_entity id="o7424" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="55" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 3–4 mm.</text>
      <biological_entity id="o7425" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray cypselae 1.2–1.7 mm, sparsely appressed-hairy.</text>
      <biological_entity constraint="ray" id="o7426" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc cypselae 1.5–2.5 mm, sparsely appressed-hairy;</text>
      <biological_entity constraint="disc" id="o7427" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 20–35, 2.5–4 mm. 2n = 8.</text>
      <biological_entity id="o7428" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" unit=",2.5-4 mm" />
        <character char_type="range_value" from="20" name="some_measurement" src="d0_s12" to="35" unit=",2.5-4 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7429" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous soils in open oak and pine-oak woodlands, rocky streambeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous soils" constraint="in open oak" />
        <character name="habitat" value="open oak" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="rocky streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>In the flora area, Psilactis tenuis is known only from the Davis Mountains. Because of its distribution in Mexico, it would be expected to occur in additional areas to the southeast of the Davis Mountains, such as the Glass Mountains and Chisos Mountains. Apparently no records of it exist from those areas.</discussion>
  
</bio:treatment>