<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">628</other_info_on_meta>
    <other_info_on_meta type="illustration_page">629</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="genus">rainiera</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1898" rank="species">stricta</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 291. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus rainiera;species stricta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220011364</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Psacalium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">strictum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 228. 1892,</place_in_publication>
      <other_info_on_pub>based on Prenanthes stricta Greene, Pittonia 2: 21. 1889, not Blume 1825</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Psacalium;species strictum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luina</taxon_name>
    <taxon_name authority="(Greene) B. L. Robinson" date="unknown" rank="species">stricta</taxon_name>
    <taxon_hierarchy>genus Luina;species stricta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 10–35 × 2–9 cm.</text>
    </statement>
    <statement id="d0_s1">
      <text>Arrays of heads 8–35+ cm.</text>
      <biological_entity id="o17496" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s0" to="9" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="distance" src="d0_s1" to="35" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17497" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o17496" id="r1626" name="part_of" negation="false" src="d0_s1" to="o17497" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 4–17 mm.</text>
      <biological_entity id="o17498" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 6–9 mm.</text>
      <biological_entity id="o17499" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Corollas 10–12 mm.</text>
      <biological_entity id="o17500" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 5–6 mm;</text>
      <biological_entity id="o17501" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi 9–10 mm.</text>
      <biological_entity id="o17502" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soils, open slopes, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soils" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>