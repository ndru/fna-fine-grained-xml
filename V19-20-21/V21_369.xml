<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
    <other_info_on_meta type="illustration_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="(Pursh) Torrey &amp; A. Gray" date="1842" rank="species">radula</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 321. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species radula</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066898</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">radula</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 575. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rudbeckia;species radula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–100 cm (with crown buds).</text>
      <biological_entity id="o8297" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, distally densely hispid.</text>
      <biological_entity id="o8298" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally densely" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o8300" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
      <biological_entity id="o8299" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles obscure (intergrading with blades);</text>
      <biological_entity id="o8301" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obovate to orbiculate, 4.6–14.5 × 2.1–12 cm, bases broadly cuneate to rounded, margins entire or serrulate, abaxial faces strigoso-hispid, not glanddotted (cauline usually much smaller, alternate distally).</text>
      <biological_entity id="o8302" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="4.6" from_unit="cm" name="length" src="d0_s5" to="14.5" to_unit="cm" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="width" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8303" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o8304" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8305" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigoso-hispid" value_original="strigoso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually borne singly.</text>
      <biological_entity id="o8306" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 10–20 cm.</text>
      <biological_entity id="o8307" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres shallowly hemispheric, (15–25 ×) 5–8 mm.</text>
      <biological_entity id="o8308" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="[15" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="]5" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (often dark purple) 25–33, lanceolate to ovate, 10–14 × 3–5 mm, apices acute to acuminate, abaxial faces hispid or glabrous.</text>
      <biological_entity id="o8309" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="33" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8310" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8311" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 9–10 mm, subentire to 3-toothed (apices purplish, mucronate).</text>
      <biological_entity id="o8312" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s10" to="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0 or 2–8;</text>
      <biological_entity id="o8313" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae (sometimes purplish) 1–2 (–10) mm.</text>
      <biological_entity id="o8314" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 100–150+;</text>
      <biological_entity id="o8315" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s13" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 7–8 mm, lobes reddish;</text>
      <biological_entity id="o8316" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8317" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark, appendages dark.</text>
      <biological_entity id="o8318" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o8319" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 3–4 mm, glabrate;</text>
      <biological_entity id="o8320" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 (often unequal) aristate scales 0.5–2.9 mm. 2n = 34.</text>
      <biological_entity id="o8321" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o8322" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8323" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o8321" id="r581" name="consist_of" negation="false" src="d0_s17" to="o8322" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, open pine barrens, flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="open pine barrens" />
        <character name="habitat" value="flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Rayless or pineland sunflower</other_name>
  
</bio:treatment>