<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Petrak" date="1917" rank="species">rydbergii</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>35(2): 315. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species rydbergii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066400</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">lactucinum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species lactucinum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 100–300 cm;</text>
      <biological_entity id="o16847" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices and taproots, spreading by creeping roots.</text>
      <biological_entity id="o16848" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character constraint="by roots" constraintid="o16850" is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o16849" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character constraint="by roots" constraintid="o16850" is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o16850" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect or ascending to lax and hanging, glabrous or thinly tomentose;</text>
      <biological_entity id="o16851" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="hanging" value_original="hanging" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0–few, ascending.</text>
      <biological_entity id="o16852" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades elliptic, 30–90+ × 10–40 cm, 1–2 times pinnately lobed, lobes linear to ovate, strongly undulate, main spines slender, 5–15 mm, faces often glaucous, glabrous or thinly tomentose and soon glabrescent;</text>
      <biological_entity id="o16853" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16854" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s4" to="90" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s4" to="40" to_unit="cm" />
        <character constraint="lobe" constraintid="o16855" is_modifier="false" name="size_or_quantity" src="d0_s4" value="1-2 times pinnately lobed lobes" />
        <character constraint="lobe" constraintid="o16856" is_modifier="false" name="size_or_quantity" src="d0_s4" value="1-2 times pinnately lobed lobes" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o16855" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o16856" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity constraint="main" id="o16857" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16858" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal present at flowering, petiolate or winged-petiolate;</text>
      <biological_entity id="o16859" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o16860" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal cauline winged-petiolate;</text>
      <biological_entity id="o16861" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="proximal cauline" id="o16862" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>mid sessile, much reduced, less deeply lobed, bases clasping, short-decurrent 0–2 cm;</text>
      <biological_entity id="o16863" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="mid" value_original="mid" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="less deeply" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o16864" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s7" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-decurrent" value_original="short-decurrent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal linear or lanceolate, bractlike, very spiny.</text>
      <biological_entity id="o16865" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o16866" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="very" name="architecture_or_shape" src="d0_s8" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Heads few–many, erect or nodding in clusters at tips of distal branches in paniculiform arrays, not closely subtended by clustered leafy bracts.</text>
      <biological_entity id="o16867" name="head" name_original="heads" src="d0_s9" type="structure">
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s9" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="at tips" constraintid="o16868" is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o16868" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o16869" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o16870" name="array" name_original="arrays" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o16871" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s9" value="clustered" value_original="clustered" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o16868" id="r1513" name="part_of" negation="false" src="d0_s9" to="o16869" />
      <relation from="o16868" id="r1514" name="in" negation="false" src="d0_s9" to="o16870" />
      <relation from="o16867" id="r1515" modifier="not closely; closely" name="subtended by" negation="false" src="d0_s9" to="o16871" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 0.5–6 cm.</text>
      <biological_entity id="o16872" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres hemispheric, 1.4–2 × 1–2 cm, phyllary margins thinly tomentose or glabrate.</text>
      <biological_entity id="o16873" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s11" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o16874" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Phyllaries in 5–8 series, strongly imbricate, (green, drying green or light-brown), ovate to lance-oblong, abaxial faces with or without poorly developed glutinous ridge;</text>
      <biological_entity id="o16875" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s12" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="lance-oblong" />
      </biological_entity>
      <biological_entity id="o16876" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16877" name="face" name_original="faces" src="d0_s12" type="structure" />
      <biological_entity id="o16878" name="ridge" name_original="ridge" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="poorly" name="development" src="d0_s12" value="developed" value_original="developed" />
        <character is_modifier="true" name="coating" src="d0_s12" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o16875" id="r1516" name="in" negation="false" src="d0_s12" to="o16876" />
      <relation from="o16877" id="r1517" name="with or without" negation="false" src="d0_s12" to="o16878" />
    </statement>
    <statement id="d0_s13">
      <text>outer and mid-bases appressed, margins entire, not scabridulous-ciliolate, apices spreading or reflexed, green to brownish, lanceovate, elongate, flattened, spines slender, 3–25 mm;</text>
      <biological_entity constraint="outer" id="o16879" name="mid-base" name_original="mid-bases" src="d0_s13" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s13" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o16880" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s13" value="scabridulous-ciliolate" value_original="scabridulous-ciliolate" />
      </biological_entity>
      <biological_entity id="o16881" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s13" to="brownish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o16882" name="spine" name_original="spines" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apices of inner straight, entire.</text>
      <biological_entity id="o16883" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="of inner straight" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Corollas dull white to pink or purple, 16–20 mm, tubes 7–8.5 mm, throats 4–6.5 mm, lobes 4.5–6 mm;</text>
      <biological_entity id="o16884" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dull" value_original="dull" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s15" to="pink or purple" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s15" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16885" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16886" name="throat" name_original="throats" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16887" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style tips 2.5 mm.</text>
      <biological_entity constraint="style" id="o16888" name="tip" name_original="tips" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae gray or brown, 3.7–4.5 mm, apical collars not differentiated;</text>
      <biological_entity id="o16889" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o16890" name="collar" name_original="collars" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s17" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi 10–15 mm. 2n = 34.</text>
      <biological_entity id="o16891" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s18" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16892" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hanging gardens, seeps, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gardens" modifier="hanging" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60.</number>
  <other_name type="common_name">Rydberg’s or alcove thistle</other_name>
  <discussion>Cirsium rydbergii is endemic to the Colorado Plateau of northern Arizona and southeastern Utah.</discussion>
  
</bio:treatment>