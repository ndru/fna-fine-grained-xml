<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Linda E. Watson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="treatment_page">489</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TANACETUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 843. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 366. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus TANACETUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Derivation unknown; possibly Greek athanasia, immortality, through Medieval Latin tanazita</other_info_on_name>
    <other_info_on_name type="fna_id">132267</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [annuals, subshrubs], 5–150 cm (usually rhizomatous; usually aromatic).</text>
      <biological_entity id="o19016" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 or 2–5+, erect or prostrate to ascending, branched proximally and/or distally, glabrous or hairy (hairs basifixed and/or medifixed, sometimes stellate).</text>
      <biological_entity id="o19017" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and/or cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o19018" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly obovate to spatulate, usually 1–3-pinnately lobed, ultimate margins entire, crenate, or dentate, faces glabrous or hairy.</text>
      <biological_entity id="o19019" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly obovate" name="shape" src="d0_s5" to="spatulate" />
        <character is_modifier="false" modifier="usually 1-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19020" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19021" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually radiate, sometimes disciform (or quasi-radiate or radiant), usually in lax to dense, corymbiform arrays, rarely borne singly.</text>
      <biological_entity id="o19022" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
        <character is_modifier="false" modifier="in lax to dense , corymbiform arrays; rarely" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres mostly hemispheric or broader, (3–) 5–22+ mm diam.</text>
      <biological_entity id="o19023" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="22" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, (20–) 30–60+ in (2–) 3–5+ series, distinct, ± ovate to oblong or oblong to lanceolate or lance-linear (sometimes carinate), unequal, margins and apices (pale to dark-brown or blackish) scarious (tips sometimes dilated).</text>
      <biological_entity id="o19024" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s8" to="30" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o19025" from="30" name="quantity" src="d0_s8" to="60" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s8" to="oblong or oblong" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o19025" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19026" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o19027" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to conic or hemispheric (sometimes hairy), epaleate.</text>
      <biological_entity id="o19028" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="conic or hemispheric" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets usually 10–21+ (pistillate and fertile or neuter; corollas pale-yellow to yellow or white, usually with yellowish bases [pink], laminae oblong to flabellate), sometimes 0 (in disciform or quasi-radiate or radiant heads, peripheral pistillate florets 8–30+; corollas pale-yellow, ± zygomorphic, lobes 3–4, sometimes ± raylike).</text>
      <biological_entity id="o19029" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="21" upper_restricted="false" />
        <character modifier="sometimes" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 60–300+, bisexual, fertile;</text>
      <biological_entity id="o19030" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s11" to="300" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, tubes ± cylindric, throats narrowly funnelform to campanulate, lobes (4–) 5, ± deltate.</text>
      <biological_entity id="o19031" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o19032" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o19033" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o19034" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae obconic or ± columnar (circular in cross-section), ribs (4–) 5–10 (–12+), faces usually gland-dotted, sometimes glabrous (pericarps without myxogenic cells or resin sacs, embryo-sac development tetrasporic);</text>
      <biological_entity id="o19035" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="columnar" value_original="columnar" />
      </biological_entity>
      <biological_entity id="o19036" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s13" to="5" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="12" upper_restricted="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s13" to="10" />
      </biological_entity>
      <biological_entity id="o19037" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi usually coroniform, rarely 0 [distinct scales or each pappus an adaxial auricle].</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9 (polyploidy).</text>
      <biological_entity id="o19038" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s14" value="coroniform" value_original="coroniform" />
        <character modifier="rarely" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o19039" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia, n Africa; some species widely cultivated.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="some species widely cultivated" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>112.</number>
  <other_name type="common_name">Tansy</other_name>
  <other_name type="common_name">tanaisie</other_name>
  <discussion>Species 160 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves usually not pinnately lobed (sometimes with 1–4+ lateral lobes near bases of blades), ultimate margins ± crenate</description>
      <determination>1 Tanacetum balsamita</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves usually 1–3-pinnately lobed, ultimate margins entire or dentate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 1–2-pinnately lobed (primary lobes 3–5 pairs, ± ovate), faces (at least abaxial) usually puberulent; ray florets 10–21+ (more in "doubles"), corollas white, laminae 2–8(–12+) mm; pappi 0 or coroniform (0.1–0.2+ mm)</description>
      <determination>2 Tanacetum parthenium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 2–3-pinnately lobed (primary lobes 4–24+ pairs, ± oblong to elliptic or linear), faces usually arachno-villous to villous, sometimes glabrescent or glabrous; ray florets 0 or 8–30+, corollas pale yellow to yellow, laminae 1–8+ mm; pappi coroniform (0.1–0.5+ mm)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: faces glabrous or sparsely hairy; heads (disciform) 20–200 in corymbiform arrays; involucres 5–10 mm diam</description>
      <determination>3 Tanacetum vulgare</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: faces usually ± villous or arachno-villous to lanate, sometimes glabrescent or glabrate; heads (radiate, quasi-radiant or -radiate, or disciform) (2–)5–12(–20+) in corymbiform arrays or borne singly; involucres 8–22+ mm diam</description>
      <determination>4 Tanacetum bipinnatum</determination>
    </key_statement>
  </key>
</bio:treatment>