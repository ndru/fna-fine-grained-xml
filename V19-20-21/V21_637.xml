<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">BLEPHARIPAPPUS</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 316. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus BLEPHARIPAPPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek blepharis, eyelash, and pappos, pappus, alluding to ciliate pappus scales</other_info_on_name>
    <other_info_on_name type="fna_id">104076</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–20 (–40+) cm.</text>
      <biological_entity id="o25226" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect (scabrous and, sometimes, hirsute, usually stipitate-glandular distally).</text>
      <biological_entity id="o25227" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o25228" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25229" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite, most alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="proximal" id="o25230" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades narrowly spatulate to linear, margins entire, faces scabrous, hirsute, strigose, sericeous, or villous (distal leaves usually stipitate-glandular as well).</text>
      <biological_entity id="o25231" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly spatulate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o25232" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25233" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in open, corymbiform arrays.</text>
      <biological_entity id="o25234" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in open , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o25235" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o25234" id="r1712" name="in" negation="false" src="d0_s6" to="o25235" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0 at tips.</text>
      <biological_entity constraint="peduncular" id="o25236" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o25237" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o25238" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o25239" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character constraint="at tips" constraintid="o25240" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o25240" name="tip" name_original="tips" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres turbinate to campanulate or hemispheric, 3–6+ mm diam.</text>
      <biological_entity id="o25241" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s8" to="campanulate or hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (2–) 3–5 (–8) in 1 series, ± lanceolate or oblanceolate, herbaceous, each ± 1/2 investing subtended floret proximally, abaxially ± hirsute and/or stipitate-glandular.</text>
      <biological_entity id="o25242" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character char_type="range_value" constraint="in series" constraintid="o25243" from="3" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s9" value="herbaceous" value_original="herbaceous" />
        <character modifier="more or less" name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="abaxially more or less" name="pubescence" src="d0_s9" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o25243" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25244" name="floret" name_original="floret" src="d0_s9" type="structure" />
      <relation from="o25242" id="r1713" modifier="proximally" name="investing subtended" negation="false" src="d0_s9" to="o25244" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles convex, glabrous, paleate (paleae falling, subtending all or most disc-florets, outer herbaceous, inner scarious).</text>
      <biological_entity id="o25245" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (2–) 3–5 (–8), pistillate, fertile;</text>
      <biological_entity id="o25246" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas whitish (nerved with purple abaxially).</text>
      <biological_entity id="o25247" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 6–25 (–60+), bisexual, fertile;</text>
      <biological_entity id="o25248" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="60" upper_restricted="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="25" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas whitish, tubes shorter than or about equaling funnelform throats, lobes 5, deltate (anthers ± dark purple; styles ± hairy proximal to branches, branches ca. 0.2 mm).</text>
      <biological_entity id="o25249" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o25250" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than or about equaling funnelform throats" constraintid="o25251" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25251" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o25252" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae ± obconic or terete (basal attachments ± central, faces ± villous, apices not beaked);</text>
      <biological_entity id="o25253" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 or of 12–18 (–26) subulate fimbriate to ciliate or plumose scales.</text>
      <biological_entity id="o25255" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s16" to="26" />
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s16" to="18" />
        <character is_modifier="true" name="shape" src="d0_s16" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="fimbriate" is_modifier="true" name="shape" src="d0_s16" to="ciliate or plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>x = 8.</text>
      <biological_entity id="o25254" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character constraint="of scales" constraintid="o25255" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s16" value="of 12-18(-26) subulate fimbriate to ciliate or plumose scales" />
      </biological_entity>
      <biological_entity constraint="x" id="o25256" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>333.</number>
  <discussion>Species 1.</discussion>
  <discussion>Blepharipappus and members of Layia were once treated as congeneric; Layia appears to be more closely related to Lagophylla than to Blepharipappus, based on molecular phylogenetic data (S. Carlquist et al. 2003).</discussion>
  
</bio:treatment>