<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
    <other_info_on_meta type="illustration_page">328</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="(Nuttall) A. Nelson" date="1904" rank="species">glacialis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>37: 270. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species glacialis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066603</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">glacialis</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 291. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species glacialis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="(Richardson ex R. Brown) A. Gray" date="unknown" rank="species">salsuginosus</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="variety">glacialis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species salsuginosus;variety glacialis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–55 (–70) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices usually simple, thick.</text>
      <biological_entity id="o10912" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o10913" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, hirsute or hirsuto-villous to densely strigillose (hairs loosely appressed, slightly crinkled), eglandular.</text>
      <biological_entity id="o10914" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="hirsuto-villous" name="pubescence" src="d0_s2" to="densely strigillose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (usually persistent) and cauline;</text>
      <biological_entity id="o10915" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades linear-oblanceolate to broadly lanceolate or spatulate, (20–) 30–160 (–200) × 7–45 mm, cauline gradually reduced distally (bases distinctly subclasping, except when greatly reduced), margins entire, faces glabrous or glabrate, hirsute, or villous to sparsely villosulous, eglandular.</text>
      <biological_entity constraint="basal" id="o10916" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s4" to="broadly lanceolate or spatulate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="160" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="200" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="160" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10917" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10918" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10919" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrate hirsute or villous" name="pubescence" src="d0_s4" to="sparsely villosulous" />
        <character char_type="range_value" from="glabrate hirsute or villous" name="pubescence" src="d0_s4" to="sparsely villosulous" />
        <character char_type="range_value" from="glabrate hirsute or villous" name="pubescence" src="d0_s4" to="sparsely villosulous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–8).</text>
      <biological_entity id="o10920" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 6–9 (–12) × 10–22 (–25) mm.</text>
      <biological_entity id="o10921" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series (faces and margins) usually glabrous, rarely sparsely villous (cross-walls not colored), densely and evenly stipitate-glandular.</text>
      <biological_entity id="o10922" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" notes="" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely; evenly" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o10923" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o10922" id="r999" name="in" negation="false" src="d0_s7" to="o10923" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 30–80;</text>
      <biological_entity id="o10924" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually blue to rose-purple or pink, sometimes white to pale blue, 8–16 (–25) mm (mostly 1.5–3 mm wide), laminae coiling.</text>
      <biological_entity id="o10925" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually blue" name="coloration" src="d0_s9" to="rose-purple or pink" />
        <character char_type="range_value" from="white" modifier="sometimes" name="coloration" src="d0_s9" to="pale blue" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10926" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.8–6 mm.</text>
      <biological_entity constraint="disc" id="o10927" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–2.8 mm, (4–) 5 (–7) -nerved, faces sparsely strigose;</text>
      <biological_entity id="o10928" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="(4-)5(-7)-nerved" value_original="(4-)5(-7)-nerved" />
      </biological_entity>
      <biological_entity id="o10929" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 20–30 bristles.</text>
      <biological_entity id="o10930" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o10931" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o10932" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
      <relation from="o10930" id="r1000" name="outer of" negation="false" src="d0_s12" to="o10931" />
      <relation from="o10930" id="r1001" name="inner of" negation="false" src="d0_s12" to="o10932" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>132.</number>
  <other_name type="common_name">Subalpine fleabane</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Peduncles densely strigillose with loosely appressed, slightly crinkled hairs; leaf faces glabrous or villous</description>
      <determination>132a Erigeron glacialis var. glacialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Peduncles hirsute to hirsuto-villous; leaf faces hirsute to hirsuto-villous</description>
      <determination>132b Erigeron glacialis var. hirsutus</determination>
    </key_statement>
  </key>
</bio:treatment>