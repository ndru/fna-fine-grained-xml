<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">flavescens</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 97. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species flavescens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066976</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, 30–90 cm.</text>
      <biological_entity id="o21477" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 2-pinnate, 6–15 cm, lobes 3–15 (–20+) × 1–6 mm;</text>
      <biological_entity id="o21478" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o21479" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21480" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="20" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 10–50.</text>
      <biological_entity id="o21481" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o21482" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 15–100 per stem.</text>
      <biological_entity id="o21483" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o21484" from="15" name="quantity" src="d0_s3" to="100" />
      </biological_entity>
      <biological_entity id="o21484" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–6 cm.</text>
      <biological_entity id="o21485" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries yellowish, 4–8 × 2–4 mm.</text>
      <biological_entity id="o21486" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 0.</text>
      <biological_entity id="o21487" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o21488" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 2.5–3.5 mm, tubes 1.5–2 mm, throats campanulate, 0.8–1.5 mm, lengths 1–1.5 times lobes.</text>
      <biological_entity id="o21489" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21490" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21491" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character constraint="lobe" constraintid="o21492" is_modifier="false" name="length" src="d0_s8" value="1-1.5 times lobes" value_original="1-1.5 times lobes" />
      </biological_entity>
      <biological_entity id="o21492" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 3–4.5 mm, ± villous;</text>
      <biological_entity id="o21493" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 16–22 scales 0.5–1.5 mm.</text>
      <biological_entity id="o21494" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o21495" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s10" to="22" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o21494" id="r1480" name="consist_of" negation="false" src="d0_s10" to="o21495" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., N.Mex., Okla., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of basal leaves (1–)2–6 mm wide, abaxial faces glabrous or less hairy than adaxial; pappi 0.5–1(–1.5) mm</description>
      <determination>8a Hymenopappus flavescens var. flavescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of basal leaves 1–2 mm wide, abaxial and adaxial faces ± equally hairy; pappi 1–1.5 mm</description>
      <determination>8b Hymenopappus flavescens var. canotomentosus</determination>
    </key_statement>
  </key>
</bio:treatment>