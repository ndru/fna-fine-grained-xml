<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="treatment_page">524</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="Alph. Wood ex Carruth" date="1877" rank="species">carruthii</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Kansas Acad. Sci.</publication_title>
      <place_in_publication>5: 51. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species carruthii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416099</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">bakeri</taxon_name>
    <taxon_hierarchy>genus Artemisia;species bakeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Osterhout" date="unknown" rank="species">coloradensis</taxon_name>
    <taxon_hierarchy>genus Artemisia;species coloradensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">kansana</taxon_name>
    <taxon_hierarchy>genus Artemisia;species kansana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">wrightii</taxon_name>
    <taxon_hierarchy>genus Artemisia;species vulgaris;subspecies wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus Artemisia;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–40 (–70) cm, faintly aromatic (rhizomatous).</text>
      <biological_entity id="o7167" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly 3–8, ascending, brown to gray-green, simple (bases curved, somewhat woody), sparsely to densely tomentose.</text>
      <biological_entity id="o7168" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="gray-green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, bicolor (± gray-green);</text>
      <biological_entity id="o7169" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bicolor" value_original="bicolor" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades narrowly elliptic, 0.1–2.5 (–3) × 0.5–1 cm (gradually smaller distally), relatively deeply pinnatifid (lobes 3–5), faces densely tomentose (abaxial) to sparsely hairy (adaxial).</text>
      <biological_entity id="o7170" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="relatively deeply" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o7171" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="densely tomentose" name="pubescence" src="d0_s3" to="sparsely hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (usually nodding) in (leafy) paniculiform arrays 10–30 × 3–9 cm (branches erect).</text>
      <biological_entity id="o7172" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" modifier="in paniculiform arrays" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="in paniculiform arrays" name="width" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate, 2–2.5 (–3) × 1.5–3 mm.</text>
      <biological_entity id="o7173" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate, gray-tomentose.</text>
      <biological_entity id="o7174" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 1–5;</text>
      <biological_entity id="o7175" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 7–25;</text>
      <biological_entity id="o7176" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas pale-yellow, 1–2 mm, glandular-pubescent.</text>
      <biological_entity id="o7177" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o7178" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (light-brown) cylindro-elliptic, ca. 0.5 mm, (curved at summits, scarcely nerved), glabrous (shining).</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o7179" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="cylindro-elliptic" value_original="cylindro-elliptic" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7180" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, usually sandy soils, wooded areas, grasslands, railroads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="open" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="wooded areas" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="railroads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., Mich., Mo., N.Mex., Okla., Tex., Utah; Mexico (Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Carruth wormwood</other_name>
  <discussion>Artemisia carruthii is closely related to members of the A. ludoviciana complex, with which it may intergrade.</discussion>
  
</bio:treatment>