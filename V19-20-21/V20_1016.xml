<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">442</other_info_on_meta>
    <other_info_on_meta type="treatment_page">443</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) G. L. Nesom" date="1991" rank="species">menziesii</taxon_name>
    <taxon_name authority="(Nuttall) G. L. Nesom" date="1991" rank="variety">vernonioides</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 101. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species menziesii;variety vernonioides</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068539</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isocoma</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">vernonioides</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 320. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Isocoma;species vernonioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Kunth) S. F. Blake" date="unknown" rank="species">venetus</taxon_name>
    <taxon_name authority="(Nuttall) H. M. Hall" date="unknown" rank="subspecies">vernonioides</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species venetus;subspecies vernonioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isocoma</taxon_name>
    <taxon_name authority="(Kunth) Greene" date="unknown" rank="species">veneta</taxon_name>
    <taxon_name authority="(Nuttall) Jepson" date="unknown" rank="variety">vernonioides</taxon_name>
    <taxon_hierarchy>genus Isocoma;species veneta;variety vernonioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to ascending or decumbent;</text>
      <biological_entity id="o10152" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending or decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>herbage hirtellous or villoso-pilose or densely gray-tomentose (hairs flattened, vitreous, long) to glabrate, sometimes stipitate-glandular.</text>
      <biological_entity id="o10153" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character char_type="range_value" from="villoso-pilose or densely gray-tomentose" name="pubescence" src="d0_s1" to="glabrate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear to oblanceolate or spatulate-oblong, 10–40 × 2–6 (–9) mm, not thick-fleshy, margins of at least the proximal pinnately lobed or shallowly toothed.</text>
      <biological_entity id="o10154" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="oblanceolate or spatulate-oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="thick-fleshy" value_original="thick-fleshy" />
      </biological_entity>
      <biological_entity id="o10155" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o10156" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o10157" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o10158" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o10157" id="r939" name="in" negation="false" src="d0_s3" to="o10158" />
    </statement>
    <statement id="d0_s4">
      <text>Corollas 5–7 mm.</text>
      <biological_entity id="o10159" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2.3–3.6 mm. 2n = 24.</text>
      <biological_entity id="o10160" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s5" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10161" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)Jul–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal bluffs and dunes, sandy flats, salt marsh borders, occasionally on dry slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="salt" />
        <character name="habitat" value="borders" modifier="marsh" />
        <character name="habitat" value="dry slopes" modifier="occasionally on" />
        <character name="habitat" value="marsh" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>5–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="5" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In San Diego and Riverside counties, plants of var. vernonioides produce small leaves that are often nearly glabrous, perhaps reflecting the influence of genes from var. menziesii.</discussion>
  
</bio:treatment>