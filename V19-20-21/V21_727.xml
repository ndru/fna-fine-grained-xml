<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="1999" rank="genus">HARMONIA</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 463. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus HARMONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Harvey Monroe Hall, 1874–1932, Californian botanist</other_info_on_name>
    <other_info_on_name type="fna_id">316914</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–40 cm.</text>
      <biological_entity id="o9292" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o9293" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o9294" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9295" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite, distal alternate;</text>
      <biological_entity constraint="proximal" id="o9296" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="distal" id="o9297" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear, margins entire or toothed, faces usually hirsute, sometimes minutely stipitate-glandular as well (glands usually black, sometimes yellowish).</text>
      <biological_entity id="o9298" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o9299" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9300" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes minutely; well" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, ± umbelliform to corymbiform arrays.</text>
      <biological_entity id="o9301" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o9302" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , more or less umbelliform to corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o9302" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="loose" value_original="loose" />
        <character char_type="range_value" from="less umbelliform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and spines 0.</text>
      <biological_entity constraint="peduncular" id="o9303" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o9304" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o9305" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o9306" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres obovoid to obconic, 2–5+ mm diam.</text>
      <biological_entity id="o9307" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s8" to="obconic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 3–8 in 1 series (lanceolate to oblanceolate, herbaceous, strongly conduplicate, each wholly enveloping a ray ovary).</text>
      <biological_entity id="o9308" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9309" from="3" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
      <biological_entity id="o9309" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, glabrous or setulose, paleate (paleae falling, in 1 series between rays and discs, distinct or weakly connate, phyllary-like, more scarious).</text>
      <biological_entity id="o9310" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets usually 3–8, pistillate, fertile;</text>
      <biological_entity id="o9311" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas bright-yellow (laminae flabelliform to obovate).</text>
      <biological_entity id="o9312" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 7–30, bisexual and fertile or functionally staminate (sometimes in same head);</text>
      <biological_entity id="o9313" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s13" to="30" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas bright-yellow, tubes shorter than narrowly funnelform throats, lobes 5, deltate (anthers yellowish to brownish; styles glabrous proximal to branches).</text>
      <biological_entity id="o9314" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
      <biological_entity id="o9315" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o9316" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9316" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o9317" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ray cypselae (black) terete to ± compressed, weakly arcuate, gibbous or not, beaked or beakless, glabrous;</text>
      <biological_entity constraint="ray" id="o9318" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s15" to="more or less compressed" />
        <character is_modifier="false" modifier="weakly" name="course_or_shape" src="d0_s15" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="gibbous" value_original="gibbous" />
        <character name="shape" src="d0_s15" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="beakless" value_original="beakless" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 or of 3–12 lanceolate to subulate, fimbrillate to plumose scales.</text>
      <biological_entity id="o9319" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character constraint="of scales" constraintid="o9320" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 3" is_modifier="false" name="quantity" src="d0_s16" to="12 lanceolate to subulate fimbrillate to plumose , scales" />
      </biological_entity>
      <biological_entity id="o9320" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s16" to="12" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s16" to="subulate fimbrillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Disc cypselae (black) ± terete, ± clavate, glabrous or hairy;</text>
      <biological_entity constraint="disc" id="o9321" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi of 7–11 lance-attenuate, linear, oblong, quadrate, or subulate, fimbriate, fimbrillate, or plumose scales.</text>
      <biological_entity id="o9323" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s18" to="11" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lance-attenuate" value_original="lance-attenuate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s18" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbrillate" value_original="fimbrillate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="plumose" value_original="plumose" />
      </biological_entity>
      <relation from="o9322" id="r646" name="consist_of" negation="false" src="d0_s18" to="o9323" />
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity id="o9322" name="pappus" name_original="pappi" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o9324" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>346.</number>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>Members of Harmonia occur in mountains of northwestern California, as far south as the northern San Francisco Bay area. All but H. nutans are known only from serpentine exposures and are probably descended from a common, serpentine-endemic ancestor (B. G. Baldwin 2001). Harmonia has been treated in Madia, which is more closely related to Carlquistia than to Harmonia (B. G. Baldwin 1996). See Baldwin (2001) for discussion of phylogeny within Harmonia.</discussion>
  <references>
    <reference>Baldwin, B. G. 2001. Harmonia guggolziorum (Compositae–Madiinae), a new tarweed from ultramafics of southern Mendocino County, California. Madroño 48: 293–297.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually reflexed in bud and fruit; ray pappi 0; disc pappi of lance-attenuate, fimbrillatescales 2–3.7 mm</description>
      <determination>1 Harmonia nutans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually erect in bud and fruit; ray pappi 0.2–1.5 mm; disc pappi of lanceolate, linear, oblong, quadrate, or subulate, fimbriate or plumose scales 0.2–3.5 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves ± evenly distributed on stems; ray cypselae strongly gibbous (bowed abaxially), beaked (beaks 0.4–0.5 mm); disc florets functionally staminate</description>
      <determination>3 Harmonia doris-nilesiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves unevenly distributed, mostly along primary (central) stems and immediately proximal to branches supporting heads; ray cypselae slightly or not gibbous, beakless (pappi elevated adaxially 0.1–0.2 mm in H. stebbinsii); disc florets (some or all) bisexual, fertile</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries densely white-villous near folded edges; ray cypselae slightly gibbous; disc pappi of linear-attenuate to subulate, plumose scales 1.2–3.5 mm</description>
      <determination>4 Harmonia stebbinsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries hirsute and/or hirtellous near folded edges; ray cypselae not gibbous; disc pappi of lanceolate, linear, oblong, or quadrate, fimbriate scales 0.2–0.8 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal, unbranched portions of primary (central) stems usually shorter than branches supporting heads (distal leaves of primary stems densely congested); disc pappi of oblong or quadrate scales 0.2–0.5 mm</description>
      <determination>5 Harmonia hallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal, unbranched portions of primary (central) stems usually longer than branches supporting heads (distal leaves of primary stems not densely congested);disc pappi of lanceolate to linear scales 0.6–0.8 mm</description>
      <determination>2 Harmonia guggolziorum</determination>
    </key_statement>
  </key>
</bio:treatment>