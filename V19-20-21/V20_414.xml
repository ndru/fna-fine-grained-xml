<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
    <other_info_on_meta type="illustration_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(S. F. Blake) L. C. Anderson" date="1964" rank="species">molestus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>17: 222. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species molestus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066351</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="unknown" rank="species">viscidiflorus</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="variety">molestus</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>30: 468. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species viscidiflorus;variety molestus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 8–20 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with woody, highly branched caudices, bark dark gray, highly fibrous with age.</text>
      <biological_entity id="o18186" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o18187" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="true" modifier="highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o18188" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray" value_original="dark gray" />
        <character constraint="with age" constraintid="o18189" is_modifier="false" modifier="highly" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o18189" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o18186" id="r1681" name="with" negation="false" src="d0_s1" to="o18187" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, green, ± puberulent, stipitate-glandular.</text>
      <biological_entity id="o18190" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves erect to closely ascending;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o18191" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="closely ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades with ± evident midnerves, linear to narrowly elliptic, 7–20 × 0.7–1.5 mm, sulcate, sometimes apiculate, apices acute, faces moderately puberulent, uniformly stipitate-glandular.</text>
      <biological_entity id="o18192" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" notes="" src="d0_s5" to="narrowly elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o18193" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s5" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o18194" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o18195" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="uniformly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o18192" id="r1682" name="with" negation="false" src="d0_s5" to="o18193" />
    </statement>
    <statement id="d0_s6">
      <text>Heads in small cymiform to racemiform arrays.</text>
      <biological_entity id="o18196" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o18197" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="small" value_original="small" />
        <character char_type="range_value" from="cymiform" is_modifier="true" name="architecture" src="d0_s6" to="racemiform" />
      </biological_entity>
      <relation from="o18196" id="r1683" name="in" negation="false" src="d0_s6" to="o18197" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres subcylindric, 9–11 × 2.5–3.5 mm.</text>
      <biological_entity id="o18198" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 20 in 4–5 series, in 4–5 strong vertical ranks, tan, often with green or dark subapical splotch, midnerves often obscure, linear or lanceolate to elliptic, 2–9 × 0.5–1.3 mm, unequal, outer ± herbaceous, inner chartaceous, strongly keeled, apices acute to rounded, tip cupped, faces of outer glabrous or puberulent.</text>
      <biological_entity id="o18199" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character constraint="in series" constraintid="o18200" name="quantity" src="d0_s8" value="20" value_original="20" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity id="o18200" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o18201" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="true" name="fragility" src="d0_s8" value="strong" value_original="strong" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o18202" name="splotch" name_original="splotch" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o18203" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18204" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18205" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o18206" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o18207" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o18208" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18209" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o18199" id="r1684" name="in" negation="false" src="d0_s8" to="o18201" />
      <relation from="o18199" id="r1685" modifier="often" name="with" negation="false" src="d0_s8" to="o18202" />
      <relation from="o18208" id="r1686" name="part_of" negation="false" src="d0_s8" to="o18209" />
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 5;</text>
      <biological_entity id="o18210" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 5.5–7.5 mm, lobes 0.9–1.5 mm;</text>
      <biological_entity id="o18211" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18212" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style-branches 2.7–3.2 mm, appendages 1–1.7 mm.</text>
      <biological_entity id="o18213" name="branch-style" name_original="style-branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="distance" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18214" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan, elliptic, 4.2–6 mm, mostly 5-ribbed, faces glabrous, sparsely glandular;</text>
      <biological_entity id="o18215" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s12" value="5-ribbed" value_original="5-ribbed" />
      </biological_entity>
      <biological_entity id="o18216" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tan, 6–7.5 mm. 2n = 18.</text>
      <biological_entity id="o18217" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18218" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils, mostly on limestone pinyon-juniper woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="limestone pinyon-juniper woodland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Arizona rabbitbrush</other_name>
  <discussion>Chrysothamnus molestus is known only from Coconino County. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>