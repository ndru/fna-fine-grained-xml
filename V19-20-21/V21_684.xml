<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="treatment_page">280</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">DEINANDRA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>4: 424. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus DEINANDRA</taxon_hierarchy>
    <other_info_on_name type="etymology">No etymology stated in protologue; meaning uncertain</other_info_on_name>
    <other_info_on_name type="fna_id">109480</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, subshrubs, or shrubs, 4–120 (–150) cm.</text>
      <biological_entity id="o16408" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o16409" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually ± erect, rarely decumbent.</text>
      <biological_entity id="o16411" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline (annuals forming winter–spring rosettes, usually withering by flowering);</text>
      <biological_entity id="o16412" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16413" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite, most alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="proximal" id="o16414" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate to linear or lance-linear, usually pinnatifid or toothed, sometimes serrate or entire, faces often hirsute or hispid-hirsute, sometimes villous, pilose, pubescent, canescent, strigose, or scabrous (often sessile or stipitate-glandular as well) or glabrous (with scabrous or hispid margins).</text>
      <biological_entity id="o16415" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate to linear" value_original="oblanceolate to linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16416" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid-hirsute" value_original="hispid-hirsute" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, usually in corymbiform or ± paniculiform arrays, sometimes in racemiform arrays or in glomerules.</text>
      <biological_entity id="o16417" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o16418" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o16419" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <biological_entity id="o16420" name="glomerule" name_original="glomerules" src="d0_s6" type="structure" />
      <relation from="o16417" id="r1129" modifier="usually" name="in" negation="false" src="d0_s6" to="o16418" />
      <relation from="o16417" id="r1130" modifier="sometimes" name="in" negation="false" src="d0_s6" to="o16419" />
      <relation from="o16417" id="r1131" modifier="sometimes" name="in" negation="false" src="d0_s6" to="o16420" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and spines 0.</text>
      <biological_entity constraint="peduncular" id="o16421" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o16422" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o16423" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o16424" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± obconic, campanulate, hemispheric, or urceolate, 2–13+ mm diam.</text>
      <biological_entity id="o16425" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="13" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries falling, 3–35 in 1 series (± lanceolate to lance-attenuate or oblanceolate, herbaceous, each usually 1/2 enveloping subtended ray-floret proximally, abaxially ± hirsute and sessile or stipitate-glandular).</text>
      <biological_entity id="o16426" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o16427" from="3" name="quantity" src="d0_s9" to="35" />
      </biological_entity>
      <biological_entity id="o16427" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, glabrous or setulose, paleate (paleae falling, in 1 series between rays and disc in annuals, in 2–3+ series or subtending all or most disc-florets in subshrubs and shrubs, connate or distinct, phyllary-like, more scarious).</text>
      <biological_entity id="o16428" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–35, pistillate, fertile;</text>
      <biological_entity id="o16429" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="35" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas deep or pale-yellow.</text>
      <biological_entity id="o16430" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 3–70, usually functionally staminate, seldom bisexual and fertile;</text>
      <biological_entity id="o16431" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="70" />
        <character is_modifier="false" modifier="usually functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="seldom" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes shorter than or about equaling funnelform throats, lobes 5, deltate (anthers usually reddish to dark purple or yellow, rarely maroon; styles glabrous proximal to branches).</text>
      <biological_entity id="o16432" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o16433" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than or about equaling funnelform throats" constraintid="o16434" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o16434" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o16435" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ray cypselae slightly obcompressed (adaxial sides flatter than abaxials), clavate (abaxially gibbous, often ± arcuate, basal attachments oblique, apices ± beaked, beaks offset adaxially, ascending, faces glabrous);</text>
      <biological_entity constraint="ray" id="o16436" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
      <biological_entity id="o16437" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Disc cypselae usually 0;</text>
      <biological_entity constraint="disc" id="o16438" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi (of disc-florets) usually of 1–15 elliptic, lance-linear, lanceolate, linear, oblong, quadrate, setiform, or subulate, entire, erose, fimbriate, fringed, or laciniate scales, sometimes 0 or coroniform (crowns of ± linear, sometimes fimbriate scales).</text>
      <biological_entity id="o16440" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s18" to="15" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s18" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="shape" src="d0_s18" value="quadrate" value_original="quadrate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="setiform" value_original="setiform" />
        <character is_modifier="true" name="shape" src="d0_s18" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s18" value="entire" value_original="entire" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s18" value="erose" value_original="erose" />
        <character is_modifier="true" name="shape" src="d0_s18" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="true" name="shape" src="d0_s18" value="fringed" value_original="fringed" />
        <character is_modifier="true" name="shape" src="d0_s18" value="laciniate" value_original="laciniate" />
        <character modifier="sometimes" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o16439" id="r1132" name="consist_of" negation="false" src="d0_s18" to="o16440" />
    </statement>
    <statement id="d0_s19">
      <text>x = 12 or 13.</text>
      <biological_entity id="o16439" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o16441" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="12" value_original="12" />
        <character name="quantity" src="d0_s19" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>339.</number>
  <discussion>Species 21 (16 in the flora).</discussion>
  <discussion>Deinandra is treated here as distinct from Hemizonia; Deinandra is more closely related to Calycadenia, Centromadia, Holocarpha, and Osmadenia than to Hemizonia in the strict sense (S. Carlquist et al. 2003). As circumscribed here (following B. G. Baldwin 1999b), Deinandra comprises all taxa included in Hemizonia sect. Madiomeris by B. D. Tanowitz (1982) plus the shrubs and subshrubs constituting the informal “Fruticosae” or “Zonamra” (J. Clausen 1951; D. D. Keck 1959b). All are self-incompatible except D. arida and D. mohavensis. Five species (D. frutescens, D. greeneana, D. martirensis, D. palmeri, D. streetsii), none annuals, are known only from Baja California, Mexico. Most species north of Mexico are cross-incompatible or ± intersterile (J. Clausen 1951).</discussion>
  <references>
    <reference>Tanowitz, B. D. 1982. Taxonomy of Hemizonia sect. Madiomeris (Asteraceae: Madiinae). Syst. Bot. 7: 314–339.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Subshrubs or shrubs; paleae in 2 series or in 3+ series or throughout receptacles</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; paleae in 1 series (between ray and disc florets)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Paleae in 2 series (1 between ray and disc florets, 1 between outermost and adjacent discflorets); ray florets (11–)13(–20); anthers reddish to dark purple</description>
      <determination>7 Deinandra clementina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Paleae in 3+ series or throughout receptacles; ray florets (4–)8; anthers yellow or brownish</description>
      <determination>8 Deinandra minthornii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets 3–5; disc florets 3–6; pappi of 5–12 scales</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets (5–)8–35; disc florets 8–70; pappi 0, or of 1–14 scales, or coroniform (reputedly sometimes rudimentary)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ray florets 3(–4); disc florets 3(–4)</description>
      <determination>12 Deinandra lobbii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ray florets 5; disc florets 6</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Phyllaries usually sessile-glandular, at least near margins, rarely stipitate-glandular (stalks shorter than glands); anthers reddish to dark purple</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Phyllaries stipitate-glandular (stalks often equal to or longer than glands); anthers yellow or brownish (reddish to dark purple or maroon in some, mostly s Californian, D. kelloggii)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Bracts subtending heads usually overlapping at least proximal 1/2 of each involucre; phyllaries glandular near margins, sometimes with non-glandular, non-pustule-based hairs as well</description>
      <determination>1 Deinandra fasciculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Bracts subtending heads usually overlapping proximal 0–1/2 of each involucre; phyllaries ± evenly glandular and with pustule-based hairs, atleast on midribs</description>
      <determination>11 Deinandra pentactis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal leaves usually pinnatifid to toothed (rarely entire); heads in open, paniculiform arrays; pappi of linear to oblong, entire or fringed scales</description>
      <determination>13 Deinandra kelloggii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal leaves usually entire, sometimes serrate; heads usually in glom-erules; pappi of irregular, erose scales</description>
      <determination>5 Deinandra mohavensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Anthers yellow or brownish</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Anthers reddish to dark purple</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Faces of proximal leaves glabrous or hispid-hirsute and stipitate-glandular; disc florets 17–60; pappi usually 0, rarely of 1–5 linear to setiform scales 0.1–0.6 mm or, reputedly, rudimentary</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Faces of proximal leaves ± hirsute and stipitate-glandular; disc florets 10–21; pappi usually of 4–13 scales, sometimes coroniform (in D. bacigalupii), rarely 0 (in Deinandra pallida)</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems solid; leaves (proximal) hispid-hirsute and stipitate-glandular; disc florets 17–25</description>
      <determination>4 Deinandra arida</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems fistulose; leaves (proximal) glabrous (margins and midribs sometimes scabrous or hispid); disc florets 28–60</description>
      <determination>10 Deinandra halliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Ray laminae pale yellow, 6–12 mm</description>
      <determination>14 Deinandra pallida</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Ray laminae deep yellow, 2–4 mm</description>
      <determination>15 Deinandra bacigalupii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Disc florets all or mostly bisexual</description>
      <determination>3 Deinandra floribunda</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Disc florets all or mostly functionally staminate</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Ray florets 15–35; pappi 0, or coroniform (irregular crowns of entire, erose, orlaciniate scales 0.1–0.9 mm)</description>
      <determination>16 Deinandra corymbosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Ray florets (7–)8–13; pappi of 4–14 elliptic, lanceolate, linear, oblong, or quadrate, barely fringed to erose scales 0.5–2 mm</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries sessile- and stipitate-glandular, mostly near proximal margins and on apices</description>
      <determination>2 Deinandra conjugens</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries ± evenly stipitate-glandular, including margins and apices</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Ray florets (7–)8(–10); disc florets 8–14(–15) (basal leaves sometimes present at flowering in South Coast Ranges)</description>
      <determination>6 Deinandra paniculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Ray florets 8–13(–15); disc florets 11–32</description>
      <determination>9 Deinandra increscens</determination>
    </key_statement>
  </key>
</bio:treatment>