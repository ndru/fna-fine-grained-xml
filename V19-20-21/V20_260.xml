<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="treatment_page">128</other_info_on_meta>
    <other_info_on_meta type="illustration_page">124</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray in A. Gray et al." date="1884" rank="subsection">glomeruliflorae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">flexicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 879. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection glomeruliflorae;species flexicaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417284</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Doria</taxon_name>
    <taxon_name authority="(Linnaeus) Lunell" date="unknown" rank="species">flexicaulis</taxon_name>
    <taxon_hierarchy>genus Doria;species flexicaulis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">flexicaulis</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="variety">ciliata</taxon_name>
    <taxon_hierarchy>genus Solidago;species flexicaulis;variety ciliata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">flexicaulis</taxon_name>
    <taxon_name authority="(Linnaeus) Pursh" date="unknown" rank="variety">latifolia</taxon_name>
    <taxon_hierarchy>genus Solidago;species flexicaulis;variety latifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">latifolia</taxon_name>
    <taxon_hierarchy>genus Solidago;species latifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scrophulariifolia</taxon_name>
    <taxon_hierarchy>genus Solidago;species scrophulariifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (15–) 25–75 (–90) cm;</text>
      <biological_entity id="o29840" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices woody, rhizomes short.</text>
      <biological_entity id="o29841" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o29842" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, flexuous, sparsely to moderately hairy in arrays.</text>
      <biological_entity id="o29843" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character is_modifier="false" name="course" src="d0_s2" value="flexuous" value_original="flexuous" />
        <character constraint="in arrays" is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: rosettes present at flowering;</text>
      <biological_entity id="o29844" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o29845" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline tapering to winged petioles, blades ovate, (70–) 100–140 (–180) × (27–) 40–60 (–80) mm (petioles 1/4–1/2 total leaf length), margins serrate-serrulate [teeth (14–) 21–33 (–45)], abaxial faces glabrous or moderately hairy, more densely so on nerves, adaxial glabrous or sparsely hairy;</text>
      <biological_entity id="o29846" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o29847" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o29848" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="70" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="140" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="180" to_unit="mm" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s4" to="140" to_unit="mm" />
        <character char_type="range_value" from="27" from_unit="mm" name="atypical_width" src="d0_s4" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29849" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate-serrulate" value_original="serrate-serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29850" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o29851" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o29852" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o29850" id="r2756" modifier="densely" name="on" negation="false" src="d0_s4" to="o29851" />
    </statement>
    <statement id="d0_s5">
      <text>mid to distal cauline sessile, blades narrowly ovate to lanceolate, 38–90 (–150) × 10–30 (–55) mm, reduced distally, becoming lanceolate in arrays, bases tapering, margins entire to serrate, apices acuminate to cuspidate, faces glabrous or sparsely hairy, abaxial glabrous or moderately hairy, more densely so along nerves.</text>
      <biological_entity id="o29853" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o29854" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29855" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="38" from_unit="mm" name="length" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="in arrays" constraintid="o29856" is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o29856" name="array" name_original="arrays" src="d0_s5" type="structure" />
      <biological_entity id="o29857" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o29858" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="entire to serrate" value_original="entire to serrate" />
      </biological_entity>
      <biological_entity id="o29859" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="cuspidate" />
      </biological_entity>
      <biological_entity id="o29860" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29861" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o29862" name="nerve" name_original="nerves" src="d0_s5" type="structure" />
      <relation from="o29861" id="r2757" modifier="densely" name="along" negation="false" src="d0_s5" to="o29862" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 25–250, in short axillary and terminal racemiform clusters, lateral panicles (2–) 7–31 (–56) cm.</text>
      <biological_entity id="o29863" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s6" to="250" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o29864" name="axillary" name_original="axillary" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o29865" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="31" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="56" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s6" to="31" to_unit="cm" />
      </biological_entity>
      <relation from="o29863" id="r2758" name="in" negation="false" src="d0_s6" to="o29864" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 0.5–5 mm, moderately to densely strigose;</text>
      <biological_entity id="o29866" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1–3, lanceolate, usually near base of involucres and grading into phyllaries.</text>
      <biological_entity id="o29867" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o29868" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o29869" name="involucre" name_original="involucres" src="d0_s8" type="structure" />
      <biological_entity id="o29870" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o29867" id="r2759" modifier="usually" name="near" negation="false" src="d0_s8" to="o29868" />
      <relation from="o29868" id="r2760" name="part_of" negation="false" src="d0_s8" to="o29869" />
      <relation from="o29867" id="r2761" name="into" negation="false" src="d0_s8" to="o29870" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 4.5–7 (–8) mm.</text>
      <biological_entity id="o29871" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in ca. 3 series, strongly unequal, outermost 1–2 mm, innermost 3.7–4.7 (–5.7) mm, linear-oblong, 1-nerved, apices obtuse to acute.</text>
      <biological_entity id="o29872" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o29873" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o29874" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o29875" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5.7" to_unit="mm" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s10" to="4.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
      <biological_entity id="o29876" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
      <relation from="o29872" id="r2762" name="in" negation="false" src="d0_s10" to="o29873" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 1–5;</text>
      <biological_entity id="o29877" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae (2–) 2.5–4 (–5) × 0.7–2 mm.</text>
      <biological_entity id="o29878" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s12" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 4–8 (–11);</text>
      <biological_entity id="o29879" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="11" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2–3 (–4) mm, lobes 1–1.6 (–2) mm.</text>
      <biological_entity id="o29880" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29881" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (obconic) 1–2 (–3) mm, moderately to densely strigose;</text>
      <biological_entity id="o29882" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 3–4.5 mm. 2n = 18, 36.</text>
      <biological_entity id="o29883" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29884" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded woods and thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded woods" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Pa., S.Dak., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Zig-zag or broadleaved goldenrod</other_name>
  <other_name type="common_name">verge d’or à tige zigzaguante</other_name>
  <discussion>The two cytotypes of Solidago flexicaulis show a strong geographic pattern. Diploids mostly occur east of the Appalachians except in the southwestern portion of the range, while tetraploids occur west of the mountains (J. G. Chmielewski and J. C. Semple 1985). The report of 2n = 90 for the species (Semple et al. 1993) was based on a specimen of the recently described Solidago faucibus. The significance of ploidy level on cypselae traits was analyzed in detail by Chmielewski et al. (1989).</discussion>
  
</bio:treatment>