<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">49</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="section">macrocline</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">laciniata</taxon_name>
    <taxon_name authority="Perdue" date="1962" rank="variety">bipinnata</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>64: 328. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section macrocline;species laciniata;variety bipinnata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068698</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal 15–40 × 10–25 cm, blades pinnately compound to 2-pinnatifid;</text>
      <biological_entity id="o847" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o848" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o849" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s0" value="compound" value_original="compound" />
        <character is_modifier="false" name="shape" src="d0_s0" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal cauline 2-pinnatifid, mid cauline blades 5–11-lobed (distal usually 3-lobed);</text>
      <biological_entity id="o850" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity constraint="cauline mid" id="o851" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="5-11-lobed" value_original="5-11-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>adaxial faces sparsely to moderately hairy.</text>
      <biological_entity id="o852" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o853" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Receptacles globose to ovoid;</text>
      <biological_entity id="o854" name="receptacle" name_original="receptacles" src="d0_s3" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s3" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>paleae 3.1–4.1.</text>
      <biological_entity id="o855" name="palea" name_original="paleae" src="d0_s4" type="structure">
        <character char_type="range_value" from="3.1" name="quantity" src="d0_s4" to="4.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray laminae 20–45 × 7–12 mm.</text>
      <biological_entity constraint="ray" id="o856" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Discs 15–19 × 10–20 mm.</text>
      <biological_entity id="o857" name="disc" name_original="discs" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="19" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 3.5–4 mm;</text>
      <biological_entity id="o858" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi mostly 0.7+ mm.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 72, ca. 72, 102+.</text>
      <biological_entity id="o859" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o860" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="72" value_original="72" />
        <character name="quantity" src="d0_s9" value="72" value_original="72" />
        <character char_type="range_value" from="102" name="quantity" src="d0_s9" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet habitats, along streams and edges of woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet habitats" constraint="along streams and edges of woods" />
        <character name="habitat" value="streams" constraint="of woods" />
        <character name="habitat" value="edges" constraint="of woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., Del., Md., Mass., N.H., N.Y., Pa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7b.</number>
  <other_name type="common_name">Northeastern cutleaf coneflower</other_name>
  
</bio:treatment>