<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
    <other_info_on_meta type="illustration_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">amblyolepis</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">setigera</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 668. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus amblyolepis;species setigera</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220000552</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helenium</taxon_name>
    <taxon_name authority="(de Candolle) Britton &amp; Rusby" date="unknown" rank="species">setigerum</taxon_name>
    <taxon_hierarchy>genus Helenium;species setigerum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal and proximal spatulate to oblanceolate, bases narrowed;</text>
      <biological_entity id="o8455" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="position" src="d0_s0" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s0" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s0" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o8456" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>mid ovate to lanceolate, bases usually semiclasping;</text>
      <biological_entity id="o8457" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="mid" id="o8458" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o8459" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="fixation" src="d0_s1" value="semiclasping" value_original="semiclasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal leaves similar, smaller.</text>
      <biological_entity id="o8460" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o8461" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles (4–) 8–20 cm, expanded distally, sparsely pilose proximally, moderately to densely pilose distally.</text>
      <biological_entity id="o8462" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="moderately to densely; distally" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric to globose, 9–17 × 12–20 mm.</text>
      <biological_entity id="o8463" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s4" to="globose" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="17" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: outer usually 8, inner 9–13.</text>
      <biological_entity id="o8464" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o8465" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="8" value_original="8" />
      </biological_entity>
      <biological_entity constraint="inner" id="o8466" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray corollas 10–22 × 4.5–9.6 (–11.6) mm.</text>
      <biological_entity constraint="ray" id="o8467" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="9.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="11.6" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s6" to="9.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc corollas 5.1–7 mm.</text>
      <biological_entity constraint="disc" id="o8468" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.1" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae 3–4.5 mm;</text>
      <biological_entity id="o8469" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus-scales 2–3 (–4.1) mm, apices usually rounded to acute or acuminate, rarely cuspidate, not aristate.</text>
      <biological_entity id="o8470" name="pappus-scale" name_original="pappus-scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s9" to="4.1" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 34 (?), 36, 37 (18II + 1I), 38, 39 (19II + 1I), 39 (18II + 3I ?), 40.</text>
      <biological_entity id="o8471" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually rounded" name="shape" src="d0_s9" to="acute or acuminate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8472" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
        <character name="quantity" src="d0_s10" value="37" value_original="37" />
        <character name="quantity" src="d0_s10" value="38" value_original="38" />
        <character name="quantity" src="d0_s10" value="39" value_original="39" />
        <character name="quantity" src="d0_s10" value="39" value_original="39" />
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan–)Mar–May(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, open fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>