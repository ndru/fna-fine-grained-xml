<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="treatment_page">192</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="1840" rank="species">viscidiflorus</taxon_name>
    <taxon_name authority="L. C. Anderson" date="1964" rank="subspecies">planifolius</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>17: 223, fig. 2c. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species viscidiflorus;subspecies planifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–25 cm.</text>
      <biological_entity id="o22778" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous (puberulous).</text>
      <biological_entity id="o22779" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ascending to spreading, green, 1-nerved, narrowly lanceolate, (10–) 14–20 × 1–2 mm, flat, not twisted, margins obscurely involute, entire, not noticeably ciliate, apices cuspidate, faces glabrous.</text>
      <biological_entity id="o22780" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s2" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o22781" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape_or_vernation" src="d0_s2" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not noticeably" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22782" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o22783" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in broad, rounded, corymbiform arrays.</text>
      <biological_entity id="o22784" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o22785" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o22784" id="r2103" name="in" negation="false" src="d0_s3" to="o22785" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres cylindric to campanulate, (4–) 5–5.6 mm.</text>
      <biological_entity id="o22786" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s4" to="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="5.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 14–18 in 3–4 series, in spirals or weak vertical ranks, stramineous, green to brown subapical patches obscure midnerves evident distally or obscure, ± convex or weakly keeled, obovate to oblong or spatulate, unequal, margins scarious, eciliate or distally obscurely ciliolate, faces glabrous.</text>
      <biological_entity id="o22787" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o22788" from="14" name="quantity" src="d0_s5" to="18" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="brown" />
      </biological_entity>
      <biological_entity id="o22788" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o22789" name="spiral" name_original="spirals" src="d0_s5" type="structure" />
      <biological_entity id="o22790" name="rank" name_original="ranks" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="weak" value_original="weak" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o22791" name="patch" name_original="patches" src="d0_s5" type="structure" />
      <biological_entity id="o22792" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="distally" name="prominence" src="d0_s5" value="evident" value_original="evident" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="less convex or weakly keeled obovate" name="shape" src="d0_s5" to="oblong or spatulate" />
        <character char_type="range_value" from="less convex or weakly keeled obovate" name="shape" src="d0_s5" to="oblong or spatulate" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o22793" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" modifier="distally obscurely" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o22794" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o22787" id="r2104" name="in" negation="false" src="d0_s5" to="o22789" />
      <relation from="o22787" id="r2105" name="in" negation="false" src="d0_s5" to="o22790" />
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 3–4 (–5);</text>
      <biological_entity id="o22795" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas 3.5–4.5 mm, lobes 1.4–1.7 mm. 2n = 18.</text>
      <biological_entity id="o22796" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22797" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22798" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy soils, sparse juniper slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandy soils" />
        <character name="habitat" value="sparse juniper slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="" from="1600" from_unit="" constraint=" + m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9c.</number>
  
</bio:treatment>