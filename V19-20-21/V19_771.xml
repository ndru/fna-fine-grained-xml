<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
    <other_info_on_meta type="illustration_page">455</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">diaperia</taxon_name>
    <taxon_name authority="(Nuttall ex de Candolle) Nuttall" date="1840" rank="species">prolifera</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 338. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus diaperia;species prolifera</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220004004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Evax</taxon_name>
    <taxon_name authority="Nuttall ex de Candolle" date="unknown" rank="species">prolifera</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 459. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Evax;species prolifera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants grayish green to silvery, 3–15 cm, sericeous to lanuginose.</text>
      <biological_entity id="o4681" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s0" to="silvery" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s0" to="lanuginose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly 2–10;</text>
      <biological_entity id="o4682" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches proximal and distal (distal opposite or, sometimes, appearing alternate when unequal), rarely none.</text>
      <biological_entity id="o4683" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s2" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: largest 7–15 × 2–4 mm;</text>
      <biological_entity id="o4684" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>capitular leaves subtending glomerules, also visible between and surpassing heads.</text>
      <biological_entity id="o4685" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="capitular" id="o4686" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4687" name="glomerule" name_original="glomerules" src="d0_s4" type="structure" />
      <biological_entity id="o4688" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s4" value="visible-surpassing" value_original="visible-surpassing" />
      </biological_entity>
      <relation from="o4686" id="r465" name="subtending" negation="false" src="d0_s4" to="o4687" />
    </statement>
    <statement id="d0_s5">
      <text>Heads in strictly dichasiform or pseudo-polytomous arrays (sometimes appearing monochasiiform), cylindric to ± ellipsoid, 3.5–4.5 mm, heights 2–3 times diams.</text>
      <biological_entity id="o4689" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s5" to="more or less ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="height" src="d0_s5" value="2-3 times diams" value_original="2-3 times diams" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles broadly or narrowly conic, 0.4–0.6 mm or ± 0.9–1.1 mm, heights 0.5–0.7 or 2–2.4 times diams.</text>
      <biological_entity id="o4690" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character name="some_measurement" src="d0_s6" value="more or less" value_original="more or less" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate paleae imbricate, longest 2.5–4 mm.</text>
      <biological_entity id="o4691" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate paleae ± 3, apices erect to somewhat spreading, ± plane.</text>
      <biological_entity id="o4692" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o4693" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect to somewhat" value_original="erect to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Functionally staminate florets 2–4;</text>
      <biological_entity id="o4694" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovaries partly developed, 0.4–0.6 mm;</text>
      <biological_entity id="o4695" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="partly" name="development" src="d0_s10" value="developed" value_original="developed" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas hidden in heads, actinomorphic, 1.4–2 mm, glabrous, lobes equal.</text>
      <biological_entity id="o4696" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character constraint="in heads" constraintid="o4697" is_modifier="false" name="prominence" src="d0_s11" value="hidden" value_original="hidden" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="actinomorphic" value_original="actinomorphic" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4697" name="head" name_original="heads" src="d0_s11" type="structure" />
      <biological_entity id="o4698" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Bisexual florets 0.</text>
      <biological_entity id="o4699" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae ± angular, obcompressed, mostly 0.9–1.2 mm.</text>
      <biological_entity id="o4700" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s13" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obcompressed" value_original="obcompressed" />
        <character char_type="range_value" from="0.9" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Colo., Kans., La., Miss., Mo., Mont., N.Mex., Nebr., Okla., S.Dak., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Big-head rabbit-tobacco</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Intermediates between the two varieties of Diaperia prolifera occur where their ranges meet in central Texas and central Oklahoma. The strictly dichasiform or pseudo-polytomous branching pattern of D. prolifera is distinctive and diagnostic within the genus. Specimens of D. prolifera from introductions around a wool mill in South Carolina (G. L. Nesom 2004c, as Evax prolifera) are as yet undetermined to variety and are not included in the distributions below.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants grayish to greenish, loosely lanuginose; heads 4–40+ in largest glomerules; receptacle heights mostly 0.5–0.7 times diams.; capitular leaves usually ± spreading, scarcely involucral, not or scarcely carinate, pliant to somewhat rigid; distal branches mostly spreading to ascending; longest pistillate paleae 3.3–4 mm</description>
      <determination>3a Diaperia prolifera var. prolifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants silvery white, tightly sericeous; heads borne singly, or 2–3 in largest glomerules; receptacle heights mostly 2–2.4 times diams.; capitular leaves erect, involucral, proximally carinate, becoming indurate; distal branches strictly ascending to erect; longest pistillate paleae 2.5–3.2 mm</description>
      <determination>3b Diaperia prolifera var. barnebyi</determination>
    </key_statement>
  </key>
</bio:treatment>