<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="treatment_page">440</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">hymenoxys</taxon_name>
    <taxon_name authority="(A. Gray) K. F. Parker" date="1950" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>10: 159. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus hymenoxys;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066984</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actinella</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 96. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Actinella;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–70 cm (polycarpic, often with sparingly branched, woody caudices).</text>
      <biological_entity id="o25787" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, green throughout or purple-red-tinted distally to throughout, usually unbranched distally, ± hairy (often tomentose proximally).</text>
      <biological_entity id="o25788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" modifier="throughout; throughout; distally to throughout" name="coloration" src="d0_s1" value="green throughout or purple-red-tinted" value_original="green throughout or purple-red-tinted" />
        <character is_modifier="false" modifier="throughout; usually; distally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades usually simple, rarely lobed (lobes 3), glabrous or ± hairy, eglandular or sparsely glanddotted (basal leaf-bases ± long-villous-woolly);</text>
      <biological_entity id="o25789" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25790" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character name="architecture" src="d0_s2" value="sparsely" value_original="sparsely" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid leaves usually simple, rarely lobed (lobes 3, terminal lobes 1.5–3 mm wide).</text>
      <biological_entity id="o25791" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o25792" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–5 per plant, usually borne singly, sometimes in paniculiform arrays.</text>
      <biological_entity id="o25793" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o25794" from="1" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o25794" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <biological_entity id="o25795" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o25793" id="r1751" modifier="sometimes" name="in" negation="false" src="d0_s4" to="o25795" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles (1.5–) 6–20 (–29) cm, ± hairy, densely tomentose distally near involucres.</text>
      <biological_entity id="o25796" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="29" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character constraint="near involucres" constraintid="o25797" is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o25797" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric to broadly campanulate, 13–20 × 23–32 mm.</text>
      <biological_entity id="o25798" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="broadly campanulate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="width" src="d0_s6" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2 series, unequal;</text>
      <biological_entity id="o25799" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o25800" name="series" name_original="series" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o25799" id="r1752" name="in" negation="false" src="d0_s7" to="o25800" />
    </statement>
    <statement id="d0_s8">
      <text>outer 13–19, basally connate only slightly to 1/5 their lengths, lanceolate to narrowly lanceolate to obovate to oblanceolate, 7–11 mm, apices acuminate to acute;</text>
      <biological_entity constraint="outer" id="o25801" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s8" to="19" />
        <character is_modifier="false" modifier="basally; only slightly; slightly" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s8" to="1/5" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner 13–18, narrowly lanceolate to oblanceolate, 8.5–12.6 mm, apices aristate.</text>
      <biological_entity id="o25802" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="acute" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o25803" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s9" to="18" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s9" to="oblanceolate" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s9" to="12.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25804" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 13–15;</text>
      <biological_entity id="o25805" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 13–26 × 5.4–9.5 mm.</text>
      <biological_entity id="o25806" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s11" to="26" to_unit="mm" />
        <character char_type="range_value" from="5.4" from_unit="mm" name="width" src="d0_s11" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 100–250+;</text>
      <biological_entity id="o25807" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s12" to="250" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 5.7–7.4 mm.</text>
      <biological_entity id="o25808" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" src="d0_s13" to="7.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae narrowly obpyramidal, 4.2–4.7 mm;</text>
      <biological_entity id="o25809" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 9–11 (–15) obovate to oblanceolate, often aristate scales 4.7–7.3 mm. 2n = 30.</text>
      <biological_entity id="o25810" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o25811" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="15" />
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s15" to="11" />
        <character char_type="range_value" from="obovate" is_modifier="true" name="shape" src="d0_s15" to="oblanceolate" />
        <character is_modifier="true" modifier="often" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s15" to="7.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25812" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
      <relation from="o25810" id="r1753" name="consist_of" negation="false" src="d0_s15" to="o25811" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, edges of juniper-pine and pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="edges" constraint="of juniper-pine and pine forests" />
        <character name="habitat" value="juniper-pine" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Bigelow’s rubberweed</other_name>
  
</bio:treatment>