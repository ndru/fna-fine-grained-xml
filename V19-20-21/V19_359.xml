<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="illustration_page">263</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prenanthes</taxon_name>
    <taxon_name authority="(Hooker) D. Dietrich" date="1847" rank="species">alata</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>4: 1309. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus prenanthes;species alata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067365</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nabalus</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">alatus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 294, plate 102. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Nabalus;species alatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sonchus</taxon_name>
    <taxon_name authority="Lessing" date="unknown" rank="species">hastatus</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>6: 99. 1831,</place_in_publication>
      <other_info_on_pub>not Prenanthes hastata Thunberg 1784</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Sonchus;species hastatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prenanthes</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="species">lessingii</taxon_name>
    <taxon_hierarchy>genus Prenanthes;species lessingii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–80 cm;</text>
      <biological_entity id="o3409" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots slender to thickened, tuberous.</text>
      <biological_entity id="o3410" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="thickened" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (–10), erect or ascending, green to purple, usually simple, glabrous or glabrate proximally, tomentulose distally.</text>
      <biological_entity id="o3411" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="10" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="purple" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: proximal present at flowering;</text>
      <biological_entity id="o3412" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o3413" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles winged (2–6 cm);</text>
      <biological_entity id="o3414" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3415" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades triangular to irregularly elliptic, 3–25 × 1–7 cm, thin or slightly coriaceous, bases abruptly constricted, truncate to slightly hastate, margins irregularly dentate or coarsely serrate, apices acute to acuminate, faces glabrate;</text>
      <biological_entity id="o3416" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3417" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="irregularly elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="7" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o3418" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s5" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="slightly hastate" />
      </biological_entity>
      <biological_entity id="o3419" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="coarsely; coarsely" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o3420" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o3421" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, ovate to lanceolate, reduced.</text>
      <biological_entity id="o3422" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o3423" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (10–17) in broad, corymbiform arrays, lateral branches often elongate and overtopping main-stems.</text>
      <biological_entity id="o3424" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o3425" from="10" name="atypical_quantity" src="d0_s7" to="17" />
      </biological_entity>
      <biological_entity id="o3425" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3426" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o3427" name="main-stem" name_original="main-stems" src="d0_s7" type="structure" />
      <relation from="o3426" id="r337" name="overtopping" negation="false" src="d0_s7" to="o3427" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly campanulate, 10–13 × 5–6 mm.</text>
      <biological_entity id="o3428" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyculi of 2–3, dark green, lanceolate or subulate bractlets 1–3 mm, glabrous or finely tomentulose.</text>
      <biological_entity id="o3429" name="calyculus" name_original="calyculi" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="of 2-3" name="coloration" src="d0_s9" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o3430" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s9" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 8, green to dark green, lanceolate, 8–11 mm, margins scarious, apices acute, finely tomentulose.</text>
      <biological_entity id="o3431" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="dark green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3432" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3433" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s10" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 7–16;</text>
      <biological_entity id="o3434" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas white to purplish, 9–16 mm.</text>
      <biological_entity id="o3435" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="purplish" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae brown to light tan, subcylindric, 4–7 mm, weakly 7–10-ribbed;</text>
      <biological_entity id="o3436" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="light tan" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s13" value="7-10-ribbed" value_original="7-10-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi pale-yellow to dull white, 8–10 mm. 2n = 16.</text>
      <biological_entity id="o3437" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s14" to="dull white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3438" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, mountain springs, seeps, cliffs near shore, moist shady places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="mountain springs" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="cliffs" constraint="near shore" />
        <character name="habitat" value="shore" />
        <character name="habitat" value="shady places" modifier="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Western or wing-leaved rattlesnakeroot</other_name>
  <other_name type="common_name">white or western white lettuce</other_name>
  <discussion>Prenanthes alata is recognized by its relatively small size, elongate and winged petioles, triangular-hastate leaf blades, heads in broad corymbiform arrays, and dark green, finely tomentulose phyllaries.</discussion>
  
</bio:treatment>