<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">30</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Strother" date="2001" rank="genus">HEDOSYNE</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>47: 204. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus HEDOSYNE</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hedosyne, delight</other_info_on_name>
    <other_info_on_name type="fna_id">316917</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–50 (–100) cm.</text>
      <biological_entity id="o16955" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, strictly branched.</text>
      <biological_entity id="o16956" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strictly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o16957" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades deltate or ovate to lanceolate, 1–3-pinnately lobed (lobes oblong to lance-linear), ultimate margins entire or toothed, faces ± scabrellous and/or hispid, usually glanddotted.</text>
      <biological_entity id="o16958" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" modifier="1-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16959" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16960" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads ± disciform, usually in loose, (± bracteate or ebracteate) paniculiform arrays (sometimes 3–6+ distal to axil of each bract).</text>
      <biological_entity id="o16961" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o16962" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o16961" id="r1168" modifier="usually" name="in" negation="false" src="d0_s6" to="o16962" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric, 4–5 mm diam.</text>
      <biological_entity id="o16963" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 10–12+ in 2+ series, distinct, outer 5 herbaceous, inner scarious to membranous.</text>
      <biological_entity id="o16964" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o16965" from="10" name="quantity" src="d0_s8" to="12" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o16965" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o16966" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16967" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="scarious" name="texture" src="d0_s8" to="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles hemispheric;</text>
      <biological_entity id="o16968" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleae spatulate to linear, membranous, sparsely hairy or glabrate, usually glanddotted.</text>
      <biological_entity id="o16969" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s10" to="linear" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 5–10;</text>
      <biological_entity id="o16970" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 0.</text>
      <biological_entity id="o16971" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Functionally staminate florets 5–10+;</text>
      <biological_entity id="o16972" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s13" to="10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas whitish, funnelform, lobes 5, soon reflexed (filaments ± connate, anthers weakly coherent or distinct).</text>
      <biological_entity id="o16973" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o16974" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" modifier="soon" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae pyriform, ± obcompressed, finely striate, glabrous, little, if at all, glanddotted;</text>
      <biological_entity id="o16975" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" modifier="finely" name="coloration_or_pubescence_or_relief" src="d0_s15" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 18.</text>
      <biological_entity id="o16976" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o16977" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>253.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>