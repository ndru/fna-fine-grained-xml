<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">melampodiinae</taxon_name>
    <taxon_name authority="Schrank" date="1820" rank="genus">acanthospermum</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">hispidum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 522. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe melampodiinae;genus acanthospermum;species hispidum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242420070</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60+ cm.</text>
      <biological_entity id="o22034" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o22035" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades rhombic-ovate to obovate, (20–) 40–120 (–150+) mm, faces finely pilosulous, glanddotted.</text>
      <biological_entity id="o22036" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="rhombic-ovate" name="shape" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_distance" src="d0_s2" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="150" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="40" from_unit="mm" name="distance" src="d0_s2" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22037" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruits ± compressed, ± cuneate to obovate, 4–6+ mm, not notably ribbed, terminal spines 2, divergent, 3–4 mm, often 1 ± uncinate, prickles seldom notably uncinate, ± scattered.</text>
      <biological_entity id="o22038" name="fruit" name_original="fruits" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="less cuneate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="not notably" name="architecture_or_shape" src="d0_s3" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o22039" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character modifier="often" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="uncinate" value_original="uncinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>2n = 22.</text>
      <biological_entity id="o22040" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="seldom notably" name="shape" src="d0_s3" value="uncinate" value_original="uncinate" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22041" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round, mostly Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, often sandy sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sites" modifier="disturbed often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Ala., Fla., Ga., N.J., Oreg., S.C., Va.; South America; also introduced in Mexico, West Indies, Central America, Europe, Asia, Africa, Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also  in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>