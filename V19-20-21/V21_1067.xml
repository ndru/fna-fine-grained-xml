<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">428</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="(Rafinesque) H. Rock" date="1957" rank="species">amarum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>59: 131. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species amarum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416623</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">amara</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ludov.,</publication_title>
      <place_in_publication>69. 1817 (as Galardia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gaillardia;species amara;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helenium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tenuifolium</taxon_name>
    <taxon_hierarchy>genus Helenium;species tenuifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–100 cm.</text>
      <biological_entity id="o20134" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–15), usually branched distally, not winged, glabrous or sparsely hairy.</text>
      <biological_entity id="o20135" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="15" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves glabrous or sparsely hairy;</text>
      <biological_entity id="o20136" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades linear to ovate, entire or pinnately toothed or lobed to pinnatifid;</text>
      <biological_entity constraint="basal" id="o20137" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="ovate entire or pinnately toothed or lobed" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="ovate entire or pinnately toothed or lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal blades linear, entire.</text>
      <biological_entity constraint="mid and distal" id="o20138" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (1–) 10–150 (–250+) per plant, in paniculiform arrays.</text>
      <biological_entity id="o20139" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="10" to_inclusive="false" />
        <character char_type="range_value" from="150" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="250" upper_restricted="false" />
        <character char_type="range_value" constraint="per plant" constraintid="o20140" from="10" name="quantity" src="d0_s5" to="150" />
      </biological_entity>
      <biological_entity id="o20140" name="plant" name_original="plant" src="d0_s5" type="structure" />
      <biological_entity id="o20141" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o20139" id="r1374" name="in" negation="false" src="d0_s5" to="o20141" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3–11 cm, sparsely hairy.</text>
      <biological_entity id="o20142" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="11" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric to globose or globoid, 5–9 × 6–10 mm.</text>
      <biological_entity id="o20143" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="globose" />
        <character name="shape" src="d0_s7" value="globoid" value_original="globoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries moderately to densely hairy.</text>
      <biological_entity id="o20144" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 8–10, pistillate, fertile;</text>
      <biological_entity id="o20145" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 4.5–14 × 2–10 mm.</text>
      <biological_entity id="o20146" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 75–250+;</text>
      <biological_entity id="o20147" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s11" to="250" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow proximally, yellow to yellowbrown or purple distally, 1.6–2.7 × 0.8–1.2 mm, lobes 5.</text>
      <biological_entity id="o20148" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="yellow" modifier="distally" name="coloration" src="d0_s12" to="yellowbrown or purple" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s12" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20149" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 0.7–1.3 mm, moderately to densely hairy;</text>
      <biological_entity id="o20150" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 6–8 entire, aristate scales 1–1.8 mm.</text>
      <biological_entity id="o20151" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o20152" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s14" to="8" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
      <relation from="o20151" id="r1375" name="consist_of" negation="false" src="d0_s14" to="o20152" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Calif., D.C., Fla., Ga., Ind., Kans., La., Mich., Miss., Mo., N.C., Okla., Pa., S.C., Tenn., Tex., Va.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Yellowdicks</other_name>
  <other_name type="common_name">fiveleaf sneezeweed</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaves usually withered by flowering; basal blades usually entire or pinnately toothed, sometimes pinnatifid; proximal blades usually entire, sometimes pinnately toothed; disc corollas yellow to yellow-brown distally</description>
      <determination>1a Helenium amarum var. amarum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaves often present at, sometimes withered by, flowering; basal blades pinnatifid; proximal blades entire or pinnately toothed or lobed to pinnatifid; disc corollas purple distally</description>
      <determination>1b Helenium amarum var. badium</determination>
    </key_statement>
  </key>
</bio:treatment>