<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">190</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="section">gyrophyllum</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">palmata</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 180. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section gyrophyllum;species palmata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416307</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80 cm.</text>
      <biological_entity id="o18079" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Internodes (± mid-stem) 25–50 mm.</text>
      <biological_entity id="o18080" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s1" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 0–1 mm (or 5–25+ mm, winged, and scarcely distinct from blades);</text>
      <biological_entity id="o18081" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18082" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades not 3-foliolate, most with 3 (–5+), ± oblong to linear lobes (5–) 15–40+ × 2–3 (–7+) mm (sometimes some leaves not lobed).</text>
      <biological_entity id="o18083" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18084" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity id="o18085" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="5" upper_restricted="false" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="3" value_original="3" />
        <character char_type="range_value" from="less oblong" is_modifier="true" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s3" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="7" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o18084" id="r1241" name="with" negation="false" src="d0_s3" to="o18085" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–4+ cm.</text>
      <biological_entity id="o18086" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 9–12+ oblong to lanceolate bractlets 3–9+ mm.</text>
      <biological_entity id="o18087" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o18088" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s5" to="12" upper_restricted="false" />
        <character char_type="range_value" from="oblong" is_modifier="true" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o18087" id="r1242" name="consist_of" negation="false" src="d0_s5" to="o18088" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8, ± oblong to nearly orbiculate, 6–10 mm.</text>
      <biological_entity id="o18089" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="8" value_original="8" />
        <character char_type="range_value" from="less oblong" name="shape" src="d0_s6" to="nearly orbiculate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray laminae 15–25+ mm.</text>
      <biological_entity constraint="ray" id="o18090" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 60–80+;</text>
      <biological_entity id="o18091" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s8" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow (sometimes drying blackish), 5–6.5 mm.</text>
      <biological_entity id="o18092" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae oblong, 5–6 mm. 2n = 26.</text>
      <biological_entity id="o18093" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18094" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Ind., Iowa, Kans., La., Mich., Minn., Mo., Nebr., Okla., S.Dak., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  
</bio:treatment>