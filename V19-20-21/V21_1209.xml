<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">481</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AGERATUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 839. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 363. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus AGERATUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a, not, and geras, old age, apparently alluding to long-lasting nature of flowers</other_info_on_name>
    <other_info_on_name type="fna_id">100805</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals and perennials, mostly 20–120 cm.</text>
      <biological_entity id="o23115" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often decumbent (rooting at proximal nodes), sparsely to densely branched.</text>
      <biological_entity id="o23117" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="sparsely to densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>all or mostly opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o23118" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly 1-nerved, deltate to ovate, or elliptic to lanceolate, margins entire or toothed, faces glabrous or ± pilose, puberulent, or strigoso-hispid, sometimes glanddotted.</text>
      <biological_entity id="o23119" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="ovate or elliptic" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="ovate or elliptic" />
      </biological_entity>
      <biological_entity id="o23120" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o23121" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigoso-hispid" value_original="strigoso-hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigoso-hispid" value_original="strigoso-hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigoso-hispid" value_original="strigoso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in dense to open, cymiform to corymbiform arrays.</text>
      <biological_entity id="o23122" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character char_type="range_value" from="cymiform" modifier="in dense to open" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, 3–6 mm.</text>
      <biological_entity id="o23123" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 30–40 in 2–3 series, usually 2-nerved, lanceolate, ± equal (often indurate, margins scarious).</text>
      <biological_entity id="o23124" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o23125" from="30" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s8" value="2-nerved" value_original="2-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o23125" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic, epaleate [paleate].</text>
      <biological_entity id="o23126" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 20–125;</text>
      <biological_entity id="o23127" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="125" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white or bluish to lavender, throats ± campanulate (lengths 2 times diams.);</text>
      <biological_entity id="o23128" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="bluish" name="coloration" src="d0_s11" to="lavender" />
      </biological_entity>
      <biological_entity id="o23129" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases not enlarged, glabrous, branches ± linear to clavate (usually papillose and dilated distally).</text>
      <biological_entity id="o23130" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o23131" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23132" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="less linear" name="shape" src="d0_s12" to="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae prismatic, 4–5-ribbed, glabrous or sparsely strigoso-hispidulous;</text>
      <biological_entity id="o23133" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigoso-hispidulous" value_original="strigoso-hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of 5–6 aristate scales, or coroniform, or 0.</text>
      <biological_entity id="o23135" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s14" to="6" />
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
      </biological_entity>
      <relation from="o23134" id="r1584" name="consist_of" negation="false" src="d0_s14" to="o23135" />
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o23134" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="coroniform" value_original="coroniform" />
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o23136" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, Central America; 2 species widespread as adventives.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="2 species widespread as adventives" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>396.</number>
  <discussion>Species ca. 40 (4 in the flora).</discussion>
  <references>
    <reference>Johnson, M. F. 1971. A monograph of the genus Ageratum L. (Compositae–Eupatorieae). Ann. Missouri Bot. Gard. 58: 6–88.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants colonial; stems and leaves glabrous or glabrate</description>
      <determination>1 Ageratum maritimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants not colonial; stems and leaves hairy</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems puberulent to minutely strigoso-hispid; cypselae glabrous</description>
      <determination>2 Ageratum corymbosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems sparsely to densely pilose (usually in combination with other forms of vestiture); cypselae sparsely strigoso-hispidulous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles minutely puberulent and sparsely to densely pilose, eglandular; phyllaries oblong-lanceolate, abruptly tapering to subulate tips 0.5–1 mm, glabrous or sparsely pilose, margins often ciliate, abaxial faces eglandular</description>
      <determination>3 Ageratum conyzoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles mixed pilose, stipitate-glandular, and viscid-puberulent; phyllaries narrowly lanceolate, gradually tapering to indurate-subulate tips 0.8–2 mm, margins not ciliate or inconspicuously ciliate, abaxial faces stipitate-glandular and sparsely to densely pilose</description>
      <determination>4 Ageratum houstonianum</determination>
    </key_statement>
  </key>
</bio:treatment>