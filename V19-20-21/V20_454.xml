<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">astranthium</taxon_name>
    <taxon_name authority="(Shinners) De Jong" date="1965" rank="species">robustum</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Mus. Michigan State Univ., Biol. Ser.</publication_title>
      <place_in_publication>2: 521. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus astranthium;species robustum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066174</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Astranthium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">integrifolium</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="variety">robustum</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>18: 158. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Astranthium;species integrifolium;variety robustum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, taprooted.</text>
      <biological_entity id="o28633" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 3–8, decumbent-ascending, sometimes 1, erect.</text>
      <biological_entity id="o28634" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent-ascending" value_original="decumbent-ascending" />
        <character modifier="sometimes" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline 20–45 × 5–11 mm.</text>
      <biological_entity id="o28635" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 4–6.8 mm.</text>
      <biological_entity id="o28636" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 18–35;</text>
      <biological_entity id="o28637" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s4" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla laminae white or abaxially lavender, often drying pinkish, 8–13 mm.</text>
      <biological_entity constraint="corolla" id="o28638" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s5" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="often" name="condition" src="d0_s5" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-floret corollas 3.2–3.8 mm.</text>
      <biological_entity constraint="disc-floret" id="o28639" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1.4–2.1 × 0.7–1.2 mm, faces minutely, glandular-papillate-pebbly, uniformly sparsely to moderately glochidiate-hairy.</text>
      <biological_entity id="o28640" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s7" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 6.</text>
      <biological_entity id="o28641" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="glandular-papillate-pebbly" value_original="glandular-papillate-pebbly" />
        <character is_modifier="false" modifier="uniformly sparsely; sparsely to moderately" name="pubescence" src="d0_s7" value="glochidiate-hairy" value_original="glochidiate-hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28642" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites in sandy or rocky soil, mesas, canyons, pine flats, roadsides, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" constraint="in sandy or rocky soil , mesas , canyons ," />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky soil" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="pine flats" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Texas western-daisy</other_name>
  <discussion>Astranthium robustum is known only from the trans-Pecos region.</discussion>
  
</bio:treatment>