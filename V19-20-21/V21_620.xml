<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="subtribe">flaveriinae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">flaveria</taxon_name>
    <taxon_name authority="M. E. Theroux" date="1977" rank="species">mcdougallii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>24: 13, fig. 1. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe flaveriinae;genus flaveria;species mcdougallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066776</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 50 cm (glabrous).</text>
      <biological_entity id="o3290" name="whole-organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s0" unit="cm" value="50" value_original="50" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o3291" name="whole_organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s0" unit="cm" value="50" value_original="50" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o3292" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o3293" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear to narrowly linear-lanceolate, 50–110 × 2–7 mm, bases weakly connate, margins entire.</text>
      <biological_entity id="o3294" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly linear-lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="110" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3295" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o3296" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 50–200+, in clusters in compound, corymbiform or paniculiform arrays.</text>
      <biological_entity id="o3297" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s4" to="200" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3298" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o3297" id="r254" modifier="in clusters" name="in" negation="false" src="d0_s4" to="o3298" />
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 0–1 linear bractlets 1–3 mm.</text>
      <biological_entity id="o3299" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o3300" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s5" to="1" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o3299" id="r255" name="consist_of" negation="false" src="d0_s5" to="o3300" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate, 3 × 0.7 mm.</text>
      <biological_entity id="o3301" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character name="length" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character name="width" src="d0_s6" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 2–6, oblong.</text>
      <biological_entity id="o3302" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o3303" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 2–6 (exserted conspicuously from involucres);</text>
      <biological_entity id="o3304" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla-tubes 1 mm, throats funnelform, 1.5 mm.</text>
      <biological_entity id="o3305" name="corolla-tube" name_original="corolla-tubes" src="d0_s10" type="structure">
        <character name="distance" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o3306" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae linear, 1.5 mm;</text>
      <biological_entity id="o3307" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi coroniform (fringed scales), ca. 0.3 mm. 2n = 36.</text>
      <biological_entity id="o3308" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="coroniform" value_original="coroniform" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3309" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Jan.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jan" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline springs and seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline springs" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Mcdougall’s yellowtops</other_name>
  <discussion>Flaveria mcdougallii is known only from four locations near shaded alkaline seeps and springs close to the Colorado River in the Grand Canyon of northern Arizona. Delimiting characteristics of F. mcdougallii include linear leaves with entire margins, pappi of crowns of scales, and the distribution. Morphologic character differences and experimental hybridization studies suggest that F. mcdougallii could well be assigned to a separate, monotypic genus.</discussion>
  
</bio:treatment>