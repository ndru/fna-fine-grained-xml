<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="treatment_page">219</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Nuttall) Elliott" date="1823" rank="genus">chrysopsis</taxon_name>
    <taxon_name authority="Semple" date="1978" rank="species">linearifolia</taxon_name>
    <taxon_name authority="Semple" date="1978" rank="subspecies">dressii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 494. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysopsis;species linearifolia;subspecies dressii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068153</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems green to dark purple, 30–80 cm.</text>
      <biological_entity id="o21213" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dark purple" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves less numerous (to 100 on tall plants), linear to linear-lanceolate or elliptic, sometimes very undulate, strongly twisted.</text>
      <biological_entity id="o21214" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="less" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="linear-lanceolate or elliptic" />
        <character is_modifier="false" modifier="sometimes very" name="shape" src="d0_s1" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads 4–30 (–50) in open corymbiform arrays.</text>
      <biological_entity id="o21215" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="50" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21216" from="4" name="quantity" src="d0_s2" to="30" />
      </biological_entity>
      <biological_entity id="o21216" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy areas in oak pine woods, fields, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" modifier="open" constraint="in oak pine woods , fields , roadsides" />
        <character name="habitat" value="oak pine woods" modifier="in" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <other_name type="common_name">Dress’s goldenaster</other_name>
  <discussion>Subspecies dressii is known from the central half and west of the peninsula. The report of the subspecies from southern Bay County (J. C. Semple 1981) was based on a few plants that were subsequently determined to be a hybrid swarm involving subsp. linearifolia and another species. This subspecies might deserve species status and more investigation is warranted. Plants of subsp. dressii could be confused with sparsely hairy forms of Chrysopsis subulata, which differ in having twisted, subulate phyllaries.</discussion>
  
</bio:treatment>