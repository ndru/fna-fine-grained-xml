<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="treatment_page">167</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="de Candolle" date="1810" rank="genus">saussurea</taxon_name>
    <taxon_name authority="Ledebour" date="1829" rank="species">nuda</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Pl.</publication_title>
      <place_in_publication>1: 15, plate 61. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus saussurea;species nuda</taxon_hierarchy>
    <other_info_on_name type="fna_id">242346848</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saussurea</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="unknown" rank="species">alpina</taxon_name>
    <taxon_hierarchy>genus Saussurea;species alpina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saussurea</taxon_name>
    <taxon_name authority="(Hooker) Rydberg" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">ledebourii</taxon_name>
    <taxon_hierarchy>genus Saussurea;species alpina;variety ledebourii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saussurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">densa</taxon_name>
    <taxon_hierarchy>genus Saussurea;species densa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saussurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nuda</taxon_name>
    <taxon_name authority="(Hooker) G. W. Douglas" date="unknown" rank="subspecies">densa</taxon_name>
    <taxon_hierarchy>genus Saussurea;species nuda;subspecies densa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saussurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nuda</taxon_name>
    <taxon_name authority="(Hooker) Hultén" date="unknown" rank="variety">densa</taxon_name>
    <taxon_hierarchy>genus Saussurea;species nuda;variety densa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–60 cm;</text>
      <biological_entity id="o19634" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branched caudices;</text>
      <biological_entity id="o19635" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>herbage subglabrous to loosely tomentose when young, ± glabrescent, at least proximally.</text>
      <biological_entity id="o19636" name="herbage" name_original="herbage" src="d0_s2" type="structure">
        <character char_type="range_value" from="subglabrous" modifier="when young" name="pubescence" src="d0_s2" to="loosely tomentose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems usually simple.</text>
      <biological_entity id="o19637" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline ± smaller distally, tapered to winged petioles to 7 cm, blades elliptic or lanceolate to ovate, 5–15 cm, bases obtuse to acute, margins subentire to sinuate, dentate or denticulate, apices acute to acuminate;</text>
      <biological_entity id="o19638" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="more or less; distally" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o19639" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19640" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19641" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o19642" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="sinuate dentate or denticulate" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="sinuate dentate or denticulate" />
      </biological_entity>
      <biological_entity id="o19643" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cauline sessile, ± decurrent.</text>
      <biological_entity constraint="distal cauline" id="o19644" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–20+ in corymbiform to subcapitate arrays;</text>
      <biological_entity id="o19646" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(peduncles 0–5 cm).</text>
      <biological_entity id="o19645" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o19646" from="3" name="quantity" src="d0_s6" to="20" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 10–15 mm.</text>
      <biological_entity id="o19647" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–4 series subequal or weakly imbricate, linear to lanceolate, abaxial faces dark green, often tinged dark purplish, loosely villous or ± tomentose.</text>
      <biological_entity id="o19648" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o19649" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19650" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="tinged dark" value_original="tinged dark" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <relation from="o19648" id="r1778" name="in" negation="false" src="d0_s9" to="o19649" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles naked.</text>
      <biological_entity id="o19651" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 15–20;</text>
      <biological_entity id="o19652" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas purple, 8–11 mm, tubes 4–6 mm, throats 1.5–2 mm, lobes 3–3.5 mm;</text>
      <biological_entity id="o19653" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19654" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19655" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19656" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers dark purple.</text>
      <biological_entity id="o19657" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae stramineous, 6–7 mm;</text>
      <biological_entity id="o19658" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of white to brownish, outer bristles 1–3 mm, inner 9–10 mm. 2n = 26 (as S. densa).</text>
      <biological_entity id="o19659" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity constraint="outer" id="o19660" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s15" to="brownish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19661" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19662" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
      </biological_entity>
      <relation from="o19659" id="r1779" name="consists_of" negation="false" src="d0_s15" to="o19660" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, estuaries, alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="estuaries" />
        <character name="habitat" value="tundra" modifier="alpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100, 2000–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="" from="0" from_unit="" />
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; B.C.; Alaska, Mont.; Russian Far East.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" value="Russian Far East" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Chaffless or dwarf saw-wort</other_name>
  <discussion>North American populations of Saussurea nuda occur in two distinctly different sets of habitats. Plants from coastal Alaska have been recognized as var. nuda and dwarfed alpine plants from the northern Rockies as var. densa. According to E. Hultén (1941–1950, vol. 10), the only differences between var. densa and var. nuda are the “more densely denticulated leaves and very congested inflorescences” of the former. Notwithstanding the very different habitats, some coastal Alaskan specimens [e.g., Ward 53 (ALA)] are indistinguishable from the alpine forms. In Siberia, S. nuda is a polymorphic species occurring from coastal sites to alpine areas (S. J. Lipschitz 1979).</discussion>
  <discussion>Saussurea ×tschuktschorum Lipschitz is apparently a hybrid between S. angustifolia and S. nuda. It resembles S. angustifolia; it has naked receptacles like S. nuda.</discussion>
  
</bio:treatment>