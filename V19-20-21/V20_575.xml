<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">282</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="S. L. Welsh" date="1983" rank="species">canaani</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>43: 366. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species canaani</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066567</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="species">higginsii</taxon_name>
    <taxon_hierarchy>genus Erigeron;species higginsii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (5–) 10–25 (–30) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices simple or branched.</text>
      <biological_entity id="o29688" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o29689" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to decumbent or nearly prostrate, sparsely strigose to glabrate, eglandular.</text>
      <biological_entity id="o29690" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="nearly" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s2" to="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent, old leaf-bases persistent, chaffy, fibrous) and cauline;</text>
      <biological_entity id="o29691" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades linear to narrowly linear-oblanceolate, 50–150 × 1–2 (–3) mm, cauline gradually or abruptly reduced distally, margins entire, faces glabrate to sparsely strigose, eglandular.</text>
      <biological_entity constraint="basal" id="o29692" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly linear-oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o29693" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o29694" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o29695" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s4" to="sparsely strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–3).</text>
      <biological_entity id="o29696" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–7 × 10–13 mm.</text>
      <biological_entity id="o29697" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series, hirsute, minutely glandular.</text>
      <biological_entity id="o29698" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o29699" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o29698" id="r2742" name="in" negation="false" src="d0_s7" to="o29699" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 15–22;</text>
      <biological_entity id="o29700" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to purplish, 3.5–6 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o29701" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="purplish" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29702" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.5–5 mm.</text>
      <biological_entity constraint="disc" id="o29703" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.8–2.2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o29704" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o29705" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of ca. 20 bristles.</text>
      <biological_entity id="o29706" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o29707" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o29708" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
      <relation from="o29706" id="r2743" name="outer of" negation="false" src="d0_s12" to="o29707" />
      <relation from="o29706" id="r2744" name="inner of" negation="false" src="d0_s12" to="o29708" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of sandstone cliffs and in sandy soil, ponderosa pine, Gambel oak, maple, Douglas fir, mixed conifers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of sandstone cliffs" />
        <character name="habitat" value="sandstone cliffs" />
        <character name="habitat" value="sandy soil" modifier="and in" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="gambel oak" />
        <character name="habitat" value="maple" />
        <character name="habitat" value="douglas fir" />
        <character name="habitat" value="mixed conifers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Abajo fleabane</other_name>
  <discussion>Erigeron higginsii may prove to be a distinct entity. It was described as differing from E. canaani in having smaller involucres (4.5–5.5 × 7.5–11 mm) with fewer rays (7–12); it occurs at 2900–3120 m in the Pine Valley Mountains of southwestern Utah.</discussion>
  
</bio:treatment>