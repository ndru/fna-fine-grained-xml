<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Nuttall) R. Chan" date="2002" rank="section">amphiachaenia</taxon_name>
    <taxon_name authority="(A. Gray) Ornduff" date="1966" rank="species">leptalea</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>40: 63. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section amphiachaenia;species leptalea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067053</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Burrielia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">leptalea</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 546. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Burrielia;species leptalea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">leptalea</taxon_name>
    <taxon_hierarchy>genus Baeria;species leptalea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 15 cm.</text>
      <biological_entity id="o2590" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (peduncles sometimes sinuous), branched distally, glabrous proximally, villous distally.</text>
      <biological_entity id="o2591" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear, 3–20 × 0.5–1 mm, (± fleshy) margins entire, faces sparsely hairy.</text>
      <biological_entity id="o2592" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2593" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2594" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres obconic to campanulate, 4–6 mm.</text>
      <biological_entity id="o2595" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s3" to="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries (± persistent) usually 4–6 (in 1 series), elliptic to ovate, glabrous but for hairy apices.</text>
      <biological_entity id="o2596" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character constraint="but-for apices" constraintid="o2597" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2597" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles subulate, papillate, glabrous.</text>
      <biological_entity id="o2598" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 6–9;</text>
      <biological_entity id="o2599" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla laminae broadly elliptic, 2.5–5 mm.</text>
      <biological_entity constraint="corolla" id="o2600" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages subulate.</text>
      <biological_entity constraint="anther" id="o2601" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae gray, narrowly clavate, to 2 mm, sparsely hairy;</text>
      <biological_entity id="o2602" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi usually of 1–4 translucent, white to yellowish, subulate, aristate scales (sometimes 0 in some florets within heads).</text>
      <biological_entity id="o2603" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" modifier="of 1-4 translucent" name="coloration" src="d0_s10" to="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 16.</text>
      <biological_entity id="o2604" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2605" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas of oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" constraint="of oak woodlands" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Salinas Valley goldfields</other_name>
  <discussion>Lasthenia leptalea grows in southern Monterey and northern San Luis Obispo counties. Originally assigned by R. Ornduff (1966b) to sect. Burrielia, L. leptalea is morphologically similar to L. gracilis, from which it can be distinguished by its subulate anther appendages and phyllaries that are hairy only at their tips.</discussion>
  
</bio:treatment>