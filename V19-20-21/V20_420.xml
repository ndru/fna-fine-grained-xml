<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="treatment_page">192</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="1840" rank="species">viscidiflorus</taxon_name>
    <taxon_name authority="(Nuttall) H. M. Hall &amp; Clements" date="1923" rank="subspecies">lanceolatus</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash</publication_title>
      <place_in_publication>326: 181. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species viscidiflorus;subspecies lanceolatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068156</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">lanceolatus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 324. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species lanceolatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">viscidiflorus</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">elegans</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species viscidiflorus;subspecies elegans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">viscidiflorus</taxon_name>
    <taxon_name authority="(Nuttall) Greene" date="unknown" rank="variety">lanceolatus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species viscidiflorus;variety lanceolatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–50 cm.</text>
      <biological_entity id="o21428" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems greenish, hirtellous to puberulent.</text>
      <biological_entity id="o21429" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s1" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades (ascending to spreading, lowermost sometimes deflexed) bright green (especially adaxially), 3-nerved or 5-nerved, linear to lanceolate, 15–45 × 2–6 mm, usually not twisted or with a single twist, margins flat to ± undulate, eciliate or ciliolate, apices abruptly acute, abaxial faces usually hirsute to hirtellous, rarely glabrous, adaxial usually glabrous.</text>
      <biological_entity id="o21430" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-nerved" value_original="5-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with a single twist" value_original="with a single twist" />
      </biological_entity>
      <biological_entity id="o21431" name="twist" name_original="twist" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o21432" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="more or less undulate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o21433" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21434" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually hirsute" name="pubescence" src="d0_s2" to="hirtellous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21435" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21430" id="r1975" name="with" negation="false" src="d0_s2" to="o21431" />
    </statement>
    <statement id="d0_s3">
      <text>Heads in small, compact, cymiform arrays.</text>
      <biological_entity id="o21436" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o21437" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="compact" value_original="compact" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o21436" id="r1976" name="in" negation="false" src="d0_s3" to="o21437" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres turbinate, 5–6.5 mm.</text>
      <biological_entity id="o21438" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 14–18 in 3–4 series, in spirals or vertical ranks, green subapical patches often lacking, midnerves evident distally or throughout, convex or ± keeled, oblong, unequal, margins scarious, eciliate or ciliolate, apices often rounded, apiculate, faces puberulent.</text>
      <biological_entity id="o21439" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o21440" from="14" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
      <biological_entity id="o21440" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o21441" name="spiral" name_original="spirals" src="d0_s5" type="structure" />
      <biological_entity id="o21442" name="rank" name_original="ranks" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o21443" name="patch" name_original="patches" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o21444" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="prominence" src="d0_s5" value="evident" value_original="evident" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21445" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o21446" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o21447" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o21439" id="r1977" name="in" negation="false" src="d0_s5" to="o21441" />
      <relation from="o21439" id="r1978" name="in" negation="false" src="d0_s5" to="o21442" />
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 5;</text>
      <biological_entity id="o21448" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas 5.5–6 mm, lobes 1–1.5 mm. 2n = 18, 36.</text>
      <biological_entity id="o21449" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21450" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21451" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Juniper/sagebrush savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="juniper\/sagebrush savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <discussion>Subspecies lanceolatus is known in South Dakota only from Pennington County.</discussion>
  
</bio:treatment>