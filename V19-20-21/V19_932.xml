<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">536</other_info_on_meta>
    <other_info_on_meta type="illustration_page">535</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="(Tzvelev) Tzvelev" date="1985" rank="genus">arctanthemum</taxon_name>
    <taxon_name authority="(Linnaeus) Tzvelev in A. I. Tolmatchew" date="1987" rank="species">arcticum</taxon_name>
    <place_of_publication>
      <publication_title>in A. I. Tolmatchew, Fl. Arct. URSS</publication_title>
      <place_in_publication>10: 115. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus arctanthemum;species arcticum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220001032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysanthemum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arcticum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 889. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysanthemum;species arcticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dendranthema</taxon_name>
    <taxon_name authority="(Linnaeus) Tzvelev" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus Dendranthema;species arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucanthemum</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="unknown" rank="species">arcticum</taxon_name>
    <taxon_hierarchy>genus Leucanthemum;species arcticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, forming tufts of 1–10+ rosettes;</text>
      <biological_entity id="o13013" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o13014" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o13015" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="10" upper_restricted="false" />
      </biological_entity>
      <relation from="o13013" id="r1185" name="forming" negation="false" src="d0_s0" to="o13014" />
      <relation from="o13013" id="r1186" name="consist_of" negation="false" src="d0_s0" to="o13015" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes at or below-ground, fleshy, tough, branched;</text>
      <biological_entity id="o13016" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="location" src="d0_s1" value="below-ground" value_original="below-ground" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="tough" value_original="tough" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>taproots (young plants) 3–6 (–10) mm diam.</text>
      <biological_entity id="o13017" name="taproot" name_original="taproots" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems glabrous or sparsely woolly proximally to ± densely white-woolly near heads.</text>
      <biological_entity id="o13018" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="near heads" constraintid="o13019" from="sparsely woolly" name="pubescence" src="d0_s3" to="proximally more or less densely white-woolly" />
      </biological_entity>
      <biological_entity id="o13019" name="head" name_original="heads" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves sometimes glaucous;</text>
      <biological_entity id="o13020" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petioles 0–95 mm;</text>
      <biological_entity id="o13021" name="petiole" name_original="petioles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="95" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bases cuneate to truncate, sheathing to clasping (distal), margins revolute, teeth obtuse to rounded, sometimes mucronulate, faces glabrous or sparsely woolly, glabrescent;</text>
      <biological_entity id="o13022" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o13023" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="cuneate to truncate" value_original="cuneate to truncate" />
        <character char_type="range_value" from="sheathing" is_modifier="true" name="architecture" src="d0_s6" to="clasping" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o13024" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="cuneate to truncate" value_original="cuneate to truncate" />
        <character char_type="range_value" from="sheathing" is_modifier="true" name="architecture" src="d0_s6" to="clasping" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o13025" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="cuneate to truncate" value_original="cuneate to truncate" />
        <character char_type="range_value" from="sheathing" is_modifier="true" name="architecture" src="d0_s6" to="clasping" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
      </biological_entity>
      <relation from="o13022" id="r1187" name="cuneate to truncate" negation="false" src="d0_s6" to="o13023" />
      <relation from="o13022" id="r1188" name="cuneate to truncate" negation="false" src="d0_s6" to="o13024" />
      <relation from="o13022" id="r1189" name="cuneate to truncate" negation="false" src="d0_s6" to="o13025" />
    </statement>
    <statement id="d0_s7">
      <text>basal and proximal cauline blades ± fan-shaped to cuneate or spatulate, 6–50 × 4–35 mm, lobes usually 3–7, apices blunt;</text>
      <biological_entity constraint="basal and proximal cauline" id="o13026" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="less fan-shaped" name="shape" src="d0_s7" to="cuneate or spatulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13027" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <biological_entity id="o13028" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal blades linear, 4–35 × 1–28 (–40) mm, bases attenuate, margins crenate, dentate, or entire, apices acute or obtuse;</text>
      <biological_entity constraint="distal" id="o13029" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13030" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o13031" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13032" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>faces ± woolly, glabrescent.</text>
      <biological_entity id="o13033" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles usually bracteate, bracts 0–4, linear-lanceolate to linear, 6–19 mm.</text>
      <biological_entity id="o13034" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o13035" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="4" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s10" to="linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray laminae 8–12 mm.</text>
      <biological_entity constraint="ray" id="o13036" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc corollas: tubes greenish yellow, (1.1–) 1.4–2 mm, throats and lobes yellow turning brownish, throats 0.7–1 mm, sometimes very sparsely glandular, lobes (0.25–) 0.4–0.6 mm.</text>
      <biological_entity constraint="disc" id="o13037" name="corolla" name_original="corollas" src="d0_s12" type="structure" />
      <biological_entity id="o13038" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish yellow" value_original="greenish yellow" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13039" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow turning brownish" value_original="yellow turning brownish" />
      </biological_entity>
      <biological_entity id="o13040" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow turning brownish" value_original="yellow turning brownish" />
      </biological_entity>
      <biological_entity id="o13041" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes very sparsely" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o13042" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 1.5–2.5 mm. 2n = 18 (72?).</text>
      <biological_entity id="o13043" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13044" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
        <character name="quantity" src="d0_s13" value="[72" value_original="[72" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.W.T., Nunavut, Ont., Que., Yukon; Alaska; Coastal Alaska and low-arctic Canada, arctic Eurasia, ne Asia (s to Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Coastal Alaska and low-arctic Canada" establishment_means="native" />
        <character name="distribution" value="arctic Eurasia" establishment_means="native" />
        <character name="distribution" value="ne Asia (s to Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>When Hultén described Chrysanthemum arcticum subsp. polare, he mapped the two subspecies. Along the Bering Strait in Alaska, all the material appears to belong to subsp. polare, except at Norton Bay, where a specimen (ALA) could be attributed to subsp. arcticum. That population would be very disjunct from the range of the subspecies along the south coast of Alaska. The two subspecies do not overlap widely in North America.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 10–40 cm (more in fruit); stems sometimes branched; blades of basal leaves fan-shaped to cuneate or spatulate, 3–5(–7)-lobed; ray laminae (15–)17–25(–31) mm, veins (5–)8–10(–12)</description>
      <determination>1a Arctanthemum arcticum subsp. arcticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants (2.5–)5–20(–26) cm (more in fruit); stems not branched; blades of basal leaves cuneate to spatulate, 0–3-lobed; ray laminae (7–)9–18(–21) mm, veins 4–5(–7)</description>
      <determination>1b Arctanthemum arcticum subsp. polare</determination>
    </key_statement>
  </key>
</bio:treatment>