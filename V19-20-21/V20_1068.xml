<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="treatment_page">463</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">psilactis</taxon_name>
    <taxon_name authority="Schultz-Bipontinus ex Hemsley" date="1879" rank="species">brevilingulata</taxon_name>
    <place_of_publication>
      <publication_title>Diagn. Pl. Nov. Mexic.</publication_title>
      <place_in_publication>2: 34. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus psilactis;species brevilingulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067400</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Schultz-Bipontinus ex Hemsley) McVaugh" date="unknown" rank="species">brevilingulatus</taxon_name>
    <taxon_hierarchy>genus Aster;species brevilingulatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(Schultz-Bipontinus ex Hemsley) B. L. Turner &amp; D. B. Horne" date="unknown" rank="species">brevilingulata</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species brevilingulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 15–75 cm.</text>
      <biological_entity id="o2427" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems and branches stipitate-glandular, more densely distally, often also with appressed or erect hairs.</text>
      <biological_entity id="o2428" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o2429" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o2430" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o2428" id="r228" modifier="densely distally; distally; often" name="with" negation="false" src="d0_s1" to="o2430" />
      <relation from="o2429" id="r229" modifier="densely distally; distally; often" name="with" negation="false" src="d0_s1" to="o2430" />
    </statement>
    <statement id="d0_s2">
      <text>Distal leaf-blades sessile, lanceolate to linear-lanceolate, smallest 2–10 × 0.5–1.5 mm.</text>
      <biological_entity constraint="distal" id="o2431" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear-lanceolate" />
        <character is_modifier="false" name="size" src="d0_s2" value="smallest" value_original="smallest" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres broadly turbinate to hemispheric, 2–4 mm.</text>
      <biological_entity id="o2432" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly turbinate" name="shape" src="d0_s3" to="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries lanceolate to linear, slightly unequal, 1.5–3.5 × 0.2–0.6 mm, bases indurate, margins scarious.</text>
      <biological_entity id="o2433" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s4" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2434" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o2435" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles flat to convex, 1.5–2.5 mm diam.</text>
      <biological_entity id="o2436" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="convex" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 15–40;</text>
      <biological_entity id="o2437" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 1–4 × 0.2–0.5 mm.</text>
      <biological_entity id="o2438" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 15–35;</text>
      <biological_entity id="o2439" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 1.5–2.5 mm.</text>
      <biological_entity id="o2440" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray cypselae 1–1.5 mm, moderately appressed-hairy.</text>
      <biological_entity constraint="ray" id="o2441" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s10" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc cypselae 1.3–2 mm, moderately appressed-hairy;</text>
      <biological_entity constraint="disc" id="o2442" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 20–25, 2–2.5 mm. 2n = 18 (Mexico).</text>
      <biological_entity id="o2443" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" unit=",2-2.5 mm" />
        <character char_type="range_value" from="20" name="some_measurement" src="d0_s12" to="25" unit=",2-2.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2444" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, roadsides, stream banks, open areas in oak and pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="open areas" constraint="in oak and pine-oak woodlands" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico; South America (Colombia, Peru).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In the flora area, Psilactis brevilingulata occurs in the Chihuahuan Desert regions of southeastern Arizona, southwestern New Mexico, and western Texas.</discussion>
  
</bio:treatment>