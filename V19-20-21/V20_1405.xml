<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">626</other_info_on_meta>
    <other_info_on_meta type="illustration_page">626</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Shuttleworth ex Chapman" date="1860" rank="genus">rugelia</taxon_name>
    <taxon_name authority="Shuttleworth ex Chapman" date="1860" rank="species">nudicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>246. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus rugelia;species nudicaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220011780</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">rugelia</taxon_name>
    <taxon_hierarchy>genus Senecio;species rugelia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cacalia</taxon_name>
    <taxon_name authority="(A. Gray) T. M. Barkley &amp; Cronquist" date="unknown" rank="species">rugelia</taxon_name>
    <taxon_hierarchy>genus Cacalia;species rugelia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 5–15+ × 3–10+ cm, marginal teeth 17–22 on each side.</text>
      <biological_entity id="o25045" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s0" to="10" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o25046" name="tooth" name_original="teeth" src="d0_s0" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o25047" from="17" name="quantity" src="d0_s0" to="22" />
      </biological_entity>
      <biological_entity id="o25047" name="side" name_original="side" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries 10–13 (–14) mm.</text>
      <biological_entity id="o25048" name="phyllarie" name_original="phyllaries" src="d0_s1" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="14" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Corollas 8–10 mm.</text>
      <biological_entity id="o25049" name="corolla" name_original="corollas" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cypselae 4–6 mm;</text>
      <biological_entity id="o25050" name="cypsela" name_original="cypselae" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pappi 8–10+ mm.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 56.</text>
      <biological_entity id="o25051" name="pappus" name_original="pappi" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25052" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forests, edges of openings in spruce-fir and hardwood regions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" />
        <character name="habitat" value="edges" constraint="of openings in spruce-fir and hardwood regions" />
        <character name="habitat" value="openings" constraint="in spruce-fir and hardwood regions" />
        <character name="habitat" value="spruce-fir" />
        <character name="habitat" value="hardwood regions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Rugelia nudicaulis is known mostly from the Great Smoky Mountains National Park on the North Carolina–Tennessee border.</discussion>
  
</bio:treatment>