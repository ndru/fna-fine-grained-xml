<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="B. L. Turner" date="1989" rank="species">carrizoanus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>67: 294, fig. 1. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species carrizoanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066974</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials (perhaps flowering first-year), 45–150 cm.</text>
      <biological_entity id="o7154" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 2-pinnate, (3–) 8–12 cm, 2-pinnate, lobes 4–10 × 0.5–1 mm;</text>
      <biological_entity id="o7155" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o7156" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
      </biological_entity>
      <biological_entity id="o7157" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 12–25+.</text>
      <biological_entity id="o7158" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o7159" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s2" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 45–60 per stem.</text>
      <biological_entity id="o7160" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o7161" from="45" name="quantity" src="d0_s3" to="60" />
      </biological_entity>
      <biological_entity id="o7161" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (1–) 2–5 cm.</text>
      <biological_entity id="o7162" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries gray-green, (3.5–) 5–6 × (1.5–) 2.5–3.5 mm.</text>
      <biological_entity id="o7163" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s5" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 0.</text>
      <biological_entity id="o7164" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o7165" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas whitish (drying pinkish), 3–4 mm, tubes 1.5–2 mm, throats funnelform, 1–1.5 mm, lengths 1–2 times lobes.</text>
      <biological_entity id="o7166" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7167" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7168" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character constraint="lobe" constraintid="o7169" is_modifier="false" name="length" src="d0_s8" value="1-2 times lobes" value_original="1-2 times lobes" />
      </biological_entity>
      <biological_entity id="o7169" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 4 mm, ± hirtellous to villous;</text>
      <biological_entity id="o7170" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="less hirtellous" name="pubescence" src="d0_s9" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 16–20 scales 1–1.5 mm.</text>
      <biological_entity id="o7171" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o7172" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s10" to="20" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o7171" id="r515" name="consist_of" negation="false" src="d0_s10" to="o7172" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>