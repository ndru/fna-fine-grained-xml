<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">19</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">xanthium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">spinosum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 987. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus xanthium;species spinosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242417494</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthium</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">ambrosioides</taxon_name>
    <taxon_hierarchy>genus Xanthium;species ambrosioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spinosum</taxon_name>
    <taxon_name authority="Bel" date="unknown" rank="variety">inerme</taxon_name>
    <taxon_hierarchy>genus Xanthium;species spinosum;variety inerme;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 (–120+) cm;</text>
      <biological_entity id="o17556" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodal spines usually in pairs, simple or 2–3-partite, 15–30+ mm.</text>
      <biological_entity constraint="nodal" id="o17557" name="spine" name_original="spines" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 1–15 (–25+) mm;</text>
      <biological_entity id="o17558" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o17559" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ± ovate to lanceolate or lance-linear, 4–8 (–12+) × 1–3 (–5+) cm, often pinnately 3 (–7+) -lobed, abaxial faces gray to white, densely strigose.</text>
      <biological_entity id="o17560" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17561" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s3" to="lanceolate or lance-linear" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="5" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="often pinnately" name="shape" src="d0_s3" value="3(-7+)-lobed" value_original="3(-7+)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Burs 10–12 (–15+) mm.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 36.</text>
      <biological_entity constraint="abaxial" id="o17562" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s3" to="white" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17563" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp or seasonally wet, alkaline soils, waste places, margins of agriculture</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline soils" modifier="damp or seasonally wet" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="margins" constraint="of agriculture" />
        <character name="habitat" value="agriculture" />
        <character name="habitat" value="damp" modifier="seasonally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que., Sask.; Ala., Ariz., Calif., Colo., Conn., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.C., Tenn., Tex., Utah, Va., Wash., W.Va.; Mexico; Central America; South America; widely established in Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="widely established in Old World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spiny cocklebur</other_name>
  <other_name type="common_name">clotbur</other_name>
  <other_name type="common_name">lampourde épineuse</other_name>
  <discussion>Some authors have contended that Xanthium spinosum originated in South America and is introduced and/or naturalized everywhere else that it is found.</discussion>
  
</bio:treatment>