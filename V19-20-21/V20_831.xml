<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="treatment_page">359</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1900" rank="genus">OREOSTEMMA</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 224. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus OREOSTEMMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oreo, mountain, and stemma, crown</other_info_on_name>
    <other_info_on_name type="fna_id">123143</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Greene" date="unknown" rank="genus">Oreastrum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 146. 1896,</place_in_publication>
      <other_info_on_pub>not Oriastrum Poeppig 1843</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Oreastrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–40 (–70) cm (taproots, sometimes thickly rhizomatous, sometimes with branching caudices).</text>
      <biological_entity id="o14508" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent-ascending to erect, simple (essentially scapose), usually distally finely, loosely tomentose, sometimes stipitate-glandular.</text>
      <biological_entity id="o14509" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent-ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="usually distally; distally finely; finely; loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal (persistent rosettes), cauline reduced;</text>
      <biological_entity id="o14510" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14511" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o14512" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-nerved, linear to oblanceolate, margins entire (apices acute to obtuse or rounded), faces glabrous, sparsely villous, or stipitate-glandular.</text>
      <biological_entity id="o14513" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o14514" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14515" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o14516" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres broadly turbinate, (5–12 ×) 10–20 mm.</text>
      <biological_entity id="o14517" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="[5" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="]10" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 25–40 in (2–) 3–4 series, appressed (commonly purplish), 1 (–3) -nerved (thin, not resinous; often low-keeled), linear to linear-elliptic or oblanceolate, subequal, usually herbaceous, sometimes proximal margins indurate, faces finely and loosely tomentose.</text>
      <biological_entity id="o14518" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o14519" from="25" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1(-3)-nerved" value_original="1(-3)-nerved" />
        <character constraint="to margins, faces" constraintid="o14520, o14521" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o14519" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o14520" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="true" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="true" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="true" modifier="usually" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o14521" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="true" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="true" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="true" modifier="usually" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, shallowly pitted, epaleate.</text>
      <biological_entity id="o14522" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 10–40, pistillate, fertile;</text>
      <biological_entity id="o14523" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="40" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, often drying blue to purple.</text>
      <biological_entity id="o14524" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="drying" value_original="drying" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s11" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 30–55, bisexual, fertile;</text>
      <biological_entity id="o14525" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="55" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellowish at maturity, tubes shorter than tubular throats (barely constricted), lobes 5, erect, lanceolate to deltate;</text>
      <biological_entity id="o14526" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o14527" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than tubular throats" constraintid="o14528" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14528" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o14529" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages linear-lanceolate.</text>
      <biological_entity constraint="style-branch" id="o14530" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (brownish) narrowly cylindric, ribs 5–10 (raised), faces glabrous or sparsely strigillose;</text>
      <biological_entity id="o14531" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o14532" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s15" to="10" />
      </biological_entity>
      <biological_entity id="o14533" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 25–40, tawny-white, barbellate, apically attenuate bristles in 1 series, sometimes plus shorter bristles or setae in a second, outer series.</text>
      <biological_entity id="o14534" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o14535" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s16" to="40" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="tawny-white" value_original="tawny-white" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o14536" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="outer" id="o14539" name="series" name_original="series" src="d0_s16" type="structure" />
      <relation from="o14534" id="r1329" name="consist_of" negation="false" src="d0_s16" to="o14535" />
      <relation from="o14535" id="r1330" name="in" negation="false" src="d0_s16" to="o14536" />
      <relation from="o14537" id="r1331" modifier="sometimes" name="in" negation="false" src="d0_s16" to="o14539" />
      <relation from="o14538" id="r1332" modifier="sometimes" name="in" negation="false" src="d0_s16" to="o14539" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity constraint="shorter" id="o14537" name="bristle" name_original="bristles" src="d0_s16" type="structure" />
      <biological_entity constraint="shorter" id="o14538" name="seta" name_original="setae" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o14540" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>191.</number>
  <other_name type="common_name">Mountaincrown</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>Oreostemma is distinguished by its basal rosettes of narrow, 3-nerved leaves, single heads on scapose stems, turbinate involucres with (2–)3–4 subequal series of herbaceous phyllaries, white rays, long style-branch appendages, and cylindric cypselae. The species mostly have been previously treated within Aster.</discussion>
  <references>
    <reference>Cronquist, A. 1948. A revision of the Oreastrum group of Aster. Leafl. W. Bot. 5: 73–82.</reference>
    <reference>Nesom, G. L. 1993d. The genus Oreostemma (Asteraceae: Astereae). Phytologia 74: 305–316.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 2–7 cm; stems, phyllaries, and often leaves sparsely villous to glabrate, densely short-stipitate-glandular; leaf blades linear</description>
      <determination>3 Oreostemma peirsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 4–70 cm; stems, leaves, and phyllaries glabrous or villous, eglandular; leaf blades oblanceolate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems, leaves, and phyllaries villous to glabrate, at least some hairs always perceptible on distal stems and proximal portions of phyllaries; phyllaries herbaceous or slightly tawny-indurate proximally, outer 0.8–1.2 mm wide proximally, 1-nerved</description>
      <determination>1 Oreostemma alpigenum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems, leaves, and phyllaries glabrous; phyllaries strongly indurate-stramineous proximally, outer 1.5–2 mm wide proximally, 3-nerved (nerves separating phyllary into 4 longitudinal bands)</description>
      <determination>2 Oreostemma elatum</determination>
    </key_statement>
  </key>
</bio:treatment>