<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">231</other_info_on_meta>
    <other_info_on_meta type="illustration_page">227</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Cavanilles" date="1802" rank="genus">dyssodia</taxon_name>
    <taxon_name authority="(Ventenant) A. Hitchcock" date="1891" rank="species">papposa</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>5: 503. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus dyssodia;species papposa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416462</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tagetes</taxon_name>
    <taxon_name authority="Ventenant" date="unknown" rank="species">papposa</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Pl. Nouv., plate</publication_title>
      <place_in_publication>36. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tagetes;species papposa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 15–50 × 10–40 mm overall, ultimate lobes (7–) 11–15, 5–20 × 1–3 mm, glabrous or sparsely hairy, dotted with oil-glands.</text>
      <biological_entity id="o12847" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s0" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s0" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12848" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s0" to="11" to_inclusive="false" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s0" to="15" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s0" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character constraint="with oil-glands" constraintid="o12849" is_modifier="false" name="coloration" src="d0_s0" value="dotted" value_original="dotted" />
      </biological_entity>
      <biological_entity id="o12849" name="oil-gland" name_original="oil-glands" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 1–5 (–10) mm.</text>
      <biological_entity id="o12850" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 6–10 mm.</text>
      <biological_entity id="o12851" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries each bearing 1–7 oil-glands.</text>
      <biological_entity id="o12852" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure" />
      <biological_entity id="o12853" name="oil-gland" name_original="oil-glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <relation from="o12852" id="r879" name="bearing" negation="false" src="d0_s3" to="o12853" />
    </statement>
    <statement id="d0_s4">
      <text>Ray laminae 1.5–2.5 × 1–2 mm.</text>
      <biological_entity constraint="ray" id="o12854" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas ca. 3 mm.</text>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae3–3.5 mm;</text>
      <biological_entity constraint="disc" id="o12855" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 1–3 mm. 2n = 26.</text>
      <biological_entity id="o12856" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12857" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands, often ruderal, fields, along roadways</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="roadways" modifier="ruderal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ariz., Ark., Calif., Colo., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.Mex., N.Y., N.Dak., Ohio, Okla., Pa., S.Dak., Tenn., Tex., Utah, Vt., W.Va., Wis., Wyo.; Mexico; Central America; introduced in South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="in South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Dogweed</other_name>
  <discussion>Records of Dyssodia papposa from Ontario and from California, Maine, Massachusetts, New Hampshire, New York, and Vermont evidently document local, probably ephemeral, introductions. In 1837, C. W. Short noted of D. papposa on a specimen label, “This plant is so abundant, and exhales an odor so unpleasant as to sicken the traveler over the western prairies of Illinois, in autumn.”</discussion>
  
</bio:treatment>