<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">128</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1771" rank="genus">ECLIPTA</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.,</publication_title>
      <place_in_publication>157, 286. 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus ECLIPTA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ekleipsis, a failing, perhaps alluding to minute or wanting pappus</other_info_on_name>
    <other_info_on_name type="fna_id">111273</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 10–50 (–70+) cm.</text>
      <biological_entity id="o13249" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or decumbent, branched from bases and/or distally (sometimes rooting at proximal nodes).</text>
      <biological_entity id="o13251" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="from bases" constraintid="o13252" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o13252" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o13253" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (1-nerved or 3-nerved) lanceolate to lance-linear, bases cuneate, margins serrate to subentire, faces sparsely scabrellous.</text>
      <biological_entity id="o13254" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o13255" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13256" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s5" to="subentire" />
      </biological_entity>
      <biological_entity id="o13257" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in loose, corymbiform arrays or borne singly.</text>
      <biological_entity id="o13258" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o13259" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o13258" id="r907" name="in" negation="false" src="d0_s6" to="o13259" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 3–5 mm diam.</text>
      <biological_entity id="o13260" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 8–12+ in 2–3 series (lanceolate to linear, subequal, thin-herbaceous, spreading in fruit).</text>
      <biological_entity id="o13261" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o13262" from="8" name="quantity" src="d0_s8" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o13262" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, paleate (paleae linear to filiform, not conduplicate, falling with fruits).</text>
      <biological_entity id="o13263" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 20–40 (in 2–3+ series), pistillate, fertile;</text>
      <biological_entity id="o13264" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="40" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white or whitish.</text>
      <biological_entity id="o13265" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 15–30+, bisexual, fertile;</text>
      <biological_entity id="o13266" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="30" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas white or whitish, tubes much shorter than ampliate, cylindric throats, lobes 4–5, ± deltate.</text>
      <biological_entity id="o13267" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o13268" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than ampliate , cylindric throats" constraintid="o13269" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o13269" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="ampliate" value_original="ampliate" />
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o13270" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obcompressed, weakly 3–4-angled (not winged, epidermes usually corky and rugose to tuberculate);</text>
      <biological_entity id="o13271" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s14" value="3-4-angled" value_original="3-4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, coroniform (sometimes with 2 teeth).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 11.</text>
      <biological_entity id="o13272" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o13273" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly warm-temperate to tropical New World; introduced in Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly warm-temperate to tropical New World" establishment_means="native" />
        <character name="distribution" value="in Old World" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>289.</number>
  <discussion>Species 1–4 (1 in the flora).</discussion>
  
</bio:treatment>