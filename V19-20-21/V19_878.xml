<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">517</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Rydberg) McArthur" date="1981" rank="subgenus">tridentatae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">tridentata</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall &amp; Clements" date="1923" rank="subspecies">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>326: 137. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus tridentatae;species tridentata;subspecies parishii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068075</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 220. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Artemisia;species parishii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="(Nuttall) W. A. Weber" date="unknown" rank="species">tridentata</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="variety">parishii</taxon_name>
    <taxon_hierarchy>genus Artemisia;species tridentata;variety parishii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Seriphidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tridentatum</taxon_name>
    <taxon_name authority="(A. Gray) W. A. Weber" date="unknown" rank="subspecies">parishii</taxon_name>
    <taxon_hierarchy>genus Seriphidium;species tridentatum;subspecies parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–200 (–300) cm (crowns rounded).</text>
      <biological_entity id="o17037" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Vegetative branches interspersed among flowering-stems.</text>
      <biological_entity id="o17038" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o17039" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure" />
      <relation from="o17038" id="r1533" name="interspersed among" negation="false" src="d0_s1" to="o17039" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cuneate or lanceolate (1–) 1.5–2 (–2.5) × 0.1–0.3 cm, usually 3-lobed, sometimes entire.</text>
      <biological_entity id="o17040" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s2" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s2" to="0.3" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays 15–30 × 2–6 cm (branches widely spreading or drooping).</text>
      <biological_entity id="o17041" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" modifier="in paniculiform arrays" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="in paniculiform arrays" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 2–4 × 1–2 mm.</text>
      <biological_entity id="o17042" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Florets 3–7.</text>
      <biological_entity id="o17043" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae hairy or glabrous.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 36.</text>
      <biological_entity id="o17044" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17045" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loose sandy soils of valleys and foothills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="loose sandy soils" constraint="of valleys and foothills" />
        <character name="habitat" value="valleys" />
        <character name="habitat" value="foothills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17b.</number>
  <other_name type="past_name">parishi</other_name>
  <other_name type="common_name">Mojave sagebrush</other_name>
  <discussion>Subspecies parishii is found in coastal ranges in southern California and Baja California, and inland to areas south of the Great Basin. It has been distinguished traditionally by the presence of drooping flowering branches and hairy cypselae, characteristics found on the type specimen. These characteristics occur sporadically in populations of other subspecies throughout the warm desert regions of southern California, Nevada, and Utah; the characteristically longer leaves and distinctive aroma support recognition of this subspecies. This treatment is the first to include Mojave Desert, Owens Valley, and Colorado Plateau populations within subsp. parishii.</discussion>
  
</bio:treatment>