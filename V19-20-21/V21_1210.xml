<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ageratum</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="species">maritimum</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 117. 1818</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <place_in_publication>4(qto.) 150. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ageratum;species maritimum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066026</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">littorale</taxon_name>
    <taxon_hierarchy>genus Ageratum;species littorale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">littorale</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">hondurense</taxon_name>
    <taxon_hierarchy>genus Ageratum;species littorale;variety hondurense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 10–50 cm (semisucculent, rhizomatous, forming colonies).</text>
      <biological_entity id="o24862" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to straggling or creeping (rooting at nodes), glabrous but for puberulous-pilose nodes.</text>
      <biological_entity id="o24864" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="growth_form_or_orientation" src="d0_s1" to="straggling" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character constraint="but-for nodes" constraintid="o24865" is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24865" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="puberulous-pilose" value_original="puberulous-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades deltate-ovate to oblong, mostly 0.8–4 × 0.5–3 cm, (fleshy) margins toothed, faces glabrous or glabrate.</text>
      <biological_entity id="o24866" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="deltate-ovate" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24867" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o24868" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles glabrous or glabrate.</text>
      <biological_entity id="o24869" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ca. 3 × 3–4 mm.</text>
      <biological_entity id="o24870" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries elliptic-lanceolate, glabrous or glabrate, tips abruptly tapered to nearly obtuse.</text>
      <biological_entity id="o24871" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o24872" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character char_type="range_value" from="abruptly tapered" name="shape" src="d0_s5" to="nearly obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas lavender or blue to white.</text>
      <biological_entity id="o24873" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s6" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae glabrous;</text>
      <biological_entity id="o24874" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi usually blunt coronas ca. 0.1 mm, rarely of separate scales.</text>
      <biological_entity id="o24875" name="pappus" name_original="pappi" src="d0_s8" type="structure" />
      <biological_entity id="o24876" name="corona" name_original="coronas" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="usually" name="shape" src="d0_s8" value="blunt" value_original="blunt" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity id="o24877" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="separate" value_original="separate" />
      </biological_entity>
      <relation from="o24876" id="r1690" modifier="rarely" name="part_of" negation="false" src="d0_s8" to="o24877" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Beach sand and nearby thickets, coral soils, salt marshes, hammocks, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beach sand" />
        <character name="habitat" value="nearby thickets" />
        <character name="habitat" value="coral soils" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico (Quintana Roo); West Indies (Cuba, Hispaniola); Central America (Belize).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico (Quintana Roo)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Cape Sable whiteweed</other_name>
  <discussion>Plants from Florida (Ageratum littorale, the type from Florida) are described here. Plants of the West Indies and Mexico (broadening the species concept to A. maritimum, the type from Cuba) have various elaborations of vestiture and a more conspicuous pappus–coronas with even to laciniate margins or rings of nearly separate scales mostly 0.2–1.5 mm. In addition to the distinctive relatively small, glabrous or glabrate leaves, plants of A. maritimum are characterized by heads in clusters, usually held well beyond the leaves.</discussion>
  
</bio:treatment>