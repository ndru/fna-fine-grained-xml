<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">23</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">PARTHENICE</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 85. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus PARTHENICE</taxon_hierarchy>
    <other_info_on_name type="etymology">No etymology in protologue; evidently alluding to similarities to members of genus Parthenium</other_info_on_name>
    <other_info_on_name type="fna_id">124103</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–200 cm.</text>
      <biological_entity id="o17875" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o17876" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o17877" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (3-nerved) deltate to ovate, margins entire or toothed, faces usually softly pubescent, sometimes rough-hairy.</text>
      <biological_entity id="o17878" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="ovate" />
      </biological_entity>
      <biological_entity id="o17879" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o17880" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually softly" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="rough-hairy" value_original="rough-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads obscurely radiate or disciform, borne in loose, paniculiform arrays.</text>
      <biological_entity id="o17881" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o17882" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o17881" id="r1229" name="borne in" negation="false" src="d0_s6" to="o17882" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 3–5 mm.</text>
      <biological_entity id="o17883" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent (outer) or falling, 13, distinct, outer 5 herbaceous (becoming reflexed), inner 6–8 membranous (each becoming cupped around a cypsela).</text>
      <biological_entity id="o17884" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character name="quantity" src="d0_s8" value="13" value_original="13" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17885" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17886" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex;</text>
      <biological_entity id="o17887" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleae of 2 kinds: peripheral purplish-mottled, becoming somewhat fleshy and arcuate-clavate, shed together in pairs with each cypsela;</text>
      <biological_entity id="o17888" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish-mottled" value_original="purplish-mottled" />
        <character is_modifier="false" modifier="becoming somewhat" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="shape" src="d0_s10" value="arcuate-clavate" value_original="arcuate-clavate" />
      </biological_entity>
      <biological_entity id="o17889" name="kind" name_original="kinds" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s10" value="peripheral" value_original="peripheral" />
      </biological_entity>
      <biological_entity id="o17890" name="pair" name_original="pairs" src="d0_s10" type="structure" />
      <biological_entity id="o17891" name="cypselum" name_original="cypsela" src="d0_s10" type="structure" />
      <relation from="o17888" id="r1230" name="consist_of" negation="false" src="d0_s10" to="o17889" />
      <relation from="o17888" id="r1231" name="shed" negation="false" src="d0_s10" to="o17890" />
      <relation from="o17888" id="r1232" name="with" negation="false" src="d0_s10" to="o17891" />
    </statement>
    <statement id="d0_s11">
      <text>inner 0 or narrowly conic, persistent.</text>
      <biological_entity id="o17892" name="palea" name_original="paleae" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o17893" name="kind" name_original="kinds" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s11" value="inner" value_original="inner" />
      </biological_entity>
      <relation from="o17892" id="r1233" name="consist_of" negation="false" src="d0_s11" to="o17893" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 6–8;</text>
      <biological_entity id="o17894" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellowish (minute), glandular.</text>
      <biological_entity id="o17895" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 10–25+, functionally staminate;</text>
      <biological_entity id="o17896" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="25" upper_restricted="false" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellowish, funnelform, distally glandular and hairy (anthers ± connate).</text>
      <biological_entity id="o17897" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="distally" name="architecture_or_function_or_pubescence" src="d0_s15" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae (ray) strongly obcompressed to obflattened, obovate, tuberculate, shed with 2 adjacent, ± fleshy paleae;</text>
      <biological_entity id="o17898" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o17899" name="palea" name_original="paleae" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s16" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" modifier="more or less" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o17898" id="r1234" name="shed with" negation="false" src="d0_s16" to="o17899" />
    </statement>
    <statement id="d0_s17">
      <text>pappi 0 or rudimentary.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 18.</text>
      <biological_entity id="o17900" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity constraint="x" id="o17901" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>247.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Sauck, J. R. 1975. Distribution, chromosomes, and taxonomy of Parthenice mollis (Compositae). Madroño 23: 227–234.</reference>
  </references>
  
</bio:treatment>