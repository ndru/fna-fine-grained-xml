<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="treatment_page">465</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="species">album</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">album</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eupatorium;species album;variety album</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068395</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">album</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glandulosum</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species album;variety glandulosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 3-nerved distal to bases, 5–9 (–11) × 1–2.5 (–4) mm, margins coarsely serrate, apices obtuse to rounded, faces pubescent.</text>
      <biological_entity id="o9792" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="3-nerved" value_original="3-nerved" />
        <character constraint="to bases" constraintid="o9793" is_modifier="false" name="position_or_shape" src="d0_s0" value="distal" value_original="distal" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s0" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s0" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s0" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s0" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9793" name="base" name_original="bases" src="d0_s0" type="structure" />
      <biological_entity id="o9794" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s0" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o9795" name="apex" name_original="apices" src="d0_s0" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s0" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>2n = 20.</text>
      <biological_entity id="o9796" name="face" name_original="faces" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9797" name="chromosome" name_original="" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open or wooded, disturbed sites, sandy pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open" />
        <character name="habitat" value="wooded" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="sandy pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Del., Fla., Ga., La., Md., Miss., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>