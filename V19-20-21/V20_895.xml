<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">xanthisma</taxon_name>
    <taxon_name authority="(R. C. Jackson) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="section">Havardii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1410. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus xanthisma;section Havardii</taxon_hierarchy>
    <other_info_on_name type="fna_id">316941</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Cassini" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="R. C. Jackson" date="unknown" rank="section">Havardii</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Kansas Sci. Bull.</publication_title>
      <place_in_publication>46: 479. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;section Havardii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nees" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(R. C. Jackson) R. L. Hartman" date="unknown" rank="section">Havardii</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;section Havardii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 25–60 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o30389" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades serrate or dentate, often coarsely so, teeth blunt or terminating in stiff callosities, not bristle-tipped.</text>
      <biological_entity id="o30390" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o30391" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often coarsely; coarsely" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
        <character name="shape" src="d0_s2" value="terminating in stiff callosities" value_original="terminating in stiff callosities" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="bristle-tipped" value_original="bristle-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads radiate.</text>
      <biological_entity id="o30392" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="radiate" value_original="radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres broadly turbinate.</text>
      <biological_entity id="o30393" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="turbinate" value_original="turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 5–7 series, appressed, stramineous, linear to narrowly oblong, mostly 1–2 mm wide, unequal, proximally rigid, distal 1/5–1/2 with green patch or strip, not expanded distally, apices narrowly obtuse to broadly acute, not bristle-tipped, densely stipitate-glandular.</text>
      <biological_entity id="o30394" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly oblong" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="mostly" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s5" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="with strip" constraintid="o30396" from="1/5" name="quantity" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o30395" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o30396" name="strip" name_original="strip" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="green patch" value_original="green patch" />
        <character is_modifier="false" modifier="not; distally" name="size" notes="" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o30397" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly obtuse" name="shape" src="d0_s5" to="broadly acute" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="bristle-tipped" value_original="bristle-tipped" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o30394" id="r2813" name="in" negation="false" src="d0_s5" to="o30395" />
    </statement>
    <statement id="d0_s6">
      <text>Receptacles: pit borders laciniate, teeth or setae mostly distinct, 0.1–0.5 mm.</text>
      <biological_entity id="o30398" name="receptacle" name_original="receptacles" src="d0_s6" type="structure" />
      <biological_entity constraint="pit" id="o30399" name="border" name_original="borders" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o30400" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30401" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray corollas yellow.</text>
      <biological_entity constraint="ray" id="o30402" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae monomorphic, oblong or narrowly ellipsoid, sometimes slightly asymmetric, flattened laterally, 2.5–3 mm, walls thin, 12–14-ribbed, barely discernible, faces sparsely silky;</text>
      <biological_entity id="o30403" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="sometimes slightly" name="architecture_or_shape" src="d0_s8" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30404" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="12-14-ribbed" value_original="12-14-ribbed" />
        <character is_modifier="false" modifier="barely" name="prominence" src="d0_s8" value="discernible" value_original="discernible" />
      </biological_entity>
      <biological_entity id="o30405" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="silky" value_original="silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of white, filiform (not basally flattened) bristles 5–7 mm in 2–3 poorly defined series.</text>
      <biological_entity id="o30407" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o30408" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="true" modifier="poorly" name="prominence" src="d0_s9" value="defined" value_original="defined" />
      </biological_entity>
      <relation from="o30406" id="r2814" name="part_of" negation="false" src="d0_s9" to="o30407" />
    </statement>
    <statement id="d0_s10">
      <text>x = 4.</text>
      <biological_entity id="o30406" name="pappus" name_original="pappi" src="d0_s9" type="structure" constraint="bristle" constraint_original="bristle; bristle">
        <character char_type="range_value" constraint="in series" constraintid="o30408" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o30409" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>195d.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>