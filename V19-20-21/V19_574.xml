<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. C. Eaton in S. Watson" date="1871" rank="genus">GLYPTOPLEURA</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>207, plate 20, figs. 11–18. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus GLYPTOPLEURA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek glyptos, carved, and pleura, rib, alluding to cypselae</other_info_on_name>
    <other_info_on_name type="fna_id">113774</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–6 cm (low-growing, densely cespitose, herbage glabrous);</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o4193" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–25+, ± prostrate, simple or branched, glabrous.</text>
      <biological_entity id="o4194" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="25" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, crowded;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o4195" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal blades ± oblanceolate, margins dentate or pinnately lobed and crustose-denticulate, cauline progressively reduced to oblanceolate, crustose-denticulate bracts.</text>
      <biological_entity constraint="basal" id="o4196" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o4197" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crustose-denticulate" value_original="crustose-denticulate" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="progressively" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o4198" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="crustose-denticulate" value_original="crustose-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (1–3) borne singly or 2–3 in bract-axils.</text>
      <biological_entity id="o4199" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o4200" name="bract-axil" name_original="bract-axils" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <relation from="o4199" id="r427" name="borne" negation="false" src="d0_s6" to="o4200" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not distally inflated, often bracteate.</text>
      <biological_entity id="o4201" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 5–8, linear to oblanceolate bractlets in ± 1 series, apices expanded, crustose-denticulate.</text>
      <biological_entity id="o4202" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" modifier="of 5-8" name="shape" src="d0_s8" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o4203" name="bractlet" name_original="bractlets" src="d0_s8" type="structure" />
      <biological_entity id="o4204" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4205" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="crustose-denticulate" value_original="crustose-denticulate" />
      </biological_entity>
      <relation from="o4203" id="r428" name="in" negation="false" src="d0_s8" to="o4204" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric to urceolate, 3–8+ mm diam.</text>
      <biological_entity id="o4206" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 5–8+ in 1–2 series, commonly purplish-tinged, linear, equal, margins scarious, apices acute.</text>
      <biological_entity id="o4207" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4208" from="5" name="quantity" src="d0_s10" to="8" upper_restricted="false" />
        <character is_modifier="false" modifier="commonly" name="coloration" notes="" src="d0_s10" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o4208" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o4209" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4210" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles ± flat, smooth, glabrous, epaleate.</text>
      <biological_entity id="o4211" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 7–18;</text>
      <biological_entity id="o4212" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s12" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas white to pale-yellow, becoming pink-purple (especially when dry).</text>
      <biological_entity id="o4213" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pale-yellow" />
        <character is_modifier="false" modifier="becoming" name="coloration_or_density" src="d0_s13" value="pink-purple" value_original="pink-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae straw-colored or light-brown, subcylindric or slightly flattened, often curved, abruptly beaked, obtusely 4–5-angled, ribs transversely roughened, alternating with 5 rows of pits, glabrous or minutely puberulent;</text>
      <biological_entity id="o4214" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subcylindric" value_original="subcylindric" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s14" value="4-5-angled" value_original="4-5-angled" />
      </biological_entity>
      <biological_entity id="o4215" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief_or_texture" src="d0_s14" value="roughened" value_original="roughened" />
        <character constraint="with rows" constraintid="o4216" is_modifier="false" name="arrangement" src="d0_s14" value="alternating" value_original="alternating" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4216" name="row" name_original="rows" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4217" name="pit" name_original="pits" src="d0_s14" type="structure" />
      <relation from="o4216" id="r429" name="part_of" negation="false" src="d0_s14" to="o4217" />
    </statement>
    <statement id="d0_s15">
      <text>pappi falling (outer, individually) or ± persistent (inner, connate at bases in easily fractured rings), of 50–80+, white, barbellulate to smooth bristles in 3–4+ series.</text>
      <biological_entity id="o4219" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="50" is_modifier="true" name="quantity" src="d0_s15" to="80" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="barbellulate" is_modifier="true" name="architecture" src="d0_s15" to="smooth" />
      </biological_entity>
      <biological_entity id="o4220" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s15" to="4" upper_restricted="false" />
      </biological_entity>
      <relation from="o4218" id="r430" name="consist_of" negation="false" src="d0_s15" to="o4219" />
      <relation from="o4219" id="r431" name="in" negation="false" src="d0_s15" to="o4220" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o4218" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o4221" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>75.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>A molecular phylogenetic investigation by J. Lee et. al (2003) provided evidence that Glyptopleura is part of a primarily western North American radiation in Cichorieae. That study did not resolve the relationship of Glyptopleura to other genera within the radiation.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ligules 4–10 mm, equaling involucres or exserted 1–5 mm</description>
      <determination>1 Glyptopleura marginata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ligules 15–25 mm, exserted 10–20 mm</description>
      <determination>2 Glyptopleura setulosa</determination>
    </key_statement>
  </key>
</bio:treatment>