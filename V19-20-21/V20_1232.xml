<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">556</other_info_on_meta>
    <other_info_on_meta type="treatment_page">557</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">integerrimus</taxon_name>
    <taxon_name authority="(A. Gray) Cronquist" date="1950" rank="variety">ochroleucus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>6: 48. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species integerrimus;variety ochroleucus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068729</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Richardson" date="unknown" rank="species">lugens</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">ochroleucus</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 388. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species lugens;variety ochroleucus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">cordatus</taxon_name>
    <taxon_hierarchy>genus Senecio;species cordatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">exaltatus</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="subspecies">ochraceus</taxon_name>
    <taxon_hierarchy>genus Senecio;species exaltatus;subspecies ochraceus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">leibergii</taxon_name>
    <taxon_hierarchy>genus Senecio;species leibergii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="(Piper) Piper" date="unknown" rank="species">ochraceus</taxon_name>
    <taxon_hierarchy>genus Senecio;species ochraceus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage arachnose, tomentose, or villous, unevenly glabrescent, at flowering.</text>
      <biological_entity id="o23646" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal and proximal cauline distinctly petiolate;</text>
      <biological_entity id="o23647" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades (cauline) usually ovate to rounded-deltate, sometimes obovate to oblanceolate.</text>
      <biological_entity id="o23648" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23649" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s2" to="rounded-deltate" />
        <character char_type="range_value" from="obovate" modifier="sometimes" name="shape" src="d0_s2" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads (3–) 8–12 (–40+).</text>
      <biological_entity id="o23650" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s3" to="8" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="40" upper_restricted="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries ± linear, 7–10 mm, tips green or minutely black.</text>
      <biological_entity id="o23651" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23652" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="minutely" name="coloration" src="d0_s4" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets usually 5;</text>
      <biological_entity id="o23653" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white or ochroleucous, laminae (5–) 10–15 (–20) mm.</text>
      <biological_entity id="o23654" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="ochroleucous" value_original="ochroleucous" />
      </biological_entity>
      <biological_entity id="o23655" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist openings in coniferous woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist openings" constraint="in coniferous woodlands" />
        <character name="habitat" value="coniferous woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22b.</number>
  
</bio:treatment>