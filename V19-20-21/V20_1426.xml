<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="treatment_page">632</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1883" rank="genus">LEPIDOSPARTUM</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 50. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus LEPIDOSPARTUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lepidos, scale, and sparton, Spanish broom (the plant)</other_info_on_name>
    <other_info_on_name type="fna_id">118069</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="de Candolle" date="unknown" rank="genus">Tetradymia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="section">Lepidosparton</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>9: 207. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tetradymia;section Lepidosparton;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs (treelets), 20–250 cm (juvenile stems and foliage tomentose, later stems and leaves glabrous or pannose to tomentose, sometimes glabrescent).</text>
      <biological_entity id="o27732" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5+, erect (much branched).</text>
      <biological_entity id="o27733" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o27734" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades palmately (sometimes obscurely) nerved;</text>
    </statement>
    <statement id="d0_s6">
      <text>the juvenile obovate to spatulate, on flowering-stems filiform to acerose or scalelike, margins entire, faces glabrous or tomentose to glabrescent.</text>
      <biological_entity id="o27735" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="shape" src="d0_s6" value="juvenile" value_original="juvenile" />
      </biological_entity>
      <biological_entity id="o27736" name="flowering-stem" name_original="flowering-stems" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="spatulate" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s6" to="acerose or scalelike" />
      </biological_entity>
      <biological_entity id="o27737" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27738" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s6" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads discoid, in ± paniculiform arrays (or clusters of 3–5).</text>
      <biological_entity id="o27739" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o27740" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o27739" id="r2569" name="in" negation="false" src="d0_s7" to="o27740" />
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 (or bractlets intergrading with phyllaries).</text>
      <biological_entity id="o27741" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres obconic to cylindric, 4–8 mm diam.</text>
      <biological_entity id="o27742" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s9" to="cylindric" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries persistent, 8–13 or 12–23+ in 2–4+ series, erect, distinct, mostly ovate to lanceolate, unequal (outer shorter), margins sometimes scarious.</text>
      <biological_entity id="o27743" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="13" />
        <character char_type="range_value" constraint="in series" constraintid="o27744" from="12" name="quantity" src="d0_s10" to="23" upper_restricted="false" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="mostly ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o27744" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o27745" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat, smooth or foveolate (glabrous or arachnose), epaleate.</text>
      <biological_entity id="o27746" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s11" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 0.</text>
      <biological_entity id="o27747" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 3–17+, bisexual, fertile;</text>
      <biological_entity id="o27748" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="17" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pale to bright-yellow, tubes longer than campanulate to funnelform throats, lobes 5, recurved, lance-linear;</text>
      <biological_entity id="o27749" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s14" to="bright-yellow" />
      </biological_entity>
      <biological_entity id="o27750" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than campanulate to funnelform throats" constraintid="o27751, o27752" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o27751" name="funnelform" name_original="funnelform" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o27752" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o27753" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches: stigmatic areas ± continuous, apices rounded-truncate.</text>
      <biological_entity id="o27754" name="style-branch" name_original="style-branches" src="d0_s15" type="structure" />
      <biological_entity constraint="stigmatic" id="o27755" name="area" name_original="areas" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s15" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o27756" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded-truncate" value_original="rounded-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± fusiform, 5–15-nerved, glabrous or ± pilose;</text>
      <biological_entity id="o27757" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-15-nerved" value_original="5-15-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of ca. 150, white or tawny, barbellulate bristles (in 3–4 series).</text>
      <biological_entity id="o27759" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="150" value_original="150" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="tawny" value_original="tawny" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o27758" id="r2570" name="consist_of" negation="false" src="d0_s17" to="o27759" />
    </statement>
    <statement id="d0_s18">
      <text>x = 30.</text>
      <biological_entity id="o27758" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o27760" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>238.</number>
  <discussion>Species 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades (flowering stems) scale-like, 2–3 mm; florets 9–17; cypselae glabrous</description>
      <determination>3 Lepidospartum squamatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades (flowering stems) filiform to acerose, 3–40 mm; florets 3–6; cypselae densely hairy</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowering stems tomentose with glabrous striae subtending leaves, interruptingtomentum; cypselae 5–6.5 mm</description>
      <determination>1 Lepidospartum latisquamum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowering stems evenly pannose, not striate, dotted with glandular blisters; cypselae ca. 4 mm</description>
      <determination>2 Lepidospartum burgessii</determination>
    </key_statement>
  </key>
</bio:treatment>