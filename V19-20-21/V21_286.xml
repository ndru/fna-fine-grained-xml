<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">FLOURENSIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 592. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus FLOURENSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Marie-Jean-Pierre Flourens, 1794–1867, physiologist, perpetual secretary, Académie des Sciences, Paris</other_info_on_name>
    <other_info_on_name type="fna_id">112885</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs [trees], to 100 (–200) [–500+] cm.</text>
      <biological_entity id="o16110" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched from bases or ± throughout.</text>
      <biological_entity id="o16112" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o16113" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o16113" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate [nearly sessile];</text>
      <biological_entity id="o16114" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades pinnately nerved, mostly elliptic to lance-oblong or ovate, bases rounded to cuneate, margins entire [toothed], faces glabrous or ± scabrellous, usually glanddotted and vernicose.</text>
      <biological_entity id="o16115" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="mostly elliptic" name="shape" src="d0_s5" to="lance-oblong or ovate" />
      </biological_entity>
      <biological_entity id="o16116" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o16117" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16118" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" modifier="usually" name="reflectance" src="d0_s5" value="vernicose" value_original="vernicose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid or radiate, borne ± singly or in ± spiciform arrays.</text>
      <biological_entity id="o16119" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in more or less spiciform arrays" />
      </biological_entity>
      <biological_entity id="o16120" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o16119" id="r1116" name="in" negation="false" src="d0_s6" to="o16120" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to hemispheric, 4–20 mm diam.</text>
      <biological_entity id="o16121" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–40 in 2–4+ series (subequal or unequal, outer longer).</text>
      <biological_entity id="o16122" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o16123" from="12" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
      <biological_entity id="o16123" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to conic-ovoid, paleate (paleae conduplicate, cartilaginous to scarious).</text>
      <biological_entity id="o16124" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="conic-ovoid" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or [5–] 13–21, either neuter, or styliferous and sterile;</text>
      <biological_entity id="o16125" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s10" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="21" />
        <character is_modifier="false" modifier="either" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o16126" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 10–50 [–150], bisexual, fertile;</text>
      <biological_entity id="o16127" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="150" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="50" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes much shorter than cylindric-funnelform throats, lobes 5, ± deltate.</text>
      <biological_entity id="o16128" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o16129" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than cylindric-funnelform throats" constraintid="o16130" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o16130" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o16131" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± compressed or flattened [subterete], oblong to oblanceolate (not winged, ± sericeous);</text>
      <biological_entity id="o16132" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="less compressed or flattened oblong" name="shape" src="d0_s14" to="oblanceolate" />
        <character char_type="range_value" from="less compressed or flattened oblong" name="shape" src="d0_s14" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent or tardily falling, of 2 subulate scales.</text>
      <biological_entity id="o16134" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o16133" id="r1117" name="consist_of" negation="false" src="d0_s15" to="o16134" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o16133" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o16135" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>281.</number>
  <discussion>Species ca. 30 (2 in the flora).</discussion>
  <references>
    <reference>Dillon, M. O. 1984. A systematic study of Flourensia (Asteraceae, Heliantheae). Fieldiana, Bot., n. s. 16: 1–66.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs; leaf blades elliptic to ovate, 10–25(–40+) × 4–15(–20) mm; ray florets 0</description>
      <determination>1 Flourensia cernua</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Subshrubs; leaf blades elliptic to lance-oblong, (20–)50–100 × 10–40+ mm; ray florets 13–21</description>
      <determination>2 Flourensia pringlei</determination>
    </key_statement>
  </key>
</bio:treatment>