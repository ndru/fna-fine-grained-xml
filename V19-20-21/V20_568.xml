<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">279</other_info_on_meta>
    <other_info_on_meta type="illustration_page">281</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="species">eatonii</taxon_name>
    <place_of_publication>
      <publication_title>Notes Compositae,</publication_title>
      <place_in_publication>91. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species eatonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066586</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 4–23 (–33) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices simple or branched.</text>
      <biological_entity id="o4215" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="33" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="23" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o4216" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending or decumbent, strigose, rarely hirtellous, sometimes minutely glandular.</text>
      <biological_entity id="o4217" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending or decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o4218" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades (3-nerved) linear to oblanceolate, 50–110 (–190) × (1–) 2–8 (–13) mm, cauline gradually reduced distally, margins entire, faces loosely strigose to sparsely hirsuto-villous, eglandular.</text>
      <biological_entity constraint="basal" id="o4219" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="110" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="190" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="110" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4220" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4221" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4222" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="loosely strigose" name="pubescence" src="d0_s4" to="sparsely hirsuto-villous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–4 (–7).</text>
      <biological_entity id="o4223" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4.5–8 × 8–14 (–17) mm (7–11 × 17–23 mm in var. nevadincola).</text>
      <biological_entity id="o4224" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series, hirsutulous to villous, sometimes minutely glandular.</text>
      <biological_entity id="o4225" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="hirsutulous" name="pubescence" notes="" src="d0_s7" to="villous" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o4226" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o4225" id="r384" name="in" negation="false" src="d0_s7" to="o4226" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 16–42;</text>
      <biological_entity id="o4227" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="42" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white or pink to bluish or purple, 5–8 (–9) mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o4228" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="bluish or purple" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4229" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.5–5 mm (–6.8 mm in var. nevadincola).</text>
      <biological_entity constraint="disc" id="o4230" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.7–3.5 mm (–4.5 mm in var. nevadincola), 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o4231" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o4232" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 12–30 bristles.</text>
      <biological_entity id="o4233" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o4234" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o4235" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
      <relation from="o4233" id="r385" name="outer of" negation="false" src="d0_s12" to="o4234" />
      <relation from="o4233" id="r386" name="inner of" negation="false" src="d0_s12" to="o4235" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="past_name">eatoni</other_name>
  <other_name type="common_name">Eaton’s fleabane</other_name>
  <discussion>Varieties 6 (6 in the flora).</discussion>
  <discussion>J. L. Strother and W. J. Ferlatte (1988) provided a detailed study of Erigeron eatonii and its closest relatives (the following key is adapted from their study). Erigeron eatonii is regarded here as comprising a group of varieties with relatively discrete, nearly non-overlapping distributions, intergrading where their ranges are contiguous (for maps, see Strother and Ferlatte). Varieties villosus and lavandulus are exceptions: var. villosus occurs north of all other varieties except var. lavandulus, which occurs completely within the range of var. villosus and might justifiably be treated at specific rank. Variety nevadincola often has been treated at specific rank; its distinctions are quantitative (larger heads, florets, and fruits) and it intergrades with var. sonnei. Erigeron canaani occurs at the southwestern extreme of the range of E. eatonii var. eatonii and may be better treated at varietal rank within E. eatonii; as noted by Strother and Ferlatte, linear-leaves (diagnostic feature of E. canaani) occur in plants of var. eatonii in other parts of its range.</discussion>
  <references>
    <reference>Strother, J. L. and W. J. Ferlatte. 1988. Review of Erigeron eatonii and allied taxa (Compositae: Astereae). Madroño 35: 77–91.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 4.5–11 mm; disc corollas (3.5–)4.5–6.8 mm; pappi of 18–30 bristles 3.5–5 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 4–5(–7) mm; disc corollas 2.5–4(–5) mm; pappi of 12–20(–28) bristles 2.5–4(–5) mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres 7–11 × (14–)17–23 mm; ray laminae 7–11 mm; disc corollas 4.4–6.8 mm</description>
      <determination>10a Erigeron eatonii var. nevadincola</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres 4.5–8 × 8–12(–16) mm; ray laminae 4.5–6.6(–8.5) mm; disc corollas 3.5–5 mm</description>
      <determination>10b Erigeron eatonii var. sonnei</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries moderately to densely minutely glandular, sparsely to moderately villous</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries sometimes sparsely minutely glandular, hirtellous to villous</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads 1–2(–7), held well beyond basal leaves</description>
      <determination>10c Erigeron eatonii var. eatonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Heads (1–)2–4(–6), held slightly if at all beyond basal leaves</description>
      <determination>10e Erigeron eatonii var. lavandulus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads 1–4; phyllaries sparsely to moderately hirtellous (hairs mostly 0.3–0.8 mm)</description>
      <determination>10d Erigeron eatonii var. plantagineus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads 1(–2); phyllaries moderately to densely villous (hairs mostly 1–2 mm)</description>
      <determination>10f Erigeron eatonii var. villosus</determination>
    </key_statement>
  </key>
</bio:treatment>