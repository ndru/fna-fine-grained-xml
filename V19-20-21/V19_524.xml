<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">343</other_info_on_meta>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
    <other_info_on_meta type="illustration_page">337</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1832" rank="genus">microseris</taxon_name>
    <taxon_name authority="(Hooker) Schultz-Bipontinus" date="1866" rank="species">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Jahresber. Pollichia</publication_title>
      <place_in_publication>22–24: 309. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus microseris;species laciniata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067186</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenonema</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">laciniatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 301. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenonema;species laciniatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scorzonella</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="unknown" rank="species">laciniata</taxon_name>
    <taxon_hierarchy>genus Scorzonella;species laciniata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–120 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o13688" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched and leafy distally, or simple and leafy only proximally (subsp. detlingii and plants of extreme environments).</text>
      <biological_entity id="o13689" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="only proximally; proximally" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (distal often sessile, clasping);</text>
      <biological_entity id="o13690" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear to broadly lanceolate, 10–50 cm, margins entire, dentate, lacerate, or pinnatifid, apices obtuse to acuminate, faces glabrous or scurfy-puberulent.</text>
      <biological_entity id="o13691" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="broadly lanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13692" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o13693" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o13694" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scurfy-puberulent" value_original="scurfy-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles erect or curved-ascending, ebracteate or leafy (10–70 cm).</text>
      <biological_entity id="o13695" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="curved-ascending" value_original="curved-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres globose to narrowly ovoid in fruit, 10–30 mm.</text>
      <biological_entity id="o13696" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o13697" from="globose" name="shape" src="d0_s7" to="narrowly ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13697" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries: often purple-spotted (especially in subsp. laciniata), apices erect, abaxial faces glabrous or scurfy-puberulent (often black-villous in subsp. leptosepala);</text>
      <biological_entity id="o13698" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purple-spotted" value_original="purple-spotted" />
      </biological_entity>
      <biological_entity id="o13699" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13700" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scurfy-puberulent" value_original="scurfy-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer lanceolate to broadly ovate, deltate, or linear, slightly to much shorter than inner, 0.5–9 mm wide, apices cuspidate to acute;</text>
      <biological_entity id="o13701" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o13702" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="broadly ovate deltate or linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="broadly ovate deltate or linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="broadly ovate deltate or linear" />
        <character constraint="than inner phyllaries" constraintid="o13703" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="slightly to much shorter" value_original="slightly to much shorter" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13703" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure" />
      <biological_entity id="o13704" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="cuspidate" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner broadly to narrowly lanceolate, apices acuminate.</text>
      <biological_entity id="o13705" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o13706" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o13707" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 13–300;</text>
      <biological_entity id="o13708" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s11" to="300" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, surpassing phyllaries by 5+ mm.</text>
      <biological_entity id="o13709" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13710" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure" />
      <relation from="o13709" id="r1258" name="surpassing" negation="false" src="d0_s12" to="o13710" />
    </statement>
    <statement id="d0_s13">
      <text>Cypselae columnar, 3.5–8 mm (tapering to bases);</text>
      <biological_entity id="o13711" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 5–10 (–15 in subsp. detlingii, or –24 in subsp. siskiyouensis), white to dull yellowish, deltate to lanceolate, aristate scales 0.5–8 mm, aristae barbellulate to barbellate.</text>
      <biological_entity id="o13712" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" modifier="of 5-10" name="coloration" src="d0_s14" to="dull yellowish" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s14" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o13713" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o13714" name="arista" name_original="aristae" src="d0_s14" type="structure">
        <character char_type="range_value" from="barbellulate" name="architecture" src="d0_s14" to="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13715" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Cutleaf or cut-leaved silverpuffs</other_name>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <discussion>Microseris laciniata comprises four, mostly allopatric subspecies, the diagnostic features of which are found mainly in the phyllaries and pappi. These races intergrade where they come in contact, with the greatest diversity occurring in the Klamath Mountains of northern California and southern Oregon (K. L. Chambers 2004b). S. Mauthe et al. (1981) reported on a detailed morphologic analysis of the heads and pappi of M. laciniata and proposed that the observed variation could be explained by the interaction of a limited number of major genes. The species is consistently self-sterile and outcrossing. It also may reproduce clonally by adventitious buds borne on lateral roots.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems simple; leaves lanceolate or oblanceolate, usually entire, rarely sparingly pinnately lobed; outer phyllaries elliptic-ovate, smallest (2.5–)4 mm wide, apices acute or cuspidate; pappus scales 4–8 mm</description>
      <determination>3d Microseris laciniata subsp. detlingii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually branched; leaves linear to lanceolate or oblanceolate, entire or pinnately lobed; outer phyllaries linear to ovate, smallest 1–9 mm wide, apices acuminate to cuspidate; pappus scales 0.5–3(–4) mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappus scales 9–24, aristae barbellate; Klamath Mountains, Oregon and California</description>
      <determination>3c Microseris laciniata subsp. siskiyouensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappus scales 5–10, aristae usually barbellulate, rarely barbellate; Klamath Mountains and elsewhere</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer phyllaries often purple-spotted, ovate-lanceolate to broadly ovate, smallest 2.5–9 mm wide, apices acute to cuspidate, abaxial faces usually glabrous; widespread</description>
      <determination>3a Microseris laciniata subsp. laciniata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer phyllaries rarely purple-spotted, linear to lanceolate or deltate, smallest 0.5–2.5 mm wide, apices acute to acuminate, abaxial faces often scurfy-puberulent, sometimes black-villous; principally Coast Range and Klamath Mountains, rarely e to Great Basin Region, California, Oregon, Washington</description>
      <determination>3b Microseris laciniata subsp. leptosepala</determination>
    </key_statement>
  </key>
</bio:treatment>