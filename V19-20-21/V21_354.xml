<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">debilis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Heiser" date="1956" rank="subspecies">cucumerifolius</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>13: 160. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species debilis;subspecies cucumerifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068432</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">cucumerifolius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 319. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthus;species cucumerifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">debilis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="variety">cucumerifolius</taxon_name>
    <taxon_hierarchy>genus Helianthus;species debilis;variety cucumerifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, hispid.</text>
      <biological_entity id="o9888" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 4–9 cm, margins usually shallowly, regularly serrate, abaxial faces sometimes glanddotted.</text>
      <biological_entity id="o9889" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s1" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9890" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="regularly" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9891" name="face" name_original="faces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 25–50 cm.</text>
      <biological_entity id="o9892" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae (15–) 20–30 mm.</text>
      <biological_entity constraint="ray" id="o9893" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Discs 16–18 (–20) mm diam. 2n = 34.</text>
      <biological_entity id="o9894" name="disc" name_original="discs" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9895" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., Fla., Ga., La., Md., Mass., Mich., N.H., N.Y., N.C., Pa., R.I., S.C., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7e</number>
  <other_name type="common_name">Cucumberleaf sunflower</other_name>
  
</bio:treatment>