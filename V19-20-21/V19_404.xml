<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Fries" date="1862" rank="species">schultzii</taxon_name>
    <place_of_publication>
      <publication_title>Uppsala Univ. Årsskr.</publication_title>
      <place_in_publication>1862: 150. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species schultzii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066956</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="Schultz-Bipontinus" date="unknown" rank="species">friesii</taxon_name>
    <place_of_publication>
      <publication_title>Bonplandia (Hanover)</publication_title>
      <place_in_publication>9: 326. 1861,</place_in_publication>
      <other_info_on_pub>not Hartman 1838</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Hieracium;species friesii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">rusbyi</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>genus Hieracium;species rusbyi;variety wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Robinson &amp; Greenman" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus Hieracium;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 12–70+ cm.</text>
      <biological_entity id="o13969" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally piloso-hirsute (hairs 4–6+ mm), distally piloso-hirsute (hairs 4–6+ mm).</text>
      <biological_entity id="o13970" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 1–4, cauline 6–9;</text>
      <biological_entity id="o13971" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o13972" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13973" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to lanceolate, 25–150 × 10–35+ mm, lengths 2.5–5+ times widths, bases cuneate, margins usually entire, apices ± acute, faces piloso-hirsute (hairs 3–5+ mm).</text>
      <biological_entity id="o13974" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13975" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="150" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="35" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2.5-5+" value_original="2.5-5+" />
      </biological_entity>
      <biological_entity id="o13976" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13977" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13978" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13979" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 3–25+ in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o13980" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o13981" from="3" name="quantity" src="d0_s4" to="25" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o13981" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s4" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles piloso-hirsute, stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o13982" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 10–15+.</text>
      <biological_entity id="o13983" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o13984" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, 8–9 (–12) mm.</text>
      <biological_entity id="o13985" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 16–21+, apices acute, abaxial faces piloso-hirsute, stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o13986" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="21" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o13987" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13988" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 25–40+;</text>
      <biological_entity id="o13989" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="40" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 5–8 mm.</text>
      <biological_entity id="o13990" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae columnar, 2.5–4 mm;</text>
      <biological_entity id="o13991" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 50–60+, white or stramineous bristles in ± 2 series, 5–6 mm.</text>
      <biological_entity id="o13992" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13993" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" is_modifier="true" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character modifier="more or less" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o13994" name="series" name_original="series" src="d0_s12" type="structure" />
      <relation from="o13992" id="r1280" name="consist_of" negation="false" src="d0_s12" to="o13993" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico; Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  
</bio:treatment>