<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="treatment_page">588</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(T. M. Barkley &amp; R. M. Beauchamp) W. A. Weber &amp; Á. Löve" date="1981" rank="species">ganderi</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 47. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species ganderi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067248</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="T. M. Barkley &amp; R. M. Beauchamp" date="unknown" rank="species">ganderi</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>26: 106, fig. 1. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species ganderi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted (bases weakly spreading).</text>
      <biological_entity id="o10279" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, glabrous or bases hairy.</text>
      <biological_entity id="o10280" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10281" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline, relatively turgid or leathery) petiolate;</text>
      <biological_entity constraint="basal" id="o10282" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades reniform to suborbiculate, 40–70+ × 40–80+ mm, bases truncate to cordate, margins dentate or shallowly lobed.</text>
      <biological_entity id="o10283" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s4" to="80" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10284" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o10285" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves abruptly reduced (petiolate or sessile, oblanceolate, pinnatifid; distal sessile, bractlike).</text>
      <biological_entity constraint="cauline" id="o10286" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–6+ in compact, cymiform arrays.</text>
      <biological_entity id="o10287" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10288" from="3" name="quantity" src="d0_s6" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10288" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ebracteate (or bractlets 1–2), glabrous.</text>
      <biological_entity id="o10289" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 or inconspicuous.</text>
      <biological_entity id="o10290" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (13–) 21, dark green with yellow margins, 8–11+ mm, glabrous.</text>
      <biological_entity id="o10291" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s9" to="21" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="21" value_original="21" />
        <character constraint="with margins" constraintid="o10292" is_modifier="false" name="coloration" src="d0_s9" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="11" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10292" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 13 or 21;</text>
      <biological_entity id="o10293" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="13" value_original="13" />
        <character name="quantity" src="d0_s10" unit="or" value="21" value_original="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 10–12+ mm.</text>
      <biological_entity constraint="corolla" id="o10294" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–60+;</text>
      <biological_entity id="o10295" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 3–4 mm, limbs 2.5–3.5 mm.</text>
      <biological_entity id="o10296" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10297" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 5–6 mm, glabrous;</text>
      <biological_entity id="o10298" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 5–7 mm.</text>
      <biological_entity id="o10299" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Apr–late May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late May" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Recently burned chaparral on mountain slopes, gabbroic soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" modifier="recently burned" constraint="on mountain slopes , gabbroic soils" />
        <character name="habitat" value="mountain slopes" modifier="on" />
        <character name="habitat" value="gabbroic soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Gander’s ragwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Packera ganderi is known only from the mountain ranges of San Diego County, where it has been collected from three sites.</discussion>
  
</bio:treatment>