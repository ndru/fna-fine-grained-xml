<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="treatment_page">469</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1868" rank="genus">hesperevax</taxon_name>
    <taxon_name authority="(Kellogg) Greene" date="1897" rank="species">acaulis</taxon_name>
    <taxon_name authority="Morefield" date="1992" rank="variety">robustior</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>17: 308, fig. 5N. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus hesperevax;species acaulis;variety robustior</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068470</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 2–7 cm.</text>
      <biological_entity id="o3935" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (–7), ± erect;</text>
      <biological_entity id="o3936" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="7" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches usually 0.</text>
      <biological_entity id="o3937" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, sessile or obscurely petiolate, largest (9–) 12–22 (–32) × 2–4 (–5) mm;</text>
      <biological_entity id="o3938" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="32" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole lengths mostly 0–0.8 times blade lengths;</text>
      <biological_entity id="o3939" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="blade" constraintid="o3940" is_modifier="false" modifier="mostly" name="length" src="d0_s4" value="0-0.8 times blade lengths" value_original="0-0.8 times blade lengths" />
      </biological_entity>
      <biological_entity id="o3940" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate to obovate, acute to obtuse;</text>
      <biological_entity id="o3941" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="obovate acute" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="obovate acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>capitular leaves erect or distally ± spreading.</text>
      <biological_entity constraint="capitular" id="o3942" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally more or less" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly or, sometimes, in glomerules of 2–8, largest 3–4 × 2.5–3.5 mm.</text>
      <biological_entity id="o3943" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="largest" value_original="largest" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3944" name="glomerule" name_original="glomerules" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o3943" id="r400" name="borne" negation="false" src="d0_s7" to="o3944" />
    </statement>
    <statement id="d0_s8">
      <text>Receptacles 1.4–1.9 × 1.2–1.7 mm.</text>
      <biological_entity id="o3945" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s8" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate paleae in 3–5 series, 2.5–4 mm.</text>
      <biological_entity id="o3946" name="palea" name_original="paleae" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3947" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o3946" id="r401" name="in" negation="false" src="d0_s9" to="o3947" />
    </statement>
    <statement id="d0_s10">
      <text>Staminate paleae: longest 2.5–3.2 mm.</text>
      <biological_entity id="o3948" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="length" src="d0_s10" value="longest" value_original="longest" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Functionally staminate florets 2–5 (–10);</text>
      <biological_entity id="o3949" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 0.7–1 mm.</text>
      <biological_entity id="o3950" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae mostly 1–1.6 mm.</text>
      <biological_entity id="o3951" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid Apr–mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jun" from="mid Apr" />
        <character name="fruiting time" char_type="range_value" to="mid Jun" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes, flats, woodlands, chaparral, in clearings or under shrubs, often with extra moisture (swales, canyons, roadsides, path edges)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="clearings" modifier="chaparral in" />
        <character name="habitat" value="shrubs" modifier="or under" />
        <character name="habitat" value="extra moisture" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="path edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <other_name type="common_name">Robust evax</other_name>
  <discussion>Variety robustior is known from the mountains of west-central California to interior southwestern Oregon (most known Oregon collections occurred before 1925). Like var. ambusticola, it tends to occur higher than var. acaulis where sympatric. Toward the south, it intergrades about equally with the other varieties (J. D. Morefield 1992c). The largest sizes described above are from a garden-grown specimen; field-collected plants from the same gathering were depauperate but otherwise typical. Variety robustior and Hesperevax sparsiflora var. brevifolia are superficially similar and have been confused; they are not known to intergrade or hybridize.</discussion>
  
</bio:treatment>