<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">397</other_info_on_meta>
    <other_info_on_meta type="treatment_page">398</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1858" rank="genus">hulsea</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1858" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>6(3): 76, plate 13. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus hulsea;species nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220006560</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hulsea</taxon_name>
    <taxon_name authority="Gandoger" date="unknown" rank="species">nana</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">larsenii</taxon_name>
    <taxon_hierarchy>genus Hulsea;species nana;variety larsenii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hulsea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vulcanica</taxon_name>
    <taxon_hierarchy>genus Hulsea;species vulcanica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–15 (–20) cm.</text>
      <biological_entity id="o19211" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–7, glandular-puberulent and sparsely lanate.</text>
      <biological_entity id="o19212" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="7" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o19213" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19214" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades narrowly spatulate, 2–6 cm, margins lobed or toothed (lobes or teeth mostly oblong), faces sparsely lanate to woolly;</text>
      <biological_entity id="o19215" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19216" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o19217" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparsely lanate" name="pubescence" src="d0_s3" to="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cauline leaves narrowly lanceolate, much reduced.</text>
      <biological_entity constraint="distal cauline" id="o19218" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o19219" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic, 8–12 mm diam.</text>
      <biological_entity id="o19220" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 8–12 mm, outer narrowly obovate to oblong-lanceolate, apices acuminate to acute.</text>
      <biological_entity id="o19221" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o19222" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly obovate" name="shape" src="d0_s7" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o19223" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 12–30;</text>
      <biological_entity id="o19224" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous, laminae yellow, 6–10 mm.</text>
      <biological_entity id="o19225" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19226" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas yellow.</text>
      <biological_entity constraint="disc" id="o19227" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 6–8 mm;</text>
      <biological_entity id="o19228" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappus-scales subequal, 1–2 mm. 2n = 38.</text>
      <biological_entity id="o19229" name="pappus-scale" name_original="pappus-scales" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19230" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine to alpine rocky slopes, taluses, mostly volcanic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine to alpine rocky slopes" />
        <character name="habitat" value="taluses" />
        <character name="habitat" value="volcanic substrates" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Dwarf alpinegold</other_name>
  <discussion>Densely lanate or woolly plants of Hulsea nana are referable to var. larsenii. Such plants may occur in distinct populations but can be found together with sparsely lanate and strictly glandular plants. The distribution of lanate to woolly plants appears associated with higher levels of insolation.</discussion>
  
</bio:treatment>