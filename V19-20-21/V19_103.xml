<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">132</other_info_on_meta>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="1901" rank="species">hydrophilum</taxon_name>
    <taxon_name authority="(A. Gray) J. T. Howell" date="1959" rank="variety">vaseyi</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>9: 11. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species hydrophilum;variety vaseyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068189</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">breweri</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">vaseyi</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 404. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species breweri;variety vaseyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="species">montigenum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species montigenum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="species">vaseyi</taxon_name>
    <taxon_hierarchy>genus Cirsium;species vaseyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Heads usually 3–3.5 cm.</text>
      <biological_entity id="o18342" name="head" name_original="heads" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cypselae oblong or elliptic, 4–5 mm. 2n = 32.</text>
      <biological_entity id="o18343" name="cypsela" name_original="cypselae" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18344" name="chromosome" name_original="" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Spring-fed marshy meadows on serpentine parent material in areas of chaparral and mixed evergreen forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="spring-fed marshy meadows" constraint="on serpentine parent material in areas of chaparral and mixed evergreen forest" />
        <character name="habitat" value="serpentine parent material" constraint="in areas of chaparral and mixed evergreen forest" />
        <character name="habitat" value="areas" constraint="of chaparral and mixed evergreen forest" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="mixed evergreen forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35b.</number>
  <other_name type="common_name">Mount Tamalpais or Vasey’s thistle</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety vaseyi is endemic to the slopes of Mt. Tamalpais in the southern North Coast Range of Marin County, California.</discussion>
  
</bio:treatment>