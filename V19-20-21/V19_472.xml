<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="species">foliosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">foliosa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species foliosa;subspecies foliosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068583</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually 1, usually erect, branched distally.</text>
      <biological_entity id="o9893" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal cauline leaves ovate to lanceolate, pinnately lobed (at bases, lobes 1–2 pairs, narrow).</text>
      <biological_entity constraint="distal cauline" id="o9894" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="lanceolate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Calyculi of 8–12+, lance-deltate to lanceolate bractlets.</text>
      <biological_entity id="o9895" name="calyculus" name_original="calyculi" src="d0_s2" type="structure" />
      <biological_entity id="o9896" name="bractlet" name_original="bractlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s2" to="12" upper_restricted="false" />
        <character char_type="range_value" from="lance-deltate" is_modifier="true" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <relation from="o9895" id="r918" name="consist_of" negation="false" src="d0_s2" to="o9896" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres 7–11 × 3–8 mm.</text>
      <biological_entity id="o9897" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 18–22+ in 3 series.</text>
      <biological_entity id="o9898" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9899" from="18" name="quantity" src="d0_s4" to="22" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9899" name="series" name_original="series" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas light yellow, 10–17 mm;</text>
      <biological_entity id="o9900" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="light yellow" value_original="light yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer ligules exserted 6–10 mm.</text>
      <biological_entity constraint="outer" id="o9901" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae ± cylindro-fusiform, 0.9–1.5 mm, ribs ± equal.</text>
      <biological_entity id="o9902" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 14.</text>
      <biological_entity id="o9903" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9904" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy areas, dunes, between shrubs in chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" modifier="open" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="shrubs" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6c.</number>
  <discussion>Subspecies foliosa is known only from San Clemente Island.</discussion>
  
</bio:treatment>