<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">137</other_info_on_meta>
    <other_info_on_meta type="illustration_page">134</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Cavanilles" date="1803" rank="genus">lagascea</taxon_name>
    <taxon_name authority="Hemsley" date="1879" rank="species">decipiens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">decipiens</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus lagascea;species decipiens;variety decipiens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068542</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 25–50 (–75) × 15–30 (–50+) mm.</text>
      <biological_entity id="o2164" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="75" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s0" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles (i.e., stalks supporting glomerules of heads) 5–55 mm.</text>
      <biological_entity id="o2165" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Glomerules hemispheric to subspheric, 10–17 × 20–35 mm.</text>
      <biological_entity id="o2166" name="glomerule" name_original="glomerules" src="d0_s2" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s2" to="subspheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="17" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 8–10 mm, connate 1/2–2/3 lengths, resinous glands to 3 mm.</text>
      <biological_entity id="o2167" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s3" to="2/3" />
      </biological_entity>
      <biological_entity id="o2168" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas 10 mm.</text>
      <biological_entity constraint="disc" id="o2169" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o2170" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi 0.1–1 mm. 2n = 34.</text>
      <biological_entity id="o2171" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2172" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall [year round in Mexico].</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="year round in Mexico" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry canyonlands, granitic slopes, deciduous forests, thorn forests, oak grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry canyonlands" />
        <character name="habitat" value="granitic slopes" />
        <character name="habitat" value="deciduous forests" />
        <character name="habitat" value="thorn forests" />
        <character name="habitat" value="oak grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Pima and Santa Cruz counties represent the northernmost extension of the range of var. decipiens, which extends into Chihuahua and Sonora to northern Sinaloa, whence it is disjunct to Guerrero, Jalisco, Michoacán, and Oaxaca.</discussion>
  
</bio:treatment>