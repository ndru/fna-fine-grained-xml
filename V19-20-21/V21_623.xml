<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">251</other_info_on_meta>
    <other_info_on_meta type="illustration_page">252</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">clappiinae</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="genus">clappia</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">suaedifolia</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 93. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe clappiinae;genus clappia;species suaedifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066409</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 2–4+ cm.</text>
      <biological_entity id="o4817" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="distance" src="d0_s0" to="4" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads held well beyond foliage.</text>
      <biological_entity id="o4818" name="head" name_original="heads" src="d0_s1" type="structure" />
      <biological_entity id="o4819" name="foliage" name_original="foliage" src="d0_s1" type="structure" />
      <relation from="o4818" id="r369" name="held" negation="false" src="d0_s1" to="o4819" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 3–8+ cm, distally dilated, fistulose.</text>
      <biological_entity id="o4820" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray corollas 8–12 × 1–2 mm.</text>
      <biological_entity constraint="ray" id="o4821" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 3–4 mm;</text>
      <biological_entity id="o4822" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 3–5 mm. 2n = 32.</text>
      <biological_entity id="o4823" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4824" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round, mostly spring and fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low, often ± saline sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline sites" modifier="low often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">suaedaefolia</other_name>
  
</bio:treatment>