<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(Hooker) Nuttall" date="1840" rank="species">viscidiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 324. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species viscidiflorus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066355</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crinitaria</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">viscidiflora</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 24. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crinitaria;species viscidiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–120 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with woody, branched caudices, bark whitish tan, becoming gray, flaky and fibrous with age.</text>
      <biological_entity id="o20077" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o20078" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o20079" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish tan" value_original="whitish tan" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
        <character constraint="with age" constraintid="o20080" is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o20080" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o20077" id="r1862" name="with" negation="false" src="d0_s1" to="o20078" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, green, soon becoming tan, glabrous or puberulent, sometimes resin-dotted, often resinous.</text>
      <biological_entity id="o20081" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="soon becoming" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="resin-dotted" value_original="resin-dotted" />
        <character is_modifier="false" modifier="often" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending, spreading, or deflexed;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o20082" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades with evident midnerves plus sometimes 1–2 pairs of smaller, collateral nerves, linear to lanceolate, 10–75 × 0.5–10 mm, flat or sulcate, often twisted, margins often undulate, sometimes ciliate, apices acute to apiculate, faces glabrous or puberulent.</text>
      <biological_entity id="o20083" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="75" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o20084" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="evident" value_original="evident" />
        <character char_type="range_value" constraint="of collateral nerves" constraintid="o20085" from="1" modifier="sometimes" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity constraint="collateral" id="o20085" name="nerve" name_original="nerves" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o20086" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20087" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="apiculate" />
      </biological_entity>
      <biological_entity id="o20088" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o20083" id="r1863" name="with" negation="false" src="d0_s5" to="o20084" />
    </statement>
    <statement id="d0_s6">
      <text>Heads in dense, rounded cymiform arrays (to 7 cm wide), not overtopped by distal leaves.</text>
      <biological_entity id="o20089" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o20090" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="true" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20091" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o20089" id="r1864" name="in" negation="false" src="d0_s6" to="o20090" />
      <relation from="o20089" id="r1865" name="overtopped by" negation="true" src="d0_s6" to="o20091" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to obconic or campanulate, 4–7 × 1.5–2.5 mm.</text>
      <biological_entity id="o20092" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="obconic or campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–24 in 3–5 series, in spirals or weak vertical ranks, mostly tan, green to brown subapical patch often present, midnerves usually evident (at least distally), linear-oblong, lanceolate to elliptic or obovate to spatulate, 1–5 × 0.5–1.2 mm, unequal, chartaceous, margins scarious, eciliate or ciliolate to erose-ciliolate, flat or convex, sometimes weakly keeled, apices acute to obtuse or rounded, sometimes apiculate, flat, faces glabrous or puberulent.</text>
      <biological_entity id="o20093" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o20094" from="12" name="quantity" src="d0_s8" to="24" />
        <character is_modifier="false" modifier="mostly" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="brown" />
      </biological_entity>
      <biological_entity id="o20094" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o20095" name="spiral" name_original="spirals" src="d0_s8" type="structure" />
      <biological_entity id="o20096" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s8" value="weak" value_original="weak" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o20097" name="patch" name_original="patch" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o20098" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s8" value="evident" value_original="evident" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic or obovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o20099" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="ciliolate" name="pubescence" src="d0_s8" to="erose-ciliolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o20100" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse or rounded" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20101" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o20093" id="r1866" name="in" negation="false" src="d0_s8" to="o20095" />
      <relation from="o20093" id="r1867" name="in" negation="false" src="d0_s8" to="o20096" />
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets (3–) 4–5 (–14);</text>
      <biological_entity id="o20102" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s9" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="14" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–6.5 mm, lobes 0.7–1.7 mm;</text>
      <biological_entity id="o20103" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20104" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style-branches 2.2–3.2 mm (exserted beyond spreading corolla lobes), appendages 0.8–1.5 mm (length shorter than stigmatic portion).</text>
      <biological_entity id="o20105" name="branch-style" name_original="style-branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="distance" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20106" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan to reddish-brown, turbinate, 2.5–4.2 mm, ± 5-angled, moderately to densely hairy;</text>
      <biological_entity id="o20107" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4.2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="5-angled" value_original="5-angled" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tan, 3.5–6 mm.</text>
      <biological_entity id="o20108" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nebr., Nev., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Yellow or sticky-leaf rabbitbrush</other_name>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves flat, glabrous; corollas 3.5–4.5 mm; nc Arizona</description>
      <determination>9c Chrysothamnus viscidiflorus subsp. planifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves twisted or hairy, or corollas 3.5–6.5 mm; w United States</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal stems, and frequently leaves, hairy</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrous; leaves glabrous, margins ciliate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems greenish, hirtellous to puberulent; leaves green, 3- or 5-nerved, 2–6 mm wide, abaxial faces hirsute to hirtellous, adaxial usually glabrous</description>
      <determination>9b Chrysothamnus viscidiflorus subsp. lanceolatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems and leaves grayish green, densely puberulent; leaves 1–2(–4) mm wide; 1-nerved (sometimes 3-nerved proximally)</description>
      <determination>9d Chrysothamnus viscidiflorus subsp. puberulus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves 0.5–1 mm wide; florets 3–4(–5); involucres ± turbinate</description>
      <determination>9a Chrysothamnus viscidiflorus subsp. axillaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves 1–10 mm wide; if 1 mm wide, involucres narrowly cylindric, and florets 4–14</description>
      <determination>9e Chrysothamnus viscidiflorus subsp. viscidiflorus</determination>
    </key_statement>
  </key>
</bio:treatment>