<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(A. Gray) Shinners" date="1951" rank="species">stenophylla</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>19: 68. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species stenophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416650</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Hooker) de Candolle" date="unknown" rank="species">hispida</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">stenophylla</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 223. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species hispida;variety stenophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">stenophylla</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species stenophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Pursh) Nuttall ex de Candolle" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">stenophylla</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species villosa;variety stenophylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 18–46 (–65) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, sometimes spreading by rhizomes, forming additional clumps.</text>
      <biological_entity id="o6406" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="46" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s0" to="46" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character constraint="by rhizomes" constraintid="o6407" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o6407" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o6408" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="additional" value_original="additional" />
      </biological_entity>
      <relation from="o6407" id="r580" name="forming" negation="false" src="d0_s1" to="o6408" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–50+, ascending to erect (sometimes brown, or reddish to dark-brown, often proximally brittle), often short-branched in distal 1/2, sparsely to densely long-hispid and strigose (hairs often broken off, especially proximally), sometimes becoming moderately hairy and densely stipitate-glandular distally (axillary fascicles of leaves often present).</text>
      <biological_entity id="o6409" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="50" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character constraint="in distal 1/2" constraintid="o6410" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="short-branched" value_original="short-branched" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" notes="" src="d0_s2" value="long-hispid" value_original="long-hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes becoming moderately" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6410" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves generally ascending, congested;</text>
      <biological_entity id="o6411" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="generally" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal cauline petiolate, blades linear-oblanceolate, (15–) 24–42 × (2–) 3–6 (–8.5) mm, bases cuneate to attenuate, margins flat, entire, strigoso-ciliate, proximally long-hispido-strigose, apices acute, faces sparsely to moderately hispido-strigose and sparsely to moderately stipitate-glandular, or moderately to densely hispido-strigose and eglandular;</text>
      <biological_entity constraint="proximal cauline" id="o6412" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o6413" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s4" to="24" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="24" from_unit="mm" name="length" src="d0_s4" to="42" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6414" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <biological_entity id="o6415" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="strigoso-ciliate" value_original="strigoso-ciliate" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s4" value="long-hispido-strigose" value_original="long-hispido-strigose" />
      </biological_entity>
      <biological_entity id="o6416" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6417" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character char_type="range_value" from="stipitate-glandular or" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s4" to="moderately densely hispido-strigose" />
        <character char_type="range_value" from="stipitate-glandular or" name="pubescence" src="d0_s4" to="moderately densely hispido-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal sessile (bright green to grayish green), blades linear-oblanceolate to narrowly oblanceolate (rarely broader), (9–) 16.5–30 × (1.5–) 2.2–4.3 (–6) mm, usually little reduced distally (often surpassing heads), bases cuneate to attenuate, margins flat, often abundantly long-hispido-strigose along whole, apices acute, sometime mucronulate to white-spinulose, faces sparsely to moderately densely hispido-strigose (2–65 hairs/mm²; hairs often scabrous, ± pustulate), usually eglandular, sometimes sparsely to densely stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o6418" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6419" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s5" to="narrowly oblanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s5" to="16.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16.5" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s5" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s5" to="4.3" to_unit="mm" />
        <character is_modifier="false" modifier="usually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6420" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
      <biological_entity id="o6421" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character constraint="along whole" constraintid="o6422" is_modifier="false" modifier="often abundantly" name="pubescence" src="d0_s5" value="long-hispido-strigose" value_original="long-hispido-strigose" />
      </biological_entity>
      <biological_entity id="o6422" name="whole" name_original="whole" src="d0_s5" type="structure" />
      <biological_entity id="o6423" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character char_type="range_value" from="mucronulate" modifier="sometime" name="shape" src="d0_s5" to="white-spinulose" />
      </biological_entity>
      <biological_entity id="o6424" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately densely" name="pubescence" src="d0_s5" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes sparsely; sparsely to densely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (1–) 2–16 (–30), in corymbiform arrays, branches ascending.</text>
      <biological_entity id="o6425" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="30" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="16" />
      </biological_entity>
      <biological_entity id="o6426" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity id="o6427" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o6425" id="r581" name="in" negation="false" src="d0_s6" to="o6426" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–16 mm, sparsely to densely hispido-strigose, eglandular or sparsely to densely stipitate-glandular;</text>
      <biological_entity id="o6428" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 1–3, proximal linear-oblanceolate to oblanceolate, sometimes little reduced distally, leaflike.</text>
      <biological_entity id="o6429" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6430" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s8" to="oblanceolate" />
        <character is_modifier="false" modifier="sometimes; distally" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindro-turbinate to narrowly campanulate, (4.5–) 5.5–8 (–11) mm.</text>
      <biological_entity id="o6431" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="cylindro-turbinate" name="shape" src="d0_s9" to="narrowly campanulate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 5–6 series, unequal (outer 1/5–1/4 length of inner), narrowly triangular-lanceolate to lanceolate, margins scarious, faces sparsely to moderately strigose, sparsely to densely stipitate-glandular.</text>
      <biological_entity id="o6432" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="narrowly triangular-lanceolate" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o6433" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o6434" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o6435" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="strigose" modifier="moderately" name="pubescence" src="d0_s10" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s10" to="sparsely densely stipitate-glandular" />
      </biological_entity>
      <relation from="o6432" id="r582" name="in" negation="false" src="d0_s10" to="o6433" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–24 (–36);</text>
      <biological_entity id="o6436" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="24" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="36" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae (5–) 7–11.5 (–16.5) × 0.7–1.5 (–2.1) mm.</text>
      <biological_entity id="o6437" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s12" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="16.5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (12–) 23–55 (–70);</text>
      <biological_entity id="o6438" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s13" to="23" to_inclusive="false" />
        <character char_type="range_value" from="55" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="70" />
        <character char_type="range_value" from="23" name="quantity" src="d0_s13" to="55" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas slightly ampliate, 4.5–7 mm, throats glabrous, lobes 0.4–0.9 mm, lobes sparsely pilose (hairs on lobes 0.1–0.25 mm).</text>
      <biological_entity id="o6439" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s14" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6440" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6441" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6442" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae monomorphic, obconic, compressed, 1.5–2.5 (–3) mm, ribs 6–10 (sometimes brownish), faces sparsely to moderately strigose;</text>
      <biological_entity id="o6443" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6444" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s15" to="10" />
      </biological_entity>
      <biological_entity id="o6445" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi off-white, outer of linear scales 0.25–0.5 mm, inner of 25–40 bristles (4–) 5–6.5 (–7.5) mm, longest weakly clavate.</text>
      <biological_entity id="o6446" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="off-white" value_original="off-white" />
      </biological_entity>
      <biological_entity id="o6447" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6449" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s16" to="40" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="6.5" to_unit="mm" />
      </biological_entity>
      <relation from="o6446" id="r583" name="outer of" negation="false" src="d0_s16" to="o6447" />
      <relation from="o6448" id="r584" name="consist_of" negation="false" src="d0_s16" to="o6449" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 18, 36.</text>
      <biological_entity constraint="inner" id="o6448" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s16" value="longest" value_original="longest" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6450" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
        <character name="quantity" src="d0_s17" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Iowa, Kans., N.Mex., Nebr., Okla., S.Dak., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Stiffleaf goldenaster</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Heterotheca stenophylla is divided into two varieties that differ in gland and hair density. Their ranges overlap to a great extent, and intermediates occur throughout the range. Both H. stenophylla and H. canescens often have numerous, overlapping leaves and often bear axillary fascicles of leaves.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal mid stems glabrate to sparsely hispido-strigose, moderately to densely stipitate- glandular; leaf faces sparsely to moderately hispido-strigose, moderately to densely stipitate-glandular (appearing dark to bright green), hair bases often broad (pustulate); phyllary faces usually sparsely strigose and stipitate-glandular</description>
      <determination>12a Heterotheca stenophylla var. stenophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal mid stems moderately to densely hispido-strigose, eglandular or sparsely glandular; leaf faces moderately to densely hispido-strigose, eglandular or sparsely stipitate-glandular beneath hairs (appearing pale green to gray-green), hairs rarely broad-based, pustulate; phyllary faces usually moderately strigose, eglandular</description>
      <determination>12b Heterotheca stenophylla var. angustifolia</determination>
    </key_statement>
  </key>
</bio:treatment>