<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
    <other_info_on_meta type="illustration_page">304</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">scorzonera</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 791. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus scorzonera;species laciniata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250005031</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials (monocarpic).</text>
      <biological_entity id="o15423" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 70–200 × (5–) 20–40 mm, margins of basal usually pinnately lobed, lobes ± linear.</text>
      <biological_entity id="o15426" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s1" to="200" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s1" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15427" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o15428" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="true" modifier="usually pinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o15427" id="r1408" name="part_of" negation="false" src="d0_s1" to="o15428" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres 7–20 × 6–12+ mm.</text>
      <biological_entity id="o15429" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries deltate to lanceolate, ± arachnose.</text>
      <biological_entity id="o15430" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 8–17 mm (± stipitate);</text>
      <biological_entity id="o15431" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 8–17 mm. 2n = 14.</text>
      <biological_entity id="o15432" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15433" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Colo., Kans., Mont., Nebr., N.Mex., Tex., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>