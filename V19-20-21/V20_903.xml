<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">dieteria</taxon_name>
    <taxon_name authority="Torrey in W. H. Emory" date="1848" rank="species">asteroides</taxon_name>
    <taxon_name authority="(B. L. Turner) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="variety">glandulosa</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1393. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus dieteria;species asteroides;variety glandulosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068248</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(Torrey) Greene" date="unknown" rank="species">asteroides</taxon_name>
    <taxon_name authority="B. L. Turner" date="unknown" rank="variety">glandulosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>60: 77. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Machaeranthera;species asteroides;variety glandulosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous or densely stipitate-glandular.</text>
      <biological_entity id="o15498" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: mid-blades 6–20 mm wide, margins usually serrate, faces sometimes puberulent, stiffly long-stipitate-glandular.</text>
      <biological_entity id="o15499" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15500" name="mid-blade" name_original="mid-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15501" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15502" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="stiffly" name="pubescence" src="d0_s1" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres hemispheric, 7–13 mm.</text>
      <biological_entity id="o15503" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary apices acuminate to long-acuminate, 2–5 mm, faces sometimes canescent, stipitate-glandular.</text>
      <biological_entity constraint="phyllary" id="o15504" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="long-acuminate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>2n = 8.</text>
      <biological_entity id="o15505" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15506" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, creosote bush scrublands, and pine/oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="creosote bush scrublands" />
        <character name="habitat" value="pine\/oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  
</bio:treatment>