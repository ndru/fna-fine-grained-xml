<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">parryi</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">aspera</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 88. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species parryi;variety aspera</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068298</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">asper</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 80. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species asper;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">asper</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species parryi;subspecies asper;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–70 cm.</text>
      <biological_entity id="o615" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves moderately crowded, gray-green;</text>
      <biological_entity id="o616" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 1-nerved, oblanceolate, 15–50 × 1–3 mm, faces abundantly stipitate-glandular;</text>
      <biological_entity id="o617" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o618" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abundantly" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distalmost ± equaling arrays.</text>
      <biological_entity constraint="distalmost" id="o619" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 4–10+ in (sometimes branched) racemiform arrays.</text>
      <biological_entity id="o620" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o621" from="4" name="quantity" src="d0_s4" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o621" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 11–15 mm.</text>
      <biological_entity id="o622" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 9–13, whitish, tan, or purplish, apices erect to spreading, acute.</text>
      <biological_entity id="o623" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="13" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o624" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5–10;</text>
      <biological_entity id="o625" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 8.3–9.5 mm, tubes sparsely hairy, throats gradually dilated, lobes 1.4–1.7 mm. 2n = 18.</text>
      <biological_entity id="o626" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8.3" from_unit="mm" name="some_measurement" src="d0_s8" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o627" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o628" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o629" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o630" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry forests to alpine barrens in pumice or gravel</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry forests" />
        <character name="habitat" value="to alpine barrens" constraint="in pumice or gravel" />
        <character name="habitat" value="pumice" modifier="in" />
        <character name="habitat" value="gravel" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27b.</number>
  <other_name type="past_name">aspra</other_name>
  <other_name type="common_name">Rough rabbitbrush</other_name>
  
</bio:treatment>