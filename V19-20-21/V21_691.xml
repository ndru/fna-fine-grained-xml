<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
    <other_info_on_meta type="illustration_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(Brandegee) B. G. Baldwin" date="1999" rank="species">clementina</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 468. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species clementina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066463</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="Brandegee" date="unknown" rank="species">clementina</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>7: 70. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species clementina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 15–80 cm.</text>
      <biological_entity id="o19331" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid (floccose in some leaf-axils).</text>
      <biological_entity id="o19333" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades toothed or entire, faces pubescent to somewhat canescent, ± hirsute, strigose, or scabrous, often stipitate-glandular.</text>
      <biological_entity id="o19334" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o19335" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19336" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually in crowded, corymbiform or paniculiform arrays.</text>
      <biological_entity id="o19337" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o19338" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o19337" id="r1316" name="in" negation="false" src="d0_s3" to="o19338" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually overlapping proximal 0–1/2+ of each involucre.</text>
      <biological_entity id="o19339" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o19341" from="0" name="quantity" src="d0_s4" to="1/2" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19340" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o19341" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o19339" id="r1317" name="subtending" negation="false" src="d0_s4" to="o19340" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± evenly stipitate-glandular (sometimes sparsely), including margins and apices, often with nonglandular, pustule-based or non-pustule-based hairs as well.</text>
      <biological_entity id="o19342" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="pustule-based" value_original="pustule-based" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <biological_entity id="o19343" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o19344" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o19345" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o19342" id="r1318" name="including" negation="false" src="d0_s5" to="o19343" />
      <relation from="o19342" id="r1319" name="including" negation="false" src="d0_s5" to="o19344" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 2 series (1 between ray and disc-florets, 1 between outermost and adjacent disc-florets).</text>
      <biological_entity id="o19346" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o19347" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <relation from="o19346" id="r1320" name="in" negation="false" src="d0_s6" to="o19347" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (11–) 13 (–20);</text>
      <biological_entity id="o19348" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s7" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="20" />
        <character name="quantity" src="d0_s7" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 4.5–7 mm.</text>
      <biological_entity id="o19349" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 18–30, all or mostly functionally staminate;</text>
      <biological_entity id="o19350" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s9" to="30" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers reddish to dark purple.</text>
      <biological_entity id="o19351" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi of 7–10 (–15) lance-linear, fimbriate scales 1–3 mm. 2n = 24.</text>
      <biological_entity id="o19352" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o19353" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="15" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s11" to="10" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s11" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="true" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19354" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
      <relation from="o19352" id="r1321" name="consist_of" negation="false" src="d0_s11" to="o19353" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas, coastal scrub, semi-barren sites, edges of salt marshes, on terraces, slopes, and ridges, often in rocky or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="semi-barren sites" />
        <character name="habitat" value="edges" constraint="of salt marshes" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="rocky" modifier="often" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Deinandra clementina is known only from six of the Channel Islands (Anacapa, San Clemente, San Nicolas, Santa Barbara, Santa Cruz, and Santa Catalina).</discussion>
  
</bio:treatment>