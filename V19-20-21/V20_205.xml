<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">sericocarpus</taxon_name>
    <taxon_name authority="Lindley in W. J. Hooker" date="1834" rank="species">rigidus</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker, Fl. Bor. Amer.</publication_title>
      <place_in_publication>2: 14. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus sericocarpus;species rigidus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067522</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">curtus</taxon_name>
    <taxon_hierarchy>genus Aster;species curtus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 19–37 cm.</text>
      <biological_entity id="o23132" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="19" from_unit="cm" name="some_measurement" src="d0_s0" to="37" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, puberulent.</text>
      <biological_entity id="o23133" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and proximalmost cauline withering by flowering;</text>
      <biological_entity id="o23134" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline sessile;</text>
      <biological_entity constraint="cauline" id="o23135" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obovate, 10–60 × 3–9 mm, margins entire, apices acute, distal acuminate, faces puberulent.</text>
      <biological_entity id="o23136" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23137" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23138" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23139" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o23140" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 2–3 per branch, in compact corymbiform arrays.</text>
      <biological_entity id="o23141" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per branch" constraintid="o23142" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o23142" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o23143" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o23141" id="r2139" name="in" negation="false" src="d0_s5" to="o23143" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncle bracts ovate, puberulent.</text>
      <biological_entity constraint="peduncle" id="o23144" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 6–9 mm.</text>
      <biological_entity id="o23145" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–4 series, outer 3–5 mm, mid 5–7 mm, puberulent.</text>
      <biological_entity id="o23146" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o23147" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o23148" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="mid" id="o23149" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o23146" id="r2140" name="in" negation="false" src="d0_s8" to="o23147" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 1–2;</text>
      <biological_entity id="o23150" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla-tubes 2–4 mm, laminae 2–3 mm.</text>
      <biological_entity id="o23151" name="corolla-tube" name_original="corolla-tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23152" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 9–17;</text>
      <biological_entity id="o23153" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla-tubes 4–6 mm, lobes 0.6–1 mm.</text>
      <biological_entity id="o23154" name="corolla-tube" name_original="corolla-tubes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23155" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Ovaries fusiform-obconic, 1–2 mm, strigose;</text>
      <biological_entity id="o23156" name="ovary" name_original="ovaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="fusiform-obconic" value_original="fusiform-obconic" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi: inner series 6–7 mm.</text>
      <biological_entity id="o23157" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity constraint="inner" id="o23158" name="series" name_original="series" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairie habitats, dry pastures, dry grassy Garry oak forests with rocky outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairie habitats" />
        <character name="habitat" value="dry pastures" />
        <character name="habitat" value="dry grassy garry oak forests" constraint="with rocky outcrops" />
        <character name="habitat" value="rocky outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Columbian white-topped aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sericocarpus rigidus grows on the southern part of Vancouver Island, British Columbia, and in scattered locations to the south end of the Puget Sound area in Washington. It is rare throughout its range and is listed as threatened in Canada, as Species of Concern by the U. S. Fish and Wildlife Service, as Sensitive in Washington, and as Threatened in Oregon. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>