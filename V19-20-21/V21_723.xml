<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="1999" rank="genus">KYHOSIA</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 465. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus KYHOSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Donald William Kyhos, b. 1929, Californian botanist</other_info_on_name>
    <other_info_on_name type="fna_id">316913</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–120 cm (rhizomatous; self-incompatible).</text>
      <biological_entity id="o4126" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (aerial) erect.</text>
      <biological_entity id="o4127" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline (at flowering);</text>
      <biological_entity id="o4128" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite (basalmost in rosettes), distal alternate;</text>
      <biological_entity constraint="proximal" id="o4129" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="distal" id="o4130" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lance-linear to linear, margins entire, faces hirsute and (distal leaves) glandular-hirtellous.</text>
      <biological_entity id="o4131" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape_or_course" src="d0_s5" value="lance-linear to linear" value_original="lance-linear to linear" />
      </biological_entity>
      <biological_entity id="o4132" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4133" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hirtellous" value_original="glandular-hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, ± corymbiform arrays.</text>
      <biological_entity id="o4134" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in loose , more or less corymbiform arrays" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , more or less corymbiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0.</text>
      <biological_entity constraint="peduncular" id="o4135" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o4136" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o4137" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o4138" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± campanulate to hemispheric, 6–12+ mm diam.</text>
      <biological_entity id="o4139" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s8" to="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 8–12 in 1 series (each mostly or wholly enveloping a ray ovary, lanceolate to lance-linear, herbaceous, abaxially hirsute and glandular-hirtellous).</text>
      <biological_entity id="o4140" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4141" from="8" name="quantity" src="d0_s9" to="12" />
      </biological_entity>
      <biological_entity id="o4141" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, glabrous, paleate (paleae falling, in 1 series between rays and discs, weakly connate or distinct).</text>
      <biological_entity id="o4142" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 8–12, pistillate, fertile;</text>
      <biological_entity id="o4143" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas bright-yellow.</text>
      <biological_entity id="o4144" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 28–65, bisexual and fertile;</text>
      <biological_entity id="o4145" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s13" to="65" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas bright-yellow, pubescent, tubes shorter than funnelform throats, lobes 5, deltate (anthers ± dark purple; styles glabrous proximal to branches).</text>
      <biological_entity id="o4146" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4147" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o4148" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4148" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o4149" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ray cypselae (black) compressed, clavate (arcuate, basal attachments centered, apices beakless, faces glabrous or hispidulous);</text>
      <biological_entity constraint="ray" id="o4150" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0, or coroniform.</text>
      <biological_entity id="o4151" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Disc cypselae (brown to black) ± terete (straight or arcuate, faces hispidulous, otherwise similar to rays);</text>
      <biological_entity constraint="disc" id="o4152" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi of 5–10 (stramineous to purplish) lanceolate to subulate, ciliate to plumose scales.</text>
      <biological_entity id="o4154" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s18" to="10" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s18" to="subulate ciliate" />
      </biological_entity>
      <relation from="o4153" id="r325" name="consist_of" negation="false" src="d0_s18" to="o4154" />
    </statement>
    <statement id="d0_s19">
      <text>x = 6.</text>
      <biological_entity id="o4153" name="pappus" name_original="pappi" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o4155" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>344.</number>
  <discussion>Species 1.</discussion>
  <discussion>Kyhosia has been treated as congeneric with Madia, which is evidently more closely related to the Hawaiian silversword alliance (Argyroxiphium, Dubautia, and Wilkesia) than to Kyhosia (B. G. Baldwin 1996). Head architecture is similar in Kyhosia and Argyroxiphium, as noted by S. Carlquist (1959). Kyhosia is the only perennial tarweed with 2n = 12.</discussion>
  
</bio:treatment>