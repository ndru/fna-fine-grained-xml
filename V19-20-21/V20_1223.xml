<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="treatment_page">554</other_info_on_meta>
    <other_info_on_meta type="illustration_page">550</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="species">atratus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 105. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species atratus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067475</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">atratus</taxon_name>
    <taxon_name authority="(Greene) Greenman" date="unknown" rank="variety">milleflorus</taxon_name>
    <taxon_hierarchy>genus Senecio;species atratus;variety milleflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">milleflorus</taxon_name>
    <taxon_hierarchy>genus Senecio;species milleflorus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (20–) 35–70 (–80+) cm (rhizomes or caudices branched, erect to weakly creeping).</text>
      <biological_entity id="o11520" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage floccose-tomentose to canescent, sometimes unevenly glabrescent.</text>
      <biological_entity id="o11521" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character char_type="range_value" from="floccose-tomentose" name="pubescence" src="d0_s1" to="canescent" />
        <character is_modifier="false" modifier="sometimes unevenly" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1– (2–5).</text>
      <biological_entity id="o11522" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves progressively reduced distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o11523" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblong-ovate to oblanceolate, (5–) 10–30 × 1.5–4 (–6) cm, bases tapered, margins dentate (denticles, dark, callous; mid leaves similar, sessile, smaller; distal leaves bractlike).</text>
      <biological_entity id="o11524" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s5" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11525" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o11526" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 20–60+ in corymbiform or subpaniculiform arrays.</text>
      <biological_entity id="o11527" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in corymbiform or subpaniculiform arrays" from="20" name="quantity" src="d0_s6" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 2–5 linear bractlets (lengths to 1/3 phyllaries).</text>
      <biological_entity id="o11528" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o11529" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o11528" id="r1056" name="consist_of" negation="false" src="d0_s7" to="o11529" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries (± 5) ± 8, 6–8 mm, tips black.</text>
      <biological_entity id="o11530" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s8" value="5" value_original="5" />
        <character modifier="more or less" name="quantity" src="d0_s8" value="8" value_original="8" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11531" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets (± 3) ± 5;</text>
      <biological_entity id="o11532" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s9" value="3" value_original="3" />
        <character modifier="more or less" name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 5–8 mm.</text>
      <biological_entity constraint="corolla" id="o11533" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity id="o11534" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11535" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or drying, rocky or sandy sites in coniferous areas, especially sites with frequent disturbance</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" modifier="dry or drying" constraint="in coniferous areas , especially sites with frequent disturbance" />
        <character name="habitat" value="sandy sites" constraint="in coniferous areas , especially sites with frequent disturbance" />
        <character name="habitat" value="coniferous areas" />
        <character name="habitat" value="sites" constraint="with frequent disturbance" />
        <character name="habitat" value="frequent disturbance" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2800–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="2800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  
</bio:treatment>