<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lactuca</taxon_name>
    <taxon_name authority="(Nuttall) Riddell" date="1835" rank="species">ludoviciana</taxon_name>
    <place_of_publication>
      <publication_title>W. J. Med. Phys. Sci.</publication_title>
      <place_in_publication>8: 491. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lactuca;species ludoviciana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416716</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sonchus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">ludovicianus</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 125. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sonchus;species ludovicianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lactuca</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">campestris</taxon_name>
    <taxon_hierarchy>genus Lactuca;species campestris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, 15–150 cm.</text>
      <biological_entity id="o9331" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves on proximal 1/2–3/4 of each stem;</text>
      <biological_entity id="o9332" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o9333" name="1/2-3/4" name_original="1/2-3/4" src="d0_s1" type="structure" />
      <biological_entity id="o9334" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o9332" id="r867" name="on" negation="false" src="d0_s1" to="o9333" />
      <relation from="o9333" id="r868" name="part_of" negation="false" src="d0_s1" to="o9334" />
    </statement>
    <statement id="d0_s2">
      <text>blades of undivided cauline leaves obovate or oblanceolate to spatulate, margins denticulate (piloso-ciliate), midribs usually piloso-setose.</text>
      <biological_entity id="o9335" name="blade" name_original="blades" src="d0_s2" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="spatulate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9336" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o9337" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o9338" name="midrib" name_original="midribs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="piloso-setose" value_original="piloso-setose" />
      </biological_entity>
      <relation from="o9335" id="r869" name="part_of" negation="false" src="d0_s2" to="o9336" />
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o9339" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o9340" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o9339" id="r870" name="in" negation="false" src="d0_s3" to="o9340" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres 12–15+ mm.</text>
      <biological_entity id="o9341" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries usually reflexed in fruit.</text>
      <biological_entity id="o9342" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character constraint="in fruit" constraintid="o9343" is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o9343" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Florets 20–50+;</text>
      <biological_entity id="o9344" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas usually yellow, sometimes bluish, usually deliquescent.</text>
      <biological_entity id="o9345" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="bluish" value_original="bluish" />
        <character is_modifier="false" modifier="usually" name="architecture_or_texture" src="d0_s7" value="deliquescent" value_original="deliquescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae: bodies brown to blackish (usually mottled), ± flattened, elliptic, 4.5–5+ mm, beaks ± filiform, 2.5–4.5 mm, faces 1 (–3) -nerved;</text>
      <biological_entity id="o9346" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity id="o9347" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s8" to="blackish" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9348" name="beak" name_original="beaks" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9349" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1(-3)-nerved" value_original="1(-3)-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi white, 5–7 (–11) mm.</text>
      <biological_entity id="o9350" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 34.</text>
      <biological_entity id="o9351" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9352" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in woods, stream banks, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in woods , stream banks ," />
        <character name="habitat" value="woods" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., Sask.; Ariz., Ark., Calif., Colo., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Minn., Mont., Nebr., N.Mex., N.Dak., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>