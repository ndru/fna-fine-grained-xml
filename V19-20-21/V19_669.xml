<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">antennaria</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="species">media</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 286. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus antennaria;species media</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066080</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="(Linnaeus) Gaertner" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">media</taxon_name>
    <taxon_hierarchy>genus Antennaria;species alpina;variety media;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="E. E. Nelson" date="unknown" rank="species">austromontana</taxon_name>
    <taxon_hierarchy>genus Antennaria;species austromontana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">candida</taxon_name>
    <taxon_hierarchy>genus Antennaria;species candida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">densa</taxon_name>
    <taxon_hierarchy>genus Antennaria;species densa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">modesta</taxon_name>
    <taxon_hierarchy>genus Antennaria;species modesta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="E. E. Nelson" date="unknown" rank="species">mucronata</taxon_name>
    <taxon_hierarchy>genus Antennaria;species mucronata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Dioecious or gynoecious (staminate plants rare or in equal frequency to pistillates, respectively).</text>
      <biological_entity id="o7917" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynoecious" value_original="gynoecious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Plants 5–13 cm.</text>
      <biological_entity id="o7918" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="13" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stolons 1–4 cm.</text>
      <biological_entity id="o7919" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 1-nerved, spatulate to oblanceolate, 6–19 × 2.5–6 mm, tips mucronate, faces gray-pubescent.</text>
      <biological_entity constraint="basal" id="o7920" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="19" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7921" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o7922" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="gray-pubescent" value_original="gray-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves linear, 5–20 mm, not flagged (apices acute).</text>
      <biological_entity constraint="cauline" id="o7923" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="flagged" value_original="flagged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 2–5 (–9) in corymbiform arrays.</text>
      <biological_entity id="o7924" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" constraint="in arrays" constraintid="o7925" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o7925" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres: staminate (3.5–) 4.5–6.5 mm;</text>
      <biological_entity id="o7926" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 4–8 mm.</text>
      <biological_entity id="o7927" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries distally dark-brown, black, or olivaceous.</text>
      <biological_entity id="o7928" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="olivaceous" value_original="olivaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas: staminate 2.5–4.5 mm;</text>
      <biological_entity id="o7929" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate 3–4.5 mm.</text>
      <biological_entity id="o7930" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 0.6–1.6 mm, glabrous or papillate;</text>
      <biological_entity id="o7931" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: staminate 2.5–4.5 mm;</text>
      <biological_entity id="o7932" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistillate 4–5.5 mm. 2n = 56, 98, 112.</text>
      <biological_entity id="o7933" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7934" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="56" value_original="56" />
        <character name="quantity" src="d0_s13" value="98" value_original="98" />
        <character name="quantity" src="d0_s13" value="112" value_original="112" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky to moist alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" modifier="dry" />
        <character name="habitat" value="rocky to moist alpine tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Rocky Mountain pussytoes</other_name>
  <discussion>Antennaria media ranges from Arizona to Alaska; dioecious and gynoecious populations are encountered (R. J. Bayer and G. L. Stebbins 1987). The dioecious (sexual) populations are restricted primarily to California and Oregon (Bayer et al. 1990). The main distinction between A. media and A. alpina is flags on distal cauline leaves present in A. alpina and mostly absent in A. media (Bayer 1990d). Phyllaries of the pistillate plants in A. alpina tend to be acute; they are blunter in A. media. At some point, it may be preferable to follow W. L. Jepson ([1923–1925]) and some later authors and treat A. media as a subspecies of A. alpina. Antennaria media appears to be an autopolyploid derivative of A. pulchella; genes from A. pulchella may have introgressed into the A. alpina and A. parvifolia complexes indirectly through A. media.</discussion>
  
</bio:treatment>