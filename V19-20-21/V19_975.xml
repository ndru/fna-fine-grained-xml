<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="mention_page">558</other_info_on_meta>
    <other_info_on_meta type="treatment_page">557</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">LEUCANTHEMUM</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 2. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus LEUCANTHEMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leuco- , white, and anthemon, flower</other_info_on_name>
    <other_info_on_name type="fna_id">118354</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (10–) 40–130 (–200+) cm (rhizomatous, roots usually red-tipped).</text>
      <biological_entity id="o19330" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="130" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect, simple or branched, glabrous or hairy (hairs basifixed).</text>
      <biological_entity id="o19331" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal or basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o19332" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obovate to lanceolate or linear, often 1 [–2+] -pinnately lobed or toothed, ultimate margins dentate or entire, faces glabrous or sparsely hairy.</text>
      <biological_entity id="o19333" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="lanceolate or linear" />
        <character is_modifier="false" modifier="often 1[-2+]-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19334" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19335" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually radiate, rarely discoid, borne singly or in 2s or 3s.</text>
      <biological_entity id="o19336" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character constraint="in 2s; in 2s" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in 2s" value_original="in 2s" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric or broader, 12–35+ mm diam.</text>
      <biological_entity id="o19337" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s6" to="35" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries persistent, 35–60+ in 3–4+ series, distinct, ovate or lanceovate to oblanceolate, unequal, margins and apices (colorless or pale to dark-brown) scarious (tips not notably dilated; abaxial faces glabrous or sparsely hairy).</text>
      <biological_entity id="o19338" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o19339" from="35" name="quantity" src="d0_s7" to="60" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s7" to="oblanceolate" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o19339" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19340" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o19341" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles convex, epaleate.</text>
      <biological_entity id="o19342" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets usually 13–34+, rarely 0, pistillate, fertile;</text>
      <biological_entity id="o19343" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s9" to="34" upper_restricted="false" />
        <character modifier="rarely" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white (drying pinkish), laminae ovate to linear.</text>
      <biological_entity id="o19344" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o19345" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 120–200+, bisexual, fertile;</text>
      <biological_entity id="o19346" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="120" name="quantity" src="d0_s11" to="200" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, tubes ± cylindric (proximally swollen, becoming spongy in fruit), throats campanulate, lobes 5, deltate (without resin sacs).</text>
      <biological_entity id="o19347" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o19348" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o19349" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o19350" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae ± columnar to obovoid, ribs ± 10, faces glabrous (pericarps with myxogenic cells on ribs and resin sacs between ribs; embryo-sac development monosporic);</text>
      <biological_entity id="o19351" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="less columnar" name="shape" src="d0_s13" to="obovoid" />
      </biological_entity>
      <biological_entity id="o19352" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o19353" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0 (wall tissue of ray cypselae sometimes produced as coronas or auricles on some cypselae).</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o19354" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o19355" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; mostly temperate Europe (some widely cultivated and sparingly adventive).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="mostly temperate Europe (some widely cultivated and sparingly adventive)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>137.</number>
  <discussion>Species 20–40+ (3 in the flora).</discussion>
  <discussion>The three leucanthemums recognized here are weakly distinct and are sometimes included (with a dozen or more others) in a single, polymorphic Leucanthemum vulgare.</discussion>
  <references>
    <reference>Vogt, R. 1991. Die Gattung Leucanthemum (Compositae–Anthemideae) auf der Iberischen Halbinsel. Ruizia 10: 1–261.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades of basal leaves usually pinnately lobed (lobes 3–7+) and/or irregularly toothed; margins of mid-stem leaves usually irregularly toothed proximally and distally</description>
      <determination>1 Leucanthemum vulgare</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades of basal leaves not lobed, usually toothed, rarely entire; margins of mid-stem leaves usually entire proximally, regularly serrate distally</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of cauline leaves oblanceolate to lanceolate or linear, 50–120+ × 8–22+ mm; larger phyllaries 2–3 mm wide; ray cypselae 2–3(–4 mm), apices usually bare, rarely adaxially auriculate</description>
      <determination>2 Leucanthemum maximum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of cauline leaves elliptic to oblanceolate, 30–120+ × 12–25(–35+) mm; larger phyllaries 4–5 mm wide; ray cypselae 3–4 mm, apices usually adaxially auriculate.</description>
      <determination>3 Leucanthemum lacustre</determination>
    </key_statement>
  </key>
</bio:treatment>