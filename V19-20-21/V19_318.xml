<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="1780" rank="genus">taraxacum</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="1780" rank="species">officinale</taxon_name>
    <place_of_publication>
      <publication_title>Prim. Fl. Holsat.,</publication_title>
      <place_in_publication>56. 1780</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus taraxacum;species officinale</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220013281</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leontodon</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">taraxacum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 798. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leontodon;species taraxacum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Taraxacum</taxon_name>
    <taxon_name authority="R. Doll" date="unknown" rank="species">officinale</taxon_name>
    <taxon_name authority="Blytt" date="unknown" rank="variety">palustre</taxon_name>
    <taxon_hierarchy>genus Taraxacum;species officinale;variety palustre;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Taraxacum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sylvanicum</taxon_name>
    <taxon_hierarchy>genus Taraxacum;species sylvanicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (1–) 5–40 (–60) cm;</text>
      <biological_entity id="o20471" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots seldom branched.</text>
      <biological_entity id="o20472" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, erect or ascending, sometimes ± purplish (usually equaling or surpassing leaves), glabrous or sparsely villous, slightly more so distally.</text>
      <biological_entity id="o20473" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes more or less" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 20+, horizontal to erect;</text>
      <biological_entity id="o20474" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s3" upper_restricted="false" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s3" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles ± narrowly winged;</text>
      <biological_entity id="o20475" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less narrowly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate, oblong, or obovate (often runcinate), (4–) 5–45 × (0.7–) 1–10 cm, bases attenuate to narrowly cuneate, margins usually shallowly to deeply lobed to lacerate or toothed, lobes retrorse, broadly to narrowly triangular to nearly lanceolate, acute to long-acuminate, terminals ± as large as distal laterals, ultimate margins toothed or entire (secondary lobules irregular, perpendicular to retrorse), teeth minute to pronounced apices acute to acuminate or obtuse, faces glabrous or sparsely villous (commonly on midveins).</text>
      <biological_entity id="o20476" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="45" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20477" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="narrowly cuneate" />
      </biological_entity>
      <biological_entity id="o20478" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="lobed" modifier="deeply" name="shape" src="d0_s5" to="lacerate or toothed" />
      </biological_entity>
      <biological_entity id="o20479" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="retrorse" value_original="retrorse" />
        <character char_type="range_value" from="triangular" modifier="broadly to narrowly; narrowly" name="shape" src="d0_s5" to="nearly lanceolate acute" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="nearly lanceolate acute" />
      </biological_entity>
      <biological_entity id="o20480" name="terminal" name_original="terminals" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o20481" name="lateral" name_original="laterals" src="d0_s5" type="structure" />
      <biological_entity constraint="ultimate" id="o20482" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20483" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o20484" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="pronounced" value_original="pronounced" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate or obtuse" />
      </biological_entity>
      <biological_entity id="o20485" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
      <relation from="o20480" id="r1849" name="as large as" negation="false" src="d0_s5" to="o20481" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 12–18, reflexed, sometimes ± glaucous, lanceolate bractlets in 2 series, 6–12 × 2.8–3.5 mm, margins very narrowly white-scarious, sometimes villous-ciliate distally, apices acuminate, hornless.</text>
      <biological_entity id="o20486" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" notes="" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" notes="" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20487" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s6" to="18" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" modifier="sometimes more or less" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o20488" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20489" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="texture" src="d0_s6" value="white-scarious" value_original="white-scarious" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_pubescence_or_shape" src="d0_s6" value="villous-ciliate" value_original="villous-ciliate" />
      </biological_entity>
      <biological_entity id="o20490" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hornless" value_original="hornless" />
      </biological_entity>
      <relation from="o20486" id="r1850" name="consist_of" negation="false" src="d0_s6" to="o20487" />
      <relation from="o20487" id="r1851" name="in" negation="false" src="d0_s6" to="o20488" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres green to dark green or brownish green, tips dark gray or purplish, campanulate, 14–25 mm.</text>
      <biological_entity id="o20491" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="dark green or brownish green" />
      </biological_entity>
      <biological_entity id="o20492" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 13–18 in 2 series, lanceolate, 2–2.8 mm wide, margins scarious (proximal 2/3) to narrowly scarious, apices acuminate, erose-scarious, usually hornless (seldom appendaged), callous.</text>
      <biological_entity id="o20493" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o20494" from="13" name="quantity" src="d0_s8" to="18" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20494" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20495" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="scarious" name="texture" src="d0_s8" to="narrowly scarious" />
      </biological_entity>
      <biological_entity id="o20496" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="texture" src="d0_s8" value="erose-scarious" value_original="erose-scarious" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="hornless" value_original="hornless" />
        <character is_modifier="false" name="texture" src="d0_s8" value="callous" value_original="callous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 40–100+;</text>
      <biological_entity id="o20497" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow (orange-yellow), 15–22 × 1.7–2 mm (outer).</text>
      <biological_entity id="o20498" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae olivaceous or olive-brown, or straw-colored to grayish, bodies oblanceoloid, (2–) 2.5–2.8 (–4) mm, cones shortly terete, 0.5–0.9 mm, beaks slender, 7–9 mm, ribs 4–12, sharp, faces proximally smooth to ± tuberculate, muricate in distal 1/3;</text>
      <biological_entity id="o20499" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="olive-brown or straw-colored" name="coloration" src="d0_s11" to="grayish" />
        <character char_type="range_value" from="olive-brown or straw-colored" name="coloration" src="d0_s11" to="grayish" />
      </biological_entity>
      <biological_entity id="o20500" name="body" name_original="bodies" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20501" name="cone" name_original="cones" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20502" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20503" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="12" />
        <character is_modifier="false" name="shape" src="d0_s11" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o20504" name="face" name_original="faces" src="d0_s11" type="structure">
        <character char_type="range_value" from="proximally smooth" name="relief" src="d0_s11" to="more or less tuberculate" />
        <character constraint="in distal 1/3" constraintid="o20505" is_modifier="false" name="relief" src="d0_s11" value="muricate" value_original="muricate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20505" name="1/3" name_original="1/3" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pappi white to sordid, 5–6 (–8) mm.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 24, 40, [16, 32].</text>
      <biological_entity id="o20506" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="sordid" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20507" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
        <character name="quantity" src="d0_s13" value="[16" value_original="[16" />
        <character name="quantity" src="d0_s13" value="32]" value_original="32]" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering nearly year-round (fall–spring, south; spring or summer, north).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="nearly" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" modifier="south" to="spring" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Often damp low places, lawns, roadsides, waste grounds, disturbed banks and shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low places" modifier="often damp" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste grounds" />
        <character name="habitat" value="disturbed banks" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="damp" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; also introduced in Mexico; introduced nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also  in Mexico" establishment_means="introduced" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Common dandelion</other_name>
  <other_name type="common_name">pissenlit officinal</other_name>
  <discussion>Taraxacum officinale is the most widespread dandelion in temperate North America, though its abundance decreases in the arid south. It is a familiar weed of lawns and roadsides. It is also the species most commonly used for medicinal and culinary purposes (e.g., E. Small and P. M. Catling 1999).</discussion>
  <discussion>Phenotypic and genotypic variation of this species have been studied in North America (L. M. King 1993; King and B. A. Schaal 1990; J. C. Lyman and N. C. Ellstrand 1998; O. T. Solbrig 1971; R. J. Taylor 1987), but results of those studies did not lead to the recognition of microspecies.</discussion>
  <discussion>Specimens of Taraxacum officinale with deeply lobed leaves are sometimes difficult to distinguish from those of T. erythrospermum when fruits are missing (see also R. J. Taylor 1987). Usually, however, early leaves of the former are much less deeply lobed than those of the latter, which are more consistently lacerate throughout development, though broadly winged initially. The two taxa are easily distinguished in fruit, the red cypselae of T. erythrospermum standing out from the dull olive ones of T. officinale.</discussion>
  <discussion>In northeastern North America, Taraxacum officinale and T. lapponicum often are confused, which has led to reports of the common dandelion farther north than I have been able to verify (it has yet to be collected from the Nunavik region of Quebec, for instance). The characters in the key above help separate the two taxa.</discussion>
  <discussion>The typification by A. J. Richards (1985) would leave the common dandelion of both Europe and North America without a valid name (J. Kirschner and J. Štepánek 1987). For the time being, with the nomenclatural situation still not resolved, I am following traditional usage of the name Taraxacum officinale.</discussion>
  
</bio:treatment>