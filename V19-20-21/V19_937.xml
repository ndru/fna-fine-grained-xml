<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">anthemis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cotula</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 894. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus anthemis;species cotula</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023151</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anthemis</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">foetida</taxon_name>
    <taxon_hierarchy>genus Anthemis;species foetida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaemelum</taxon_name>
    <taxon_name authority="(Linnaeus) Allioni" date="unknown" rank="species">cotula</taxon_name>
    <taxon_hierarchy>genus Chamaemelum;species cotula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maruta</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="unknown" rank="species">cotula</taxon_name>
    <taxon_hierarchy>genus Maruta;species cotula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (5–) 15–35 (–90) cm, usually ill-scented.</text>
      <biological_entity id="o7220" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="odor" src="d0_s0" value="ill-scented" value_original="ill-scented" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green (sometimes red-tinged), usually erect, branched mostly distally or ± throughout, glabrous, glabrate, puberulent, or sparsely strigillose to strigoso-sericeous (glabrescent, hairs mostly medifixed) and gland-dotted.</text>
      <biological_entity id="o7221" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly distally; distally; distally; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s1" to="strigoso-sericeous" />
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s1" to="strigoso-sericeous" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 25–55 × 15–30 mm, 1–2-pinnately lobed.</text>
      <biological_entity id="o7222" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="1-2-pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles mostly (2–) 4–6 (–15) cm.</text>
      <biological_entity id="o7223" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 5–9 mm diam., ± villosulous to arachnose.</text>
      <biological_entity id="o7224" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="less villosulous" name="pubescence" src="d0_s4" to="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles paleate mostly distally;</text>
      <biological_entity id="o7225" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly distally; distally" name="architecture" src="d0_s5" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>paleae subulate to acerose 2–3+ mm (often gland-dotted).</text>
      <biological_entity id="o7226" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s6" to="acerose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 10–15, styliferous and sterile;</text>
      <biological_entity id="o7227" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white, laminae 5–15+ mm.</text>
      <biological_entity id="o7228" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7229" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 2–2.5 mm (sparsely gland-dotted).</text>
      <biological_entity constraint="disc" id="o7230" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1.3–2 mm, ribs ± tuberculate (furrows often gland-dotted);</text>
      <biological_entity id="o7231" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7232" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0.2n = 18.</text>
      <biological_entity id="o7233" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7234" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, clearings, fields, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600(–1500+) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Mayweed</other_name>
  <other_name type="common_name">stinking chamomile</other_name>
  <other_name type="common_name">camomille maroute</other_name>
  <discussion>Anthemis cotula is a weed throughout North America.</discussion>
  
</bio:treatment>