<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
    <other_info_on_meta type="illustration_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1995" rank="genus">ampelaster</taxon_name>
    <taxon_name authority="(Walter) G. L. Nesom" date="1995" rank="species">carolinianus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 250. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ampelaster;species carolinianus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066059</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">carolinianus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>208. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species carolinianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="J. Jacquin ex Sprengel" date="unknown" rank="species">scandens</taxon_name>
    <taxon_hierarchy>genus Aster;species scandens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Symphyotrichum</taxon_name>
    <taxon_name authority="(Walter) Wunderlin &amp; B. F. Hansen" date="unknown" rank="species">carolinianum</taxon_name>
    <taxon_hierarchy>genus Symphyotrichum;species carolinianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Virgulus</taxon_name>
    <taxon_name authority="(Walter) Reveal &amp; Keener" date="unknown" rank="species">carolinianus</taxon_name>
    <taxon_hierarchy>genus Virgulus;species carolinianus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants deciduous to evergreen by production of new growth, sprawling, climbing over other plants.</text>
      <biological_entity id="o7014" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" constraint="by production" constraintid="o7015" from="deciduous" name="duration" src="d0_s0" to="evergreen" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character constraint="over plants" constraintid="o7017" is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7015" name="production" name_original="production" src="d0_s0" type="structure" />
      <biological_entity id="o7016" name="growth" name_original="growth" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o7017" name="plant" name_original="plants" src="d0_s0" type="structure" />
      <relation from="o7015" id="r636" name="part_of" negation="false" src="d0_s0" to="o7016" />
    </statement>
    <statement id="d0_s1">
      <text>Stems weak, 5–10 mm diam., branches at right angles.</text>
      <biological_entity id="o7018" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="weak" value_original="weak" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7019" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o7020" name="angle" name_original="angles" src="d0_s1" type="structure" />
      <relation from="o7019" id="r637" name="at" negation="false" src="d0_s1" to="o7020" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 30–70 × 10–15 (–20) mm, reduced distally, membranous, bases auriculate-clasping, apices acuminate.</text>
      <biological_entity id="o7021" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7022" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o7023" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–15 per branch.</text>
      <biological_entity id="o7024" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per branch" constraintid="o7025" from="1" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o7025" name="branch" name_original="branch" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–4 cm, densely pilose;</text>
      <biological_entity id="o7026" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate to ovate, 3–10 mm.</text>
      <biological_entity id="o7027" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets: laminae 9–15 (–20) × 1–1.6 mm;</text>
      <biological_entity id="o7028" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure" />
      <biological_entity id="o7029" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style-branch appendages narrowly triangular.</text>
      <biological_entity id="o7030" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure" />
      <biological_entity constraint="style-branch" id="o7031" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets corollas 6–8 mm, limbs 50–60% corollas, lobes 0.6–1.1 mm, 10–20% corollas.</text>
      <biological_entity constraint="disc-floret" id="o7032" name="corolla" name_original="corollas" src="d0_s8" type="structure" constraint_original="disc-florets">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7033" name="limb" name_original="limbs" src="d0_s8" type="structure" />
      <biological_entity id="o7034" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
      <biological_entity id="o7035" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" modifier="50 60%" name="some_measurement" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7036" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae usually tan to brown, sometimes mottled purple to black between light colored ribs, 3.5–4.3 mm;</text>
      <biological_entity id="o7037" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually tan" name="coloration" src="d0_s9" to="brown" />
        <character char_type="range_value" constraint="between ribs" constraintid="o7038" from="mottled purple" modifier="sometimes" name="coloration" src="d0_s9" to="black" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7038" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="light colored" value_original="light colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi shorter than disc corollas.</text>
      <biological_entity constraint="disc" id="o7040" name="corolla" name_original="corollas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o7039" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character constraint="than disc corollas" constraintid="o7040" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7041" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering peak late fall–winter, year round (Fla).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="year round (Fla)" to="winter" from="late fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy shores, stream banks, edges of swamps and moist thickets, wet woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy shores" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="edges" constraint="of swamps and moist thickets , wet woodlands" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="moist thickets" />
        <character name="habitat" value="wet woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Climbing aster</other_name>
  <discussion>Ampelaster carolinianus grows on the outer coastal plain. It is possibly extirpated in North Carolina.</discussion>
  
</bio:treatment>