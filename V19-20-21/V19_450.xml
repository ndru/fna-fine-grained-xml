<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tragopogon</taxon_name>
    <taxon_name authority="Ownbey" date="1950" rank="species">miscellus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>37: 498. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus tragopogon;species miscellus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067784</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–150+ cm.</text>
      <biological_entity id="o1843" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: apices usually recurved to coiled, faces initially floccose to tomentulose, soon glabrescent.</text>
      <biological_entity id="o1844" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1845" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity id="o1846" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="initially floccose" name="pubescence" src="d0_s1" to="tomentulose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles distally inflated.</text>
      <biological_entity id="o1847" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres conic to urceolate in bud.</text>
      <biological_entity id="o1848" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in bud" constraintid="o1849" from="conic" name="shape" src="d0_s3" to="urceolate" />
      </biological_entity>
      <biological_entity id="o1849" name="bud" name_original="bud" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Outer florets usually slightly shorter than phyllaries (see discussion for exception);</text>
      <biological_entity constraint="outer" id="o1850" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character constraint="than phyllaries" constraintid="o1851" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="usually slightly shorter" value_original="usually slightly shorter" />
      </biological_entity>
      <biological_entity id="o1851" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>corollas yellow.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 24.</text>
      <biological_entity id="o1852" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1853" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Idaho, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Hybrid or Ownbey’s goatsbeard</other_name>
  <other_name type="common_name">Moscow salsify</other_name>
  <discussion>Tragopogon miscellus has been reported from near Gardiner, Montana; it is no longer present there.</discussion>
  <discussion>Plants of Tragopogon miscellus are larger and more robust than those of T. pratensis. They are allote-traploids, formed (probably repeatedly) from hybrids between T. pratensis and T. dubius. Outer florets are shorter than the phyllaries except at some sites in Pullman, Washington, where outer florets equal or surpass the phyllaries. (The different inflorescence morphs result from reciprocal polyploid origins.) F1 hybrids between T. dubius and T. pratensis (= T. ×crantzii Dichtl) may resemble T. miscellus but are less robust, have low pollen stainability, and set few, if any, seeds. Tragopogon miscellus does not occur in Europe, but hybrids between T. dubius and T. pratensis occur occasionally.</discussion>
  
</bio:treatment>