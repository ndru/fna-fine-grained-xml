<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">debilis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">debilis</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species debilis;subspecies debilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068433</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent, glabrous or puberulent.</text>
      <biological_entity id="o642" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 3–10 cm, margins serrulate or shallowly, regularly serrate, abaxial faces sparsely, if at all, glanddotted.</text>
      <biological_entity id="o643" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="distance" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o644" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrulate" value_original="serrulate" />
        <character name="architecture_or_shape" src="d0_s1" value="shallowly" value_original="shallowly" />
        <character is_modifier="false" modifier="regularly" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o645" name="face" name_original="faces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 10–20 (–22) cm.</text>
      <biological_entity id="o646" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="22" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae 12–20 mm.</text>
      <biological_entity constraint="ray" id="o647" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Discs 11–14 mm diam. 2n = 34.</text>
      <biological_entity id="o648" name="disc" name_original="discs" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="diameter" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o649" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7a</number>
  <other_name type="common_name">Beach sunflower</other_name>
  
</bio:treatment>