<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="treatment_page">579</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(A. Gray) W. A. Weber &amp; Á. Löve" date="1981" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 45. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species bolanderi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067232</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 362. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species bolanderi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–50+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted (caudices creeping).</text>
      <biological_entity id="o3274" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–3, loosely clustered, glabrous.</text>
      <biological_entity id="o3275" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o3276" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades ± orbiculate to oblong, or lyrate (lateral lobes 1–3 pairs, sinuses usually rounded, midribs usually winged), 50–120+ × 30–70+ mm, bases cordate to subcordate, margins shallowly to deeply incised.</text>
      <biological_entity id="o3277" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="less orbiculate" name="shape" src="d0_s4" to="oblong or lyrate" />
        <character char_type="range_value" from="less orbiculate" name="shape" src="d0_s4" to="oblong or lyrate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="120" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="70" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3278" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o3279" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually reduced (petiolate or sessile and clasping; lanceolate, incised).</text>
      <biological_entity constraint="cauline" id="o3280" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 8–15+ in corymbiform or subumbelliform arrays.</text>
      <biological_entity id="o3281" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o3282" from="8" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3282" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="subumbelliform" value_original="subumbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ebracteate or inconspicuously bracteate, glabrous.</text>
      <biological_entity id="o3283" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi conspicuous.</text>
      <biological_entity id="o3284" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (8), 13, or 21, dark green, 5–8 mm, glabrous or sparsely tomentose proximally.</text>
      <biological_entity id="o3285" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="atypical_quantity" src="d0_s9" value="8" value_original="8" />
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character name="quantity" src="d0_s9" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8 or 13;</text>
      <biological_entity id="o3286" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" unit="or" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 10–15+ mm.</text>
      <biological_entity constraint="corolla" id="o3287" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 30–45+;</text>
      <biological_entity id="o3288" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="45" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 3–4 mm, limbs 3.5–4.5 mm.</text>
      <biological_entity id="o3289" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3290" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 2.5–3.5 mm, glabrous;</text>
      <biological_entity id="o3291" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 5–7 mm. 2n = 46.</text>
      <biological_entity id="o3292" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3293" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The two varieties of Packera bolanderi have been treated as species. The morphologic characteristics used to distinguish the varieties vary widely within some populations. Presence or absence of multicellular hairs has been used to separate the varieties; that trait is also variable. The morphologic distinction is weak; habitat and elevation records lend support for recognizing two varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves (relatively thick and turgid); phyllaries 13 or 21, 6–8+ mm, hairy proximally (hairs tan or brownish, multicelled)</description>
      <determination>5a Packera bolanderi var. bolanderi</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves (relatively thin); phyllaries 8, 13, (or 21), 5–6 mm, glabrous</description>
      <determination>5b Packera bolanderi var. harfordii</determination>
    </key_statement>
  </key>
</bio:treatment>