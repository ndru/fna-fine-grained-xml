<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="treatment_page">260</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">LAGOPHYLLA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 390. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus LAGOPHYLLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lago, hare, and phyllon, leaf, alluding to sericeous leaves of original species</other_info_on_name>
    <other_info_on_name type="fna_id">117501</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 8–100 (–150) cm.</text>
      <biological_entity id="o24332" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect.</text>
      <biological_entity id="o24333" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o24334" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24335" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite, most alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>± sessile;</text>
      <biological_entity constraint="proximal" id="o24336" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades narrowly elliptic to linear or (proximal) oblanceolate to spatulate, margins usually entire (proximal sometimes toothed), faces hirsute to strigose, sericeous, or villous (all or distal sometimes stipitate-glandular as well).</text>
      <biological_entity id="o24337" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="to margins, faces" constraintid="o24338, o24339" is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="hirsute" name="pubescence" notes="" src="d0_s5" to="strigose sericeous or villous" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s5" to="strigose sericeous or villous" />
      </biological_entity>
      <biological_entity id="o24338" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" is_modifier="true" name="shape" src="d0_s5" to="spatulate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s5" to="strigose sericeous or villous" />
      </biological_entity>
      <biological_entity id="o24339" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" is_modifier="true" name="shape" src="d0_s5" to="spatulate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s5" to="strigose sericeous or villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in ± paniculiform arrays or in glomerules.</text>
      <biological_entity id="o24340" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o24341" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o24340" id="r1661" name="in" negation="false" src="d0_s6" to="o24341" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0.</text>
      <biological_entity constraint="peduncular" id="o24342" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o24343" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o24344" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o24345" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± hemispheric or obovoid to obconic, 3–6+ mm diam.</text>
    </statement>
    <statement id="d0_s9">
      <text>(sometimes subtended by calyculi of 2–5 bractlets).</text>
      <biological_entity id="o24346" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="less hemispheric or obovoid" name="shape" src="d0_s8" to="obconic" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 5 in 1 series (linear to oblanceolate, herbaceous, each wholly enveloping a subtended ray ovary, abaxially piloso-hirsute to hirtellous or scabrellous).</text>
      <biological_entity id="o24347" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character constraint="in series" constraintid="o24348" name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o24348" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat to convex, densely hirtellous, paleate (paleae in rings between rays and discs, distinct or proximally connate, scarious).</text>
      <biological_entity id="o24349" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s11" to="convex" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 5, pistillate, fertile;</text>
      <biological_entity id="o24350" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow (often nerved with red to purple abaxially).</text>
      <biological_entity id="o24351" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 6, functionally staminate;</text>
      <biological_entity id="o24352" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellow, tubes shorter than funnelform throats, lobes 5, deltate (anthers ± dark purple; styles glabrous proximal to branches).</text>
      <biological_entity id="o24353" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o24354" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character constraint="than funnelform throats" constraintid="o24355" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24355" name="throat" name_original="throats" src="d0_s15" type="structure" />
      <biological_entity id="o24356" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s15" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Ray cypselae ± obcompressed (attachments basal, apices beakless, faces glabrous);</text>
      <biological_entity constraint="ray" id="o24357" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="obcompressed" value_original="obcompressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 0.</text>
      <biological_entity id="o24358" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Disc cypselae 0;</text>
      <biological_entity constraint="disc" id="o24359" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 7.</text>
      <biological_entity id="o24360" name="pappus" name_original="pappi" src="d0_s19" type="structure">
        <character name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o24361" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>334.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>Molecular phylogenetic data have indicated that Lagophylla is sister to Layia (S. Carlquist et al. 2003). Members of Lagophylla are easily overlooked in the field; leaves usually wither before flowering and heads often close at midday. Ray laminae often shrivel greatly in pressed specimens (making lengths difficult to assess). The species are moderately interfertile (W. C. Thompson 1983); most do not co-occur and no natural hybrids have been documented.</discussion>
  <references>
    <reference>Thompson, W. C. 1983. A Biosystematic Study of Lagophylla (Compositae: Heliantheae) and Related Taxa. Ph.D. dissertation. University of California, Davis.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves grayish (the distal stipitate-glandular abaxially, glands whitish or yellowish; plants otherwise eglandular); heads in paniculiform arrays or in glomerules; calyculi of 2–5 bractlets;phyllaries piloso-hirsute on angles; ray laminae 3–6 mm (plants self-compatible)</description>
      <determination>4 Lagophylla ramosissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves green or gray-green (the distal eglandular or stipitate-glandular, glands yellow, golden, or purple; plants otherwise eglandular or stipitate-glandular on distal stems); heads in paniculiform arrays; calyculi 0 or of 2–5 bractlets; phyllaries hirtellous to scabrellous or piloso-hirsute on angles; ray laminae 7–13 mm (plants strongly or weakly self-incompatible)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branching excurrent; distal leaves stipitate-glandular, glands yellow or golden; calyculi of 3–5 bractlets; involucres obconic; phyllaries hirtellous to piloso-hirsute on angles,hairs 0.3–1+ mm, ± patent to antrorsely curved</description>
      <determination>2 Lagophylla glandulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branching ± pseudo-dichotomous (main stems ± zigzag); leaves usually eglandular (the distal eglandular or stipitate-glandular, glands mostly purple, some yellow); calyculi 0, or of 2–3 bractlets; involucres ± hemispheric to obovoid; phyllaries ± hirtellous to scabrellous or piloso-hirsute on angles, hairs 0.1–1+ mm, ± antrorsely curved</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries 4–5 mm, piloso-hirsute on angles, hairs 0.5–1+ mm; cypselae glossy</description>
      <determination>1 Lagophylla minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries 4–6.5 mm, ± hirtellous to scabrellous on angles, hairs 0.1–0.6 mm; cypselaedull</description>
      <determination>3 Lagophylla dichotoma</determination>
    </key_statement>
  </key>
</bio:treatment>