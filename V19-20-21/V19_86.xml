<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="treatment_page">125</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(A. Gray) Petrak" date="1911" rank="species">wheeleri</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>31: 67. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species wheeleri</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066406</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wheeleri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 56. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species wheeleri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="species">blumeri</taxon_name>
    <taxon_hierarchy>genus Cirsium;species blumeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Rydberg) Petrak" date="unknown" rank="species">olivescens</taxon_name>
    <taxon_hierarchy>genus Cirsium;species olivescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Greene) Wooton &amp; Standley" date="unknown" rank="species">perennans</taxon_name>
    <taxon_hierarchy>genus Cirsium;species perennans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wheeleri</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">salinense</taxon_name>
    <taxon_hierarchy>genus Cirsium;species wheeleri;variety salinense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, slender, 15–60 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted with deep-seated root sprouts.</text>
      <biological_entity id="o4389" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character constraint="with root sprouts" constraintid="o4390" is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="root" id="o4390" name="sprout" name_original="sprouts" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="deep-seated" value_original="deep-seated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–few, erect, closely gray-tomentose;</text>
      <biological_entity id="o4391" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="closely" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0–few, ascending.</text>
      <biological_entity id="o4392" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades lanceolate to narrowly elliptic, 10–25 × 1–4 cm, unlobed and merely spinulose or pinnately lobed about halfway to midveins, often terminal lobes long-tapered, lobes short, lanceolate to triangular, entire to few toothed or lobed, well separated by wide, U-shaped sinuses, main spines slender, 2–5 mm, abaxial faces gray-tomentose, adaxial green, glabrous to thinly tomentose;</text>
      <biological_entity id="o4393" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4394" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="merely" name="shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character constraint="to midveins" constraintid="o4395" is_modifier="false" name="position" src="d0_s4" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity id="o4395" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o4396" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-tapered" value_original="long-tapered" />
      </biological_entity>
      <biological_entity id="o4397" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="triangular entire" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="triangular entire" />
        <character constraint="by sinuses" constraintid="o4398" is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o4398" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="shape" src="d0_s4" value="u--shaped" value_original="u--shaped" />
      </biological_entity>
      <biological_entity constraint="main" id="o4399" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4400" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4401" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="thinly tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal usually present at flowering, winged-petiolate;</text>
      <biological_entity id="o4402" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o4403" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline winged-petiolate proximally, mid and distal sessile, progressively reduced distally, bases not or scarcely decurrent, sometimes distal weakly clasping;</text>
      <biological_entity id="o4404" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o4405" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o4406" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4407" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o4408" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distalmost often reduced to lanceolate or linear, long-acuminate bracts.</text>
      <biological_entity id="o4409" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distalmost" id="o4410" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="to bracts" constraintid="o4411" is_modifier="false" modifier="often" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4411" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s7" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–6, borne singly or few at branch tips in corymbiform arrays.</text>
      <biological_entity id="o4412" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
        <character constraint="at branch tips" constraintid="o4413" is_modifier="false" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="branch" id="o4413" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <biological_entity id="o4414" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4413" id="r446" name="in" negation="false" src="d0_s8" to="o4414" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–10 cm.</text>
      <biological_entity id="o4415" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres hemispheric to subcylindric, 1.5–2.2 × 1.5–2.5 cm, thinly floccose-tomentose or glabrate.</text>
      <biological_entity id="o4416" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s10" to="subcylindric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s10" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s10" value="floccose-tomentose" value_original="floccose-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 6–9 series, imbricate, pale green with darker apices, brownish when dry, lanceolate (outer) to linear-lanceolate (inner), margins of outer entire, abaxial faces with narrow glutinous ridge;</text>
      <biological_entity id="o4417" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character constraint="with apices" constraintid="o4419" is_modifier="false" name="coloration" src="d0_s11" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="when dry" name="coloration" notes="" src="d0_s11" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o4418" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <biological_entity id="o4419" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o4420" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o4421" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4423" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o4417" id="r447" name="in" negation="false" src="d0_s11" to="o4418" />
      <relation from="o4420" id="r448" name="part_of" negation="false" src="d0_s11" to="o4421" />
      <relation from="o4422" id="r449" name="with" negation="false" src="d0_s11" to="o4423" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle appressed proximally, apices spreading to ascending, bodies entire or rarely spinulose, spines slender, 3–7 mm;</text>
      <biological_entity constraint="abaxial" id="o4422" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o4424" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="middle" value_original="middle" />
        <character is_modifier="false" modifier="proximally" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o4425" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="ascending" />
      </biological_entity>
      <biological_entity id="o4426" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s12" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o4427" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner often dark purple or blackish, flexuous, scarious, entire to pectinate-fringed, tapered or expanded.</text>
      <biological_entity id="o4428" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="inner; inner">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s13" to="pectinate-fringed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="size" src="d0_s13" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o4429" name="inner" name_original="inner" src="d0_s13" type="structure" />
      <relation from="o4428" id="r450" name="part_of" negation="false" src="d0_s13" to="o4429" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas white or pink to pale-purple, 20–28 mm, tubes 9–14 mm, throats 5–7.5 mm, lobes 5–10 mm;</text>
      <biological_entity id="o4430" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="pale-purple" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s14" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4431" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4432" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4433" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 2.5–6 mm.</text>
      <biological_entity constraint="style" id="o4434" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae stramineous with brownish streaks, 6.5–7 mm, apical collars colored like body, narrow;</text>
      <biological_entity id="o4435" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character constraint="with streaks" constraintid="o4436" is_modifier="false" name="coloration" src="d0_s16" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4436" name="streak" name_original="streaks" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4437" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o4438" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="colored" value_original="colored" />
        <character is_modifier="true" name="shape" src="d0_s16" value="like" value_original="like" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 15–20 mm. 2n = 28.</text>
      <biological_entity id="o4439" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4440" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Jul–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests, pine-oak, juniper-dominated woodlands, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="pine-oak" />
        <character name="habitat" value="juniper-dominated woodlands" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex., Utah; Mexico (Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Wheeler’s thistle</other_name>
  <discussion>Cirsium wheeleri occurs from the mountains of the Colorado Plateau of central Utah and southwestern Colorado south through the highlands of Arizona and New Mexico to southwestern Texas and northwestern Mexico. The recently described C. wheeleri var. salinense is a minor variant with subentire leaves that is scattered through much of the range of the species.</discussion>
  
</bio:treatment>