<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Kirpicznikov" date="1950" rank="genus">pseudognaphalium</taxon_name>
    <taxon_name authority="(Weatherby) G. L. Nesom" date="2001" rank="species">micradenium</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>19: 618. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus pseudognaphalium;species micradenium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417063</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">obtusifolium</taxon_name>
    <taxon_name authority="Weatherby" date="unknown" rank="variety">micradenium</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>25: 22. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species obtusifolium;variety micradenium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">helleri</taxon_name>
    <taxon_name authority="(Weatherby) Mahler" date="unknown" rank="variety">micradenium</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;species helleri;variety micradenium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pseudognaphalium</taxon_name>
    <taxon_name authority="(Britton) Anderberg" date="unknown" rank="species">helleri</taxon_name>
    <taxon_name authority="(Weatherby) Kartesz" date="unknown" rank="subspecies">micradenium</taxon_name>
    <taxon_hierarchy>genus Pseudognaphalium;species helleri;subspecies micradenium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (fragrant), 15–60 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or fibrous-rooted.</text>
      <biological_entity id="o10422" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems glandular-puberulent (without persistent tomentum), stipitate-glands 0.1–0.2 mm, stalks narrower than gland widths.</text>
      <biological_entity id="o10423" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o10424" name="stipitate-gland" name_original="stipitate-glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10425" name="stalk" name_original="stalks" src="d0_s2" type="structure">
        <character constraint="than gland" constraintid="o10426" is_modifier="false" name="width" src="d0_s2" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o10426" name="gland" name_original="gland" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades linear to linear-lanceolate or linear-oblanceolate, 1.5–5.5 cm × 1.5–10 mm, bases not clasping, not decurrent, margins flat, faces bicolor, abaxial white to gray, tomentose, adaxial green, both minutely stipitate-glandular.</text>
      <biological_entity id="o10427" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-lanceolate or linear-oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10428" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o10429" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o10430" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="bicolor" value_original="bicolor" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10431" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s3" to="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10432" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o10433" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o10434" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o10433" id="r970" name="in" negation="false" src="d0_s4" to="o10434" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate-campanulate, 5–6 mm.</text>
      <biological_entity id="o10435" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 4–6 series, white to tawny white (hyaline, shiny), narrowly ovate to oblong, glabrous.</text>
      <biological_entity id="o10436" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s6" to="tawny white" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s6" to="oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10437" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <relation from="o10436" id="r971" name="in" negation="false" src="d0_s6" to="o10437" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate florets 47–78.</text>
      <biological_entity id="o10438" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="47" name="quantity" src="d0_s7" to="78" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Bisexual florets (7–) 11–20.</text>
      <biological_entity id="o10439" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s8" to="11" to_inclusive="false" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ridged, smooth.</text>
      <biological_entity id="o10440" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry woods and openings, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry woods" />
        <character name="habitat" value="openings" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., Ind., Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Pa., S.C., Tenn., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Delicate rabbit-tobacco</other_name>
  <discussion>Pseudognaphalium micradenium has a more northern and Appalachian distribution than P. helleri. A report of P. micradenium for Louisiana probably was based on specimens of P. helleri. The two species differ in vestiture and other features; stems of P. micradenium are more slender than those of its close relatives.</discussion>
  
</bio:treatment>