<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
    <other_info_on_meta type="illustration_page">375</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">pyrrhopappus</taxon_name>
    <taxon_name authority="(Walter) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">carolinianus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 144. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus pyrrhopappus;species carolinianus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220011313</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leontodon</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">carolinianum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>192. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leontodon;species carolinianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrrhopappus</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="species">carolinianus</taxon_name>
    <taxon_name authority="(Shinners) H. E. Ahles" date="unknown" rank="variety">georgianus</taxon_name>
    <taxon_hierarchy>genus Pyrrhopappus;species carolinianus;variety georgianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrrhopappus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">georgianus</taxon_name>
    <taxon_hierarchy>genus Pyrrhopappus;species georgianus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (sometimes persisting), (5–) 20–50 (–100+) cm.</text>
      <biological_entity id="o17772" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually branching from bases and/or distally, rarely scapiform, usually glabrous proximally, sometimes pilosulous.</text>
      <biological_entity id="o17773" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from bases" constraintid="o17774" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="distally; rarely" name="shape" notes="" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" modifier="usually; proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
      <biological_entity id="o17774" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves (1–) 3–9+, proximal mostly lanceolate, margins usually dentate, sometimes pinnately lobed, distal narrowly lanceolate to lance-attenuate, margins entire or with 1–2 lobes near bases.</text>
      <biological_entity constraint="cauline" id="o17775" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="9" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17776" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o17777" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17778" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="lance-attenuate" />
      </biological_entity>
      <biological_entity id="o17779" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with 1-2 lobes" value_original="with 1-2 lobes" />
      </biological_entity>
      <biological_entity id="o17780" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <biological_entity id="o17781" name="base" name_original="bases" src="d0_s2" type="structure" />
      <relation from="o17779" id="r1597" name="with" negation="false" src="d0_s2" to="o17780" />
      <relation from="o17780" id="r1598" name="near" negation="false" src="d0_s2" to="o17781" />
    </statement>
    <statement id="d0_s3">
      <text>Heads (1–) 3–5+ in loose, corymbiform arrays.</text>
      <biological_entity id="o17782" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o17783" from="3" name="quantity" src="d0_s3" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17783" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi: bractlets 13–16+ in 2–3 series, subulate to filiform, 8–12+ mm.</text>
      <biological_entity id="o17784" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o17785" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17786" from="13" name="quantity" src="d0_s4" to="16" upper_restricted="false" />
        <character char_type="range_value" from="subulate" name="shape" notes="" src="d0_s4" to="filiform" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17786" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± cylindric to campanulate, 17–24+ mm.</text>
      <biological_entity id="o17787" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="less cylindric" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s5" to="24" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 16–21+.</text>
      <biological_entity id="o17788" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s6" to="21" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 50–150+;</text>
      <biological_entity id="o17789" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s7" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 2.5–3.5 mm (pollen equatorial diameters mostly 43–47 µm).</text>
      <biological_entity id="o17790" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae: bodies reddish-brown, 4–6 mm, beaks 8–10 mm;</text>
      <biological_entity id="o17791" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
      <biological_entity id="o17792" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17793" name="beak" name_original="beaks" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 7–10+ mm.</text>
      <biological_entity id="o17794" name="cypsela" name_original="cypselae" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 12.</text>
      <biological_entity id="o17795" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17796" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)May–Jun(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, edges of woods, prairies, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="edges" constraint="of woods , prairies , sandy soils" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Kans., Mo., Nebr., N.C., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>