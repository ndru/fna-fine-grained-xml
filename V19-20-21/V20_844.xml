<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="genus">herrickia</taxon_name>
    <taxon_name authority="(Nuttall) Brouillet" date="2004" rank="species">glauca</taxon_name>
    <taxon_name authority="(S. F. Blake) Brouillet" date="2004" rank="variety">pulchra</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 897. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus herrickia;species glauca;variety pulchra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068465</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="species">glaucodes</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="subspecies">pulcher</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>35: 174. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species glaucodes;subspecies pulcher;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(M. E. Jones) S. F. Blake" date="unknown" rank="species">wasatchensis</taxon_name>
    <taxon_name authority="(S. F. Blake) S. L. Welsh" date="unknown" rank="variety">pulcher</taxon_name>
    <taxon_hierarchy>genus Aster;species wasatchensis;variety pulcher;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eurybia</taxon_name>
    <taxon_name authority="(S. F. Blake) G. L. Nesom" date="unknown" rank="species">pulchra</taxon_name>
    <taxon_hierarchy>genus Eurybia;species pulchra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eurybia</taxon_name>
    <taxon_name authority="(Nuttall) G. L. Nesom" date="unknown" rank="species">glauca</taxon_name>
    <taxon_name authority="(S. F. Blake) Brouillet" date="unknown" rank="variety">pulchra</taxon_name>
    <taxon_hierarchy>genus Eurybia;species glauca;variety pulchra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–80 cm, short-stipitate-glandular.</text>
      <biological_entity id="o7153" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3+, glabrous or glabrescent proximally, ± densely stipitate-glandular distally.</text>
      <biological_entity id="o7154" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="more or less densely; distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate, elliptic, or obovate-oblong to lanceolate, 20–90 × 5–21 mm, faces usually glabrous (proximal), sometimes sparsely stipitate-glandular or abaxial midnerves stipitate-glandular (distal often).</text>
      <biological_entity id="o7155" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="obovate-oblong" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="obovate-oblong" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="90" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7156" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7157" name="midnerve" name_original="midnerves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 10–30+.</text>
      <biological_entity id="o7158" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s3" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles ± densely stipitate-glandular;</text>
      <biological_entity id="o7159" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 0–2, foliaceous, densely stipitate-glandular.</text>
      <biological_entity id="o7160" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to cylindro campanulate, 6.5–9.5 mm.</text>
      <biological_entity id="o7161" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="cylindro" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s6" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 26–40, ± densely stipitate-glandular (outer, green zones) or eglandular (inner).</text>
      <biological_entity id="o7162" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="26" name="quantity" src="d0_s7" to="40" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 10–14;</text>
      <biological_entity id="o7163" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae 11.5–16 × 2.2–2.9 mm.</text>
      <biological_entity id="o7164" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="11.5" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s9" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 29–40;</text>
      <biological_entity id="o7165" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="29" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 7–8.5 mm, lobes 1–1.8 mm.</text>
      <biological_entity id="o7166" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7167" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Salt desert shrub, sagebrush, pinyon-juniper and ponderosa pine communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="salt desert shrub" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="ponderosa pine communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <other_name type="common_name">Beautiful aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety pulchra is known only from the San Juan River basin (Washington and Kane counties, Utah; Coconino County, Arizona). It is of conservation concern in Utah. Recently, S. L. Welsh (Welsh et al. 2003) transferred var. pulchra from Aster glaucodes (syn. H. glauca) to A. wasatchensis on the grounds of its glandularity and “general conformation,” while acknowledging the glaucous foliage shared with Herrickia glauca. Yet the variety does not share critical features of the involucre with H. wasatchensis, and the glandularity is most likely a shared primitive feature within the group and is therefore not indicative of a particular relationship within the group. Distribution and morphology strongly suggest the relationships reported here. Alternately, var. pulchra may represent a distinct species, as hypothesized by G. L. Nesom (1994b).</discussion>
  
</bio:treatment>