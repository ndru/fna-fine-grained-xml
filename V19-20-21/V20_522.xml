<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) Shinners" date="1951" rank="species">oregona</taxon_name>
    <taxon_name authority="(A. Gray) Semple" date="1987" rank="variety">scaberrima</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>39: 383. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species oregona;variety scaberrima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068482</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">oregona</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">scaberrima</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 124. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species oregona;variety scaberrima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually branched distally, densely, coarsely stipitate-glandular.</text>
      <biological_entity id="o30192" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely; coarsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves green to dark green, mid cauline averaging 13.9 × 3.9 mm (9.2–19.4 × 2.2–7.8 mm);</text>
      <biological_entity id="o30193" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="dark green" />
      </biological_entity>
      <biological_entity constraint="mid cauline" id="o30194" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="length" src="d0_s1" unit="mm" value="13.9" value_original="13.9" />
        <character name="width" src="d0_s1" unit="mm" value="3.9" value_original="3.9" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branch leaves glabrate to sparsely hispid (fewer than 5 hairs/mm²), moderately to densely coarsely glandular.</text>
      <biological_entity constraint="branch" id="o30195" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s2" to="sparsely hispid" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s2" to="sparsely hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Outer phyllaries narrowly to broadly ovate.</text>
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity constraint="outer" id="o30196" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30197" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand and gravel soils, gravel bars, along arroyos, flood plains of streams in dry oak woods and grassy hills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand" />
        <character name="habitat" value="gravel soils" />
        <character name="habitat" value="gravel bars" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="flood plains" constraint="of streams in dry oak woods and grassy hills" />
        <character name="habitat" value="streams" constraint="in dry oak woods and grassy hills" />
        <character name="habitat" value="dry oak woods" />
        <character name="habitat" value="grassy hills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3d.</number>
  <discussion>Variety scaberrima is found in the inner Coast Ranges. Its branches tend to be longer on average; the arrays are thus rather large and open compared to undamaged plants of the other varieties. The smaller leaves of much branched and few-headed specimens of var. rudis can result in a habit similar to that of var. scaberrima. This could create confusion in identity if the ovate outer phyllaries of the latter are overlooked.</discussion>
  
</bio:treatment>