<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Pursh) Shinners" date="1951" rank="species">villosa</taxon_name>
    <taxon_name authority="Semple" date="1996" rank="variety">sierrablancensis</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Heterotheca Phyllotheca,</publication_title>
      <place_in_publication>147, fig. 49. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species villosa;variety sierrablancensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068497</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 22–34 (–46) cm, moderately to densely hispido-strigose, abundantly long-hirsute, sparsely to moderately stipitate-glandular.</text>
      <biological_entity id="o785" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="34" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="46" to_unit="cm" />
        <character char_type="range_value" from="22" from_unit="cm" name="some_measurement" src="d0_s0" to="34" to_unit="cm" />
        <character is_modifier="false" modifier="moderately to densely; densely" name="pubescence" src="d0_s0" value="hispido-strigose" value_original="hispido-strigose" />
        <character char_type="range_value" from="abundantly long-hirsute" name="pubescence" src="d0_s0" to="sparsely moderately stipitate-glandular" />
        <character char_type="range_value" from="abundantly long-hirsute" name="pubescence" src="d0_s0" to="sparsely moderately stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal cauline leaf-blades (perpendicular proximally, ascending distally) lanceolate to ovate-oblong, 13–23 × (3–) 4–6.5 mm, ± reduced distally, bases rounded, margins flat, apices broadly acute to obtuse, faces moderately to densely strigose (hairs 29–118/mm2), sparsely or usually moderately stipitate-glandular (glands 2–50/mm2).</text>
      <biological_entity constraint="distal cauline" id="o786" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="ovate-oblong" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s1" to="23" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s1" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="6.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less; distally" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o787" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o788" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o789" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o790" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="or" value_original="or" />
        <character is_modifier="false" modifier="usually moderately" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads (1–) 3–12 (–17) in congested, corymbiform to subumbelliform arrays.</text>
      <biological_entity id="o791" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="17" />
        <character char_type="range_value" constraint="in arrays" constraintid="o792" from="3" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
      <biological_entity id="o792" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="congested" value_original="congested" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s2" to="subumbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 6–31 (–49) mm, moderately to densely hispido-strigose, moderately glandular;</text>
      <biological_entity id="o793" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="31" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="49" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="31" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s3" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="moderately" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 1–3, linear-lanceolate to linear-oblong, reduced distally, not foliar.</text>
      <biological_entity id="o794" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="linear-oblong" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="not" name="position" src="d0_s4" value="foliar" value_original="foliar" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres narrowly campanulate (fresh), 5.5–7.8 (–8.7) mm.</text>
      <biological_entity id="o795" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8.7" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries narrowly triangular-lanceolate, margins often reddish purple distally, faces moderately short-strigose, eglandular or sparsely stipitate-glandular.</text>
      <biological_entity id="o796" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular-lanceolate" value_original="triangular-lanceolate" />
      </biological_entity>
      <biological_entity id="o797" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s6" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o798" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s6" value="short-strigose" value_original="short-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (10–) 13–20, laminae (8.5–) 9–12 (–14) mm (showy).</text>
      <biological_entity id="o799" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s7" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 18.</text>
      <biological_entity id="o800" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o801" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, disturbed areas, exposed, igneous rocky slopes, rocky granitic outcrops, road cuts, forests of Abies, Pinus ponderosa, and/or Pseudotsuga</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="rocky slopes" modifier="exposed igneous" />
        <character name="habitat" value="rocky granitic outcrops" />
        <character name="habitat" value="road cuts" />
        <character name="habitat" value="forests" constraint="of abies , pinus ponderosa" />
        <character name="habitat" value="pinus ponderosa" />
        <character name="habitat" value="igneous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15i.</number>
  <other_name type="common_name">White Mountains hairy goldenaster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety sierrablancensis is distinguished by its lanceolate to ovate-oblong, moderately strigose and sparsely to usually moderately stipitate-glandular leaves, its subumbelliform arrays, and its showy rays. It is close to var. nana, but usually has a denser indument and more often ovate than oblong leaves. Some plants from the White Mountains approach Heterotheca fulcrata var. fulcrata in leaf shape but lack the distinct, large leafy bracts of the latter. Less hairy plants with oblong leaves are similar vegetatively to var. nana, but usually have longer, showier rays.</discussion>
  
</bio:treatment>