<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Moench" date="1794" rank="genus">echinacea</taxon_name>
    <taxon_name authority="McGregor" date="1968" rank="species">simulata</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>3: 282. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus echinacea;species simulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416466</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinacea</taxon_name>
    <taxon_name authority="McGregor" date="unknown" rank="species">speciosa</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Kansas Acad. Sci.</publication_title>
      <place_in_publication>70: 366. 1967,</place_in_publication>
      <other_info_on_pub>not (Wenderoth) Paxton 1849</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Echinacea;species speciosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinacea</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall" date="unknown" rank="species">pallida</taxon_name>
    <taxon_name authority="(McGregor) Binns, B. R. Baum &amp; Arnason" date="unknown" rank="variety">simulata</taxon_name>
    <taxon_hierarchy>genus Echinacea;species pallida;variety simulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 100 cm (roots fusiform, branched).</text>
      <biological_entity id="o5487" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage sparsely to densely hairy (hairs spreading).</text>
      <biological_entity id="o5488" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly green to purplish.</text>
      <biological_entity id="o5489" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly green" name="coloration" src="d0_s2" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petioles 4–20 cm;</text>
      <biological_entity constraint="basal" id="o5490" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5491" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-), 3-nerved, or 5-nerved, linear to lanceolate, 5–40 × 0.5–4 cm, bases attenuate, margins entire (usually ciliate).</text>
      <biological_entity constraint="basal" id="o5492" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5493" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="[1" value_original="[1" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-nerved" value_original="5-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5494" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o5495" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 20–40+ cm.</text>
      <biological_entity id="o5496" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s5" to="40" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate to ovate, 7–15 × 1.5–3.5 mm.</text>
      <biological_entity id="o5497" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles: paleae 10–14 mm, tips pinkish to purple, incurved, sharp-pointed.</text>
      <biological_entity id="o5498" name="receptacle" name_original="receptacles" src="d0_s7" type="structure" />
      <biological_entity id="o5499" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5500" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s7" to="purple" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray corollas rose to pink or white, laminae drooping to reflexed, 40–90 × 4–7 mm, glabrous or sparsely hairy abaxially.</text>
      <biological_entity constraint="ray" id="o5501" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s8" to="pink or white" />
      </biological_entity>
      <biological_entity id="o5502" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="drooping" name="orientation" src="d0_s8" to="reflexed" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s8" to="90" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Discs conic to hemispheric, 20–30 × 20–30 mm.</text>
      <biological_entity id="o5503" name="disc" name_original="discs" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="hemispheric" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 5–6.5 mm, lobes pink to purplish.</text>
      <biological_entity constraint="disc" id="o5504" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5505" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae tan, 3–4.5 mm, faces smooth, usually glabrous, sometimes (rays) hairy;</text>
      <biological_entity id="o5506" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tan" value_original="tan" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5507" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi to ca. 1 mm (usually without major teeth).</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 22.</text>
      <biological_entity id="o5508" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5509" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, open, wooded hillsides, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="rocky" />
        <character name="habitat" value="wooded hillsides" />
        <character name="habitat" value="open" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ga., Ill., Ky., Mo., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Wavy-leaf purple coneflower</other_name>
  <discussion>Echinacea simulata has been reported as introduced in Illinois (http://www.natureserve.org).</discussion>
  
</bio:treatment>