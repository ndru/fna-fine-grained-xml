<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">microcephalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">microcephalum</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species microcephalum;variety microcephalum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068427</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–120 cm.</text>
      <biological_entity id="o5423" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal and mid-blades serrate to undulate-serrate, glabrous or sparsely hairy;</text>
      <biological_entity id="o5424" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o5425" name="mid-blade" name_original="mid-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s1" to="undulate-serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal blades glabrous or sparsely hairy.</text>
      <biological_entity id="o5426" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o5427" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 20–150 (–300+) per plant.</text>
      <biological_entity id="o5428" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="300" upper_restricted="false" />
        <character char_type="range_value" constraint="per plant" constraintid="o5429" from="20" name="quantity" src="d0_s3" to="150" />
      </biological_entity>
      <biological_entity id="o5429" name="plant" name_original="plant" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–3.5 cm, projecting terminal heads barely beyond foliage.</text>
      <biological_entity id="o5430" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5431" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o5432" name="foliage" name_original="foliage" src="d0_s4" type="structure" />
      <relation from="o5431" id="r409" name="beyond" negation="false" src="d0_s4" to="o5432" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 4–8 × 4–8 mm.</text>
      <biological_entity id="o5433" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 7–13;</text>
      <biological_entity id="o5434" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas yellow, 2.6–4.8 × 1.6–3.9 mm.</text>
      <biological_entity id="o5435" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s7" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s7" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 120–300+;</text>
      <biological_entity id="o5436" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="120" name="quantity" src="d0_s8" to="300" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 1.2–1.6 mm, lobes (4–) 5.</text>
      <biological_entity id="o5437" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5438" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pappi (0.2–) 0.3–0.4 (–0.5) mm.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 26.</text>
      <biological_entity id="o5439" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5440" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Jun–Jul(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or clay soils, ditches, washes, around ponds and lakes, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="washes" constraint="around ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Okla., Tex.; Mexico (Coahuila, Nuevo León, and Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16a.</number>
  
</bio:treatment>