<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="treatment_page">526</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">tenuifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 131. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species tenuifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067116</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze" date="unknown" rank="species">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–150 cm.</text>
      <biological_entity id="o5339" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose.</text>
      <biological_entity id="o5340" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems glabrous or sparsely pilose.</text>
      <biological_entity id="o5341" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline (arising from separated nodes) 1-nerved, linear to linear-lanceolate, 100–300 × 1–2 (–2.5) mm, gradually or abruptly reduced distally, essentially glabrous, glanddotted (proximal margins sometimes ciliate).</text>
      <biological_entity id="o5342" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-lanceolate" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s3" to="300" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; abruptly; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in compact, racemiform arrays.</text>
      <biological_entity id="o5343" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o5344" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="compact" value_original="compact" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o5343" id="r403" name="in" negation="false" src="d0_s4" to="o5344" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles (ascending) 1–7 mm.</text>
      <biological_entity id="o5345" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate-campanulate, 5–7 × 4–5 mm.</text>
      <biological_entity id="o5346" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series, lanceolate to oblong or elliptic-oblong, unequal, essentially glabrous, margins with (pinkish purple) hyaline borders, apices usually rounded-retuse and minutely involute-cuspidate to apiculate.</text>
      <biological_entity id="o5347" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s7" to="oblong or elliptic-oblong" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5348" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o5349" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o5350" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o5351" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="rounded-retuse" value_original="rounded-retuse" />
        <character char_type="range_value" from="minutely involute-cuspidate" name="architecture_or_shape" src="d0_s7" to="apiculate" />
      </biological_entity>
      <relation from="o5347" id="r404" name="in" negation="false" src="d0_s7" to="o5348" />
      <relation from="o5349" id="r405" name="with" negation="false" src="d0_s7" to="o5350" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 4–6;</text>
      <biological_entity id="o5352" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous inside.</text>
      <biological_entity id="o5353" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2.5–4 mm;</text>
      <biological_entity id="o5354" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles barbellate.</text>
      <biological_entity id="o5355" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o5356" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o5357" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Longleaf pine savannas, longleaf pine-scrub oak, turkey oak-bluejack oak, slash pine-sand pine-scrub, sand pine-scrub, sand ridges, hills, and flats, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="longleaf pine savannas" />
        <character name="habitat" value="longleaf pine-scrub oak" />
        <character name="habitat" value="turkey oak-bluejack oak" />
        <character name="habitat" value="pine-sand pine-scrub" modifier="slash" />
        <character name="habitat" value="sand pine-scrub" />
        <character name="habitat" value="sand ridges" />
        <character name="habitat" value="hills" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Pine-needle or shortleaf gayfeather</other_name>
  
</bio:treatment>