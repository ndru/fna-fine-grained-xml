<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="treatment_page">306</other_info_on_meta>
    <other_info_on_meta type="illustration_page">305</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="S. F. Blake" date="1934" rank="species">uncialis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>47: 173. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species uncialis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066692</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 0.8–7 cm (cespitose);</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches usually lignescent, sometimes relatively elongate.</text>
      <biological_entity id="o30152" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o30153" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s1" value="lignescent" value_original="lignescent" />
        <character is_modifier="false" modifier="sometimes relatively" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect (greenish), sparsely loosely strigose to hirsuto-villous, usually densely villous distally, eglandular.</text>
      <biological_entity id="o30154" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="sparsely loosely strigose" name="pubescence" src="d0_s2" to="hirsuto-villous" />
        <character is_modifier="false" modifier="usually densely; distally" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves all or mostly basal (persistent; petiole base margins spreading-ascending-ciliate);</text>
      <biological_entity id="o30156" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>(greenish) spatulate, blades elliptic-obovate to suborbiculate, 10–40 × (1.5–) 2–6 mm (bases abruptly contracted to petioles), margins entire (apices rounded to obtuse), faces strigose to hirsuto-villous, less densely so abaxially, eglandular.</text>
      <biological_entity id="o30155" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o30157" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic-obovate" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30158" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30159" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s4" to="hirsuto-villous" />
        <character is_modifier="false" modifier="less densely; densely; abaxially" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o30160" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–5 × 7–10 mm.</text>
      <biological_entity id="o30161" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (margins purplish), hirsuto-villous, sometimes sparsely minutely glandular (midregions and near apices).</text>
      <biological_entity id="o30162" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o30163" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o30162" id="r2797" name="in" negation="false" src="d0_s7" to="o30163" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 22–30;</text>
      <biological_entity id="o30164" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to pink, often with broad pink abaxial midstripe, 6–7 mm, laminae not coiling or reflexing, spreading.</text>
      <biological_entity id="o30165" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30166" name="midstripe" name_original="midstripe" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o30167" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o30165" id="r2798" modifier="often" name="with" negation="false" src="d0_s9" to="o30166" />
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2–2.4 mm.</text>
      <biological_entity constraint="disc" id="o30168" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.3–1.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o30169" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o30170" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 14–22 bristles.</text>
      <biological_entity id="o30171" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o30172" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o30173" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" is_modifier="true" name="quantity" src="d0_s12" to="22" />
      </biological_entity>
      <relation from="o30171" id="r2799" name="outer of" negation="false" src="d0_s12" to="o30172" />
      <relation from="o30171" id="r2800" name="inner of" negation="false" src="d0_s12" to="o30173" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>76.</number>
  <other_name type="common_name">Lone fleabane</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 0.8–2.5 cm, hirsuto-villous; leaves 10–20 mm, hirsuto-villous to loosely strigose</description>
      <determination>76a Erigeron uncialis var. uncialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 3–7 cm, loosely villoso-strigose; leaves 20–40 mm, sparsely closely strigose</description>
      <determination>76b Erigeron uncialis var. conjugans</determination>
    </key_statement>
  </key>
</bio:treatment>