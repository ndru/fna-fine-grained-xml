<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Vallisneria</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1015. 1753; Gen. Pl. ed. 5; 446, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus Vallisneria</taxon_hierarchy>
    <other_info_on_name type="etymology">for Antonio Vallisneri, Italian botanist, 1661–1730</other_info_on_name>
    <other_info_on_name type="fna_id">134329</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, of fresh or brackish waters.</text>
      <biological_entity id="o3502" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes and stolons present.</text>
      <biological_entity id="o3503" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3504" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Erect stems rooted in substrate, unbranched, short.</text>
      <biological_entity id="o3505" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="in substrate" constraintid="o3506" is_modifier="false" name="architecture" src="d0_s2" value="rooted" value_original="rooted" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o3506" name="substrate" name_original="substrate" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, submersed, sessile;</text>
      <biological_entity id="o3507" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="location" src="d0_s3" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, base grading into sheath, apex obtuse to apiculate;</text>
      <biological_entity id="o3508" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o3509" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o3510" name="sheath" name_original="sheath" src="d0_s4" type="structure" />
      <biological_entity id="o3511" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="apiculate" />
      </biological_entity>
      <relation from="o3509" id="r474" name="into" negation="false" src="d0_s4" to="o3510" />
    </statement>
    <statement id="d0_s5">
      <text>midvein with 4–5 rows of lacunae on each side, balde appearing 3-zoned with light-colored middle zone bordered on each side by darker zone;</text>
      <biological_entity id="o3513" name="row" name_original="rows" src="d0_s5" type="structure" constraint="lacuna" constraint_original="lacuna; lacuna">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o3514" name="lacuna" name_original="lacunae" src="d0_s5" type="structure" />
      <biological_entity id="o3515" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="middle" id="o3516" name="zone" name_original="zone" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="light-colored" value_original="light-colored" />
        <character constraint="on side" constraintid="o3517" is_modifier="false" name="architecture" src="d0_s5" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o3517" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o3518" name="zone" name_original="zone" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="darker" value_original="darker" />
      </biological_entity>
      <relation from="o3512" id="r475" name="with" negation="false" src="d0_s5" to="o3513" />
      <relation from="o3513" id="r476" name="part_of" negation="false" src="d0_s5" to="o3514" />
      <relation from="o3513" id="r477" name="on" negation="false" src="d0_s5" to="o3515" />
      <relation from="o3517" id="r478" name="by" negation="false" src="d0_s5" to="o3518" />
    </statement>
    <statement id="d0_s6">
      <text>abaxial surfacely without prickles or aerenchyma;</text>
      <biological_entity id="o3512" name="midvein" name_original="midvein" src="d0_s5" type="structure">
        <character constraint="with middle zone" constraintid="o3516" is_modifier="false" name="coloration" notes="" src="d0_s5" value="3-zoned" value_original="3-zoned" />
        <character constraint="without aerenchyma" constraintid="o3520" is_modifier="false" name="position" src="d0_s6" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity id="o3519" name="prickle" name_original="prickles" src="d0_s6" type="structure" />
      <biological_entity id="o3520" name="aerenchymum" name_original="aerenchyma" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>intravaginal squamules entire.</text>
      <biological_entity constraint="intravaginal" id="o3521" name="squamule" name_original="squamules" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences cymose, long-pedunculate;</text>
      <biological_entity id="o3522" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="long-pedunculate" value_original="long-pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>spathe not winged.</text>
      <biological_entity id="o3523" name="spathe" name_original="spathe" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers unisexual, staminate and pistillate on different plants, submersed or floating, sessile (staminate) or pedicellate (pistillate);</text>
      <biological_entity id="o3524" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o3525" is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="location" src="d0_s10" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="location" src="d0_s10" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o3525" name="plant" name_original="plants" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>petals transparent.</text>
      <biological_entity id="o3526" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="transparent" value_original="transparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: filaments distinct, released from spathe and floating to surface;</text>
      <biological_entity id="o3527" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3528" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character constraint="to surface" constraintid="o3530" is_modifier="false" name="growth_form_or_location" src="d0_s12" value="floating" value_original="floating" />
      </biological_entity>
      <biological_entity id="o3529" name="spathe" name_original="spathe" src="d0_s12" type="structure" />
      <biological_entity id="o3530" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <relation from="o3528" id="r479" name="released from" negation="false" src="d0_s12" to="o3529" />
    </statement>
    <statement id="d0_s13">
      <text>anthers spheric;</text>
      <biological_entity id="o3531" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3532" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheric" value_original="spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen in monads.</text>
      <biological_entity id="o3533" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3534" name="pollen" name_original="pollen" src="d0_s14" type="structure" />
      <biological_entity id="o3535" name="monad" name_original="monads" src="d0_s14" type="structure" />
      <relation from="o3534" id="r480" name="in" negation="false" src="d0_s14" to="o3535" />
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers floating;</text>
      <biological_entity id="o3536" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s15" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary 1-locular;</text>
      <biological_entity id="o3537" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 1, not 2-fid.</text>
      <biological_entity id="o3538" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits cylindric to ellipsoid, ridged, dehiscing irregularly.</text>
      <biological_entity id="o3539" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s18" to="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s18" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ellipsoid, glabrous.</text>
      <biological_entity id="o3540" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Wild-celery</other_name>
  <discussion>Vallisneria is generally considered to have 1-flowered pistillate inflorescences. A few populations in southern United States and Central America, however, have cymes with up to 30 flowers. The United States populations include those in Florida, Alabama, and Mississippi.</discussion>
  <discussion>Species 26 (1 in the flora).</discussion>
  <references>
    <reference>Fernald, M. L. 1918. The diagnostic character of Vallisneria americana. Rhodora 20: 108–110.</reference>
    <reference>Lowden, R. M. 1982. An approach to the taxonomy of Vallisneria L. (Hydrocharitaceae). Aquatic Bot. 13: 269–298.</reference>
    <reference>Marie-Victorin, Fr&amp;egrave;re. 1943. Les Vallisn&amp;eacute;ries Am&amp;eacute;ricaines. Contr. Inst. Bot. Univ. Montr&amp;eacute;al 46: 1–38.</reference>
  </references>
  
</bio:treatment>