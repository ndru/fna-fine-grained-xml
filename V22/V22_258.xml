<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">Gibasis</taxon_name>
    <place_of_publication>
      <publication_title>Flora Telluriana</publication_title>
      <place_in_publication>2: 16. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus Gibasis</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin gibbus, swollen, and basis, base</other_info_on_name>
    <other_info_on_name type="fna_id">113513</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial or annual.</text>
      <biological_entity id="o8970" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous [tuberous].</text>
      <biological_entity id="o8971" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2-ranked [spirally arranged];</text>
      <biological_entity id="o8972" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade sessile.</text>
      <biological_entity id="o8973" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and sometimes axillary, pairs or umbels of cymes, cymes pedunculate, axis sharply angled at junction with peduncle;</text>
      <biological_entity id="o8974" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o8975" name="umbel" name_original="umbels" src="d0_s4" type="structure" />
      <biological_entity id="o8976" name="cyme" name_original="cymes" src="d0_s4" type="structure" />
      <biological_entity id="o8977" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o8978" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character constraint="at junction" constraintid="o8979" is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o8979" name="junction" name_original="junction" src="d0_s4" type="structure" />
      <biological_entity id="o8980" name="peduncle" name_original="peduncle" src="d0_s4" type="structure" />
      <relation from="o8975" id="r1183" name="part_of" negation="false" src="d0_s4" to="o8976" />
      <relation from="o8979" id="r1184" name="with" negation="false" src="d0_s4" to="o8980" />
    </statement>
    <statement id="d0_s5">
      <text>spathaceous bract absent;</text>
      <biological_entity id="o8981" name="bract" name_original="bract" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spathaceous" value_original="spathaceous" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles persistent.</text>
      <biological_entity id="o8982" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o8983" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels well developed;</text>
      <biological_entity id="o8984" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s8" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals distinct, subequal;</text>
      <biological_entity id="o8985" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, white [to pink or blue], equal;</text>
      <biological_entity id="o8986" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, all fertile, equal;</text>
      <biological_entity id="o8987" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded;</text>
      <biological_entity id="o8988" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular, ovules 2 per locule, 1-seriate.</text>
      <biological_entity id="o8989" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity id="o8990" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character constraint="per locule" constraintid="o8991" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s13" value="1-seriate" value_original="1-seriate" />
      </biological_entity>
      <biological_entity id="o8991" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules 3-valved, 3-locular.</text>
      <biological_entity id="o8992" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2 per locule;</text>
      <biological_entity id="o8993" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character constraint="per locule" constraintid="o8994" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8994" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>hilum elongate-punctiform to linear;</text>
      <biological_entity id="o8995" name="hilum" name_original="hilum" src="d0_s16" type="structure">
        <character char_type="range_value" from="elongate-punctiform" name="shape" src="d0_s16" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>embryotega abaxial.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 4, 5, 6.</text>
      <biological_entity id="o8996" name="embryotegum" name_original="embryotega" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity constraint="x" id="o8997" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="4" value_original="4" />
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character name="quantity" src="d0_s18" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; neotropical, centered in Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="neotropical" establishment_means="introduced" />
        <character name="distribution" value="centered in Mexico" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <discussion>Species 11 (1 in the flora).</discussion>
  <references>
    <reference>Hunt, D. R. 1986c. A revision of Gibasis Rafin. American Commelinaceae: XII. Kew Bull. 41: 107–129.</reference>
    <reference>Rohweder, O. 1956. Commelinaceae in die Farinosae in der Vegetation von El Salvador. Abh. Auslandsk., Reihe C, Naturwiss. 18: 98–178.</reference>
  </references>
  
</bio:treatment>