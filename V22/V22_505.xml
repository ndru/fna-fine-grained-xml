<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">bromeliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tillandsia</taxon_name>
    <taxon_name authority="Swartz" date="1797" rank="species">pruinosa</taxon_name>
    <place_of_publication>
      <publication_title>Flora Indiae Occidentalis</publication_title>
      <place_in_publication>1: 594. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bromeliaceae;genus tillandsia;species pruinosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000400</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually single, rarely clustering, flowering to 10 cm.</text>
      <biological_entity id="o5197" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_growth_form" src="d0_s0" value="clustering" value_original="clustering" />
        <character is_modifier="false" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short.</text>
      <biological_entity id="o5198" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 5–10, many-ranked, contorted or secund-spreading, gray-green to silver, 6–10 × 0.3–0.5 cm, densely pruinose-scaly, scales coarse;</text>
      <biological_entity id="o5199" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="many-ranked" value_original="many-ranked" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="secund-spreading" value_original="secund-spreading" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s2" to="silver" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s2" value="pruinose-scaly" value_original="pruinose-scaly" />
      </biological_entity>
      <biological_entity id="o5200" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="coarse" value_original="coarse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath dark chestnut-brown within, ovate to elliptic, conspicuously inflated, forming small pseudobulb, 1.5–3 cm wide;</text>
      <biological_entity id="o5201" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark chestnut-brown" value_original="dark chestnut-brown" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5202" name="pseudobulb" name_original="pseudobulb" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
      </biological_entity>
      <relation from="o5201" id="r691" name="forming" negation="false" src="d0_s3" to="o5202" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear-subulate, semisucculent, margins involute, apex attenuate.</text>
      <biological_entity id="o5203" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="semisucculent" value_original="semisucculent" />
      </biological_entity>
      <biological_entity id="o5204" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o5205" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape inconspicuous, usually ascending, 1–3 cm, 2–3 mm diam.;</text>
      <biological_entity id="o5206" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o5207" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts densely imbricate, erect to spreading, like leaves but gradually smaller;</text>
      <biological_entity id="o5208" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o5209" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
      <biological_entity id="o5210" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o5209" id="r692" name="like" negation="false" src="d0_s6" to="o5210" />
    </statement>
    <statement id="d0_s7">
      <text>sheath of bracts narrowing abruptly into blade;</text>
      <biological_entity id="o5211" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o5212" name="sheath" name_original="sheath" src="d0_s7" type="structure">
        <character constraint="into blade" constraintid="o5214" is_modifier="false" name="width" src="d0_s7" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o5213" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o5214" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <relation from="o5212" id="r693" name="part_of" negation="false" src="d0_s7" to="o5213" />
    </statement>
    <statement id="d0_s8">
      <text>single spikes, usually ascending, pinnate, broadly elliptic, compressed, 2–3 × 1.2–2.4 cm, apex acute to obtuse.</text>
      <biological_entity id="o5215" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s8" to="2.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5216" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o5217" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Floral bracts imbricate, erect, pink, broad (covering all or most of rachis, rachis not visible at anthesis), ovate, keeled, 2–2.2 cm, thin-leathery, base not visible at anthesis, apex broadly acute, surfaces densely scaly, venation even to slight.</text>
      <biological_entity constraint="floral" id="o5218" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="2.2" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="thin-leathery" value_original="thin-leathery" />
      </biological_entity>
      <biological_entity id="o5219" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o5220" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5221" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s9" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="even" name="prominence" src="d0_s9" value="slight" value_original="slight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 3–12, conspicuous;</text>
      <biological_entity id="o5222" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="12" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals free, elliptic, adaxial pair keeled, 1.2–1.8 cm, thin-leathery, veined, apex obtuse, surfaces glabrous;</text>
      <biological_entity id="o5223" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character constraint="pair keeled , 1.2-1.8 cm" is_modifier="false" name="position" src="d0_s11" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="texture" src="d0_s11" value="thin-leathery" value_original="thin-leathery" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o5224" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o5225" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla tubular;</text>
      <biological_entity id="o5226" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="tubular" value_original="tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals erect, blue-violet, ligulate, to 3 cm;</text>
      <biological_entity id="o5227" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blue-violet" value_original="blue-violet" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s13" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted;</text>
      <biological_entity id="o5228" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma exserted, conduplicate-spiral.</text>
      <biological_entity id="o5229" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s15" value="conduplicate-spiral" value_original="conduplicate-spiral" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits to 3.5 cm.</text>
      <biological_entity id="o5230" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s16" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic in shady, humid hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="epiphytic" constraint="in shady , humid hammocks" />
        <character name="habitat" value="shady" />
        <character name="habitat" value="humid hammocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <discussion>In the flora area, Tillandsia pruinosa is infrequent but locally abundant.</discussion>
  
</bio:treatment>