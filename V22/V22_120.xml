<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Septati</taxon_name>
    <taxon_name authority="M. A. Curtis" date="1835" rank="species">megacephalus</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>1: 132. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus septati;species megacephalus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000155</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">scirpoides</taxon_name>
    <taxon_name authority="Coville" date="unknown" rank="variety">carolinianus</taxon_name>
    <taxon_hierarchy>genus Juncus;species scirpoides;variety carolinianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scirpoides</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">echinatus</taxon_name>
    <taxon_hierarchy>genus Juncus;species scirpoides;variety echinatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, 3–11 dm.</text>
      <biological_entity id="o9676" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="11" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes 3–4 diam.</text>
      <biological_entity id="o9677" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="diameter" src="d0_s1" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect, terete, 3–4 mm diam., smooth.</text>
      <biological_entity id="o9678" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1–2, purple, apex acute.</text>
      <biological_entity id="o9679" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s3" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o9680" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 0–1, cauline 2–3;</text>
      <biological_entity id="o9681" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o9682" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="1" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9683" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 0.5–2 mm, apex acute, membranaceous;</text>
      <biological_entity id="o9684" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9685" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9686" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade terete, 0–24 cm × 0.5–1.7 mm, most distal cauline leaf-blade 0–2 cm, shorter than sheath.</text>
      <biological_entity id="o9687" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o9688" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="24" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o9689" name="blade-leaf" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="distance" src="d0_s6" to="2" to_unit="cm" />
        <character constraint="than sheath" constraintid="o9690" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9690" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences panicles of (1–) 3–21 heads, 1–8 cm, branches erect to spreading;</text>
      <biological_entity constraint="inflorescences" id="o9691" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9692" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
      <biological_entity id="o9693" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="spreading" />
      </biological_entity>
      <relation from="o9691" id="r1293" name="consist_of" negation="false" src="d0_s7" to="o9692" />
    </statement>
    <statement id="d0_s8">
      <text>primary bract erect;</text>
      <biological_entity constraint="primary" id="o9694" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>heads 40–60-flowered, spheric, 8–12 mm diam.</text>
      <biological_entity id="o9695" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="40-60-flowered" value_original="40-60-flowered" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals straw-colored to reddish-brown, lanceolate-subulate;</text>
      <biological_entity id="o9696" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9697" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="straw-colored" name="coloration" src="d0_s10" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate-subulate" value_original="lanceolate-subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer tepals 2.9–4.1 mm, apex acuminate;</text>
      <biological_entity id="o9698" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o9699" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s11" to="4.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9700" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner tepals 2.2–3.7 mm, apex acuminate;</text>
      <biological_entity id="o9701" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o9702" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9703" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 3, anthers 1/4–1/2 filament length.</text>
      <biological_entity id="o9704" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9705" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o9706" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s13" to="1/2" />
      </biological_entity>
      <biological_entity id="o9707" name="filament" name_original="filament" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules exserted,, straw-colorerd, 1-locular, subulate, 2.5–4.2 mm, apex tapering to subulate beak, valves not separat ing at dehiscence, fertile throughout or only proximal to middle.</text>
      <biological_entity id="o9708" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="straw-colorerd" value_original="straw-colorerd" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9709" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="tapering" name="shape" src="d0_s14" to="subulate" />
      </biological_entity>
      <biological_entity id="o9710" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity id="o9711" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="throughout; throughout" name="reproduction" src="d0_s14" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="only proximal" name="position" src="d0_s14" to="middle" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds ellipsoid to ovoid, 0.4 mm, not tailed;</text>
      <biological_entity id="o9712" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="ovoid" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.4" value_original="0.4" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>body clear yellowbrown.</text>
      <biological_entity id="o9713" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="clear yellowbrown" value_original="clear yellowbrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh marshes, moist hollows of sand dunes, swales, roadside ditches, and dry fertile soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh marshes" />
        <character name="habitat" value="moist hollows" constraint="of sand dunes , swales , roadside ditches , and dry fertile soil" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="dry fertile soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Md., Miss., N.C., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>77</number>
  
</bio:treatment>