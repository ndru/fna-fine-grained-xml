<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Scheidweiler" date="1839" rank="genus">tinantia</taxon_name>
    <taxon_name authority="(Torrey) C. B. Clarke in A. L. P. de Candolle and C. de Candolle" date="1881" rank="species">anomala</taxon_name>
    <place_of_publication>
      <publication_title>in A. L. P. de Candolle and C. de Candolle,Monographiae Phanerogamarum</publication_title>
      <place_in_publication>3: 287. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tinantia;species anomala</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000407</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">anomala</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 225. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tradescantia;species anomala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Commelinantia</taxon_name>
    <taxon_name authority="(Torrey) Tharp" date="unknown" rank="species">anomala</taxon_name>
    <taxon_hierarchy>genus Commelinantia;species anomala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose, to 80 cm.</text>
      <biological_entity id="o7627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves dimorphic, glaucous;</text>
      <biological_entity id="o7628" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal leaves tapered into long petiole, linear-spatulate;</text>
      <biological_entity constraint="basal" id="o7629" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="into petiole" constraintid="o7630" is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="linear-spatulate" value_original="linear-spatulate" />
      </biological_entity>
      <biological_entity id="o7630" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal cauline leaves sessile, broadly to narrowly lanceolate, 6–20 cm, base commonly cordate-clasping, apex acute to acuminate.</text>
      <biological_entity constraint="distal cauline" id="o7631" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7632" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="commonly" name="architecture_or_fixation" src="d0_s3" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o7633" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, cymes solitary;</text>
      <biological_entity id="o7634" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o7635" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracteoles 3–5 mm.</text>
      <biological_entity id="o7636" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2–2.5 cm wide;</text>
      <biological_entity id="o7637" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals boatshaped, 9–12 mm, glabrous;</text>
      <biological_entity id="o7638" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="boat-shaped" value_original="boat-shaped" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal petal white, rhombic, 3–4 mm;</text>
      <biological_entity constraint="proximal" id="o7639" name="petal" name_original="petal" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>distal petals blue to lavender, obovate, 15–18 mm;</text>
      <biological_entity constraint="distal" id="o7640" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="lavender" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal lateral stamens with purple hairs;</text>
      <biological_entity constraint="proximal lateral" id="o7641" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o7642" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o7641" id="r1019" name="with" negation="false" src="d0_s10" to="o7642" />
    </statement>
    <statement id="d0_s11">
      <text>distal stamens bearded with yellow-tipped hairs;</text>
      <biological_entity constraint="distal" id="o7643" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character constraint="with hairs" constraintid="o7644" is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o7644" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="yellow-tipped" value_original="yellow-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 2 per locule.</text>
      <biological_entity id="o7645" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character constraint="per locule" constraintid="o7646" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7646" name="locule" name_original="locule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules oblong, 6–8 mm.</text>
      <biological_entity id="o7647" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds gray-brown.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 26.</text>
      <biological_entity id="o7648" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray-brown" value_original="gray-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7649" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone talus slopes, granitic slopes, edges of woods and ravines, prefers some shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone talus slopes" />
        <character name="habitat" value="granitic slopes" />
        <character name="habitat" value="edges" constraint="of woods and ravines" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="some shade" modifier="prefers" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">False dayflower</other_name>
  <other_name type="common_name">widow's-tears</other_name>
  <discussion>Only one record of Tinantia anomala is known from Durango.</discussion>
  
</bio:treatment>