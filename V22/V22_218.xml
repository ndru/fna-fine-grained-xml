<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Poiophylli</taxon_name>
    <taxon_name authority="(Wiegand) R. E. Brooks" date="1999" rank="species">anthelatus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 11. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus poiophylli;species anthelatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">tenuis</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="variety">anthelatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club.</publication_title>
      <place_in_publication>27: 523. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species tenuis;variety anthelatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Gray" date="unknown" rank="species">macer</taxon_name>
    <taxon_name authority="(Wiegand) F. J. Hermann" date="unknown" rank="variety">anthelatus</taxon_name>
    <taxon_hierarchy>genus Juncus;species macer;variety anthelatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, to (3–) 7–9 dm.</text>
      <biological_entity id="o13626" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cataphylls 1–2.</text>
      <biological_entity id="o13627" name="cataphyll" name_original="cataphylls" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, 2–3 (–5);</text>
      <biological_entity id="o13628" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles transparent, 2–3.5 mm at summit of leaf-sheath, apex acutish, membranous;</text>
      <biological_entity id="o13629" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="transparent" value_original="transparent" />
        <character char_type="range_value" constraint="at summit" constraintid="o13630" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13630" name="summit" name_original="summit" src="d0_s3" type="structure" />
      <biological_entity id="o13631" name="sheath" name_original="leaf-sheath" src="d0_s3" type="structure" />
      <biological_entity id="o13632" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acutish" value_original="acutish" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o13630" id="r1820" name="part_of" negation="false" src="d0_s3" to="o13631" />
    </statement>
    <statement id="d0_s4">
      <text>blade flat, (10–) 20–30 cm × 0.5–2.3 mm, margins entire.</text>
      <biological_entity id="o13633" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" src="d0_s4" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13634" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 10–100-flowered, diffuse;</text>
      <biological_entity id="o13635" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="10-100-flowered" value_original="10-100-flowered" />
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>internodes of monochasia greater than 6 mm, primary bract usually exceeding inflorescence.</text>
      <biological_entity id="o13636" name="internode" name_original="internodes" src="d0_s6" type="structure" constraint="monochasium" constraint_original="monochasium; monochasium">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o13637" name="monochasium" name_original="monochasia" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o13638" name="bract" name_original="bract" src="d0_s6" type="structure" />
      <biological_entity id="o13639" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
      <relation from="o13636" id="r1821" name="part_of" negation="false" src="d0_s6" to="o13637" />
      <relation from="o13638" id="r1822" modifier="usually" name="exceeding" negation="false" src="d0_s6" to="o13639" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicels 0.1–0.3 (–3) mm;</text>
      <biological_entity id="o13640" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13641" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 2;</text>
      <biological_entity id="o13642" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13643" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals green, lanceolate, 3.2–4.5 × 0.7–1 mm;</text>
      <biological_entity id="o13644" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13645" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer and inner series nearly equal, in fruit apically erect;</text>
      <biological_entity id="o13646" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer and inner" id="o13647" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o13648" name="fruit" name_original="fruit" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="apically" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o13647" id="r1823" name="in" negation="false" src="d0_s10" to="o13648" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, filaments 0.8–1.1 mm, anthers 0.3–0.7 mm;</text>
      <biological_entity id="o13649" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13650" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o13651" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13652" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 0.2 mm.</text>
      <biological_entity id="o13653" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13654" name="style" name_original="style" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules tan, 1-locular, widely ellipsoid to obovoid, 2–3.2 × 1.1–1.6 mm.</text>
      <biological_entity id="o13655" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="1-locular" value_original="1-locular" />
        <character char_type="range_value" from="widely ellipsoid" name="shape" src="d0_s13" to="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s13" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds tan, ellipsoid, 0.33–0.556 mm, not tailed.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 80.</text>
      <biological_entity id="o13656" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.33" from_unit="mm" name="some_measurement" src="d0_s14" to="0.556" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13657" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed or partially shaded sites in moist or seasonally wet, sandy or clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="exposed or partially shaded" constraint="in moist or seasonally wet , sandy or clay soils" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="wet" modifier="seasonally" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="shaded" modifier="partially seasonally" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25</number>
  
</bio:treatment>