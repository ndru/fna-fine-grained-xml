<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">alismataceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">sagittaria</taxon_name>
    <taxon_name authority="E. Sheldon" date="1893" rank="species">cuneata</taxon_name>
    <place_of_publication>
      <publication_title>Bulletin of the Torrey Botanical Club</publication_title>
      <place_in_publication>20:283, plate 159. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family alismataceae;genus sagittaria;species cuneata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000331</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagittaria</taxon_name>
    <taxon_name authority="Nuttall ex J. G. Smith" date="unknown" rank="species">arifolia</taxon_name>
    <taxon_hierarchy>genus Sagittaria;species arifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, to 110 cm;</text>
      <biological_entity id="o657" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent;</text>
      <biological_entity id="o658" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stolons present;</text>
      <biological_entity id="o659" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corms present.</text>
      <biological_entity id="o660" name="corm" name_original="corms" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves emersed, floating, and submersed;</text>
    </statement>
    <statement id="d0_s5">
      <text>submersed phyllodial, flattened, to 45 cm;</text>
    </statement>
    <statement id="d0_s6">
      <text>floating with petiole triangular, to 100 cm, blade cordate or sagittate, rarely linear or ovate, 7.5–9 × 3.5–4 cm;</text>
      <biological_entity id="o661" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="location" src="d0_s4" value="emersed" value_original="emersed" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s4" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s4" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="location" src="d0_s5" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="45" to_unit="cm" />
        <character constraint="with petiole" constraintid="o662" is_modifier="false" name="growth_form_or_location" src="d0_s6" value="floating" value_original="floating" />
      </biological_entity>
      <biological_entity id="o662" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>emersed with petiole recurved, 3.5–51 cm, blade linear to sagittate, 2.5–17 × 1.5–11 cm, basal lobes when present shorter than remainder of blade.</text>
      <biological_entity id="o663" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s6" to="4" to_unit="cm" />
        <character constraint="with petiole" constraintid="o664" is_modifier="false" name="location" src="d0_s7" value="emersed" value_original="emersed" />
      </biological_entity>
      <biological_entity id="o664" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s7" to="51" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o665" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="sagittate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s7" to="17" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o666" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences racemes, rarely panicles, of 2–10 whorls, emersed, 14–21 × 2–10 cm, peduncle triangular, 10–50 cm;</text>
      <biological_entity constraint="inflorescences" id="o667" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="location" notes="" src="d0_s8" value="emersed" value_original="emersed" />
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s8" to="21" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o668" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
      <biological_entity id="o669" name="whorl" name_original="whorls" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o670" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="50" to_unit="cm" />
      </biological_entity>
      <relation from="o667" id="r93" name="consist_of" negation="false" src="d0_s8" to="o669" />
    </statement>
    <statement id="d0_s9">
      <text>bracts connate more than or equal to ¼ total length, lance-attenuate or acute, mostly (4–) 7–40 mm, membranous, not papillose;</text>
    </statement>
    <statement id="d0_s10">
      <text>fruiting pedicels ascending, cylindric, 0.5–2 cm.</text>
      <biological_entity id="o671" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="more than" value_original="more than" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal to ¼ total length" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lance-attenuate" value_original="lance-attenuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character char_type="range_value" from="4" from_unit="mm" modifier="mostly" name="atypical_some_measurement" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s9" to="40" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o672" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o671" id="r94" name="fruiting" negation="false" src="d0_s10" to="o672" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers to 25 mm diam.;</text>
      <biological_entity id="o673" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals recurved, not enclosing flower or fruiting head;</text>
      <biological_entity id="o674" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o675" name="flower" name_original="flower" src="d0_s12" type="structure" />
      <biological_entity id="o676" name="head" name_original="head" src="d0_s12" type="structure" />
      <relation from="o674" id="r95" name="enclosing" negation="true" src="d0_s12" to="o675" />
      <relation from="o674" id="r96" name="enclosing" negation="false" src="d0_s12" to="o676" />
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated, equal to or longer than anthers, glabrous;</text>
      <biological_entity id="o678" name="anther" name_original="anthers" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pistillate pedicellate, without ring of sterile stamens.</text>
      <biological_entity id="o679" name="ring" name_original="ring" src="d0_s14" type="structure" constraint="stamen" constraint_original="stamen; stamen">
        <character is_modifier="false" name="architecture" src="d0_s14" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o680" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o679" id="r97" name="part_of" negation="false" src="d0_s14" to="o680" />
    </statement>
    <statement id="d0_s15">
      <text>Fruiting heads 0.8–1.5 cm diam.;</text>
      <biological_entity id="o677" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character constraint="than anthers" constraintid="o678" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="diameter" src="d0_s15" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o681" name="head" name_original="heads" src="d0_s15" type="structure" />
      <relation from="o677" id="r98" name="fruiting" negation="false" src="d0_s15" to="o681" />
    </statement>
    <statement id="d0_s16">
      <text>achenes obovoid, abaxially keeled, 1.8–2.6 × 1.3–2.5 mm, beaked;</text>
      <biological_entity id="o682" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s16" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>face not tuberculate, wings 0–1, entire, glands 0–1;</text>
      <biological_entity id="o683" name="face" name_original="face" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s17" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o684" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o685" name="gland" name_original="glands" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>beak apical, erect, 0.1–0.4 mm. 2n = 22.</text>
      <biological_entity id="o686" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="apical" value_original="apical" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o687" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous and muddy shores and shallow waters of rivers, lakes, ponds, pastures, and ditches, occasional in tidal waters, or in deep flowing water with slow current</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="muddy shores" />
        <character name="habitat" value="shallow waters" constraint="of rivers , lakes , ponds , pastures , and ditches" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="tidal waters" modifier="occasional in" />
        <character name="habitat" value="water" modifier="or in deep flowing" constraint="with slow current" />
        <character name="habitat" value="slow current" modifier="with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Labr.), N.W.T., N.S., Ont., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Okla., Oreg., Pa., S.Dak., Tex., Utah, Vt., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19</number>
  <other_name type="common_name">Northern arrowhead</other_name>
  <other_name type="common_name">wapato</other_name>
  <other_name type="common_name">Sagittaria cuneaire</other_name>
  <discussion>Sagittaria cuneata is extremely variable. On emersed plants, the leaf petioles are often bent toward the ground. Submersed plants often grow from a basal rosette with a long flexuous petiole and a floating sagittate leave. Plants in deep rivers often develop broad, straplike phyllodia.</discussion>
  
</bio:treatment>