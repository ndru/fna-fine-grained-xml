<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">lemnaceae</taxon_name>
    <taxon_name authority="Horkel ex Schleiden" date="1844" rank="genus">wolffia</taxon_name>
    <taxon_name authority="(Engelmann) Landolt" date="1977" rank="species">borealis</taxon_name>
    <place_of_publication>
      <publication_title>Ber. Geobot. Inst. ETH Stiftung Rubel</publication_title>
      <place_in_publication>44:137. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lemnaceae;genus wolffia;species borealis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000451</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Wolffia</taxon_name>
    <taxon_name authority="Weddell" date="unknown" rank="species">brasiliensis</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">borealis</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. Hegelmaier, Lemnac.,</publication_title>
      <place_in_publication>127. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Wolffia;species brasiliensis;variety borealis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Fronds boatshaped, 0.7–1.5 mm, 1.3–2 times as long as wide, 0.7–1 times as deep as wide, with point at apex bent upward;</text>
      <biological_entity id="o4390" name="frond" name_original="fronds" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="boat-shaped" value_original="boat-shaped" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s0" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s0" value="1.3-2" value_original="1.3-2" />
        <character is_modifier="false" name="width" src="d0_s0" value="0.7-1 times as deep as wide" />
      </biological_entity>
      <biological_entity id="o4391" name="point" name_original="point" src="d0_s0" type="structure" />
      <biological_entity id="o4392" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="bent" value_original="bent" />
      </biological_entity>
      <relation from="o4390" id="r582" name="with" negation="false" src="d0_s0" to="o4391" />
      <relation from="o4391" id="r583" name="at" negation="false" src="d0_s0" to="o4392" />
    </statement>
    <statement id="d0_s1">
      <text>papilla absent;</text>
      <biological_entity id="o4393" name="papilla" name_original="papilla" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>upper surface intensely green, with 50–100 stomates;</text>
      <biological_entity constraint="upper" id="o4394" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="intensely" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o4395" name="stomate" name_original="stomates" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" is_modifier="true" name="quantity" src="d0_s2" to="100" />
      </biological_entity>
      <relation from="o4394" id="r584" name="with" negation="false" src="d0_s2" to="o4395" />
    </statement>
    <statement id="d0_s3">
      <text>pigment cells present in vegetative tissue (visible in dead fronds as brown dots).</text>
      <biological_entity id="o4397" name="tissue" name_original="tissue" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o4396" id="r585" name="present in" negation="false" src="d0_s3" to="o4397" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 20, 22, 30, 40.</text>
      <biological_entity id="o4396" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="pigment" value_original="pigment" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4398" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="20" value_original="20" />
        <character name="quantity" src="d0_s4" value="22" value_original="22" />
        <character name="quantity" src="d0_s4" value="30" value_original="30" />
        <character name="quantity" src="d0_s4" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (very rare) summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="very rare" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesotrophic to eutrophic, quiet waters in temperate regions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesotrophic to eutrophic" constraint="in temperate regions" />
        <character name="habitat" value="mesotrophic to quiet waters" constraint="in temperate regions" />
        <character name="habitat" value="temperate regions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont., Que.; Calif., Colo., Idaho, Ill., Ind., Iowa, Kans., Ky., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.Y., Ohio, Okla., Oreg., Pa., S.Dak., Tenn., Utah, Vt., Wash., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Wolffie boréale</other_name>
  <discussion>The name Wolffia punctata has been applied to this species in error.</discussion>
  <discussion>I know of no specimens of Wolffia borealis from Rhode Island.</discussion>
  
</bio:treatment>