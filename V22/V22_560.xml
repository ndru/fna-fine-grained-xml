<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert B. Faden</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">168</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kunth" date="unknown" rank="family">Mayacaceae</taxon_name>
    <taxon_hierarchy>family Mayacaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10543</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o15434" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline, alternate;</text>
      <biological_entity id="o15435" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sheath absent;</text>
      <biological_entity id="o15436" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade simple, linear to filiform, margins entire, 1-veined.</text>
      <biological_entity id="o15437" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="filiform" />
      </biological_entity>
      <biological_entity id="o15438" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: solitary flowers that appear lateral.</text>
      <biological_entity id="o15439" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o15440" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o15441" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals 3, sepaloid, distinct, equal;</text>
      <biological_entity id="o15442" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sepaloid" value_original="sepaloid" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 3, petaloid, distinct, equal;</text>
      <biological_entity id="o15443" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 3;</text>
      <biological_entity id="o15444" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminodes absent;</text>
      <biological_entity id="o15445" name="staminode" name_original="staminodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers dehiscing by apical pores or porelike slits;</text>
      <biological_entity id="o15446" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="by slits" constraintid="o15448" is_modifier="false" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity constraint="apical" id="o15447" name="pore" name_original="pores" src="d0_s10" type="structure" />
      <biological_entity id="o15448" name="slit" name_original="slits" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="pore-like" value_original="porelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 3-carpellate, 1-locular;</text>
      <biological_entity id="o15449" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-carpellate" value_original="3-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 2-seriate;</text>
      <biological_entity id="o15450" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s12" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 1, simple;</text>
      <biological_entity id="o15451" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas 1, simple or slightly 3-fid.</text>
      <biological_entity id="o15452" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits loculicidal capsules.</text>
      <biological_entity id="o15453" name="fruit" name_original="fruits" src="d0_s15" type="structure" />
      <biological_entity id="o15454" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds: embryotega present.</text>
      <biological_entity id="o15455" name="seed" name_original="seeds" src="d0_s16" type="structure" />
      <biological_entity id="o15456" name="embryotegum" name_original="embryotega" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>3 in warm temperate to tropical America and 1 in West Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="3 in warm temperate to tropical America and 1 in West Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>206</number>
  <other_name type="common_name">Mayaca or Bog-moss Family</other_name>
  <discussion>Genera 1, species ca. 4 (1 species in the flora).</discussion>
  
</bio:treatment>