<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">repens</taxon_name>
    <place_of_publication>
      <publication_title>Flora Boreali-Americana</publication_title>
      <place_in_publication>1: 191. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species repens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000171</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, floriferous culm 0.5–3 dm.</text>
      <biological_entity id="o16779" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o16780" name="culm" name_original="culm" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="floriferous" value_original="floriferous" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms first ascending, soon arcuate-stoloniferous and creeping or floating, or growing submersed along bottom, each node with cluster of basal leaves and fibrous-roots, eventually each emergent terrestrial node with floriferous culm.</text>
      <biological_entity id="o16781" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="soon" name="architecture" src="d0_s1" value="arcuate-stoloniferous" value_original="arcuate-stoloniferous" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
      <biological_entity id="o16782" name="bottom" name_original="bottom" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="submersed" value_original="submersed" />
      </biological_entity>
      <biological_entity id="o16783" name="node" name_original="node" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o16784" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16785" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure" />
      <biological_entity id="o16786" name="node" name_original="node" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="emergent" value_original="emergent" />
        <character is_modifier="true" name="growth_form_or_habitat" src="d0_s1" value="terrestrial" value_original="terrestrial" />
      </biological_entity>
      <biological_entity id="o16787" name="culm" name_original="culm" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="floriferous" value_original="floriferous" />
      </biological_entity>
      <relation from="o16781" id="r2161" name="growing" negation="false" src="d0_s1" to="o16782" />
      <relation from="o16783" id="r2162" modifier="with cluster" name="part_of" negation="false" src="d0_s1" to="o16784" />
      <relation from="o16783" id="r2163" modifier="with cluster" name="part_of" negation="false" src="d0_s1" to="o16785" />
      <relation from="o16786" id="r2164" modifier="eventually" name="with" negation="false" src="d0_s1" to="o16787" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o16788" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles 0.5–1 mm, apex acutish, membranous or thicker;</text>
      <biological_entity id="o16789" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16790" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acutish" value_original="acutish" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="width" src="d0_s3" value="thicker" value_original="thicker" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spreading, flat, 2–10 (–15) cm × 1–3 mm.</text>
      <biological_entity id="o16791" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences glomerules, (1–) 2–10, each with 3–12 flowers, open;</text>
      <biological_entity constraint="inflorescences" id="o16792" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o16793" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <relation from="o16792" id="r2165" name="with" negation="false" src="d0_s5" to="o16793" />
    </statement>
    <statement id="d0_s6">
      <text>primary bract usually shorter than inflorescence.</text>
      <biological_entity constraint="primary" id="o16794" name="bract" name_original="bract" src="d0_s6" type="structure">
        <character constraint="than inflorescence" constraintid="o16795" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o16795" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: tepals green, margins scarious;</text>
      <biological_entity id="o16796" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16797" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o16798" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner series narrowly lanceolate, 5–9 mm, apex usually recurved;</text>
      <biological_entity id="o16799" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o16800" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16801" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer series obviously shorter, apex usually erect;</text>
      <biological_entity id="o16802" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o16803" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="obviously" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o16804" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3, filaments 1.5–3 mm, anthers 0.5–0.8 mm;</text>
      <biological_entity id="o16805" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16806" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o16807" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16808" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.5 mm.</text>
      <biological_entity id="o16809" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16810" name="style" name_original="style" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules tan, 3-locular, narrowly ellipsoid, 3.5–5.5 × 0.8–1.2 mm.</text>
      <biological_entity id="o16811" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, ovoid, 0.3–0.4 mm, not tailed.</text>
      <biological_entity id="o16812" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shores of ponds, lakes, and borrow pits, flatwood depressions, ditches, and drainage canals</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" constraint="of ponds , lakes , and borrow pits , flatwood depressions , ditches , and drainage canals" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="borrow pits" />
        <character name="habitat" value="flatwood depressions" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="drainage canals" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., La., Md., Miss., N.C., Okla., S.C., Tenn., Tex., Va.; Mexico (Tabasco); West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico (Tabasco)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39</number>
  <other_name type="common_name">Creeping rush</other_name>
  
</bio:treatment>