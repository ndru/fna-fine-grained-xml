<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Palisot de Beauvois ex Desvaux" date="unknown" rank="family">eriocaulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">eriocaulon</taxon_name>
    <taxon_name authority="B. L. Robinson" date="1903" rank="species">parkeri</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>5: 175. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family eriocaulaceae;genus eriocaulon;species parkeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000068</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 10–20 (–30) cm.</text>
      <biological_entity id="o5420" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves linear-attenuate, 2–6 (–9) cm, apex filiform-terete.</text>
      <biological_entity id="o5421" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-attenuate" value_original="linear-attenuate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5422" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform-terete" value_original="filiform-terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: scape sheaths slightly longer or slightly shorter than leaves, loose;</text>
      <biological_entity id="o5423" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="scape" id="o5424" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
        <character constraint="than leaves" constraintid="o5425" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o5425" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>scapes linear, 0.5–1 mm wide, 4–5-ribbed;</text>
      <biological_entity id="o5426" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o5427" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="4-5-ribbed" value_original="4-5-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mature heads dull gray or lead-colored, rarely straw-colored, hemispheric to subglobose, 3–4 mm wide, mostly nearly glabrous;</text>
      <biological_entity id="o5428" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o5429" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="lead-colored" value_original="lead-colored" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s4" to="subglobose" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="mostly nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>receptacle glabrous;</text>
      <biological_entity id="o5430" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o5431" name="receptacle" name_original="receptacle" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer involucral-bracts usually not reflexed, not obscured by braceteoles and perianth, straw-colored, greenish, or light gray to gray, dull, ovate to suborbiculate or obovate, 2 mm, margins often erose or lacerate, apex blunt, glabrous;</text>
      <biological_entity id="o5432" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="outer" id="o5433" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually not" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character constraint="by perianth" constraintid="o5434" is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="obscured" value_original="obscured" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="light gray" name="coloration" src="d0_s6" to="gray" />
        <character char_type="range_value" from="light gray" name="coloration" src="d0_s6" to="gray" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="suborbiculate or obovate" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5434" name="perianth" name_original="perianth" src="d0_s6" type="structure" />
      <biological_entity id="o5435" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o5436" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner bracts, receptacular bracteoles grayish, cuneate to narrowly obovate, 2 mm, margins often erose or lacerate, apex obtuse, glabrous or with a few white hairs abaxially at apex.</text>
      <biological_entity id="o5437" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o5438" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity constraint="receptacular" id="o5439" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="narrowly obovate" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5440" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o5441" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="with a few white hairs" />
      </biological_entity>
      <biological_entity id="o5442" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o5443" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o5441" id="r728" name="with" negation="false" src="d0_s7" to="o5442" />
      <relation from="o5442" id="r729" name="at" negation="false" src="d0_s7" to="o5443" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 2, gray, linear to oblong or oblanceolate, 2 mm, apex obtuse, glabrous or with a few white hairs abaxially at apex;</text>
      <biological_entity id="o5444" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5445" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gray" value_original="gray" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="oblong or oblanceolate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5446" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with a few white hairs" />
      </biological_entity>
      <biological_entity id="o5447" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o5448" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o5446" id="r730" name="with" negation="false" src="d0_s8" to="o5447" />
      <relation from="o5447" id="r731" name="at" negation="false" src="d0_s8" to="o5448" />
    </statement>
    <statement id="d0_s9">
      <text>androphore club-shaped;</text>
      <biological_entity id="o5449" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5450" name="androphore" name_original="androphore" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 2, triangular, minute, white-hairy;</text>
      <biological_entity id="o5451" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5452" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 4;</text>
      <biological_entity id="o5453" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5454" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers black.</text>
      <biological_entity id="o5455" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5456" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: sepals 2, gray, oblong or oblanceolate, 2 mm, scarious, apex obtuse, abaxially hairy apically;</text>
      <biological_entity id="o5457" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5458" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o5459" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abaxially; apically" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 2, yellow-white, spatulate, 2 mm, apex obtuse, glabrous or with a few white, club-shaped hairs apically, adaxially;</text>
      <biological_entity id="o5460" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5461" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow-white" value_original="yellow-white" />
        <character is_modifier="false" name="shape" src="d0_s14" value="spatulate" value_original="spatulate" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5462" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="with a few white , club-shaped hairs" />
      </biological_entity>
      <biological_entity id="o5463" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="few" value_original="few" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s14" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o5462" id="r732" modifier="adaxially" name="with" negation="false" src="d0_s14" to="o5463" />
    </statement>
    <statement id="d0_s15">
      <text>pistil 2-carpellate.</text>
      <biological_entity id="o5464" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5465" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds redbrown, ovoid to broadly ellipsoid, 0.5 (–7) mm, with delicate reticulum of horizontally oriented alveolae.</text>
      <biological_entity id="o5466" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="broadly ellipsoid" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o5467" name="reticulum" name_original="reticulum" src="d0_s16" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s16" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity id="o5468" name="alveola" name_original="alveolae" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="horizontally" name="orientation" src="d0_s16" value="oriented" value_original="oriented" />
      </biological_entity>
      <relation from="o5466" id="r733" name="with" negation="false" src="d0_s16" to="o5467" />
      <relation from="o5467" id="r734" name="part_of" negation="false" src="d0_s16" to="o5468" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Muddy tidewater banks, brackish marsh, mud flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="muddy tidewater banks" />
        <character name="habitat" value="brackish marsh" />
        <character name="habitat" value="mud flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Que.; Conn., Del., Maine, Md., Mass., N.J., N.Y., N.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">&amp;Eacute;riocaulon de Parker</other_name>
  <discussion>A considerable amount of transitional material occurs between Eriocaulon parkeri and E. aquaticum at places along coastal streams where brackish habitat meets more acid habitat upstream.</discussion>
  
</bio:treatment>