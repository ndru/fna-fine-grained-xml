<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Norman Taylor" date="unknown" rank="family">cymodoceaceae</taxon_name>
    <taxon_name authority="Kutzing in R. F. Hohenacker" date="1860" rank="genus">Syringodium</taxon_name>
    <place_of_publication>
      <publication_title>in R. F. Hohenacker,Algae Marinae Exsiccatae</publication_title>
      <place_in_publication>9: 426. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cymodoceaceae;genus Syringodium</taxon_hierarchy>
    <other_info_on_name type="fna_id">132147</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, subtended by ovate scales.</text>
      <biological_entity id="o6789" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6790" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="ovate" value_original="ovate" />
      </biological_entity>
      <relation from="o6789" id="r896" name="subtended by" negation="false" src="d0_s0" to="o6790" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2–3;</text>
      <biological_entity id="o6791" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade terete or semiterete, apex acute to obtuse, lacunae 2;</text>
      <biological_entity id="o6792" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s2" value="semiterete" value_original="semiterete" />
      </biological_entity>
      <biological_entity id="o6793" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
      <biological_entity id="o6794" name="lacuna" name_original="lacunae" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>veins 1–2 [–7–10], midrib inconspicuous, not obviously widened distally, lateral-veins inconspicuous, not ending in tooth.</text>
      <biological_entity id="o6795" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="7-10" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <biological_entity id="o6796" name="midrib" name_original="midrib" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="not obviously; distally" name="width" src="d0_s3" value="widened" value_original="widened" />
      </biological_entity>
      <biological_entity id="o6797" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o6798" name="tooth" name_original="tooth" src="d0_s3" type="structure" />
      <relation from="o6797" id="r897" name="ending in" negation="true" src="d0_s3" to="o6798" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose.</text>
      <biological_entity id="o6799" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers axillary to bract, inflated sheath present.</text>
      <biological_entity id="o6800" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="to bract" constraintid="o6801" is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o6801" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o6802" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: anthers attached at same height on axis.</text>
      <biological_entity id="o6803" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6804" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character constraint="at axis" constraintid="o6805" is_modifier="false" name="fixation" src="d0_s6" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o6805" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="height" value_original="height" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers sessile, styles divided into 2 long stigmas.</text>
      <biological_entity id="o6806" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6807" name="style" name_original="styles" src="d0_s7" type="structure">
        <character constraint="into stigmas" constraintid="o6808" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o6808" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits drupaceous, obovoid [ellipsoid], quadrangular in cross-section.</text>
      <biological_entity id="o6809" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character constraint="in cross-section" constraintid="o6810" is_modifier="false" name="shape" src="d0_s8" value="quadrangular" value_original="quadrangular" />
      </biological_entity>
      <biological_entity id="o6810" name="cross-section" name_original="cross-section" src="d0_s8" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Warm oceanic waters of both hemispheres.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Warm oceanic waters of both hemispheres" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Manatee-grass</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  <references>
    <reference>Cox, P. A., T. Elmqvist, and P. B. Tomlinson. 1990. Submarine pollination and reproductive morphology in Syringodium filiforme (Cymodoceaceae). Biotropica 22: 259–265.</reference>
    <reference>Tomlinson, P. B. and U. Posluszny. 1978. Aspects of floral morphology and development in the seagrass Syringodium filiforme (Cymodoceaceae). Bot. Gaz. 139: 333–345.</reference>
  </references>
  
</bio:treatment>