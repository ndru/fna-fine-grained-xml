<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">elodea</taxon_name>
    <taxon_name authority="(Planchon) H. St. John" date="1920" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>22:29. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus elodea;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000059</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anacharis</taxon_name>
    <taxon_name authority="Planchon" date="unknown" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mag. Nat. Hist., ser.</publication_title>
      <place_in_publication>2, 1: 86. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anacharis;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves mostly in 3s, often recurved, linear to lanceolate, 4–15.5 × 0.9–1.7 (–2.4) mm, margins folded.</text>
      <biological_entity id="o3422" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o3423" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s0" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="length" src="d0_s0" to="15.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s0" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" is_modifier="true" name="width" src="d0_s0" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="folded" value_original="folded" />
      </biological_entity>
      <relation from="o3422" id="r467" name="in" negation="false" src="d0_s0" to="o3423" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: staminate spathes subglobose to ovoid, 2.2–4 mm;</text>
      <biological_entity id="o3424" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o3425" name="spathe" name_original="spathes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s1" to="ovoid" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>peduncles abscissing in bud;</text>
      <biological_entity id="o3426" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o3427" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character constraint="in bud" constraintid="o3428" is_modifier="false" name="condition" src="d0_s2" value="abscissing" value_original="abscissing" />
      </biological_entity>
      <biological_entity id="o3428" name="bud" name_original="bud" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>pistillate spathes linear, 8.5–15 mm.</text>
      <biological_entity id="o3429" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o3430" name="spathe" name_original="spathes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers unisexual;</text>
      <biological_entity id="o3431" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminate flowers: stamens 9, pedicels briefly remaining attached following anthesis;</text>
      <biological_entity id="o3432" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3433" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="9" value_original="9" />
      </biological_entity>
      <biological_entity id="o3434" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s5" value="attached" value_original="attached" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="anthesis" value_original="anthesis" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner 3 filaments connate proximally, forming column;</text>
      <biological_entity id="o3435" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position" src="d0_s6" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o3436" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o3437" name="column" name_original="column" src="d0_s6" type="structure" />
      <relation from="o3436" id="r468" name="forming" negation="false" src="d0_s6" to="o3437" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 1–1.4 mm;</text>
      <biological_entity id="o3438" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3439" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pollen in tetrads;</text>
      <biological_entity id="o3440" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3441" name="pollen" name_original="pollen" src="d0_s8" type="structure" />
      <biological_entity id="o3442" name="tetrad" name_original="tetrads" src="d0_s8" type="structure" />
      <relation from="o3441" id="r469" name="in" negation="false" src="d0_s8" to="o3442" />
    </statement>
    <statement id="d0_s9">
      <text>pistillate flowers: styles mostly 1.2–2 mm.</text>
      <biological_entity id="o3443" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3444" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds fusiform, 4–4.6 mm, base with long hairs.</text>
      <biological_entity id="o3445" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="4.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3447" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
      </biological_entity>
      <relation from="o3446" id="r470" name="with" negation="false" src="d0_s10" to="o3447" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 48 (Britain).</text>
      <biological_entity id="o3446" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o3448" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waters, mostly calcareous, of lakes and rivers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waters" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rivers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Ark., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mont., Mo., Nebr., Nev., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tenn., Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>I know of no instance in North America where Elodea nuttalli or E. canadensis is weedy. Both are weedy in Europe, and E. canadensis is weedy also in Australia and New Zealand.</discussion>
  
</bio:treatment>