<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="Hagström" date="1916" rank="species">subsibiricus</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Svenska Vetenskapsakademiens Handlingar</publication_title>
      <place_in_publication>n.s. 44:555(5):84. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species subsibiricus</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000310</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potamogeton</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">porsildiorum</taxon_name>
    <taxon_hierarchy>genus Potamogeton;species porsildiorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizome absent.</text>
      <biological_entity id="o7307" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems compressed-filiform, without spots, to 50 cm;</text>
      <biological_entity constraint="cauline" id="o7308" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed-filiform" value_original="compressed-filiform" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7309" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o7308" id="r968" name="without" negation="false" src="d0_s1" to="o7309" />
    </statement>
    <statement id="d0_s2">
      <text>glands white, 0.3–0.5 mm diam.</text>
      <biological_entity id="o7310" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Turions lateral, common, 3.5–9.5 cm × 2–5 mm, soft;</text>
      <biological_entity id="o7311" name="turion" name_original="turions" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="common" value_original="common" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s3" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves 2-ranked;</text>
      <biological_entity id="o7312" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>outer leaves 3–4 per side, base not corrugate;</text>
      <biological_entity constraint="outer" id="o7313" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o7314" from="3" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o7314" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o7315" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner leaves undifferentiated.</text>
      <biological_entity constraint="inner" id="o7316" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves submersed, spirally arranged, sessile, flaccid;</text>
      <biological_entity id="o7317" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="location" src="d0_s7" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="texture" src="d0_s7" value="flaccid" value_original="flaccid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules deliquescent, inconspicuous, convolute, free from blade, pale-brown, not ligulate, 1–2 cm, not fibrous, not shredding, apex obtuse;</text>
      <biological_entity id="o7318" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_texture" src="d0_s8" value="deliquescent" value_original="deliquescent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="convolute" value_original="convolute" />
        <character constraint="from blade" constraintid="o7319" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o7319" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o7320" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade dark green, linear, not arcuate, 3.5–9.5 cm × 1.5–2 mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, rounded or nearly acute to mucronate, lacunae in 1–2 rows each side of midvein;</text>
      <biological_entity id="o7321" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s9" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7322" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s9" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7323" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o7324" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o7325" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="hoodlike" value_original="hoodlike" />
        <character char_type="range_value" from="nearly acute" name="shape" src="d0_s9" to="mucronate" />
      </biological_entity>
      <biological_entity id="o7326" name="lacuna" name_original="lacunae" src="d0_s9" type="structure" />
      <biological_entity id="o7327" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o7328" name="side" name_original="side" src="d0_s9" type="structure" />
      <biological_entity id="o7329" name="midvein" name_original="midvein" src="d0_s9" type="structure" />
      <relation from="o7322" id="r969" name="without" negation="false" src="d0_s9" to="o7323" />
      <relation from="o7326" id="r970" name="in" negation="false" src="d0_s9" to="o7327" />
      <relation from="o7328" id="r971" name="part_of" negation="false" src="d0_s9" to="o7329" />
    </statement>
    <statement id="d0_s10">
      <text>veins 9–17.</text>
      <biological_entity id="o7330" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s10" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o7331" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s11" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peduncles not dimorphic, axillary, erect, cylindric, 1.7–3.5 cm;</text>
      <biological_entity id="o7332" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s12" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s12" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>spikes not dimorphic, cylindric, 10–30 mm.</text>
      <biological_entity id="o7333" name="spike" name_original="spikes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits sessile, reddish-brown, oblong-obovoid, compressed, abaxially ridged, not laterally keeled, 3–4 × 1.5–2 mm;</text>
      <biological_entity id="o7334" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-obovoid" value_original="oblong-obovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s14" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="not laterally" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak nearly erect, 0.3–0.5 mm;</text>
      <biological_entity id="o7335" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o7336" name="side" name_original="sides" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o7337" name="tubercle" name_original="tubercles" src="d0_s16" type="structure" />
      <relation from="o7336" id="r972" name="without" negation="false" src="d0_s16" to="o7337" />
    </statement>
    <statement id="d0_s17">
      <text>embryo with 1 full coil.</text>
      <biological_entity id="o7338" name="embryo" name_original="embryo" src="d0_s17" type="structure" />
      <biological_entity id="o7339" name="coil" name_original="coil" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="full" value_original="full" />
      </biological_entity>
      <relation from="o7338" id="r973" name="with" negation="false" src="d0_s17" to="o7339" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting early summer–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow water of ponds and lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow water" constraint="of ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–915 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="915" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.W.T., Ont., Nunavut, Que., Yukon; Alaska; Siberia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Siberia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12</number>
  <other_name type="common_name">Yenissei River pondweed</other_name>
  <other_name type="common_name">potamot de l’Ilenissei</other_name>
  
</bio:treatment>