<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>C. Barre Hellquist,Robert R. Haynes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Agardh" date="unknown" rank="family">Aponogetonaceae</taxon_name>
    <taxon_hierarchy>family Aponogetonaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10054</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, caulescent;</text>
      <biological_entity id="o8307" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>turions absent.</text>
      <biological_entity id="o8308" name="turion" name_original="turions" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, floating [submersed], petiolate [sessile];</text>
      <biological_entity id="o8309" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath not persisting longer than blade, not leaving circular scar when shed, not ligulate, not auriculate;</text>
      <biological_entity id="o8310" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persisting" value_original="persisting" />
        <character constraint="than blade" constraintid="o8311" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8311" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o8312" name="scar" name_original="scar" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s3" value="circular" value_original="circular" />
        <character is_modifier="false" modifier="not; when shed; not" name="architecture" src="d0_s3" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <relation from="o8310" id="r1101" name="leaving" negation="true" src="d0_s3" to="o8312" />
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to narrowly lanceolate [linear];</text>
      <biological_entity id="o8313" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>intravaginal squamules (i.e., minute appressed, planate trichomes attached at basal edge) scales, more than 2.</text>
      <biological_entity constraint="squamules" id="o8314" name="scale" name_original="scales" src="d0_s5" type="structure" constraint_original="intravaginal squamules">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, spikes, subtended by spathe, pedunculate;</text>
      <biological_entity id="o8315" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o8316" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o8317" name="spathe" name_original="spathe" src="d0_s6" type="structure" />
      <relation from="o8316" id="r1102" name="subtended by" negation="false" src="d0_s6" to="o8317" />
    </statement>
    <statement id="d0_s7">
      <text>peduncle following fertilization not elongating, not spiraling.</text>
      <biological_entity id="o8318" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="length" src="d0_s7" value="elongating" value_original="elongating" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="spiraling" value_original="spiraling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual [unisexual];</text>
    </statement>
    <statement id="d0_s9">
      <text>subtending bracts absent;</text>
      <biological_entity id="o8319" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8320" name="bract" name_original="bracts" src="d0_s9" type="structure" />
      <relation from="o8319" id="r1103" name="subtending" negation="false" src="d0_s9" to="o8320" />
    </statement>
    <statement id="d0_s10">
      <text>perianth present [absent];</text>
      <biological_entity id="o8321" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals 1 [–6];</text>
      <biological_entity id="o8322" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6–18 [–50] in 2–3 [–4] series, not epitepalous;</text>
      <biological_entity id="o8323" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="50" />
        <character char_type="range_value" constraint="in series" constraintid="o8324" from="6" name="quantity" src="d0_s12" to="18" />
        <character is_modifier="false" modifier="not" name="position" notes="" src="d0_s12" value="epitepalous" value_original="epitepalous" />
      </biological_entity>
      <biological_entity id="o8324" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers distinct, dehiscing longitudinally;</text>
      <biological_entity id="o8325" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s13" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen ellipsoid;</text>
      <biological_entity id="o8326" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 2–6 [–9], distinct, not stipitate;</text>
      <biological_entity id="o8327" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules basal-marginal, anatropous.</text>
      <biological_entity id="o8328" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="basal-marginal" value_original="basal-marginal" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="anatropous" value_original="anatropous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits follicles.</text>
      <biological_entity constraint="fruits" id="o8329" name="follicle" name_original="follicles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds 4;</text>
      <biological_entity id="o8330" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>embryo straight.</text>
      <biological_entity id="o8331" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, s Africa, and tropical regions of Eastern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="and tropical regions of Eastern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>192</number>
  <other_name type="common_name">Aponogeton or Cape-pondweed Family</other_name>
  <discussion>Genera 1, species 5247 (1 genus, 1 species in the flora).</discussion>
  <references>
    <reference>Bruggen, H. W. E. van. 1973. Revision of the genus Aponogeton (Aponogetonaceae): VI. The species of Africa. Bull. Jard. Bot. Natl. Belg. 43: 1–2, 193–233.</reference>
    <reference>Bruggen, H. W. E. van. 1985. Monograph of the Genus Aponogeton (Aponogetonaceae). Stuttgart.</reference>
  </references>
  
</bio:treatment>