<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="A. Bennett" date="1902" rank="species">strictifolius</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>40: 148. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species strictifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potamogeton</taxon_name>
    <taxon_name authority="A. Bennett" date="unknown" rank="species">strictifolius</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">rutiloides</taxon_name>
    <taxon_hierarchy>genus Potamogeton;species strictifolius;variety rutiloides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes absent.</text>
      <biological_entity id="o8049" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems terete, without spots, 27–95 cm;</text>
      <biological_entity constraint="cauline" id="o8050" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character char_type="range_value" from="27" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="95" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8051" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o8050" id="r1063" name="without" negation="false" src="d0_s1" to="o8051" />
    </statement>
    <statement id="d0_s2">
      <text>glands white, green, greenish brown, or gold, to 0.3 mm diam.</text>
      <biological_entity id="o8052" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gold" value_original="gold" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gold" value_original="gold" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Turions terminal or lateral, common, 2.5–4.8 cm × 0.8–2.2 mm, soft;</text>
      <biological_entity id="o8053" name="turion" name_original="turions" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="common" value_original="common" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves 2-ranked, flattened with outer and inner leaves in same plane;</text>
      <biological_entity id="o8054" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="2-ranked" value_original="2-ranked" />
        <character constraint="with outer inner leaves" constraintid="o8055" is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="outer and inner" id="o8055" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>outer leaves 3–4 per side, base not corrugate, or rarely corrugate, apex acute;</text>
      <biological_entity constraint="outer" id="o8056" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o8057" from="3" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o8057" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o8058" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
      </biological_entity>
      <biological_entity id="o8059" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner leaves undifferentiated.</text>
      <biological_entity constraint="inner" id="o8060" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves submersed, spirally arranged, rigid, sessile;</text>
      <biological_entity id="o8061" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="location" src="d0_s7" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules disintegrating, inconspicuous, connate, free from blade, white, not ligulate, 0.6–1.6 cm, fibrous, shredding at tip, apex obtuse;</text>
      <biological_entity id="o8062" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s8" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character constraint="from blade" constraintid="o8063" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s8" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o8063" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o8064" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o8065" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o8062" id="r1064" name="shredding at" negation="false" src="d0_s8" to="o8064" />
    </statement>
    <statement id="d0_s9">
      <text>blade green to olive-green, linear, not arcuate, 1.2–6.3 cm × 0.6–2 mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, acute to nearly bristle-tipped, rarely obtuse to apiculate, lacunae absent;</text>
      <biological_entity id="o8066" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s9" to="olive-green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s9" to="6.3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8067" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s9" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8068" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o8069" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o8070" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="hoodlike" value_original="hoodlike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s9" value="bristle-tipped" value_original="bristle-tipped" />
        <character char_type="range_value" from="rarely obtuse" name="shape" src="d0_s9" to="apiculate" />
      </biological_entity>
      <biological_entity id="o8071" name="lacuna" name_original="lacunae" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o8067" id="r1065" name="without" negation="false" src="d0_s9" to="o8068" />
    </statement>
    <statement id="d0_s10">
      <text>veins 3–5 (–7).</text>
      <biological_entity id="o8072" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o8073" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s11" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peduncles not dimorphic, terminal, erect, rarely recurved, cylindric, rarely slightly clavate, 1–4.5 cm;</text>
      <biological_entity id="o8074" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>spike not dimorphic, cylindric, 0.6–1.3 cm.</text>
      <biological_entity id="o8075" name="spike" name_original="spike" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s13" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits sessile, green-brown, ovoid, turgid, not abaxially or laterally keeled, 1.9–2.1 × 1.3–1.8 mm;</text>
      <biological_entity id="o8076" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="green-brown" value_original="green-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="turgid" value_original="turgid" />
        <character is_modifier="false" modifier="not abaxially; abaxially; laterally" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s14" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak erect, 0.5–0.8 mm;</text>
      <biological_entity id="o8077" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o8078" name="side" name_original="sides" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o8079" name="tubercle" name_original="tubercles" src="d0_s16" type="structure" />
      <relation from="o8078" id="r1066" name="without" negation="false" src="d0_s16" to="o8079" />
    </statement>
    <statement id="d0_s17">
      <text>embryo with 1 full spiral.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 26.</text>
      <biological_entity id="o8080" name="embryo" name_original="embryo" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o8081" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline waters of lakes and slow-moving streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline waters" constraint="of lakes and slow-moving streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="slow-moving streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.B., Ont., Que., Sask., Yukon; Conn., Ill., Ind., Maine, Mass., Mich., Minn., Nebr., N.Y., N.Dak., Ohio, Pa., S.Dak., Utah, Va., Vt., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Potamot à feuilles raides</other_name>
  <discussion>Potamogeton strictifolius is a relatively uncommon species found in alkaline waters. Fairly rigid leaves of the species make floating onto paper unnecessary in the collecting process. The leaves have a tendency to become revolute during the growing season. The species superficially resembles several other species of linear-leaved pondweeds. Consequently, many specimens of this species have been misidentified as other species and vice versa. Thus, literature records are often suspect.</discussion>
  <discussion>One hybrid, Potamogeton strictifolius × P. zosteriformis (= P. × haynesii Hellquist &amp; G. E. Crow), has been described.</discussion>
  
</bio:treatment>