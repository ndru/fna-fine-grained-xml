<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Ertter" date="1986" rank="species">tiehmii</taxon_name>
    <place_of_publication>
      <publication_title>Memoirs of the New York Botanical Garden</publication_title>
      <place_in_publication>39: 60, figs. 13f–g, 14. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species tiehmii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000184</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose, 0.1–0.6 dm.</text>
      <biological_entity id="o7577" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="0.6" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 150, 0.1–0.2 mm diam.</text>
      <biological_entity id="o7578" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="150" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s1" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves to 2.5 cm × 0.1–0.3 mm.</text>
      <biological_entity id="o7579" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="length" src="d0_s2" unit="cm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences headlike clusters, each with 1–4 (–7) flowers;</text>
      <biological_entity id="o7580" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="headlike" value_original="headlike" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o7581" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="7" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <relation from="o7580" id="r1014" name="with" negation="false" src="d0_s3" to="o7581" />
    </statement>
    <statement id="d0_s4">
      <text>bracts subtending inflorescence 2–4 (–8), ovate, inconspicuous, 0.6–1.5 mm, membranous, apex acute.</text>
      <biological_entity id="o7582" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7583" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <biological_entity id="o7584" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o7582" id="r1015" name="subtending" negation="false" src="d0_s4" to="o7583" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 3-merous;: tepals 4 (–6), green or darker, acute to acuminate, 1.9–2.9 × 0.4–0.6 mm, nearly equal;</text>
      <biological_entity id="o7585" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o7586" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character name="quantity" src="d0_s5" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker" value_original="darker" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s5" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 2 (–3), filaments 0.5–0.8 mm, anthers 0.3–0.4 mm;</text>
      <biological_entity id="o7587" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o7588" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7589" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7590" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 0.2–0.3 mm, stigma 0.2–0.7 mm.</text>
      <biological_entity id="o7591" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o7592" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7593" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules pink-tinged, 2-locular or 3-locular, ellipsoid to narrowly oblate, 1.9–2.9 × 1.1–1.5 mm, nearly equal or slightly longer than tepals.</text>
      <biological_entity id="o7594" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink-tinged" value_original="pink-tinged" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="3-locular" value_original="3-locular" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s8" to="narrowly oblate" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s8" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character constraint="than tepals" constraintid="o7595" is_modifier="false" name="length_or_size" src="d0_s8" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o7595" name="tepal" name_original="tepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds oblate to ovoid, 0.35–0.55 mm. n = 17.</text>
      <biological_entity id="o7596" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s9" to="ovoid" />
        <character char_type="range_value" from="0.35" from_unit="mm" name="some_measurement" src="d0_s9" to="0.55" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="n" id="o7597" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid spring" />
        <character name="fruiting time" char_type="range_value" to="early fall" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bare, moist granitic sand along streams, seepage areas around outcrops, and depressions in meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist granitic sand" modifier="bare" constraint="along streams , seepage areas around outcrops , and depressions in meadows" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="seepage areas" constraint="around outcrops" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="depressions" constraint="in meadows" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Wash.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44</number>
  <other_name type="common_name">Tiehm's dwarf rush</other_name>
  
</bio:treatment>