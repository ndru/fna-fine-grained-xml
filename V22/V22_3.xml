<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Luzula</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="species">spicata</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck and A. P. de Candolle,Fl. France, ed. 3</publication_title>
      <place_in_publication>1: 161. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus luzula;species spicata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000242</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">spicatus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 330. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species spicatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms densely cespitose, reddish, 3–33 cm, base thick, extending 1–8 cm into soil.</text>
      <biological_entity id="o863" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="33" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o864" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: sheath throats densely hairy;</text>
      <biological_entity id="o865" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="sheath" id="o866" name="throat" name_original="throats" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal leaves erect, channeled, linear, 2–15 cm × 1–4 mm, apex not callous;</text>
      <biological_entity id="o867" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o868" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="channeled" value_original="channeled" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o869" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="callous" value_original="callous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves 2–3.</text>
      <biological_entity id="o870" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o871" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences panicles of dense, nodding, spikelike clusters (each 1–25 mm), often interrupted by 10–70 mm;</text>
      <biological_entity constraint="inflorescences" id="o872" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal inflorescence bract conspicuous, generally exceeding inflorescence;</text>
      <biological_entity constraint="inflorescence" id="o873" name="bract" name_original="bract" src="d0_s5" type="structure" constraint_original="proximal inflorescence">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o874" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure" />
      <relation from="o873" id="r122" modifier="generally" name="exceeding" negation="false" src="d0_s5" to="o874" />
    </statement>
    <statement id="d0_s6">
      <text>bracts clear;</text>
      <biological_entity id="o875" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="clear" value_original="clear" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles clear, margins ciliate, apex narrow, extended.</text>
      <biological_entity id="o876" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity id="o877" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o878" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="size" src="d0_s7" value="extended" value_original="extended" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals brown with clear margins or very pale throughout (outer whorl bristle-pointed), 2–2.5 mm;</text>
      <biological_entity id="o879" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o880" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character constraint="with margins" constraintid="o881" is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="very; throughout" name="coloration" src="d0_s8" value="pale" value_original="pale" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o881" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="clear" value_original="clear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer whorl longer than inner whorl;</text>
      <biological_entity id="o882" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o883" name="whorl" name_original="whorl" src="d0_s9" type="structure">
        <character constraint="than inner whorl" constraintid="o884" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="inner" id="o884" name="whorl" name_original="whorl" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers ± equaling filaments.</text>
      <biological_entity id="o885" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o886" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o887" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules pale to dark-brown or blackish, round (apex ± acute), generally shorter than tepals, apex ± acute.</text>
      <biological_entity id="o888" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s11" to="dark-brown or blackish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character constraint="than tepals" constraintid="o889" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="generally shorter" value_original="generally shorter" />
      </biological_entity>
      <biological_entity id="o889" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
      <biological_entity id="o890" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown, cylindric-ovoid, body 1–1.2 mm;</text>
      <biological_entity id="o891" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric-ovoid" value_original="cylindric-ovoid" />
      </biological_entity>
      <biological_entity id="o892" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>caruncle 0.2 mm. 2n = 24.</text>
      <biological_entity id="o893" name="caruncle" name_original="caruncle" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o894" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine slopes and heaths, dry or damp situations among grasses, herbs, or lichens, and in subalpine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="alpine" />
        <character name="habitat" value="heaths" />
        <character name="habitat" value="dry" constraint="among grasses , herbs , or lichens" />
        <character name="habitat" value="damp situations" constraint="among grasses , herbs , or lichens" />
        <character name="habitat" value="grasses" />
        <character name="habitat" value="herbs" />
        <character name="habitat" value="lichens" />
        <character name="habitat" value="subalpine forests" modifier="and in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Circumpolar; Greenland; St. Pierre and Miquelon; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska, Calif., Colo., Idaho, Maine, Mont., Nev., N.H., N.Mex., N.Y., Oreg., Utah, Vt., Wash., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Circumpolar" establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Spiked wood rush</other_name>
  <discussion>The culms of Luzula spicata are thick and reddish with bases extending 1–8 cm into the soil; sheath throats are densely hairy; basal leaves are erect, linear, and channeled; inflorescence bracts are conspicuous and often exceed glomerules; and bracteoles have narrow and extended apices.</discussion>
  
</bio:treatment>