<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="E. S. Anderson &amp; Woodson" date="1935" rank="species">ozarkana</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Arnold Arbor.</publication_title>
      <place_in_publication>9: 56, plate 12, map 3. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species ozarkana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000426</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o9385" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o9386" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9386" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems not flexuous, 10–50 cm;</text>
      <biological_entity id="o9387" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous to pilose.</text>
      <biological_entity id="o9388" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves spirally arranged, sessile;</text>
      <biological_entity id="o9389" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade silvery or gray-green, lanceolate to linear-lanceolate or linear-oblong, 8–28 × 1–6 cm (distal leaf-blades wider than sheaths when sheaths opened, flattened), base rounded to cuneate, apex acuminate, ± glaucous, usually glabrous.</text>
      <biological_entity id="o9390" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear-lanceolate or linear-oblong" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="28" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9391" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
      <biological_entity id="o9392" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences all or mostly terminal;</text>
      <biological_entity id="o9393" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous.</text>
      <biological_entity id="o9394" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o9395" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s7" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 2–3.2 cm, glandular-pilosulose;</text>
      <biological_entity id="o9396" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3.2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pilosulose" value_original="glandular-pilosulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 6–12 mm, sparsely to densely glandular-pilosulose;</text>
      <biological_entity id="o9397" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s9" value="glandular-pilosulose" value_original="glandular-pilosulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, white or pale-pink to pale lavender, broadly ovate, not clawed, 1.2–1.6 cm;</text>
      <biological_entity id="o9398" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s10" to="pale lavender" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s10" to="1.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens free.</text>
      <biological_entity id="o9399" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–8 mm.</text>
      <biological_entity id="o9400" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 3–4 mm. 2n = 12, 24.</text>
      <biological_entity id="o9401" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9402" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="12" value_original="12" />
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods, mainly on rocky slopes and along cliffs, occasionally in bottomlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="bottomlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Mo., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>Tradescantia ozarkana is endemic to the Ozarks.</discussion>
  
</bio:treatment>