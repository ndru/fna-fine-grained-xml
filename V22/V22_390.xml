<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">difformis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.</publication_title>
      <place_in_publication>500. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species difformis</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000462</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, (10–) 15–70 (–90) cm, base not bulbous.</text>
      <biological_entity id="o3710" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3711" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems compact.</text>
      <biological_entity id="o3712" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in narrow to broad fans, 5–50 cm;</text>
      <biological_entity id="o3713" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3714" name="fan" name_original="fans" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrow" is_modifier="true" name="width" src="d0_s2" to="broad" />
      </biological_entity>
      <relation from="o3713" id="r498" name="in" negation="false" src="d0_s2" to="o3714" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths soft, base reddish;</text>
      <biological_entity id="o3715" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o3716" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade deep green, linear to linear—sword-shaped, flat, not twisted, under 7 mm wide, papillate or smooth, margins smooth or papillate, somewhat scabrous.</text>
      <biological_entity id="o3717" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-sword-shaped" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character constraint="under 7 mm" is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3718" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths exceeded by leaves;</text>
      <biological_entity id="o3719" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o3720" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o3721" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o3720" id="r499" name="exceeded by" negation="false" src="d0_s5" to="o3721" />
    </statement>
    <statement id="d0_s6">
      <text>scapes linear to filiform, terete proximally, terete or ancipital-2-edged–winged distally, 0.5–3 mm wide, variably ribbed, distally with 3 or more somewhat scabrous ribs, ribs papillate to scabrous;</text>
      <biological_entity id="o3722" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o3723" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="filiform" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="ancipital-2-edged-winged" value_original="ancipital-2-edged-winged" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="variably" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o3724" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3725" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character char_type="range_value" from="papillate" name="relief" src="d0_s6" to="scabrous" />
      </biological_entity>
      <relation from="o3723" id="r500" modifier="distally" name="with" negation="false" src="d0_s6" to="o3724" />
    </statement>
    <statement id="d0_s7">
      <text>spikes prevalently ovoid, 5–20 mm, apex usually acute;</text>
      <biological_entity id="o3726" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o3727" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="prevalently" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3728" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts (4–) 5–7 (–9) mm, margins entire, apex rounded.</text>
      <biological_entity id="o3729" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o3730" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3731" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3732" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included, slightly curved, 5–7 mm, keel brown, scarious, lacerate;</text>
      <biological_entity id="o3733" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o3734" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3735" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding in morning, blade obtriangular, 4 mm;</text>
      <biological_entity id="o3736" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3737" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in morning" constraintid="o3738" is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <biological_entity id="o3738" name="morning" name_original="morning" src="d0_s10" type="structure" />
      <biological_entity id="o3739" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtriangular" value_original="obtriangular" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o3740" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3741" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds translucent to mealy, ovoid to ellipsoid, 0.5 mm, finely lined longitudinally with small papillae.</text>
      <biological_entity id="o3742" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="mealy" value_original="mealy" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="ellipsoid" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
        <character constraint="with papillae" constraintid="o3743" is_modifier="false" modifier="finely" name="architecture" src="d0_s12" value="lined" value_original="lined" />
      </biological_entity>
      <biological_entity id="o3743" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S., Ont.; Ala., Ark., Conn., D.C., Del., Fla., Ga., Ind., Ky., La., Maine, Mass., Md., Mich., Miss., N.C., N.H., N.J., N.Y., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., Vt., Wis.; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves and scapes (except for edges and ribs) smooth; scapes somewhat to much widened distally; 2 ribs comparably wider, making wings, smooth or papillate; seeds translucent</description>
      <determination>17a Xyris difformis var. difformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves and scapes variously papillate or minutely scabrous; scapes not much widened distally; ribs all equally prominent, somewhat scabrous; seeds translucent or mealy.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Seeds translucent; leaves spreading-ascending in broad fans, rarely as long as 10 cm, linear–sword-shaped; spikes rarely longer than 5 mm.</description>
      <determination>17b Xyris difformis var. curtissii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Seeds mealy; leaves ascending in broad to narrow fans, rarely shorter than (10–)15 cm, linear; spikes 6–10(–15) mm</description>
      <determination>17c Xyris difformis var. floridana</determination>
    </key_statement>
  </key>
</bio:treatment>