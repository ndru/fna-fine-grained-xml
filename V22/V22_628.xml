<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">alismataceae</taxon_name>
    <taxon_name authority="Richard &amp; Engelmann ex A. Gray" date="1848" rank="genus">Echinodorus</taxon_name>
    <place_of_publication>
      <publication_title>Manual</publication_title>
      <place_in_publication>460. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family alismataceae;genus Echinodorus</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek echius, rough husk, and doros, leathern bottle, alluding to ovaries, which in some species are armed with persistent styles, forming prickly head of fruit</other_info_on_name>
    <other_info_on_name type="fna_id">111229</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Engelmann ex Hooker f.) J. G. Smith" date="unknown" rank="genus">Helianthium</taxon_name>
    <taxon_hierarchy>genus Helianthium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial, emersed, floating-leaved, or rarely submersed, glabrous to stellate-pubescent;</text>
      <biological_entity id="o12429" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="location" src="d0_s0" value="emersed" value_original="emersed" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="floating-leaved" value_original="floating-leaved" />
        <character is_modifier="false" modifier="rarely" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s0" to="stellate-pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes present or absent;</text>
      <biological_entity id="o12430" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stolons absent;</text>
      <biological_entity id="o12431" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corms absent;</text>
      <biological_entity id="o12432" name="corm" name_original="corms" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubers absent.</text>
      <biological_entity id="o12433" name="tuber" name_original="tubers" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Roots not septate.</text>
      <biological_entity id="o12434" name="root" name_original="roots" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves sessile or petiolate;</text>
      <biological_entity id="o12435" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petioles triangular, rarely terete;</text>
      <biological_entity id="o12436" name="petiole" name_original="petioles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade with translucent markings as dots or lines present or absent, linear to lanceolate to ovate, base attenuate to cordate, margins entire or undulating, apex obtuse to acute.</text>
      <biological_entity id="o12437" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o12438" name="marking" name_original="markings" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o12439" name="line" name_original="lines" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_relief" src="d0_s8" value="dots" value_original="dots" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12440" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s8" to="cordate" />
      </biological_entity>
      <biological_entity id="o12441" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12442" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <relation from="o12437" id="r1659" name="with" negation="false" src="d0_s8" to="o12438" />
      <relation from="o12438" id="r1660" name="as" negation="false" src="d0_s8" to="o12439" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences racemes or panicles, rarely umbels, of 1–18 whorls, erect or decumbent, emersed;</text>
      <biological_entity id="o12443" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="location" src="d0_s9" value="emersed" value_original="emersed" />
      </biological_entity>
      <biological_entity id="o12444" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="location" src="d0_s9" value="emersed" value_original="emersed" />
      </biological_entity>
      <biological_entity id="o12445" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="location" src="d0_s9" value="emersed" value_original="emersed" />
      </biological_entity>
      <biological_entity id="o12446" name="umbel" name_original="umbels" src="d0_s9" type="structure" />
      <biological_entity id="o12447" name="whorl" name_original="whorls" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="18" />
      </biological_entity>
      <relation from="o12443" id="r1661" name="consist_of" negation="false" src="d0_s9" to="o12447" />
      <relation from="o12444" id="r1662" name="consist_of" negation="false" src="d0_s9" to="o12447" />
      <relation from="o12445" id="r1663" name="consist_of" negation="false" src="d0_s9" to="o12447" />
    </statement>
    <statement id="d0_s10">
      <text>bracts coarse, apex obtuse to acute, surfaces smooth or papillose along veins, apex obtuse to acute.</text>
      <biological_entity id="o12448" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o12449" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
      <biological_entity id="o12450" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character constraint="along veins" constraintid="o12451" is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o12451" name="vein" name_original="veins" src="d0_s10" type="structure" />
      <biological_entity id="o12452" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers bisexual, subsessile to pedicellate;</text>
      <biological_entity id="o12453" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s11" to="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracts subtending pedicels, subulate to lanceolate, shorter than to longer than pedicels, apex obtuse to acute;</text>
      <biological_entity id="o12454" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s12" to="lanceolate" />
        <character constraint="shorter than to longer than pedicels" constraintid="o12456" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter than to longer than pedicels" />
      </biological_entity>
      <biological_entity id="o12455" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o12457" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12456" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12458" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute" />
      </biological_entity>
      <relation from="o12454" id="r1664" name="subtending" negation="false" src="d0_s12" to="o12455" />
    </statement>
    <statement id="d0_s13">
      <text>pedicels ascending to recurved;</text>
      <biological_entity id="o12459" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s13" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>receptacle convex;</text>
      <biological_entity id="o12460" name="receptacle" name_original="receptacle" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals recurved to spreading, herbaceous to leathery, sculpturing absent;</text>
      <biological_entity id="o12461" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character char_type="range_value" from="recurved" name="orientation" src="d0_s15" to="spreading" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s15" to="leathery" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals white, entire;</text>
      <biological_entity id="o12462" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 9–25;</text>
      <biological_entity id="o12463" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s17" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>filaments linear, glabrous;</text>
      <biological_entity id="o12464" name="filament" name_original="filaments" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pistils 15–250 or more, spirally arranged on convex receptacle, forming head, distinct;</text>
      <biological_entity id="o12465" name="pistil" name_original="pistils" src="d0_s19" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s19" to="250" />
        <character constraint="on receptacle" constraintid="o12466" is_modifier="false" modifier="spirally" name="arrangement" src="d0_s19" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o12466" name="receptacle" name_original="receptacle" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o12467" name="head" name_original="head" src="d0_s19" type="structure" />
      <relation from="o12465" id="r1665" name="forming" negation="false" src="d0_s19" to="o12467" />
    </statement>
    <statement id="d0_s20">
      <text>ovules 1;</text>
      <biological_entity id="o12468" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>style terminal or lateral.</text>
      <biological_entity id="o12469" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="position" src="d0_s21" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s21" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits plump, often longitudinally ribbed, sometimes flattened, rarely abaxially keeled, abaxial wings absent, lateral wings absent, glands often present.</text>
      <biological_entity id="o12470" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character is_modifier="false" name="size" src="d0_s22" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="often longitudinally" name="architecture_or_shape" src="d0_s22" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="rarely abaxially" name="shape" src="d0_s22" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12471" name="wing" name_original="wings" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o12472" name="wing" name_original="wings" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12473" name="gland" name_original="glands" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Western Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Western Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Species 26 (4 in the flora).</discussion>
  <references>
    <reference>Fassett, N. C. 1955. Echinodorus in the American tropics. Rhodora 57: 133–156, 174–188, 202–212.</reference>
    <reference>Haynes, R. R. and L. B. Holm-Nielsen. 1986. Notes on Echinodorus (Alismataceae). Brittonia 38: 325–332.</reference>
    <reference>Rataj, K. 1975. Revizion [sic] of the Genus Echinodorus Rich. Prague.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistils 15–20; submersed leaves mostly present, sessile; emersed leaves narrowly lanceolate to ovate; plants relatively delicate</description>
      <determination>1 Echinodorus tenellus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistils 45–250; submersed leaves mostly absent, when present, petiolate; emersed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence decumbent to arching, proliferating; sepal veins papillate</description>
      <determination>3a Echinodorus cordifolius subsp. cordifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence erect, not proliferating; sepal veins not papillate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamens 9–15; plants to 70 cm.</description>
      <determination>2 Echinodorus berteroi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamens 21; plants to 200 cm.</description>
      <determination>4 Echinodorus floridanus</determination>
    </key_statement>
  </key>
</bio:treatment>