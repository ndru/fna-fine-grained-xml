<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Palisot de Beauvois ex Desvaux" date="unknown" rank="family">eriocaulaceae</taxon_name>
    <taxon_name authority="Kunth" date="1841" rank="genus">lachnocaulon</taxon_name>
    <taxon_name authority="Ruhland in H. G. A. Engler" date="1903" rank="species">engleri</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler,Das Pflanzenreich</publication_title>
      <place_in_publication>13[IV,30]: 241. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family eriocaulaceae;genus lachnocaulon;species engleri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000201</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, short-lived perennial, solitary or cespitose, forming low domes of rosettes, 6–15 cm.</text>
      <biological_entity id="o7485" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7486" name="dome" name_original="domes" src="d0_s0" type="structure">
        <character is_modifier="true" name="position" src="d0_s0" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o7487" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <relation from="o7485" id="r998" name="forming" negation="false" src="d0_s0" to="o7486" />
      <relation from="o7485" id="r999" name="part_of" negation="false" src="d0_s0" to="o7487" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly linear-triangular, 2–4 cm, apex acute.</text>
      <biological_entity id="o7488" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s1" value="linear-triangular" value_original="linear-triangular" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7489" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: scape sheaths shorter than or as long as principal leaves;</text>
      <biological_entity id="o7490" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="scape" id="o7491" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character constraint="than or as-long-as principal leaves" constraintid="o7492" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="principal" id="o7492" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>scapes filiform, distally 0.4 mm thick, 4–5-ribbed, glabrous;</text>
      <biological_entity id="o7493" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o7494" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character modifier="distally" name="thickness" src="d0_s3" unit="mm" value="0.4" value_original="0.4" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mature heads dark-brown or reddish-brown, nearly globose, usually short cylindric by seeding time, 3–4 mm;</text>
      <biological_entity id="o7495" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o7496" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character constraint="by time" constraintid="o7497" is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7497" name="time" name_original="time" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>receptacle densely pilose;</text>
      <biological_entity id="o7498" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o7499" name="receptacle" name_original="receptacle" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucral-bracts soon reflexed, brown, mostly ovate, 1 (–1.5) mm, apex broadly acute to obtuse, surfaces glabrous or apex sparsely pilose, hairs translucent, club-shaped;</text>
      <biological_entity id="o7500" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o7501" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="soon" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7502" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity id="o7503" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7504" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7505" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s6" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>receptacular-bracts, dark-brown, broadly spatulate or obovate-cuneate, concave, with broad, low, pale keel, 1–1.5 mm, apex short-acuminate, glabrous or abaxial surface with translucent, club-shaped hairs distally.</text>
      <biological_entity id="o7506" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate-cuneate" value_original="obovate-cuneate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7507" name="receptacular-bract" name_original="receptacular-bracts" src="d0_s7" type="structure" />
      <biological_entity id="o7508" name="keel" name_original="keel" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="true" name="position" src="d0_s7" value="low" value_original="low" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o7509" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-acuminate" value_original="short-acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7510" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o7511" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s7" value="translucent" value_original="translucent" />
        <character is_modifier="true" name="shape" src="d0_s7" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o7506" id="r1000" name="with" negation="false" src="d0_s7" to="o7508" />
      <relation from="o7510" id="r1001" name="with" negation="false" src="d0_s7" to="o7511" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3, dark-brown, narrowly spatulate to oblanceolate-cuneate, concave, 1 mm, glabrous or abaxially with scattered translucent, club-shaped hairs distally;</text>
      <biological_entity id="o7512" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7513" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="narrowly spatulate" name="shape" src="d0_s8" to="oblanceolate-cuneate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="concave" value_original="concave" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s8" value="abaxially" value_original="abaxially" />
      </biological_entity>
      <biological_entity id="o7514" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
        <character is_modifier="true" name="shape" src="d0_s8" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o7513" id="r1002" name="with" negation="false" src="d0_s8" to="o7514" />
    </statement>
    <statement id="d0_s9">
      <text>androphore pale, narrowly obconic, as long as sepals, glabrous, rarely apex with few club-shaped hairs;</text>
      <biological_entity id="o7515" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7516" name="androphore" name_original="androphore" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7517" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o7518" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o7519" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="true" name="shape" src="d0_s9" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o7516" id="r1003" name="as long as" negation="false" src="d0_s9" to="o7517" />
      <relation from="o7518" id="r1004" name="with" negation="false" src="d0_s9" to="o7519" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2–3, appendages (2–) 3.</text>
      <biological_entity id="o7520" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7521" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o7522" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s10" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 3, dark-brown, spatulate-oblong, concave, 1 mm, abaxially sparsely pubescent distally, hairs translucent;</text>
      <biological_entity id="o7523" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7524" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate-oblong" value_original="spatulate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="abaxially sparsely; distally" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7525" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>gynoecium 3-carpellate;</text>
      <biological_entity id="o7526" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7527" name="gynoecium" name_original="gynoecium" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles: apex 3-cleft, appendages 3.</text>
      <biological_entity id="o7528" name="style" name_original="styles" src="d0_s13" type="structure" />
      <biological_entity id="o7529" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="3-cleft" value_original="3-cleft" />
      </biological_entity>
      <biological_entity id="o7530" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds rich translucent brown, broadly ovoid to ellipsoid, 0.4–0.5 mm, longitudinal ribs conspicuous and pale, transverse striae fine forming linear cancellae.</text>
      <biological_entity id="o7531" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="rich" value_original="rich" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="translucent brown" value_original="translucent brown" />
        <character char_type="range_value" from="broadly ovoid" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7532" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o7533" name="stria" name_original="striae" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="width" src="d0_s14" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o7534" name="cancella" name_original="cancellae" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o7533" id="r1005" name="forming" negation="false" src="d0_s14" to="o7534" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy-peaty lake and pond shores, drawdowns, ditchbanks, low sandy clearings in savanna and flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sandy-peaty lake" />
        <character name="habitat" value="pond shores" modifier="and" />
        <character name="habitat" value="drawdowns" />
        <character name="habitat" value="ditchbanks" />
        <character name="habitat" value="low sandy clearings" constraint="in savanna and flatwoods" />
        <character name="habitat" value="savanna" />
        <character name="habitat" value="flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>