<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">Xyris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 42. 1753; Gen. Pl. ed. 5; 25, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus Xyris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek xyron, razor, in reference to a plant with two-edged leaves</other_info_on_name>
    <other_info_on_name type="fna_id">135197</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Jupica</taxon_name>
    <taxon_hierarchy>genus Jupica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Adanson" date="unknown" rank="genus">Kotsjiletti</taxon_name>
    <taxon_hierarchy>genus Kotsjiletti;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Ramotha</taxon_name>
    <taxon_hierarchy>genus Ramotha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Steudel" date="unknown" rank="genus">Schizmaxon</taxon_name>
    <taxon_hierarchy>genus Schizmaxon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Adanson" date="unknown" rank="genus">Xuris</taxon_name>
    <taxon_hierarchy>genus Xuris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Thouars" date="unknown" rank="genus">Xyroides</taxon_name>
    <taxon_hierarchy>genus Xyroides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, occasionally annual, rosulate.</text>
      <biological_entity id="o7893" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="rosulate" value_original="rosulate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple, erect, sometimes caudiciform, short to branching, elongate.</text>
      <biological_entity id="o7894" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="caudiciform" value_original="caudiciform" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, 2-ranked, equitant;</text>
      <biological_entity id="o7895" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="equitant" value_original="equitant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade mostly linear to filiform, flattened to nearly terete, margins smooth to variously papillate or scabrous.</text>
      <biological_entity id="o7896" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s3" to="filiform flattened" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s3" to="filiform flattened" />
      </biological_entity>
      <biological_entity id="o7897" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s3" to="variously papillate or scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: scapes variously elongate;</text>
      <biological_entity id="o7898" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o7899" name="scape" name_original="scapes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spike bracts imbricate, the proximal sterile, decussate, distal ones fertile, in spiral [decussate], bearing medially, usually subapically, a distinct and mostly differently textured and colored dorsal area or this indistinct or absent.</text>
      <biological_entity id="o7900" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="spike" id="o7901" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="decussate" value_original="decussate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7902" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="in spiral; in spiral; in spiral; in spiral" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o7903" name="area" name_original="area" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="medially; usually subapically" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="mostly differently" name="texture" src="d0_s5" value="textured" value_original="textured" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="colored" value_original="colored" />
      </biological_entity>
      <relation from="o7902" id="r1054" modifier="in spiral" name="bearing" negation="false" src="d0_s5" to="o7903" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers solitary in bract-axils;</text>
      <biological_entity id="o7904" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="in bract-axils" constraintid="o7905" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o7905" name="bract-axil" name_original="bract-axils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>sepals 3, distinct, unequal, lateral (outer) sepals distinct [connate], chaffy, boatshaped, chaffy, inner one membranous, capping corolla;</text>
      <biological_entity id="o7906" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o7907" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="chaffy" value_original="chaffy" />
        <character is_modifier="false" name="shape" src="d0_s7" value="boat-shaped" value_original="boat-shaped" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="chaffy" value_original="chaffy" />
        <character is_modifier="false" name="position" src="d0_s7" value="inner" value_original="inner" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7908" name="corolla" name_original="corolla" src="d0_s7" type="structure" />
      <relation from="o7907" id="r1055" name="capping" negation="false" src="d0_s7" to="o7908" />
    </statement>
    <statement id="d0_s8">
      <text>keel papery, or scarious, variously winged, variously margined;</text>
      <biological_entity id="o7909" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="papery" value_original="papery" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="texture" src="d0_s8" value="papery" value_original="papery" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="variously" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="variously" name="architecture" src="d0_s8" value="margined" value_original="margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 3, distinct [to connate], equal, strongly clawed;</text>
      <biological_entity id="o7910" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade ephemeral, spreading, yellow to white;</text>
      <biological_entity id="o7911" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="ephemeral" value_original="ephemeral" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes 3, distinct, strongly clawed, apically yoked, bearded with moniliform hairs (rarely reduced, beardless);</text>
      <biological_entity id="o7912" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
        <character constraint="with hairs" constraintid="o7913" is_modifier="false" modifier="apically" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o7913" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="moniliform" value_original="moniliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 3, epipetalous;</text>
      <biological_entity id="o7914" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s12" value="epipetalous" value_original="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 2–4-locular;</text>
      <biological_entity id="o7915" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="2-4-locular" value_original="2-4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovaries y thin-walled, placentation marginal to parietal [basal, free-central, axile];</text>
      <biological_entity id="o7916" name="ovary" name_original="ovaries" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles elongate, tubular, 3-branched;</text>
      <biological_entity id="o7917" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-branched" value_original="3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 3, U-shaped or funnelform.</text>
      <biological_entity id="o7918" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s16" value="u--shaped" value_original="u--shaped" />
        <character name="shape" src="d0_s16" value="funnel" value_original="funnelform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 3-valved, usually thin, loculicidal.</text>
      <biological_entity id="o7919" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" modifier="usually" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds [1–] 15–90 or more, mealy or translucent, ovoid to cylindric, 0.3–1 (–4) mm, variously lined and cross-lined;</text>
      <biological_entity id="o7920" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s18" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s18" to="90" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s18" value="mealy" value_original="mealy" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s18" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="cylindric" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s18" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="variously" name="architecture" src="d0_s18" value="lined" value_original="lined" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="cross-lined" value_original="cross-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>endosperm starchy and proteinaceous;</text>
      <biological_entity id="o7921" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="texture" src="d0_s19" value="starchy" value_original="starchy" />
        <character is_modifier="false" name="texture" src="d0_s19" value="proteinaceous" value_original="proteinaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>embryo basal-lateral, minute.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 9.</text>
      <biological_entity id="o7922" name="embryo" name_original="embryo" src="d0_s20" type="structure">
        <character is_modifier="false" name="position" src="d0_s20" value="basal-lateral" value_original="basal-lateral" />
        <character is_modifier="false" name="size" src="d0_s20" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity constraint="x" id="o7923" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, West Indies, Central America, South America, Asia, Africa, South Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="South Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Yellow-eyed-grass</other_name>
  <discussion>Most species of Xyris occur in the Guiana Highlands, Amazonia, and Brazilia in South America, with other smaller centers of endemism in Africa and Australasia.</discussion>
  <discussion>The key below is constructed to work with healthy plants that are in flower and/or fruit. Identifications based on sterile material are doubtful at best, then attempted only after long experience with such plants.</discussion>
  <discussion>In the descriptions, scapes are described as linear if, 1 cm below the spike, they are 1 mm or more wide, or as filiform if, at that level, they are less than 1 mm wide. Petal color is assumed to be yellow, although X. caroliniana and X. platylepis have white-petaled forms. Leaf widths or thicknesses are measured at widest or thickest part of blade. Spike measurements are taken from mature spikes, those having at least some ripe fruit.</discussion>
  <discussion>Species more than 250 (21 in the flora).</discussion>
  <references>
    <reference>Kral, R. 1966b. The genus Xyris (Xyridaceae) in the southeastern United States and Canada. Sida 2: 177–260.</reference>
    <reference>Kral, R. 1988. The genus Xyris (Xyridaceae) in Venezuela and contiguous northern South America. Ann. Missouri Bot. Gard. 75: 522–722.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Keel of lateral sepals usually firm, papillate, ciliate, ciliolate, or fimbriate, or in various combinations of these.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Keel of lateral sepals scarious, lacerate to (rarely) nearly entire.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants mostly under 30 cm, annual or perennial; scapes narrowly linear; scape sheaths mostly equaling or exceeding principal leaves; keels of lateral sepals papillate or ciliate; seeds 0.3–0.5 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants usually over 30 cm, perennial; scapes linear; scape sheaths exceeded by leaves; keels of lateral sepals ciliate, long-fimbriate, or lacerate; seeds 0.5 mm or longer.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Margins of fertile bracts scarious, lacerate, often squarrose with red inner band; spikes mostly as broad as long; lateral sepal keel reddish apically; plants annual.</description>
      <determination>1 Xyris brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Margins of fertile bracts entire or erose; spikes mostly longer than broad; lateral sepal keel not reddish apically, concolorous; plants annual or perennial.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Base of leaf sheaths with distinct chestnut brown patch; leaf blades olive green; plants perennial.</description>
      <determination>2 Xyris drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Base of leaf sheaths without chestnut brown patch; leaf blades strongly maroon-tinged; plants annual.</description>
      <determination>3 Xyris flabelliformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lateral sepal tips exserted even on closed spikes; lateral sepal keels fimbriate or lacero-fimbriate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lateral sepal tips included; lateral sepal keels ciliate.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lateral sepal keel apex lacero-fimbriate; fertile bracts with scarious, lacerate, often squarrose borders; seeds mostly 0.5–0.6 mm</description>
      <determination>4 Xyris elliottii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Lateral sepal keel apex distinctly long-fimbriate; fertile bracts entire to erose; seeds 0.7–1 mm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plant base deeply set; leaf base chestnut brown; spikes ellipsoid to lanceoloid or cylindric, acute; leaves not in fans, narrowly linear, twisted, fleshy; sepal fimbriae red; scape ribs smooth or somewhat scabrous.5</description>
      <determination>5 Xyris caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plant base shallowly set; leaf base dull, straw-colored, green, or pinkish; spikes ovoid to ellipsoid, blunt; leaves in fans, broadly linear, slightly twisted, flat; sepal fimbriae pale; scape ribs harsh.</description>
      <determination>6 Xyris fimbriata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves in fans; blade flat or slightly twisted.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves erect or ascending; blade twisted.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Seeds mealy; leaf base maroon; leaf blade deep green with strong maroon tints; petal blade obtriangular, 3–4 mm</description>
      <determination>7 Xyris stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Seeds translucent; leaf base tan, straw-colored, or pale pink, but not maroon; leaf blade usually pale or olive green; petal blade obovate, 10 mm.0 mm</description>
      <determination>8 Xyris ambigua</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plant base abruptly bulbous; leaf blade elongate-linear, 2–5 mm wide.</description>
      <determination>9 Xyris torta</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plant base not abruptly bulbous; leaf blade narrowly linear to filiform, to 1 mm wide</description>
      <determination>10 Xyris isoetifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf sheaths or sheath base light green, straw-colored, or dull brown.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf sheaths or sheath base with red, pink, or purple tints, or glossy brown or red-brown.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Plants annual; leaves smooth; seeds translucent; scape ribs papillate.ate</description>
      <determination>20 Xyris jupicai</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Plants perennial; leaves papillate to rugose, minutely scabrous; seeds mealy; scape ribs minutely scabrous</description>
      <determination>21 Xyris serotina</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf sheaths firm, glossy red-brown to tan.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf sheaths soft, pink, purple, or red.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Staminodes beardless; seeds fusiform to cylindric, (0.7–)0.8–1 mm; leaf blade rarely to 1 mm wide.</description>
      <determination>11 Xyris baldwiniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Staminodes bearded; seeds ellipsoid, 0.5–0.6 mm; leaf blade rarely less than 1 mm wide.</description>
      <determination>4 Xyris elliottii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Tips of lateral sepals exserted.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Tips of lateral sepals included.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Apex of Llateral sepals apex firm, red; seeds narrowly ellipsoid, 0.7–0.9 mm; leaf sheaths papillate.</description>
      <determination>12 Xyris montana</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Apex of Llateral sepals apex thin, not red; seeds ellipsoid, 0.4–0.8 mm; leaf sheaths smooth.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blade 1–2(–3) mm wide; spikes 10–16 mm; fertile bracts 4–6 mm; seeds 0.4–0.6 mm.</description>
      <determination>13 Xyris longisepala</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blade 5–15 mm wide; spikes 10–20(–25) mm; fertile bracts 5–8 mm; seeds (0.6–)0.7(–0.8) mm</description>
      <determination>14 Xyris smalliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Seeds opaque or mealy.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Seeds translucent or mealy.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Base of mature plant bulbous</description>
      <determination>15 Xyris tennesseensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Base of mature plant not bulbous.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blade smooth, at least 10 mm wide; scape distally with 1–2 smooth ribs</description>
      <determination>16a Xyris laxifolia var. iridifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blade papillate or somewhat scabrous, under 7 mm wide; scape distally with 3 or more somewhat scabrous ribs.</description>
      <determination>17 Xyris difformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Base of plant not bulbous; leaves in fans; plants flowering in morning</description>
      <determination>17 Xyris difformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Base of plant bulbous or nearly bulbous; principal leaves erect or ascending; plants flowering midday to afternoon.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Seeds 0.5–0.6 mm</description>
      <determination>18 Xyris platylepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Seeds 0.6–1 mm</description>
      <determination>19 Xyris scabrifolia</determination>
    </key_statement>
  </key>
</bio:treatment>