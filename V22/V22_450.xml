<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Piper" date="1906" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Contributions from the U. S. National Herbarium</publication_title>
      <place_in_publication>11: 182. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species covillei;</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000116</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="E. Meyer" date="unknown" rank="species">falcatus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">paniculatus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>2: 495. 1868,</place_in_publication>
      <other_info_on_pub>not J. paniculatus Hoppe ex Mertens &amp; W. D. J. Koch 1826</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species falcatus;variety paniculatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, strongly rhizomatous, 0.5–2.5 dm.</text>
      <biological_entity id="o2088" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms erect, slightly flattened.</text>
      <biological_entity id="o2089" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 2–5, cauline 2–4;</text>
      <biological_entity id="o2090" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2091" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o2092" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles absent or, if present, 0.5–1.5 mm, apex acute;</text>
      <biological_entity id="o2093" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2094" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s3" value="," value_original="," />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2095" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade flat, 5–15 cm × 2–3 mm.</text>
      <biological_entity id="o2096" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2097" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences glomerules, 1–6, each with 3–7 flowers, open to aggregate, 2–6 cm;</text>
      <biological_entity constraint="inflorescences" id="o2098" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="open" name="architecture" notes="" src="d0_s5" to="aggregate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2099" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <relation from="o2098" id="r266" name="with" negation="false" src="d0_s5" to="o2099" />
    </statement>
    <statement id="d0_s6">
      <text>primary bract or distal leaves usually exceeding inflorescences.</text>
      <biological_entity constraint="primary" id="o2100" name="bract" name_original="bract" src="d0_s6" type="structure" />
      <biological_entity constraint="primary distal" id="o2101" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o2102" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <relation from="o2100" id="r267" modifier="usually" name="exceeding" negation="false" src="d0_s6" to="o2102" />
      <relation from="o2101" id="r268" modifier="usually" name="exceeding" negation="false" src="d0_s6" to="o2102" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: tepals usually brown, ovate-oblong, 3–4 mm, margins scarious;</text>
      <biological_entity id="o2103" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2104" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-oblong" value_original="ovate-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2105" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner series slightly shorter, minutely papillate;</text>
      <biological_entity id="o2106" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o2107" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s8" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 6, filaments 0.8–1.4 mm, anthers 0.8–1.4 mm;</text>
      <biological_entity id="o2108" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2109" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o2110" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2111" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 1 mm.</text>
      <biological_entity id="o2112" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2113" name="style" name_original="style" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 3-locular, narrowly ovoid, 4–5 mm.</text>
      <biological_entity id="o2114" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds narrowly ovoid, 0.3 mm, not tailed.</text>
      <biological_entity id="o2115" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.; Coastal to montane areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Coastal to montane areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36</number>
  <other_name type="common_name">Coville's rush</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth dark brown, apex of inner series rounded; capsule dark brown, 1 mm longer than perianth</description>
      <determination>36a Juncus covillei var. covillei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth pale brown, apex of inner series acute to obtuse; capsule pale brown, barely exceeding perianth</description>
      <determination>36b Juncus covillei var. obtusatus</determination>
    </key_statement>
  </key>
</bio:treatment>