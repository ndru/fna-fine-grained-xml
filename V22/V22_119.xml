<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">lemnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">lemna</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">minor</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 970. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lemnaceae;genus lemna;species minor</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200027349</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots to 15 cm, tip mostly rounded;</text>
      <biological_entity id="o12302" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12303" name="tip" name_original="tip" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheath not winged.</text>
      <biological_entity id="o12304" name="sheath" name_original="sheath" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stipes white, small, often decaying.</text>
      <biological_entity id="o12305" name="stipe" name_original="stipes" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fronds floating, 1 or 2–5 or more, coherent in groups, ovate, scarcely gibbous, flat, 1–8 mm, 1.3–2 times as long as wide, margins entire;</text>
      <biological_entity id="o12306" name="frond" name_original="fronds" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_location" src="d0_s3" value="floating" value_original="floating" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character constraint="in groups" constraintid="o12307" is_modifier="false" name="fusion" src="d0_s3" value="coherent" value_original="coherent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s3" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1.3-2" value_original="1.3-2" />
      </biological_entity>
      <biological_entity id="o12307" name="group" name_original="groups" src="d0_s3" type="structure" />
      <biological_entity id="o12308" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins 3 (–5) (if more than 3, outer ones branching from inner ones), greatest distance between lateral-veins near or proximal to middle;</text>
      <biological_entity id="o12309" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character constraint="between lateral-veins" constraintid="o12310" is_modifier="false" name="size" src="d0_s4" value="greatest" value_original="greatest" />
        <character char_type="range_value" from="proximal" name="position" notes="" src="d0_s4" to="middle" />
      </biological_entity>
      <biological_entity id="o12310" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>papillae not always distinct (one near apex usually larger);</text>
      <biological_entity id="o12311" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not always" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lower surface very seldom slightly reddish (much less than on upper), coloring beginning from attachment point of root, upper surface occasionally diffusely reddish;</text>
      <biological_entity constraint="lower" id="o12312" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="very seldom slightly" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o12313" name="point" name_original="point" src="d0_s6" type="structure" />
      <biological_entity id="o12314" name="root" name_original="root" src="d0_s6" type="structure" />
      <relation from="o12312" id="r1644" name="beginning from" negation="false" src="d0_s6" to="o12313" />
      <relation from="o12313" id="r1645" name="part_of" negation="false" src="d0_s6" to="o12314" />
    </statement>
    <statement id="d0_s7">
      <text>air spaces 0.3 mm or shorter;</text>
      <biological_entity constraint="upper" id="o12315" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="occasionally diffusely" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o12316" name="space" name_original="spaces" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distinct turions absent.</text>
      <biological_entity id="o12317" name="turion" name_original="turions" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: ovaries 1-ovulate, utricular scale with narrow opening at apex.</text>
      <biological_entity id="o12318" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12319" name="ovary" name_original="ovaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-ovulate" value_original="1-ovulate" />
      </biological_entity>
      <biological_entity constraint="utricular" id="o12320" name="scale" name_original="scale" src="d0_s9" type="structure" />
      <biological_entity id="o12321" name="opening" name_original="opening" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o12322" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <relation from="o12320" id="r1646" name="with" negation="false" src="d0_s9" to="o12321" />
      <relation from="o12321" id="r1647" name="at" negation="false" src="d0_s9" to="o12322" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits 0.8–1 mm, laterally winged toward apex.</text>
      <biological_entity id="o12323" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character constraint="toward apex" constraintid="o12324" is_modifier="false" modifier="laterally" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o12324" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds with 8–15 distinct ribs, staying within fruit wall after ripening.</text>
      <biological_entity id="o12326" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o12327" name="wall" name_original="wall" src="d0_s11" type="structure" />
      <relation from="o12325" id="r1648" name="with" negation="false" src="d0_s11" to="o12326" />
      <relation from="o12325" id="r1649" modifier="after ripening" name="staying within" negation="false" src="d0_s11" to="o12327" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 40, 42 (B), 50, 63, 126.</text>
      <biological_entity id="o12325" name="seed" name_original="seeds" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o12328" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
        <character name="quantity" src="d0_s12" value="50" value_original="50" />
        <character name="quantity" src="d0_s12" value="63" value_original="63" />
        <character name="quantity" src="d0_s12" value="126" value_original="126" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (rare) late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="rare" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesotrophic to –eutrophic, quiet waters, in suboceanic, cool-temperate regions with relatively mild winters</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesotrophic" />
        <character name="habitat" value="eutrophic" constraint="in suboceanic , cool-temperate regions with relatively mild winters" />
        <character name="habitat" value="quiet waters" constraint="in suboceanic , cool-temperate regions with relatively mild winters" />
        <character name="habitat" value="suboceanic" constraint="with relatively mild winters" />
        <character name="habitat" value="cool-temperate regions" constraint="with relatively mild winters" />
        <character name="habitat" value="mild winters" modifier="relatively" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; B.C., Ont., Que., Sask.; Ala., Ariz., Ark., Calif., Conn., Del., D.C., Fla., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tenn., Utah, Vt., Va., Wash., W.Va., Wis.; w Eurasia; Africa; Atlantic Islands; Australia (introduced), Australia; introduced, New Zealand (introduced).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="w Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Australia ()" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="New Zealand ()" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Lenticule mineure</other_name>
  <discussion>Indication of this species in Newfoundland (H. J. Scoggan 1978–1979) probably refers to Lemna turionifera.</discussion>
  <discussion>A specimen in the Gray Herbarium from St. Pierre and Miquelon may represent Lemna minor or L. turionifera; its determination is questionable.</discussion>
  
</bio:treatment>