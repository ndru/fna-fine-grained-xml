<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert R. Haynes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Takhtajan ex Cronquist" date="unknown" rank="family">Limnocharitaceae</taxon_name>
    <taxon_hierarchy>family Limnocharitaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10509</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous or stoloniferous, caulescent, glabrous;</text>
      <biological_entity id="o13350" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sap milky.</text>
      <biological_entity id="o13351" name="sap" name_original="sap" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="milky" value_original="milky" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Roots not septate.</text>
      <biological_entity id="o13352" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal [alternate], submersed and floating [emersed], sessile or petiolate, sheathing proximally;</text>
      <biological_entity id="o13353" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="location" src="d0_s3" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s3" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade with translucent markings absent, basal lobes absent;</text>
      <biological_entity id="o13354" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o13355" name="marking" name_original="markings" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s4" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o13354" id="r1787" name="with" negation="false" src="d0_s4" to="o13355" />
    </statement>
    <statement id="d0_s5">
      <text>venation reticulate, primary-veins parallel from base of blade to apex, secondary-veins reticulate.</text>
      <biological_entity constraint="basal" id="o13356" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s5" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o13357" name="primary-vein" name_original="primary-veins" src="d0_s5" type="structure">
        <character constraint="from base" constraintid="o13358" is_modifier="false" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o13358" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o13359" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o13360" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o13361" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s5" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o13358" id="r1788" name="part_of" negation="false" src="d0_s5" to="o13359" />
      <relation from="o13358" id="r1789" name="to" negation="false" src="d0_s5" to="o13360" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences scapose umbels, floating [erect], bracteate.</text>
      <biological_entity id="o13362" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="growth_form_or_location" notes="" src="d0_s6" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o13363" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, hypogynous, pedicellate;</text>
      <biological_entity id="o13364" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals persistent, 3;</text>
      <biological_entity id="o13365" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals deciduous, 3, usually delicate;</text>
      <biological_entity id="o13366" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s9" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens [6–] 20–25, distinct;</text>
      <biological_entity id="o13367" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s10" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="25" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 4-loculed, dehiscing longitudinally;</text>
      <biological_entity id="o13368" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="4-loculed" value_original="4-loculed" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s11" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistils [3–] 10–12, coherent proximally [distinct], 1-loculed;</text>
    </statement>
    <statement id="d0_s13">
      <text>placentation laminar;</text>
      <biological_entity id="o13369" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="12" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s12" value="coherent" value_original="coherent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-loculed" value_original="1-loculed" />
        <character is_modifier="false" name="placentation" src="d0_s13" value="laminar" value_original="laminar" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 50 or more.</text>
      <biological_entity id="o13370" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or more" value="50" value_original="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits follicles.</text>
      <biological_entity constraint="fruits" id="o13371" name="follicle" name_original="follicles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds: embryo U-shaped;</text>
      <biological_entity id="o13372" name="seed" name_original="seeds" src="d0_s16" type="structure" />
      <biological_entity id="o13373" name="embryo" name_original="embryo" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="u--shaped" value_original="u--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endosperm absent in mature seed.</text>
      <biological_entity id="o13374" name="seed" name_original="seeds" src="d0_s17" type="structure" />
      <biological_entity id="o13375" name="endosperm" name_original="endosperm" src="d0_s17" type="structure">
        <character constraint="in seed" constraintid="o13376" is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13376" name="seed" name_original="seed" src="d0_s17" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s17" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; tropical and subtropical regions, North America, Central America, South America, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="tropical and subtropical regions" establishment_means="introduced" />
        <character name="distribution" value="North America" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>189</number>
  <other_name type="common_name">Water-poppy Ffamily</other_name>
  <discussion>Another genus, Limnocharis, was reported from southern Louisiana and southern Florida (W. C. Muenscher 1944). The genus was not included for Louisiana by J. W. Thieret (1972); it was included by R. K. Godfrey and J. W. Wooten (1979) on the basis of Muenscher’s report although the authors stated that they had seen no specimens from the United States. Also, the genus is not mapped in the "Florida Atlas" (available on the Internet). Richard Wunderlin, primary compiler of the "Atlas"," has indicated that no specimens have been examined (pers. comm.).</discussion>
  <discussion>Genera 3, species 8 (1 genus, 1 species in the flora).</discussion>
  <references>
    <reference>Haynes, R. R. and L. B. Holm-Nielsen. 1992. The Limnocharitaceae. In: Organization for Flora Neotropica. 1968+. Flora Neotropica. 75+ nosvols. New York. NoVol. 56, pp. 1–34.</reference>
  </references>
  
</bio:treatment>