<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Plumier ex Linnaeus" date="unknown" rank="genus">commelina</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">caroliniana</taxon_name>
    <place_of_publication>
      <publication_title>Flora Caroliniana, secundum.</publication_title>
      <place_in_publication>68. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus commelina;species caroliniana</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">222000037</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, diffusely spreading.</text>
      <biological_entity id="o3853" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="diffusely" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots at nodes.</text>
      <biological_entity id="o3854" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o3855" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o3854" id="r513" name="at" negation="false" src="d0_s1" to="o3855" />
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to scandent.</text>
      <biological_entity id="o3856" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="growth_form" src="d0_s2" to="scandent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade lanceolate to lanceolate-elliptic or lanceolate-oblong, 2.5–10.5 × 0.7–2.4 cm, margins scabrous, apex acute to acuminate, glabrous.</text>
      <biological_entity id="o3857" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3858" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="lanceolate-elliptic or lanceolate-oblong" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="2.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3859" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3860" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: distal cyme vestigial, included (rarely 1-flowered and exserted);</text>
      <biological_entity id="o3861" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o3862" name="cyme" name_original="cyme" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="position" src="d0_s4" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes solitary, bright green, paler basally, without contrasting veins, pedunculate, not at all to slightly falcate, 1.2–3 (–3.7) × 0.5–1 cm, margins distinct, usually ciliate, apex acuminate, glabrous or very sparsely pilose;</text>
      <biological_entity id="o3863" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o3864" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="bright green" value_original="bright green" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s5" value="paler" value_original="paler" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o3865" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o3866" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s5" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="1" to_unit="cm" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o3867" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s5" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="width" src="d0_s5" to="1" to_unit="cm" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o3864" id="r514" name="without" negation="false" src="d0_s5" to="o3865" />
      <relation from="o3864" id="r515" modifier="at" name="to" negation="false" src="d0_s5" to="o3866" />
      <relation from="o3864" id="r516" modifier="at" name="to" negation="false" src="d0_s5" to="o3867" />
    </statement>
    <statement id="d0_s6">
      <text>peduncles 0.6–2.3 cm.</text>
      <biological_entity id="o3868" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o3869" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s6" to="2.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o3870" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals all blue, proximal petal white medially, smaller;</text>
      <biological_entity id="o3871" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3872" name="petal" name_original="petal" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial stamen with white connective;</text>
      <biological_entity constraint="medial" id="o3873" name="stamen" name_original="stamen" src="d0_s9" type="structure" />
      <biological_entity id="o3874" name="connective" name_original="connective" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <relation from="o3873" id="r517" name="with" negation="false" src="d0_s9" to="o3874" />
    </statement>
    <statement id="d0_s10">
      <text>staminodes 3;</text>
      <biological_entity id="o3875" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>antherodes yellow, often with central maroon spot, cruciform.</text>
      <biological_entity id="o3876" name="antherode" name_original="antherodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="cruciform" value_original="cruciform" />
      </biological_entity>
      <biological_entity constraint="central" id="o3877" name="maroon-spot" name_original="maroon spot" src="d0_s11" type="structure" />
      <relation from="o3876" id="r518" modifier="often" name="with" negation="false" src="d0_s11" to="o3877" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 3-locular, 2-valved, (5–) 6–8 mm.</text>
      <biological_entity id="o3878" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-valved" value_original="2-valved" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 5, dark-brown, 2.4–4.3 (–4.6) × (1.6–) 2–2.3 mm, smooth to faintly alveolate, mealy.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = ca. 86.</text>
      <biological_entity id="o3879" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="4.3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s13" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_width" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s13" to="faintly alveolate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="mealy" value_original="mealy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3880" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="86" value_original="86" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (rarely winter).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="atypical_range" modifier="rarely" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="winter" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, roadsides, railroad rights-of-way, yards, waste places, especially in moist situations, weed in crops, especially rice, sugar cane and corn, and rarely in forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad rights-of-way" />
        <character name="habitat" value="yards" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="moist situations" modifier="especially in" />
        <character name="habitat" value="crops" modifier="weed in" />
        <character name="habitat" value="rice" />
        <character name="habitat" value="sugar cane" />
        <character name="habitat" value="corn" />
        <character name="habitat" value="forests" modifier="and rarely in" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Fla., Ga., La., Md., Miss., Mo., N.C., S.C., Tex.; native, India.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="India" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <references>
    <reference>Faden, R. B. 1989. Commelina caroliniana (Commelinaceae): A misunderstood species in the United States is an old introduction from Asia. Taxon 38: 43–53.</reference>
  </references>
  
</bio:treatment>