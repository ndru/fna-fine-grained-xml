<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Royle" date="unknown" rank="genus">murdannia</taxon_name>
    <taxon_name authority="(Linnaeus) Bruckner in H. G. A. Engler and K. Prantl" date="1930" rank="species">spirata</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl,Die naturlichen Pflanzenfamilien, zweite Auflage</publication_title>
      <place_in_publication>15a: 173. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus murdannia;species spirata</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">222000251</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Commelina</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">spirata</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>2: 176. 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Commelina;species spirata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, erect to decumbent, usually much branched with age, to 30 cm.</text>
      <biological_entity id="o15682" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="decumbent" />
        <character constraint="with age" constraintid="o15683" is_modifier="false" modifier="usually much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o15683" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade lanceolate-oblong to ovate, 1–4 × 0.3–1 cm.</text>
      <biological_entity id="o15684" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15685" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate-oblong" name="shape" src="d0_s1" to="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: cymes 1–2, several-flowered, elongate;</text>
      <biological_entity id="o15686" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o15687" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="several-flowered" value_original="several-flowered" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracteoles persistent, 2–6 mm apart.</text>
      <biological_entity id="o15688" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o15689" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers bisexual, radially symmetric, 8 mm wide;</text>
      <biological_entity id="o15690" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character name="width" src="d0_s4" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 2.5–4 mm;</text>
      <biological_entity id="o15691" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals rose or lavender with darker veins, 4–5 mm;</text>
      <biological_entity id="o15692" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose" value_original="rose" />
        <character constraint="with veins" constraintid="o15693" is_modifier="false" name="coloration" src="d0_s6" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15693" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="darker" value_original="darker" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>fertile stamens 3;</text>
      <biological_entity id="o15694" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments bearded;</text>
      <biological_entity id="o15695" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminodes 3.</text>
      <biological_entity id="o15696" name="staminode" name_original="staminodes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 3–5 mm.</text>
      <biological_entity id="o15697" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 3–7 per locule, less than 1 mm, warty.</text>
      <biological_entity id="o15698" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o15699" from="3" name="quantity" src="d0_s11" to="7" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity id="o15699" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall–early winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early winter" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Palm hammocks, low prairies, glades, pastures, and roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="palm hammocks" />
        <character name="habitat" value="low prairies" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; native, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>Murdannia spirata was first collected in Florida in 1965.</discussion>
  
</bio:treatment>