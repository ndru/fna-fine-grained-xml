<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">149</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">lemnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">lemna</taxon_name>
    <taxon_name authority="Philippi" date="1864" rank="species">valdiviana</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>33:239. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lemnaceae;genus lemna;species valdiviana</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000211</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lemna</taxon_name>
    <taxon_name authority="(Elliott) C. H. Thompson" date="unknown" rank="species">cyclostasa</taxon_name>
    <taxon_hierarchy>genus Lemna;species cyclostasa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lemna</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">torreyi</taxon_name>
    <taxon_hierarchy>genus Lemna;species torreyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots to 1.5 cm, tip rounded to pointed;</text>
      <biological_entity id="o13768" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13769" name="tip" name_original="tip" src="d0_s0" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s0" to="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheath not winged.</text>
      <biological_entity id="o13770" name="sheath" name_original="sheath" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stipes white, small, often decaying.</text>
      <biological_entity id="o13771" name="stipe" name_original="stipes" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fronds floating or (rarely) submersed, 1 or 2–few, coherent in groups, ovate to lanceolate, flat, thin, 1–5 mm, 1.3–3 times as long as wide, margins entire;</text>
      <biological_entity id="o13772" name="frond" name_original="fronds" src="d0_s3" type="structure">
        <character is_modifier="false" name="location" src="d0_s3" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s3" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="1-or-2-few" value_original="1-or-2-few" />
        <character constraint="in groups" constraintid="o13773" is_modifier="false" name="fusion" src="d0_s3" value="coherent" value_original="coherent" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1.3-3" value_original="1.3-3" />
      </biological_entity>
      <biological_entity id="o13773" name="group" name_original="groups" src="d0_s3" type="structure" />
      <biological_entity id="o13774" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins 1, mostly prominent, longer than extension of air spaces, or running through at least 3/4 of distance between node and apex;</text>
      <biological_entity id="o13776" name="extension" name_original="extension" src="d0_s4" type="structure" constraint="air; air" />
      <biological_entity id="o13777" name="air" name_original="air" src="d0_s4" type="structure" />
      <biological_entity id="o13778" name="space" name_original="spaces" src="d0_s4" type="structure" />
      <biological_entity id="o13779" name="node" name_original="node" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="through at-least" name="quantity" src="d0_s4" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o13780" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="through at-least" name="quantity" src="d0_s4" value="3/4" value_original="3/4" />
      </biological_entity>
      <relation from="o13776" id="r1837" name="part_of" negation="false" src="d0_s4" to="o13777" />
      <relation from="o13776" id="r1838" name="part_of" negation="false" src="d0_s4" to="o13778" />
      <relation from="o13775" id="r1839" name="running" negation="false" src="d0_s4" to="o13779" />
      <relation from="o13775" id="r1840" name="running" negation="false" src="d0_s4" to="o13780" />
    </statement>
    <statement id="d0_s5">
      <text>with or without small papillae along midline of upper surface;</text>
      <biological_entity id="o13775" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" modifier="mostly" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character constraint="than extension" constraintid="o13776" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13781" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13782" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <relation from="o13775" id="r1841" name="with or without" negation="false" src="d0_s5" to="o13781" />
      <relation from="o13781" id="r1842" name="part_of" negation="false" src="d0_s5" to="o13782" />
    </statement>
    <statement id="d0_s6">
      <text>anthocyanin absent;</text>
    </statement>
    <statement id="d0_s7">
      <text>largest air spaces much shorter than 0.3 mm;</text>
      <biological_entity id="o13783" name="anthocyanin" name_original="anthocyanin" src="d0_s6" type="substance">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s7" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o13784" name="space" name_original="spaces" src="d0_s7" type="structure">
        <character modifier="much shorter than" name="some_measurement" src="d0_s7" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>turions absent.</text>
      <biological_entity id="o13785" name="turion" name_original="turions" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: ovaries 1-ovulate, utricular scale open on 1 side.</text>
      <biological_entity id="o13786" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13787" name="ovary" name_original="ovaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-ovulate" value_original="1-ovulate" />
      </biological_entity>
      <biological_entity constraint="utricular" id="o13788" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character constraint="on side" constraintid="o13789" is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o13789" name="side" name_original="side" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits 1–1.35 mm, not winged.</text>
      <biological_entity id="o13790" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.35" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds with 15–29 distinct ribs.</text>
      <biological_entity id="o13792" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s11" to="29" />
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o13791" id="r1843" name="with" negation="false" src="d0_s11" to="o13792" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 40, 42.</text>
      <biological_entity id="o13791" name="seed" name_original="seeds" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o13793" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (very rare) spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="very rare" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesotrophic, quiet waters in temperate to tropical regions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesotrophic" constraint="in temperate to tropical regions" />
        <character name="habitat" value="quiet waters" constraint="in temperate to tropical regions" />
        <character name="habitat" value="temperate" />
        <character name="habitat" value="tropical regions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Calif., Conn., D.C., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Mass., Mich., Miss., Mo., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va., Wyo.; Mexico; West Indies (Bermuda); Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Bermuda)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <discussion>I know of no specimens of Lemna valdiviana from Delaware, but the species is to be expected there.</discussion>
  
</bio:treatment>