<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Poiophylli</taxon_name>
    <taxon_name authority="Engelmann" date="1866" rank="species">vaseyi</taxon_name>
    <place_of_publication>
      <place_in_publication>2:448. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus poiophylli;species vaseyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000196</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Oakes &amp; Tuckerman" date="unknown" rank="species">greenei</taxon_name>
    <taxon_name authority="(Engelmann) B. Boivin" date="unknown" rank="variety">vaseyi</taxon_name>
    <taxon_hierarchy>genus Juncus;species greenei;variety vaseyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, tufted, 2–7 dm.</text>
      <biological_entity id="o16901" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes densely branching.</text>
      <biological_entity id="o16902" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 1–15 (–25).</text>
      <biological_entity id="o16903" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="25" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1–2 (–3).</text>
      <biological_entity id="o16904" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, (1–) 2–3;</text>
      <biological_entity id="o16905" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 0.2–0.4 (–0.6) mm, scarious, rarely ± leathery;</text>
      <biological_entity id="o16906" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="rarely more or less" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade green, nearly terete, 10–30 cm × 0.5–1 mm, margins entire.</text>
      <biological_entity id="o16907" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16908" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, 5–15 (–30) -flowered, congested or not, 1–5 cm;</text>
      <biological_entity id="o16909" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-15(-30)-flowered" value_original="5-15(-30)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
        <character name="architecture_or_arrangement" src="d0_s7" value="not" value_original="not" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary bract usually much shorter than inflorescence.</text>
      <biological_entity constraint="primary" id="o16910" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character constraint="than inflorescence" constraintid="o16911" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="usually much shorter" value_original="usually much shorter" />
      </biological_entity>
      <biological_entity id="o16911" name="inflorescence" name_original="inflorescence" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: bracteoles 2;</text>
      <biological_entity id="o16912" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16913" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals greenish to tan, lanceolate or widely so, 3.3–4.4 mm;</text>
      <biological_entity id="o16914" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16915" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character name="shape" src="d0_s10" value="widely" value_original="widely" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer and inner series nearly equal, apex acuminate;</text>
      <biological_entity id="o16916" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="outer and inner" id="o16917" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o16918" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6, filaments 0.5–0.9 mm, anthers 0.4–0.8 mm;</text>
      <biological_entity id="o16919" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16920" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o16921" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16922" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 0.1–0.2 mm.</text>
      <biological_entity id="o16923" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16924" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules golden tan or light-brown, 3-locular, ellipsoid, (3.3–) 3.8–4.7 × (1.1–) 1.3–1.7 mm.</text>
      <biological_entity id="o16925" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="golden tan" value_original="golden tan" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="atypical_length" src="d0_s14" to="3.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s14" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="atypical_width" src="d0_s14" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s14" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds tan, ellipsoid to lunate, body (0.52–) 5.5–0.65 (–0.7) mm, tails 0.2–0.5 mm. 2n = ca. 80.</text>
      <biological_entity id="o16926" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="lunate" />
      </biological_entity>
      <biological_entity id="o16927" name="body" name_original="body" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.52" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.65" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16928" name="tail" name_original="tails" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16929" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Permanently moist, usually exposed areas including wet meadows, raised sites or margins of bogs, or depressions along sandy lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" modifier="permanently" />
        <character name="habitat" value="exposed areas" modifier="usually" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="sites" modifier="raised" constraint="of bogs , or depressions along sandy lakeshores" />
        <character name="habitat" value="margins" constraint="of bogs , or depressions along sandy lakeshores" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="depressions" constraint="along sandy lakeshores" />
        <character name="habitat" value="sandy lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Labr.), N.W.T., N.S., Ont., Que., Sask.; Colo., Idaho, Ill., Iowa, Maine, Mich., Minn., Mont., N.Dak., N.Y., S.Dak., Vt., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17</number>
  <other_name type="common_name">Vasey's rush</other_name>
  
</bio:treatment>