<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sue A. Thompson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">Acoraceae</taxon_name>
    <taxon_hierarchy>family Acoraceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20008</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, wetland, usually with aromatic oil, especially in rhizomes.</text>
      <biological_entity id="o7082" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7083" name="oil" name_original="oil" src="d0_s0" type="substance">
        <character is_modifier="true" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
      </biological_entity>
      <biological_entity id="o7084" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o7082" id="r936" modifier="usually" name="with" negation="false" src="d0_s0" to="o7083" />
      <relation from="o7082" id="r937" modifier="especially" name="in" negation="false" src="d0_s0" to="o7084" />
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes horizontal, creeping at or near surface, branched.</text>
      <biological_entity id="o7085" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="near surface" value_original="near surface" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o7086" name="surface" name_original="surface" src="d0_s1" type="structure" />
      <relation from="o7085" id="r938" name="near" negation="false" src="d0_s1" to="o7086" />
    </statement>
    <statement id="d0_s2">
      <text>Stems repent, branched rhizomes.</text>
      <biological_entity id="o7087" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="repent" value_original="repent" />
      </biological_entity>
      <biological_entity id="o7088" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls absent.</text>
      <biological_entity id="o7089" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves not differentiated into petiole and blade, equitant, sword-shaped, larger than 1.5 cm;</text>
      <biological_entity id="o7091" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o7092" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>venation parallel along length of leaf.</text>
      <biological_entity id="o7090" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="into blade" constraintid="o7092" is_modifier="false" modifier="not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s4" value="equitant" value_original="equitant" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sword--shaped" value_original="sword--shaped" />
        <character modifier="larger than" name="some_measurement" src="d0_s4" unit="cm" value="1.5" value_original="1.5" />
        <character constraint="along " constraintid="o7093" is_modifier="false" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o7093" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: spadices, from 3-angled axis (peduncle fused with proximal portion of sympodial leaf, i.e., leaf encircling terminal inflorescence), distal sympodial leaf extending beyond spadix;</text>
      <biological_entity id="o7094" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o7095" name="spadex" name_original="spadices" src="d0_s6" type="structure" />
      <biological_entity id="o7096" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7097" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="sympodial" value_original="sympodial" />
      </biological_entity>
      <biological_entity id="o7098" name="spadix" name_original="spadix" src="d0_s6" type="structure" />
      <relation from="o7094" id="r939" name="from" negation="false" src="d0_s6" to="o7096" />
      <relation from="o7097" id="r940" name="extending beyond" negation="false" src="d0_s6" to="o7098" />
    </statement>
    <statement id="d0_s7">
      <text>true spathe absent;</text>
      <biological_entity id="o7099" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o7100" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s7" value="true" value_original="true" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>spadix nearly cylindric, tapering, apex obtuse.</text>
      <biological_entity id="o7101" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o7102" name="spadix" name_original="spadix" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o7103" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o7104" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals 6;</text>
      <biological_entity id="o7105" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, distinct;</text>
    </statement>
    <statement id="d0_s12">
      <text>ovariesy 1, (1–) 3-locular, sessile;</text>
      <biological_entity id="o7106" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="(1-)3-locular" value_original="(1-)3-locular" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas sessile (styles essentially absent), minute.</text>
      <biological_entity id="o7107" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s13" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits berries;</text>
      <biological_entity constraint="fruits" id="o7108" name="berry" name_original="berries" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pericarp thin, leathery.</text>
      <biological_entity id="o7109" name="pericarp" name_original="pericarp" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–6 (–14), from apex of locule.</text>
      <biological_entity id="o7110" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="14" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="6" />
      </biological_entity>
      <biological_entity id="o7111" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o7112" name="locule" name_original="locule" src="d0_s16" type="structure" />
      <relation from="o7110" id="r941" name="from" negation="false" src="d0_s16" to="o7111" />
      <relation from="o7111" id="r942" name="part_of" negation="false" src="d0_s16" to="o7112" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate Northern Hemisphere, tropical Asia at higher elevations, and sporadically introduced into Southern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate Northern Hemisphere" establishment_means="introduced" />
        <character name="distribution" value="tropical Asia at higher elevations" establishment_means="introduced" />
        <character name="distribution" value="and sporadically  into Southern Hemisphere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>202</number>
  <other_name type="common_name">Sweet-Flag Family</other_name>
  <discussion>Acorus historically was recognized as an aberrant genus within Araceae, but much evidence supports its treatment as a separate family and the removal of this family from Arales (M. H. Grayum 1987). Other than the absence of a close association with Arales, the phylogenetic affinities of Acoraceae remain unclear. Evidence based on DNA sequences fails to show any close relationships between Acorus and other genera, and instead supports Acorus as the oldest extant lineage of monocotyledons (M. R. Duvall et al. 1993).</discussion>
  <discussion>The removal of Acorus from Araceae is supported by the absence of a spathe and the unique vasculature of the structure traditionally interpreted as a spathe (T. S. Ray 1987). The structure that has been called a spathe in Acorus is not morphologically equivalent to the spathe of Araceae; instead it is interpreted as the distal part of the sympodial leaf. The proximal part of the sympodial leaf is adnate to the peduncle, forming a 3-angled axis that bears the inflorescence.</discussion>
  <discussion>Genera 1, species 3–6 (2 species in the flora).</discussion>
  <references>
    <reference>Grayum, M. H. 1987. A summary of evidence and arguments supporting the removal of Acorus from the Araceae. Taxon 36: 723–729.</reference>
    <reference>Thompson, S. A. 1995. Systematics and Biology of the Araceae and Acoraceae of Temperate North America. Ph.D. dissertation. University of Illinois. Urbana-Champaign.</reference>
  </references>
  
</bio:treatment>