<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="Reichenbach in H. G. L. Reichenbach et al." date="1845" rank="species">confervoides</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. L. Reichenbach et al.,Icones florae germanicae et helveticae</publication_title>
      <place_in_publication>7: 13. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species confervoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000280</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potamogeton</taxon_name>
    <taxon_name authority="J. W. Robbins ([as tuckermani)]" date="unknown" rank="species">tuckermanii</taxon_name>
    <taxon_hierarchy>genus Potamogeton;species tuckermanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes obvious.</text>
      <biological_entity id="o9542" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s0" value="obvious" value_original="obvious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems terete, without spots, 10–80 cm.</text>
      <biological_entity constraint="cauline" id="o9543" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9544" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o9543" id="r1268" name="without" negation="false" src="d0_s1" to="o9544" />
    </statement>
    <statement id="d0_s2">
      <text>Turions present, in axils of old leaves and from disintegrating branches, fusiform, 0.7–2 cm, leaves spreading to ascending.</text>
      <biological_entity id="o9545" name="turion" name_original="turions" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9546" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s2" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o9547" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
      <relation from="o9545" id="r1269" name="in axils of old leaves and from" negation="false" src="d0_s2" to="o9546" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves submersed, spirally arranged, flaccid, sessile;</text>
      <biological_entity id="o9548" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="location" src="d0_s3" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="texture" src="d0_s3" value="flaccid" value_original="flaccid" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules deliquescent, inconspicuous, convolute, free from blade, pale green, not ligulate, 0.5–1.2 cm, not fibrous, not shredding at tip, apex obtuse;</text>
      <biological_entity id="o9549" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_texture" src="d0_s4" value="deliquescent" value_original="deliquescent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="convolute" value_original="convolute" />
        <character constraint="from blade" constraintid="o9550" is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1.2" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o9550" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o9551" name="tip" name_original="tip" src="d0_s4" type="structure" />
      <biological_entity id="o9552" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o9549" id="r1270" name="shredding at" negation="true" src="d0_s4" to="o9551" />
    </statement>
    <statement id="d0_s5">
      <text>blade pale green, linear, not arcuate, 1.8–6.5 cm × 0.1–0.5 mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, extremely attenuate, bristly, lacunae present, each side of midvein to margins;</text>
      <biological_entity id="o9553" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s5" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s5" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9554" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9555" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity id="o9556" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o9557" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="hoodlike" value_original="hoodlike" />
        <character is_modifier="false" modifier="extremely" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o9558" name="lacuna" name_original="lacunae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9559" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o9560" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o9561" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o9554" id="r1271" name="without" negation="false" src="d0_s5" to="o9555" />
      <relation from="o9559" id="r1272" name="part_of" negation="false" src="d0_s5" to="o9560" />
      <relation from="o9559" id="r1273" name="to" negation="false" src="d0_s5" to="o9561" />
    </statement>
    <statement id="d0_s6">
      <text>veins 1.</text>
      <biological_entity id="o9562" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o9563" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s7" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles not dimorphic, terminal, ascending, somewhat clavate, (3–) 5–25 cm;</text>
      <biological_entity id="o9564" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s8" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>spikes not dimorphic, capitate, 5–12 mm.</text>
      <biological_entity id="o9565" name="spike" name_original="spikes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits sessile, light green, round-obovoid or nearly orbicular, compressed, abaxially and laterally keeled, 2–3 × 1.7–2.8 mm, lateral keels without sharp point;</text>
      <biological_entity id="o9566" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="round-obovoid" value_original="round-obovoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="abaxially; laterally" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9567" name="keel" name_original="keels" src="d0_s10" type="structure" />
      <biological_entity id="o9568" name="point" name_original="point" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="sharp" value_original="sharp" />
      </biological_entity>
      <relation from="o9567" id="r1274" name="without" negation="false" src="d0_s10" to="o9568" />
    </statement>
    <statement id="d0_s11">
      <text>beak erect, 0.5 mm;</text>
      <biological_entity id="o9569" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o9570" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity constraint="basal" id="o9571" name="tubercle" name_original="tubercles" src="d0_s12" type="structure" />
      <relation from="o9570" id="r1275" name="without" negation="false" src="d0_s12" to="o9571" />
    </statement>
    <statement id="d0_s13">
      <text>embryo with less than 1 full spiral.</text>
      <biological_entity id="o9572" name="embryo" name_original="embryo" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Chromosome number apparently unknown not available.</text>
      <biological_entity id="o9573" name="chromosome" name_original="chromosome" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic waters of bogs, ponds, and lakes, often at higher elevation in e portion of range</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic waters" constraint="of bogs , ponds , and lakes , often at higher elevation in e" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="higher elevation" />
        <character name="habitat" value="portion" modifier="in e" constraint="of range" />
        <character name="habitat" value="range" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon, St. Pierre; N.B., Nfld. and Labr., N.S., Ont., Que.; Conn., Maine, Mass., Mich., N.H., N.J., N.Y., N.C., Pa., R.I., S.C., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" value="St. Pierre" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Alga pondweed</other_name>
  <other_name type="common_name">Tuckerman’s pondweed</other_name>
  <other_name type="common_name">potamot confervoide</other_name>
  <discussion>Potamogeton confervoides is most uncommon and found only in fairly acidic waters. It is easily recognized by its linear, bristly leaves and the unusually long peduncle that seems out of place on a plant with such fine leaves. The leaves are so fine that they almost appear as greenish colored hair in the water. When the plant is removed from the water, the leaves are extremely flaccid leaves and essentially collapse onto each other.</discussion>
  
</bio:treatment>