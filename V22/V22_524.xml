<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">107</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schultz Schultzenstein" date="unknown" rank="family">arecaceae</taxon_name>
    <taxon_name authority="Griffith" date="1844" rank="subfamily">CORYPHOIDEAE</taxon_name>
    <taxon_name authority="Martius in S. L. Endlicher" date="1837" rank="tribe">CORYPHEAE</taxon_name>
    <taxon_name authority="Martius in S. L. Endlicher" date="1837" rank="subtribe">SABALINAE</taxon_name>
    <taxon_name authority="Adanson ex Guersent" date="1804" rank="genus">Sabal</taxon_name>
    <place_of_publication>
      <publication_title>Bulletin des Sciences, par la Societe Philomatique</publication_title>
      <place_in_publication>87: 205-206. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family arecaceae;subfamily coryphoideae;tribe corypheae;subtribe sabalinae;genus sabal;</taxon_hierarchy>
    <other_info_on_name type="etymology">derivation of name unknown</other_info_on_name>
    <other_info_on_name type="fna_id">128944</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="O. F. Cook" date="unknown" rank="genus">Inodes</taxon_name>
    <taxon_hierarchy>genus Inodes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dwarf, moderate, or tall, usually robust.</text>
      <biological_entity id="o609" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height" src="d0_s0" value="dwarf" value_original="dwarf" />
        <character is_modifier="false" name="size" src="d0_s0" value="moderate" value_original="moderate" />
        <character is_modifier="false" name="height" src="d0_s0" value="tall" value_original="tall" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, aerial or subterranean, covered with leaf-bases or clean, obscurely [strongly] ringed, becoming striate or smooth with age.</text>
      <biological_entity id="o610" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <biological_entity id="o611" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="obscurely" name="relief" src="d0_s1" value="ringed" value_original="ringed" />
        <character is_modifier="true" modifier="becoming" name="pubescence" src="d0_s1" value="striate" value_original="striate" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o610" id="r86" name="covered with" negation="false" src="d0_s1" to="o611" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves few-to-many;</text>
      <biological_entity id="o612" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s2" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fibers soft;</text>
      <biological_entity constraint="sheath" id="o613" name="fiber" name_original="fibers" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole split at base, completely unarmed;</text>
      <biological_entity constraint="petiole" id="o614" name="split" name_original="split" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="completely" name="architecture" notes="" src="d0_s4" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o615" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o614" id="r87" name="at" negation="false" src="d0_s4" to="o615" />
    </statement>
    <statement id="d0_s5">
      <text>adaxial hastula well developed, obtuse to acuminate-triangular;</text>
      <biological_entity constraint="adaxial" id="o616" name="hastulum" name_original="hastula" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s5" value="developed" value_original="developed" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acuminate-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa present;</text>
      <biological_entity id="o617" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade weakly to strongly costapalmate;</text>
      <biological_entity id="o618" name="blade" name_original="blade" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>plication induplicate;</text>
      <biological_entity id="o619" name="plication" name_original="plication" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="induplicate" value_original="induplicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>segments lanceolate, basally connate to connate for 2 1/2 length [or in groups of 2 or 3 segments connate for nearly entire length], often bearing threadlike fibers between segments;</text>
      <biological_entity id="o620" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="basally connate" name="length" src="d0_s9" to="connate" />
      </biological_entity>
      <biological_entity id="o621" name="fiber" name_original="fibers" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="thread-like" value_original="threadlike" />
      </biological_entity>
      <biological_entity id="o623.o620." name="segment-segment" name_original="segments" src="d0_s9" type="structure" />
      <relation from="o620" id="r88" modifier="often" name="bearing" negation="false" src="d0_s9" to="o621" />
    </statement>
    <statement id="d0_s10">
      <text>apices acute or 2-cleft, stiff or lax.</text>
      <biological_entity id="o624" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="2-cleft" value_original="2-cleft" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences axillary within crown of leaves, paniculate, erect or arching beyond leaves [shorter than leaves], with 2 or 3 [–4] orders of branching;</text>
      <biological_entity id="o625" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character constraint="within crown" constraintid="o626" is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character constraint="beyond leaves" constraintid="o628" is_modifier="false" name="orientation" src="d0_s11" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o626" name="crown" name_original="crown" src="d0_s11" type="structure" />
      <biological_entity id="o627" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o628" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <relation from="o626" id="r89" name="part_of" negation="false" src="d0_s11" to="o627" />
    </statement>
    <statement id="d0_s12">
      <text>peduncular bracts 2–5, tightly clasping, inconspicuous;</text>
      <biological_entity constraint="peduncular" id="o629" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" modifier="tightly" name="architecture_or_fixation" src="d0_s12" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachillae glabrous.</text>
      <biological_entity id="o630" name="rachilla" name_original="rachillae" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers bisexual, borne singly along rachillae, sessile, creamy white, fragrant;</text>
      <biological_entity id="o631" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="odor" src="d0_s14" value="fragrant" value_original="fragrant" />
      </biological_entity>
      <biological_entity id="o632" name="rachilla" name_original="rachillae" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o631" id="r90" name="borne" negation="false" src="d0_s14" to="o632" />
    </statement>
    <statement id="d0_s15">
      <text>perianth 2-seriate;</text>
      <biological_entity id="o633" name="perianth" name_original="perianth" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s15" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calyx cupulate;</text>
    </statement>
    <statement id="d0_s17">
      <text>3-lobed;</text>
      <biological_entity id="o634" name="calyx" name_original="calyx" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 3, imbricate, elliptic, obovate or spatulate, alternate with outer whorl of stamens [basally connate], basally adnate to filaments;</text>
      <biological_entity id="o635" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s18" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spatulate" value_original="spatulate" />
        <character constraint="with outer whorl" constraintid="o636" is_modifier="false" name="arrangement" src="d0_s18" value="alternate" value_original="alternate" />
        <character constraint="to filaments" constraintid="o638" is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s18" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o636" name="whorl" name_original="whorl" src="d0_s18" type="structure" />
      <biological_entity id="o637" name="stamen" name_original="stamens" src="d0_s18" type="structure" />
      <biological_entity id="o638" name="filament" name_original="filaments" src="d0_s18" type="structure" />
      <relation from="o636" id="r91" name="part_of" negation="false" src="d0_s18" to="o637" />
    </statement>
    <statement id="d0_s19">
      <text>stamens 6 in 2 whorls;</text>
      <biological_entity id="o639" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character constraint="in whorls" constraintid="o640" name="quantity" src="d0_s19" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o640" name="whorl" name_original="whorls" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>filaments narrowly triangular, basally connate;</text>
      <biological_entity id="o641" name="filament" name_original="filaments" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s20" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers dorsifixed, versatile;</text>
      <biological_entity id="o642" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s21" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="fixation" src="d0_s21" value="versatile" value_original="versatile" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>pistils 1, 1-carpellate, glabrous;</text>
      <biological_entity id="o643" name="pistil" name_original="pistils" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="1-carpellate" value_original="1-carpellate" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>nectaries 3, septal;</text>
      <biological_entity id="o644" name="nectary" name_original="nectaries" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s23" value="septal" value_original="septal" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>ovules 3, but usually only one develops into seed;</text>
      <biological_entity id="o645" name="ovule" name_original="ovules" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o646" name="seed" name_original="seed" src="d0_s24" type="structure" />
      <relation from="o645" id="r92" modifier="usually only; only" name="develops into" negation="false" src="d0_s24" to="o646" />
    </statement>
    <statement id="d0_s25">
      <text>stigma minutely 3-lobed, papillose.</text>
      <biological_entity id="o647" name="stigma" name_original="stigma" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s25" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="relief" src="d0_s25" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>Fruits drupes, berrylike, spheroid [oblate or pyriform] or lobed when more than 1 seed develops;</text>
      <biological_entity constraint="fruits" id="o648" name="drupe" name_original="drupes" src="d0_s26" type="structure">
        <character is_modifier="false" name="shape" src="d0_s26" value="berrylike" value_original="berrylike" />
        <character is_modifier="false" modifier="when 1+" name="shape" src="d0_s26" value="spheroid" value_original="spheroid" />
        <character is_modifier="false" modifier="when 1+" name="shape" src="d0_s26" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o649" name="seed" name_original="seed" src="d0_s26" type="structure" />
    </statement>
    <statement id="d0_s27">
      <text>exocarp black;</text>
      <biological_entity id="o650" name="exocarp" name_original="exocarp" src="d0_s27" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s27" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>mesocarp blackish, dry to fleshy;</text>
      <biological_entity id="o651" name="mesocarp" name_original="mesocarp" src="d0_s28" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s28" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="dry" name="texture" src="d0_s28" to="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>endocarp brown, membranaceous.</text>
      <biological_entity id="o652" name="endocarp" name_original="endocarp" src="d0_s29" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s29" value="brown" value_original="brown" />
        <character is_modifier="false" name="texture" src="d0_s29" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>Seeds 1–3, oblate, glossy;</text>
      <biological_entity id="o653" name="seed" name_original="seeds" src="d0_s30" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s30" to="3" />
        <character is_modifier="false" name="shape" src="d0_s30" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="reflectance" src="d0_s30" value="glossy" value_original="glossy" />
      </biological_entity>
    </statement>
    <statement id="d0_s31">
      <text>endosperm bony, homogeneous;</text>
      <biological_entity id="o654" name="endosperm" name_original="endosperm" src="d0_s31" type="structure">
        <character is_modifier="false" name="texture" src="d0_s31" value="osseous" value_original="bony" />
        <character is_modifier="false" name="variability" src="d0_s31" value="homogeneous" value_original="homogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s32">
      <text>embryo nearly apical, lateral or nearly lateral;</text>
      <biological_entity id="o655" name="embryo" name_original="embryo" src="d0_s32" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s32" value="apical" value_original="apical" />
        <character is_modifier="false" name="position" src="d0_s32" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s32" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s33">
      <text>eophyll undivided, linear-lanceolate.</text>
    </statement>
    <statement id="d0_s34">
      <text>nx = 18.</text>
      <biological_entity id="o656" name="eophyll" name_original="eophyll" src="d0_s33" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s33" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s33" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Palmetto</other_name>
  <discussion>Sabal flowers are bisexual and are pollinated mostly by native bees, especially of the families Halictidae and Megachilidae (S. Zona 1987, 1990), European honeybee, and wasps (Vespidae; P. F. Ramp 1989). Fruits are eagerly sought by both mammals (bears, deer, raccoons) and birds (S. Zona and A. Henderson 1989). Sabal minor, perhaps more than other species, is also dispersed by water (P. F. Ramp 1989; S. Zona 1990).</discussion>
  <discussion>Species 16 (5 in the flora).</discussion>
  <references>
    <reference>Lockett, L. 1991. Native Texas palms north of the lower Rio Grande Valley: Recent discoveries. Principes 35: 64–71.</reference>
    <reference>Simpson, B. J. 1988. A Field Guide to Texas Trees. Austin.</reference>
    <reference>Zona, S. 1990. A monograph of Sabal (Arecaceae: Coryphoideae). Aliso 12: 583–666.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences with 2 (rarely 3 in basal branches) orders of branching (not counting main axis).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence with 3 orders of branching.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf strongly costapalmate and curved, bearing fibers between segments; inflorescence bushy and compact, ± long as leaves</description>
      <determination>1 Sabal etonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf weakly costapalmate and little if at all curved, not bearing fibers between segments; inflorescence sparsely branched, much longer than leaves</description>
      <determination>4 Sabal minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems subterranean; leaves 6 or fewer</description>
      <determination>3 Sabal miamiensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems usually aerial; leaves more than 10.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruit 8.1–13.9 mm diam.</description>
      <determination>5 Sabal palmetto</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruit 14.8–19.3 mm diam</description>
      <determination>2 Sabal mexicana</determination>
    </key_statement>
  </key>
</bio:treatment>