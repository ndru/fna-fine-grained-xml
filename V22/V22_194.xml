<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Tradescantia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 288. 1753; Gen. Pl. ed. 5; 139, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus Tradescantia</taxon_hierarchy>
    <other_info_on_name type="etymology">for John Tradescant, gardener to Charles I of England</other_info_on_name>
    <other_info_on_name type="fna_id">133268</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Hance" date="unknown" rank="genus">Rhoeo</taxon_name>
    <taxon_hierarchy>genus Rhoeo;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Schumann &amp; Sydow" date="unknown" rank="genus">Setcreasea</taxon_name>
    <taxon_hierarchy>genus Setcreasea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Schnizlein" date="unknown" rank="genus">Zebrina</taxon_name>
    <taxon_hierarchy>genus Zebrina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o15020" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots thin or tuberous.</text>
      <biological_entity id="o15021" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spirally arranged or 2-ranked;</text>
      <biological_entity id="o15022" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s2" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade sessile or rarely petiolate [petiolate].</text>
      <biological_entity id="o15023" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or terminal and axillary, pairs of cymes, cymes sessile, umbellike, contracted, subtended by spathaceous bract;</text>
      <biological_entity id="o15024" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o15025" name="cyme" name_original="cymes" src="d0_s4" type="structure" />
      <biological_entity id="o15026" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="umbel-like" value_original="umbellike" />
        <character is_modifier="false" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o15027" name="bract" name_original="bract" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="spathaceous" value_original="spathaceous" />
      </biological_entity>
      <relation from="o15024" id="r1975" name="part_of" negation="false" src="d0_s4" to="o15025" />
      <relation from="o15026" id="r1976" name="subtended by" negation="false" src="d0_s4" to="o15027" />
    </statement>
    <statement id="d0_s5">
      <text>bract similar to leaves or differentiated, margins distinct;</text>
      <biological_entity id="o15028" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o15029" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15030" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o15028" id="r1977" name="to" negation="false" src="d0_s5" to="o15029" />
      <relation from="o15028" id="r1978" name="to" negation="false" src="d0_s5" to="o15030" />
    </statement>
    <statement id="d0_s6">
      <text>bracteoles persistent.</text>
      <biological_entity id="o15031" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o15032" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels very short or well developed;</text>
      <biological_entity id="o15033" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="well; well" name="development" src="d0_s8" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals distinct (basally connate in T. zebrina), subequal;</text>
      <biological_entity id="o15034" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals distinct (rarely connate basally), white to pink, blue, or violet, equal, rarely clawed;</text>
      <biological_entity id="o15035" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink blue or violet" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink blue or violet" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink blue or violet" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, all fertile, equal;</text>
      <biological_entity id="o15036" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded or glabrous;</text>
      <biological_entity id="o15037" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular, ovules (1–) 2 per locule, 1-seriate.</text>
      <biological_entity id="o15038" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
      <biological_entity id="o15039" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="2" to_inclusive="false" />
        <character constraint="per locule" constraintid="o15040" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s13" value="1-seriate" value_original="1-seriate" />
      </biological_entity>
      <biological_entity id="o15040" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules 3-valved, 3-locular.</text>
      <biological_entity id="o15041" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2 per locule (1 in T. spathacea);</text>
      <biological_entity id="o15042" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character constraint="per locule" constraintid="o15043" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15043" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>hilum oblong to linear;</text>
      <biological_entity id="o15044" name="hilum" name_original="hilum" src="d0_s16" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s16" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>embryotega abaxial to lateral.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 6–8, probably others.</text>
      <biological_entity id="o15045" name="embryotegum" name_original="embryotega" src="d0_s17" type="structure">
        <character char_type="range_value" from="abaxial" name="position" src="d0_s17" to="lateral" />
      </biological_entity>
      <biological_entity constraint="x" id="o15046" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s18" to="8" />
      </biological_entity>
      <biological_entity id="o15047" name="other" name_original="others" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Neotemperate and neotropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Neotemperate and neotropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Spiderwort</other_name>
  <other_name type="common_name">wandering-Jew</other_name>
  <other_name type="common_name">spider-lily</other_name>
  <other_name type="common_name">éphémères</other_name>
  <discussion>The species described by E. Anderson and R. E. Woodson Jr. (1935) are narrowly defined and typological. Nevertheless, they are recognizable entities even if some of them may prove eventually unworthy of specific rank. Where specific problems have been recognized, they are noted in the discussions at the end of the species.</discussion>
  <discussion>Tradescantia species hybridize freely when growing together (E. Anderson and R. E. Woodson Jr. 1935). My observations in the field and garden tend to confirm this. The definite or probable hybrids are listed after the species. The list is almost certainly incomplete. The questionable records are based on uncertain determinations. The record of a possible hybrid between T. ohiensis and Callisia rosea (as Cuthbertia rosea), cited by Anderson and Woodson, is omitted: the specimens appear to be merely gracile plants of T. ohiensis. Some native species are occasionally cultivated, although most garden plants seem to be hybrids of T. virginiana and other species (E. Anderson 1952). They are usually sold as Tradescantia × andersoniana (an invalid name) followed by a cultivar epithet.</discussion>
  <discussion>Species ca. 70 (30 in the flora).</discussion>
  <references>
    <reference>Anderson, E. 1954. A field survey of chromosome numbers in the species of Tradescantia closely related to Tradescantia virginiana. Ann. Missouri Bot. Gard. 41: 305–327.</reference>
    <reference>Anderson, E. and K. Sax. 1936. A cytological monograph of the American species of Tradescantia. Bot. Gaz. 97: 433–476.</reference>
    <reference>Anderson, E. and R. E. Woodson Jr. 1935. The species of Tradescantia indigenous to the United States. Contr. Arnold Arbor. 9: 1–132.</reference>
    <reference>Hunt, D. R. 1980. Sections and series in Tradescantia. American Commelinaceae: IX. Kew Bull. 35: 437–442.</reference>
    <reference>MacRoberts, D. T. 1980. Notes on Tradescantia (Commelinaceae) V. Tradescantia of Louisiana. Bull. Mus. Life Sci. Louisiana State Univ. 4: 1–15.</reference>
    <reference>Sinclair, C. 1967. Studies on the Erect Tradescantias. Ph.D. dissertation. University of Missouri.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers subsessile; petals clawed, claws connate at least basally; stamens epipetalous.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers distinctly pedicellate; petals neither clawed nor connate; stamens free.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 2-ranked, bases oblique, cuneate; blade usually variegated; sepals connate basally</description>
      <determination>30 Tradescantia zebrina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves spirally arranged, bases symmetric, rounded to broadly cuneate; blade not variegated; sepals distinct.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves narrowly lanceolate, apex acuminate; stamen filaments glabrous</description>
      <determination>29 Tradescantia leiandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves lanceolate-elliptic to oblong-elliptic or ovate, apex obtuse to abruptly acute-apiculate; stamen filaments glabrous or bearded.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves oblong-elliptic to lanceolate-elliptic, mostly 7–15 cm; peduncle (3.5–)4–13 cm; leaves usually purplish violet</description>
      <determination>28 Tradescantia pallida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves oblong-elliptic to ovate, mostly 3–7 cm; peduncle 1–5(–6) cm; leaves green.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Filaments and ovary glabrous; flowering June to October</description>
      <determination>26 Tradescantia brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Filaments bearded, ovary densely bearded; flowering February to May</description>
      <determination>27 Tradescantia buckleyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sprawling to decumbent plants rooting at nodes; leaves lanceolate to lanceolate-elliptic or lanceolate-oblong to ovate-lanceolate or ovate-elliptic.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Erect or ascending plants, rarely rooting at nodes; leaves mostly linear-lanceolate to lanceolate-oblong.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves lanceolate-elliptic to ovate-lanceolate, to 5  2 cm; cyme pairs usually 1–2 per shoot; bracts all or mostly foliaceous, occasionally reduced</description>
      <determination>24 Tradescantia fluminensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves lanceolate-oblong to ovate-elliptic, to 10  3.5 cm; cyme pairs 2–4 per shoot; bracts, especially those of axillary inflorescences, usually reduced</description>
      <determination>25 Tradescantia crassula</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences all or chiefly axillary.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences terminal, commonly terminal and axillary.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Inflorescences pedunculate in axils well proximal to shoot apex, enclosed in boat-shaped spathes; leaves glabrous; flowers white</description>
      <determination>23 Tradescantia spathacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Inflorescences mostly sessile in axils of distal leaves; boat-shaped spathes absent; leaves usually arachnoid-villous; flowers blue to purple</description>
      <determination>22 Tradescantia crassifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Distal leaf blades wider than opened, flattened sheaths.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Distal leaf blades equal to or narrower than opened, flattened sheaths.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pedicels 1–1.7 cm; proximal leaves petiolate; stems frequently flexuous; plants flowering mainly May–Sep</description>
      <determination>1 Tradescantia subaspera</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pedicels (1.5–)2–3.2 cm; proximal leaves narrowed directly into sheath; stems not flexuous; plants flowering mainly Feb–May.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Sepals 9–16 mm, ± inflated, eglandular-pilose; flowers usually deep blue, purple, or rose-red</description>
      <determination>4 Tradescantia ernestiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Sepals 6–12 mm, not inflated, glandular-pilose or mixed glandular- and eglandular-pubescent; flowers usually white or pale pink to pale lavender.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves not glaucous; capsules 8–10 mm</description>
      <determination>2 Tradescantia edwardsiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves ± glaucous; capsules 6–8 mm</description>
      <determination>3 Tradescantia ozarkana</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Sepals glabrous or with eglandular hairs only (very rarely a few minute glandular hairs at base).</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Sepals pubescent with glandular and often eglandular hairs.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Sepals glabrous (or with apical tuft of eglandular hairs or a few minute glandular hairs at base).</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Sepals covered with eglandular hairs.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Stems 5–18 cm; pedicels, sepal bases often with minute glandular hairs; petals 10 mm</description>
      <determination>20 Tradescantia wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Stems 15–115 cm; pedicels glabrous; sepals glabrous or with apical tuft of eglandular hairs; petals usually 0.8–20 mm.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Plants distinctly glaucous; leaves 5–45 cm, arcuate, forming acute angle with stem (also see Tradescantia occidentalis var. scopulorum)</description>
      <determination>5 Tradescantia ohiensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Plants not at all to slightly glaucous; leaves 4–11 cm, straight, forming nearly right angle with stem</description>
      <determination>6 Tradescantia paludosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Bracts saccate at base, blades reduced, densely, minutely velvety.</description>
      <determination>7 Tradescantia gigantea</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Bracts not saccate at base, blades well developed, sparsely to densely pilose.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Flowering stems 2–7 cm (elongating to 20 cm in fruit), pilose to villous; sepals purple or rose-colored (rarely pale green), not inflated; rocky prairies</description>
      <determination>8 Tradescantia tharpii</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Flowering stems 5–50 cm, glabrous to pilose or hirsute; sepals various; habitat various but rarely rocky prairies.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Roots (1.5–)2–4 mm thick; stems commonly glabrous proximal to inflorescence; sepals usually ± inflated; ne and Appalachian</description>
      <determination>9 Tradescantia virginiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Roots 1–1.5(–2) mm thick; stems usually pilose to hirsute throughout; sepals not inflated; se</description>
      <determination>10 Tradescantia hirsutiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Pedicels 0.8–1 cm, glandular-puberulent; sepals 4–6 mm; petals 9–12 mm; hilum much shorter than seed</description>
      <determination>21 Tradescantia pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Pedicels (0.8–)1–6 cm, glandular- or eglandular-pubescent; sepals (4–)6–16 mm; petals (6–)10–19 mm; hilum as long as seed.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Sepals with mostly glandular pubescence.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Sepals with mixture of glandular, eglandular pubescence.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Internodes and leaves glabrous</description>
      <determination>11 Tradescantia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Internodes pubescent (rarely glabrous in Tradescantia roseolens)</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Pedicels 1–2.8 cm; roots all thin and fibrous; South Carolina to Florida and Alabama</description>
      <determination>12 Tradescantia roseolens</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Pedicels 2.5–4.5 cm; at least some roots thick and tuberous; Texas</description>
      <determination>13 Tradescantia pedicellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Stems, leaves completely glabrous; plants glaucous; sepal hairs mainly glandular (also see Tradescantia roseolens).</description>
      <determination>11 Tradescantia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Stems, leaves usually sparsely to densely pubescent, if glabrous then sepal hairs mainly eglandular; plants usually not glaucous (somewhat glaucous in T. roseolens); sepal hairs various.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems densely arachnoid-pubescent; roots thick, brownish-tomentose.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems variously pubescent but not arachnoid-pubescent; roots various but not brownish-tomentose.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Stems erect or ascending, unbranched or sparsely branched, 30–105 cm</description>
      <determination>14 Tradescantia reverchonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Stems spreading, diffusely branched, 10–30 cm.</description>
      <determination>15 Tradescantia subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Plants diffuse, spreading; stems much branched.</description>
      <determination>16 Tradescantia humilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Plants erect or ascending; stems unbranched or sparsely branched.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Sepal hairs mainly eglandular, glandular hairs few, inconspicuous</description>
      <determination>10 Tradescantia hirsutiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Sepal hairs mainly glandular or eglandular, glandular hairs numerous, conspicuous.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Sepals puberulent, hairs all less than 1 mm; roots relatively thin, 0.5–1(–2) mm thick</description>
      <determination>12 Tradescantia roseolens</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Sepals pilose-puberulent, longer hairs 1.5–6 mm; roots relatively stout, 1–3 mm thick.</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Plants bright green; stems, leaves usually glabrous</description>
      <determination>17 Tradescantia bracteata</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Plants dull green; stems, leaves usually pubescent (rarely glabrescent).</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Stems (2–)15–40 cm; pedicels 1.5–3.5 cm; leaves, bracts puberulent, usually sparsely to densely pilose, margins ± densely ciliolate</description>
      <determination>18 Tradescantia hirsuticaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Stems 2–10 cm; pedicels (2–)4–6 cm; leaves, bracts pilose but not puberulent, margins sparsely ciliate</description>
      <determination>19 Tradescantia longipes</determination>
    </key_statement>
  </key>
</bio:treatment>