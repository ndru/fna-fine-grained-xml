<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="Greene" date="1893" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>1: 247. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species pinetorum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000430</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to ascending, rarely rooting at nodes.</text>
      <biological_entity id="o17055" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
        <character constraint="at nodes" constraintid="o17056" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o17056" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots sometimes tuberous.</text>
      <biological_entity id="o17057" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems sparsely branched, 8–39 cm, scabridulous or rarely glabrescent.</text>
      <biological_entity id="o17058" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="39" to_unit="cm" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade linear-lanceolate, 1–10 × 0.15–0.8 cm (distal leaf-blades wider or narrower than sheaths when sheaths opened, flattened), firmly membranaceous, glaucous, glabrous.</text>
      <biological_entity id="o17059" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17060" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="firmly" name="texture" src="d0_s3" value="membranaceous" value_original="membranaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, solitary, or frequently with 1–3 axillary inflorescences from distal nodes;</text>
      <biological_entity id="o17061" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o17062" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17063" name="node" name_original="nodes" src="d0_s4" type="structure" />
      <relation from="o17061" id="r2194" modifier="frequently" name="with" negation="false" src="d0_s4" to="o17062" />
      <relation from="o17062" id="r2195" name="from" negation="false" src="d0_s4" to="o17063" />
    </statement>
    <statement id="d0_s5">
      <text>bracts foliaceous.</text>
      <biological_entity id="o17064" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o17065" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s6" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels 0.8–1 cm, glandular-puberulent;</text>
      <biological_entity id="o17066" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals frequently suffused with red, glaucous, 4–6 mm, glandular-puberulent;</text>
      <biological_entity id="o17067" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="suffused with red" value_original="suffused with red" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals distinct, bright blue to rose and purple, not clawed, 9–12 mm;</text>
      <biological_entity id="o17068" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="bright blue" name="coloration" src="d0_s9" to="rose and purple" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens free;</text>
      <biological_entity id="o17069" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments bearded.</text>
      <biological_entity id="o17070" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 3–4 mm.</text>
      <biological_entity id="o17071" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.5–2 mm;</text>
      <biological_entity id="o17072" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hilum much shorter than seed.</text>
      <biological_entity id="o17073" name="hilum" name_original="hilum" src="d0_s14" type="structure">
        <character constraint="than seed" constraintid="o17074" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o17074" name="seed" name_original="seed" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist canyons and stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist canyons" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21</number>
  
</bio:treatment>