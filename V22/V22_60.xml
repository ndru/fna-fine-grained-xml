<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">zingiberaceae</taxon_name>
    <taxon_name authority="J. Konig in A. J. Retzius" date="1783" rank="genus">Hedychium</taxon_name>
    <place_of_publication>
      <publication_title>in A. J. Retzius,Observ. Bot.</publication_title>
      <place_in_publication>3: 73–74. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zingiberaceae;genus Hedychium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek edys, sweet, and chion, snow, for the fragrant white flowers</other_info_on_name>
    <other_info_on_name type="fna_id">114823</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Pseudostems well developed, 1–3 m.</text>
      <biological_entity id="o2033" name="pseudostem" name_original="pseudostems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s0" value="developed" value_original="developed" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences projecting from tip of pseudostem, dense, conelike [lax];</text>
      <biological_entity id="o2034" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character constraint="from tip" constraintid="o2035" is_modifier="false" name="orientation" src="d0_s1" value="projecting" value_original="projecting" />
        <character is_modifier="false" name="density" notes="" src="d0_s1" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s1" value="conelike" value_original="conelike" />
      </biological_entity>
      <biological_entity id="o2035" name="tip" name_original="tip" src="d0_s1" type="structure" />
      <biological_entity id="o2036" name="pseudostem" name_original="pseudostem" src="d0_s1" type="structure" />
      <relation from="o2035" id="r260" name="part_of" negation="false" src="d0_s1" to="o2036" />
    </statement>
    <statement id="d0_s2">
      <text>bracts of main axis crowded [not crowded], [2.5–] 4–6 [–7] cm, ovate to lanceolate [or almost circular];</text>
      <biological_entity id="o2037" name="bract" name_original="bracts" src="d0_s2" type="structure" constraint="axis" constraint_original="axis; axis">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="main" id="o2038" name="axis" name_original="axis" src="d0_s2" type="structure" />
      <relation from="o2037" id="r261" name="part_of" negation="false" src="d0_s2" to="o2038" />
    </statement>
    <statement id="d0_s3">
      <text>cincinni sessile, [1–] 2–3 [–6] -flowered, enclosed in bracts;</text>
      <biological_entity id="o2039" name="cincinnus" name_original="cincinni" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="[1-]2-3[-6]-flowered" value_original="[1-]2-3[-6]-flowered" />
      </biological_entity>
      <biological_entity id="o2040" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <relation from="o2039" id="r262" name="enclosed in" negation="false" src="d0_s3" to="o2040" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles small, inconspicuous, hidden by bracts.</text>
      <biological_entity id="o2041" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="by bracts" constraintid="o2042" is_modifier="false" name="prominence" src="d0_s4" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o2042" name="bract" name_original="bracts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx cylindric, 3-toothed or lobed, split down one side [not split];</text>
      <biological_entity id="o2043" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2044" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o2045" name="split" name_original="split" src="d0_s5" type="structure" />
      <biological_entity id="o2046" name="side" name_original="side" src="d0_s5" type="structure" />
      <relation from="o2045" id="r263" name="down" negation="false" src="d0_s5" to="o2046" />
    </statement>
    <statement id="d0_s6">
      <text>corolla-tube slender, lobes linear;</text>
      <biological_entity id="o2047" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2048" name="corolla-tube" name_original="corolla-tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o2049" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filament linear, tubular-incurved, enclosing style;</text>
      <biological_entity id="o2050" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2051" name="filament" name_original="filament" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="tubular-incurved" value_original="tubular-incurved" />
      </biological_entity>
      <biological_entity id="o2052" name="style" name_original="style" src="d0_s7" type="structure" />
      <relation from="o2051" id="r264" name="enclosing" negation="false" src="d0_s7" to="o2052" />
    </statement>
    <statement id="d0_s8">
      <text>anther long-exserted, not spurred, terminal appendage none;</text>
      <biological_entity id="o2053" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2054" name="anther" name_original="anther" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="long-exserted" value_original="long-exserted" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s8" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2055" name="appendage" name_original="appendage" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral staminodes large, petallike, lip oblong, plane, 2-lobed.</text>
      <biological_entity id="o2056" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o2057" name="staminode" name_original="staminodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s9" value="petal-like" value_original="petallike" />
      </biological_entity>
      <biological_entity id="o2058" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s9" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits capsule, globose.</text>
    </statement>
    <statement id="d0_s11">
      <text>x = 17.</text>
      <biological_entity constraint="fruits" id="o2059" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity constraint="x" id="o2060" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; North America, Mexico, West Indies, Central America, South America, Australia; native, Asia and Madagascar.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Asia and Madagascar" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Species ca. 50 (1 in the flora).</discussion>
  
</bio:treatment>