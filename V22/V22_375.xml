<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert R. Haynes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">Hydrocharitaceae</taxon_name>
    <taxon_hierarchy>family Hydrocharitaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10426</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, caulescent or without evident stem, glabrous or pubescent, entirely submersed, with both submersed and floating leaves, or with submersed stolons and emergent leaves, in fresh, brackish, or marine waters;</text>
      <biological_entity id="o11901" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="without evident stem" value_original="without evident stem" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="entirely" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="in fresh" name="habitat" src="d0_s0" value="brackish" value_original="brackish" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="marine" value_original="marine" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="brackish" value_original="brackish" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="marine" value_original="marine" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="water" value_original="water" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o11902" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o11903" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="submersed" value_original="submersed" />
      </biological_entity>
      <biological_entity id="o11904" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="emergent" value_original="emergent" />
      </biological_entity>
      <relation from="o11901" id="r1566" name="without" negation="false" src="d0_s0" to="o11902" />
      <relation from="o11901" id="r1567" name="with both submersed and floating leaves , or with" negation="false" src="d0_s0" to="o11903" />
      <relation from="o11901" id="r1568" name="with both submersed and floating leaves , or with" negation="false" src="d0_s0" to="o11904" />
    </statement>
    <statement id="d0_s1">
      <text>turions rarely present.</text>
      <biological_entity id="o11905" name="turion" name_original="turions" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems rhizomatous, creeping, with abbreviated erect axis at nodes, or erect, leafy, elongate.</text>
      <biological_entity id="o11906" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o11907" name="axis" name_original="axis" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="abbreviated" value_original="abbreviated" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o11908" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o11906" id="r1569" name="with" negation="false" src="d0_s2" to="o11907" />
      <relation from="o11907" id="r1570" name="at" negation="false" src="d0_s2" to="o11908" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, alternate, opposite, or whorled, sessile or petiolate;</text>
      <biological_entity id="o11909" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules sometimes present, forming tubular sheath around stem;</text>
      <biological_entity id="o11910" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11911" name="sheath" name_original="sheath" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o11912" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <relation from="o11910" id="r1571" name="forming" negation="false" src="d0_s4" to="o11911" />
      <relation from="o11910" id="r1572" name="around" negation="false" src="d0_s4" to="o11912" />
    </statement>
    <statement id="d0_s5">
      <text>blade margins entire or serrate;</text>
      <biological_entity constraint="blade" id="o11913" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>veins 1–many.</text>
      <biological_entity id="o11914" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s6" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, terminal, or scapose, 1-flowered or cymose, subtended by spathe;</text>
      <biological_entity id="o11915" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
      </biological_entity>
      <biological_entity id="o11916" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <relation from="o11915" id="r1573" name="subtended by" negation="false" src="d0_s7" to="o11916" />
    </statement>
    <statement id="d0_s8">
      <text>spathe a 2-fid bract or pair of opposite bracts.</text>
      <biological_entity id="o11917" name="spathe" name_original="spathe" src="d0_s8" type="structure" constraint="bract" constraint_original="bract; bract" />
      <biological_entity id="o11918" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o11919" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o11917" id="r1574" name="part_of" negation="false" src="d0_s8" to="o11919" />
      <relation from="o11918" id="r1575" name="part_of" negation="false" src="d0_s8" to="o11919" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers unisexual, staminate and pistillate on same plants or on different plants, often with rudiments of opposite type, or bisexual, actinomorphic, rarely slightly zygomorphic;</text>
      <biological_entity id="o11920" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character constraint="on same plants or on plants; on same plants or on different plants , often with rudiments of opposite type , or bisexual , actinomorphic , rarely slightly zygomorphic" constraintid="o11923, o11924" is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11921" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11922" name="rudiment" name_original="rudiments" src="d0_s9" type="structure" />
      <biological_entity id="o11923" name="plant" name_original="plants" src="d0_s9" type="structure" />
      <biological_entity id="o11924" name="rudiment" name_original="rudiments" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="true" modifier="rarely slightly" name="architecture" src="d0_s9" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
      <relation from="o11921" id="r1576" name="with" negation="false" src="d0_s9" to="o11922" />
    </statement>
    <statement id="d0_s10">
      <text>perianth epigynous, free, mostly 6-parted, then differentiated into sepals and petals, rarely 3-parted, then petals absent in Thalassia and Halophila;</text>
      <biological_entity id="o11925" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="epigynous" value_original="epigynous" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s10" value="6-parted" value_original="6-parted" />
        <character constraint="into petals" constraintid="o11927" is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s10" value="3-parted" value_original="3-parted" />
      </biological_entity>
      <biological_entity id="o11926" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <biological_entity id="o11927" name="petal" name_original="petals" src="d0_s10" type="structure" />
      <biological_entity id="o11928" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in thalassia and halophila" is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens (0–) 2–many in 1 or more whorls (inner often staminodial), epigynous, distinct or connate;</text>
      <biological_entity id="o11929" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character constraint="in epigynous" constraintid="o11930" is_modifier="false" name="quantity" src="d0_s11" value="(0-)2-many" value_original="(0-)2-many" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o11930" name="epigynou" name_original="epigynous" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen spheric, in monads or tetrads or in slender chains;</text>
      <biological_entity id="o11931" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity constraint="slender" id="o11932" name="chain" name_original="chains" src="d0_s12" type="structure" />
      <relation from="o11931" id="r1577" name="in monads or tetrads or in" negation="false" src="d0_s12" to="o11932" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 0–1, if present, inferior, 2–6 [–16] -carpellate, 1-locular or falsely 6–9-locular;</text>
    </statement>
    <statement id="d0_s14">
      <text>placentation parietal.</text>
      <biological_entity id="o11933" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="1" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s13" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-6[-16]-carpellate" value_original="2-6[-16]-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="falsely" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="6-9-locular" value_original="6-9-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits berrylike.</text>
      <biological_entity id="o11934" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="berrylike" value_original="berrylike" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds many, fusiform, ellipsoid, ovoid, or spheric;</text>
      <biological_entity id="o11935" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spheric" value_original="spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>seed-coat glabrous, papillose, or echinate.</text>
      <biological_entity id="o11936" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="echinate" value_original="echinate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>191</number>
  <other_name type="common_name">Tape-grass or Frog-bit Family</other_name>
  <discussion>Hydrocharitaceae, like other members of the Alismatidae, have one or more (fewer than 20) scales (intravaginal squamules) in the axils of their leaves. These scales (or hairs in some taxa) secrete mucilage and are without any venation. The structures are often referred to as "squamulae intravaginales" or "intravaginal scales" in the literature.</discussion>
  <discussion>Genera 17, species ca. 76 (10 genera, 14 species in the flora).</discussion>
  <references>
    <reference>Ancibor, E. 1979. Systematic anatomy of vegetative organs of the Hydrocharitaceae. Bot. J. Linn. Soc. 78: 237–266.</reference>
    <reference>Catling, P. M. and W. G. Dore. 1982. Status and identification of Hydrocharis morsus-ranae and Limnobium spongia (Hydrocharitaceae) in northeastern North America. Rhodora 84: 523–545.</reference>
    <reference>Cook, C. D. K. 1982. Pollinating mechanisms in the Hydrocharitaceae. In: J.-J. Symoens et al., eds. 1982. Studies on Aquatic Vascular Plants. Brussels. Pp. 1–15.</reference>
    <reference>Godfrey, R. K. and J. W. Wooten. 1979. Aquatic and Wetland Plants of Southeastern United States:. Monocotyledons. Athens, Ga.</reference>
    <reference>Hartog, C. den. 1970. The Sea-grasses of the World. Amsterdam.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants of marine waters; pollen in moniliform chains.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants of fresh or slightly brackish waters; pollen in monads or tetrads.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf-bearing branches arising from rhizomes at distances of several internodes; styles 6–8; fruits echinate, dehiscing into 6–8 irregular valves.es</description>
      <determination>9 Thalassia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf-bearing branches arising from rhizome at each node; styles 3–5; fruits smooth or ridged, not echinate, dehiscing by pericarp decay</description>
      <determination>10 Halophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems elongate (more than 3 cm), erect; leaves cauline, whorled.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems short (less than 2 cm) or, if elongate, then stoloniferous; leaves basal.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves with prickles along abaxial surface of midvein; intravaginal squamules fringed with orange-brown hairs.</description>
      <determination>8 Hydrilla</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves without prickles along abaxial surface of midvein; intravaginal scales squamules entire or, if fringed, marginal hairs clear, not orange-brown.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Whorls with 5 or more leaves per node.</description>
      <determination>6 Egeria</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Whorls with 2–4(–7) leaves per node or leaves opposite at proximalmost nodes.</description>
      <determination>7 Elodea</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems floating on or suspended in water; some leaves with aerenchyma on abaxial surface; peduncles mostly short (less than 5 cm).</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems rooted in substrate; leaves without aerenchyma on abaxial surface; peduncles mostly elongate (more than 5 cm).</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Anthers oval; filaments distinct to base or nearly so; seeds minutely tuberculate or muricate; styles 6, 2-fid less than ½ length.</description>
      <determination>1 Hydrocharis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Anthers elongate; filaments connate at least ½ length; seeds echinate; styles 3–9, 2-fid nearly to base.</description>
      <determination>2 Limnobium</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves petiolate; spathe winged or ribbed.</description>
      <determination>3 Ottelia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves sessile; spathe not winged or ribbed.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Seeds glabrous; leaves with rows of lacunae on each side of midvein, giving blade 3-zoned appearance of middle, light-colored zone bordered on each side by darker zone; flowers unisexual.</description>
      <determination>4 Vallisneria</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Seeds echinate; leaves with continuous intercellular spaces, blade of uniform color from margin to margin; flowers bisexual.</description>
      <determination>5 Blyxa</determination>
    </key_statement>
  </key>
</bio:treatment>