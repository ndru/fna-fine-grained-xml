<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Luzula</taxon_name>
    <taxon_name authority="(Ehrhart) Lejeune" date="1811" rank="species">multiflora</taxon_name>
    <taxon_name authority="(Buchenau) V. I. Krecztowicz" date="1928" rank="subspecies">frigida</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>12: 490. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus luzula;species multiflora;subspecies frigida;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000234</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="(Buchenau) Samuelsson" date="unknown" rank="species">campestris</taxon_name>
    <taxon_name authority="Buchenau" date="unknown" rank="variety">frigida</taxon_name>
    <place_of_publication>
      <publication_title>in Oesterr. Bot. Z.</publication_title>
      <place_in_publication>48: 284. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Luzula;species campestris;variety frigida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="(Erhard) Lejeune" date="unknown" rank="species">frigida</taxon_name>
    <taxon_hierarchy>genus Luzula;species frigida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="(Willdenow) de Candolle" date="unknown" rank="species">multiflora</taxon_name>
    <taxon_name authority="Celakovsky" date="unknown" rank="variety">fusconigra</taxon_name>
    <taxon_hierarchy>genus Luzula;species multiflora;variety fusconigra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sudetica</taxon_name>
    <taxon_name authority="(Buchenau) Fernald" date="unknown" rank="variety">frigida</taxon_name>
    <taxon_hierarchy>genus Luzula;species sudetica;variety frigida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Cauline leaves 2–3, not overlapping;</text>
      <biological_entity constraint="cauline" id="o6892" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="3" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s0" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal inflorescence bract not conspicuously exceeding inflorescence.</text>
      <biological_entity constraint="inflorescence" id="o6893" name="bract" name_original="bract" src="d0_s1" type="structure" constraint_original="proximal inflorescence" />
      <biological_entity id="o6894" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <relation from="o6893" id="r913" modifier="not conspicuously" name="exceeding" negation="false" src="d0_s1" to="o6894" />
    </statement>
    <statement id="d0_s2">
      <text>Tepals: outer and inner whorl dark-brown to chestnut to blackish, outer whorl pointed, inner whorl truncate-mucronate.</text>
      <biological_entity id="o6895" name="tepal" name_original="tepals" src="d0_s2" type="structure" />
      <biological_entity constraint="outer and inner" id="o6896" name="whorl" name_original="whorl" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s2" to="chestnut" />
      </biological_entity>
      <biological_entity constraint="outer" id="o6897" name="whorl" name_original="whorl" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6898" name="whorl" name_original="whorl" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate-mucronate" value_original="truncate-mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsules dark chestnut to blackish, equal to slightly exceeding tepals.</text>
      <biological_entity id="o6899" name="capsule" name_original="capsules" src="d0_s3" type="structure">
        <character char_type="range_value" from="dark chestnut" name="coloration" src="d0_s3" to="blackish" />
        <character is_modifier="false" name="variability" src="d0_s3" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o6900" name="tepal" name_original="tepals" src="d0_s3" type="structure" />
      <relation from="o6899" id="r914" modifier="slightly" name="exceeding" negation="false" src="d0_s3" to="o6900" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds 1.1–1.4 mm;</text>
      <biological_entity id="o6901" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>caruncle 0.2–0.3 mm. 2n = 36.</text>
      <biological_entity id="o6902" name="caruncle" name_original="caruncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6903" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Peaty barrens, slopes, fields, heaths, grasslands, and snowbeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="peaty barrens" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="heaths" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="snowbeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Man., Nfld. and Labr. (Labr.), N.W.T., N.S., Nunavut, Ont., Que., Yukon; Alaska, Maine, Mass., N.H., Vt.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17c</number>
  
</bio:treatment>