<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William D. Reese†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">METEORIACEAE</taxon_name>
    <taxon_hierarchy>family METEORIACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10568</other_info_on_name>
  </taxon_identification>
  <number>66.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized [to large], occasionally small, usually in pendent tufts, green, yellowish, brownish, or black, dull [or glossy].</text>
      <biological_entity id="o24264" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24265" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="pendent" value_original="pendent" />
      </biological_entity>
      <relation from="o24264" id="r3444" modifier="usually" name="in" negation="false" src="d0_s0" to="o24265" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, subpinnate to irregularly branched, branches erect, pendent, [or spreading], short-to-elongate, simple or branched;</text>
      <biological_entity id="o24266" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="subpinnate" name="architecture" src="d0_s1" to="irregularly branched" />
      </biological_entity>
      <biological_entity id="o24267" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="short" name="size" src="d0_s1" to="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent;</text>
      <biological_entity id="o24268" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia foliose.</text>
      <biological_entity id="o24269" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves similar, spirally inserted, imbricate, spreading, spreading-complanate, or squarrose;</text>
      <biological_entity id="o24270" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="spirally" name="position" src="d0_s4" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spreading-complanate" value_original="spreading-complanate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="squarrose" value_original="squarrose" />
      </biological_entity>
      <biological_entity constraint="branch" id="o24271" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="spirally" name="position" src="d0_s4" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spreading-complanate" value_original="spreading-complanate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="squarrose" value_original="squarrose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins usually serrate, sometimes papillose-denticulate, [entire, or serrulate];</text>
      <biological_entity id="o24272" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="papillose-denticulate" value_original="papillose-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute to acuminate or piliferous, usually broadly to narrowly acuminate;</text>
      <biological_entity id="o24273" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate or piliferous" />
        <character is_modifier="false" modifier="usually broadly; broadly to narrowly" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa single or rarely ecostate;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells sometimes differentiated;</text>
      <biological_entity id="o24274" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="single" value_original="single" />
        <character name="quantity" src="d0_s7" value="rarely" value_original="rarely" />
      </biological_entity>
      <biological_entity id="o24275" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells elongate, usually papillose.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous or sometimes autoicous;</text>
      <biological_entity constraint="medial laminal" id="o24276" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perigonia and perichaetia axillary, gemmiform.</text>
      <biological_entity id="o24277" name="perigonium" name_original="perigonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
      <biological_entity id="o24278" name="perichaetium" name_original="perichaetia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta single, dark, short.</text>
      <biological_entity id="o24279" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark" value_original="dark" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule usually erect, usually exserted, oblong-ovoid to ovoid, symmetric;</text>
      <biological_entity id="o24280" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="oblong-ovoid" name="shape" src="d0_s13" to="ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata proximal on capsule;</text>
      <biological_entity id="o24281" name="stoma" name_original="stomata" src="d0_s14" type="structure">
        <character constraint="on capsule" constraintid="o24282" is_modifier="false" name="position" src="d0_s14" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o24282" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>annulus usually indistinct;</text>
      <biological_entity id="o24283" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s15" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum rostrate or short-rostrate;</text>
      <biological_entity id="o24284" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="rostrate" value_original="rostrate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>peristome double;</text>
      <biological_entity id="o24285" name="peristome" name_original="peristome" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>exostome teeth 16;</text>
      <biological_entity constraint="exostome" id="o24286" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>endostome basal membrane present, segments 16, cilia absent or inconspicuous.</text>
      <biological_entity id="o24287" name="endostome" name_original="endostome" src="d0_s19" type="structure" />
      <biological_entity constraint="basal" id="o24288" name="membrane" name_original="membrane" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24289" name="segment" name_original="segments" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o24290" name="cilium" name_original="cilia" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s19" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Calyptra cucullate or conic-mitrate, naked or hairy.</text>
      <biological_entity id="o24291" name="calyptra" name_original="calyptra" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s20" value="conic-mitrate" value_original="conic-mitrate" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="naked" value_original="naked" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Spores spheric, smooth or rough.</text>
      <biological_entity id="o24292" name="spore" name_original="spores" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s21" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s21" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; primarily tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="primarily tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera ca. 30, species ca. 260 (2 genera, 2 species in the flora).</discussion>
  <discussion>Meteoriaceae is a large and diverse family characteristically found on twigs and bark in warm, humid regions of the world. Many of the taxa grow as tangled festoons of delicate branches dangling from the substrate. The twig habitat, creeping stems with pendent branches, elongate, mostly papillose laminal cells, and pointed leaves, often with 1-seriate filiform apices, are characteristic traits of Meteoriaceae. The taxonomy and phylogenetic relationships of this complex group of mosses are difficult to interpret. See W. R. Buck (1994) for proposals on generic concepts in Meteoriaceae.</discussion>
  <references>
    <reference>Buck, W. R. 1994. A new attempt at understanding the Meteoriaceae. J. Hattori Bot. Lab. 75: 51–72.</reference>
    <reference>Noguchi, A. 1976. A taxonomic revision of the family Meteoriaceae of Asia. J. Hattori Bot. Lab. 41: 231–357.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and branches densely foliate; medial laminal cells in rows diverging from costa.</description>
      <determination>1 Papillaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and branches loosely foliate; medial laminal cells not in rows diverging from costa.</description>
      <determination>2 Barbella</determination>
    </key_statement>
  </key>
</bio:treatment>