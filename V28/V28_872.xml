<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">558</other_info_on_meta>
    <other_info_on_meta type="mention_page">559</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">platygyrium</taxon_name>
    <taxon_name authority="Cardot" date="1910" rank="species">fuscoluteum</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>37: 49. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus platygyrium;species fuscoluteum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099250</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Regmatodon</taxon_name>
    <taxon_name authority="Bescherelle" date="unknown" rank="species">fuscoluteus</taxon_name>
    <taxon_hierarchy>genus regmatodon;species fuscoluteus</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants relatively large, yellowish-brown.</text>
      <biological_entity id="o4597" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish-brown" value_original="yellowish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with branches elongate, creeping, loosely foliate, never julaceous, straight.</text>
      <biological_entity id="o4598" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
        <character is_modifier="false" modifier="never" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o4599" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o4598" id="r616" name="with" negation="false" src="d0_s1" to="o4599" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves homomallous, somewhat plicate, 1.3–1.6 mm;</text>
      <biological_entity id="o4600" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s2" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins conspicuously recurved proximally;</text>
      <biological_entity id="o4601" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="conspicuously; proximally" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex abruptly slenderly acuminate;</text>
      <biological_entity id="o4602" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly slenderly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells 50–65 × 5–6 µm. Specialized asexual reproduction rare, by brood branchlets, inconspicuous on branch apices.</text>
      <biological_entity constraint="medial laminal" id="o4603" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" from_unit="um" name="length" src="d0_s5" to="65" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s5" to="6" to_unit="um" />
        <character is_modifier="false" name="development" src="d0_s5" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="rare" value_original="rare" />
        <character constraint="on branch apices" constraintid="o4606" is_modifier="false" name="prominence" notes="" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o4604" name="brood" name_original="brood" src="d0_s5" type="structure" />
      <biological_entity id="o4605" name="branchlet" name_original="branchlets" src="d0_s5" type="structure" />
      <biological_entity constraint="branch" id="o4606" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <relation from="o4603" id="r617" name="by" negation="false" src="d0_s5" to="o4604" />
      <relation from="o4603" id="r618" name="by" negation="false" src="d0_s5" to="o4605" />
    </statement>
    <statement id="d0_s6">
      <text>Seta 1–1.3 cm.</text>
      <biological_entity id="o4607" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule cylindric, nearly symmetric to distinctly asymmetric, 2–2.5 mm;</text>
      <biological_entity id="o4608" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="nearly symmetric" name="architecture_or_shape" src="d0_s7" to="distinctly asymmetric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>operculum slenderly long-rostrate.</text>
      <biological_entity id="o4609" name="operculum" name_original="operculum" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s8" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyptra 2–2.5 mm.</text>
      <biological_entity id="o4610" name="calyptra" name_original="calyptra" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 18 µm.</text>
      <biological_entity id="o4611" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="um" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Feb.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Feb" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane forests, old logs, stumps, tree trunks, damp boulders, rock faces, shaded banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane forests" />
        <character name="habitat" value="stumps" modifier="old" />
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="shaded banks" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1300-2800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1300" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Platygyrium fuscoluteum is distinguished by its glossy yellowish brown creeping plants bearing usually homomallous leaves with long slender apices on elongate branches. Microscopically, the often somewhat plicate leaves with recurved margins and quadrate alar cells make the species easy to recognize. Platygyrium fuscoluteum is not sympatric with P. repens. Brood branchlets are much less conspicuous and much less common than in P. repens, but can often be found by careful search; they are rather effectively concealed in leaf axils at the branch apices. Homomallium mexicanum often co-occurs with P. fuscoluteum and could be mistaken for it. However, the capsule of Homomallium mexicanum is curved and constricted below the mouth, and the leaves are imbricate (not homomallous), have erect margins, and are not plicate.</discussion>
  
</bio:treatment>