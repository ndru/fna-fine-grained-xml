<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Terry T. McIntosh,Steven G. Newmaster</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">657</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="genus">CINCLIDIUM</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>1801(1): 27, plate 2. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus CINCLIDIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kinklis, latticed gate, and eidos, shape or form, alluding to endostome</other_info_on_name>
    <other_info_on_name type="fna_id">107100</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–8 (–13) cm, in tufts or mats.</text>
      <biological_entity id="o15092" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="13" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15093" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o15094" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o15092" id="r2132" name="in" negation="false" src="d0_s0" to="o15093" />
      <relation from="o15092" id="r2133" name="in" negation="false" src="d0_s0" to="o15094" />
    </statement>
    <statement id="d0_s1">
      <text>Stems dark-brown or reddish-brown, erect, simple or branching distally, not dendroid;</text>
      <biological_entity id="o15095" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids dark-brown, macronemata often matted in longitudinal rows along stem, micronemata absent.</text>
      <biological_entity id="o15096" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character constraint="in rows" constraintid="o15097" is_modifier="false" modifier="often" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
        <character is_modifier="false" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15097" name="row" name_original="rows" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o15098" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o15097" id="r2134" name="along" negation="false" src="d0_s2" to="o15098" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves green, yellowish green, reddish green, reddish-brown, or black when old, contorted when dry, spreading or erect-spreading, ± flat or reflexed to arcuate-recurved when moist, orbicular, spatulate, elliptic, ovate, ovatelanceolate, or obovate, 2–6 mm;</text>
      <biological_entity id="o15099" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character name="prominence_or_shape" src="d0_s3" value="reflexed to arcuate-recurved" value_original="reflexed to arcuate-recurved" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s3" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base short-decurrent or occasionally not decurrent;</text>
      <biological_entity id="o15100" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-decurrent" value_original="short-decurrent" />
        <character is_modifier="false" modifier="occasionally not" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins recurved or rarely plane, green, brown, or reddish-brown, 1-stratose or 2-stratose, entire;</text>
      <biological_entity id="o15101" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute, acuminate, obtuse, or rounded, rarely retuse, usually apiculate or rarely cuspidate, cusp usually toothed;</text>
      <biological_entity id="o15102" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="rarely; rarely" name="architecture_or_shape" src="d0_s6" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o15103" name="cusp" name_original="cusp" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa percurrent or excurrent, occasionally subpercurrent or ending well before apex, distal abaxial surface smooth, abaxial stereid band present, U-shaped;</text>
      <biological_entity id="o15104" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="occasionally" name="position" src="d0_s7" value="subpercurrent" value_original="subpercurrent" />
        <character name="position" src="d0_s7" value="ending well before apex" value_original="ending well before apex" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o15105" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15106" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o15107" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="u--shaped" value_original="u--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells elongate, short-elongate, or ± isodiametric, 25–110 µm, in diagonal rows or not, not or weakly collenchymatous, walls pitted;</text>
      <biological_entity constraint="medial laminal" id="o15108" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s8" to="110" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o15109" name="row" name_original="rows" src="d0_s8" type="structure" />
      <biological_entity id="o15110" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="weakly" name="architecture" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o15111" name="row" name_original="rows" src="d0_s8" type="structure" />
      <biological_entity id="o15112" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="weakly" name="architecture" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o15113" name="row" name_original="rows" src="d0_s8" type="structure" />
      <biological_entity id="o15114" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="weakly" name="architecture" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
        <character is_modifier="false" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o15108" id="r2135" name="in" negation="false" src="d0_s8" to="o15109" />
      <relation from="o15108" id="r2136" modifier="in diagonal rows or , or weakly collenchymatous , walls" name="in" negation="false" src="d0_s8" to="o15110" />
      <relation from="o15110" id="r2137" name="in" negation="false" src="d0_s8" to="o15111" />
      <relation from="o15110" id="r2138" modifier="in diagonal rows or , or weakly collenchymatous , walls" name="in" negation="false" src="d0_s8" to="o15112" />
      <relation from="o15112" id="r2139" name="in" negation="false" src="d0_s8" to="o15113" />
      <relation from="o15112" id="r2140" modifier="in diagonal rows or , or weakly collenchymatous , walls" name="in" negation="false" src="d0_s8" to="o15114" />
    </statement>
    <statement id="d0_s9">
      <text>marginal cells differentiated, short-linear or linear, in 2–4 rows.</text>
      <biological_entity id="o15116" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o15115" id="r2141" name="in" negation="false" src="d0_s9" to="o15116" />
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition synoicous or dioicous.</text>
      <biological_entity constraint="marginal" id="o15115" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta single, yellowish, red, or reddish, 1.5–7 cm, flexuose.</text>
      <biological_entity id="o15117" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s12" to="7" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s12" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule pendent, light or yellowish-brown, yellowish green, or light green, elliptic, ovate-elliptic, or subglobose, 1.5–3 mm;</text>
      <biological_entity id="o15118" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum hemispheric;</text>
      <biological_entity id="o15119" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome greenish yellow;</text>
      <biological_entity id="o15120" name="exostome" name_original="exostome" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome yellowbrown, segments fused into dome at apex.</text>
      <biological_entity id="o15121" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
      <biological_entity id="o15122" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character constraint="into dome" constraintid="o15123" is_modifier="false" name="fusion" src="d0_s16" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o15123" name="dome" name_original="dome" src="d0_s16" type="structure" />
      <biological_entity id="o15124" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <relation from="o15123" id="r2142" name="at" negation="false" src="d0_s16" to="o15124" />
    </statement>
    <statement id="d0_s17">
      <text>Spores 25–70 µm.</text>
      <biological_entity id="o15125" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s17" to="70" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>Cinclidium is most readily distinguished from similar genera in Mniaceae with entire leaf margins, in particular Rhizomnium, by the longitudinal rows of macronemata that extend up the stem and the costa having a U-shaped band of stereid cells (T. J. Koponen 1974). The fused, dome-shaped endostome, which is taller than the exostome, is also diagnostic for Cinclidium, although sporophytes are uncommon. G. S. Mogensen (1973) stressed that gametophytic variation is common in all of the species of Cinclidium, although a number of vegetative characters are useful in separating them. Cinclidium arcticum and C. latifolium have diploid sporophytes (n = 7), whereas C. stygium and C. subrotundum have tetraploid sporophytes (n = 14).</discussion>
  <references>
    <reference>Mogensen, G. S. 1973. A revision of the moss genus Cinclidium Sw. (Mniaceae Mitt.). Lindbergia 2: 49–80.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves strongly reflexed to arcuate-recurved when moist.</description>
      <determination>2 Cinclidium latifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ± flat, not strongly reflexed when moist</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves ovate, ovate-lanceolate, or narrowly elliptic, rarely obovate or orbicular; apices acuminate or acute; medial laminal cells short-elongate or ± isodiametric.</description>
      <determination>1 Cinclidium arcticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves broadly elliptic, ovate, spatulate, obovate, or ± orbicular; apices obtuse or rounded, sometimes acuminate or acute; medial laminal cells elongate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves broadly elliptic, ovate, obovate, or rarely ± orbicular; apices apiculate or cuspidate, apiculus sharp; margins 1-stratose; capsules ovate-elliptic.</description>
      <determination>3 Cinclidium stygium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves broadly elliptic, ± orbicular, or spatulate, occasionally ovate or obovate; apices usually apiculate, apiculus blunt; margins 2- or 3-stratose; capsules subglobose.</description>
      <determination>4 Cinclidium subrotundum</determination>
    </key_statement>
  </key>
</bio:treatment>