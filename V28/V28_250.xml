<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="(Bridel) J. R. Spence" date="2009" rank="subgenus">CLADODIUM</taxon_name>
    <taxon_name authority="(Schreber) J. R. Spence" date="2005" rank="species">bimum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 20. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus cladodium;species bimum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099304</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="Schreber" date="unknown" rank="species">bimum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Regensburg)</publication_title>
      <place_in_publication>1: 79. 1802</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mnium;species bimum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="(Schreber) Turner" date="unknown" rank="species">bimum</taxon_name>
    <taxon_hierarchy>genus bryum;species bimum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense turfs, green or yellow-green.</text>
      <biological_entity id="o12910" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12911" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o12910" id="r1821" name="in" negation="false" src="d0_s0" to="o12911" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–3 (–4) cm, comose, innovations elongate and evenly foliate;</text>
      <biological_entity id="o12912" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>often strongly radiculose.</text>
      <biological_entity id="o12913" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green, weakly twisted to contorted when dry, ovate, flat to weakly concave, (1–) 2–3 (–3.5) mm, not much enlarged toward stem apex;</text>
      <biological_entity id="o12914" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s3" to="weakly concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o12915" is_modifier="false" modifier="not much" name="size" src="d0_s3" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="stem" id="o12915" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base weakly decurrent;</text>
      <biological_entity id="o12916" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins revolute to mid leaf or beyond, limbidium strong, in 2 or 3 rows;</text>
      <biological_entity id="o12917" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="mid" id="o12918" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o12919" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s5" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute;</text>
      <biological_entity id="o12920" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa short-excurrent, awn smooth;</text>
      <biological_entity id="o12921" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o12922" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells 3–4: 1, same width or narrower than more distal cells;</text>
      <biological_entity constraint="proximal laminal" id="o12923" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character constraint="than more distal cells" constraintid="o12924" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12924" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>medial and distal cells rhomboidal, 12–18 µm wide, 3–4: 1, walls firm to sometimes very incrassate, sometimes at oblique 30–45° angle to costa.</text>
      <biological_entity constraint="medial and distal" id="o12925" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s9" to="18" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12926" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="sometimes very" name="size" src="d0_s9" value="incrassate" value_original="incrassate" />
        <character constraint="at" is_modifier="false" modifier="30-45°" name="orientation_or_shape" src="d0_s9" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o12928" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <relation from="o12927" id="r1822" name="to" negation="false" src="d0_s9" to="o12928" />
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition synoicous.</text>
      <biological_entity id="o12927" name="angle" name_original="angle" src="d0_s9" type="structure">
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1–3 (–4) cm.</text>
      <biological_entity id="o12929" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule brown, elongate-ovate, symmetric, 3–5 mm, mouth yellow;</text>
      <biological_entity id="o12930" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate-ovate" value_original="elongate-ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12931" name="mouth" name_original="mouth" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic, apiculate;</text>
      <biological_entity id="o12932" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome well developed;</text>
      <biological_entity id="o12933" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth yellow basally, hyaline distally, lamellae usually straight mid-tooth, pores absent along mid line;</text>
      <biological_entity constraint="exostome" id="o12934" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o12935" name="lamella" name_original="lamellae" src="d0_s16" type="structure" />
      <biological_entity id="o12936" name="mid-tooth" name_original="mid-tooth" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="usually" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o12937" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character constraint="along mid line" constraintid="o12938" is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="mid" id="o12938" name="line" name_original="line" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>endostome not adherent to exostome, basal membrane high, 1/2 exostome height, segments with ovate perforations, cilia long, appendiculate.</text>
      <biological_entity id="o12939" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character constraint="to exostome" constraintid="o12940" is_modifier="false" modifier="not" name="fusion" src="d0_s17" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o12940" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o12941" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
        <character name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o12942" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity id="o12943" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity id="o12944" name="perforation" name_original="perforations" src="d0_s17" type="structure">
        <character is_modifier="true" name="height" src="d0_s17" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o12945" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <relation from="o12943" id="r1823" name="with" negation="false" src="d0_s17" to="o12944" />
    </statement>
    <statement id="d0_s18">
      <text>Spores (10–) 12–16 µm, finely papillose, pale-yellow or green.</text>
      <biological_entity id="o12946" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="atypical_some_measurement" src="d0_s18" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s18" to="16" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="green" value_original="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, soil over rock, rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Dak., Ohio, Oreg., Pa., Utah, Vt., Wash., W.Va., Wis., Wyo.; South America; Eurasia; Africa; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum bimum has in the past been considered a form of P. pseudotriquetrum, but V. I. Zolotov (2000) provided characters that can generally separate the two species. Ptychostomum bimum is a common species on damp to wet soil or soil over rock, but its distribution is not well understood as it has had little recognition in the past. The species appears to be much more common than P. pseudotriquetrum in eastern North America.</discussion>
  
</bio:treatment>