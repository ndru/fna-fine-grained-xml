<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Judith A. Harpel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">645</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pterigynandraceae</taxon_name>
    <taxon_name authority="W. R. Buck &amp; H. A. Crum" date="unknown" rank="genus">IWATSUKIELLA</taxon_name>
    <place_of_publication>
      <publication_title>J. Hattori Bot. Lab.</publication_title>
      <place_in_publication>44: 351. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pterigynandraceae;genus IWATSUKIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Zennoske Iwatsuki, b. 1929, Japanese bryologist, and Latin -ella, diminutive</other_info_on_name>
    <other_info_on_name type="fna_id">116663</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in prostrate mats, yellowish.</text>
      <biological_entity id="o17272" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17273" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <relation from="o17272" id="r2491" name="in" negation="false" src="d0_s0" to="o17273" />
    </statement>
    <statement id="d0_s1">
      <text>Stems freely branched;</text>
      <biological_entity id="o17274" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent.</text>
      <biological_entity id="o17275" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves appressed, imbricate, suborbicular to ovatelanceolate;</text>
      <biological_entity id="o17276" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s3" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane to recurved, entire;</text>
      <biological_entity id="o17277" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex long-piliferous;</text>
    </statement>
    <statement id="d0_s6">
      <text>ecostate or occasionally costa double, short;</text>
      <biological_entity id="o17278" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-piliferous" value_original="long-piliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells not differentiated;</text>
      <biological_entity id="o17279" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o17280" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells rhombic to elliptic, smooth, walls thick.</text>
      <biological_entity constraint="medial laminal" id="o17281" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s8" to="elliptic" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o17282" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect, oblong-cylindric, symmetric.</text>
      <biological_entity id="o17283" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Sporophytes of Iwatsukiella are rare in North America.</discussion>
  
</bio:treatment>