<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">643</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">HOOKERIACEAE</taxon_name>
    <taxon_hierarchy>family HOOKERIACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10415</other_info_on_name>
  </taxon_identification>
  <number>49.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in creeping mats or patches.</text>
      <biological_entity id="o10735" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10736" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o10737" name="patch" name_original="patches" src="d0_s0" type="structure" />
      <relation from="o10735" id="r1519" name="in" negation="false" src="d0_s0" to="o10736" />
      <relation from="o10735" id="r1520" name="in" negation="false" src="d0_s0" to="o10737" />
    </statement>
    <statement id="d0_s1">
      <text>Stems green, loosely complanate-foliate;</text>
      <biological_entity id="o10738" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis and sclerodermis absent, central strand present.</text>
      <biological_entity id="o10739" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10740" name="sclerodermi" name_original="sclerodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o10741" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves broad, dorsal leaves often broader, more symmetric than lateral leaves;</text>
      <biological_entity id="o10742" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o10743" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="width" src="d0_s3" value="broader" value_original="broader" />
        <character constraint="than lateral leaves" constraintid="o10744" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="more symmetric" value_original="more symmetric" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10744" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>margins plane, entire, unbordered;</text>
      <biological_entity id="o10745" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbordered" value_original="unbordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex rounded-obtuse to sharply or broadly acute, flat;</text>
    </statement>
    <statement id="d0_s6">
      <text>ecostate;</text>
      <biological_entity id="o10746" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded-obtuse" name="shape" src="d0_s5" to="sharply or broadly acute" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminal cells large, lax, smooth, uniform across insertion and base or somewhat shorter than medial.</text>
      <biological_entity constraint="laminal" id="o10747" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="insertion" src="d0_s7" value="uniform" value_original="uniform" />
      </biological_entity>
      <biological_entity constraint="medial laminal" id="o10749" name="cell" name_original="cells" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction by filaments among apical leaf rhizoids, filaments unbranched, 1-seriate, subpapillose.</text>
      <biological_entity id="o10748" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="than medial laminal cells" constraintid="o10749" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="somewhat shorter" value_original="somewhat shorter" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o10750" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o10751" name="rhizoid" name_original="rhizoids" src="d0_s8" type="structure" constraint_original="apical leaf" />
      <relation from="o10750" id="r1521" name="among" negation="false" src="d0_s8" to="o10751" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous or sometimes dioicous;</text>
      <biological_entity id="o10752" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="1-seriate" value_original="1-seriate" />
        <character is_modifier="false" name="relief" src="d0_s8" value="subpapillose" value_original="subpapillose" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perigonia on stem, gemmate.</text>
      <biological_entity id="o10753" name="perigonium" name_original="perigonia" src="d0_s10" type="structure" />
      <biological_entity id="o10754" name="stem" name_original="stem" src="d0_s10" type="structure" />
      <relation from="o10753" id="r1522" name="on" negation="false" src="d0_s10" to="o10754" />
    </statement>
    <statement id="d0_s11">
      <text>Seta red to reddish or blackish, elongate, smooth.</text>
      <biological_entity id="o10755" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s11" to="reddish or blackish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule suberect to pendulous, smooth;</text>
      <biological_entity id="o10756" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="suberect" name="orientation" src="d0_s12" to="pendulous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exothecial cells strongly collenchymatous;</text>
      <biological_entity constraint="exothecial" id="o10757" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s13" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus distinct;</text>
      <biological_entity id="o10758" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome dark red, diplolepidous;</text>
      <biological_entity id="o10759" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="diplolepidous" value_original="diplolepidous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome unfurrowed, with zigzag median line;</text>
      <biological_entity id="o10760" name="exostome" name_original="exostome" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="unfurrowed" value_original="unfurrowed" />
      </biological_entity>
      <biological_entity constraint="median" id="o10761" name="line" name_original="line" src="d0_s16" type="structure" />
      <relation from="o10760" id="r1523" name="with" negation="false" src="d0_s16" to="o10761" />
    </statement>
    <statement id="d0_s17">
      <text>endostome basal membrane high, segments extending barely beyond exostome teeth, ± perforate, cilia rudimentary or absent.</text>
      <biological_entity id="o10762" name="endostome" name_original="endostome" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o10763" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o10764" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s17" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o10765" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
      <biological_entity id="o10766" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o10764" id="r1524" modifier="barely" name="beyond" negation="false" src="d0_s17" to="o10765" />
    </statement>
    <statement id="d0_s18">
      <text>Calyptra somewhat lobed at base, multistratose at middle, smooth, naked.</text>
      <biological_entity id="o10767" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character constraint="at base" constraintid="o10768" is_modifier="false" modifier="somewhat" name="shape" src="d0_s18" value="lobed" value_original="lobed" />
        <character constraint="at middle" constraintid="o10769" is_modifier="false" name="architecture" notes="" src="d0_s18" value="multistratose" value_original="multistratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o10768" name="base" name_original="base" src="d0_s18" type="structure" />
      <biological_entity id="o10769" name="middle" name_original="middle" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; tropical and temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="tropical and temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 2, species ca. 8 (1 genus, 2 species in the flora).</discussion>
  <discussion>Hookeriaceae, once consisting of many genera (W. H. Welch 1966, 1976), is currently recognized as comprising two genera (B. Goffinet et al. 2008; W. Frey 2009+): Crossomitrium Müller Hal., with six species (B. H. Allen 1990), and Hookeria with ten species, only two of which are well known (M. R. Crosby et al., unpubl.). The absolute absence of a costa in the leaves is a major character separating this family from those closely related.</discussion>
  <discussion>The two species of Hookeria in the flora area have laminal cells large enough to observe with a hand lens, lax and broadly oblong-hexagonal or rhomboidal. Lepidopilum (Bridel) Bridel, as L. polytrichoides (Hedwig) Bridel, previously recognized in Hookeriaceae but now in Pilotrichaceae, has been reported by W. H. Welch (1962) and W. D. Reese (1984) from a single collection near Pensacola, Florida. This species has not been found in that area, despite extensive sampling. Reese suggested that it was collected elsewhere and mislabeled.</discussion>
  <references>
    <reference>Welch, W. H. 1962. The Hookeriaceae of the United States and Canada. Bryologist 65: 1–24.</reference>
    <reference>Welch, W. H. 1976. Hookeriaceae. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Ser. 2, part 9.</reference>
  </references>
  
</bio:treatment>