<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="N. Pedersen" date="unknown" rank="genus">imbribryum</taxon_name>
    <taxon_name authority="(De Notaris) J. R. Spence" date="2007" rank="species">gemmiparum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>89: 112. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus imbribryum;species gemmiparum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099190</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="De Notaris" date="unknown" rank="species">gemmiparum</taxon_name>
    <place_of_publication>
      <publication_title>Comment. Soc. Crittog. Ital.</publication_title>
      <place_in_publication>2: 211. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species gemmiparum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, bright green to yellow-green near tips, stramineous with age.</text>
      <biological_entity id="o1946" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character char_type="range_value" constraint="near tips" constraintid="o1947" from="bright green" name="coloration" src="d0_s0" to="yellow-green" />
        <character constraint="with age" constraintid="o1948" is_modifier="false" name="coloration" notes="" src="d0_s0" value="stramineous" value_original="stramineous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1947" name="tip" name_original="tips" src="d0_s0" type="structure" />
      <biological_entity id="o1948" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2 (–3) cm, weakly julaceous, with or without metallic sheen, older stem sometimes densely brown radiculose.</text>
      <biological_entity id="o1949" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves soft, loosely set, somewhat distant proximally, green, younger leaves sometimes yellowish, ovate, strongly concave, 1–3 mm;</text>
      <biological_entity id="o1950" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="somewhat; proximally" name="arrangement" notes="" src="d0_s2" value="distant" value_original="distant" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1951" name="set" name_original="set" src="d0_s2" type="structure" />
      <biological_entity id="o1952" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="younger" value_original="younger" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base weakly decurrent;</text>
      <biological_entity id="o1953" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane throughout, rarely revolute proximally, entire, limbidium absent;</text>
      <biological_entity id="o1954" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="rarely; proximally" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1955" name="limbidium" name_original="limbidium" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex rounded to acute, not cucullate;</text>
      <biological_entity id="o1956" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa brown to yellowbrown, not reaching apex or percurrent, awn absent;</text>
      <biological_entity id="o1957" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s6" to="yellowbrown" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o1958" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o1959" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o1957" id="r259" name="reaching" negation="true" src="d0_s6" to="o1958" />
    </statement>
    <statement id="d0_s7">
      <text>basal row of pigmented cells absent;</text>
      <biological_entity constraint="basal" id="o1960" name="row" name_original="row" src="d0_s7" type="structure" />
      <biological_entity id="o1961" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pigmented" value_original="pigmented" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o1960" id="r260" name="consist_of" negation="false" src="d0_s7" to="o1961" />
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells abruptly short-rectangular, occasionally with scattered quadrate cells, 2–3: 1;</text>
      <biological_entity constraint="proximal laminal" id="o1962" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="short-rectangular" value_original="short-rectangular" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1963" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="shape" src="d0_s8" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o1962" id="r261" modifier="occasionally" name="with" negation="false" src="d0_s8" to="o1963" />
    </statement>
    <statement id="d0_s9">
      <text>medial and distal cells hexagonal, (30–) 40–60 × (14–) 16–26 µm, 3 (–4):1, walls thin, parallel to costa.</text>
      <biological_entity constraint="medial and distal" id="o1964" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="30" from_unit="um" name="atypical_length" src="d0_s9" to="40" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s9" to="60" to_unit="um" />
        <character char_type="range_value" from="14" from_unit="um" name="atypical_width" src="d0_s9" to="16" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="width" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1966" name="costa" name_original="costa" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction rare, by rhizoidal tubers [leaf-axil bulbils], on rhizoids arising from leaf-axils, pink to orange, 100–200 µm. Seta ± straight, brown.</text>
      <biological_entity id="o1965" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character constraint="to costa" constraintid="o1966" is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o1967" name="rhizoidal" name_original="rhizoidal" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="rare" value_original="rare" />
      </biological_entity>
      <biological_entity id="o1968" name="tuber" name_original="tubers" src="d0_s10" type="structure" />
      <biological_entity id="o1969" name="rhizoid" name_original="rhizoids" src="d0_s10" type="structure">
        <character constraint="from leaf-axils" constraintid="o1970" is_modifier="false" name="orientation" src="d0_s10" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o1970" name="axil-leaf" name_original="leaf-axils" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="orange" />
        <character char_type="range_value" from="100" from_unit="um" name="distance" src="d0_s10" to="200" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1971" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule inclined to nutant, brown, pyriform, 2–3 mm, neck short.</text>
      <biological_entity id="o1972" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s11" to="nutant" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1973" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores 12–18 µm, smooth to papillose, yellowish.</text>
      <biological_entity id="o1974" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s12" to="18" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp to wet calcareous soil, soil over rock, associated with springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp to wet calcareous" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-1800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont.; Alaska, Ariz., Calif., Colo., Idaho, Maine, Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.J., N.Mex., N.Y., Okla., Oreg., Pa., S.Dak., Utah, Vt., Wash., Wyo.; s Europe; Asia (Turkey); n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Turkey)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>European plants of Imbribryum gemmiparum are reported to produce small, leafy bulbils in the leaf axils, but there is some controversy as these are also sometimes described as short innovations with stalks. Molecular studies show that at least some European material appears to be related to the Gemmabryum dichotomum complex. However, North American material is much larger in size and otherwise morphologically closest to other Imbribryum species. In eastern United States material, a short leaf apiculus is often present. Future studies may indicate that there are two species present, one in North America and the other in Europe and the Mediterranean. The plants are typically found in strongly calcareous springs; capsules are rare.</discussion>
  
</bio:treatment>