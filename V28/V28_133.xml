<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="illustration_page">82</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hedwigiaceae</taxon_name>
    <taxon_name authority="P. Beauvois" date="unknown" rank="genus">hedwigia</taxon_name>
    <taxon_name authority="(Hedwig) P. Beauvois" date="1805" rank="species">ciliata</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Aethéogam.,</publication_title>
      <place_in_publication>15. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hedwigiaceae;genus hedwigia;species ciliata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200001684</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anictangium</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">ciliatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>40. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus anictangium;species ciliatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hedwigia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ciliata</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="subspecies">subnuda</taxon_name>
    <taxon_hierarchy>genus hedwigia;species ciliata;subspecies subnuda</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1.5–3 mm;</text>
      <biological_entity id="o19768" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>margins broadly recurved to 1/3 leaf length, narrowly recurved to apex on one or both sides, or plane mid leaf and plane, erect, and/or incurved in acumen on same plant, teeth in apex broad, irregularly papillose to smooth;</text>
      <biological_entity id="o19769" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="broadly" name="length" src="d0_s1" value="recurved" value_original="recurved" />
        <character constraint="to apex" constraintid="o19770" is_modifier="false" modifier="narrowly" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in acumen" constraintid="o19773" is_modifier="false" name="orientation" src="d0_s1" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity id="o19770" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o19771" name="side" name_original="sides" src="d0_s1" type="structure" />
      <biological_entity constraint="mid" id="o19772" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character is_modifier="true" name="shape" src="d0_s1" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o19773" name="acumen" name_original="acumen" src="d0_s1" type="structure" />
      <biological_entity id="o19774" name="plant" name_original="plant" src="d0_s1" type="structure" />
      <biological_entity id="o19775" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character char_type="range_value" from="irregularly papillose" name="relief" notes="" src="d0_s1" to="smooth" />
      </biological_entity>
      <biological_entity id="o19776" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o19770" id="r2833" name="on" negation="false" src="d0_s1" to="o19771" />
      <relation from="o19773" id="r2834" name="on" negation="false" src="d0_s1" to="o19774" />
      <relation from="o19775" id="r2835" name="in" negation="false" src="d0_s1" to="o19776" />
    </statement>
    <statement id="d0_s2">
      <text>apex erect, erect-spreading, or often secund when dry, broad and hyaline, muticous, or acuminate with acumen narrow-based, 1/10–1/5 leaf length, broadly or narrowly, shallowly or deeply channeled, extreme apex flat;</text>
      <biological_entity id="o19777" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" name="width" src="d0_s2" value="broad" value_original="broad" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s2" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="muticous" value_original="muticous" />
        <character constraint="with acumen" constraintid="o19778" is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19778" name="acumen" name_original="acumen" src="d0_s2" type="structure">
        <character char_type="range_value" from="1/10" name="quantity" src="d0_s2" to="1/5" />
      </biological_entity>
      <biological_entity id="o19779" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly; narrowly; shallowly; deeply" name="length" src="d0_s2" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o19780" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>medial and distal laminal cells with papillae (1–) 2 (–4), small, simple, sessile, or low-stalked, variously branched;</text>
      <biological_entity constraint="medial and distal laminal" id="o19781" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="low-stalked" value_original="low-stalked" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="low-stalked" value_original="low-stalked" />
        <character is_modifier="false" modifier="variously" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o19782" name="papilla" name_original="papillae" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="4" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <relation from="o19781" id="r2836" name="with" negation="false" src="d0_s3" to="o19782" />
    </statement>
    <statement id="d0_s4">
      <text>laminal cells in hyaline area with papillae low, simple, in linear rows throughout or sparsely to coarsely and irregularly papillose proximally and smooth to minutely rounded-papillose distally;</text>
      <biological_entity constraint="laminal" id="o19783" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely to coarsely; coarsely; irregularly; proximally" name="relief" notes="" src="d0_s4" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="smooth" modifier="distally" name="relief" src="d0_s4" to="minutely rounded-papillose" />
      </biological_entity>
      <biological_entity id="o19784" name="area" name_original="area" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o19785" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o19786" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o19783" id="r2837" name="in" negation="false" src="d0_s4" to="o19784" />
      <relation from="o19784" id="r2838" name="with" negation="false" src="d0_s4" to="o19785" />
      <relation from="o19783" id="r2839" name="in" negation="false" src="d0_s4" to="o19786" />
    </statement>
    <statement id="d0_s5">
      <text>apical cell short-rhomboidal, obtuse to truncate, (40–) 50–75 (–80) µm, multipapillose-coronate.</text>
      <biological_entity constraint="apical" id="o19787" name="cell" name_original="cell" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="short-rhomboidal" value_original="short-rhomboidal" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="truncate" />
        <character char_type="range_value" from="40" from_unit="um" name="atypical_some_measurement" src="d0_s5" to="50" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s5" to="80" to_unit="um" />
        <character char_type="range_value" from="50" from_unit="um" name="some_measurement" src="d0_s5" to="75" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s5" value="multipapillose-coronate" value_original="multipapillose-coronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perichaetia with leaves plane, margins long-ciliate distally.</text>
      <biological_entity id="o19788" name="perichaetium" name_original="perichaetia" src="d0_s6" type="structure" />
      <biological_entity id="o19789" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o19790" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s6" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <relation from="o19788" id="r2840" name="with" negation="false" src="d0_s6" to="o19789" />
    </statement>
    <statement id="d0_s7">
      <text>Vaginula sparsely to densely pilose.</text>
      <biological_entity id="o19791" name="vaginulum" name_original="vaginula" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra naked, sparsely pilose proximally, or densely pilose throughout.</text>
      <biological_entity id="o19792" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="late spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rock, acidic rock (granite, sedimentary), conglomerates, limestone, soil, cliffs, dry, sunny boulders, in woods on acidic glacial erratic rock, tree trunks and branches, asphalt shingles, edges of asphalt roads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rock" />
        <character name="habitat" value="acidic rock" />
        <character name="habitat" value="acidic rock ( granite" />
        <character name="habitat" value="sedimentary)" />
        <character name="habitat" value="conglomerates" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="sunny boulders" />
        <character name="habitat" value="woods" modifier="in" constraint="on acidic glacial erratic rock" />
        <character name="habitat" value="acidic glacial erratic rock" />
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="branches" />
        <character name="habitat" value="asphalt shingles" />
        <character name="habitat" value="edges" constraint="of asphalt roads" />
        <character name="habitat" value="asphalt roads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., Wash., W.Va., Wis., Wyo.; Mexico; West Indies; Central America; South America; Eurasia; Africa; Atlantic Islands; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In plants of Hedwigia ciliata with dense paraphyses extending onto the calyptra, the paraphyses are sparsely papillose and have sharp lateral teeth on one side at the distal ends of some cells. In the eastern to northeastern part of the flora area, the typical facies have vaginula and calyptra that are densely pilose. The hyaline apices are absent to short or to 1/3 the leaf length.</discussion>
  <discussion>Although the type of Hedwigia ciliata var. leucophaea has apparently been lost, L. Hedenäs (1994) gave formal synonymy for var. leucophaea. W. R. Buck and D. H. Norris (1996) proposed the name H. nivalis for a facies of what was formerly called H. ciliata in the southwestern United States, and for all tropical American material. B. H. Allen (2010) accepted this suggestion. M. Lueth and A. Schaefer-Verwimp (2004) reported var. leucophaea as new to South America and that the two specimens of the variety were collected from the type locality of H. nivalis. The leaves in var. leucophaea are more oblong (less ovate) than in H. nivalis, the acumina are narrower, and the calyptra is pilose. These characters in the flora area are present and variable throughout the range of H. ciliata, especially in areas west of mid-continent.</discussion>
  
</bio:treatment>