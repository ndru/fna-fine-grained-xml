<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="illustration_page">227</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">mnium</taxon_name>
    <taxon_name authority="Schwagrichen" date="1826" rank="species">lycopodioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(2,1): 24, plate 160 [bottom]. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus mnium;species lycopodioides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200001496</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="H. Müller" date="unknown" rank="species">ambiguum</taxon_name>
    <taxon_hierarchy>genus mnium;species ambiguum</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (1–) 2–4 (–7) cm.</text>
      <biological_entity id="o11943" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems red, brown, or sometimes yellowish-brown.</text>
      <biological_entity id="o11944" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green to dark green, contorted and twisted when dry, narrowly elliptic, ovate-elliptic, or ovate-oblong, 3–4 (–6.5) mm;</text>
      <biological_entity id="o11945" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="dark green" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-oblong" value_original="ovate-oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base long-decurrent;</text>
      <biological_entity id="o11946" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins reddish-brown, 2-stratose, toothed to below mid leaf, teeth paired or sometimes single near apex, long, sharp, occasionally short and blunt;</text>
      <biological_entity id="o11947" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o11948" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s4" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o11949" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character constraint="near apex" constraintid="o11950" is_modifier="false" modifier="sometimes" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
        <character is_modifier="false" modifier="occasionally" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o11950" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute, rounded-acute, or obtuse, apiculate or cuspidate, cusp sometimes toothed;</text>
      <biological_entity id="o11951" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-acute" value_original="rounded-acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-acute" value_original="rounded-acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o11952" name="cusp" name_original="cusp" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent or excurrent, distal abaxial surface toothed;</text>
      <biological_entity id="o11953" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o11954" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells ± isodiametric or short-elongate, (17–) 22–40 µm, smaller towards margins, often in longitudinal rows, not in diagonal rows, usually strongly collenchymatous, blue postmortal color absent;</text>
      <biological_entity constraint="medial laminal" id="o11955" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-elongate" value_original="short-elongate" />
        <character char_type="range_value" from="17" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="22" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s7" to="40" to_unit="um" />
        <character constraint="towards margins" constraintid="o11956" is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="usually strongly" name="architecture" notes="" src="d0_s7" value="collenchymatous" value_original="collenchymatous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11956" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o11957" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o11958" name="row" name_original="rows" src="d0_s7" type="structure" />
      <relation from="o11955" id="r1676" modifier="often" name="in" negation="false" src="d0_s7" to="o11957" />
      <relation from="o11955" id="r1677" name="in" negation="true" src="d0_s7" to="o11958" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells linear, in 2 or 3 rows.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o11959" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta single.</text>
      <biological_entity id="o11960" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule yellowish or yellowbrown, 2.5–6 (–7) mm;</text>
      <biological_entity id="o11961" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowbrown" value_original="yellowbrown" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum short-rostrate;</text>
      <biological_entity id="o11962" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome greenish yellow.</text>
      <biological_entity id="o11963" name="exostome" name_original="exostome" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 19–30 µm.</text>
      <biological_entity id="o11964" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="19" from_unit="um" name="some_measurement" src="d0_s14" to="30" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded, often calcareous rock or cliffs, banks along streams, tree bases, logs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded" />
        <character name="habitat" value="calcareous rock" modifier="often" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="banks" constraint="along streams , tree bases ," />
        <character name="habitat" value="streams" />
        <character name="habitat" value="tree bases" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Yukon; Conn., Idaho, Maine, Mass., Mich., Minn., Mont., Nebr., N.H., N.Y., Oreg., Pa., R.I., S.Dak., Vt., Wash., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mnium lycopodioides is often difficult to separate from M. marginatum. This is reflected in herbarium collections where many, in particular sterile, specimens of M. marginatum have been identified as M. lycopodioides. Mnium lycopodioides usually has narrower leaves, although some collections of M. marginatum have narrow leaves, with narrow leaf margins composed of two or three rows of cells. Mnium lycopodioides has a shorter rostrum on the calyptra, a useful character with fertile collections.</discussion>
  
</bio:treatment>