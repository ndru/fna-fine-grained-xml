<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">PTYCHOSTOMUM</taxon_name>
    <taxon_name authority="(Sprengel) J. R. Spence" date="2005" rank="species">weigelii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 22. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus ptychostomum;species weigelii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099330</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Sprengel" date="unknown" rank="species">weigelii</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Prim. Fl. Hal.,</publication_title>
      <place_in_publication>55. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species weigelii</taxon_hierarchy>
  </taxon_identification>
  <number>30.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose low open turfs, green or rarely pinkish red.</text>
      <biological_entity id="o4523" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="pinkish red" value_original="pinkish red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4524" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="position" src="d0_s0" value="low" value_original="low" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o4523" id="r606" name="in" negation="false" src="d0_s0" to="o4524" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–4 (–6) cm, fertile stems comose, innovations evenly foliate.</text>
      <biological_entity id="o4525" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4526" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
      <biological_entity id="o4527" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green, yellow-green, or rarely red or pink, distant, strongly contorted to shrunken when dry, ovatelanceolate, flat, 1–3 mm, not much enlarged toward stem apex;</text>
      <biological_entity id="o4528" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distant" value_original="distant" />
        <character is_modifier="false" modifier="strongly" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="size" src="d0_s2" value="shrunken" value_original="shrunken" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o4529" is_modifier="false" modifier="not much" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="stem" id="o4529" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>base usually green, strongly and broadly decurrent, decurrencies almost reaching next more proximal leaf;</text>
      <biological_entity id="o4530" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="strongly; broadly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o4531" name="decurrency" name_original="decurrencies" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o4532" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="true" name="growth_order_or_position" src="d0_s3" value="next" value_original="next" />
      </biological_entity>
      <relation from="o4531" id="r607" modifier="almost" name="reaching" negation="false" src="d0_s3" to="o4532" />
    </statement>
    <statement id="d0_s4">
      <text>margins revolute proximally, plane distally, limbidium weak, in 1 (or 2) rows;</text>
      <biological_entity id="o4533" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o4534" name="limbidium" name_original="limbidium" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o4535" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4534" id="r608" name="in" negation="false" src="d0_s4" to="o4535" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute;</text>
      <biological_entity id="o4536" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa not reaching apex to short-excurrent, awn slender;</text>
      <biological_entity id="o4537" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o4538" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o4539" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
      <relation from="o4537" id="r609" name="reaching" negation="true" src="d0_s6" to="o4538" />
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells hexagonal to rectangular, 3–4: 1;</text>
      <biological_entity constraint="proximal laminal" id="o4540" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s7" to="rectangular" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial and distal cells 18–25 (–30) µm wide, 2–3: 1, walls thin.</text>
      <biological_entity constraint="medial and distal" id="o4541" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="um" name="width" src="d0_s8" to="30" to_unit="um" />
        <character char_type="range_value" from="18" from_unit="um" name="width" src="d0_s8" to="25" to_unit="um" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o4542" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta redbrown, 2–4 cm, slender, straight to somewhat flexuose.</text>
      <biological_entity id="o4543" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s11" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule brown, turbinate to pyriform, symmetric, 3–4 mm, mouth yellow;</text>
      <biological_entity id="o4544" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s12" to="pyriform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4545" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum convex, apiculate;</text>
      <biological_entity id="o4546" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome well developed;</text>
      <biological_entity id="o4547" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s14" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth yellow basally, hyaline distally, lamellae straight, pores absent near base along mid line;</text>
      <biological_entity constraint="exostome" id="o4548" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o4549" name="lamella" name_original="lamellae" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o4550" name="pore" name_original="pores" src="d0_s15" type="structure">
        <character constraint="near base" constraintid="o4551" is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4551" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity constraint="mid" id="o4552" name="line" name_original="line" src="d0_s15" type="structure" />
      <relation from="o4551" id="r610" name="along" negation="false" src="d0_s15" to="o4552" />
    </statement>
    <statement id="d0_s16">
      <text>endostome not adherent to exostome, basal membrane 1/2 exostome height, segments broadly perforate, cilia well developed, long, appendiculate.</text>
      <biological_entity id="o4553" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character constraint="to exostome" constraintid="o4554" is_modifier="false" modifier="not" name="fusion" src="d0_s16" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o4554" name="exostome" name_original="exostome" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o4555" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o4556" name="exostome" name_original="exostome" src="d0_s16" type="structure" />
      <biological_entity id="o4557" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="broadly" name="height" src="d0_s16" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o4558" name="cilium" name_original="cilia" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s16" value="developed" value_original="developed" />
        <character is_modifier="false" name="length_or_size" src="d0_s16" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores (12–) 14–18 µm, yellow or green.</text>
      <biological_entity id="o4559" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="atypical_some_measurement" src="d0_s17" to="14" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s17" to="18" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, wetlands, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Maine, Mass., Mich., Minn., Mont., Nev., N.H., N.Dak., Ohio, Oreg., Pa., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; South America; Eurasia; Africa; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum weigelii is a common arctic-boreal to north-temperate species in wet sites, easily identified by the broad, long marginal decurrencies of the leaves that nearly reach to the next leaf. Leaves of the similar P. cyclophyllum and P. turbinatum are either not decurrent or only produce short, very slender decurrencies. Plants of P. weigelii from Colorado are pinkish red.</discussion>
  
</bio:treatment>