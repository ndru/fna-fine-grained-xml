<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="illustration_page">205</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">mielichhoferiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">pohlia</taxon_name>
    <taxon_name authority="(Schimper) Mårtenssen" date="1956" rank="species">filum</taxon_name>
    <place_of_publication>
      <publication_title>Kungl. Svenska Vetenskapsakad. Avh. Naturskyddsärenden</publication_title>
      <place_in_publication>14: 149. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mielichhoferiaceae;genus pohlia;species filum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">filum</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Eur. ed.</publication_title>
      <place_in_publication>2, 470. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species filum</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, green to light green, glossy.</text>
      <biological_entity id="o15494" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="light green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–4 cm.</text>
      <biological_entity id="o15495" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, lanceolate, 0.6–1.2 mm;</text>
      <biological_entity id="o15496" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s2" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate to serrate in distal 1/3;</text>
      <biological_entity id="o15497" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in distal 1/3" constraintid="o15498" is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrulate to serrate" value_original="serrulate to serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15498" name="1/3" name_original="1/3" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa subpercurrent;</text>
      <biological_entity id="o15499" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal medial laminal cells rhombic to rhomboidal, 35–95 µm, walls thin.</text>
      <biological_entity constraint="distal medial laminal" id="o15500" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s5" to="rhomboidal" />
        <character char_type="range_value" from="35" from_unit="um" name="some_measurement" src="d0_s5" to="95" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction usually present when sterile;</text>
      <biological_entity id="o15501" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" modifier="when sterile" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>axillary gemmae 1 (or 2), bulbiform, oblong or elliptic to subglobose, green to yellow, black when old, leaf primordia restricted to apex or rarely 1 or 2 proximally, laminate, small, stiff.</text>
      <biological_entity constraint="axillary" id="o15502" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s7" value="bulbiform" value_original="bulbiform" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="subglobose" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="yellow" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s7" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o15504" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o15503" id="r2196" name="restricted to" negation="false" src="d0_s7" to="o15504" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="leaf" id="o15503" name="primordium" name_original="primordia" src="d0_s7" type="structure">
        <character modifier="rarely" name="quantity" src="d0_s7" unit="or proximally" value="1" value_original="1" />
        <character modifier="rarely" name="quantity" src="d0_s7" unit="or proximally" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="laminate" value_original="laminate" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonial leaves ovate;</text>
      <biological_entity constraint="perigonial" id="o15505" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves scarcely differentiated, lanceolate.</text>
      <biological_entity constraint="perichaetial" id="o15506" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="scarcely" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta orangebrown.</text>
      <biological_entity id="o15507" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined 95–180°, brown to stramineous, pyriform, neck 1/3 urn length;</text>
      <biological_entity id="o15508" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="95-180°" name="orientation" src="d0_s12" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s12" to="stramineous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o15509" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o15510" name="urn" name_original="urn" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>exothecial cells short-rectangular, walls sinuate;</text>
      <biological_entity constraint="exothecial" id="o15511" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o15512" name="wall" name_original="walls" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata superficial;</text>
      <biological_entity id="o15513" name="stoma" name_original="stomata" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superficial" value_original="superficial" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>annulus present;</text>
      <biological_entity id="o15514" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum convex-conic;</text>
      <biological_entity id="o15515" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="convex-conic" value_original="convex-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome teeth yellowbrown, narrowly triangular-acute;</text>
      <biological_entity constraint="exostome" id="o15516" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="triangular-acute" value_original="triangular-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome hyaline, basal membrane 1/2 exostome length, segments distinctly keeled, broadly perforate, cilia short to rudimentary.</text>
      <biological_entity id="o15517" name="endostome" name_original="endostome" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15518" name="membrane" name_original="membrane" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o15519" name="exostome" name_original="exostome" src="d0_s18" type="structure" />
      <biological_entity id="o15520" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="distinctly" name="length" src="d0_s18" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s18" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o15521" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" value_original="short" />
        <character is_modifier="false" name="prominence" src="d0_s18" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 16–23 µm, finely roughened.</text>
      <biological_entity id="o15522" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s19" to="23" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly, organic-poor soil, glacial outwash, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="organic-poor soil" modifier="gravelly" />
        <character name="habitat" value="glacial outwash" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T., Nunavut, P.E.I., Que., Sask.; Alaska, Oreg.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pohlia filum is an easily recognized species characterized by erect, somewhat glossy leaves, and ovoid gemmae that normally arise singly in the leaf axils. The gemmae have a few small, triangular, rather stiff leaf primordia at the apex, rarely with one or two primordia proximal on the gemma body. The gemmae of P. drummondii are more elongate-cylindric and branchlike, with larger, flexuose, often green leaf primordia at the apex and also frequently more proximally on the gemma body.</discussion>
  
</bio:treatment>