<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">rosulabryum</taxon_name>
    <taxon_name authority="(Syed) Ochyra" date="2003" rank="species">laevifilum</taxon_name>
    <place_of_publication>
      <publication_title>Biodivers. Poland</publication_title>
      <place_in_publication>3: 162. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus rosulabryum;species laevifilum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099356</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Syed" date="unknown" rank="species">laevifilum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bryol.</publication_title>
      <place_in_publication>7: 293, figs. 13, 14. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species laevifilum</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants very small to medium-sized, bright green.</text>
      <biological_entity id="o12674" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.3–1.5 cm, distinctly singly rosulate, innovations short, rosulate.</text>
      <biological_entity id="o12675" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="distinctly singly" name="arrangement" src="d0_s1" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity id="o12676" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves of main rosette and innovations similar;</text>
      <biological_entity constraint="main" id="o12678" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity id="o12679" name="innovation" name_original="innovations" src="d0_s2" type="structure" />
      <relation from="o12677" id="r1789" name="part_of" negation="false" src="d0_s2" to="o12678" />
      <relation from="o12677" id="r1790" name="part_of" negation="false" src="d0_s2" to="o12679" />
    </statement>
    <statement id="d0_s3">
      <text>irregularly shrunken or contorted but not spirally twisted around stem, erect-spreading when moist, obovate, flat, 0.4–3 mm;</text>
      <biological_entity id="o12677" name="leaf" name_original="leaves" src="d0_s2" type="structure" constraint="rosette" constraint_original="rosette; rosette">
        <character is_modifier="false" modifier="irregularly" name="size" src="d0_s3" value="shrunken" value_original="shrunken" />
      </biological_entity>
      <biological_entity id="o12680" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="not spirally" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base decurrent;</text>
      <biological_entity id="o12681" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane or recurved to mid leaf, serrulate distally, limbidium present, often weak, of 1 or 2 rows of cells;</text>
      <biological_entity id="o12682" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" notes="" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o12683" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o12684" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="fragility" src="d0_s5" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o12685" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <relation from="o12684" id="r1791" modifier="of 1 or 2rows" name="part_of" negation="false" src="d0_s5" to="o12685" />
    </statement>
    <statement id="d0_s6">
      <text>apex acute;</text>
      <biological_entity id="o12686" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa highly variable, not reaching apex to short-excurrent in same rosette, awn slender when present, irregularly twisted when dry;</text>
      <biological_entity id="o12687" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="highly" name="variability" src="d0_s7" value="variable" value_original="variable" />
        <character constraint="in rosette" constraintid="o12689" is_modifier="false" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o12688" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o12689" name="rosette" name_original="rosette" src="d0_s7" type="structure" />
      <biological_entity id="o12690" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="when present" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <relation from="o12687" id="r1792" name="reaching" negation="true" src="d0_s7" to="o12688" />
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells long-rectangular in rosette leaves;</text>
      <biological_entity constraint="proximal laminal" id="o12691" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="in rosette leaves" constraintid="o12692" is_modifier="false" name="shape" src="d0_s8" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
      <biological_entity constraint="rosette" id="o12692" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>medial and distal cells short-rhomboidal, 12–20 µm wide, 3–4: 1, walls thin, not porose.</text>
      <biological_entity constraint="medial and distal" id="o12693" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rhomboidal" value_original="short-rhomboidal" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s9" to="20" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction by gemmae in distal leaf-axils or sometimes arising from leaves, green when young, brown when mature, finely papillose to ± smooth, and rhizoidal tubers, brown, (70–) 100–200 µm. Sexual condition dioicous.</text>
      <biological_entity id="o12694" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="porose" value_original="porose" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity id="o12695" name="gemma" name_original="gemmae" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12696" name="leaf-axil" name_original="leaf-axils" src="d0_s10" type="structure" />
      <biological_entity id="o12697" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="sometimes" name="orientation" src="d0_s10" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="finely papillose" name="relief" src="d0_s10" to="more or less smooth" />
      </biological_entity>
      <biological_entity constraint="rhizoidal" id="o12698" name="tuber" name_original="tubers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="70" from_unit="um" name="atypical_some_measurement" src="d0_s10" to="100" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="100" from_unit="um" name="some_measurement" src="d0_s10" to="200" to_unit="um" />
      </biological_entity>
      <relation from="o12695" id="r1793" name="in" negation="false" src="d0_s10" to="o12696" />
      <relation from="o12695" id="r1794" name="in" negation="false" src="d0_s10" to="o12697" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule inclined to nutant, brown to redbrown, subcylindric, 2–4 mm.</text>
      <biological_entity id="o12699" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s11" to="nutant" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s11" to="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bark, rotten wood, rock, soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bark" />
        <character name="habitat" value="rotten wood" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Que., Sask.; Alaska, Ariz., Ark., Calif., Colo., Idaho., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., Tenn., Utah, Vt., Va., Wash., Wis., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rosulabryum laevifilum is the most widespread species of the genus in the flora area with filiform gemmae, and it is the most common corticolous species. The sporophytes are rare. Specimens on bark are often very small compared with those on other substrates. See discussion under 8. R. flaccidum.</discussion>
  
</bio:treatment>