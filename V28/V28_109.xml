<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="illustration_page">73</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">schlotheimia</taxon_name>
    <taxon_name authority="(Hooker) Schwagrichen" date="1824" rank="species">rugifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(1,2): 150. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus schlotheimia;species rugifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062003</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthotrichum</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">rugifolium</taxon_name>
    <place_of_publication>
      <publication_title>Musci Exot.</publication_title>
      <place_in_publication>2: plate 128. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthotrichum;species rugifolium</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants reddish-brown to dark-brown.</text>
      <biological_entity id="o2313" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s0" to="dark-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2–0.8 cm.</text>
      <biological_entity id="o2314" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s1" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves oblong to oblong-ligulate, rugose, 1.4–1.6 mm;</text>
      <biological_entity id="o2315" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="oblong-ligulate" value_original="oblong-ligulate" />
        <character is_modifier="false" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="distance" src="d0_s2" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire;</text>
      <biological_entity id="o2316" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex abruptly apiculate;</text>
      <biological_entity id="o2317" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa short-excurrent;</text>
      <biological_entity id="o2318" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells roundedquadrate, 5–9 µm. Seta 2.5–4 mm.</text>
      <biological_entity constraint="distal laminal" id="o2319" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="roundedquadrate" value_original="roundedquadrate" />
        <character char_type="range_value" from="5" from_unit="um" name="some_measurement" src="d0_s6" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity id="o2320" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule 1.7–2.2 mm.</text>
      <biological_entity id="o2321" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra papillose distally.</text>
      <biological_entity id="o2322" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Logs, trunks of trees frequently above 3 m, branches in tree canopies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="trunks" modifier="logs" constraint="of trees frequently above 3 m , branches in tree canopies" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="3 m" />
        <character name="habitat" value="branches" constraint="in tree canopies" />
        <character name="habitat" value="tree canopies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex., Va.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schlotheimia rugifolia is distinguished by its rugose, abruptly apiculate leaves. Characteristically, the plants have a dark reddish brown color, which is best developed under xeric conditions.</discussion>
  
</bio:treatment>