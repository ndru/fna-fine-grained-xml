<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">96</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">aulacomniaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">arrhenopterum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">heterostichum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>198, plate 46, figs. 1 – 9. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aulacomniaceae;genus arrhenopterum;species heterostichum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099003</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aulacomnium</taxon_name>
    <taxon_name authority="(Hedwig) Bruch &amp; Schimper" date="unknown" rank="species">heterostichum</taxon_name>
    <taxon_hierarchy>genus aulacomnium;species heterostichum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in small mats, glossy distally when dry, scarcely altered when moist.</text>
      <biological_entity id="o21100" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="when dry" name="reflectance" notes="" src="d0_s0" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="when moist" name="variability" src="d0_s0" value="altered" value_original="altered" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21101" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <relation from="o21100" id="r3030" name="in" negation="false" src="d0_s0" to="o21101" />
    </statement>
    <statement id="d0_s1">
      <text>Stems reddish-brown or yellow, weakly complanate-foliate, densely branched, mostly from subfloral branches;</text>
      <biological_entity id="o21102" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21103" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="subfloral" value_original="subfloral" />
      </biological_entity>
      <relation from="o21102" id="r3031" modifier="mostly" name="from" negation="false" src="d0_s1" to="o21103" />
    </statement>
    <statement id="d0_s2">
      <text>rhizoids conspicuous along stem.</text>
      <biological_entity id="o21104" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character constraint="along stem" constraintid="o21105" is_modifier="false" name="prominence" src="d0_s2" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o21105" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves with margins revolute on one side (or both) proximally;</text>
      <biological_entity id="o21106" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21107" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="on side" constraintid="o21108" is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o21108" name="side" name_original="side" src="d0_s3" type="structure" />
      <relation from="o21106" id="r3032" name="with" negation="false" src="d0_s3" to="o21107" />
    </statement>
    <statement id="d0_s4">
      <text>costa prominent, flexuose, ending before apex, sometimes with short lateral spurs, guide cells often in 2 rows near base, 1 row toward apex.</text>
      <biological_entity id="o21109" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuose" value_original="flexuose" />
      </biological_entity>
      <biological_entity id="o21110" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="lateral" id="o21111" name="spur" name_original="spurs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="guide" id="o21112" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o21113" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21114" name="base" name_original="base" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o21115" name="row" name_original="row" src="d0_s4" type="structure" />
      <biological_entity id="o21116" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o21109" id="r3033" name="ending before" negation="false" src="d0_s4" to="o21110" />
      <relation from="o21109" id="r3034" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o21111" />
      <relation from="o21112" id="r3035" name="in" negation="false" src="d0_s4" to="o21113" />
      <relation from="o21113" id="r3036" name="near" negation="false" src="d0_s4" to="o21114" />
      <relation from="o21115" id="r3037" name="toward" negation="false" src="d0_s4" to="o21116" />
    </statement>
    <statement id="d0_s5">
      <text>Perigonia sessile;</text>
      <biological_entity id="o21117" name="perigonium" name_original="perigonia" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer leaves short, narrowed to insertion, apex slightly flaring, inner leaves concave basally, scarcely longer than antheridia, margins entire, apex acute, basal laminal cells elongate, larger than distal cells, smooth.</text>
      <biological_entity constraint="outer" id="o21118" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" name="insertion" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o21119" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21120" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character constraint="than antheridia" constraintid="o21121" is_modifier="false" name="length_or_size" src="d0_s6" value="scarcely longer" value_original="scarcely longer" />
      </biological_entity>
      <biological_entity id="o21121" name="antheridium" name_original="antheridia" src="d0_s6" type="structure" />
      <biological_entity id="o21122" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21123" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o21124" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character constraint="than distal cells" constraintid="o21125" is_modifier="false" name="size" src="d0_s6" value="larger" value_original="larger" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21125" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial outer leaves symmetric, narrowed to insertion, inner leaves narrow, attenuate to long-attenuate.</text>
      <biological_entity constraint="perichaetial outer" id="o21126" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="insertion" src="d0_s7" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21127" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s7" to="long-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 1–1.5 cm.</text>
      <biological_entity id="o21128" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule 3.5–5 mm.</text>
      <biological_entity id="o21129" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mineral or humid soil, slopes of ravines, bark at tree bases</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mineral" />
        <character name="habitat" value="humid soil" />
        <character name="habitat" value="slopes" constraint="of ravines" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="tree bases" modifier="bark at" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S., Ont.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.; Asia (China, Japan, Korea, Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The range of Arrhenopterum heterostichum in North America corresponds largely to the range of temperate deciduous or mixed deciduous-conifer forest, ranging westward into forested portions of the tallgrass prairie.</discussion>
  
</bio:treatment>