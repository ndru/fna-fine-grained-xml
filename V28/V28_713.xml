<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="(Hampe) Hampe" date="unknown" rank="genus">sciuro-hypnum</taxon_name>
    <taxon_name authority="(Schimper) Ignatov &amp; Huttunen" date="2003" rank="species">glaciale</taxon_name>
    <place_of_publication>
      <publication_title>Arctoa</publication_title>
      <place_in_publication>11: 270. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus sciuro-hypnum;species glaciale</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099414</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">glaciale</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>6: 15, plate 542. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus brachythecium;species glaciale</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, in loose tufts, green to yellowish or grayish.</text>
      <biological_entity id="o22404" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellowish or grayish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22405" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o22404" id="r3194" name="in" negation="false" src="d0_s0" to="o22405" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 6 cm, creeping or arching, terete-foliate, irregularly branched, branches to 8 mm, straight to curved, terete-foliate.</text>
      <biological_entity id="o22406" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o22407" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="8" to_unit="mm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s1" to="curved" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves appressed or erect, closely imbricate, ovate-triangular, concave, not or slightly plicate, 1–1.5 (–1.8) × 0.6–0.9 (–1.1) mm;</text>
      <biological_entity id="o22408" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not; slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base short-decurrent, inconspicuous;</text>
      <biological_entity id="o22409" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins recurved proximally, plane distally, serrulate almost throughout;</text>
      <biological_entity id="o22410" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="almost throughout; throughout" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex gradually tapered, acute or short-acuminate;</text>
      <biological_entity id="o22411" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 40–70% leaf length, weak to strong, terminal abaxial spine absent;</text>
      <biological_entity id="o22412" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o22413" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="40-70%" name="character" src="d0_s6" value="length" value_original="length" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s6" to="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells 15 × 10 µm, walls as thick as in laminal cells, region conspicuous, opaque or weakly pellucid;</text>
      <biological_entity constraint="terminal abaxial" id="o22414" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22415" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="um" value="15" value_original="15" />
        <character name="width" src="d0_s7" unit="um" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o22416" name="wall" name_original="walls" src="d0_s7" type="structure" />
      <biological_entity id="o22417" name="laminal" name_original="laminal" src="d0_s7" type="structure" />
      <biological_entity id="o22418" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o22419" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="opaque" value_original="opaque" />
        <character is_modifier="false" modifier="weakly" name="coloration" src="d0_s7" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <relation from="o22416" id="r3195" name="in" negation="false" src="d0_s7" to="o22417" />
      <relation from="o22416" id="r3196" name="in" negation="false" src="d0_s7" to="o22418" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells elongate, 30–70 × 6–11 µm; basal juxtacostal cells not clearly differentiated, slightly shorter than laminal cells, region in 3–6 rows.</text>
      <biological_entity constraint="laminal" id="o22420" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s8" to="70" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s8" to="11" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o22421" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o22422" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not clearly" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
        <character constraint="than laminal cells" constraintid="o22423" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity constraint="laminal" id="o22423" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o22424" name="region" name_original="region" src="d0_s8" type="structure" />
      <biological_entity id="o22425" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <relation from="o22424" id="r3197" name="in" negation="false" src="d0_s8" to="o22425" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovatelanceolate to lanceolate.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="branch" id="o22426" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s9" to="lanceolate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta orangebrown, 1–1.7 cm, rough.</text>
      <biological_entity id="o22427" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="orangebrown" value_original="orangebrown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="1.7" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined to horizontal, orangebrown, ovate, curved dorsally, to 1.8 mm.</text>
      <biological_entity id="o22428" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s12" to="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="dorsally" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 12–16 µm.</text>
      <biological_entity id="o22429" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s13" to="16" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock, near glaciers, open, cold habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="glaciers" />
        <character name="habitat" value="open" />
        <character name="habitat" value="cold habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Wyo.; s South America; n Europe; n Asia; Antarctic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
        <character name="distribution" value="Antarctic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In North America Sciuro-hypnum glaciale is confirmed in Greenland and high mountains in Wyoming, but probably occurs also in other regions of the North American Arctic. The species can be recognized by its broadly ovate-triangular and concave leaves forming closely imbricate foliage. In high mountains of the West, the species can be confused with S. oedipodium, but the latter species has less concave leaves with longer acumina and conspicuous decurrencies. Small-leaved plants of S. latifolium are superficially very similar to S. glaciale, but are distinct in their sharply delimited alar group of pellucid cells and entire leaf margins. The leaves are broadest at 1/7–1/3 the leaf length, with alar regions that reach from the margins halfway to the costa.</discussion>
  
</bio:treatment>