<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">348</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leskeaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">PSEUDOLESKEA</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>5: 147, plates 477, 478. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leskeaceae;genus PSEUDOLESKEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudes, false, and genus Leskea</other_info_on_name>
    <other_info_on_name type="fna_id">127120</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in thin to thick mats, green, yellow-green, orange-green, or gold-green, brown with age.</text>
      <biological_entity id="o22827" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="orange-green" value_original="orange-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gold-green" value_original="gold-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="orange-green" value_original="orange-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gold-green" value_original="gold-green" />
        <character constraint="with age" constraintid="o22829" is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22828" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character char_type="range_value" from="thin" is_modifier="true" name="width" src="d0_s0" to="thick" />
      </biological_entity>
      <biological_entity id="o22829" name="age" name_original="age" src="d0_s0" type="structure" />
      <relation from="o22827" id="r3254" name="in" negation="false" src="d0_s0" to="o22828" />
    </statement>
    <statement id="d0_s1">
      <text>Stems irregularly branched;</text>
      <biological_entity id="o22830" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia many, few on older stems, or sometimes absent, filamentous to foliose, cells smooth or prorulose;</text>
      <biological_entity id="o22831" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="many" value_original="many" />
        <character constraint="on stems" constraintid="o22832" is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="texture" src="d0_s2" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliose" value_original="foliose" />
      </biological_entity>
      <biological_entity id="o22832" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22833" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s2" value="prorulose" value_original="prorulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rhizoids in clusters arising from base of stem-leaves.</text>
      <biological_entity id="o22834" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure" />
      <biological_entity id="o22835" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o22836" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure" />
      <relation from="o22834" id="r3255" name="in" negation="false" src="d0_s3" to="o22835" />
      <relation from="o22835" id="r3256" name="part_of" negation="false" src="d0_s3" to="o22836" />
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves similar.</text>
      <biological_entity id="o22837" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="branch" id="o22838" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves appressed to julaceous when dry, erect-spreading when moist, ovate to lanceolate, weakly to strongly plicate on either side of costa;</text>
      <biological_entity id="o22839" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s5" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s5" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o22840" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o22841" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <relation from="o22840" id="r3257" name="part_of" negation="false" src="d0_s5" to="o22841" />
    </statement>
    <statement id="d0_s6">
      <text>margins recurved proximally, entire to serrulate distally, limbidium present or absent;</text>
      <biological_entity id="o22842" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" modifier="distally" name="architecture_or_shape" src="d0_s6" to="serrulate" />
      </biological_entity>
      <biological_entity id="o22843" name="limbidium" name_original="limbidium" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex abruptly acute to long-acuminate, hairpoint present or absent;</text>
      <biological_entity id="o22844" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="abruptly acute" name="shape" src="d0_s7" to="long-acuminate" />
      </biological_entity>
      <biological_entity id="o22845" name="hairpoint" name_original="hairpoint" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa single, moderately strong, 2/3 leaf length to subpercurrent, opaque, not or weakly sinuate;</text>
      <biological_entity id="o22846" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character is_modifier="false" modifier="moderately" name="fragility" src="d0_s8" value="strong" value_original="strong" />
        <character name="quantity" src="d0_s8" value="2/3" value_original="2/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>alar cells transversely elongate, quadrate, or short-rectangular;</text>
      <biological_entity id="o22847" name="leaf" name_original="leaf" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="opaque" value_original="opaque" />
        <character is_modifier="false" modifier="not; weakly" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o22848" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells similar to medial cells, usually shorter, 1–4: 1, walls pitted;</text>
      <biological_entity constraint="proximal laminal" id="o22849" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" notes="" src="d0_s10" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="medial" id="o22850" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <biological_entity id="o22851" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o22849" id="r3258" name="to" negation="false" src="d0_s10" to="o22850" />
    </statement>
    <statement id="d0_s11">
      <text>medial cells quadrate to elongate-rhomboidal, 1–4: 1, 1-papillose to prorate, walls firm to thin;</text>
      <biological_entity constraint="medial" id="o22852" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s11" to="elongate-rhomboidal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character constraint="to prorate" constraintid="o22853" is_modifier="false" name="relief" src="d0_s11" value="1-papillose" value_original="1-papillose" />
      </biological_entity>
      <biological_entity id="o22853" name="prorate" name_original="prorate" src="d0_s11" type="structure" />
      <biological_entity id="o22854" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="firm" value_original="firm" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apical cells elongate.</text>
      <biological_entity constraint="medial" id="o22855" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s12" to="elongate-rhomboidal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="apical" id="o22856" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="development" src="d0_s13" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perichaetial leaves pale translucent, erect when moist, longer, apex more acuminate, costa percurrent.</text>
      <biological_entity constraint="perichaetial" id="o22857" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale translucent" value_original="pale translucent" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o22858" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o22859" name="costa" name_original="costa" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="percurrent" value_original="percurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seta 0.8–1.2 cm.</text>
      <biological_entity id="o22860" name="seta" name_original="seta" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s16" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsule inclined, cylindric, asymmetric or symmetric;</text>
      <biological_entity id="o22861" name="capsule" name_original="capsule" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>annulus absent;</text>
      <biological_entity id="o22862" name="annulus" name_original="annulus" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum smooth conic (conic-apiculate in P. baileyi);</text>
      <biological_entity id="o22863" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s19" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>peristome well developed;</text>
      <biological_entity id="o22864" name="peristome" name_original="peristome" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s20" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>exostome teeth lanceolate, striate to papillose;</text>
      <biological_entity constraint="exostome" id="o22865" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="striate" name="relief" src="d0_s21" to="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endostome basal membrane high, segments slender-lanceolate to filiform, cilia well developed to short and rudimentary.</text>
      <biological_entity id="o22866" name="endostome" name_original="endostome" src="d0_s22" type="structure" />
      <biological_entity constraint="basal" id="o22867" name="membrane" name_original="membrane" src="d0_s22" type="structure">
        <character is_modifier="false" name="height" src="d0_s22" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o22868" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character char_type="range_value" from="slender-lanceolate" name="shape" src="d0_s22" to="filiform" />
      </biological_entity>
      <biological_entity id="o22869" name="cilium" name_original="cilia" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s22" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Spores 10–28 µm, papillose.</text>
      <biological_entity id="o22870" name="spore" name_original="spores" src="d0_s23" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s23" to="28" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s23" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 12 (8 in the flora).</discussion>
  <discussion>Pseudoleskea are Northern Hemisphere species of montane to alpine regions found on rock and soil, typically in cool to cold climates. Pseudoleskea differs from Lescuraea in the shorter and mostly thicker-walled laminal cells and in the mostly perfect hypnoid peristome, showing some reduction in a few species. The stem leaves are weakly to strongly concave; the capsule is red to red-brown with bordered exostome teeth and 1–3 endostome cilia. J. R. Rohrer (1986) compared differences among the taxa. Pseudoleskea baileyi does not belong in the genus and appears to be closest morphologically to the eastern Asian Rigodiadelphus Dixon (M. U. Krieger 2002); it is included here for completeness.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Medial laminal cells quadrate to elongate, papillose over lumen</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Medial laminal cells usually rhomboidal, prorate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf apices slenderly long-acuminate; medial laminal cells elongate-rhomboidal to fusiform or vermicular, most papillae off-centered on distal cell lumina; on bark and twigs of shrubs and trees.</description>
      <determination>7 Pseudoleskea stenophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf apices short-acuminate to acute; medial laminal cells quadrate to isodiametric, papillae usually centered over lumina; on soil or rock, never corticolous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves appressed to julaceous when dry, usually larger than 0.8 mm; margins recurved proximally; laminal cells opaque, papillae low to large, not distinctly pointed-mammillose; costae with terminal projections short, not abaxially strongly dentate.</description>
      <determination>4 Pseudoleskea patens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves catenulate, weakly incurved when dry, usually smaller than 0.8 mm; margins plane; laminal cells pellucid, papillae large, distinctly pointed-mammillose; costae with terminal projections strong, abaxially strongly dentate.</description>
      <determination>8 Pseudoleskea tribulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf hair-points present; paraphyllia absent; costae ending in acumen but not percurrent; stems with central strand absent.</description>
      <determination>2 Pseudoleskea baileyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf hair-points absent; paraphyllia present; costae usually percurrent; stems with central strand present</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaf medial laminal cell walls distinctly and strongly pitted.</description>
      <determination>1 Pseudoleskea atricha</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaf medial laminal cell walls not pitted</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Juxtacostal laminal cells somewhat longer than distal cells; medial laminal cells strongly prorate; margins broadly recurved to acumen; apices long- acuminate.</description>
      <determination>6 Pseudoleskea saviana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Juxtacostal laminal cells somewhat shorter than distal cells; medial laminal cells prorate; margins narrowly recurved to mid leaf or near acumen; apices acute to short- or rarely long-acuminate</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Apical cells 1-2:1; medial cell lumina smaller than 8 µm, walls incrassate, cell shape heterogeneous.</description>
      <determination>3 Pseudoleskea incurvata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Apical cells 2-3:1; medial cell lumina larger than 10 µm, walls thin, cell shape homogeneous.</description>
      <determination>5 Pseudoleskea radicosa</determination>
    </key_statement>
  </key>
</bio:treatment>