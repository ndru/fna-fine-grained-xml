<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
    <other_info_on_meta type="illustration_page">36</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="genus">amphidium</taxon_name>
    <taxon_name authority="(Hampe ex Müller Hal.) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>215[I,3]: 460. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus amphidium;species californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250061821</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zygodon</taxon_name>
    <taxon_name authority="Hampe ex Müller Hal." date="unknown" rank="species">californicus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>20: 361. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus zygodon;species californicus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–4 cm.</text>
      <biological_entity id="o16956" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves linear-lanceolate, 1.5–4 mm;</text>
      <biological_entity id="o16957" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins recurved, toothed or entire proximally, toothed or notched distally;</text>
      <biological_entity id="o16958" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s2" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal laminal cells yellowish, walls thick;</text>
      <biological_entity constraint="basal laminal" id="o16959" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o16960" name="wall" name_original="walls" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cells 9–11 µm, slightly obscured by papillae, papillae elliptic, small.</text>
      <biological_entity constraint="distal" id="o16961" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s4" to="11" to_unit="um" />
        <character constraint="by papillae" constraintid="o16962" is_modifier="false" modifier="slightly" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o16962" name="papilla" name_original="papillae" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o16963" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perichaetial leaves elongate, narrow, base subsheathing, apex long-pointed.</text>
      <biological_entity constraint="perichaetial" id="o16964" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o16965" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="subsheathing" value_original="subsheathing" />
      </biological_entity>
      <biological_entity id="o16966" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="long-pointed" value_original="long-pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta 1.5–2 mm.</text>
      <biological_entity id="o16967" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule immersed to emergent, 1.2–1.6 mm;</text>
      <biological_entity id="o16968" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="location" src="d0_s8" value="emergent" value_original="emergent" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>operculum conic-apiculate, less than diam. of capsule.</text>
      <biological_entity id="o16969" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="conic-apiculate" value_original="conic-apiculate" />
      </biological_entity>
      <biological_entity id="o16970" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="diam" value_original="diam" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 10–15 µm.</text>
      <biological_entity id="o16971" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s10" to="15" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of neutral to acidic rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of neutral to acidic rock" />
        <character name="habitat" value="acidic rock" modifier="neutral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Amphidium californicum is distinguished by the linear leaves with notched distal margins. Amphidium lapponicum has decidedly broader leaves and distal cells obscured by large, warty, cuticular papillae.</discussion>
  
</bio:treatment>