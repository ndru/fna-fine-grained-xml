<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alain Vanderpoorten</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">301</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Loeske" date="unknown" rank="genus">Hygroamblystegium</taxon_name>
    <place_of_publication>
      <publication_title>Moosfl. Harz.,</publication_title>
      <place_in_publication>298. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus Hygroamblystegium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hygros, wet, and genus Amblystegium</other_info_on_name>
    <other_info_on_name type="fna_id">116031</other_info_on_name>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, yellow to dark green or blackish.</text>
      <biological_entity id="o3034" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="dark green or blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems irregularly and often freely branched;</text>
      <biological_entity id="o3035" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often freely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand usually present;</text>
      <biological_entity id="o3036" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o3037" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia often present, ± lanceolate;</text>
      <biological_entity id="o3038" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids on stem, never from abaxial costa surface, often forming tomentum, sparsely branched, smooth;</text>
      <biological_entity id="o3039" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; sparsely" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3040" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="costa" id="o3041" name="surface" name_original="surface" src="d0_s4" type="structure" constraint_original="abaxial costa" />
      <biological_entity id="o3042" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o3039" id="r390" name="on" negation="false" src="d0_s4" to="o3040" />
      <relation from="o3039" id="r391" modifier="never" name="from" negation="false" src="d0_s4" to="o3041" />
      <relation from="o3039" id="r392" modifier="often" name="forming" negation="false" src="d0_s4" to="o3042" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hairs well developed and many, or small, delicate, sparse, distal cells 1 or 2, hyaline.</text>
      <biological_entity constraint="axillary" id="o3043" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s5" value="developed" value_original="developed" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3044" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem and branch leaves erect, not or slightly falcate-secund, oblong-lanceolate to oblong-ovate, rarely broadly ovate, not plicate, to or longer than 1 mm;</text>
      <biological_entity id="o3045" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not; slightly" name="arrangement" src="d0_s6" value="falcate-secund" value_original="falcate-secund" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s6" to="oblong-ovate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character modifier="longer than" name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="branch" id="o3046" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not; slightly" name="arrangement" src="d0_s6" value="falcate-secund" value_original="falcate-secund" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s6" to="oblong-ovate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character modifier="longer than" name="some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base not decurrent;</text>
      <biological_entity id="o3047" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane, entire to denticulate, limbidia absent;</text>
      <biological_entity id="o3048" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s8" to="denticulate" />
      </biological_entity>
      <biological_entity id="o3049" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex gradually acuminate, acute, or obtuse, acumen plane or furrowed;</text>
      <biological_entity id="o3050" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o3051" name="acumen" name_original="acumen" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa single, ending mid leaf to excurrent, broad, often curved beyond mid leaf;</text>
      <biological_entity constraint="mid" id="o3053" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
      <biological_entity constraint="mid" id="o3054" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
      <relation from="o3052" id="r393" name="ending" negation="false" src="d0_s10" to="o3053" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells not or poorly differentiated, rectangular to quadrate, not or slightly inflated, green to brownish orange, walls thin or slightly incrassate in mature leaves, region indistinct, ovate, along basal margins;</text>
      <biological_entity id="o3052" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character constraint="beyond mid leaf" constraintid="o3054" is_modifier="false" modifier="often" name="course" src="d0_s10" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o3055" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="poorly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s11" to="quadrate" />
        <character is_modifier="false" modifier="not; slightly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s11" to="brownish orange" />
      </biological_entity>
      <biological_entity id="o3056" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character constraint="in leaves" constraintid="o3057" is_modifier="false" modifier="slightly" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o3057" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o3058" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3059" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <relation from="o3058" id="r394" name="along" negation="false" src="d0_s11" to="o3059" />
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cells rhombic to oblong-hexagonal, (9–) 13–65 (–67) µm, smooth.</text>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="medial laminal" id="o3060" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s12" to="oblong-hexagonal" />
        <character char_type="range_value" from="9" from_unit="um" name="atypical_some_measurement" src="d0_s12" to="13" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s12" to="67" to_unit="um" />
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s12" to="65" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule inclined, cylindric to oblong-cylindric, arcuate;</text>
      <biological_entity id="o3061" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="oblong-cylindric" />
        <character is_modifier="false" name="course_or_shape" src="d0_s14" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome perfect;</text>
      <biological_entity id="o3062" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="perfect" value_original="perfect" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth bordered;</text>
      <biological_entity constraint="exostome" id="o3063" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome cilia in groups of 1–3, nodulose to appendiculate.</text>
      <biological_entity constraint="endostome" id="o3064" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="of 1-3" name="shape" notes="" src="d0_s17" value="nodulose" value_original="nodulose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <biological_entity id="o3065" name="group" name_original="groups" src="d0_s17" type="structure" />
      <relation from="o3064" id="r395" name="in" negation="false" src="d0_s17" to="o3065" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 12–45 µm.</text>
      <biological_entity id="o3066" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s18" to="45" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Eurasia, s Africa, Pacific Islands (New Zealand), Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Hygroamblystegium is morphologically highly variable; conflicting taxonomic treatments arose from different interpretations of character significance. The species is mostly aquatic to subaquatic; a substantial proportion of the morphological variation is purely plastic. None of the species form monophyletic groups in molecular phylogenetic analyses. All exhibit various ploidy levels, and Hygroamblystegium can be compared to a complex of agamospermous vascular plants, where the wide range of genetic variability within apparent species of polyphyletic origin are retained in the face of fast and independent evolution of polyploid genomes, unlikely to be associated with any pattern of morphological variation. All species names previously recognized in the flora area are considered here as synonyms of H. varium. Some traits are indicative of habitat conditions. Although plants with convergent morphologies do not form monophyletic lineages and should thus not be given species status, their recognition at an infra-specific level is supported ecologically. These infra-specific taxa correspond to terrestrial plants of wet meadows, fens, and marshes (var. humile), and aquatic plants of oligotrophic, calcareous springs, which, owing to their endemic occurrence in North America, may be recognized at the subspecies level, namely subsp. noterophilum. Although phylogenetically unrelated, Hygroamblystegium sometimes bears strong resemblance to Cratoneuron filicinum (Hedwig) Spruce. The best example of this convergence has been called H. tenax var. spinifolium (Schimper) Jennings, a morphologically well-characterized taxon of large plants with long-excurrent, thick costae. Despite the lack of inflated, hyaline alar cells and the strictly straight leaves, this is an aquatic expression of C. filicinum in oligotrophic, calcareous spring areas.</discussion>
  <references>
    <reference>Vanderpoorten, A. 2004. A simple taxonomic treatment for a complicated evolutionary story: The genus Hygroamblystegium. Monogr. Syst. Bot., Missouri Bot. Gard. 98: 320–327.</reference>
    <reference>Vanderpoorten, A., A. J. Shaw, and C. J. Cox. 2004. Evolution of multiple paralogous adenosine kinase genes in the moss genus Hygroamblystegium: Phylogenetic implications. Molec. Phylogen. Evol. 31: 505–516.</reference>
  </references>
  
</bio:treatment>