<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">33</other_info_on_meta>
    <other_info_on_meta type="illustration_page">34</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">meesiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">meesia</taxon_name>
    <taxon_name authority="(Linnaeus ex Jolyclerc) Ångström" date="1844" rank="species">triquetra</taxon_name>
    <place_of_publication>
      <publication_title>Nova Acta Regiae Soc. Sci. Upsal.</publication_title>
      <place_in_publication>12: 357. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family meesiaceae;genus meesia;species triquetra</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200001545</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="Linnaeus ex Jolyclerc" date="unknown" rank="species">triquetrum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Sex. Vég.,</publication_title>
      <place_in_publication>749. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mnium;species triquetrum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–1.2 cm.</text>
      <biological_entity id="o17198" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s0" to="1.2" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves contorted when dry, widespreading and distinctly 3-ranked when moist, ovatelanceolate, 1.5–4 mm;</text>
      <biological_entity id="o17199" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" modifier="when moist" name="arrangement" src="d0_s1" value="3-ranked" value_original="3-ranked" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>base decurrent;</text>
      <biological_entity id="o17200" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane to weakly reflexed proximally, serrulate distally;</text>
      <biological_entity id="o17201" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="weakly; proximally" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex acute;</text>
      <biological_entity id="o17202" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa narrow, ending in or just before apex;</text>
      <biological_entity id="o17203" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o17204" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o17203" id="r2478" name="ending in" negation="false" src="d0_s5" to="o17204" />
    </statement>
    <statement id="d0_s6">
      <text>inner laminal cells smaller, walls thicker than marginal cells.</text>
      <biological_entity constraint="inner laminal" id="o17205" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o17207" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o17206" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character constraint="than marginal cells" constraintid="o17207" is_modifier="false" name="width" src="d0_s6" value="thicker" value_original="thicker" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 3–10 cm.</text>
      <biological_entity id="o17208" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule 3–4.5 mm.</text>
      <biological_entity id="o17209" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 33–38 µm.</text>
      <biological_entity id="o17210" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="33" from_unit="um" name="some_measurement" src="d0_s10" to="38" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich fens, arctic and boreal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich fens" />
        <character name="habitat" value="arctic" />
        <character name="habitat" value="boreal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Ont., Que., Yukon; Alaska, Calif., Mich., Minn., Mont., Nebr., N.J., N.Y., Oreg., S.Dak., Vt.; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meesia triquetra is frequent where appropriate habitat is available, and disjunct in a few locations farther south. The species is usually easily distinguished by its three-ranked, widely spreading leaves that have serrulate margins. The sexual condition is dioicous.</discussion>
  
</bio:treatment>