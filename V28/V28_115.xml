<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="D. Mohr" date="unknown" rank="genus">ulota</taxon_name>
    <taxon_name authority="(Hooker &amp; Greville) Bridel" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 299. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus ulota;species drummondii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250062018</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthotrichum</taxon_name>
    <taxon_name authority="Hooker &amp; Greville" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>in R. K. Greville, Scott. Crypt. Fl.</publication_title>
      <place_in_publication>2: plate 115. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthotrichum;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ulota</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">bicolor</taxon_name>
    <taxon_hierarchy>genus ulota;species bicolor</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">U.</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="species">funstonii</taxon_name>
    <taxon_hierarchy>genus u.;species funstonii</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–1.2 cm.</text>
      <biological_entity id="o17988" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s0" to="1.2" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o17989" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves flexuose to ± contorted-flexuose, loosely erect when dry, lanceolate, 1.7–3.7 mm;</text>
      <biological_entity id="o17990" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="flexuose" name="course" src="d0_s2" to="more or less contorted-flexuose" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="distance" src="d0_s2" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base ovate;</text>
      <biological_entity id="o17991" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or slightly reflexed;</text>
      <biological_entity id="o17992" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex obtuse;</text>
      <biological_entity id="o17993" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal laminal cells rounded to elliptic;</text>
      <biological_entity constraint="basal laminal" id="o17994" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells 7–11 µm, smooth or slightly papillose, papillae low, simple.</text>
      <biological_entity constraint="distal" id="o17995" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s7" to="11" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o17996" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="low" value_original="low" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves not differentiated from stem-leaves.</text>
      <biological_entity constraint="perichaetial" id="o17997" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="from stem-leaves" constraintid="o17998" is_modifier="false" modifier="not" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o17998" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seta 5–5.5 mm.</text>
      <biological_entity id="o17999" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule cylindric and contracted at mouth when mature, fusiform to fusiform-cylindric when old and dry, 1.7–3.2 mm, strongly 8-ribbed 1/2–2/3 length, mouth small, distinctly smaller than mid capsule;</text>
      <biological_entity id="o18000" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character constraint="at mouth" constraintid="o18001" is_modifier="false" name="condition_or_size" src="d0_s12" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o18001" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character char_type="range_value" from="fusiform" modifier="when old and dry" name="shape" src="d0_s12" to="fusiform-cylindric" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s12" value="8-ribbed" value_original="8-ribbed" />
        <character char_type="range_value" from="1/2" name="length" src="d0_s12" to="2/3" />
      </biological_entity>
      <biological_entity id="o18002" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="small" value_original="small" />
        <character constraint="than mid capsule" constraintid="o18003" is_modifier="false" name="size" src="d0_s12" value="distinctly smaller" value_original="distinctly smaller" />
      </biological_entity>
      <biological_entity constraint="mid" id="o18003" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stomata in neck and basal capsule;</text>
      <biological_entity id="o18004" name="stoma" name_original="stomata" src="d0_s13" type="structure" />
      <biological_entity id="o18005" name="neck" name_original="neck" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o18006" name="capsule" name_original="capsule" src="d0_s13" type="structure" />
      <relation from="o18004" id="r2585" name="in" negation="false" src="d0_s13" to="o18005" />
      <relation from="o18004" id="r2586" name="in" negation="false" src="d0_s13" to="o18006" />
    </statement>
    <statement id="d0_s14">
      <text>peristome single;</text>
      <biological_entity id="o18007" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth split to 16, erect, flexuose-incurved, obscurely and densely papillose;</text>
      <biological_entity constraint="teeth" id="o18008" name="split" name_original="split" src="d0_s15" type="structure" constraint_original="exostome teeth">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="flexuose-incurved" value_original="flexuose-incurved" />
        <character is_modifier="false" modifier="obscurely; densely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome segments absent.</text>
      <biological_entity constraint="endostome" id="o18009" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra conic, very hairy.</text>
      <biological_entity id="o18010" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="very" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 21–24 µm.</text>
      <biological_entity id="o18011" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="21" from_unit="um" name="some_measurement" src="d0_s18" to="24" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Twigs and trunks of conifer and deciduous trees, dense coastal forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="twigs" constraint="of conifer and deciduous trees , dense coastal forests" />
        <character name="habitat" value="trunks" constraint="of conifer and deciduous trees , dense coastal forests" />
        <character name="habitat" value="conifer" />
        <character name="habitat" value="deciduous trees" />
        <character name="habitat" value="dense coastal forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Nfld. and Labr. (Nfld.), Que.; Alaska; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">drummondi</other_name>
  <discussion>The distribution of Ulota drummondii is bicentric; it is known from both coasts in North America, from Newfoundland south to Cape Breton and the Gaspé in the East, and from along the Aleutians south to the Queen Charlotte Islands in the West. This species is distinguished by its narrowly fusiform capsules that are ribbed almost their entire length and are somewhat smaller at the mouth. The whitish single peristome is erect and flexuose and easily distinguishes U. drummondii from U. crispa which has a reflexed exostome. Additionally, the leaves of U. drummondii are blunt and not strongly twisted-contorted.</discussion>
  
</bio:treatment>