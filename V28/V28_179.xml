<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">philonotis</taxon_name>
    <taxon_name authority="(Hedwig) Bridel" date="1827" rank="species">fontana</taxon_name>
    <taxon_name authority="(Turner) Bridel" date="1827" rank="variety">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 20. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus philonotis;species fontana;variety pumila</taxon_hierarchy>
    <other_info_on_name type="fna_id">250062092</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bartramia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fontana</taxon_name>
    <taxon_name authority="Turner" date="unknown" rank="variety">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Hibern. Spic.,</publication_title>
      <place_in_publication>107, plate 10, fig. 1. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bartramia;species fontana;variety pumila</taxon_hierarchy>
  </taxon_identification>
  <number>11c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small.</text>
      <biological_entity id="o18512" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–6 cm.</text>
      <biological_entity id="o18513" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves with distalmost not spiraled around stem, stiffly erect, sometimes somewhat secund, crowded to distant, lanceolate, not plicate.</text>
      <biological_entity id="o18514" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes somewhat" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character char_type="range_value" from="crowded" name="arrangement" src="d0_s2" to="distant" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o18515" name="spiraled" name_original="spiraled" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="not" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
      </biological_entity>
      <biological_entity id="o18516" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o18514" id="r2653" name="with" negation="false" src="d0_s2" to="o18515" />
      <relation from="o18515" id="r2654" name="around" negation="false" src="d0_s2" to="o18516" />
    </statement>
    <statement id="d0_s3">
      <text>Capsule 1–2 mm.</text>
      <biological_entity id="o18517" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seepage slopes, along creeks, clay, silt, intermixed with other bryophytes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepage slopes" constraint="along creeks" />
        <character name="habitat" value="creeks" modifier="along" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="silt" />
        <character name="habitat" value="other bryophytes" modifier="intermixed with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.S., N.W.T., Nunavut, Ont., Que., Yukon; Alaska, Calif., Colo., Minn., Mont., Oreg., Tenn., Wash., Wyo.; c, n Europe; c, sw Asia; Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety pumila is diminutive and typically grows in dense mats or sods; the stems are tightly interlaced with tomentum. The stiffly erect leaves that are neither catenulate nor spiraled aid in its identification. The range is arctic-alpine. This variety is a characteristic member of bog communities throughout the Arctic tundra and taiga.</discussion>
  
</bio:treatment>