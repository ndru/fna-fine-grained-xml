<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="illustration_page">87</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hedwigiaceae</taxon_name>
    <taxon_name authority="P. Beauvois" date="unknown" rank="genus">hedwigia</taxon_name>
    <taxon_name authority="Hedenas" date="1994" rank="species">stellata</taxon_name>
    <place_of_publication>
      <publication_title>J. Bryol.</publication_title>
      <place_in_publication>18: 144, figs. 1 – 2. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hedwigiaceae;genus hedwigia;species stellata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099138</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1.5–2.5 mm;</text>
      <biological_entity id="o24386" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s0" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>margins recurved to mid leaf, plane mid leaf, erect to incurved in acumen, teeth in apex sharply, narrowly spinose, smooth;</text>
      <biological_entity id="o24387" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="mid" id="o24388" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity constraint="mid" id="o24389" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character char_type="range_value" constraint="in acumen" constraintid="o24390" from="erect" name="orientation" src="d0_s1" to="incurved" />
      </biological_entity>
      <biological_entity id="o24390" name="acumen" name_original="acumen" src="d0_s1" type="structure" />
      <biological_entity id="o24391" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture_or_shape" notes="" src="d0_s1" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o24392" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <relation from="o24391" id="r3453" name="in" negation="false" src="d0_s1" to="o24392" />
    </statement>
    <statement id="d0_s2">
      <text>apex widespreading to squarrose when dry, broad, hyaline, acumen broad-based, 1/5–1/3 (–1/2) leaf length, shallowly to deeply channeled, extreme apex flat or sometimes twisted;</text>
      <biological_entity id="o24393" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="widespreading" modifier="when dry" name="orientation" src="d0_s2" to="squarrose" />
        <character is_modifier="false" name="width" src="d0_s2" value="broad" value_original="broad" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o24394" name="acumen" name_original="acumen" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="broad-based" value_original="broad-based" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s2" to="1/3-1/2" />
      </biological_entity>
      <biological_entity id="o24395" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="shallowly to deeply" name="length" src="d0_s2" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o24396" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>medial and distal laminal cells with papillae 1 (or 2), large, stalked, strongly, irregularly branched, branches of unequal lengths, especially on abaxial surface;</text>
      <biological_entity constraint="medial and distal laminal" id="o24397" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s3" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stalked" value_original="stalked" />
        <character is_modifier="false" modifier="strongly; irregularly" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o24398" name="papilla" name_original="papillae" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24399" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o24400" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o24397" id="r3454" name="with" negation="false" src="d0_s3" to="o24398" />
      <relation from="o24399" id="r3455" modifier="of unequal lengths; especially" name="on" negation="false" src="d0_s3" to="o24400" />
    </statement>
    <statement id="d0_s4">
      <text>laminal cells in hyaline area with papillae large, spinose, intermixed with smaller papillae;</text>
      <biological_entity constraint="laminal" id="o24401" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="spinose" value_original="spinose" />
      </biological_entity>
      <biological_entity id="o24402" name="area" name_original="area" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o24403" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o24404" name="papilla" name_original="papillae" src="d0_s4" type="structure" />
      <relation from="o24401" id="r3456" name="in" negation="false" src="d0_s4" to="o24402" />
      <relation from="o24402" id="r3457" name="with" negation="false" src="d0_s4" to="o24403" />
      <relation from="o24401" id="r3458" name="intermixed with" negation="false" src="d0_s4" to="o24404" />
    </statement>
    <statement id="d0_s5">
      <text>apical cell long-linear, sharply pointed, 120–175 (–200) µm, smooth, 1-papillose on one margin, or 2-papillose and appearing 2-fid at tip.</text>
      <biological_entity constraint="apical" id="o24405" name="cell" name_original="cell" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="long-linear" value_original="long-linear" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s5" value="pointed" value_original="pointed" />
        <character char_type="range_value" from="175" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s5" to="200" to_unit="um" />
        <character char_type="range_value" from="120" from_unit="um" name="some_measurement" src="d0_s5" to="175" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character constraint="on margin" constraintid="o24406" is_modifier="false" name="relief" src="d0_s5" value="1-papillose" value_original="1-papillose" />
        <character is_modifier="false" name="relief" notes="" src="d0_s5" value="2-papillose" value_original="2-papillose" />
      </biological_entity>
      <biological_entity id="o24406" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity id="o24407" name="tip" name_original="tip" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <relation from="o24405" id="r3459" name="appearing" negation="false" src="d0_s5" to="o24407" />
    </statement>
    <statement id="d0_s6">
      <text>Perichaetia with leaves plane to weakly concave, margins long-ciliate distally, especially at apex.</text>
      <biological_entity id="o24408" name="perichaetium" name_original="perichaetia" src="d0_s6" type="structure" />
      <biological_entity id="o24409" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s6" to="weakly concave" />
      </biological_entity>
      <biological_entity id="o24410" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s6" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o24411" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o24408" id="r3460" name="with" negation="false" src="d0_s6" to="o24409" />
      <relation from="o24410" id="r3461" modifier="especially" name="at" negation="false" src="d0_s6" to="o24411" />
    </statement>
    <statement id="d0_s7">
      <text>Vaginula sparsely pilose.</text>
      <biological_entity id="o24412" name="vaginulum" name_original="vaginula" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra naked to sparsely pilose.</text>
      <biological_entity id="o24413" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry boulders, rock, open areas, outcrops in grassy woodlands, on cultivated plum trees, coastal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry boulders" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="outcrops" constraint="in grassy woodlands" />
        <character name="habitat" value="grassy woodlands" />
        <character name="habitat" value="cultivated plum trees" />
        <character name="habitat" value="coastal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; South America (Chile); Europe (Denmark, Italy, Norway, Portugal, Spain, Sweden, United Kingdom); Asia (Kazakhstan); Atlantic Islands (Faroe Islands, Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Europe (Denmark)" establishment_means="native" />
        <character name="distribution" value="Europe (Italy)" establishment_means="native" />
        <character name="distribution" value="Europe (Norway)" establishment_means="native" />
        <character name="distribution" value="Europe (Portugal)" establishment_means="native" />
        <character name="distribution" value="Europe (Spain)" establishment_means="native" />
        <character name="distribution" value="Europe (Sweden)" establishment_means="native" />
        <character name="distribution" value="Europe (United Kingdom)" establishment_means="native" />
        <character name="distribution" value="Asia (Kazakhstan)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Faroe Islands)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The marginal teeth in the hyaline leaf apices of Hedwigia stellata display the same characters as the apical cell: generally sharply pointed and narrow, with an occasional papilla; in H. ciliata the teeth are broader and often irregularly covered with scattered papillae, especially on their tips. Hedwigia stellata is common in California only in the San Francisco and Los Angeles areas (W. R. Buck and D. H. Norris 1996). The chlorophyllose, generally yellow-brown boundary between the body of the leaf and the hyaline apex is often irregular with short longitudinal lines of pigmented and non-pigmented cells juxtaposed, resulting in a jagged transverse line. In many leaves, the ultimate laminal cells are somewhat darker in coloration just at this boundary. This is true also in specimens of H. detonsa.</discussion>
  
</bio:treatment>