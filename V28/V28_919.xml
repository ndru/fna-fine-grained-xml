<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
    <other_info_on_meta type="illustration_page">583</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">cryphaeaceae</taxon_name>
    <taxon_name authority="D. Mohr in F. Weber" date="unknown" rank="genus">cryphaea</taxon_name>
    <taxon_name authority="Schimper ex Sullivant in A. Gray" date="1856" rank="species">glomerata</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 656, plate 5 [near upper right]. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cryphaeaceae;genus cryphaea;species glomerata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062396</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems with branches short-to-elongate, simple.</text>
      <biological_entity id="o9906" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o9907" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="short" name="size" src="d0_s0" to="elongate" />
      </biological_entity>
      <relation from="o9906" id="r1419" name="with" negation="false" src="d0_s0" to="o9907" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves widespreading when moist, 0.8–1.2 mm;</text>
      <biological_entity id="o9908" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>apex broadly acute;</text>
      <biological_entity id="o9909" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa ending mid leaf or in base of acumen, laterally spurred, tip ± 2-fid.</text>
      <biological_entity id="o9910" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" notes="" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity constraint="mid" id="o9911" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o9912" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o9913" name="acumen" name_original="acumen" src="d0_s3" type="structure" />
      <biological_entity id="o9914" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <relation from="o9910" id="r1420" name="ending" negation="false" src="d0_s3" to="o9911" />
      <relation from="o9910" id="r1421" name="in" negation="false" src="d0_s3" to="o9912" />
      <relation from="o9912" id="r1422" name="part_of" negation="false" src="d0_s3" to="o9913" />
    </statement>
    <statement id="d0_s4">
      <text>Perichaetia with inner leaves 1.6–1.9 mm, awn usually distinct, denticulate, 1/5–1/4 length expanded portion of leaf.</text>
      <biological_entity id="o9915" name="perichaetium" name_original="perichaetia" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o9916" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s4" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9917" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s4" to="1/4" />
      </biological_entity>
      <biological_entity id="o9918" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o9919" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <relation from="o9915" id="r1423" name="with" negation="false" src="d0_s4" to="o9916" />
      <relation from="o9918" id="r1424" name="part_of" negation="false" src="d0_s4" to="o9919" />
    </statement>
    <statement id="d0_s5">
      <text>Capsule with peristome double;</text>
      <biological_entity id="o9920" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <biological_entity id="o9921" name="peristome" name_original="peristome" src="d0_s5" type="structure" />
      <relation from="o9920" id="r1425" name="with" negation="false" src="d0_s5" to="o9921" />
    </statement>
    <statement id="d0_s6">
      <text>exostome teeth single;</text>
      <biological_entity constraint="exostome" id="o9922" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>endostome segments linear (slender).</text>
      <biological_entity constraint="endostome" id="o9923" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra broadly conic.</text>
      <biological_entity id="o9924" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores smooth or papillose.</text>
      <biological_entity id="o9925" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Sep–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Jun" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Twigs, branches, trunks of shrubs and trees, logs, rock, humid forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="twigs" />
        <character name="habitat" value="branches" />
        <character name="habitat" value="trunks" constraint="of shrubs and trees" />
        <character name="habitat" value="shrubs" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="rock" modifier="logs" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="humid forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., Ky., La., Md., Mass., Miss., Mo., N.J., N.C., Ohio, Okla., S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cryphaea glomerata often grows mixed with C. nervosa; the two can be easily distinguished under low magnification by the more narrowly pointed and plicate-appearing leaves of C. nervosa. The ranges of C. glomerata and the subtropical C. filiformis overlap in southern Florida. Cryphaea glomerata is similar to, and often occurs with, C. ravenelii, which is easily distinguished by its blunt leaf apex.</discussion>
  
</bio:treatment>