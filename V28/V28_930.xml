<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William D. Reese†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">593</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leucodontaceae</taxon_name>
    <taxon_name authority="Schwagrichen" date="unknown" rank="genus">LEUCODON</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(2): 1. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leucodontaceae;genus LEUCODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leukos, white, and odon, tooth, alluding to pale peristome teeth</other_info_on_name>
    <other_info_on_name type="fna_id">118387</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in rigid tufts, green to brownish, glossy or dull.</text>
      <biological_entity id="o18640" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18641" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
      </biological_entity>
      <relation from="o18640" id="r2674" name="in" negation="false" src="d0_s0" to="o18641" />
    </statement>
    <statement id="d0_s1">
      <text>Stems irregularly branched, usually simple.</text>
      <biological_entity id="o18642" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves ovate to ovatelanceolate;</text>
      <biological_entity id="o18643" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base not cordate, occasionally rounded, not decurrent;</text>
      <biological_entity id="o18644" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane to reflexed or revolute, entire;</text>
      <biological_entity id="o18645" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex abruptly or gradually slender-acuminate;</text>
    </statement>
    <statement id="d0_s6">
      <text>ecostate;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells quadrate, region large;</text>
      <biological_entity id="o18646" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="slender-acuminate" value_original="slender-acuminate" />
      </biological_entity>
      <biological_entity id="o18647" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o18648" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminal cells smooth or rarely slightly papillose distally over cell lumina;</text>
      <biological_entity constraint="laminal" id="o18649" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character constraint="over cell lumina" constraintid="o18650" is_modifier="false" modifier="rarely slightly" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="cell" id="o18650" name="lumen" name_original="lumina" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells oblong-rhomboidal to linear;</text>
      <biological_entity constraint="medial laminal" id="o18651" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong-rhomboidal" name="shape" src="d0_s9" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apical cells smooth or papillose, or rarely so abaxially.</text>
      <biological_entity constraint="apical" id="o18652" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Branch leaves imbricate, erect-appressed when dry, ascending to spreading when moist, ovatelanceolate, somewhat concave, larger than stem-leaves;</text>
      <biological_entity constraint="branch" id="o18653" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s11" value="erect-appressed" value_original="erect-appressed" />
        <character char_type="range_value" from="ascending" modifier="when moist" name="orientation" src="d0_s11" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character constraint="than stem-leaves" constraintid="o18654" is_modifier="false" name="size" src="d0_s11" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o18654" name="stem-leaf" name_original="stem-leaves" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>margins plane to reflexed, entire or serrulate distally;</text>
      <biological_entity id="o18655" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s12" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apex acute to acuminate.</text>
    </statement>
    <statement id="d0_s14">
      <text>Specialized asexual reproduction absent or by small, slender, caducous axillary branchlets.</text>
      <biological_entity id="o18656" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="acuminate" />
        <character is_modifier="false" name="development" src="d0_s14" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o18657" name="branchlet" name_original="branchlets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="true" name="size" src="d0_s14" value="small" value_original="small" />
        <character is_modifier="true" name="size" src="d0_s14" value="slender" value_original="slender" />
        <character is_modifier="true" name="duration" src="d0_s14" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Perigonia axillary, gemmiform.</text>
      <biological_entity id="o18658" name="perigonium" name_original="perigonia" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s15" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Perichaetia on short, lateral branches, leaves strongly differentiated, greatly elongate, sheathing.</text>
      <biological_entity id="o18659" name="perichaetium" name_original="perichaetia" src="d0_s16" type="structure" />
      <biological_entity constraint="lateral" id="o18660" name="branch" name_original="branches" src="d0_s16" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o18661" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="greatly" name="shape" src="d0_s16" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o18659" id="r2675" name="on" negation="false" src="d0_s16" to="o18660" />
    </statement>
    <statement id="d0_s17">
      <text>Seta short-to-elongate.</text>
      <biological_entity id="o18662" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character char_type="range_value" from="short" name="size" src="d0_s17" to="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule immersed to exserted, ovoid to pyriform;</text>
      <biological_entity id="o18663" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="position" src="d0_s18" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="pyriform" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>annulus cells in several rows, differentiated;</text>
      <biological_entity constraint="annulus" id="o18664" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="variability" notes="" src="d0_s19" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o18665" name="row" name_original="rows" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="several" value_original="several" />
      </biological_entity>
      <relation from="o18664" id="r2676" name="in" negation="false" src="d0_s19" to="o18665" />
    </statement>
    <statement id="d0_s20">
      <text>operculum conic or oblique-rostrate;</text>
      <biological_entity id="o18666" name="operculum" name_original="operculum" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s20" value="oblique-rostrate" value_original="oblique-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>exostome teeth linear, papillose;</text>
      <biological_entity constraint="exostome" id="o18667" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s21" value="linear" value_original="linear" />
        <character is_modifier="false" name="relief" src="d0_s21" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endostome basal membrane low, segments imperfectly connate, papillose.</text>
      <biological_entity id="o18668" name="endostome" name_original="endostome" src="d0_s22" type="structure" />
      <biological_entity constraint="basal" id="o18669" name="membrane" name_original="membrane" src="d0_s22" type="structure">
        <character is_modifier="false" name="position" src="d0_s22" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o18670" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="imperfectly" name="fusion" src="d0_s22" value="connate" value_original="connate" />
        <character is_modifier="false" name="relief" src="d0_s22" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, except Australia and Oceania; temperate and warm regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="except Australia and Oceania" establishment_means="native" />
        <character name="distribution" value="temperate and warm regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 30 (3 in the flora).</discussion>
  <discussion>Leucodon is distinguished from the other genera in the family by its ecostate leaves and smooth calyptra. Forsstroemia is similar, but its plants are more glossy and the leaves are variably costate. Also, according to L. R. Stark (1985), paraphyses are present in the perichaetia of Forsstroemia but absent in Leucodon. In Leucodon, the axillary hairs are 3–5 per leaf axil, of two short proximal cells with brownish walls, and two or three longer distal cells. The stems are erect to spreading (or rarely pendent), straight or curved, and terete-foliate. The leaf apices are straight or falcate, sometimes filiform. The medial laminal cells are thick-walled and often porose. The calyptra is cucullate, or split on one side but gripping the seta below the capsule, and is naked. The exostome teeth are imperfect, perforate, and sometimes 2-fid.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves not plicate, not secund; apical laminal cells papillose abaxially; stems julaceous, short or rarely elongate; fragile branchlets in distal leaf axils absent.</description>
      <determination>1 Leucodon julaceus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ± plicate, secund or rarely so; apical laminal cells smooth or rarely papillose abaxially; stems not julaceous, elongate; fragile branchlets in distal leaf axils present or absent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Caducous branchlets absent; perichaetia, perigonia, and sporophytes common; leaves erect-appressed to secund; apical laminal cells smooth abaxially; terminal laminal cell concolorous to hyaline.</description>
      <determination>2 Leucodon brachypus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Caducous branchlets common, sometimes scarce or apparently absent; perichaetia extremely rare, perigonia and sporophytes unknown; leaves erect-appressed, rarely secund; apical laminal cells rarely papillose abaxially; terminal laminal cell hyaline.</description>
      <determination>3 Leucodon andrewsianus</determination>
    </key_statement>
  </key>
</bio:treatment>