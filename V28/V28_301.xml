<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>A. Jonathan Shaw</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">188</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Steere &amp; Z. Iwatsuki" date="unknown" rank="family">Pseudoditrichaceae</taxon_name>
    <taxon_hierarchy>family Pseudoditrichaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10734</other_info_on_name>
  </taxon_identification>
  <number>44.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scattered or gregarious, acrocarpous.</text>
      <biological_entity id="o489" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched.</text>
      <biological_entity id="o490" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading, linear-lanceolate;</text>
      <biological_entity id="o491" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base oblong, ± sheathing;</text>
      <biological_entity id="o492" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa narrow, delicate proximally, nearly filling acumen;</text>
      <biological_entity id="o493" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="proximally" name="fragility" src="d0_s4" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity id="o494" name="acumen" name_original="acumen" src="d0_s4" type="structure" />
      <relation from="o493" id="r66" modifier="nearly" name="filling" negation="false" src="d0_s4" to="o494" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cell-walls ± thick;</text>
      <biological_entity constraint="laminal" id="o495" name="cell-wall" name_original="cell-walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal-cells long-rectangular;</text>
      <biological_entity id="o496" name="basal-cell" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial cells irregularly rhombic at shoulders;</text>
      <biological_entity constraint="medial" id="o497" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character constraint="at shoulders" constraintid="o498" is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
      </biological_entity>
      <biological_entity id="o498" name="shoulder" name_original="shoulders" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>distal cells longer on distal costa.</text>
      <biological_entity constraint="distal" id="o500" name="costa" name_original="costa" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="distal" id="o499" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="on distal costa" constraintid="o500" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>male plants smaller, leaves less secund than females;</text>
      <biological_entity id="o501" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="male" value_original="male" />
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o502" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="less secund than females" value_original="less secund than females" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perigonial and perichaetial leaves scarcely differentiated.</text>
      <biological_entity constraint="perigonial and perichaetial" id="o503" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="scarcely" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta elongate.</text>
      <biological_entity id="o504" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule suberect, ovoid;</text>
      <biological_entity id="o505" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>exothecial cells elongate-rectangular;</text>
      <biological_entity constraint="exothecial" id="o506" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate-rectangular" value_original="elongate-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>annulus revoluble;</text>
      <biological_entity id="o507" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="revoluble" value_original="revoluble" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome diplolepidous-alternate;</text>
      <biological_entity id="o508" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="diplolepidous-alternate" value_original="diplolepidous-alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome teeth pale-yellow to hyaline, narrowly lanceolate, somewhat irregular, trabeculate, pitted basally, papillose distally;</text>
      <biological_entity constraint="exostome" id="o509" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s17" to="hyaline" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="trabeculate" value_original="trabeculate" />
        <character is_modifier="false" modifier="basally" name="relief" src="d0_s17" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome basal membrane low, scarcely exceeding capsule rim, hyaline to pale-yellow, segments divided to base and diverging, halves of adjacent segments converging opposite exostome teeth, cilia absent.</text>
      <biological_entity id="o510" name="endostome" name_original="endostome" src="d0_s18" type="structure" />
      <biological_entity constraint="basal" id="o511" name="membrane" name_original="membrane" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="low" value_original="low" />
        <character char_type="range_value" from="hyaline" name="coloration" src="d0_s18" to="pale-yellow" />
      </biological_entity>
      <biological_entity constraint="capsule" id="o512" name="rim" name_original="rim" src="d0_s18" type="structure" />
      <biological_entity id="o513" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character constraint="to base" constraintid="o514" is_modifier="false" name="shape" src="d0_s18" value="divided" value_original="divided" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s18" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o514" name="base" name_original="base" src="d0_s18" type="structure" />
      <biological_entity id="o515" name="half" name_original="halves" src="d0_s18" type="structure">
        <character constraint="opposite exostome teeth" constraintid="o517" is_modifier="false" name="arrangement" src="d0_s18" value="converging" value_original="converging" />
      </biological_entity>
      <biological_entity id="o516" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s18" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o517" name="tooth" name_original="teeth" src="d0_s18" type="structure" />
      <biological_entity id="o518" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o511" id="r67" modifier="scarcely" name="exceeding" negation="false" src="d0_s18" to="o512" />
      <relation from="o515" id="r68" name="part_of" negation="false" src="d0_s18" to="o516" />
    </statement>
    <statement id="d0_s19">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o519" name="calyptra" name_original="calyptra" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Pseudoditrichaceae consists of a single species, known only from the type specimen collected near Great Bear Lake, Northwest Territories. In the original description, Steere and Iwatsuki interpreted the endostome segments as opposite the exostome teeth. A. J. Shaw (1984) showed that the apparently opposite segments consist of two halves of adjacent alternate segments that are split to and diverge from their bases, and that the peristome should be interpreted as diplolepidous-alternate. The gametophytes are scarcely distinguishable from those of Ditrichum. This and the unique peristome warrant familial recognition.</discussion>
  <references>
    <reference>Shaw, A. J. 1984. A reinterpretation of peristome structure in Pseudoditrichum mirabile Steere and Iwatsuki (Pseudoditrichaceae). Bryologist 87: 314–318.</reference>
  </references>
  
</bio:treatment>