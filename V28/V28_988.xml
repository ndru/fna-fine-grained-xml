<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">629</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">630</other_info_on_meta>
    <other_info_on_meta type="mention_page">637</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">anomodontaceae</taxon_name>
    <taxon_name authority="Hooker &amp; Taylor" date="unknown" rank="genus">Anomodon</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Brit.,</publication_title>
      <place_in_publication>79, plates 3 [near upper right], 33 [upper center left &amp; right]. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family anomodontaceae;genus Anomodon</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, anomalos, abnormal, and odon, tooth, alluding to reduced peristome</other_info_on_name>
    <other_info_on_name type="fna_id">101932</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Dozy &amp; Molkenboer" date="unknown" rank="genus">Haplohymenium</taxon_name>
    <taxon_hierarchy>genus haplohymenium</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, ± glaucous, green to rusty brown.</text>
      <biological_entity id="o7130" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="rusty brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with branches erect to arcuate, sometimes attenuate to flagellate distally;</text>
      <biological_entity id="o7131" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s1" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="flagellate" value_original="flagellate" />
      </biological_entity>
      <biological_entity id="o7132" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o7131" id="r975" name="with" negation="false" src="d0_s1" to="o7132" />
    </statement>
    <statement id="d0_s2">
      <text>central strand cells differentiated or not;</text>
      <biological_entity constraint="strand" id="o7133" name="cell" name_original="cells" src="d0_s2" type="structure" constraint_original="central strand">
        <character is_modifier="false" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
        <character name="variability" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia absent in all but 2 species.</text>
      <biological_entity id="o7134" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character constraint="in species" constraintid="o7135" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7135" name="species" name_original="species" src="d0_s3" type="taxon_name">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branch leaves appressed to moderately secund or crispate when dry, complanate or erect to imbricate when moist, broadly ovate to lanceolate, ± abruptly narrowed mid leaf, not plicate proximally (somewhat plicate in A. longifolius);</text>
      <biological_entity constraint="branch" id="o7136" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s4" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="complanate" value_original="complanate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o7137" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less abruptly" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="not; proximally" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane, sometimes undulate or revolute, entire, papillose, crenulate, serrulate, or sometimes denticulate near apex;</text>
      <biological_entity id="o7138" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character constraint="near apex" constraintid="o7139" is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o7139" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>apex rounded, obtuse, acute, or narrowly acuminate;</text>
      <biological_entity id="o7140" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa usually strong, sometimes obscured by laminal cells and ending before mid leaf, sometimes discreetly, asymmetrically 2-fid at end, abaxial costa cells smooth or papillose;</text>
      <biological_entity id="o7141" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s7" value="strong" value_original="strong" />
        <character constraint="by laminal, cells" constraintid="o7142, o7143" is_modifier="false" modifier="sometimes" name="prominence" src="d0_s7" value="obscured" value_original="obscured" />
        <character constraint="at end" constraintid="o7145" is_modifier="false" modifier="sometimes discreetly; discreetly; asymmetrically" name="shape" notes="" src="d0_s7" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o7142" name="laminal" name_original="laminal" src="d0_s7" type="structure" />
      <biological_entity id="o7143" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity constraint="mid" id="o7144" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <biological_entity id="o7145" name="end" name_original="end" src="d0_s7" type="structure" />
      <biological_entity constraint="costa" id="o7146" name="cell" name_original="cells" src="d0_s7" type="structure" constraint_original="abaxial costa">
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o7141" id="r976" name="ending before" negation="false" src="d0_s7" to="o7144" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells hexagonal, obscure to irregular, small, papillae 1 or many, high, on both surfaces, walls thin;</text>
      <biological_entity constraint="laminal" id="o7147" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s8" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o7148" name="papilla" name_original="papillae" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="many" value_original="many" />
        <character is_modifier="false" name="height" src="d0_s8" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o7149" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o7150" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o7148" id="r977" name="on" negation="false" src="d0_s8" to="o7149" />
    </statement>
    <statement id="d0_s9">
      <text>basal-cells sometimes oblong, pellucid, smooth, walls incrassate.</text>
      <biological_entity id="o7151" name="basal-cell" name_original="basal-cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pellucid" value_original="pellucid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7152" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Perichaetial leaves well differentiated.</text>
      <biological_entity constraint="perichaetial" id="o7153" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="well" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta to 2.2 cm.</text>
      <biological_entity id="o7154" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s11" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule symmetric;</text>
      <biological_entity id="o7155" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stomata sometimes present;</text>
      <biological_entity id="o7156" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome reduced (well developed in A. rostratus);</text>
      <biological_entity id="o7157" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth white to pale-brown, densely papillose, occasionally cross-striolate, sometimes slightly trabeculate;</text>
      <biological_entity constraint="exostome" id="o7158" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s15" to="pale-brown" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s15" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="sometimes slightly" name="architecture" src="d0_s15" value="trabeculate" value_original="trabeculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome sometimes very reduced or absent (sect. Haplohymenium), basal membrane 2–7 cells high, segments keeled to linear and reduced or absent.</text>
      <biological_entity id="o7159" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sometimes very" name="size" src="d0_s16" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7160" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s16" to="7" />
      </biological_entity>
      <biological_entity id="o7161" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="false" name="height" src="d0_s16" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o7162" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character char_type="range_value" from="keeled" name="shape" src="d0_s16" to="linear" />
        <character is_modifier="false" name="size" src="d0_s16" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra smooth to papillose or hirsute (sect. Haplohymenium).</text>
      <biological_entity id="o7163" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s17" to="papillose" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 9–20 (–23) µm.</text>
      <biological_entity id="o7164" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s18" to="23" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s18" to="20" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America (Bolivia), Europe, s Africa, Pacific Islands (New Zealand), Australia; temperate, circumboreal regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="temperate" establishment_means="native" />
        <character name="distribution" value="circumboreal regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 16 (8 in the flora).</discussion>
  <discussion>Anomodon viticulosus grows mostly on shaded calcareous outcrops, but the other species are found on tree trunks, including their base, logs, or sometimes soil or rock. Anomodon attenuatus, A. minor, and A. rostratus may grow on the same tree, with A. rugelii sometimes joining them in submontane regions. Although A. tristis may be found growing with the above species, it usually forms much thinner, more delicate mats higher on the tree. In North America, at least two species (A. attenuatus and A. rostratus) fruit profusely; A. minor and A. rugelii fruit less abundantly and perhaps less frequently, while sporophytes of A. viticulosus are extremely rare in North America (only one fertile specimen of A. viticulosus seen, none of A. tristis).</discussion>
  <discussion>Haplohymenium was created to accommodate plants that resemble Anomodon but are more slender and have a papillose calyptra with long, hyaline scattered hairs. Segregating Haplohymenium would make the rest of Anomodon paraphyletic, as Haplohymenium is a sister group of A. minor, A. rugelii, and A. viticulosus, all of which are part of subg. Anomodon (Í. Granzow-de la Cerda 1997); these taxa are more distantly related to subg. Pseudoanomodon (Limpricht) Ochyra, to which A. attenuatus and A. rostratus belong.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves long-lanceolate; apices ending in hair-point or subulalike</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves ligulate; apices rounded, obtuse, acute, or apiculate, not ending in hair-point or subulalike</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems profusely branched; primary branches erect.</description>
      <determination>6 Anomodon rostratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems sparingly branched; primary branches prostrate or pendulous.</description>
      <determination>8 Anomodon longifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants small; stems less than 1 mm thick when dry; branch leaves less than 2.1 mm; apices often broken off; costa ending much before apex, obscured by laminal cells distally</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants small to large; stems usually more than 0.8 mm thick when dry; branch leaves sometimes greater than 2 mm; apices intact; costa ending near apex, not or rarely obscured by laminal cells distally</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems 0.3-0.5 mm thick when dry; leaves 0.5-0.9 mm; basal laminal cells few, region not reaching margin; costa weak, obscured by laminal cells almost throughout.</description>
      <determination>4 Anomodon tristis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems 0.5-1 mm thick when dry; leaves 1.2-1.8(-2.1) mm; basal laminal cells many, region reaching margin; costa moderately strong, obscured by laminal cells distally.</description>
      <determination>5 Anomodon thraustus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants dark green to rusty brown; leaves incurved-contorted when dry; bases auriculate; costae golden yellow to rusty brown; pseudoparaphyllia present.</description>
      <determination>2 Anomodon rugelii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants green to yellowish; leaves erect, imbricate, appressed or rarely slightly crisped when dry; bases broadly decurrent; costae pellucid or light green; pseudoparaphyllia absent</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants large; stems 1-1.8 mm thick when dry; leaves flexuose, secund, spreading when moist, erect when dry, greater than 2 mm.</description>
      <determination>1 Anomodon viticulosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants medium-sized to large; stems usually less than 1 mm thick when dry; leaves complanate when moist, appressed when dry, usually less than 2 mm</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems not pinnate, secondary branches not attenuate, often slightly clavate at apices; perichaetia on terminal branches, beyond distalmost branching points; leaves abruptly narrowed mid leaf; apices rounded; margins entire at apex; abaxial costa cells with rounded-simple papillae in rows.</description>
      <determination>3 Anomodon minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems irregularly pinnate, secondary branches attenuate at apices; perichaetia never present beyond distalmost branching points; leaves slightly narrowed mid leaf; apices acute, sometimes obtuse or slightly apiculate; margins sometimes denticulate at apex; abaxial costa cells smooth.</description>
      <determination>7 Anomodon attenuatus</determination>
    </key_statement>
  </key>
</bio:treatment>