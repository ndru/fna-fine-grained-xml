<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">391</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">400</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Vanderpoorten" date="unknown" rank="family">calliergonaceae</taxon_name>
    <taxon_name authority="(Sullivant) Kindberg" date="unknown" rank="genus">CALLIERGON</taxon_name>
    <place_of_publication>
      <publication_title>Canad. Rec. Sci.</publication_title>
      <place_in_publication>6: 72. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calliergonaceae;genus CALLIERGON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kallos, beauty, and ergon, work, alluding to appearance</other_info_on_name>
    <other_info_on_name type="fna_id">105112</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="section">Calliergon</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 672. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;section calliergon</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to very large, green, pale green, yellowish, brownish, or sometimes pale-pink.</text>
      <biological_entity id="o9176" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized to very" value_original="medium-sized to very" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="pale-pink" value_original="pale-pink" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± densely radially branched;</text>
      <biological_entity id="o9177" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less densely radially" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present;</text>
      <biological_entity id="o9178" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o9179" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>outer pseudoparaphyllia broad, apex rounded to acuminate or cleft;</text>
      <biological_entity constraint="outer" id="o9180" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o9181" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acuminate or cleft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials at various points on leaves, or on scattered points or in rows on stem;</text>
      <biological_entity id="o9182" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure" />
      <biological_entity constraint="rhizoid" id="o9183" name="initial" name_original="initials" src="d0_s4" type="structure" />
      <biological_entity id="o9184" name="point" name_original="points" src="d0_s4" type="structure">
        <character is_modifier="true" name="variability" src="d0_s4" value="various" value_original="various" />
      </biological_entity>
      <biological_entity id="o9185" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9186" name="row" name_original="rows" src="d0_s4" type="structure" />
      <relation from="o9182" id="r1288" name="at" negation="false" src="d0_s4" to="o9184" />
      <relation from="o9183" id="r1289" name="at" negation="false" src="d0_s4" to="o9184" />
      <relation from="o9184" id="r1290" name="on" negation="false" src="d0_s4" to="o9185" />
      <relation from="o9182" id="r1291" name="on scattered points or in" negation="false" src="d0_s4" to="o9186" />
      <relation from="o9183" id="r1292" name="on scattered points or in" negation="false" src="d0_s4" to="o9186" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hairs well developed, many, distal cells 2–8 (–10), hyaline.</text>
      <biological_entity constraint="axillary" id="o9187" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s5" value="developed" value_original="developed" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9188" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves ± broadly ovate to broadly rounded-triangular, abruptly narrowed to apex, straight, concave or strongly so, not plicate;</text>
      <biological_entity id="o9189" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="less broadly ovate" name="shape" src="d0_s6" to="broadly rounded-triangular" />
        <character constraint="to apex" constraintid="o9190" is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="course" notes="" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character name="shape" src="d0_s6" value="strongly" value_original="strongly" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o9190" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>base broadly decurrent;</text>
      <biological_entity id="o9191" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins entire or occasionally slightly sinuate;</text>
      <biological_entity id="o9192" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="occasionally slightly" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex rounded or obtuse;</text>
      <biological_entity id="o9193" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa single, ending just before apex, or branched to 2-fid and ending 1/2–9/10 leaf length;</text>
      <biological_entity id="o9195" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o9196" name="leaf" name_original="leaf" src="d0_s10" type="structure">
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s10" to="9/10" />
      </biological_entity>
      <relation from="o9194" id="r1293" name="ending" negation="false" src="d0_s10" to="o9195" />
      <relation from="o9194" id="r1294" name="ending" negation="false" src="d0_s10" to="o9196" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells differentiated, rectangular to long-rectangular, strongly inflated, hyaline, walls thin, region distinctly or indistinctly delimited, transversely triangular to broadly ovate;</text>
      <biological_entity id="o9194" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s10" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o9197" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s11" to="long-rectangular" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o9198" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o9199" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="prominence" src="d0_s11" value="delimited" value_original="delimited" />
        <character char_type="range_value" from="transversely triangular" name="shape" src="d0_s11" to="broadly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cell-walls incrassate or thin, porose or not.</text>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous or autoicous;</text>
      <biological_entity constraint="medial laminal" id="o9200" name="cell-wall" name_original="cell-walls" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="porose" value_original="porose" />
        <character name="architecture" src="d0_s12" value="not" value_original="not" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner perichaetial leaves not plicate;</text>
      <biological_entity constraint="inner perichaetial" id="o9201" name="leaf" name_original="leaves" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s14" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>vaginula naked.</text>
      <biological_entity id="o9202" name="vaginulum" name_original="vaginula" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsule with annulus not separating;</text>
      <biological_entity id="o9203" name="capsule" name_original="capsule" src="d0_s16" type="structure" />
      <biological_entity id="o9204" name="annulus" name_original="annulus" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s16" value="separating" value_original="separating" />
      </biological_entity>
      <relation from="o9203" id="r1295" name="with" negation="false" src="d0_s16" to="o9204" />
    </statement>
    <statement id="d0_s17">
      <text>exostome external surface reticulate basally, margins dentate distally.</text>
      <biological_entity id="o9205" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity constraint="external" id="o9206" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_coloration_or_relief" src="d0_s17" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o9207" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 13–31 µm.</text>
      <biological_entity id="o9208" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s18" to="31" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 or 6 (5 in the flora).</discussion>
  <discussion>Calliergon is recognized by the robust shoots and straight, ovate to broadly cordate leaves with rounded or broadly obtuse apices. Except for expressions of C. giganteum with a pink hue, red pigment is absent. The alar regions are well developed and in some species very large, and consist of inflated and hyaline cells. Rhizoid initials are frequent in the leaves, and the axillary hairs are large and abundant. Except for C. megalophyllum, which mostly grows submerged or in reed or sedge belts along lakeshores, the plants typically occur in somewhat nutrient-rich and relatively mineral-rich wetlands, such as fens and swamps. Weak expressions, especially of C. cordifolium, could be confused with Sarmentypnum sarmentosum or Straminergon stramineum; such variants are discussed under these species. W. C. Steere (1941b) described C. aftonianum from fossil material of isolated branches. This is most likely a Calliergon species, albeit with a short or sometimes absent costa. Weakly developed costae occur also in weak plants of the species treated here. However, since J. A. Janssens and P. H. Glaser (1986) reported extant plants with short costae from North America, further studies are required to decide the relationships of C. aftonianum.</discussion>
  <references>
    <reference>Karczmarz, K. 1971. A monograph of the genus Calliergon (Sull.) Kindb. Monogr. Bot. 34: 1–209.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Alar regions indistinctly delimited</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Alar regions sharply delimited</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Alar regions from margins to costa or almost so; proximal laminal cells not markedly wider than distal cells; stem leaves ovate-cordate, triangular-ovate, or narrowly elongate- cordate.</description>
      <determination>1 Calliergon cordifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Alar regions from margins 65-80% distance to costa; proximal laminal cells to 2 times as wide as distal cells; stem leaves broadly ovate or broadly ovate-cordate.</description>
      <determination>2 Calliergon orbicularicordatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf costae (88-)95-282 µm wide at base, single, ending shortly before apex; alar regions from margins to costa or nearly so.</description>
      <determination>3 Calliergon giganteum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf costae 53-119 µm wide at base, single, ending shortly before apex, or single, branched or 2-fid, ending 1/2-9/10 leaf length; alar regions from margins 35-80% distance to costa</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sexual condition autoicous; stem leaf costae 1/2-9/10 leaf length, usually branched or distally 2-fid; axillary hair apical cell short- to long-linear.</description>
      <determination>4 Calliergon richardsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sexual condition dioicous; stem leaf costae ending shortly before apex, single; axillary hair apical cell elongate-rectangular to short-linear.</description>
      <determination>5 Calliergon megalophyllum</determination>
    </key_statement>
  </key>
</bio:treatment>