<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">cupressiforme</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="variety">filiforme</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent</publication_title>
      <place_in_publication>2(2): 138. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus hypnum;species cupressiforme;variety filiforme</taxon_hierarchy>
    <other_info_on_name type="fna_id">250064844</other_info_on_name>
  </taxon_identification>
  <number>5b</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, pale green to dull green, sometimes yellowish.</text>
      <biological_entity id="o3229" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s0" to="dull green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–6 cm, creeping, subjulaceous, irregularly branched to somewhat pinnate.</text>
      <biological_entity id="o3230" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched to somewhat" value_original="branched to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves straight to somewhat falcate, oblong-lanceolate, gradually narrowed to apex;</text>
      <biological_entity id="o3231" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="to apex" constraintid="o3232" is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o3232" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins nearly entire;</text>
    </statement>
    <statement id="d0_s4">
      <text>alar cells many, subquadrate;</text>
      <biological_entity id="o3233" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3234" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminal cells 60–80 × 3–4 µm (or slightly larger).</text>
      <biological_entity constraint="laminal" id="o3235" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="60" from_unit="um" name="length" src="d0_s5" to="80" to_unit="um" />
        <character char_type="range_value" from="3" from_unit="um" name="width" src="d0_s5" to="4" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Branch leaves 1–1.4 × 0.2–0.4 mm or slightly smaller.</text>
      <biological_entity constraint="branch" id="o3236" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsule maturity unknown.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vertical surfaces of cliffs and tree trunks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vertical surfaces" constraint="of cliffs and tree trunks" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="tree trunks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., P.E.I.; Ark., Conn., Maine, Mass., Mich., N.H., N.J., N.Y., N.C., Okla., Pa., Tenn., Tex., Va.; s South America; Europe; Asia; Africa; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>When well-developed, var. filiforme is distinctive, with filiform julaceous stems and straight leaves. There are variants with somewhat falcate-secund leaves that can be confused with H. andoi. The latter species has long-attenuate leaf apices, is not julaceous, although filiform, and the leaves are not strongly imbricate as in var. filiforme. The many specimens that closely resemble var. filiforme strongly suggest that it may be an environmental form induced by the habitat that holds moisture rather briefly during the growing season.</discussion>
  
</bio:treatment>