<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">620</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="illustration_page">618</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">lembophyllaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">isothecium</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">cardotii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>17: 278. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lembophyllaceae;genus isothecium;species cardotii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099196</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, dark green or yellow to golden green, slightly glossy.</text>
      <biological_entity id="o207" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="golden green" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with stipe present, secondary stems 3–15 cm, not julaceous, pinnate and frondlike or irregularly branched and somewhat dendroid, branches arching downward when dry, flagelliform branches rare, irregularly branched;</text>
      <biological_entity id="o208" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o209" name="stipe" name_original="stipe" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o210" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="frondlike" value_original="frondlike" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s1" value="dendroid" value_original="dendroid" />
      </biological_entity>
      <biological_entity id="o211" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s1" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o212" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="flagelliform" value_original="flagelliform" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="rare" value_original="rare" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o208" id="r33" name="with" negation="false" src="d0_s1" to="o209" />
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia broadly foliose, blunt or with few short teeth.</text>
      <biological_entity id="o213" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s2" value="foliose" value_original="foliose" />
        <character is_modifier="false" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s2" value="with few short teeth" value_original="with few short teeth" />
      </biological_entity>
      <biological_entity id="o214" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <relation from="o213" id="r34" name="with" negation="false" src="d0_s2" to="o214" />
    </statement>
    <statement id="d0_s3">
      <text>Primary-stem leaves broadly to narrowly triangular;</text>
      <biological_entity constraint="primary-stem" id="o215" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire to weakly toothed at apex;</text>
      <biological_entity id="o216" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at apex" constraintid="o217" from="entire" name="shape" src="d0_s4" to="weakly toothed" />
      </biological_entity>
      <biological_entity id="o217" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex attenuate, sometimes falcate;</text>
      <biological_entity id="o218" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ecostate or costa present;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells differentiated, region small, to 1/10 leaf length.</text>
      <biological_entity id="o219" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="true" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o220" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o221" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="1/10" />
      </biological_entity>
      <biological_entity id="o222" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Branchlet leaves ovatelanceolate;</text>
      <biological_entity constraint="branchlet" id="o223" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>margins nearly entire to weakly toothed basally, coarsely toothed distally;</text>
      <biological_entity id="o224" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="nearly entire" name="shape" src="d0_s9" to="weakly toothed basally" />
        <character is_modifier="false" modifier="coarsely; distally" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa to 2/3 leaf length, occasionally weakly 2-fid, sometimes ending in abaxial spine;</text>
      <biological_entity id="o225" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="2/3" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o227" name="spine" name_original="spine" src="d0_s10" type="structure" />
      <relation from="o226" id="r35" modifier="sometimes" name="ending in" negation="false" src="d0_s10" to="o227" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells truncate, rectangular, shorter, region well defined, sometimes excavate, extending 1/3 distance across base, less than 1/10 leaf length.</text>
      <biological_entity id="o226" name="leaf" name_original="leaf" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="occasionally weakly" name="length" src="d0_s10" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o228" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o229" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s11" value="defined" value_original="defined" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="excavate" value_original="excavate" />
      </biological_entity>
      <biological_entity id="o230" name="distance" name_original="distance" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o231" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="1/10" />
      </biological_entity>
      <biological_entity id="o232" name="leaf" name_original="leaf" src="d0_s11" type="structure" />
      <relation from="o229" id="r36" name="extending" negation="false" src="d0_s11" to="o230" />
      <relation from="o229" id="r37" name="across" negation="false" src="d0_s11" to="o231" />
    </statement>
    <statement id="d0_s12">
      <text>Seta 1–2 cm.</text>
      <biological_entity id="o233" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule 1.5–2 mm.</text>
      <biological_entity id="o234" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks, rocky cliffs, boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="rocky cliffs" />
        <character name="habitat" value="boulders" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">cardoti</other_name>
  <discussion>Isothecium cardotii is clearly related to I. stoloniferum and can grow with it, but is readily separated in size alone in most specimens. Occasionally, specimens of I. stoloniferum are of similar size, but the presence of flagelliform branches identifies I. stoloniferum. The primary stem leaves are often unpigmented, abruptly narrowed and small; the branchlet leaf costae are broad at the base and tapering toward the apex.</discussion>
  
</bio:treatment>