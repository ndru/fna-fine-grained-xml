<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Sullivant) Mitten" date="unknown" rank="genus">campylium</taxon_name>
    <taxon_name authority="(Lindberg &amp; Arnell) Hedenas" date="1989" rank="species">longicuspis</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>14: 144. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus campylium;species longicuspis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099060</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblystegium</taxon_name>
    <taxon_name authority="Lindberg &amp; Arnell" date="unknown" rank="species">longicuspis</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Svenska Vetensk. Acad. Handl., n. s.</publication_title>
      <place_in_publication>23(10): 123. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus amblystegium;species longicuspis</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, golden brown.</text>
      <biological_entity id="o23491" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden brown" value_original="golden brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or creeping, unbranched or irregularly branched;</text>
      <biological_entity id="o23492" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent.</text>
      <biological_entity id="o23493" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves erect to spreading, ± suddenly or more gradually narrowed to apex, concave or strongly so, 2.1–4.6 × 0.6–1 mm;</text>
      <biological_entity id="o23494" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character constraint="to apex" constraintid="o23495" is_modifier="false" modifier="more or less suddenly; suddenly; gradually" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="concave" value_original="concave" />
        <character name="shape" src="d0_s3" value="strongly" value_original="strongly" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" src="d0_s3" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23495" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base subsheathing, ovate or narrowly so;</text>
      <biological_entity id="o23496" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="subsheathing" value_original="subsheathing" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character name="shape" src="d0_s4" value="narrowly" value_original="narrowly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>acumen frequently differentiated, constituting 18–33% leaf length.</text>
      <biological_entity id="o23498" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s5" to="33%" />
      </biological_entity>
      <relation from="o23497" id="r3336" name="constituting" negation="false" src="d0_s5" to="o23498" />
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o23497" name="acumen" name_original="acumen" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="frequently" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Peaty shallow soil over limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow soil" modifier="peaty" constraint="over limestone" />
        <character name="habitat" value="limestone" modifier="over" />
        <character name="habitat" value="peaty" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; arctic Eurasia; c Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" value="arctic Eurasia" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Campylium longicuspis is known from a single arctic North American locality in northeast Greenland. The species is easily distinguished from C. stellatum by its ovate or narrowly ovate, rather than cordate or rounded-triangular stem leaves, and shorter leaf acumen, as well as by its autoicous rather than dioicous sexual condition. The stem leaves are straight or slightly homomallous. Differences between 1. C. laxifolium and C. longicuspis are discussed with the former. This species is very rare, and possibly overlooked in the Arctic.</discussion>
  <references>
    <reference>Hedenäs, L. 1989c. Amblystegium longicuspis Lindb. &amp; H. Arn., its status and taxonomic position. Lindbergia 14: 142–146.</reference>
  </references>
  
</bio:treatment>