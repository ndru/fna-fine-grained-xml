<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="(Bridel) W. R. Buck" date="1998" rank="species">ruderale</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>82: 240. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species ruderale</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099031</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">ruderale</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent., suppl.</publication_title>
      <place_in_publication>2: 158. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species ruderale</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="(Spruce ex Mitten) A. Jaeger" date="unknown" rank="species">stereopoma</taxon_name>
    <taxon_hierarchy>genus brachythecium;species stereopoma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="species">wootonii</taxon_name>
    <taxon_hierarchy>genus b.;species wootonii</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, in moderately dense mats, light green to light yellow-green, occasionally intense green.</text>
      <biological_entity id="o13410" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="light yellow-green" />
        <character is_modifier="false" modifier="occasionally" name="prominence" src="d0_s0" value="intense" value_original="intense" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13411" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o13410" id="r1890" name="in" negation="false" src="d0_s0" to="o13411" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 5 cm, creeping, terete-foliate, irregularly to regularly pinnate, branches to 5 mm, straight, terete-foliate.</text>
      <biological_entity id="o13412" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly to regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o13413" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves closely to loosely appressed, imbricate, broadly ovate, broadest at 1/7 leaf length, concave, plicate, 1.1–1.5 (–1.9) × 0.5–0.7 (–0.8) mm;</text>
      <biological_entity id="o13414" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="closely to loosely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character constraint="at leaf" constraintid="o13415" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s2" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13415" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/7" value_original="1/7" />
        <character is_modifier="false" name="length" notes="" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base rounded, narrowly short-decurrent;</text>
      <biological_entity id="o13416" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or recurved, almost subentire to serrulate, sharply serrate distally;</text>
      <biological_entity id="o13417" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire to serrulate" value_original="subentire to serrulate" />
        <character is_modifier="false" modifier="sharply; distally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex gradually to abruptly tapered, acumen long;</text>
      <biological_entity id="o13418" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13419" name="acumen" name_original="acumen" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 40–70% leaf length, slender, terminal spine absent;</text>
      <biological_entity id="o13420" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o13421" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="40-70%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells subquadrate to short-rectangular, small, 15–25 × 13–16 µm, walls moderately thick, region ± clearly delimited, extensive, of 5–7 × 5–7 cells;</text>
      <biological_entity constraint="terminal" id="o13422" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13423" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s7" to="25" to_unit="um" />
        <character char_type="range_value" from="13" from_unit="um" name="width" src="d0_s7" to="16" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13424" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13425" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less clearly" name="prominence" src="d0_s7" value="delimited" value_original="delimited" />
        <character is_modifier="false" name="size" src="d0_s7" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o13426" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <relation from="o13425" id="r1891" name="consist_of" negation="false" src="d0_s7" to="o13426" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear, 45–65 × 5–7 µm; basal-cells 20–30 × 10–13 µm, region in 2 or 3 rows, not markedly different from more distal cells.</text>
      <biological_entity constraint="laminal" id="o13427" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s8" to="65" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="7" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13428" name="basal-cell" name_original="basal-cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="length" src="d0_s8" to="30" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s8" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13429" name="region" name_original="region" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o13430" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o13429" id="r1892" name="in" negation="false" src="d0_s8" to="o13430" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves similar, narrower;</text>
      <biological_entity constraint="branch" id="o13431" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>margins more strongly serrate.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o13432" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta reddish orange, 1.5–2.5 cm, smooth.</text>
      <biological_entity id="o13433" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish orange" value_original="reddish orange" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s12" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule slightly inclined, reddish orange, cylindric, weakly curved, 2–2.6 mm;</text>
      <biological_entity id="o13434" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s13" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="weakly" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus not separating;</text>
      <biological_entity id="o13435" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s14" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum long-conic or short-rostrate.</text>
      <biological_entity id="o13436" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="long-conic" value_original="long-conic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 12–18 µm.</text>
      <biological_entity id="o13437" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s16" to="18" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, soil, open xeric places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="open xeric places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (1800-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1800" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico; West Indies; Central America; South America; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachythecium ruderale was known historically from the flora area only from New Mexico, where it was described as B. wootonii, and has more recently been discovered in Arizona, Cochise County, 1950–2130 m (Allen 27950, MO; Goodding 10822, MO). This is the most common species of Brachythecium in Mexico, Central America, and northern South America. The widespread tropical African B. implicatum Hornschuch ex Müller Hal. was considered conspecific with B. ruderale (as B. stereopoma) by W. R. Buck (1993). The typical expression of the species has closely appressed leaves resulting in an almost julaceous appearance. However, sometimes branch leaves are narrow and more loosely arranged. In North America, B. ruderale can be confused with B. acuminatum, which differs in having a shorter acumen, eplicate leaves, and homogeneous cells across the leaf base; with B. laetum, which differs in its more strongly plicate stem leaves, small opaque cells across the leaf base, and rather short distal laminal cells; and with B. oedipodium, which differs in having eplicate leaves and conspicuous, long and broad decurrencies. In B. ruderale, the pellucid or opaque alar regions reach from the margin 33% distance to the costa and extend distally to just below the broadest point of the leaf.</discussion>
  
</bio:treatment>