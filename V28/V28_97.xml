<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Leónsky-Haapasaari &amp; D. H. Norris" date="1998" rank="species">shevockii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>101: 435, figs. 1 – 23. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species shevockii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250061969</other_info_on_name>
  </taxon_identification>
  <number>38.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 1.5 cm.</text>
      <biological_entity id="o5937" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves rigid, erect when dry, ligulate to elliptic-lanceolate, to 5 mm;</text>
      <biological_entity id="o5938" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="0" from_unit="mm" name="distance" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins recurved from just beyond base to near apex, entire;</text>
      <biological_entity id="o5939" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="just beyond base" constraintid="o5940" is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5940" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o5941" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <relation from="o5940" id="r813" name="to" negation="false" src="d0_s2" to="o5941" />
    </statement>
    <statement id="d0_s3">
      <text>apex obtuse to rounded-acute;</text>
      <biological_entity id="o5942" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells short-rectangular, walls thin, not nodose;</text>
      <biological_entity constraint="basal laminal" id="o5943" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o5944" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cells 13–16 µm, 1-stratose, papillae 2 or 3 per cell, simple to 2-fid, high;</text>
      <biological_entity constraint="distal" id="o5945" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s5" to="16" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o5946" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or per" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="height" src="d0_s5" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o5947" name="cell" name_original="cell" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>marginal cells 2-stratose or 3-stratose.</text>
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity constraint="marginal" id="o5948" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-stratose" value_original="3-stratose" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta to 0.5 mm.</text>
      <biological_entity id="o5949" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule immersed, short-cylindric to oblong-ovate, 1–2 mm, strongly 8-ribbed entire length, constricted below mouth when dry;</text>
      <biological_entity id="o5950" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="immersed" value_original="immersed" />
        <character char_type="range_value" from="short-cylindric" name="shape" src="d0_s10" to="oblong-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s10" value="8-ribbed" value_original="8-ribbed" />
        <character is_modifier="false" name="length" src="d0_s10" value="entire" value_original="entire" />
        <character constraint="below mouth" constraintid="o5951" is_modifier="false" name="size" src="d0_s10" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o5951" name="mouth" name_original="mouth" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stomata immersed, 2/3 to completely covered by subsidiary-cells, cells projecting, inner walls thickened;</text>
      <biological_entity id="o5952" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="immersed" value_original="immersed" />
        <character name="quantity" src="d0_s11" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o5953" name="subsidiary-cell" name_original="subsidiary-cells" src="d0_s11" type="structure" />
      <biological_entity id="o5954" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o5955" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o5952" id="r814" modifier="completely" name="covered by" negation="false" src="d0_s11" to="o5953" />
      <relation from="o5952" id="r815" modifier="completely" name="covered by" negation="false" src="d0_s11" to="o5954" />
      <relation from="o5952" id="r816" modifier="completely" name="covered by" negation="false" src="d0_s11" to="o5955" />
    </statement>
    <statement id="d0_s12">
      <text>peristome double;</text>
      <biological_entity id="o5956" name="peristome" name_original="peristome" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>prostome absent;</text>
      <biological_entity id="o5957" name="prostome" name_original="prostome" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>exostome teeth 8, reflexed, papillose to papillose-reticulate near apex;</text>
      <biological_entity constraint="exostome" id="o5958" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="near apex" constraintid="o5959" from="papillose" name="relief" src="d0_s14" to="papillose-reticulate" />
      </biological_entity>
      <biological_entity id="o5959" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>endostome segments 8, occasionally 16, well developed, of 1 row of cells, smooth or roughened.</text>
      <biological_entity constraint="endostome" id="o5960" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
        <character modifier="occasionally" name="quantity" src="d0_s15" value="16" value_original="16" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
        <character is_modifier="false" name="relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s15" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o5961" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5962" name="cell" name_original="cells" src="d0_s15" type="structure" />
      <relation from="o5960" id="r817" name="consist_of" negation="false" src="d0_s15" to="o5961" />
      <relation from="o5961" id="r818" name="part_of" negation="false" src="d0_s15" to="o5962" />
    </statement>
    <statement id="d0_s16">
      <text>Calyptra oblong, smooth, hairy, hairs papillose.</text>
      <biological_entity id="o5963" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5964" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 15–18 µm.</text>
      <biological_entity id="o5965" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s17" to="18" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry granitic rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry granitic rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1100-1600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1100" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Orthotrichum shevockii is known only from the southern Sierra Mountains. The species is distinguished by small, blackish tufts, obtuse to bluntly acute leaf apices, strongly multipapillose distal laminal cells, 2-stratose (or more) leaf margins, and completely immersed, strongly 8-ribbed capsules with immersed stomata, reflexed exostome teeth, well-developed endostome, and papillose calyptra hairs.</discussion>
  
</bio:treatment>