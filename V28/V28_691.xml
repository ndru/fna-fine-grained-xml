<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="mention_page">445</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">homalothecium</taxon_name>
    <taxon_name authority="(Mitten) E. Lawton" date="1965" rank="species">aeneum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>92: 351. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus homalothecium;species aeneum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099145</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">aeneum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 31, plate 5 [lower left]. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species aeneum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Homalothecium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nevadense</taxon_name>
    <taxon_name authority="(Mitten) Heike Hofmann" date="unknown" rank="variety">aeneum</taxon_name>
    <taxon_hierarchy>genus homalothecium;species nevadense;variety aeneum</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, in dense tufts, light green when young, golden or brownish yellow with age.</text>
      <biological_entity id="o18518" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" modifier="when young" name="coloration" notes="" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden" value_original="golden" />
        <character constraint="with age" constraintid="o18520" is_modifier="false" name="coloration" src="d0_s0" value="brownish yellow" value_original="brownish yellow" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18519" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o18520" name="age" name_original="age" src="d0_s0" type="structure" />
      <relation from="o18518" id="r2655" name="in" negation="false" src="d0_s0" to="o18519" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 10 cm, densely regularly pinnate, branches 5–7 mm, curved to almost circinate, secund from substrate.</text>
      <biological_entity id="o18521" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="densely regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o18522" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="almost" name="shape_or_vernation" src="d0_s1" value="circinate" value_original="circinate" />
        <character constraint="from substrate" constraintid="o18523" is_modifier="false" name="architecture" src="d0_s1" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o18523" name="substrate" name_original="substrate" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect-appressed, triangular-lanceolate, (0.7–) 1.5–2 (–2.8) × (0.2–) 0.5–0.7 (–0.8) mm;</text>
      <biological_entity id="o18524" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_length" src="d0_s2" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s2" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base short-rounded, narrowly decurrent;</text>
      <biological_entity id="o18525" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="short-rounded" value_original="short-rounded" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or often recurved in places, serrulate or subentire;</text>
      <biological_entity id="o18526" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="in places" constraintid="o18527" is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o18527" name="place" name_original="places" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate or gradually tapered;</text>
      <biological_entity id="o18528" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 70–95% leaf length, terminal spine present;</text>
      <biological_entity id="o18529" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o18530" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="70-95%" name="character" src="d0_s6" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells ovate, 6–7 µm wide, walls thick, region of 5–10 × 6–10 cells, moderately distinctly delimited, opaque to moderately translucent;</text>
      <biological_entity constraint="terminal" id="o18531" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18532" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s7" to="7" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18533" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o18534" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately distinctly" name="prominence" notes="" src="d0_s7" value="delimited" value_original="delimited" />
        <character char_type="range_value" from="opaque" name="coloration" src="d0_s7" to="moderately translucent" />
      </biological_entity>
      <biological_entity id="o18535" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <relation from="o18534" id="r2656" name="consist_of" negation="false" src="d0_s7" to="o18535" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear-flexuose, 30–85 × 5–6 µm; basal-cells irregularly long-ovate, region in 1–3 rows, indistinctly delimited from distal cells.</text>
      <biological_entity constraint="laminal" id="o18536" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="linear-flexuose" value_original="linear-flexuose" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s8" to="85" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18537" name="basal-cell" name_original="basal-cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s8" value="long-ovate" value_original="long-ovate" />
      </biological_entity>
      <biological_entity id="o18538" name="region" name_original="region" src="d0_s8" type="structure">
        <character constraint="from distal cells" constraintid="o18540" is_modifier="false" modifier="indistinctly" name="prominence" notes="" src="d0_s8" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o18539" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18540" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <relation from="o18538" id="r2657" name="in" negation="false" src="d0_s8" to="o18539" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves appressed when dry, spreading when moist, narrowly lanceolate, (0.5–) 0.7–1.8 × (0.1–) 0.2–0.4 mm;</text>
      <biological_entity constraint="branch" id="o18541" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_length" src="d0_s9" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s9" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_width" src="d0_s9" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>margins plane or recurved at places, serrulate or serrate proximally, entire or minutely serrulate distally, teeth recurved in alar region;</text>
      <biological_entity id="o18542" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="plane" value_original="plane" />
        <character constraint="at places" constraintid="o18543" is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="minutely; distally" name="architecture_or_shape" src="d0_s10" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o18543" name="place" name_original="places" src="d0_s10" type="structure" />
      <biological_entity id="o18544" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character constraint="in alar, region" constraintid="o18545, o18546" is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o18545" name="alar" name_original="alar" src="d0_s10" type="structure" />
      <biological_entity id="o18546" name="region" name_original="region" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>apex acuminate;</text>
      <biological_entity id="o18547" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>costa to 70–95% leaf length, variable within one shoot, terminal spine present;</text>
      <biological_entity id="o18548" name="costa" name_original="costa" src="d0_s12" type="structure" />
      <biological_entity id="o18549" name="leaf" name_original="leaf" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="70-95%" name="character" src="d0_s12" value="length" value_original="length" />
        <character constraint="within shoot" constraintid="o18550" is_modifier="false" name="variability" src="d0_s12" value="variable" value_original="variable" />
      </biological_entity>
      <biological_entity id="o18550" name="shoot" name_original="shoot" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>alar cells subquadrate or irregularly ovate, 7–10 µm wide, walls moderately incrassate, region small, of 5–10 cells along submarginal fold, distinctly delimited;</text>
      <biological_entity constraint="terminal" id="o18551" name="spine" name_original="spine" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18552" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s13" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18553" name="wall" name_original="walls" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="moderately" name="size" src="d0_s13" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o18554" name="region" name_original="region" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="small" value_original="small" />
        <character is_modifier="false" modifier="distinctly" name="prominence" notes="" src="d0_s13" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o18555" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s13" to="10" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o18556" name="fold" name_original="fold" src="d0_s13" type="structure" />
      <relation from="o18554" id="r2658" name="consist_of" negation="false" src="d0_s13" to="o18555" />
      <relation from="o18555" id="r2659" name="along" negation="false" src="d0_s13" to="o18556" />
    </statement>
    <statement id="d0_s14">
      <text>laminal cells linear-flexuose, 25–85 × 4–6 µm; basal-cells in 1 (or 2) rows;</text>
      <biological_entity constraint="laminal" id="o18557" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="linear-flexuose" value_original="linear-flexuose" />
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s14" to="85" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s14" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18558" name="basal-cell" name_original="basal-cells" src="d0_s14" type="structure" />
      <biological_entity id="o18559" name="row" name_original="rows" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <relation from="o18558" id="r2660" name="in" negation="false" src="d0_s14" to="o18559" />
    </statement>
    <statement id="d0_s15">
      <text>distal cells smooth.</text>
    </statement>
    <statement id="d0_s16">
      <text>Sexual condition phyllodioicous or dioicous.</text>
      <biological_entity constraint="distal" id="o18560" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s16" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seta 0.7–1.5 cm, rough proximally to almost throughout or smooth distally.</text>
      <biological_entity id="o18561" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s17" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="proximally to almost; throughout" name="pubescence_or_relief" src="d0_s17" value="rough" value_original="rough" />
        <character is_modifier="false" modifier="almost throughout; distally" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule inclined to horizontal, occasionally suberect, ovate-cylindric, curved, 1.8–2.5 mm;</text>
      <biological_entity id="o18562" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s18" to="horizontal" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s18" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovate-cylindric" value_original="ovate-cylindric" />
        <character is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>annulus separating by fragments;</text>
      <biological_entity id="o18563" name="annulus" name_original="annulus" src="d0_s19" type="structure">
        <character constraint="by fragments" constraintid="o18564" is_modifier="false" name="arrangement" src="d0_s19" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o18564" name="fragment" name_original="fragments" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>operculum long-conic;</text>
      <biological_entity id="o18565" name="operculum" name_original="operculum" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="long-conic" value_original="long-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>peristome hygrocastique;</text>
      <biological_entity id="o18566" name="peristome" name_original="peristome" src="d0_s21" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s21" value="hygrocastique" value_original="hygrocastique" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>exostome teeth with long transitional zone;</text>
      <biological_entity constraint="exostome" id="o18567" name="tooth" name_original="teeth" src="d0_s22" type="structure" />
      <biological_entity id="o18568" name="zone" name_original="zone" src="d0_s22" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s22" value="long" value_original="long" />
        <character is_modifier="true" name="architecture" src="d0_s22" value="transitional" value_original="transitional" />
      </biological_entity>
      <relation from="o18567" id="r2661" name="with" negation="false" src="d0_s22" to="o18568" />
    </statement>
    <statement id="d0_s23">
      <text>endostome basal membrane 40% endostome height, segments as long as exostome teeth, narrow, cilia almost as long as segments.</text>
      <biological_entity id="o18569" name="endostome" name_original="endostome" src="d0_s23" type="structure" />
      <biological_entity constraint="basal" id="o18570" name="membrane" name_original="membrane" src="d0_s23" type="structure" />
      <biological_entity id="o18571" name="endostome" name_original="endostome" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="40%" name="character" src="d0_s23" value="height" value_original="height" />
      </biological_entity>
      <biological_entity id="o18572" name="segment" name_original="segments" src="d0_s23" type="structure">
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s23" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o18573" name="tooth" name_original="teeth" src="d0_s23" type="structure" />
      <biological_entity id="o18574" name="cilium" name_original="cilia" src="d0_s23" type="structure" />
      <biological_entity id="o18575" name="segment" name_original="segments" src="d0_s23" type="structure" />
      <relation from="o18572" id="r2662" name="as long as" negation="false" src="d0_s23" to="o18573" />
      <relation from="o18574" id="r2663" name="as long as" negation="false" src="d0_s23" to="o18575" />
    </statement>
    <statement id="d0_s24">
      <text>Spores 10–17 µm.</text>
      <biological_entity id="o18576" name="spore" name_original="spores" src="d0_s24" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s24" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous rock, sandstone, granite, soil, rotten logs, tree trunks, forests, open areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous rock" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="tree trunks" modifier="rotten logs" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="open areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Calif., Colo., Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>H. Hofmann (1998) suggested treating Homalothecium aeneum as a subspecies of H. nevadense, but D. H. Norris and J. R. Shevock (2004) did not accept this decision. Analysis by S. Huttunen et al. (2008) and L. Hedenäs et al. (2009) found the two species closely related, but sufficiently distinguished to be treated as separate. The ranges of both taxa are almost identical, although H. aeneum is more common in the northern part of their range and H. nevadense in the south. The two species are distinguished mainly by sporophytic characters, and sterile collections are usually difficult to interpret. Hofmann (1997, 1998) and Norris and Shevock considered the main distinguishing character to be that H. aeneum has alar cells more numerous, regular in shape, with clear outlines and not opaque, whereas H. nevadense has fewer alar cells, irregular in shape, with unclear outlines and appearing opaque. The difference in cell shape is here confirmed, while variation in number of cells seems difficult to apply as the broader leaves of H. nevadense from better developed plants have a fairly extensive alar group.</discussion>
  
</bio:treatment>