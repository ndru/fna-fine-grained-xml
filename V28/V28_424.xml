<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="illustration_page">273</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">hygrohypnum</taxon_name>
    <taxon_name authority="(Lindberg) Loeske" date="1904" rank="species">alpinum</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>43: 194. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus hygrohypnum;species alpinum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099163</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblystegium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">molle</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="variety">alpinum</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Scand.,</publication_title>
      <place_in_publication>33. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus amblystegium;species molle;variety alpinum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants soft, pale-yellow, brownish yellow-green, or bright green.</text>
      <biological_entity id="o13031" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish yellow-green" value_original="brownish yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish yellow-green" value_original="brownish yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 5 cm, leafy throughout, irregularly branched;</text>
      <biological_entity id="o13032" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, epidermal-cells small, walls thick, similar to subadjacent cortical cells, central strand hyaline to brownish.</text>
      <biological_entity id="o13033" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13034" name="epidermal-cell" name_original="epidermal-cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o13035" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13036" name="cortical" name_original="cortical" src="d0_s2" type="structure" />
      <biological_entity id="o13037" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="central" id="o13038" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character char_type="range_value" from="hyaline" name="coloration" src="d0_s2" to="brownish" />
      </biological_entity>
      <relation from="o13035" id="r1834" name="to" negation="false" src="d0_s2" to="o13036" />
      <relation from="o13035" id="r1835" name="to" negation="false" src="d0_s2" to="o13037" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves appressed-imbricate and plane or erect-spreading and convoluted-ruffled parallel to long axis of leaf, straight, sometimes secund, spreading when dry or moist, oval or orbicular, sometimes broadly ovate, concave, (1–) 1.2–1.6 (–1.8) × (0.9–) 1–1.7 (–1.8) mm;</text>
      <biological_entity id="o13039" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="appressed-imbricate" value_original="appressed-imbricate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o13040" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="when dry or moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s3" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" modifier="sometimes broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s3" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s3" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_width" src="d0_s3" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13041" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <relation from="o13040" id="r1836" name="part_of" negation="false" src="d0_s3" to="o13041" />
    </statement>
    <statement id="d0_s4">
      <text>margins plane, entire, less often finely denticulate or serrulate in apex;</text>
      <biological_entity id="o13042" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="less often; often finely" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character constraint="in apex" constraintid="o13043" is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13043" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex broadly acute to short-apiculate;</text>
      <biological_entity id="o13044" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s5" to="short-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa slender, usually double and short, sometimes single and 2-fid;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells irregular to rectangular, walls thin, usually hyaline, region clearly differentiated, ± rectangular, long axis parallel to margins;</text>
      <biological_entity id="o13045" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o13046" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s7" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o13047" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o13048" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="clearly" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o13049" name="axis" name_original="axis" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character constraint="to margins" constraintid="o13050" is_modifier="false" name="arrangement" src="d0_s7" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o13050" name="margin" name_original="margins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>basal laminal cells shorter, longer, or similar to medial cells, walls usually thicker, hyaline;</text>
      <biological_entity constraint="basal laminal" id="o13051" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="medial" id="o13052" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o13053" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="width" src="d0_s8" value="thicker" value_original="thicker" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o13051" id="r1837" name="to" negation="false" src="d0_s8" to="o13052" />
    </statement>
    <statement id="d0_s9">
      <text>medial cells short-fusiform to linear-flexuose, 35–60 (–64) × (4–) 5–6 µm; apical cells shorter;</text>
      <biological_entity constraint="medial" id="o13054" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-fusiform" value_original="short-fusiform" />
        <character is_modifier="false" name="course" src="d0_s9" value="linear-flexuose" value_original="linear-flexuose" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="64" to_unit="um" />
        <character char_type="range_value" from="35" from_unit="um" name="length" src="d0_s9" to="60" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="atypical_width" src="d0_s9" to="5" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s9" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="apical" id="o13055" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>marginal cells similar to medial cells.</text>
      <biological_entity constraint="medial" id="o13057" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <relation from="o13056" id="r1838" name="to" negation="false" src="d0_s10" to="o13057" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="marginal" id="o13056" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perichaetial inner leaves linear-lanceolate to linear, plicae 0–4, deep, margins coarsely serrate at apex, ecostate, costa faintly single, or double, few apical laminal cells coarsely papillose abaxially by distally over-riding end walls.</text>
      <biological_entity constraint="perichaetial inner" id="o13058" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s12" to="linear" />
      </biological_entity>
      <biological_entity id="o13059" name="plica" name_original="plicae" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o13060" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character constraint="at apex" constraintid="o13061" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s12" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o13061" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o13062" name="costa" name_original="costa" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="faintly" name="quantity" src="d0_s12" value="single" value_original="single" />
      </biological_entity>
      <biological_entity constraint="apical laminal" id="o13063" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
        <character constraint="by end walls" constraintid="o13064" is_modifier="false" modifier="coarsely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="end" id="o13064" name="wall" name_original="walls" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta yellow-red, orange-red, or red, 0.8–2 cm.</text>
      <biological_entity id="o13065" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-red" value_original="yellow-red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s13" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule with endostome cilia rudimentary or absent.</text>
      <biological_entity id="o13066" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="endostome" id="o13067" name="cilium" name_original="cilia" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o13066" id="r1839" name="with" negation="false" src="d0_s14" to="o13067" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Irrigated emergent, acidic rock in montane streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic rock" modifier="irrigated emergent" constraint="in montane streams" />
        <character name="habitat" value="montane streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (200-900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="200" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Mont., Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hygrohypnum alpinum is disjunct from Europe to western North America. Broadly ovate to regularly orbicular leaves coupled with a rectangular group of thin-walled, enlarged, irregular to rectangular alar cells make the species distinctive. The convoluted ruffling of the leaves and the abaxial papillae of the inner perichaetial leaves are additional distinguishing traits. The stems are ascending to erect. The leaves often have excavated alar cells and are sometimes folded at the apex.</discussion>
  
</bio:treatment>