<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
    <other_info_on_meta type="illustration_page">437</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="(Cardot) Brotherus in H. G. A. Engler and K. Prantl" date="unknown" rank="genus">homalotheciella</taxon_name>
    <taxon_name authority="(Hedwig) Brotherus in H. G. A. Engler and K. Prantl" date="1908" rank="species">subcapillata</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>232/233[I,3]: 1133. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus homalotheciella;species subcapillata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099144</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pterigynandrum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">subcapillatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>83, plate 16, figs. 7 – 12. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus pterigynandrum;species subcapillatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Burnettia</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="species">fabrofolia</taxon_name>
    <taxon_hierarchy>genus burnettia;species fabrofolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Homalotheciella</taxon_name>
    <taxon_name authority="(Grout) Brotherus" date="unknown" rank="species">fabrofolia</taxon_name>
    <taxon_hierarchy>genus homalotheciella;species fabrofolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Homalothecium</taxon_name>
    <taxon_name authority="(Hedwig) Sullivant" date="unknown" rank="species">subcapillatum</taxon_name>
    <taxon_hierarchy>genus homalothecium;species subcapillatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pterogonium</taxon_name>
    <taxon_name authority="Schwägrichen" date="unknown" rank="species">decumbens</taxon_name>
    <taxon_hierarchy>genus pterogonium;species decumbens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pterigynandrum</taxon_name>
    <taxon_name authority="P. Beauvois ex Bridel" date="unknown" rank="species">brachycladon</taxon_name>
    <taxon_hierarchy>genus pterigynandrum;species brachycladon</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glossy.</text>
      <biological_entity id="o359" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2 cm, branches short, ascending-erect, straight or often curved.</text>
      <biological_entity id="o360" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="cm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o361" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s1" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 0.7–1.3 × 0.3–0.5 mm;</text>
      <biological_entity id="o362" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s2" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane;</text>
    </statement>
    <statement id="d0_s4">
      <text>alar cells in 2–5 vertical rows;</text>
      <biological_entity id="o363" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o364" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o365" name="row" name_original="rows" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o364" id="r52" name="in" negation="false" src="d0_s4" to="o365" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cells 40–60 × 5 µm, smooth.</text>
      <biological_entity constraint="laminal" id="o366" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s5" to="60" to_unit="um" />
        <character name="width" src="d0_s5" unit="um" value="5" value_original="5" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perigonia gemmiform, axillary on stem.</text>
      <biological_entity id="o367" name="perigonium" name_original="perigonia" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="gemmiform" value_original="gemmiform" />
        <character constraint="on stem" constraintid="o368" is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o368" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Seta 0.5–1 cm, straight.</text>
      <biological_entity id="o369" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule exserted, 1–1.5 mm, smooth or wrinkled-plicate when dry;</text>
      <biological_entity id="o370" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s8" value="wrinkled-plicate" value_original="wrinkled-plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>exothecial cells oblong-rectangular;</text>
      <biological_entity constraint="exothecial" id="o371" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-rectangular" value_original="oblong-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stomata few, proximal on capsule;</text>
      <biological_entity id="o372" name="stoma" name_original="stomata" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character constraint="on capsule" constraintid="o373" is_modifier="false" name="position" src="d0_s10" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o373" name="capsule" name_original="capsule" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>peristome pale;</text>
      <biological_entity id="o374" name="peristome" name_original="peristome" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exostome teeth pale yellow-orange, 300 µm, external surface finely cross-striolate, internal surface transversely ridged, fimbriate-bordered, papillose at apices.</text>
      <biological_entity constraint="exostome" id="o375" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale yellow-orange" value_original="pale yellow-orange" />
        <character name="some_measurement" src="d0_s12" unit="um" value="300" value_original="300" />
      </biological_entity>
      <biological_entity constraint="external" id="o376" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s12" value="cross-striolate" value_original="cross-striolate" />
      </biological_entity>
      <biological_entity constraint="internal" id="o377" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s12" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="fimbriate-bordered" value_original="fimbriate-bordered" />
        <character constraint="at apices" constraintid="o378" is_modifier="false" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o378" name="apex" name_original="apices" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Calyptra 2 mm.</text>
      <biological_entity id="o379" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Sep–Feb.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Feb" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Trunks of soft-barked trees, logs, roots, stumps, rock, mesic to wet forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="trunks" constraint="of soft-barked trees , logs , roots , stumps , rock , mesic to wet forests" />
        <character name="habitat" value="soft-barked trees" />
        <character name="habitat" value="roots" modifier="logs" />
        <character name="habitat" value="stumps" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="wet forests" />
        <character name="habitat" value="mesic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Ky., La., Maine, Md., Miss., Mo., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Homalotheciella subcapillata is distinguished as small, glossy, creeping, pinnate plants bearing leaves with long, slender tips crowded on short branches. The species is easy to recognize under the microscope by its leaves with denticulate margins, weak costa, and quadrate alar cells. The delicately haired calyptra also is distinctive among Brachytheciaceae, as most species of the family have naked calyptrae. Pylaisiella selwynii (Hypnaceae) can be very similar to H. subcapillata, but differs in its more regularly curved branches and ecostate entire-margined leaves.</discussion>
  
</bio:treatment>