<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">435</other_info_on_meta>
    <other_info_on_meta type="illustration_page">434</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="H. A. Crum &amp; L. E. Anderson" date="unknown" rank="genus">donrichardsia</taxon_name>
    <taxon_name authority="(Grout) H. A. Crum &amp; L. E. Anderson" date="1979" rank="species">macroneuron</taxon_name>
    <place_of_publication>
      <publication_title>Fieldiana, Bot., n. s.</publication_title>
      <place_in_publication>1: 7. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus donrichardsia;species macroneuron</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099082</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hygroamblystegium</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="species">macroneuronc</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>36: 1, figs. 1 – 5. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hygroamblystegium;species macroneuronc</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eurhynchium</taxon_name>
    <taxon_name authority="(Grout) H. A. Crum" date="unknown" rank="species">macroneuron</taxon_name>
    <taxon_hierarchy>genus eurhynchium;species macroneuron</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxyrrhynchium</taxon_name>
    <taxon_name authority="(Grout) Wynns" date="unknown" rank="species">macroneuron</taxon_name>
    <taxon_hierarchy>genus oxyrrhynchium;species macroneuron</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–14 cm, branches to 10 mm.</text>
      <biological_entity id="o18577" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18578" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves 0.9–1.5 (–1.8) × 0.4–0.8 mm;</text>
      <biological_entity id="o18579" name="stem-leaf" name_original="stem-leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s1" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s1" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa excurrent, percurrent, or ending 3–6 cells before apex, 1/5–1/2 leaf width, sometimes laterally spurred;</text>
      <biological_entity id="o18580" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o18581" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o18582" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s2" to="1/2" />
      </biological_entity>
      <relation from="o18580" id="r2664" name="ending" negation="false" src="d0_s2" to="o18581" />
      <relation from="o18580" id="r2665" name="before" negation="false" src="d0_s2" to="o18582" />
    </statement>
    <statement id="d0_s3">
      <text>alar cells almost undifferentiated;</text>
      <biological_entity id="o18583" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes laterally" name="width" src="d0_s2" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o18584" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="almost" name="prominence" src="d0_s3" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>laminal cells partly 2–4-stratose, 30–65 × 6–9 µm; basal-cells 7–10 µm wide.</text>
      <biological_entity constraint="laminal" id="o18585" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="partly" name="architecture" src="d0_s4" value="2-4-stratose" value_original="2-4-stratose" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s4" to="65" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s4" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18586" name="basal-cell" name_original="basal-cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s4" to="10" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Branch leaves 1–1.5 × 0.1–0.8 mm.</text>
      <biological_entity constraint="branch" id="o18587" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s5" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, shallowly submerged in calcareous springs at base of limestone slopes in narrow canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="calcareous springs" />
        <character name="habitat" value="base" constraint="of limestone slopes" />
        <character name="habitat" value="limestone slopes" />
        <character name="habitat" value="narrow canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="600" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The single known locality of Donrichardsia macroneuron is Seven Hundred Springs in Edwards County. This locality was described in detail by R. Wyatt and A. Stoneburner (1980), who also tried to find more localities in similar habitats nearby, but without success. Donrichardsia macroneuron has a very broad costa reminiscent of Hygroamblystegium noterophilum. The latter species has short, oblong-rhomboidal cells, never linear and flexuose.</discussion>
  
</bio:treatment>