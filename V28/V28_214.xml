<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence &amp; H. P. Ramsay" date="unknown" rank="genus">gemmabryum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">GEMMABRYUM</taxon_name>
    <taxon_name authority="(R. Wilczek &amp; Demaret) J. R. Spence" date="2007" rank="species">gemmilucens</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>89: 111. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus gemmabryum;section gemmabryum;species gemmilucens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099122</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="R. Wilczek &amp; Demaret" date="unknown" rank="species">gemmilucens</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Jard. Bot. Natl. Belg.</publication_title>
      <place_in_publication>46: 537, fig. 9. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species gemmilucens</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green or yellow-green.</text>
      <biological_entity id="o24227" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1 (–2) cm, gemmiform to evenly foliate.</text>
      <biological_entity id="o24228" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="gemmiform" value_original="gemmiform" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ovate, strongly concave, 0.5–1.5 (–2) mm;</text>
      <biological_entity id="o24229" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane to revolute proximally;</text>
      <biological_entity id="o24230" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="plane" modifier="proximally" name="shape" src="d0_s3" to="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o24231" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent to rarely short-excurrent;</text>
      <biological_entity id="o24232" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s5" to="rarely short-excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal laminal cells abruptly quadrate to short-rectangular, 1–2: 1;</text>
      <biological_entity constraint="proximal laminal" id="o24233" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="abruptly quadrate" name="shape" src="d0_s6" to="short-rectangular" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial and distal cells 8–12 (–16) µm wide, 3–4: 1.</text>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction by leaf-axil bulbils, bulbils (2–) 5–25 per axil, cylindric to spheric, 100–200 µm, primordia usually absent, occasionally present, short, peglike.</text>
      <biological_entity constraint="medial and distal" id="o24234" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="um" name="width" src="d0_s7" to="16" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity constraint="leaf-axil" id="o24235" name="bulbil" name_original="bulbils" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o24236" name="bulbil" name_original="bulbils" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" constraint="per axil" constraintid="o24237" from="5" name="quantity" src="d0_s8" to="25" />
        <character char_type="range_value" from="cylindric" name="shape" notes="" src="d0_s8" to="spheric" />
        <character char_type="range_value" from="100" from_unit="um" name="some_measurement" src="d0_s8" to="200" to_unit="um" />
      </biological_entity>
      <biological_entity id="o24237" name="axil" name_original="axil" src="d0_s8" type="structure" />
      <biological_entity id="o24238" name="primordium" name_original="primordia" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="peglike" value_original="peglike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule unknown.</text>
      <biological_entity id="o24239" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp soil, clearings, fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp soil" />
        <character name="habitat" value="clearings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; w, s Europe; Atlantic Islands (Canary Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gemmabryum gemmilucens is a distinctive Mediterranean climate species like G. gemmiferum, typically found on disturbed soil that receives rain in winter and is dry in spring and summer. The species is often associated with ephemeral bryophytes, and is much more common than G. gemmiferum. The leaf axil bulbils are yellow to pale orange or brown. A robust form on serpentine rock in central California has much broader leaves and lacks bulbils; it may represent a distinct species.</discussion>
  
</bio:treatment>