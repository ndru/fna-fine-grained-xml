<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
    <other_info_on_meta type="illustration_page">587</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">cryphaeaceae</taxon_name>
    <taxon_name authority="D. Mohr in F. Weber" date="unknown" rank="genus">cryphaea</taxon_name>
    <taxon_name authority="(Hooker &amp; Wilson) Müller Hal." date="1846" rank="species">nervosa</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>19: 211. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cryphaeaceae;genus cryphaea;species nervosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062397</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Daltonia</taxon_name>
    <taxon_name authority="Hooker &amp; Wilson" date="unknown" rank="species">nervosa</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>4: 420, plate 24, fig. B. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus daltonia;species nervosa</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems with branches short or elongate, simple.</text>
      <biological_entity id="o1105" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o1106" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o1105" id="r142" name="with" negation="false" src="d0_s0" to="o1106" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves widespreading when moist, 1–1.2 mm;</text>
      <biological_entity id="o1107" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>apex acuminate;</text>
      <biological_entity id="o1108" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent or nearly so, not spurred, tip not 2-fid.</text>
      <biological_entity id="o1109" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
        <character name="architecture" src="d0_s3" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o1110" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Perichaetia with inner leaves 1.6–1.9 mm, awn usually distinct, denticulate, 1/5–1/4 length expanded portion of leaf.</text>
      <biological_entity id="o1111" name="perichaetium" name_original="perichaetia" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o1112" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s4" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1113" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s4" to="1/4" />
      </biological_entity>
      <biological_entity id="o1114" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o1115" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <relation from="o1111" id="r143" name="with" negation="false" src="d0_s4" to="o1112" />
      <relation from="o1114" id="r144" name="part_of" negation="false" src="d0_s4" to="o1115" />
    </statement>
    <statement id="d0_s5">
      <text>Capsule with peristome double;</text>
      <biological_entity id="o1116" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <biological_entity id="o1117" name="peristome" name_original="peristome" src="d0_s5" type="structure" />
      <relation from="o1116" id="r145" name="with" negation="false" src="d0_s5" to="o1117" />
    </statement>
    <statement id="d0_s6">
      <text>exostome teeth single;</text>
      <biological_entity constraint="exostome" id="o1118" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>endostome segments irregularly linear.</text>
      <biological_entity constraint="endostome" id="o1119" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="irregularly" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra subcucullate to cucullate.</text>
      <biological_entity id="o1120" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character char_type="range_value" from="subcucullate" name="shape" src="d0_s8" to="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores papillose.</text>
      <biological_entity id="o1121" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Dec–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Apr" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Twigs, branches, trunks of trees, humid forests, swamp forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="twigs" />
        <character name="habitat" value="branches" />
        <character name="habitat" value="trunks" constraint="of trees , humid forests ," />
        <character name="habitat" value="trees" />
        <character name="habitat" value="humid forests" />
        <character name="habitat" value="forests" modifier="swamp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cryphaea nervosa usually grows in more humid habitats than does C. glomerata; the two species are often intermingled. The narrowly pointed leaves of C. nervosa make it easy to recognize in the field. The costa is often so prominent that the leaves of dry plants appear to be plicate.</discussion>
  
</bio:treatment>