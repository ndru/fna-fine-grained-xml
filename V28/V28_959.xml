<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Inés Sastre-De Jesús</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">610</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">neckeraceae</taxon_name>
    <taxon_name authority="Ireland" date="1974" rank="genus">NEOMACOUNIA</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>77: 454, figs. 1 – 24. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family neckeraceae;genus NEOMACOUNIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Macoun, 1831 – 1920 Canadian botanist and explorer</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">122012</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, shelf-forming, yellow-green, glossy.</text>
      <biological_entity id="o19243" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, sparsely and irregularly branched;</text>
      <biological_entity id="o19244" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="sparsely; irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent.</text>
      <biological_entity id="o19245" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem and branch leaves erect, imbricate, ovate to oblong-ovate, symmetric to asymmetric, concave, some finely plicate;</text>
      <biological_entity id="o19246" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="oblong-ovate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="symmetric to asymmetric" value_original="symmetric to asymmetric" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="finely" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o19247" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="oblong-ovate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="symmetric to asymmetric" value_original="symmetric to asymmetric" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="finely" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins serrulate at apex, teeth not recurved;</text>
      <biological_entity id="o19248" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o19249" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o19249" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o19250" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex abruptly acute to acuminate;</text>
    </statement>
    <statement id="d0_s6">
      <text>ecostate, costa double, or rarely single, short;</text>
      <biological_entity id="o19251" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="abruptly acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o19252" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal laminal cells irregularly rectangular, walls pitted.</text>
      <biological_entity constraint="basal laminal" id="o19253" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o19254" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perichaetial inner leaves oblong to obovate-oblong.</text>
      <biological_entity constraint="perichaetial inner" id="o19255" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="obovate-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 0.04–0.05 cm.</text>
      <biological_entity id="o19256" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.04" from_unit="cm" name="some_measurement" src="d0_s10" to="0.05" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule globose;</text>
      <biological_entity id="o19257" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome single;</text>
      <biological_entity id="o19258" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome teeth narrowly lanceolate, slightly striate basally, smooth distally.</text>
      <biological_entity constraint="exostome" id="o19259" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="slightly; basally" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 24–28 µm.</text>
      <biological_entity id="o19260" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="24" from_unit="um" name="some_measurement" src="d0_s14" to="28" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Neomacounia was described to accommodate a species placed originally in Forsstroemia (Leptodontaceae). Ireland used characters of capsule, seta, calyptra, branchlets, costa, leaves, distal and basal laminal cells, and pseudoparaphyllia to support the placement of this genus in Neckeraceae.</discussion>
  
</bio:treatment>