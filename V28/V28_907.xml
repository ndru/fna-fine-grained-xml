<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">578</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="mention_page">579</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">sematophyllaceae</taxon_name>
    <taxon_name authority="Loeske ex M. Fleischer" date="unknown" rank="genus">brotherella</taxon_name>
    <taxon_name authority="(Renauld &amp; Cardot) M. Fleischer" date="1923" rank="species">roellii</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Buitenzorg</publication_title>
      <place_in_publication>4: 1245. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sematophyllaceae;genus brotherella;species roellii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099038</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhaphidostegium</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="unknown" rank="species">roellii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Centralbl.</publication_title>
      <place_in_publication>44: 423. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus rhaphidostegium;species roellii</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, reclining, pale yellow-green.</text>
      <biological_entity id="o3359" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale yellow-green" value_original="pale yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–5 cm, to 1 mm wide across main leafy shoot, somewhat complanate-foliate, little or irregularly branched, branches sometimes flagelliform;</text>
      <biological_entity id="o3360" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" constraint="across branches" constraintid="o3362" from="0" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="main" id="o3361" name="shoot" name_original="shoot" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
      </biological_entity>
      <biological_entity id="o3362" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="flagelliform" value_original="flagelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia narrowly lanceolate.</text>
      <biological_entity id="o3363" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves slightly secund to falcate, occasionally almost erect, ovatelanceolate, tapering to apex, 0.8–1.2 cm;</text>
      <biological_entity id="o3364" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character is_modifier="false" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="occasionally almost" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="to apex" constraintid="o3365" is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3365" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>margins toothed in acumen;</text>
      <biological_entity id="o3367" name="acumen" name_original="acumen" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>alar cells 2 or 3, redbrown.</text>
      <biological_entity id="o3366" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="in acumen" constraintid="o3367" is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o3368" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta redbrown, 0.5–1 cm.</text>
      <biological_entity id="o3369" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule suberect, cylindric, symmetric to somewhat asymmetric;</text>
      <biological_entity id="o3370" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s8" value="symmetric to somewhat" value_original="symmetric to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s8" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>operculum rostrate.</text>
      <biological_entity id="o3371" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="winter" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks and bases, organic soil, forest margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="bases" />
        <character name="habitat" value="organic soil" />
        <character name="habitat" value="forest margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the genus, plants of Brotherella roellii are those of smallest stature in North America. It is the only autoicous species, thus sporophytes are fairly common. When sterile, however, B. roellii commonly produces readily deciduous flagelliferous shoots or branches. The species appears to be extinct in Washington; no specimens have been collected there for more than 70 years. In British Columbia, most collections have been made in secondary forests or on forest edges.</discussion>
  
</bio:treatment>