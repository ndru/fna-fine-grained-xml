<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">hylocomiaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">HYLOCOMIUM</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>5: 169, plates 487 – 493. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hylocomiaceae;genus HYLOCOMIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hylokomos, forest inhabitant, alluding to habitat</other_info_on_name>
    <other_info_on_name type="fna_id">116061</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping to more often arching-ascending, 1–3 mm wide across leafy stem, sympodial, rarely appearing monopodial in reduced arctic-alpine forms, regularly 2-pinnate or 3-pinnate;</text>
      <biological_entity id="o252" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="arching-ascending" value_original="arching-ascending" />
        <character char_type="range_value" constraint="across stem" constraintid="o253" from="1" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="sympodial" value_original="sympodial" />
        <character constraint="in forms" constraintid="o254" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="monopodial" value_original="monopodial" />
        <character is_modifier="false" modifier="regularly" name="architecture_or_shape" src="d0_s0" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="3-pinnate" value_original="3-pinnate" />
      </biological_entity>
      <biological_entity id="o253" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o254" name="form" name_original="forms" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="habitat" src="d0_s0" value="arctic-alpine" value_original="arctic-alpine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>paraphyllia many, base multiseriate, branches 1-seriate or 2-seriate.</text>
      <biological_entity id="o255" name="paraphyllium" name_original="paraphyllia" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o256" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="multiseriate" value_original="multiseriate" />
      </biological_entity>
      <biological_entity id="o257" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="1-seriate" value_original="1-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves tightly appressed on stipe to erect among branches, heteromallous, not crowded, oblong-ovate to ovate, lightly plicate, sometimes rugose in acumen, 1.6–3.2 (–4) mm;</text>
      <biological_entity id="o258" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character constraint="on stipe" constraintid="o259" is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="arrangement" notes="" src="d0_s2" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s2" to="ovate" />
        <character is_modifier="false" modifier="lightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character constraint="in acumen" constraintid="o261" is_modifier="false" modifier="sometimes" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_distance" notes="" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="distance" notes="" src="d0_s2" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o259" name="stipe" name_original="stipe" src="d0_s2" type="structure">
        <character constraint="among branches" constraintid="o260" is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o260" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o261" name="acumen" name_original="acumen" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>base not decurrent;</text>
      <biological_entity id="o262" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins serrulate to nearly entire proximally, serrate to serrulate distally;</text>
      <biological_entity id="o263" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="serrulate" modifier="proximally" name="architecture_or_shape" src="d0_s4" to="nearly entire" />
        <character char_type="range_value" from="serrate" modifier="distally" name="architecture_or_shape" src="d0_s4" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex abruptly acuminate, or occasionally rounded, obtuse, or abruptly acute;</text>
      <biological_entity id="o264" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa double, 1/4–1/2 leaf length, sometimes nearly ecostate;</text>
      <biological_entity id="o265" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s6" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells not differentiated;</text>
      <biological_entity id="o266" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o267" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminal cells occasionally minutely prorate.</text>
      <biological_entity constraint="laminal" id="o268" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovate to elliptic-lanceolate.</text>
      <biological_entity constraint="branch" id="o269" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="elliptic-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule inclined to pendent;</text>
      <biological_entity id="o270" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s10" to="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>operculum conic, obliquely long-rostrate;</text>
      <biological_entity id="o271" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s11" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exostome teeth reticulate proximally;</text>
      <biological_entity constraint="exostome" id="o272" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_coloration_or_relief" src="d0_s12" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>endostome segments broadly perforate, usually split into one large gaping perforation.</text>
      <biological_entity constraint="endostome" id="o273" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s13" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o274" name="split" name_original="split" src="d0_s13" type="structure" />
      <biological_entity id="o275" name="perforation" name_original="perforation" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="gaping" value_original="gaping" />
      </biological_entity>
      <relation from="o274" id="r41" name="into" negation="false" src="d0_s13" to="o275" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, n Africa, Pacific Islands (New Zealand), Australia; cool temperate, boreal, and arctic regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="cool temperate" establishment_means="native" />
        <character name="distribution" value="boreal" establishment_means="native" />
        <character name="distribution" value="and arctic regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>