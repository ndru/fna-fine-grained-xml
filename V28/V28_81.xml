<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">69</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Sullivant &amp; Lesquereux in C. F. Austin" date="1870" rank="species">ohioense</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. Austin, Musci Appalach.,</publication_title>
      <place_in_publication>169. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species ohioense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250061905</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthotrichum</taxon_name>
    <taxon_name authority="Sullivant &amp; Lesquereux" date="unknown" rank="species">citrinum</taxon_name>
    <taxon_hierarchy>genus orthotrichum;species citrinum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ohioense</taxon_name>
    <taxon_name authority="(Sullivant &amp; Lesquereux) Lesquereux &amp; James" date="unknown" rank="variety">citrinum</taxon_name>
    <taxon_hierarchy>genus o.;species ohioense;variety citrinum</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.4–1.4 cm.</text>
      <biological_entity id="o2773" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s0" to="1.4" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves stiff, loosely erect-appressed when dry, narrowly lanceolate to oblong-lanceolate, 1.4–2.9 mm;</text>
      <biological_entity id="o2774" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s1" value="erect-appressed" value_original="erect-appressed" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s1" to="oblong-lanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="distance" src="d0_s1" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins strongly revolute to just before apex, entire;</text>
      <biological_entity id="o2775" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="just before apex" constraintid="o2776" is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2776" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apex obtuse to bluntly acute, not incurved when dry;</text>
      <biological_entity id="o2777" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="bluntly acute" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s3" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells subquadrate to short-rectangular, walls thin, not nodose;</text>
      <biological_entity constraint="basal laminal" id="o2778" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s4" to="short-rectangular" />
      </biological_entity>
      <biological_entity id="o2779" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cells 8–10 µm, 1-stratose, lumina irregular due to uneven wall thickenings, papillae 1–2 per cell, conic, low.</text>
      <biological_entity constraint="distal" id="o2780" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s5" to="10" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o2781" name="lumen" name_original="lumina" src="d0_s5" type="structure" />
      <biological_entity constraint="wall" id="o2782" name="thickening" name_original="thickenings" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s5" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="variability" src="d0_s5" value="due-to-uneven" value_original="due-to-uneven" />
      </biological_entity>
      <biological_entity id="o2784" name="cell" name_original="cell" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity id="o2783" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o2784" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="position" src="d0_s5" value="low" value_original="low" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Vaginula with hairs absent.</text>
      <biological_entity id="o2785" name="vaginulum" name_original="vaginula" src="d0_s8" type="structure" />
      <biological_entity id="o2786" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o2785" id="r363" name="with" negation="false" src="d0_s8" to="o2786" />
    </statement>
    <statement id="d0_s9">
      <text>Seta to 1 mm.</text>
      <biological_entity id="o2787" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule emergent, oblong to ovate, 1–1.5 mm, 8-ribbed almost to base, ribs thin, delicate, not constricted below mouth;</text>
      <biological_entity id="o2788" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="location" src="d0_s10" value="emergent" value_original="emergent" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
        <character constraint="to base" constraintid="o2789" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="8-ribbed" value_original="8-ribbed" />
      </biological_entity>
      <biological_entity id="o2789" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o2790" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
        <character constraint="below mouth" constraintid="o2791" is_modifier="false" modifier="not" name="size" src="d0_s10" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o2791" name="mouth" name_original="mouth" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stomata immersed, mid capsule, 1/2 to completely covered by subsidiary-cells, cells projecting vertically over stomata, inner walls thickened;</text>
      <biological_entity id="o2792" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="immersed" value_original="immersed" />
      </biological_entity>
      <biological_entity constraint="mid" id="o2793" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o2794" name="subsidiary-cell" name_original="subsidiary-cells" src="d0_s11" type="structure" />
      <biological_entity id="o2795" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character constraint="over stomata" constraintid="o2796" is_modifier="false" name="orientation" src="d0_s11" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o2796" name="stoma" name_original="stomata" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o2797" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o2793" id="r364" modifier="completely" name="covered by" negation="false" src="d0_s11" to="o2794" />
    </statement>
    <statement id="d0_s12">
      <text>peristome double;</text>
      <biological_entity id="o2798" name="peristome" name_original="peristome" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>prostome absent;</text>
      <biological_entity id="o2799" name="prostome" name_original="prostome" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>exostome teeth 8, splitting to 16 with age, reflexed, finely and densely papillose;</text>
      <biological_entity constraint="exostome" id="o2800" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s14" value="splitting" value_original="splitting" />
        <character constraint="with age" constraintid="o2801" name="quantity" src="d0_s14" value="16" value_original="16" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="finely; densely" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o2801" name="age" name_original="age" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>endostome often absent, or segments 8, well developed, of 1 row of cells, smooth.</text>
      <biological_entity id="o2802" name="endostome" name_original="endostome" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2803" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o2804" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2805" name="cell" name_original="cells" src="d0_s15" type="structure" />
      <relation from="o2803" id="r365" name="consist_of" negation="false" src="d0_s15" to="o2804" />
      <relation from="o2804" id="r366" name="part_of" negation="false" src="d0_s15" to="o2805" />
    </statement>
    <statement id="d0_s16">
      <text>Calyptra oblong, smooth, naked or slightly hairy, hairs smooth.</text>
      <biological_entity id="o2806" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2807" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 15–19 µm.</text>
      <biological_entity id="o2808" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s17" to="19" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous trees in mesic hardwood forests, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous trees" constraint="in mesic hardwood forests" />
        <character name="habitat" value="mesic hardwood forests" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (100-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Ky., Maine, Md., Mass., Mich., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orthotrichum ohioense is distinguished by the 16 exostome teeth, eight rudimentary endostome segments, slightly 8-ribbed ovate capsules, leaves that are lanceolate and bluntly acute at the apices, and laminal cells smaller than 10 µm.</discussion>
  
</bio:treatment>