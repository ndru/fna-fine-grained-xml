<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">PTYCHOSTOMUM</taxon_name>
    <taxon_name authority="(Schwagrichen) J. R. Spence" date="2005" rank="species">schleicheri</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 22. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus ptychostomum;species schleicheri;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099326</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Schwägrichen" date="unknown" rank="species">schleicheri</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(2): 113, plate 73 [left]. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species schleicheri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">schleicheri</taxon_name>
    <taxon_name authority="(Schwägrichen) Schimper" date="unknown" rank="variety">latifolium</taxon_name>
    <taxon_hierarchy>genus b.;species schleicheri;variety latifolium</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense or open turfs, pale-yellow, yellow-green, or copper.</text>
      <biological_entity id="o24316" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="copper" value_original="copper" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="copper" value_original="copper" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24317" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o24316" id="r3447" name="in" negation="false" src="d0_s0" to="o24317" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–6 (–10) cm, fertile stems comose, innovations evenly foliate.</text>
      <biological_entity id="o24318" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24319" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
      <biological_entity id="o24320" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves yellow to yellow-copper, crowded, older leaves ± imbricate, younger leaves contorted to shrunken when dry, broadly ovate, weakly to strongly concave, 2–4 (–5) mm, not much enlarged toward stem apex;</text>
      <biological_entity id="o24321" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s2" to="yellow-copper" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o24322" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o24323" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="younger" value_original="younger" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="size" src="d0_s2" value="shrunken" value_original="shrunken" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s2" to="weakly strongly concave" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s2" to="weakly strongly concave" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o24324" is_modifier="false" modifier="not much" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="stem" id="o24324" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>base green, not or weakly decurrent;</text>
      <biological_entity id="o24325" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="not; weakly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or revolute proximally, plane distally, limbidium moderately strong, in 1–3 rows;</text>
      <biological_entity id="o24326" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o24327" name="limbidium" name_original="limbidium" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="fragility" src="d0_s4" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o24328" name="row" name_original="rows" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <relation from="o24327" id="r3448" name="in" negation="false" src="d0_s4" to="o24328" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute;</text>
      <biological_entity id="o24329" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent to short-excurrent, awn slender;</text>
      <biological_entity id="o24330" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s6" to="short-excurrent" />
      </biological_entity>
      <biological_entity id="o24331" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells rectangular, 3: 1;</text>
      <biological_entity constraint="proximal laminal" id="o24332" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial and distal cells 28–36 µm wide, 2: 1, walls thin.</text>
      <biological_entity constraint="medial and distal" id="o24333" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="28" from_unit="um" name="width" src="d0_s8" to="36" to_unit="um" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o24334" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta redbrown, 2–3 (–4) cm, slender, straight to somewhat flexuose.</text>
      <biological_entity id="o24335" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="3" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s11" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule yellowbrown, turbinate to pyriform, symmetric, 3–5 mm, mouth yellow;</text>
      <biological_entity id="o24336" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowbrown" value_original="yellowbrown" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s12" to="pyriform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24337" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum conic, rounded;</text>
      <biological_entity id="o24338" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome well developed;</text>
      <biological_entity id="o24339" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s14" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth yellow basally, hyaline distally, lamellae straight, pores absent near base along mid line;</text>
      <biological_entity constraint="exostome" id="o24340" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o24341" name="lamella" name_original="lamellae" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24342" name="pore" name_original="pores" src="d0_s15" type="structure">
        <character constraint="near base" constraintid="o24343" is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24343" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity constraint="mid" id="o24344" name="line" name_original="line" src="d0_s15" type="structure" />
      <relation from="o24343" id="r3449" name="along" negation="false" src="d0_s15" to="o24344" />
    </statement>
    <statement id="d0_s16">
      <text>endostome not adherent to exostome, basal membrane 1/2 exostome height, segments with ovate perforations, cilia appendiculate to nodose.</text>
      <biological_entity id="o24345" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character constraint="to exostome" constraintid="o24346" is_modifier="false" modifier="not" name="fusion" src="d0_s16" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o24346" name="exostome" name_original="exostome" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o24347" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o24348" name="exostome" name_original="exostome" src="d0_s16" type="structure" />
      <biological_entity id="o24349" name="segment" name_original="segments" src="d0_s16" type="structure" />
      <biological_entity id="o24350" name="perforation" name_original="perforations" src="d0_s16" type="structure">
        <character is_modifier="true" name="height" src="d0_s16" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o24351" name="cilium" name_original="cilia" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="appendiculate" value_original="appendiculate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="nodose" value_original="nodose" />
      </biological_entity>
      <relation from="o24349" id="r3450" name="with" negation="false" src="d0_s16" to="o24350" />
    </statement>
    <statement id="d0_s17">
      <text>Spores 16–20 (–22) µm, yellow to brown.</text>
      <biological_entity id="o24352" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s17" to="22" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s17" to="20" to_unit="um" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s17" to="brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, along streams, seepy tundra slopes, late snowmelt areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" constraint="along streams , seepy tundra slopes , late snowmelt areas" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="seepy tundra slopes" />
        <character name="habitat" value="late snowmelt areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-4000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T., Nunavut, Yukon; Alaska, Calif., Idaho, Mont., Oreg., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum schleicheri is an arctic-alpine species. When well developed, the plants are distinctively large, with long stems, and crowded, yellowish, somewhat imbricate leaves. Much material named P. schleicheri appears to be misidentified and can be referred to P. turbinatum. This species is atypical for Ptychostomum and shows some morphological similarities to Mniaceae, especially in the lack of tmema cells in the rhizoidal filiform gemmae, extremely wide laminal cells, and pronounced shelflike leaf base that remains attached to the stem after the leaf is removed. Ptychostomum subneodamense is similar but has a 1-stratose border, usually contorted and shrunken leaves when dry, and much smaller laminal cells.</discussion>
  
</bio:treatment>