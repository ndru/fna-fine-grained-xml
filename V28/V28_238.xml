<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">662</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">PLAGIOBRYOIDES</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 24. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus PLAGIOBRYOIDES</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Plagiobryum and Greek -oides, similarity</other_info_on_name>
    <other_info_on_name type="fna_id">317908</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in dense turfs, green, yellow-green, redbrown, pink, or red.</text>
      <biological_entity id="o9628" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9629" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o9628" id="r1363" name="in" negation="false" src="d0_s0" to="o9629" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–4 cm, evenly foliate, not or rarely weakly julaceous, somewhat branched;</text>
      <biological_entity id="o9630" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
        <character is_modifier="false" modifier="not; rarely weakly" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids few (many in P. brachyneura), micronemata and macronemata absent from stems or present in clusters on proximal stem.</text>
      <biological_entity id="o9631" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character constraint="from " constraintid="o9633" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9632" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o9633" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves imbricate to somewhat contorted when dry, erect to erect-spreading when moist, ovate to suborbicular, flat to concave, 0.4–3 (–3.5) mm;</text>
      <biological_entity id="o9634" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate to somewhat" value_original="imbricate to somewhat" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s3" to="erect-spreading" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="suborbicular flat" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="suborbicular flat" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base decurrent or not;</text>
      <biological_entity id="o9635" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character name="shape" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane or sometimes recurved proximally, ± entire, 1–3-stratose, limbidium absent or present;</text>
      <biological_entity id="o9636" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes; proximally" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-stratose" value_original="1-3-stratose" />
      </biological_entity>
      <biological_entity id="o9637" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex obtuse to acute;</text>
      <biological_entity id="o9638" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa not reaching apex to very short-excurrent, apiculus smooth, guide cells absent or sometimes present proximally;</text>
      <biological_entity id="o9639" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o9640" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o9641" name="apiculu" name_original="apiculus" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o9639" id="r1364" name="reaching" negation="true" src="d0_s7" to="o9640" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells not differentiated from juxtacostal cells;</text>
      <biological_entity constraint="guide" id="o9642" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes; proximally" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9643" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="from cells" constraintid="o9644" is_modifier="false" modifier="not" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o9644" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>laminal areolation somewhat heterogeneous;</text>
      <biological_entity constraint="laminal" id="o9645" name="areolation" name_original="areolation" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="variability" src="d0_s9" value="heterogeneous" value_original="heterogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells usually long-rectangular, 3–5: 1;</text>
      <biological_entity constraint="proximal laminal" id="o9646" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="long-rectangular" value_original="long-rectangular" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="5" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>medial and distal cells highly variable, irregularly rhomboidal, sometimes ± quadrate near apex, usually wider than 16 µm, 1–4: 1, walls thin or incrassate, not pitted.</text>
      <biological_entity constraint="medial and distal" id="o9647" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="highly" name="variability" src="d0_s11" value="variable" value_original="variable" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s11" value="rhomboidal" value_original="rhomboidal" />
        <character constraint="near apex" constraintid="o9648" is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s11" value="quadrate" value_original="quadrate" />
        <character modifier="usually wider than" name="some_measurement" notes="" src="d0_s11" unit="um" value="16" value_original="16" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9648" name="apex" name_original="apex" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction absent or by rhizoidal tubers red, spheric, greater than 200 µm. Sexual condition dioicous or rarely synoicous;</text>
      <biological_entity id="o9649" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s12" value="synoicous" value_original="synoicous" />
      </biological_entity>
      <biological_entity id="o9650" name="rhizoidal" name_original="rhizoidal" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="200" from_unit="um" name="some_measurement" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9651" name="tuber" name_original="tubers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perigonia and perichaetia terminal, leaves same size as vegetative leaves or usually larger, not forming rosette, inner leaves little differentiated.</text>
      <biological_entity id="o9652" name="perigonium" name_original="perigonia" src="d0_s13" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s13" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9653" name="perichaetium" name_original="perichaetia" src="d0_s13" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s13" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9654" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <biological_entity id="o9655" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o9656" name="rosette" name_original="rosette" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="usually" name="size" src="d0_s13" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o9657" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="usually" name="size" src="d0_s13" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9658" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <biological_entity id="o9659" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o9660" name="rosette" name_original="rosette" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="usually" name="size" src="d0_s13" value="larger" value_original="larger" />
        <character is_modifier="false" name="variability" src="d0_s13" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o9661" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="usually" name="size" src="d0_s13" value="larger" value_original="larger" />
        <character is_modifier="false" name="variability" src="d0_s13" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9662" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <relation from="o9654" id="r1365" name="as" negation="false" src="d0_s13" to="o9655" />
      <relation from="o9654" id="r1366" name="as" negation="false" src="d0_s13" to="o9656" />
      <relation from="o9654" id="r1367" name="as" negation="false" src="d0_s13" to="o9657" />
      <relation from="o9656" id="r1368" name="as" negation="false" src="d0_s13" to="o9659" />
      <relation from="o9657" id="r1369" name="as" negation="false" src="d0_s13" to="o9659" />
      <relation from="o9656" id="r1370" name="as" negation="false" src="d0_s13" to="o9660" />
      <relation from="o9656" id="r1371" name="as" negation="false" src="d0_s13" to="o9661" />
      <relation from="o9657" id="r1372" name="as" negation="false" src="d0_s13" to="o9660" />
      <relation from="o9657" id="r1373" name="as" negation="false" src="d0_s13" to="o9661" />
    </statement>
    <statement id="d0_s14">
      <text>Seta single, straight to flexuose or twisted, rarely geniculate.</text>
      <biological_entity id="o9663" name="seta" name_original="seta" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character char_type="range_value" from="straight" name="course" src="d0_s14" to="flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule inclined to suberect, elongate-pyriform, not zygomorphic, 2–4 mm;</text>
      <biological_entity id="o9664" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s15" to="suberect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate-pyriform" value_original="elongate-pyriform" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="zygomorphic" value_original="zygomorphic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hypophysis differentiated, often elongate;</text>
      <biological_entity id="o9665" name="hypophysis" name_original="hypophysis" src="d0_s16" type="structure">
        <character is_modifier="false" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s16" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum conic, apiculate or not, not rostrate;</text>
      <biological_entity id="o9666" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="apiculate" value_original="apiculate" />
        <character name="architecture_or_shape" src="d0_s17" value="not" value_original="not" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>peristome double;</text>
      <biological_entity id="o9667" name="peristome" name_original="peristome" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>exostome pale-yellow or tan proximally, hyaline distally, teeth lanceolate;</text>
      <biological_entity id="o9668" name="exostome" name_original="exostome" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s19" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o9669" name="tooth" name_original="teeth" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>endostome separate from exostome or sometimes adherent, basal membrane high, segments rarely longer than exostome, rarely absent, narrowly perforate, cilia absent or sometimes present.</text>
      <biological_entity id="o9670" name="endostome" name_original="endostome" src="d0_s20" type="structure">
        <character constraint="from " constraintid="o9672" is_modifier="false" name="arrangement" src="d0_s20" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity id="o9671" name="exostome" name_original="exostome" src="d0_s20" type="structure" />
      <biological_entity constraint="basal" id="o9672" name="membrane" name_original="membrane" src="d0_s20" type="structure">
        <character is_modifier="true" modifier="sometimes" name="fusion" src="d0_s20" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="height" src="d0_s20" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o9673" name="segment" name_original="segments" src="d0_s20" type="structure">
        <character constraint="than exostome" constraintid="o9674" is_modifier="false" name="length_or_size" src="d0_s20" value="rarely longer" value_original="rarely longer" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s20" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o9674" name="exostome" name_original="exostome" src="d0_s20" type="structure" />
      <biological_entity id="o9675" name="cilium" name_original="cilia" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Spores shed singly, 11–28 µm, papillose or smooth, pale-brown, tan, or yellow-tan.</text>
      <biological_entity id="o9676" name="spore" name_original="spores" src="d0_s21" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s21" value="singly" value_original="singly" />
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s21" to="28" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s21" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s21" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow-tan" value_original="yellow-tan" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow-tan" value_original="yellow-tan" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, se Asia, Africa, w Pacific Islands, Australia; tropical to warm-temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="w Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="tropical to warm-temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 15 (5 in the flora).</discussion>
  <discussion>Plagiobryoides is similar to Plagiobryum in gametophyte structure, but has more or less symmetric capsules with the endostome typically shorter than the exostome, and spores that are shed singly. The two genera are probably closely related, with primary speciation of Plagiobryum in arctic-alpine regions of the Northern Hemisphere and Plagiobryoides in the Tropics. Although the genus was originally described for the highly unusual Plagiobryoides incrassatolimbata, the overall leaf structure, especially the laminal areolation, is similar to that of the other species, with typically very broad thin-walled cells, becoming very long proximally, while the capsules are also similar in overall shape. As in Plagiobryum, the endostome segments can sometimes be longer than the exostome teeth, especially in some populations of Plagiobryoides cellularis. Several species have only recently been collected in the flora area, and are quite rare. B. H. Allen (2002) provided valuable information on the Neotropical species. The leaves are usually red at the base. The leaf apices are not hyaline; the innovation leaves are smaller than the cauline leaves, with more obtuse apices and weaker costae; the setae are red-brown; the capsules are brown; and the exostome teeth are trabeculate and without pores.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems often to 4 cm; leaves somewhat crowded; plants often red to red-brown; leaf bases long-decurrent; limbidium present, (1-)2-4-stratose; specialized asexual reproduction absent.</description>
      <determination>3 Plagiobryoides incrassatolimbata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually 0.5-2 cm; leaves crowded or distant; plants pale pink-green, green, yellow-green, or olive green; leaf bases not or somewhat decurrent; limbidium absent or 1-stratose; specialized asexual reproduction sometimes present</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Specialized asexual reproduction absent; sexual condition synoicous; peristome reduced; exostome teeth irregular, often short; endostome adherent to exostome, cilia absent; spores 18-22 µm.</description>
      <determination>1 Plagiobryoides brachyneura</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Specialized asexual reproduction sometimes present; sexual condition dioicous; peristome when present well developed; exostome teeth lanceolate, long; endostome not adherent to exostome, cilia usually absent, occasionally 1 or 2; spores 18-28 µm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf apices broadly acute to obtuse; costae not reaching apex to rarely percurrent; leaves concave; bases somewhat decurrent.</description>
      <determination>4 Plagiobryoides renauldii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf apices acute; costae reaching apex to very short-excurrent; leaves flat or weakly concave; bases not decurrent</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal laminal cells 2-4:1; plants pale pink-green; leaves imbricate when dry; specialized asexual reproduction absent; capsules with hypophysis strongly differentiated, elongate.</description>
      <determination>2 Plagiobryoides cellularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal laminal cells 1-3:1; plants bright green; leaves somewhat contorted to imbricate when dry; specialized asexual reproduction by rhizoidal tubers; capsules with hypophysis weakly differentiated, short.</description>
      <determination>5 Plagiobryoides vinosula</determination>
    </key_statement>
  </key>
</bio:treatment>