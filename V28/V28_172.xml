<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="illustration_page">110</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">philonotis</taxon_name>
    <taxon_name authority="Lindberg" date="1867" rank="species">capillaris</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>6: 40. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus philonotis;species capillaris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062081</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, delicate, scattered or in lax tufts, yellowish to light green.</text>
      <biological_entity id="o14882" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="in lax tufts" value_original="in lax tufts" />
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s0" to="light green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14883" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
      </biological_entity>
      <relation from="o14882" id="r2100" name="in" negation="false" src="d0_s0" to="o14883" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 cm, weakly erect to procumbent, simple, tomentose proximally.</text>
      <biological_entity id="o14884" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect when dry, erect-spreading when moist, lanceolate, 0.5–1.5 mm;</text>
      <biological_entity id="o14885" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane to narrowly revolute, bluntly serrulate nearly to base, teeth single, projecting from distal ends of marginal cells;</text>
      <biological_entity id="o14886" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s3" to="narrowly revolute" />
        <character constraint="to base" constraintid="o14887" is_modifier="false" modifier="bluntly" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o14887" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o14888" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character constraint="from distal ends" constraintid="o14889" is_modifier="false" name="orientation" src="d0_s3" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14889" name="end" name_original="ends" src="d0_s3" type="structure" />
      <biological_entity constraint="marginal" id="o14890" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <relation from="o14889" id="r2101" name="part_of" negation="false" src="d0_s3" to="o14890" />
    </statement>
    <statement id="d0_s4">
      <text>apex acuminate;</text>
      <biological_entity id="o14891" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa excurrent, distal abaxial surface rough;</text>
      <biological_entity id="o14892" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o14893" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells quadrate to rectangular, less than 5: 1, prorulose at distal or sometimes proximal ends on abaxial side, prorulae not conspicuous, sometimes obscure, few;</text>
      <biological_entity constraint="laminal" id="o14894" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="rectangular" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="5" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="prominence" notes="" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o14895" name="distal" name_original="distal" src="d0_s6" type="structure" />
      <biological_entity id="o14896" name="end" name_original="ends" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s6" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14897" name="side" name_original="side" src="d0_s6" type="structure" />
      <relation from="o14894" id="r2102" name="at" negation="false" src="d0_s6" to="o14895" />
      <relation from="o14894" id="r2103" name="at" negation="false" src="d0_s6" to="o14896" />
      <relation from="o14896" id="r2104" name="on" negation="false" src="d0_s6" to="o14897" />
    </statement>
    <statement id="d0_s7">
      <text>basal-cells short-rectangular to quadrate, shorter, broader than distal, 40 × 7 µm; medial and distal cells oblong, 10–30 × 5–7 µm. Specialized asexual reproduction absent.</text>
      <biological_entity constraint="laminal" id="o14898" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="rectangular" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o14899" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="short-rectangular" name="shape" src="d0_s7" to="quadrate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character constraint="than distal cells" constraintid="o14900" is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character name="length" src="d0_s7" unit="um" value="40" value_original="40" />
        <character name="width" src="d0_s7" unit="um" value="7" value_original="7" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14900" name="cell" name_original="cells" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="medial and distal" id="o14901" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s7" to="7" to_unit="um" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonia discoid.</text>
      <biological_entity id="o14902" name="perigonium" name_original="perigonia" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="discoid" value_original="discoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 2–3 cm, straight or flexuose.</text>
      <biological_entity id="o14903" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s10" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule 1.3–2 mm.</text>
      <biological_entity id="o14904" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores subreniform, 20–26 µm.</text>
      <biological_entity id="o14905" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subreniform" value_original="subreniform" />
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s12" to="26" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy soil, humus, rock ledges, shady habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sandy soil" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="shady habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (50-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="50" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Oreg., Wash.; Europe; sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Philonotis capillaris is distinguished by the unpaired teeth of the leaf margin that project from the distal ends of the cells and by the relatively short laminal cells with prorulae at the distal ends. In the flora area, the species is of restricted distribution, limited to Pacific coastal habitats from Alaska to California with incursions eastward to the western slopes of the Idaho Rockies. The species is associated with an oceanic climate.</discussion>
  
</bio:treatment>