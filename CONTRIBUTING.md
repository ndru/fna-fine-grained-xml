# Contributing

## How to Contribute

When contributing, post comments and discuss changes you wish to make via [Issues](https://bitbucket.org/aafc-mbb/fna-data-curation/issues?status=new&status=open).

Please include the following information when creating an issue about an error found on the [FNA Beta Semantic MediaWiki (SMW)](http://beta.semanticfna.org/Main_Page):

| Item | Description |
|-|-|
| Volume\* | Issue found in this FNA Volume |
| Page | If available, the FNA print volume page number to refer to when validating the issue |
| Family | Issue found in the treatment with this Taxonomic family |
| Taxon\* | Issue found in the treatment with this Taxon name |
| Original\* | Original text containing the issue |
| Revised\* | Revised text with the issue resolved |
| Location\* | Issue found in this location within the FNA treatment (see below for acceptable values) |
| Category\* | Issue falls into this category (see below for acceptable values). Category describes the type of issue. |
| Reason\* | Issue was created for this reason (see below for acceptable values) |
| Authority (Source)\* | Who authorizes that the issue be fixed? |
| Authority (Date)\* | Date issue is raised |
| Fixed in FNA SMW? | Has the issue been fixed on the FNA SMW? |
| Fixed in efloras? | Has the issue been fixed on the equivalent efloras page? |
| Comments | Additional information |

\* **Required**

### Acceptable values

|  | Acceptable values |  | Acceptable values |  | Acceptable values |
|-|-|-|-|-|-|
| **Location** | Accepted name | **Category** | Authority | **Reason** | Addition to author |
|  | Basionym |  | Authorship |  | Addition to text |
|  | Common name |  | Accepted name publication |  | Author mistake |
|  | Description |  | Citation |  | Author update |
|  | Discussion |  | Common name |  | Error in text |
|  | Distribution map |  | Determination |  | Illegitimate name |
|  | Distribution statement |  | Discussion |  | Incorrect citation |
|  | Etymology |  | Distribution |  | Incorrect date |
|  | Key |  | Elevation |  | Incorrect page |
|  | Map |  | Epithet |  | Invalid author |
|  | Phenology |  | Etymology |  | Invalid authority |
|  | Reference |  | Habitat |  | Invalid location |
|  | Synonym |  | Morphology |  | Invalid map |
|  |  |  | Nomenclature |  | Invalid name |
|  |  |  | Phenology |  | Invalid synonym |
|  |  |  | Reference |  | Name conserved |
|  |  |  | Other |  | New synonym |
|  |  |  |  |  | Nomen nudum |

### Pull Requests

Feel free to propose changes by creating Pull Requests. If you don't have write access, editing a file will create a Fork of this project for you to save your proposed changes to. Submitting a change to a file will write it to a new Branch in your Fork, so you can send a Pull Request.

If this is your first time contributing on BitBucket, don't worry! Let us know if you have any questions.