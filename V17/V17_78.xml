<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">358</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Borsch, Kai Müller &amp; Eb. Fischer" date="unknown" rank="family">LINDERNIACEAE</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">MICRANTHEMUM</taxon_name>
    <taxon_name authority="(Chapman) Shinners" date="1964" rank="species">glomeratum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>1: 252. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linderniaceae;genus micranthemum;species glomeratum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Micranthemum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_name authority="Chapman" date="1892" rank="variety">glomeratum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S. ed.</publication_title>
      <place_in_publication>2 repr. 2, 690. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus micranthemum;species nuttallii;variety glomeratum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemianthus</taxon_name>
    <taxon_name authority="(Chapman) Pennell" date="unknown" rank="species">glomeratus</taxon_name>
    <taxon_hierarchy>genus hemianthus;species glomeratus</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Manatee mudflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves opposite or whorled;</text>
      <biological_entity id="o11639" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>petiole 0–0.2 mm;</text>
      <biological_entity id="o11640" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic to oblanceolate, 2.5–8.5 (–15) × 0.5–3.1 mm.</text>
      <biological_entity id="o11641" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s2" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 0.5–4 mm.</text>
      <biological_entity id="o11642" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers chasmogamous;</text>
      <biological_entity id="o11643" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>calyx irregularly symmetric, tube deeply lobed abaxially, 1.1–1.4 mm, tube 0.5–0.8 mm, lobes 0.5–0.7 mm;</text>
      <biological_entity id="o11644" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o11645" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="deeply; abaxially" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11646" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11647" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla strongly bilaterally symmetric, unilabiate or nearly so, 1.7–1.8 mm, longer than calyx, tube 0.5–0.8 mm, abaxial lobes 3, forming a prominent lip, 0.9–1 mm, lateral 0, adaxial (0–) 0.7–0.8 mm.</text>
      <biological_entity id="o11648" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="unilabiate" value_original="unilabiate" />
        <character name="architecture" src="d0_s6" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
        <character constraint="than calyx" constraintid="o11649" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o11649" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
      <biological_entity id="o11650" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11651" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11652" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11653" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o11651" id="r934" name="forming a" negation="false" src="d0_s6" to="o11652" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules 1.1–1.3 × 0.9–1.2 mm.</text>
      <biological_entity id="o11654" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s7" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds ellipsoid, 0.2–0.3 × 0.05–0.1 mm.</text>
      <biological_entity id="o11655" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s8" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s8" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Micranthemum glomeratum primarily occurs in the central part of the peninsula; it has not been found in the extreme southern counties. In the northern panhandle region, there is an outlier record from Gadsden County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins of lakes and streams, sandy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of lakes and streams , sandy soils" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>