<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">chamaedrys</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 13. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species chamaedrys</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Germander speedwell</other_name>
  <other_name type="common_name">véronique petit-chêne</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o23993" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, rarely erect, (7–) 10–30 (–50) cm, densely eglandular-hairy, hairs along stem in 2 prominent lines.</text>
      <biological_entity id="o23994" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o23995" name="hair" name_original="hairs" src="d0_s1" type="structure" />
      <biological_entity id="o23996" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <biological_entity id="o23997" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o23995" id="r1840" name="along" negation="false" src="d0_s1" to="o23996" />
      <relation from="o23996" id="r1841" name="in" negation="false" src="d0_s1" to="o23997" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade narrowly ovate to ovate-orbiculate, (10–) 12–30 (–42) × (6–) 10–22 (–30) mm, 1–2 times as long as wide, base truncate to ± cordate, margins crenate to deeply incised, apex obtuse, surfaces variably hairy.</text>
      <biological_entity id="o23998" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23999" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s2" to="ovate-orbiculate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s2" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="42" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="22" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1-2" value_original="1-2" />
      </biological_entity>
      <biological_entity id="o24000" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="more or less cordate" />
      </biological_entity>
      <biological_entity id="o24001" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s2" to="deeply incised" />
      </biological_entity>
      <biological_entity id="o24002" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24003" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="variably" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1–4, axillary, 40–100 (–200) mm, 15–40 (–60) -flowered, axis eglandular-hairy, sometimes also glandular-hairy;</text>
      <biological_entity id="o24004" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="200" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s3" to="100" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="15-40(-60)-flowered" value_original="15-40(-60)-flowered" />
      </biological_entity>
      <biological_entity id="o24005" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts linear-elliptic, 3–7 mm.</text>
      <biological_entity id="o24006" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="linear-elliptic" value_original="linear-elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels suberect, (3–) 5–8 (–10) mm, equal to or shorter than subtending bract, eglandular and glandular-hairy.</text>
      <biological_entity id="o24007" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="suberect" value_original="suberect" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="than subtending bract" constraintid="o24008" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o24008" name="bract" name_original="bract" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 4-lobed, lobes 2–8 mm, apex acute, eglandular and, sometimes, glandular-hairy;</text>
      <biological_entity id="o24009" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o24010" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
      <biological_entity id="o24011" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24012" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla blue obscure darker nerves and sometimes whitish margin, (6–) 10–14 (–17) mm diam.;</text>
      <biological_entity id="o24013" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24014" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" notes="" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="diameter" notes="" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" notes="" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24015" name="nerve" name_original="nerves" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o24016" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="darker" value_original="darker" />
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 4.5–6.5 mm;</text>
      <biological_entity id="o24017" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24018" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style (2.5–) 4–5 mm.</text>
      <biological_entity id="o24019" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24020" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules strongly compressed in cross-section, obcordiform to obdeltoid, (2–) 3.5–4 (–5) × (3.5–) 4–5 (–5.5) mm, apex ± emarginate, eglandular-hairy.</text>
      <biological_entity id="o24021" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o24022" is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="obcordiform" name="shape" notes="" src="d0_s10" to="obdeltoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s10" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_width" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24022" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o24023" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds (2–) 12–20 (–28), yellow, ellipsoid, flat, 1.1–1.7 × 0.6–1.5 mm, 0.2–0.4 mm thick, smooth to subrugose.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16, 32 (Eurasia).</text>
      <biological_entity id="o24024" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="12" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="28" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character name="thickness" src="d0_s11" unit="mm" value="1.1-1.7×0.6-1.5" value_original="1.1-1.7×0.6-1.5" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s11" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24025" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The description provided here for Veronica chamaedrys is solely for the tetraploid cytotype, most probably the exclusive cytotype in the flora area and in central and western Europe. The diploid cytotype is so far only known from eastern and southeastern Europe (K. E. Bardy et al. 2010). A significant change in morphology can occur in shaded habitats, in which especially the petiole can be elongated beyond the range given.</discussion>
  <discussion>It is unclear whether Veronica chamaedrys is introduced throughout the flora area; it may be native in northeastern areas of North America.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich soils, deciduous forests, forest edges, roadsides, chaparral, scrub, meadows, lawns.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich soils" />
        <character name="habitat" value="deciduous forests" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="lawns" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Alaska, Conn., D.C., Idaho, Ill., Ind., Maine, Md., Mass., Mich., Mo., Mont., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., Vt., Va., Wash., W.Va., Wis.; Eurasia; introduced in South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="in South America (Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>