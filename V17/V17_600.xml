<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="Brandegee" date="1899" rank="species">floridus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 454. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species floridus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>231.</number>
  <other_name type="common_name">Panamint beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o3278" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, 50–120 cm, glaucous.</text>
      <biological_entity id="o3279" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline 45–80 (–100) × 15–30 (–40) mm, blade obovate to ovate, oblanceolate, or lanceolate, base tapered, margins coarsely dentate, apex obtuse to acute;</text>
      <biological_entity id="o3280" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3281" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="ovate oblanceolate or lanceolate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="ovate oblanceolate or lanceolate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="ovate oblanceolate or lanceolate" />
      </biological_entity>
      <biological_entity id="o3282" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o3283" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o3284" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 6–9 pairs, sessile or proximals short-petiolate, 50–100 × (8–) 20–45 mm, blade ovate to lanceolate, base cordate-clasping or tapered, margins coarsely dentate, apex acute, sometimes obtuse.</text>
      <biological_entity id="o3285" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o3286" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="9" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3287" name="proximal" name_original="proximals" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3288" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o3289" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="cordate-clasping" value_original="cordate-clasping" />
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o3290" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o3291" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, secund, 30–80 cm, axis glabrous or sparsely glandular-pubescent distally, verticillasters 9–19, cymes 1–5-flowered;</text>
      <biological_entity id="o3292" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s4" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3293" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o3294" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s4" to="19" />
      </biological_entity>
      <biological_entity id="o3295" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, 10–55 × 2–30 mm;</text>
      <biological_entity constraint="proximal" id="o3296" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glandular-pubescent.</text>
      <biological_entity id="o3297" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o3298" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 4.2–6 × 2–3 mm, glandular-pubescent;</text>
      <biological_entity id="o3299" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o3300" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to light pink, rose-pink, or light yellow, with reddish nectar guides, strongly bilabiate, ventricose, 21–32 mm, glandular-pubescent externally, glandular-pubescent internally, tube 6–12 mm, length 2–2.2 times calyx lobes, throat gradually to abruptly inflated, constricted or slightly so at orifice, 6–16 mm diam., rounded abaxially;</text>
      <biological_entity id="o3301" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3302" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="light pink rose-pink or light yellow" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="light pink rose-pink or light yellow" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="light pink rose-pink or light yellow" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s8" to="32" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o3303" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o3304" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character constraint="lobe" constraintid="o3305" is_modifier="false" name="length" src="d0_s8" value="2-2.2 times calyx lobes" value_original="2-2.2 times calyx lobes" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o3305" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o3306" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character name="size" src="d0_s8" value="slightly" value_original="slightly" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" notes="" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3307" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o3302" id="r305" name="with" negation="false" src="d0_s8" to="o3303" />
      <relation from="o3306" id="r306" name="at" negation="false" src="d0_s8" to="o3307" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, filaments glabrous, pollen-sacs explanate, 1.4–1.8 mm, sutures smooth;</text>
      <biological_entity id="o3308" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3309" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o3310" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3311" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="distance" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3312" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 16–19 mm, included, 0.7–0.9 mm diam., tip straight, glabrous;</text>
      <biological_entity id="o3313" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3314" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="diameter" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3315" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 15–20 mm, glabrous.</text>
      <biological_entity id="o3316" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3317" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 8–13 × 4–7 mm, glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o3318" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3319" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The two varieties of Penstemon floridus are sympatric in the southern Silver Peak Range and southern White Mountains, where they apparently hybridize and introgress (N. H. Holmgren 1984).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 24–32 mm, tubes 7–12 mm, throats abruptly inflated, constricted at orifices, (10–)11–16 mm diam.</description>
      <determination>231a. Penstemon floridus var. floridus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 21–27 mm, tubes 6–9 mm, throats gradually inflated, slightly constricted at orifices, 6–10(–11) mm diam.</description>
      <determination>231b. Penstemon floridus var. austinii</determination>
    </key_statement>
  </key>
</bio:treatment>