<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="2012" rank="species">inamoena</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 44. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species inamoena</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greene" date="1903" rank="species">inamoenus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>5: 137. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species inamoenus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray ex Bentham) A. Gray" date="unknown" rank="species">jamesii</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">texensis</taxon_name>
    <taxon_hierarchy>genus m.;species jamesii;variety texensis</taxon_hierarchy>
  </taxon_identification>
  <number>56.</number>
  <other_name type="common_name">Texas monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, fibrous-rooted, rooting at proximal nodes, sometimes forming matlike colonies.</text>
      <biological_entity id="o14786" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character constraint="at proximal nodes" constraintid="o14787" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14787" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <biological_entity id="o14788" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
      </biological_entity>
      <relation from="o14786" id="r1158" modifier="sometimes" name="forming" negation="false" src="d0_s0" to="o14788" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to decumbent-ascending basally, becoming fully erect at least in inflorescence, simple, sometimes few-branched from proximal nodes, usually distinctly fistulose, 10–30 cm, glabrous.</text>
      <biological_entity id="o14789" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" modifier="basally" name="orientation" src="d0_s1" to="decumbent-ascending" />
        <character constraint="in inflorescence" constraintid="o14790" is_modifier="false" modifier="becoming fully" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from proximal nodes" constraintid="o14791" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character is_modifier="false" modifier="usually distinctly" name="shape" notes="" src="d0_s1" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14790" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o14791" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal sometimes deciduous by flowering;</text>
      <biological_entity id="o14792" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14793" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="sometimes" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole: basal and proximals to mid cauline 10–70 mm, distals 0 mm;</text>
      <biological_entity id="o14794" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o14795" name="proximal" name_original="proximals" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="mid" value_original="mid" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14796" name="distal" name_original="distals" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately veined, ovate to broadly ovate or elliptic-ovate, becoming subreniform distally, (5–) 15–35 (–60) mm, distal closely paired, auriculate-subclasping, base truncate to subcordate, margins dentate-serrate to shallowly dentate, teeth 5–11 per side, apex obtuse to rounded, surfaces glabrous.</text>
      <biological_entity id="o14797" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o14798" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly ovate or elliptic-ovate" />
        <character is_modifier="false" modifier="becoming; distally" name="shape" src="d0_s4" value="subreniform" value_original="subreniform" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14799" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="auriculate-subclasping" value_original="auriculate-subclasping" />
      </biological_entity>
      <biological_entity id="o14800" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o14801" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="dentate-serrate to shallowly" value_original="dentate-serrate to shallowly" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o14802" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o14803" from="5" name="quantity" src="d0_s4" to="11" />
      </biological_entity>
      <biological_entity id="o14803" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o14804" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o14805" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers plesiogamous, (6–) 8–18 (–24), often produced from all nodes, loosely to densely racemose, chasmogamous.</text>
      <biological_entity id="o14807" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o14806" id="r1159" modifier="often" name="produced from all" negation="false" src="d0_s5" to="o14807" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 9–20 mm, glabrous, rarely sparsely stipitate-glandular.</text>
      <biological_entity id="o14808" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o14806" id="r1160" name="fruiting" negation="false" src="d0_s6" to="o14808" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces purple-spotted, sometimes greenish, broadly cylindric-campanulate, inflated, sagittally compressed, 7–11 mm, glabrous, rarely minutely scabrous-hirtellous or sparsely stipitate-glandular, throat not closing, abaxial lobe slightly upcurving 10–45º, spreading 45º, or sometimes deflexed 40º.</text>
      <biological_entity id="o14806" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s5" to="8" to_inclusive="false" />
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="24" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="18" />
        <character is_modifier="false" modifier="often; often; loosely to densely" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o14809" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o14810" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="cylindric-campanulate" value_original="cylindric-campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sagittally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely minutely" name="pubescence" src="d0_s7" value="scabrous-hirtellous" value_original="scabrous-hirtellous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o14811" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14812" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="deflexed" value_original="deflexed" />
      </biological_entity>
      <relation from="o14806" id="r1161" name="fruiting" negation="false" src="d0_s7" to="o14809" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, red-spotted, bilaterally symmetric, bilabiate;</text>
      <biological_entity id="o14813" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric, 7–11 mm, exserted (1–) 2–3 (–4) mm beyond calyx margin;</text>
      <biological_entity id="o14814" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="distance" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_distance" src="d0_s9" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="beyond calyx margin" constraintid="o14815" from="2" from_unit="mm" name="distance" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o14815" name="margin" name_original="margin" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>limb slightly expanded.</text>
      <biological_entity id="o14816" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o14817" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o14818" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 4.5–6 mm. 2n = 60.</text>
      <biological_entity id="o14819" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14820" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe inamoena is distinctive in its completely glabrous herbage, small corollas, flowers in racemes mostly at distal nodes and with reduced bracts, short and open-throated fruiting calyces, erect and fistulose stems, and apparent annual duration (fibrous-rooted but usually rooting at proximal cauline nodes).</discussion>
  <discussion>Presumably because of its autogamous reproduction, Erythranthe inamoena has been confused with E. cordata, especially in the trans-Pecos region of Texas, where the two are sympatric. In contrast to E. cordata, E. inamoena usually has glabrous pedicels and calyces, flowers often produced from all nodes, smaller calyces [(7–)8–11 mm] that do not close at maturity, and corollas with a shorter tube-throat (7–11 mm).</discussion>
  <discussion>Some populations in Brewster, Presidio, and Val Verde counties are identified here as Erythranthe inamoena (based on proximal-to-distal distribution of flowers and the short mature calyces with open throat) but have sparsely stipitate-glandular pedicels and calyces.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Apr(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Jan" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edge of seeps and creeks, mud or gravel, shallow running water, wet crevices, canyon drainages.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edge" constraint="of seeps and creeks , mud or gravel , shallow running water , wet crevices , canyon" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="mud" />
        <character name="habitat" value="gravel" />
        <character name="habitat" value="shallow running water" />
        <character name="habitat" value="wet crevices" />
        <character name="habitat" value="canyon" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>