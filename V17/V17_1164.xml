<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">654</other_info_on_meta>
    <other_info_on_meta type="mention_page">576</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Eastwood" date="1902" rank="species">scabrida</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 523. 1902</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species scabrida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>103.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.7–1.5 (–2.2) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot or stout, branched roots.</text>
      <biological_entity id="o6440" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2.2" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o6441" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <biological_entity id="o6442" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o6440" id="r552" name="with" negation="false" src="d0_s2" to="o6441" />
      <relation from="o6440" id="r553" name="with" negation="false" src="d0_s2" to="o6442" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several, decumbent, often sprawling, distally ascending, unbranched unless injured, rarely with small, leafy axillary shoots, hairs spreading, whitish, short, ± stiff, eglandular.</text>
      <biological_entity id="o6443" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s3" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6444" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o6445" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o6443" id="r554" modifier="rarely" name="with" negation="false" src="d0_s3" to="o6444" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves gray-green, sometimes green or reddish purple, reduced and scalelike on proximal 10–25% of stem, linear, lanceolate, or narrowly elliptic, 1.5–4 (–5) cm, not fleshy, margins plane, flat or involute, 0–3 (–5) -lobed, apex acute;</text>
      <biological_entity id="o6446" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="on proximal leaves" constraintid="o6447" is_modifier="false" name="shape" src="d0_s4" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6447" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6448" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o6449" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-3(-5)-lobed" value_original="0-3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o6450" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o6447" id="r555" modifier="10-25%" name="part_of" negation="false" src="d0_s4" to="o6448" />
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading or spreading-ascending, linear or lanceolate, apex acute.</text>
      <biological_entity id="o6451" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o6452" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2.5–10 × 2.5–5 cm;</text>
      <biological_entity id="o6453" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally green to deep greenish purple, distally bright red, sometimes brick-red or orange-red, linear to lanceolate, 3–5 (–7) -lobed;</text>
      <biological_entity id="o6454" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep greenish purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="bright red" value_original="bright red" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="brick-red" value_original="brick-red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange-red" value_original="orange-red" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading, linear-lanceolate to lanceolate, sometimes expanded near tip, long, arising near base and more distally, apex acute to obtuse.</text>
      <biological_entity id="o6455" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s8" to="lanceolate" />
        <character constraint="near tip" constraintid="o6456" is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s8" value="long" value_original="long" />
        <character constraint="near base" constraintid="o6457" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o6456" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o6457" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o6458" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" modifier="distally" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, 18–33 mm;</text>
      <biological_entity id="o6459" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o6460" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="33" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6460" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial clefts 6–12 mm, adaxial 8–15 mm, clefts 40–60% of calyx length, deeper than laterals, lateral 2–6 mm, 15–25% of calyx length;</text>
      <biological_entity constraint="abaxial" id="o6461" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6462" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6463" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character constraint="than laterals" constraintid="o6465" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6464" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o6465" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o6463" id="r556" modifier="40-60%" name="part_of" negation="false" src="d0_s10" to="o6464" />
    </statement>
    <statement id="d0_s11">
      <text>lobes lanceolate or narrowly triangular, apex acute to acuminate.</text>
      <biological_entity id="o6466" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o6467" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight or slightly curved, 25–40 (–45) mm;</text>
      <biological_entity id="o6468" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="45" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s12" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 11–24 mm;</text>
      <biological_entity id="o6469" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s13" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, adaxially green to yellow, 10–17 (–20) mm;</text>
      <biological_entity id="o6470" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s14" to="yellow" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip green, reduced, visible through front cleft, 1.5–2.5 mm, 10–15% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o6471" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character constraint="through front" constraintid="o6472" is_modifier="false" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6472" name="front" name_original="front" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o6473" name="beak" name_original="beak" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>teeth incurved, green, 0.5–1 mm. 2n = 24.</text>
      <biological_entity id="o6474" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6475" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja scabrida consists of two varieties distinguished by range and habitat differences, as well as by the morphological characters of the key. Both varieties are often confused with the similar and broadly sympatric C. chromosa but can be differentiated from the latter most easily by the presence of scalelike vestigial leaves on the proximal stems of C. scabrida.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf lobes lanceolate, sometimes linear-lanceolate; stems and leaves appearing grayish; substrates sandstone or clay.</description>
      <determination>103a. Castilleja scabrida var. scabrida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf lobes linear; stems and leaves greenish, rarely reddish purple to grayish green; substrates calcareous.</description>
      <determination>103b. Castilleja scabrida var. barnebyana</determination>
    </key_statement>
  </key>
</bio:treatment>