<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Suksdorf) G. L. Nesom" date="2012" rank="species">jungermannioides</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 38. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species jungermannioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Suksdorf" date="1900" rank="species">jungermannioides</taxon_name>
    <place_of_publication>
      <publication_title>Deutsche Bot. Monatsschr.</publication_title>
      <place_in_publication>18: 154. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species jungermannioides</taxon_hierarchy>
  </taxon_identification>
  <number>34.</number>
  <other_name type="common_name">Liverwort monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, stoloniferous, stolons thin, forming overwintering turions.</text>
      <biological_entity id="o3964" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o3965" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="width" src="d0_s0" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o3966" name="turion" name_original="turions" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="overwintering" value_original="overwintering" />
      </biological_entity>
      <relation from="o3965" id="r362" name="forming" negation="false" src="d0_s0" to="o3966" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to procumbent, simple or branching near base, 5–38 (–60) cm, densely glandular-villous, hairs 0.5–1.2 (–1.5) mm, gland-tipped, internodes evident.</text>
      <biological_entity id="o3967" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="growth_form" src="d0_s1" to="procumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="near base" constraintid="o3968" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character char_type="range_value" from="38" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="38" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o3968" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o3969" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o3970" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal not persistent;</text>
      <biological_entity id="o3971" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3972" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–5 (–20) mm;</text>
      <biological_entity id="o3973" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subpalmately to pinnately veined, broadly ovate to broadly lanceolate, 7–35 (–40) × 8–25 mm, base rounded, margins sharply, irregularly dentate to denticulate, apex acute to obtuse, surfaces glandular-villous.</text>
      <biological_entity id="o3974" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="subpalmately to pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="broadly lanceolate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3975" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3976" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="irregularly dentate" name="shape" src="d0_s4" to="denticulate" />
      </biological_entity>
      <biological_entity id="o3977" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o3978" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 2 or 3, from medial to distal nodes.</text>
      <biological_entity id="o3980" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o3979" id="r363" name="from" negation="false" src="d0_s5" to="o3980" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 15–35 mm, densely glandular-villous, hairs 0.5–1.2 (–1.5) mm, gland-tipped.</text>
      <biological_entity id="o3979" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o3981" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o3982" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <relation from="o3979" id="r364" name="fruiting" negation="false" src="d0_s6" to="o3981" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces plicate-angled, cylindric-urceolate, weakly inflated, 6–12 mm, densely glandular-villous, hairs 0.5–1.2 (–1.5) mm, gland-tipped, lobes 1–2 mm, apex rounded to mucronate.</text>
      <biological_entity id="o3983" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o3984" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o3985" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="plicate-angled" value_original="plicate-angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric-urceolate" value_original="cylindric-urceolate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o3986" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o3987" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3988" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="mucronate" />
      </biological_entity>
      <relation from="o3983" id="r365" name="fruiting" negation="false" src="d0_s7" to="o3984" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, with scattered red spots, palate ridges with 2 white patches at tips, bilaterally symmetric, strongly bilabiate;</text>
      <biological_entity id="o3989" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity constraint="palate" id="o3990" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="red spots" value_original="red spots" />
      </biological_entity>
      <biological_entity id="o3991" name="patch" name_original="patches" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o3992" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <relation from="o3989" id="r366" name="with" negation="false" src="d0_s8" to="o3990" />
      <relation from="o3990" id="r367" name="with" negation="false" src="d0_s8" to="o3991" />
      <relation from="o3991" id="r368" name="at" negation="false" src="d0_s8" to="o3992" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat funnelform, (12–) 16–20 (–24) mm, exserted beyond calyx margin;</text>
      <biological_entity id="o3993" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_distance" src="d0_s9" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s9" to="24" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="distance" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o3994" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o3993" id="r369" name="exserted beyond" negation="false" src="d0_s9" to="o3994" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 8–10 mm, lobes obovate-oblong, apex rounded to truncate.</text>
      <biological_entity id="o3995" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3996" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate-oblong" value_original="obovate-oblong" />
      </biological_entity>
      <biological_entity id="o3997" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles scabrous.</text>
      <biological_entity id="o3998" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o3999" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 5–9 mm. 2n = 32.</text>
      <biological_entity id="o4000" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4001" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The occurrence of Erythranthe jungermannioides in the Columbia River Gorge of Klickitat County, Washington (the only record from the state of Washington), is based on an imprecise, unconfirmed observation from the early 1990s (Washington National Heritage Program 2005).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Basalt crevices in seepage zones in vertical cliff faces and canyon walls.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basalt crevices" constraint="in seepage zones in vertical cliff" />
        <character name="habitat" value="seepage zones" constraint="in vertical cliff" />
        <character name="habitat" value="vertical cliff" />
        <character name="habitat" value="canyon walls" modifier="faces and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400(–1200) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>