<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Aublet" date="1775" rank="genus">BACOPA</taxon_name>
    <taxon_name authority="(Linnaeus) Wettstein in H. G. A. Engler and K. Prantl" date="1891" rank="species">monnieri</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam.</publication_title>
      <place_in_publication>67[IV,3b]: 77. 1891</place_in_publication>
      <other_info_on_pub>(as monniera)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus bacopa;species monnieri</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="Linnaeus" date="1756" rank="species">monnieri</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Pl. II,</publication_title>
      <place_in_publication>9. 1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus lysimachia;species monnieri</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Brahmi</other_name>
  <other_name type="common_name">Indian pennywort</other_name>
  <other_name type="common_name">herb of grace</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, sometimes annuals.</text>
      <biological_entity id="o19335" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, 15–30 cm, glabrous.</text>
      <biological_entity id="o19337" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves glabrous;</text>
      <biological_entity id="o19338" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade fleshy, base narrowly cuneate, margins entire or serrate, apex obtuse, 1-nerved.</text>
      <biological_entity id="o19339" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o19340" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19341" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o19342" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 5–30 mm;</text>
      <biological_entity id="o19343" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracteoles present.</text>
      <biological_entity id="o19344" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 5, ovate to lanceolate, calyx radially symmetric;</text>
      <biological_entity id="o19345" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19346" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o19347" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla white with yellow throat, 5–10 mm, lobes 5;</text>
      <biological_entity id="o19348" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19349" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character constraint="with throat" constraintid="o19350" is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19350" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o19351" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 4, didynamous.</text>
      <biological_entity id="o19352" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 64.</text>
      <biological_entity id="o19353" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19354" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bacopa monnieri is thought to be native throughout much of its range, though it is weedy and cultivated. It readily colonizes irrigated fields, especially rice fields, and seeds easily get mixed with rice and are planted in new locations. Bacopa monnieri is introduced in parts of Europe (Portugal, Spain) and Asia (China, Taiwan). It can be propagated vegetatively by cuttings.</discussion>
  <discussion>Bacopa monnieri is used medicinally in Asia in traditional Indian medicine (Ayurveda). It is edible and contains steroidal saponins, including bacosides, that have beneficial effects on the nervous system. Leaf, stem, and root extracts are used as cardiac and nerve tonics, sedatives, and vasoconstrictors. Leaves and stems are diuretic and used in treating constipation and indigestion. An alcohol extract of the whole plant has been used to treat Walker carcinoma and as a cardiovascular and muscle relaxer. In the United States, recent studies suggest it has potential for enhancing cognitive performance in the elderly and in the treatment of Alzheimer’s disease (C. Calabres et al. 2008; S. Aguiar and T. Borowski 2013). Extracts are used in the treatment of nerve and brain disorders; they also are believed to enhance intellect and decrease fertility.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wetlands, wet sands, mud flats, riparian areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wetlands" />
        <character name="habitat" value="wet sands" />
        <character name="habitat" value="mud flats" />
        <character name="habitat" value="areas" modifier="riparian" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Calif., Fla., Ga., La., Md., Miss., N.C., Okla., S.C., Tex., Va.; Mexico; West Indies; Central America; South America; s Europe; Asia; Africa; Australia; introduced in sw Europe (Portugal, Spain), Asia (China, Taiwan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value=" in sw Europe (Portugal)" establishment_means="introduced" />
        <character name="distribution" value=" in sw Europe (Spain)" establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Taiwan)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>