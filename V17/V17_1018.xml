<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">585</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1833" rank="species">ambigua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">ambigua</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species ambigua;variety ambigua</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">castillejoides</taxon_name>
    <taxon_hierarchy>genus orthocarpus;species castillejoides</taxon_hierarchy>
  </taxon_identification>
  <number>2a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ± short-decumbent proximally, becoming ascending to erect, often branched.</text>
      <biological_entity id="o33596" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less; proximally" name="growth_form_or_orientation" src="d0_s0" value="short-decumbent" value_original="short-decumbent" />
        <character char_type="range_value" from="ascending" modifier="becoming" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves widely lanceolate, sometimes linear or narrowly oblong, rarely ovate, not fleshy, apex rounded.</text>
      <biological_entity id="o33597" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o33598" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts proximally greenish, rarely dull brownish, distally white, rarely fading pinkish, on lobe apices;</text>
      <biological_entity id="o33599" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s2" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="reflectance" src="d0_s2" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s2" value="fading pinkish" value_original="fading pinkish" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o33600" name="apex" name_original="apices" src="d0_s2" type="structure" />
      <relation from="o33599" id="r2555" name="on" negation="false" src="d0_s2" to="o33600" />
    </statement>
    <statement id="d0_s3">
      <text>lobes ascending, linear to oblong, 12–15 mm, usually arising near or above mid length.</text>
      <biological_entity id="o33601" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblong" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character constraint="near mid lobes" constraintid="o33602" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="mid" id="o33602" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Calyces with all 4 clefts subequal;</text>
      <biological_entity id="o33603" name="calyx" name_original="calyces" src="d0_s4" type="structure" />
      <biological_entity id="o33604" name="cleft" name_original="clefts" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
        <character is_modifier="false" name="size" src="d0_s4" value="subequal" value_original="subequal" />
      </biological_entity>
      <relation from="o33603" id="r2556" name="with" negation="false" src="d0_s4" to="o33604" />
    </statement>
    <statement id="d0_s5">
      <text>lateral clefts 2–3 mm, 20% of calyx length.</text>
      <biological_entity constraint="lateral" id="o33605" name="cleft" name_original="clefts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 14–21 mm;</text>
      <biological_entity id="o33606" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s6" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>beak pale to bright-yellow or white, rarely pink, 4–6 mm;</text>
      <biological_entity id="o33607" name="beak" name_original="beak" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s7" to="bright-yellow or white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>abaxial lip pale to bright-yellow;</text>
      <biological_entity constraint="abaxial" id="o33608" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>teeth white, rarely pink or reddish purple.</text>
      <biological_entity id="o33609" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety ambigua is highly variable among populations. Much of the variation tends towards two ecotypes. One ecotype has thin leaves with narrow, often acute lobes and more or less spreading-ascending stems. Plants of this ecotype inhabit sandy coastal bluffs and short-grass, herbaceous meadows, often well inland, in Marin, Napa, and Sonoma counties in California. Plants of a second ecotype have fleshy leaves and bracts with more or less obtuse lobes and more upright stems. These are found in Salicornia-dominated coastal salt marshes and range from southern Vancouver Island to San Francisco Bay. Plants found around Tomales Bay, Marin County, may have pink beaks. Variety ambigua occasionally hybridizes with Castilleja exserta and reportedly with C. rubicundula.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy coastal bluffs, inland grasslands, upper margins of salt marshes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy coastal bluffs" />
        <character name="habitat" value="upper margins" modifier="inland grasslands" constraint="of salt marshes" />
        <character name="habitat" value="salt marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>