<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Munz" date="1926" rank="genus">EPIXIPHIUM</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Munz" date="1926" rank="species">wislizeni</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 15: 380. 1926</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus epixiphium;species wislizeni</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maurandya</taxon_name>
    <taxon_name authority="Engelmann ex A. Gray in W. H. Emory" date="1859" rank="species">wislizeni</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 111. 1859</place_in_publication>
      <other_info_on_pub>(as Maurandia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus maurandya;species wislizeni</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asarina</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Pennell" date="unknown" rank="species">wislizeni</taxon_name>
    <taxon_hierarchy>genus asarina;species wislizeni</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Sand snapdragon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 30–120 cm, vines.</text>
      <biological_entity id="o22212" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o22213" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 14–54 mm;</text>
      <biological_entity id="o22214" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22215" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s1" to="54" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade hastate to broadly sagittate, 21–75 × 8–48 mm, surfaces glabrous.</text>
      <biological_entity id="o22216" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22217" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="hastate" name="shape" src="d0_s2" to="broadly sagittate" />
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s2" to="75" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="48" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22218" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels ascending, 3–9 mm, thickened in fruit.</text>
      <biological_entity id="o22219" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
        <character constraint="in fruit" constraintid="o22220" is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o22220" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 13–18 × 2–4 mm, basally keeled, membranous in flower, 20–32 × 8–12 mm, indurate in fruit;</text>
      <biological_entity id="o22221" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o22222" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character constraint="in flower" constraintid="o22223" is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character constraint="in fruit" constraintid="o22224" is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o22223" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s4" to="32" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" notes="" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22224" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>corolla-tube 17–22 mm, glabrous or sparsely hairy, throat open, palate not inflated, abaxial plicae white spotted with blue or violet, lobes: abaxial reflexed, adaxial erect, equal, 7–12 mm;</text>
      <biological_entity id="o22225" name="corolla-tube" name_original="corolla-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="distance" src="d0_s5" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22226" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o22227" name="palate" name_original="palate" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22228" name="plica" name_original="plicae" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white spotted with blue or white spotted with violet" />
        <character name="coloration" src="d0_s5" value="," value_original="," />
      </biological_entity>
      <biological_entity id="o22229" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o22230" name="plica" name_original="plicae" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22231" name="plica" name_original="plicae" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens included, filaments incurved, hairy at base, abaxial 11–13 mm, adaxial 13–15 mm, pollen-sacs oblong;</text>
      <biological_entity id="o22232" name="corolla-tube" name_original="corolla-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="distance" src="d0_s6" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22233" name="throat" name_original="throat" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o22234" name="palate" name_original="palate" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22235" name="plica" name_original="plicae" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white spotted with blue or white spotted with violet" />
        <character name="coloration" src="d0_s6" value="," value_original="," />
      </biological_entity>
      <biological_entity id="o22236" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <biological_entity id="o22237" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o22238" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character constraint="at base" constraintid="o22239" is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22239" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o22240" name="plica" name_original="plicae" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22241" name="plica" name_original="plicae" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22242" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary glabrous, locules subequal;</text>
      <biological_entity id="o22243" name="corolla-tube" name_original="corolla-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="distance" src="d0_s7" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22244" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o22245" name="palate" name_original="palate" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22246" name="plica" name_original="plicae" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white spotted with blue or white spotted with violet" />
        <character name="coloration" src="d0_s7" value="," value_original="," />
      </biological_entity>
      <biological_entity id="o22247" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o22248" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22249" name="locule" name_original="locules" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style included, 13–15 mm, indurate in fruit, stigma recurved.</text>
      <biological_entity id="o22250" name="corolla-tube" name_original="corolla-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="distance" src="d0_s8" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22251" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o22252" name="palate" name_original="palate" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22253" name="plica" name_original="plicae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white spotted with blue or white spotted with violet" />
        <character name="coloration" src="d0_s8" value="," value_original="," />
      </biological_entity>
      <biological_entity id="o22254" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o22255" name="style" name_original="style" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character constraint="in fruit" constraintid="o22256" is_modifier="false" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o22256" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o22257" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules ovoid, compressed apically, 11–15 mm, indurate.</text>
      <biological_entity id="o22258" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="indurate" value_original="indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 3–4 mm, surface tuberculate, circumalate.</text>
      <biological_entity id="o22259" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o22260" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22261" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Epixiphium wislizeni is easily identified in fruit and by its occurrence on unconsolidated, sandy soils. When vegetative or in flower, it resembles Maurandella antirrhiniflora. In Texas, E. wislizeni is known from the trans-Pecos region.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Active and stabilized siliceous and gypseous dunes and sandy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="active" />
        <character name="habitat" value="stabilized siliceous" />
        <character name="habitat" value="gypseous dunes" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>