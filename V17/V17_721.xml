<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">SYNTHYRIS</taxon_name>
    <taxon_name authority="Pennell" date="1933" rank="species">canbyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>85: 93. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus synthyris;species canbyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="(Pennell) M. M. Martínez Ort. &amp; Albach" date="unknown" rank="species">canbyi</taxon_name>
    <taxon_hierarchy>genus veronica;species canbyi</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Canby’s kittentail</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, some withering in 2d year as new leaves expand;</text>
      <biological_entity id="o2041" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character constraint="as leaves" constraintid="o2042" is_modifier="false" name="life_cycle" src="d0_s0" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o2042" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade ovate or cordate, 25+ mm wide, not leathery, base cordate to lobate, margins ± incised-crenate or laciniate to ± pinnatifid, sometimes ± palmately lobed, teeth apices obtuse to rounded, surfaces sparsely hairy;</text>
      <biological_entity id="o2043" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o2044" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="lobate" />
      </biological_entity>
      <biological_entity id="o2045" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="less incised-crenate or laciniate" name="shape" src="d0_s1" to="more or less pinnatifid" />
        <character is_modifier="false" modifier="sometimes more or less palmately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="teeth" id="o2046" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="rounded" />
      </biological_entity>
      <biological_entity id="o2047" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal veins extending into distal 1/2 of blade, lateral-veins 2–4 on each side of midvein.</text>
      <biological_entity constraint="basal" id="o2048" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o2049" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o2050" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o2051" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o2052" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o2052" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o2053" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <relation from="o2048" id="r173" name="extending into" negation="false" src="d0_s2" to="o2049" />
      <relation from="o2049" id="r174" name="part_of" negation="false" src="d0_s2" to="o2050" />
      <relation from="o2052" id="r175" name="part_of" negation="false" src="d0_s2" to="o2053" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes erect, to 16 cm in fruit;</text>
      <biological_entity id="o2054" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="in fruit" constraintid="o2055" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="16" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2055" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sterile bracts 2+, ovate-spatulate, sometimes 1+ cm;</text>
      <biological_entity id="o2056" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-spatulate" value_original="ovate-spatulate" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="sometimes" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers 10–50, loosely aggregated.</text>
      <biological_entity id="o2057" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="50" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s5" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sepals 4.</text>
      <biological_entity id="o2058" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Petals (3 or) 4 (or 5), apex entire or erose;</text>
      <biological_entity id="o2059" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o2060" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue, ± regular, campanulate, much longer than calyx, glabrous, tube conspicuous.</text>
      <biological_entity id="o2061" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character constraint="than calyx" constraintid="o2062" is_modifier="false" name="length_or_size" src="d0_s8" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2062" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity id="o2063" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Stamens epipetalous.</text>
      <biological_entity id="o2064" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="epipetalous" value_original="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ovaries glabrous or sparsely hairy at apex;</text>
      <biological_entity id="o2065" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character constraint="at apex" constraintid="o2066" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2066" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovules 10–16.</text>
      <biological_entity id="o2067" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules glabrous or sparsely hairy.</text>
      <biological_entity id="o2068" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Synthyris canbyi is known from the Mission, Rattlesnake, and Swan mountain ranges of northwest Montana. Flowering in S. canbyi begins at the margins of melting snow banks. Specimens from the southern end of the Mission Range appear intermediate between S. canbyi and S. dissecta.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul; fruiting Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine ridges, scree slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine ridges" />
        <character name="habitat" value="scree slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>