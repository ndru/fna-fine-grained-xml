<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Meinke) G. L. Nesom" date="2012" rank="species">hymenophylla</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 38. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species hymenophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Meinke" date="1983" rank="species">hymenophyllus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>30: 147, fig. 1. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species hymenophyllus</taxon_hierarchy>
  </taxon_identification>
  <number>36.</number>
  <other_name type="common_name">Thin-sepal monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, filiform-taprooted.</text>
      <biological_entity id="o26857" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="filiform-taprooted" value_original="filiform-taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to ascending-erect, sharply bent at basal nodes, simple or few-branched, 5–25 cm, glandular-puberulent to glandular-villous, hairs 0.1–0.8 mm, vitreous, flattened, multicellular, gland-tipped.</text>
      <biological_entity id="o26858" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending-erect" />
        <character constraint="at basal nodes" constraintid="o26859" is_modifier="false" modifier="sharply" name="shape" src="d0_s1" value="bent" value_original="bent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="glandular-puberulent" name="pubescence" src="d0_s1" to="glandular-villous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o26859" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o26860" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s1" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration" src="d0_s1" value="vitreous" value_original="vitreous" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, largest at mid-stem;</text>
      <biological_entity id="o26861" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character constraint="at mid-stem" constraintid="o26862" is_modifier="false" name="size" src="d0_s2" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o26862" name="mid-stem" name_original="mid-stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 6–30 mm;</text>
      <biological_entity id="o26863" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pinnately veined, broadly lanceolate to ovate, 10–35 × 10–30 mm, distinctly membranous, base cuneate to shallowly cordate, margins coarsely dentate to shallowly denticulate or entire, apex acute to obtuse, surfaces glandular-puberulent to glandular-villous, hairs 0.1–0.8 mm, vitreous, flattened, multicellular, gland-tipped, glandular.</text>
      <biological_entity id="o26864" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="distinctly" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o26865" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="shallowly cordate" />
      </biological_entity>
      <biological_entity id="o26866" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="coarsely dentate" name="shape" src="d0_s4" to="shallowly denticulate or entire" />
      </biological_entity>
      <biological_entity id="o26867" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o26868" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glandular-puberulent" name="pubescence" src="d0_s4" to="glandular-villous" />
      </biological_entity>
      <biological_entity id="o26869" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration" src="d0_s4" value="vitreous" value_original="vitreous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–6, from proximal to distal nodes.</text>
      <biological_entity id="o26871" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o26870" id="r2062" name="from" negation="false" src="d0_s5" to="o26871" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels divergent at right angles from stem, usually closely paired, 10–45 mm, negatively phototropic, causing capsules to pressed against a cliff face or crevice at time of dehiscence, glandular-puberulent to glandular-villous, hairs 0.1–0.8 mm, vitreous, flattened, multicellular, gland-tipped.</text>
      <biological_entity id="o26872" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o26873" name="angle" name_original="angles" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o26874" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually closely" name="arrangement" src="d0_s6" value="paired" value_original="paired" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26875" name="capsule" name_original="capsules" src="d0_s6" type="structure" />
      <biological_entity id="o26876" name="face" name_original="face" src="d0_s6" type="structure">
        <character constraint="against " constraintid="o26877" is_modifier="false" modifier="negatively" name="fixation" src="d0_s6" value="pressed" value_original="pressed" />
        <character is_modifier="true" name="habitat" src="d0_s6" value="cliff" value_original="cliff" />
      </biological_entity>
      <biological_entity id="o26877" name="time" name_original="time" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration" src="d0_s6" value="vitreous" value_original="vitreous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o26878" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="dehiscence" value_original="dehiscence" />
        <character char_type="range_value" from="glandular-puberulent" is_modifier="true" name="pubescence" src="d0_s6" to="glandular-villous" />
      </biological_entity>
      <relation from="o26870" id="r2063" name="fruiting" negation="false" src="d0_s6" to="o26872" />
      <relation from="o26873" id="r2064" name="from" negation="false" src="d0_s6" to="o26874" />
      <relation from="o26874" id="r2065" modifier="negatively" name="causing" negation="false" src="d0_s6" to="o26875" />
      <relation from="o26877" id="r2066" name="part_of" negation="false" src="d0_s6" to="o26878" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces angled, tubular-campanulate, slightly inflated, 5–7 mm, margins distinctly toothed or lobed, sparsely stipitate-glandular, lobes pronounced, erect.</text>
      <biological_entity id="o26870" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o26879" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o26880" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="tubular-campanulate" value_original="tubular-campanulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26881" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26882" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o26870" id="r2067" name="fruiting" negation="false" src="d0_s7" to="o26879" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas light yellow, throat and abaxial lobes red or purple-spotted, sometimes with small white patches, bilaterally symmetric, weakly bilabiate;</text>
      <biological_entity id="o26883" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="light yellow" value_original="light yellow" />
      </biological_entity>
      <biological_entity id="o26884" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26885" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity id="o26886" name="patch" name_original="patches" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="small white" value_original="small white" />
      </biological_entity>
      <relation from="o26884" id="r2068" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o26886" />
      <relation from="o26885" id="r2069" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o26886" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat funnelform, 10–14 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o26887" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o26888" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o26887" id="r2070" name="exserted beyond" negation="false" src="d0_s9" to="o26888" />
    </statement>
    <statement id="d0_s10">
      <text>lobes obovate-oblong, apex rounded to truncate or notched.</text>
      <biological_entity id="o26889" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate-oblong" value_original="obovate-oblong" />
      </biological_entity>
      <biological_entity id="o26890" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="truncate or notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o26891" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o26892" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 3–6 mm. 2n = 32.</text>
      <biological_entity id="o26893" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26894" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the protologue, R. J. Meinke observed that plants of Erythranthe hymenophylla have reflexed fruiting pedicels that increase seed dispersal back onto the vertical cliff wall, the characteristic habitat of the species. The hanging habit of E. hymenophylla is reflected in a sharp (90º to 180º) bend in the basal nodes and the long pedicels that are closely paired and divergent in parallel at about right angles from the stem. The species also is characterized by it very short calyx to corolla length, relatively short capsules, and large seeds.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep, seasonally moist, basalt cliffs with west or southwest exposure, mesic coniferous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basalt cliffs" constraint="with west or southwest exposure" />
        <character name="habitat" value="west" />
        <character name="habitat" value="southwest exposure" />
        <character name="habitat" value="steep" />
        <character name="habitat" value="moist" modifier="seasonally" />
        <character name="habitat" value="basalt cliffs with west" />
        <character name="habitat" value="mesic" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>