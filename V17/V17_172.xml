<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CHELONE</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">lyonii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 737. 1813</place_in_publication>
      <other_info_on_pub>(as lyoni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus chelone;species lyonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Lyon’s or pink turtlehead</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 35–100 cm.</text>
      <biological_entity id="o38367" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (2–) 10–40 mm;</text>
      <biological_entity id="o38368" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o38369" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade broadly lanceolate to ovate, 37–137 × (20–) 30–55 (–80) mm, base rounded to truncate, margins once-serrate, teeth 3–8 per cm, abaxial surface glabrous or pilose to slightly villous, adaxial glabrate or mostly glabrous.</text>
      <biological_entity id="o38370" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o38371" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="37" from_unit="mm" name="length" src="d0_s2" to="137" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_width" src="d0_s2" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s2" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38372" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded to truncate" value_original="rounded to truncate" />
      </biological_entity>
      <biological_entity id="o38373" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="once-serrate" value_original="once-serrate" />
      </biological_entity>
      <biological_entity id="o38374" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per cm" constraintid="o38375" from="3" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o38375" name="cm" name_original="cm" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o38376" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s2" to="slightly villous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o38377" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cymes 27–71 mm;</text>
      <biological_entity id="o38378" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character char_type="range_value" from="27" from_unit="mm" name="some_measurement" src="d0_s3" to="71" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 2–7 × 2–8 mm, ape× obtuse to acute, sometimes acuminate.</text>
      <biological_entity id="o38379" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: caly× lobes 5–11 × 3–7 mm, margins sparsely to densely ciliate;</text>
      <biological_entity id="o38380" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o38381" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla pink-red to purple, tube 15–21 mm, abaxial lobes 10–12 (–14) × 5–12 mm, adaxial strongly keeled;</text>
      <biological_entity id="o38382" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o38383" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="pink-red" name="coloration" src="d0_s6" to="purple" />
      </biological_entity>
      <biological_entity id="o38384" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o38385" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o38386" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>palate yellow-bearded;</text>
      <biological_entity id="o38387" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o38388" name="palate" name_original="palate" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="yellow-bearded" value_original="yellow-bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>adaxial filaments 16–23 mm;</text>
      <biological_entity id="o38389" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o38390" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminode (8–) 10–15 mm, ape× white to light pink;</text>
      <biological_entity id="o38391" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o38392" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light pink" value_original="light pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 20–30 mm. 2n = 28.</text>
      <biological_entity id="o38393" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38394" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o38395" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Garden escapes of Chelone lyonii are reported from Connecticut, Maine, Massachusetts, and New York. Chelone lyonii can be distinguished from other members of the genus by its longer petioles, rounded to truncate leaf bases, and white to light pink staminodes. It is sometimes confused with C. obliqua, especially where C. lyonii is found with leaves narrowed to the base. Chelone lyonii can be distinguished from C. obliqua by wider leaf blades, longer petioles, more strongly keeled corollas, shorter abaxial corolla lips, and longer staminodes. Chelone lyonii has been proposed as a diploid progenitor for the allopolyploid C. obliqua (A. D. Nelson 1995; Nelson and W. J. Elisens 1999).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, cove and spruce-fir forests, balds.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="cove" />
        <character name="habitat" value="spruce-fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Miss., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>