<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PLANTAGO</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1839" rank="species">hookeriana</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>1838: 39. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus plantago;species hookeriana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Plantago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hookeriana</taxon_name>
    <taxon_name authority="(A. Gray) Poe" date="unknown" rank="variety">nuda</taxon_name>
    <taxon_hierarchy>genus plantago;species hookeriana;variety nuda</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Hooker's plantain</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
      <biological_entity id="o25403" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots taproots, slender.</text>
      <biological_entity constraint="roots" id="o25404" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0–10 mm.</text>
      <biological_entity id="o25405" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 60–120 × 4–10 mm;</text>
      <biological_entity id="o25406" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, margins toothed (teeth to 4 mm), rarely entire, veins conspicuous or not, surfaces villous or sericeous.</text>
      <biological_entity id="o25407" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o25408" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25409" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character name="prominence" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o25410" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scapes 280–580 mm, hairy, hairs antrorse, long and short.</text>
      <biological_entity id="o25411" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character char_type="range_value" from="280" from_unit="mm" name="some_measurement" src="d0_s5" to="580" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25412" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes greenish or brownish, 50–180 mm, densely flowered, flowers in whorls or pairs;</text>
      <biological_entity id="o25413" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s6" to="180" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o25414" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25415" name="whorl" name_original="whorls" src="d0_s6" type="structure" />
      <relation from="o25414" id="r1927" modifier="in whorls or pairs" name="in" negation="false" src="d0_s6" to="o25415" />
    </statement>
    <statement id="d0_s7">
      <text>bracts ovate or triangular, 1.5–6 mm, length 0.6–1.4 times sepals, apex acute or acuminate.</text>
      <biological_entity id="o25416" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o25417" is_modifier="false" name="length" src="d0_s7" value="0.6-1.4 times sepals" value_original="0.6-1.4 times sepals" />
      </biological_entity>
      <biological_entity id="o25417" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o25418" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 2.5–4 mm;</text>
      <biological_entity id="o25419" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25420" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla bilaterally symmetric, abaxial and lateral lobes reflexed, adaxial erect, 2.2–2.5 mm, base slightly cordate;</text>
      <biological_entity id="o25421" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25422" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity constraint="abaxial and lateral" id="o25423" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="adaxial and lateral" id="o25424" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25425" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4, connective significantly elongated, apex acute.</text>
      <biological_entity id="o25426" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25427" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o25428" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="significantly" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o25429" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 2, 2.4–3.4 mm. 2n = 20.</text>
      <biological_entity id="o25430" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s11" to="3.4" to_unit="mm" unit=",.4-3.4 mm" />
        <character name="some_measurement" src="d0_s11" unit=",.4-3.4 mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25431" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Miss., Tex.; Mexico (Chihuahua, Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>