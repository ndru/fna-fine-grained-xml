<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Richard K. Rabeler, Craig C. Freeman, Wayne J. Elisens</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">360</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">PEDALIACEAE</taxon_name>
    <taxon_hierarchy>family pedaliaceae</taxon_hierarchy>
  </taxon_identification>
  <number />
  <other_name type="common_name">Sesame Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [shrubs or trees], annual [perennial], not fleshy [± fleshy], autotrophic, stipitate-glandular, hairs mucilaginous.</text>
      <biological_entity id="o5119" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="nutrition" src="d0_s0" value="autotrophic" value_original="autotrophic" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o5120" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coating" src="d0_s0" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or arching [prostrate].</text>
      <biological_entity id="o5121" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite, rarely alternate distally, simple, 3-lobed, or 3-foliolate [digitate];</text>
      <biological_entity id="o5122" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="rarely; distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o5123" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o5124" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy [± fleshy], not leathery, margins entire, toothed, or lobed.</text>
      <biological_entity id="o5125" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o5126" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes or axillary, flowers solitary;</text>
      <biological_entity id="o5127" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5128" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o5129" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o5130" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, perianth and androecium hypogynous;</text>
      <biological_entity id="o5131" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o5132" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o5133" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 5, proximally connate, calyx ± bilaterally symmetric;</text>
      <biological_entity id="o5134" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5135" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less bilaterally" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, connate, corolla bilaterally symmetric, bilabiate [lobes subequal], funnelform;</text>
      <biological_entity id="o5136" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5137" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 4, adnate to corolla base, didynamous, glabrous, filaments glabrous;</text>
      <biological_entity id="o5138" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character constraint="to corolla base" constraintid="o5139" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="didynamous" value_original="didynamous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o5139" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o5140" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminode 0 or 1;</text>
      <biological_entity id="o5141" name="staminode" name_original="staminode" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s12" unit="or" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistil 1, 2-carpellate, ovary superior, 2-locular or 4-locular, often with false septa proximally, especially in fruit, placentation axile;</text>
      <biological_entity id="o5142" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
      <biological_entity id="o5143" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" name="placentation" notes="" src="d0_s13" value="axile" value_original="axile" />
      </biological_entity>
      <biological_entity constraint="false" id="o5144" name="septum" name_original="septa" src="d0_s13" type="structure" />
      <biological_entity id="o5145" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
      <relation from="o5143" id="r451" modifier="often" name="with" negation="false" src="d0_s13" to="o5144" />
      <relation from="o5143" id="r452" modifier="especially" name="in" negation="false" src="d0_s13" to="o5145" />
    </statement>
    <statement id="d0_s14">
      <text>ovules anatropous, unitegmic, tenuinucellate;</text>
      <biological_entity id="o5146" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="unitegmic" value_original="unitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="tenuinucellate" value_original="tenuinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1;</text>
      <biological_entity id="o5147" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma 1, 2-lobed, lobes spreading, filiform.</text>
      <biological_entity id="o5148" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s16" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o5149" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s16" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, dehiscence loculicidal, sometimes incomplete, with or without 2, subapical horns.</text>
      <biological_entity constraint="fruits" id="o5150" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="loculicidal" value_original="loculicidal" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s17" value="incomplete" value_original="incomplete" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o5151" name="horn" name_original="horns" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <relation from="o5150" id="r453" name="with or without" negation="false" src="d0_s17" to="o5151" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds [10–] 30–60, white, brown, or black, obovoid to pyriform or ovoid;</text>
      <biological_entity id="o5152" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s18" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s18" to="60" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s18" to="pyriform or ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>embryo straight, endosperm sparse.</text>
      <biological_entity id="o5153" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5154" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s19" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera ca. 13, species ca. 79 (2 genera, 2 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; s Asia, Africa, Australia; introduced also in South America, Europe, elsewhere in Asia, n Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="elsewhere in Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Often combined with the chiefly New World Martyniaceae, Pedaliaceae are an Old World family distinguished by mucilaginous hairs, nectaries on the pedicels, axile placentation, colpate pollen, and fruits without an enlarged, woody endocarp (H.-D. Ihlenfeldt 1967, 2004). Pedaliaceae appear to be most closely related to members of Acanthaceae and Bignoniaceae. Molecular study supports results of morphological studies (R. G. Olmstead et al. 2001) and indicates that Martyniaceae and Pedaliaceae are nested in different clades.</discussion>
  <discussion>The center of diversity of Pedaliaceae is sub-Saharan Africa, where all but one genus are native. Both genera introduced to the flora area belong to Sesameae (Endlicher) Meisner, one of three tribes recognized (H.-D. Ihlenfeldt 2004).</discussion>
  <references>
    <reference>Ihlenfeldt, H.-D. 1967. Über die Abgrenzung und die natürliche Gleiderung der Pedaliaceae R. Br. Mitt. Staatsinst. Allg. Bot. Hamburg 12: 43–128.</reference>
    <reference>Ihlenfeldt, H.-D. 2004. Pedaliaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 14+ vols. Berlin, etc. Vol. 7. Pp. 307–322.</reference>
    <reference>Manning, S. D. 1991. The genera of Pedaliaceae in the southeastern United States. J. Arnold Arbor. Suppl. Ser. 1: 313–347.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules with 2, subapical horns; plants foul smelling; staminodes 0 (or 1).</description>
      <determination>1 Ceratotheca</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules without horns; plants not foul smelling; staminodes.</description>
      <determination>12 Sesamum</determination>
    </key_statement>
  </key>
</bio:treatment>