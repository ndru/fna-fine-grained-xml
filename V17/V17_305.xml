<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Coerulei</taxon_name>
    <taxon_name authority="Rydberg" date="1905" rank="species">cyathophorus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 643. 1905</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species cyathophorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Sagebrush or Middle Park beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, (13–) 19–42 cm, glabrous.</text>
      <biological_entity id="o29389" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="13" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="19" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="19" from_unit="cm" name="some_measurement" src="d0_s0" to="42" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, glabrous;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 13–57 × 4–19 mm, blade spatulate to oblanceolate, base tapered, apex mucronate, sometimes rounded to obtuse;</text>
      <biological_entity id="o29390" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o29391" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o29392" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o29393" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o29394" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
        <character char_type="range_value" from="rounded" modifier="sometimes" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–7 pairs, sessile, 9–30 × 6–15 mm, blade oblanceolate to ovate, base clasping to cordate-clasping, apex acute to acuminate, sometimes mucronate.</text>
      <biological_entity constraint="cauline" id="o29395" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29396" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="ovate" />
      </biological_entity>
      <biological_entity id="o29397" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="clasping" name="architecture_or_fixation" src="d0_s3" to="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o29398" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted or continuous, cylindric, 10–16 cm, axis glabrous, verticillasters (6–) 8–12, cymes 2–7-flowered, proximal bracts orbiculate to ovate, 11–18 × 6–14 mm;</text>
      <biological_entity id="o29399" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="16" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29400" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29401" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
      <biological_entity id="o29402" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-7-flowered" value_original="2-7-flowered" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o29403" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels glabrous.</text>
      <biological_entity id="o29404" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29405" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate, 4–7 × 1.1–1.9 mm, margins erose, broadly scarious, glabrous;</text>
      <biological_entity id="o29406" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o29407" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s6" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29408" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="broadly" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lavender to lavender-blue, without nectar guides, funnelform, nearly rotate, 11–14 mm, glabrous externally or glandular, glabrous internally, tube 6.5–8 mm, throat gradually inflated, 4–5 mm diam., rounded abaxially;</text>
      <biological_entity id="o29409" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o29410" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s7" to="lavender-blue" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="" src="d0_s7" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o29411" name="guide" name_original="guides" src="d0_s7" type="structure" />
      <biological_entity id="o29412" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29413" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o29410" id="r2232" name="without" negation="false" src="d0_s7" to="o29411" />
    </statement>
    <statement id="d0_s8">
      <text>stamens: 4 prominently exserted (4–5 mm) beyond limb, pollen-sacs parallel, 1.2–1.5 mm, sutures smooth;</text>
      <biological_entity id="o29414" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o29415" name="limb" name_original="limb" src="d0_s8" type="structure" />
      <biological_entity id="o29416" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29417" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o29414" id="r2233" modifier="prominently" name="exserted beyond" negation="false" src="d0_s8" to="o29415" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 6–7 mm, reaching orifice or exserted (adaxially), 0.4–0.5 mm diam., tip recurved, distal 1–2 mm densely villous, hairs golden yellow, to 0.8 mm;</text>
      <biological_entity id="o29418" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o29419" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29420" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o29421" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o29422" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o29419" id="r2234" name="reaching" negation="false" src="d0_s9" to="o29420" />
    </statement>
    <statement id="d0_s10">
      <text>style 12–14 mm.</text>
      <biological_entity id="o29423" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o29424" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 5–6 × 4–5 mm.</text>
      <biological_entity id="o29425" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon cyathophorus occurs mostly in Middle Park, North Park, and the southern end of the North Platte River valley in the Southern Rocky Mountains. Populations have been documented in Grand, Jackson, and Summit counties, Colorado, and Carbon County, Wyoming. Besides having all four stamens prominently exserted, P. cyathophorus is unique in Penstemon in having the distal end of the staminode situated against the adaxial surface of the throat in mature corollas; it is situated against the abaxial surface in other species of Penstemon.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–2900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>