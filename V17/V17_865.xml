<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Christopher P. Randle</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">502</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MELAMPYRUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 605. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 264. 1754</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus melampyrum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek melam- (combining form of melas before b and p), black, and pyros, wheat, alluding to color of seeds</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Cow-wheat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>hemiparasitic.</text>
      <biological_entity id="o3054" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, not fleshy, puberulent.</text>
      <biological_entity id="o3055" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, moreorless decussate;</text>
      <biological_entity id="o3056" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="decussate" value_original="decussate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent or minute;</text>
      <biological_entity id="o3057" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire, sometimes margins of distal leaves proximally toothed.</text>
      <biological_entity id="o3058" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o3059" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3060" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3061" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o3060" id="r281" name="part_of" negation="false" src="d0_s5" to="o3061" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, flowers 2 per axil;</text>
      <biological_entity id="o3062" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o3063" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per axil" constraintid="o3064" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3064" name="axil" name_original="axil" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o3065" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels absent or minute;</text>
      <biological_entity id="o3066" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s8" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o3067" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 4, calyx bilaterally symmetric, campanulate, lobes subulate;</text>
      <biological_entity id="o3068" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3069" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o3070" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o3071" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla white, sometimes tinged with pink, strongly bilabiate, scarcely curved, abaxial lobes 3, adaxial 2, adaxial lip galeate;</text>
      <biological_entity id="o3072" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3073" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o3074" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="tinged with pink" value_original="tinged with pink" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" modifier="scarcely" name="course" src="d0_s11" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3075" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3076" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3077" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="galeate" value_original="galeate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, didynamous, filaments glabrous;</text>
      <biological_entity id="o3078" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3079" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o3080" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o3081" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3082" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o3083" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3084" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma moreorless capitate.</text>
      <biological_entity id="o3085" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o3086" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence loculicidal.</text>
      <biological_entity id="o3087" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 1–4, tan drying black, ellipsoid, wings absent.</text>
      <biological_entity id="o3088" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="tan" value_original="tan" />
        <character is_modifier="false" name="condition" src="d0_s17" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o3089" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3090" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 20–35 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melampyrum occupies a broad range of habitats and infects a taxonomically and ecologically diverse range of hosts. No recent comprehensive taxonomic treatment of Melampyrum exists. Species number estimates range from 20 (K. J. Kim and S. M. Yun 2012) to 35 (E. Fischer 2004). A molecular phylogenetic study of the Rhinantheae (in the sense of Fisher) supports the monophyly of Melampyrum, placing it sister to the remaining Rhinantheae (J. Těšitel et al. 2010). Melampyrum is unusual in Orobanchaceae in that the seeds are relatively large and few per capsule. The seeds bear an elaiosome that has been linked to dispersal by ants (W. Gibson 1993).</discussion>
  
</bio:treatment>