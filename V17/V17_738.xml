<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">304</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">SYNTHYRIS</taxon_name>
    <taxon_name authority="Piper" date="1902" rank="species">schizantha</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 223. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus synthyris;species schizantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="(Piper) M. M. Martínez Ort. &amp; Albach" date="unknown" rank="species">schizantha</taxon_name>
    <taxon_hierarchy>genus veronica;species schizantha</taxon_hierarchy>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Fringed kittentail</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, some withering in 2d year as new leaves expand;</text>
      <biological_entity id="o10969" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character constraint="as leaves" constraintid="o10970" is_modifier="false" name="life_cycle" src="d0_s0" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o10970" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade ovate to reniform or orbiculate, 25+ mm wide, chartaceous, base cordate to lobate, margins incised-crenate, teeth apices obtuse to rounded, surfaces ± hairy;</text>
      <biological_entity id="o10971" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="reniform or orbiculate" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o10972" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="lobate" />
      </biological_entity>
      <biological_entity id="o10973" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="incised-crenate" value_original="incised-crenate" />
      </biological_entity>
      <biological_entity constraint="teeth" id="o10974" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="rounded" />
      </biological_entity>
      <biological_entity id="o10975" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal veins extending into distal 1/2 of blade, lateral-veins 2–4 on each side of midvein.</text>
      <biological_entity constraint="basal" id="o10976" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o10977" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o10978" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o10979" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o10980" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o10980" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o10981" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <relation from="o10976" id="r891" name="extending into" negation="false" src="d0_s2" to="o10977" />
      <relation from="o10977" id="r892" name="part_of" negation="false" src="d0_s2" to="o10978" />
      <relation from="o10980" id="r893" name="part_of" negation="false" src="d0_s2" to="o10981" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes erect, to 35 cm in fruit;</text>
      <biological_entity id="o10982" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10983" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10983" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sterile bracts 2, fan-shaped, largest 2+ cm;</text>
      <biological_entity id="o10984" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s4" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers 15–80, loosely aggregated.</text>
      <biological_entity id="o10985" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s5" to="80" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s5" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sepals 4.</text>
      <biological_entity id="o10986" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Petals (3 or) 4 (or 5), apex laciniate;</text>
      <biological_entity id="o10987" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10988" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue to lavender with veins deeper colored (except tube yellowish white), ± regular, campanulate, much longer than calyx, glabrous, tube conspicuous.</text>
      <biological_entity id="o10989" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with veins" constraintid="o10990" from="blue" name="coloration" src="d0_s8" to="lavender" />
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character constraint="than calyx" constraintid="o10991" is_modifier="false" name="length_or_size" src="d0_s8" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10990" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="deeper colored" value_original="deeper colored" />
      </biological_entity>
      <biological_entity id="o10991" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity id="o10992" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Stamens epipetalous.</text>
      <biological_entity id="o10993" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="epipetalous" value_original="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ovaries: ovules 2–7.</text>
      <biological_entity id="o10994" name="ovary" name_original="ovaries" src="d0_s10" type="structure" />
      <biological_entity id="o10995" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules glabrous or sparsely hairy along margins.</text>
      <biological_entity id="o10996" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o10997" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10997" name="margin" name_original="margins" src="d0_s11" type="structure" />
    </statement>
  </description>
  <discussion>Synthyris schizantha is known from the southern Olympic Mountains in Grays Harbor County and the Cascade Mountains in Lewis County, Washington, and in the vicinity of Saddle Mountain in Clatsop County, Oregon.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist slopes, forest edges.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist slopes" />
        <character name="habitat" value="forest edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>