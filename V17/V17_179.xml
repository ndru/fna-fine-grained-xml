<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">CHIONOPHILA</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">jamesii</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 331. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus chionophila;species jamesii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Rocky Mountain snowlover</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1 or 2 (or 3), (3–) 5–12 (–15) cm, puberulent or retrorsely hairy, sometimes glabrate.</text>
      <biological_entity id="o10592" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s0" unit="or" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal and proximal cauline, blade oblanceolate to narrowly oblanceolate or spatulate, 12–78 × 2–18 mm, surfaces glabrous or glabrate;</text>
      <biological_entity id="o10593" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o10594" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="narrowly oblanceolate or spatulate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s1" to="78" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10595" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 1–3 pairs, blade narrowly lanceolate to linear, 8–28 × 1–3 mm.</text>
      <biological_entity id="o10596" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o10597" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o10598" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="28" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1–5 cm, verticillasters 2–7, continuous, sparsely puberulent and, usually, sparsely glandular-puberulent;</text>
      <biological_entity id="o10599" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10600" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="usually; sparsely" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts ovate to lanceolate, proximal ones 8–19 × 4–7 mm.</text>
      <biological_entity id="o10601" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10602" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0–4 mm, glabrous or sparsely glandular-pubescent.</text>
      <biological_entity id="o10603" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: caly× tube 8–9 mm, sparsely glandular-puberulent, lobes triangular, 1.5–2.5 × 2–2.5 mm;</text>
      <biological_entity id="o10604" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o10605" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla greenish white or creamy white, 10–15 mm, glabrous externally, palate and proximal parts of abaxial limb densely white-lanate, hairs to 1.5 mm, tube 3–4 mm, pollen-sacs 0.5–0.6 mm, explanate;</text>
      <biological_entity id="o10606" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10607" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy white" value_original="creamy white" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10608" name="palate" name_original="palate" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10609" name="part" name_original="parts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10610" name="limb" name_original="limb" src="d0_s7" type="structure" />
      <biological_entity id="o10611" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10612" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10613" name="pollen-sac" name_original="pollen-sacs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s7" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="explanate" value_original="explanate" />
      </biological_entity>
      <relation from="o10608" id="r845" name="part_of" negation="false" src="d0_s7" to="o10610" />
      <relation from="o10609" id="r846" name="part_of" negation="false" src="d0_s7" to="o10610" />
    </statement>
    <statement id="d0_s8">
      <text>staminode 5–7 mm;</text>
      <biological_entity id="o10614" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10615" name="staminode" name_original="staminode" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 10–12 mm.</text>
      <biological_entity id="o10616" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10617" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 8–9.5 × 4.5–6 mm.</text>
      <biological_entity id="o10618" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds tan to brown, ellipsoid to fusiform with tail on each end, 3–4.2 mm. 2n = 16.</text>
      <biological_entity id="o10619" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s11" to="brown" />
        <character char_type="range_value" constraint="with tail" constraintid="o10620" from="ellipsoid" name="shape" src="d0_s11" to="fusiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10620" name="tail" name_original="tail" src="d0_s11" type="structure" />
      <biological_entity id="o10621" name="end" name_original="end" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o10622" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
      <relation from="o10620" id="r847" name="on" negation="false" src="d0_s11" to="o10621" />
    </statement>
  </description>
  <discussion>Chionophila jamesii occurs in the central Rocky Mountains from the Medicine Bow Mountains of south-central Wyoming to the Culebra Range in Taos County in north-central New Mexico.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly slopes, alpine meadows, subalpine bogs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="bogs" modifier="subalpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3300–4100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4100" to_unit="m" from="3300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>