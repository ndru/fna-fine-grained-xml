<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">242</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1828" rank="species">richardsonii</taxon_name>
    <taxon_name authority="(D. D. Keck) Cronquist in C. L. Hitchcock et al." date="1959" rank="variety">curtiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Fl. Pacif. N.W.</publication_title>
      <place_in_publication>4: 401. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species richardsonii;variety curtiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_name authority="D. D. Keck" date="1957" rank="subspecies">curtiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 249. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species richardsonii;subspecies curtiflorus</taxon_hierarchy>
  </taxon_identification>
  <number>220b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blade ovate to lanceolate, margins laciniate-dentate, sometimes irregularly laciniate-pinnatifid.</text>
      <biological_entity id="o6972" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o6973" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s0" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o6974" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="laciniate-dentate" value_original="laciniate-dentate" />
        <character is_modifier="false" modifier="sometimes irregularly" name="shape" src="d0_s0" value="laciniate-pinnatifid" value_original="laciniate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: corolla 15–20 mm;</text>
      <biological_entity id="o6975" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o6976" name="corolla" name_original="corolla" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>staminode glabrous or sparsely hairy distally;</text>
      <biological_entity id="o6977" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6978" name="staminode" name_original="staminode" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>style 11–15 mm.</text>
      <biological_entity id="o6979" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6980" name="style" name_original="style" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety curtiflorus is known from Gilliam, Wasco, and Wheeler counties in north-central Oregon. D. D. Keck and A. Cronquist (1957) cited a specimen from Wheeler County (L. F. Henderson 5503, CAS) as probably this taxon, noting the flowers were immature. A duplicate specimen at MO clearly is var. curtiflorus, with corollas 15–18 mm and staminodes with one or two hairs near their tips. An anomalous specimen at MO (E. A. Purer 7811) collected in Yakima County, Washington, appears close to var. curtiflorus, with corollas 18–20 mm, staminodes sparsely lanate, and leaf margins dentate. This collection was made about 120 km north of the northernmost collections of the variety.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus slopes, cliffs, crevices, basalt.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>