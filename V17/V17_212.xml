<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <taxon_name authority="(A. Gray) Straw" date="1967" rank="species">rothrockii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 203. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella;species rothrockii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="species">rothrockii</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 260. 1878</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species rothrockii</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Rothrock’s keckiella</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 3–6 dm, densely short-hairy when young, not glaucous.</text>
      <biological_entity id="o12960" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s0" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves subopposite or whorled (3s), subsessile;</text>
      <biological_entity id="o12961" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblanceolate or lanceolate to widely obovate, 5–16 mm, margins entire or serrulate, sometimes with 3–7 undulations instead of teeth.</text>
      <biological_entity id="o12962" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="widely obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12963" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o12964" name="undulation" name_original="undulations" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o12965" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <relation from="o12963" id="r1018" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o12964" />
      <relation from="o12964" id="r1019" name="instead of" negation="false" src="d0_s2" to="o12965" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences spikelike racemes, short-hairy.</text>
      <biological_entity id="o12966" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity id="o12967" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: caly× 4–7 mm, lobes lanceolate;</text>
      <biological_entity id="o12968" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="area" src="d0_s4" unit="mm" value="×4-7" value_original="×4-7" />
      </biological_entity>
      <biological_entity id="o12969" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla brownish yellow, pale-yellow, or cream, purple or reddish brown-lined, 10–15 mm, tube plus distinct throat 7–11 mm, longer than adaxial lip, adaxial lip 4–5 mm;</text>
      <biological_entity id="o12970" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12971" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish brown-lined" value_original="reddish brown-lined" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12972" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o12973" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character constraint="than adaxial lip" constraintid="o12974" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12974" name="lip" name_original="lip" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o12975" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs 0.8–1.1 mm;</text>
      <biological_entity id="o12976" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12977" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode glabrous, usually included.</text>
      <biological_entity id="o12978" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12979" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s7" value="included" value_original="included" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 10–12 mm, glabrescent; leaves canescent.</description>
      <determination>1a. Keckiella rothrockii var. rothrockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 13–15 mm, sparsely hairy; leaves glabrescent.</description>
      <determination>1b. Keckiella rothrockii var. jacintensis</determination>
    </key_statement>
  </key>
</bio:treatment>