<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">664</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">610</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">662</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="species">viscidula</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 297. 1878</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species viscidula</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>116.</number>
  <other_name type="common_name">Sticky paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.5–3 (–4) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o22818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22819" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o22818" id="r1743" name="with" negation="false" src="d0_s2" to="o22819" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several, ascending to erect, decumbent at base, unbranched, sometimes branched, hairs spreading, long, soft, mixed with shorter stipitate-glandular ones.</text>
      <biological_entity id="o22820" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character constraint="at base" constraintid="o22821" is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o22821" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o22822" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with shorter ones" constraintid="o22823" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o22823" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green to brown, linear, linear-lanceolate, lanceolate, or oblong (narrowly ovate nearing inflorescence), 1–4 (–5) cm, not fleshy, margins wavy, flat or involute, (0–) 2 (–5) -lobed, apex acute;</text>
      <biological_entity id="o22824" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o22825" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)2(-5)-lobed" value_original="(0-)2(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o22826" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending-spreading, oblong to narrowly lanceolate, apex acute or obtuse.</text>
      <biological_entity id="o22827" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o22828" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–14 × 1–3.5 cm;</text>
      <biological_entity id="o22829" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="14" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish to greenish brown, distally pale-yellow, cream, or yellow-green, sometimes yellow-orange or red (sometimes gradually differentiated from proximal coloration), lanceolate, broadly lanceolate, or oblong, 3 (–5) -lobed, proximal wavy-margined;</text>
      <biological_entity id="o22830" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="greenish brown distally pale-yellow cream or yellow-green" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="greenish brown distally pale-yellow cream or yellow-green" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="greenish brown distally pale-yellow cream or yellow-green" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s7" to="greenish brown distally pale-yellow cream or yellow-green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="yellow-orange" value_original="yellow-orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22831" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="wavy-margined" value_original="wavy-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, linear to narrowly lanceolate, long, arising near or above mid length, sometimes wavy-margined, apex acute to rounded.</text>
      <biological_entity id="o22832" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="wavy-margined" value_original="wavy-margined" />
      </biological_entity>
      <biological_entity id="o22833" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts or proximally paler, (10–) 14–18 mm;</text>
      <biological_entity id="o22834" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o22835" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="paler" value_original="paler" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22835" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts (4–) 5–9 mm, 30–40% of calyx length, deeper than laterals, lateral (1–) 2–6 mm, ca. 25% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o22836" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character constraint="than laterals" constraintid="o22837" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="30-40%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22837" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes narrowly ovate to lanceolate, linear, or narrowly lanceolate, apex acute to obtuse.</text>
      <biological_entity id="o22838" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s11" to="lanceolate linear or narrowly lanceolate" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s11" to="lanceolate linear or narrowly lanceolate" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s11" to="lanceolate linear or narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o22839" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, 16–22 (–25) mm;</text>
      <biological_entity id="o22840" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 10–15 mm;</text>
      <biological_entity id="o22841" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, straight to sometimes curved, adaxially green to yellow, 5–8 (–9) mm;</text>
      <biological_entity id="o22842" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight to sometimes" value_original="straight to sometimes" />
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight to sometimes" value_original="straight to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s14" to="yellow" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip green or yellow, sometimes deep purple, reduced, inconspicuous, often visible in abaxial cleft, 1–2 mm, 20% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o22843" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="in abaxial lip" constraintid="o22844" is_modifier="false" modifier="often" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22844" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o22845" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o22843" id="r1744" modifier="20%" name="as long as" negation="false" src="d0_s15" to="o22845" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect, green to white, sometimes yellow or pink, 0.5–1 mm. 2n = 24, 72.</text>
      <biological_entity id="o22846" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s16" to="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22847" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
        <character name="quantity" src="d0_s16" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja viscidula is a member of the complex including C. applegatei and C. martini, centered in California. Castilleja viscidula favors isolated mountain ranges, from the Wallowa and, possibly, the Blue mountains of northeastern Oregon, eastward into southwestern Idaho, and southward into central Nevada. Most populations are greenish yellow, but in one portion of the Wallowa Mountains, reddish bracted plants are common. Many yellowish bracted populations in the same mountain range surround this reddish population. Intermediate color forms are rarely encountered. Most ranges where C. viscidula occurs have generated slightly differing local races, demonstrating some reproductive isolation and divergence. In addition, hybrid swarms between this species and C. nana are known from several mountain ranges in central and northern Nevada, and an apparent hybrid with C. flava var. flava is known from the Independence Mountains of northern Nevada.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to mesic sagebrush steppes, rocky slopes, ledges, open woodlands, montane to subalpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic sagebrush steppes" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="dry to mesic sagebrush steppes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>