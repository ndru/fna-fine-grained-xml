<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="D. D. Keck" date="1936" rank="species">personatus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 248. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species personatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Closed-throat beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs;</text>
      <biological_entity id="o28788" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex herbaceous, rhizomelike.</text>
      <biological_entity id="o28789" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 30–57 cm, puberulent or retrorsely hairy, rarely glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o28790" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="57" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely; distally" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, 3–6 (or 7) pairs, petiolate or sessile, (6–) 30–65 (–95) × (5–) 12–50 mm, blade ovate to oblong or lanceolate, base truncate to cordate, margins entire or ± denticulate, apex obtuse to acute, puberulent to pubescent abaxially, glabrate adaxially, glaucescent.</text>
      <biological_entity id="o28791" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s3" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="95" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="65" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28792" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="oblong or lanceolate" />
      </biological_entity>
      <biological_entity id="o28793" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
      <biological_entity id="o28794" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o28795" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="pubescent abaxially" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="coating" src="d0_s3" value="glaucescent" value_original="glaucescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, conic, 7–33 cm, axis sparsely to densely glandular-pubescent, verticillasters (2 or) 3–5 (–7), cymes 2–5 (–7) -flowered;</text>
      <biological_entity id="o28796" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="conic" value_original="conic" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="33" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28797" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o28798" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o28799" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-5(-7)-flowered" value_original="2-5(-7)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to lanceolate, 7–28 (–72) × 3–17 (–45) mm;</text>
      <biological_entity constraint="proximal" id="o28800" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="72" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="28" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glandular-pubescent.</text>
      <biological_entity id="o28801" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o28802" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate, 3.8–6 × 1.1–2 mm, glandular-pubescent;</text>
      <biological_entity id="o28803" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o28804" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to purple or white, lined internally with violet nectar guides, personate, tubular-funnelform, 20–26 mm, glabrous or sparsely glandular-pubescent externally, moderately to densely white-villous internally, tube 7–8 mm, throat 6–8 mm diam.;</text>
      <biological_entity id="o28805" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28806" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="purple or white" />
        <character constraint="with nectar guides" constraintid="o28807" is_modifier="false" name="architecture" src="d0_s8" value="lined" value_original="lined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="personate" value_original="personate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="26" to_unit="mm" />
        <character char_type="range_value" from="sparsely glandular-pubescent externally" modifier="internally" name="pubescence" src="d0_s8" to="moderately densely white-villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o28807" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o28808" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28809" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs 0.8–1.1 mm, glabrous;</text>
      <biological_entity id="o28810" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28811" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o28812" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s9" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 4–5 mm, flattened distally, 0.1–0.2 mm diam., tip straight, distal 3–4 mm densely villous, hairs yellow, to 1 mm;</text>
      <biological_entity id="o28813" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28814" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28815" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28816" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o28817" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 17–19 mm.</text>
      <biological_entity id="o28818" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28819" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s11" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–9 × 4–5 mm.</text>
      <biological_entity id="o28820" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon personatus is known from the northern Sierra Nevada in Butte, Nevada, Plumas, and Sierra counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sugar pine, yellow pine, California red fir, and mixed evergreen forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sugar pine" />
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="red fir" />
        <character name="habitat" value="mixed evergreen forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>