<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <taxon_name authority="(A. Gray) Straw" date="1967" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 203. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Gray in W. H. Brewer et al." date="1876" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>1: 557. 1876</place_in_publication>
      <other_info_on_pub>(as Pentstemon lemmoni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species lemmonii</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Lemmon’s keckiella</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, virgate with age, 5–15 dm, glabrous when young, glaucous.</text>
      <biological_entity id="o3452" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character constraint="with age" constraintid="o3453" is_modifier="false" name="architecture" src="d0_s0" value="virgate" value_original="virgate" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o3453" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite;</text>
      <biological_entity id="o3454" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to ovate, 10–65 mm, margins usually 2–12-toothed.</text>
      <biological_entity id="o3455" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3456" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="2-12-toothed" value_original="2-12-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences panicles, glandular-hairy.</text>
      <biological_entity constraint="inflorescences" id="o3457" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: caly× 3.5–6.7 mm, lobes lanceolate;</text>
      <biological_entity id="o3458" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="area" src="d0_s4" unit="mm" value="×3.5-6.7" value_original="×3.5-6.7" />
      </biological_entity>
      <biological_entity id="o3459" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla purplish brown, abaxial lip pale-yellow, brown-purple lined, 11–15 mm, tube plus distinct throat 5.5–9.5 mm, longer than adaxial lip, adaxial lip 2.6–6 mm;</text>
      <biological_entity id="o3460" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3461" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="purplish brown" value_original="purplish brown" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3462" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown-purple lined" value_original="brown-purple lined" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3463" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o3464" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s5" to="9.5" to_unit="mm" />
        <character constraint="than adaxial lip" constraintid="o3465" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3465" name="lip" name_original="lip" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o3466" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs 0.6–0.9 mm;</text>
      <biological_entity id="o3467" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3468" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s6" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode densely yellow-hairy, exserted.</text>
      <biological_entity id="o3469" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 16.</text>
      <biological_entity id="o3470" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="yellow-hairy" value_original="yellow-hairy" />
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3471" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Keckiella lemmonii is essentially restricted to the mountains of northern California from the North Coast Ranges through the Klamath Mountains and the northern high Sierra Nevada.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, coniferous and mixed forests, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="mixed forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>