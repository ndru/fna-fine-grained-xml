<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="Greene" date="1892" rank="species">montanus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 240. 1892</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species montanus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Cordroot or mountain beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, sometimes cespitose.</text>
      <biological_entity id="o33561" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 6–30 cm, pubescent, sometimes retrorsely hairy proximally and glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o33563" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes retrorsely; proximally" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, 4–7 pairs, distals seldom distinctly smaller than proximals, short-petiolate or sessile, 9–35 (–56) × 7–17 (–28) mm, blade obovate to oblanceolate, ovate, lanceolate, or elliptic, base tapered, cuneate, or cordate-clasping, margins entire, subentire, serrate, or dentate, apex obtuse to acute, glabrous, puberulent, or puberulent and glandular-pubescent, glaucous or not.</text>
      <biological_entity id="o33564" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o33565" name="distal" name_original="distals" src="d0_s2" type="structure">
        <character constraint="than proximals" constraintid="o33566" is_modifier="false" name="size" src="d0_s2" value="seldom distinctly smaller" value_original="seldom distinctly smaller" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="56" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="28" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33566" name="proximal" name_original="proximals" src="d0_s2" type="structure" />
      <biological_entity id="o33567" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate ovate lanceolate or elliptic" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate ovate lanceolate or elliptic" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate ovate lanceolate or elliptic" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate ovate lanceolate or elliptic" />
      </biological_entity>
      <biological_entity id="o33568" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o33569" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o33570" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses continuous, secund, 1–7 cm, axis sparsely to moderately glandular-pubescent, sometimes also pubescent proximally, verticillasters 1–5, cymes 1-flowered;</text>
      <biological_entity id="o33571" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33572" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o33573" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o33574" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts ovate to lanceolate, 6–28 × 3–22 mm;</text>
      <biological_entity constraint="proximal" id="o33575" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="28" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels ascending to erect, glandular-pubescent.</text>
      <biological_entity id="o33576" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o33577" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate to linear, 8–15 × 1.8–3.5 mm, glandular-pubescent;</text>
      <biological_entity id="o33578" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o33579" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lavender, blue, or violet, unlined internally or lined with faint lavender or blue nectar guides abaxially, not personate, funnelform, 26–33 (–39) mm, glabrous externally, moderately to densely white-lanate internally abaxially, tube 7–9 mm, throat 8–10 mm diam.;</text>
      <biological_entity id="o33580" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o33581" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="violet" value_original="violet" />
        <character constraint="with nectar guides" constraintid="o33582" is_modifier="false" modifier="internally" name="architecture" src="d0_s7" value="lined" value_original="lined" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s7" value="personate" value_original="personate" />
        <character char_type="range_value" from="33" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="39" to_unit="mm" />
        <character char_type="range_value" from="26" from_unit="mm" name="some_measurement" src="d0_s7" to="33" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately to densely; internally abaxially; abaxially" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o33582" name="guide" name_original="guides" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o33583" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33584" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens included, pollen-sacs 1.1–1.6 mm;</text>
      <biological_entity id="o33585" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33586" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o33587" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s8" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminode 11–15 mm, slightly flattened distally, 0.1–0.3 mm diam., tip straight to recurved, glabrous or distal 3–6 mm pubescent, hairs white, to 1.4 mm;</text>
      <biological_entity id="o33588" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33589" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33590" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o33591" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o33592" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 25–30 mm.</text>
      <biological_entity id="o33593" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33594" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–14 × 5–7 mm.</text>
      <biological_entity id="o33595" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon montanus is known from the Central and Northern Rocky mountains.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades puberulent and glandular-pubescent, not glaucous, blade margins serrate to dentate.</description>
      <determination>7a. Penstemon montanus var. montanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades glabrous or puberulent, usually glaucous, blade margins entire or subentire.</description>
      <determination>7b. Penstemon montanus var. idahoensis</determination>
    </key_statement>
  </key>
</bio:treatment>