<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">404</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="2012" rank="species">geniculata</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 38. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species geniculata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">geniculatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 280. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species geniculatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="A. L. Grant" date="unknown" rank="species">dudleyi</taxon_name>
    <taxon_hierarchy>genus m.;species dudleyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="unknown" rank="species">floribundus</taxon_name>
    <taxon_name authority="(Greene) A. L. Grant" date="unknown" rank="variety">geniculatus</taxon_name>
    <taxon_hierarchy>genus m.;species floribundus;variety geniculatus</taxon_hierarchy>
  </taxon_identification>
  <number>50.</number>
  <other_name type="common_name">Bent-stem or Dudley’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, fibrous-rooted or filiform-taprooted.</text>
      <biological_entity id="o27585" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="filiform-taprooted" value_original="filiform-taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to decumbent or prostrate, geniculate at nodes, simple or diffusely branched, 5–60 cm, moderately villous, hairs 0.8–2 mm, multicellular, eglandular and also 0.1–0.3 mm, stipitate-glandular.</text>
      <biological_entity id="o27586" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="at nodes" constraintid="o27587" is_modifier="false" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="diffusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27587" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o27588" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s1" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal usually deciduous by flowering;</text>
      <biological_entity id="o27589" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27590" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="usually" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–10 (–35) mm;</text>
      <biological_entity id="o27591" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pinnately to subpinnately veined, broadly ovate or elliptic-ovate to triangular, 8–35 × 5–30 mm, base cuneate to rounded or subcordate, margins serrate or dentate, teeth 3–10 per side, apex acute to obtuse or rounded, surfaces moderately villous, hairs 0.8–2 mm, multicellular, eglandular, and 0.1–0.3 mm, stipitate-glandular.</text>
      <biological_entity id="o27592" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately to subpinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s4" to="triangular" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27593" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded or subcordate" />
      </biological_entity>
      <biological_entity id="o27594" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o27595" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o27596" from="3" name="quantity" src="d0_s4" to="10" />
      </biological_entity>
      <biological_entity id="o27596" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o27597" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse or rounded" />
      </biological_entity>
      <biological_entity id="o27598" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27599" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, (1–) 6–20, from all or medial to distal nodes.</text>
      <biological_entity id="o27601" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o27600" id="r2116" name="from" negation="false" src="d0_s5" to="o27601" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 12–26 (–55) mm, moderately villous, hairs 0.8–2 mm, multicellular, eglandular and also 0.1–0.3 mm, stipitate-glandular.</text>
      <biological_entity id="o27600" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o27602" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o27603" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="55" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="26" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <relation from="o27600" id="r2117" name="fruiting" negation="false" src="d0_s6" to="o27602" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces red-spotted, campanulate-cylindric, weakly inflated, (5–) 6–8 mm, margins distinctly toothed or lobed, sparsely to moderately villous-glandular, ribs shallowly wing-angled, lobes pronounced, erect to spreading or spreading-recurving.</text>
      <biological_entity id="o27604" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o27605" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o27606" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-spotted" value_original="red-spotted" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate-cylindric" value_original="campanulate-cylindric" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27607" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture_or_function_or_pubescence" src="d0_s7" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <biological_entity id="o27608" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s7" value="wing-angled" value_original="wing-angled" />
      </biological_entity>
      <biological_entity id="o27609" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="spreading or spreading-recurving" />
      </biological_entity>
      <relation from="o27604" id="r2118" name="fruiting" negation="false" src="d0_s7" to="o27605" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, without white patches, throat red-spotted, spots concentrated or becoming coalescent into a somewhat discrete splotch at base of each of 3 abaxial lobes and sometimes 2 adaxial, bilaterally symmetric, ± bilabiate;</text>
      <biological_entity id="o27610" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o27611" name="patch" name_original="patches" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o27612" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
      </biological_entity>
      <biological_entity id="o27613" name="spot" name_original="spots" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s8" value="concentrated" value_original="concentrated" />
        <character constraint="into splotch" constraintid="o27614" is_modifier="false" modifier="becoming" name="fusion" src="d0_s8" value="coalescent" value_original="coalescent" />
      </biological_entity>
      <biological_entity id="o27614" name="splotch" name_original="splotch" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="somewhat" name="fusion" src="d0_s8" value="discrete" value_original="discrete" />
      </biological_entity>
      <biological_entity id="o27615" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o27616" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character modifier="sometimes" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27617" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o27610" id="r2119" name="without" negation="false" src="d0_s8" to="o27611" />
      <relation from="o27614" id="r2120" name="at" negation="false" src="d0_s8" to="o27615" />
      <relation from="o27615" id="r2121" modifier="of" name="consist_of" negation="false" src="d0_s8" to="o27616" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric, 9–12 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o27618" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="9" from_unit="mm" name="distance" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o27619" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o27618" id="r2122" name="exserted beyond" negation="false" src="d0_s9" to="o27619" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 10–18 mm diam.</text>
      <biological_entity id="o27620" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o27621" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o27622" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 4–6 (–7) mm.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 32.</text>
      <biological_entity id="o27623" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27624" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe geniculata is known from an apparently disjunct cluster of populations in Butte, Sutter, and Yuba counties and then from Tuolumne and Stanislaus counties south to Kern County.</discussion>
  <discussion>Erythranthe geniculata, compared to E. floribunda, has larger, chasmogamous, and allogamous flowers. The anther pairs of E. geniculata are at different levels, and the stigma is slightly above the adaxial anther pair; in E. floribunda both anther pairs and the stigma are at the same level.</discussion>
  <discussion>Erythranthe arenaria, E. geniculata, and E. norrisii constitute a group of apparently closely related species within sect. Mimulosma endemic along the Sierra Nevada. All have ovate-petiolate leaves (only the basal ones are sometimes ovate in E. arenaria) with pinnate to subpinnate venation. The more widespread E. floribunda, which is part of the above group, also is similar, but all three endemics have larger corollas with the tube exserted at greater length beyond the calyx margin.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granite crevices, canyon slopes, talus, crevices in volcanic outcrops, edges of boulders, roadsides, damp sandy soils, sandy water edges, gravelly soils and creek bottoms.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granite crevices" />
        <character name="habitat" value="canyon slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="crevices" constraint="in volcanic outcrops" />
        <character name="habitat" value="volcanic outcrops" />
        <character name="habitat" value="edges" constraint="of boulders" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy soils" modifier="damp" />
        <character name="habitat" value="sandy water edges" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="creek bottoms" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–900(–1200) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>