<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="Coville" date="1893" rank="species">fruticiformis</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>4: 170. 1893</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species fruticiformis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>232.</number>
  <other_name type="common_name">Death Valley beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs.</text>
      <biological_entity id="o24807" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 30–60 cm, glaucous.</text>
      <biological_entity id="o24809" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: cauline 6–12 pairs, sessile or short-petiolate, (12–) 25–65 × 2–8 mm, blade lanceolate to linear, sometimes oblanceolate, base truncate or tapered, margins entire, rarely ± serrulate distally, apex obtuse to acute.</text>
      <biological_entity id="o24810" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o24811" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_length" src="d0_s2" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s2" to="65" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24812" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o24813" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o24814" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely more or less; distally" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o24815" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses interrupted, cylindric, 8–30 cm, axis glabrous, verticillasters 3–8, cymes 1–3-flowered;</text>
      <biological_entity id="o24816" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24817" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24818" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity id="o24819" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts lanceolate to linear, 9–28 × 2–5 mm;</text>
      <biological_entity constraint="proximal" id="o24820" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="28" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels spreading to ascending, glabrous.</text>
      <biological_entity id="o24821" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24822" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes depressed-ovate to round or ovate, 3.5–6.5 × 2.5–5 mm, glabrous;</text>
      <biological_entity id="o24823" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o24824" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="depressed-ovate" name="shape" src="d0_s6" to="round or ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla bluish lavender to lavender or light pink, with reddish purple nectar guides, strongly bilabiate, ventricose-ampliate, 22–28 mm, glabrous or glandular-pubescent externally, moderately to densely white-lanate internally abaxially, sometimes also sparsely to densely glandular-pubescent internally, tube 6–8 mm, length 1–1.5 times calyx lobes, throat abruptly inflated, constricted at orifice, 8–14 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o24825" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24826" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bluish lavender" value_original="bluish lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="light pink" value_original="light pink" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s7" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="size" src="d0_s7" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s7" to="28" to_unit="mm" />
        <character char_type="range_value" from="glandular-pubescent externally" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s7" to="moderately densely white-lanate" />
        <character is_modifier="false" modifier="sometimes; sparsely to densely; internally" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o24827" name="guide" name_original="guides" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o24828" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character constraint="lobe" constraintid="o24829" is_modifier="false" name="length" src="d0_s7" value="1-1.5 times calyx lobes" value_original="1-1.5 times calyx lobes" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o24829" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o24830" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o24831" is_modifier="false" name="size" src="d0_s7" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" notes="" src="d0_s7" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o24831" name="orifice" name_original="orifice" src="d0_s7" type="structure" />
      <relation from="o24826" id="r1892" name="with" negation="false" src="d0_s7" to="o24827" />
    </statement>
    <statement id="d0_s8">
      <text>stamens included, filaments of shorter pair glandular-puberulent proximally, pollen-sacs explanate to subexplanate, 1.6–2.2 mm, sutures smooth;</text>
      <biological_entity id="o24832" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24833" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o24834" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o24835" name="pair" name_original="pair" src="d0_s8" type="structure" />
      <biological_entity id="o24836" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="distance" src="d0_s8" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24837" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o24834" id="r1893" name="part_of" negation="false" src="d0_s8" to="o24835" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 15–19 mm, exserted, 0.6–0.8 mm diam., tip recurved, distal 8–9 mm moderately pilose to lanate, hairs yellow, to 2.5 mm;</text>
      <biological_entity id="o24838" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24839" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24840" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24841" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="moderately pilose" name="pubescence" src="d0_s9" to="lanate" />
      </biological_entity>
      <biological_entity id="o24842" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal 5–6 mm glandular-pubescent;</text>
      <biological_entity id="o24843" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o24844" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 19–24 mm, glabrous.</text>
      <biological_entity id="o24845" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24846" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s11" to="24" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 9–13 × 5–7 mm, glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o24847" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24848" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon fruticiformis is known from the Mojave Desert. Specimens from the Kingston Range and the eastern flank of the southern Sierra Nevada sometimes combine features of P. fruticiformis and P. incertus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas glabrous externally; calyx lobes depressed-ovate to round or ovate, 3.5–5 × 3.2–5 mm.</description>
      <determination>232a. Penstemon fruticiformis var. fruticiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas ± glandular-pubescent externally; calyx lobes ovate, 4.5–6.5 × 2.5–4 mm.</description>
      <determination>232b. Penstemon fruticiformis var. amargosae</determination>
    </key_statement>
  </key>
</bio:treatment>