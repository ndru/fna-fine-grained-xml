<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">417</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="2012" rank="species">glaucescens</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 43. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species glaucescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">glaucescens</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 113. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species glaucescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">guttatus</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">glaucescens</taxon_name>
    <taxon_hierarchy>genus m.;species guttatus;variety glaucescens</taxon_hierarchy>
  </taxon_identification>
  <number>72.</number>
  <other_name type="common_name">Shieldbract monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, slender-taprooted or fibrous-rooted, rarely with runners from basal nodes.</text>
      <biological_entity id="o23301" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="slender-taprooted" value_original="slender-taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o23302" name="runner" name_original="runners" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o23303" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <relation from="o23301" id="r1787" modifier="rarely" name="with" negation="false" src="d0_s0" to="o23302" />
      <relation from="o23302" id="r1788" name="from" negation="false" src="d0_s0" to="o23303" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched, terete, sometimes 4-angled distally, (5–) 30–60 (–80) cm, glabrous, glaucous.</text>
      <biological_entity id="o23304" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="sometimes; distally" name="shape" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o23305" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole: basal and proximal cauline as long as or much longer than blade, slender, sometimes pubescent or villous, distals absent;</text>
      <biological_entity id="o23306" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="than blade" constraintid="o23307" is_modifier="false" name="length_or_size" src="d0_s3" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o23307" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o23308" name="distal" name_original="distals" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 3–5-veined, (proximal) ovate to ovate-elliptic or orbicular-ovate, sometimes subcordate, 10–50 mm, midcauline to distal orbicular, 5–45 mm wide, distinctly connate-perfoliate, disclike distally, base rounded to subcordate, margins: proximals denticulate to dentate or coarsely, irregularly toothed, sometimes lobed at base, distals nearly entire or toothed, teeth scattered, small, apex rounded, surfaces glabrous, glaucous.</text>
      <biological_entity id="o23309" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-5-veined" value_original="3-5-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="ovate-elliptic or orbicular-ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23310" name="midcauline" name_original="midcauline" src="d0_s4" type="structure" />
      <biological_entity id="o23311" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="shape" src="d0_s4" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" modifier="distinctly" name="architecture" src="d0_s4" value="connate-perfoliate" value_original="connate-perfoliate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="disclike" value_original="disclike" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o23312" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="shape" src="d0_s4" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" modifier="distinctly" name="architecture" src="d0_s4" value="connate-perfoliate" value_original="connate-perfoliate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="disclike" value_original="disclike" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o23313" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s4" to="dentate" />
        <character name="shape" src="d0_s4" value="coarsely" value_original="coarsely" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character constraint="at base" constraintid="o23314" is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o23314" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o23315" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o23316" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o23317" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o23318" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <relation from="o23310" id="r1789" name="to" negation="false" src="d0_s4" to="o23311" />
      <relation from="o23310" id="r1790" name="to" negation="false" src="d0_s4" to="o23312" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–16, from distal nodes, sometimes from nearly all, chasmogamous.</text>
      <biological_entity constraint="distal" id="o23320" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o23319" id="r1791" name="from" negation="false" src="d0_s5" to="o23320" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 10–50 mm, glabrous, glaucous.</text>
      <biological_entity id="o23321" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o23319" id="r1792" name="fruiting" negation="false" src="d0_s6" to="o23321" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces broadly campanulate, inflated, sagittally compressed, 7–16 mm, glabrous, glaucous, throat closing.</text>
      <biological_entity id="o23319" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="16" />
        <character is_modifier="false" modifier="sometimes; nearly" name="reproduction" notes="" src="d0_s5" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o23322" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o23323" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sagittally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o23324" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
      <relation from="o23319" id="r1793" name="fruiting" negation="false" src="d0_s7" to="o23322" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, sometimes with a median splotch, abaxial limb densely dark yellow, others much lighter, throat floor and tube red-dotted, bilaterally symmetric, bilabiate;</text>
      <biological_entity id="o23325" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="median" id="o23326" name="splotch" name_original="splotch" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o23327" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark yellow" value_original="dark yellow" />
      </biological_entity>
      <biological_entity id="o23328" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="much" name="coloration" src="d0_s8" value="lighter" value_original="lighter" />
      </biological_entity>
      <biological_entity id="o23329" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <biological_entity id="o23330" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-dotted" value_original="red-dotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o23325" id="r1794" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o23326" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat funnelform, 12–23 mm, exserted 4–8 mm beyond calyx margin;</text>
      <biological_entity id="o23331" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="distance" src="d0_s9" to="23" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" constraint="beyond calyx margin" constraintid="o23332" from="4" from_unit="mm" name="distance" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o23332" name="margin" name_original="margin" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 14–36 mm.</text>
      <biological_entity id="o23333" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="36" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles minutely hirtellous-puberulent.</text>
      <biological_entity id="o23334" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="hirtellous-puberulent" value_original="hirtellous-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o23335" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 5–11 mm. 2n = 28.</text>
      <biological_entity id="o23336" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23337" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants from one locality in Butte County are unusual in producing filiform, small-leaved runners from basal cauline nodes. Erythranthe glaucescens is known only from Butte and Tehama counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seepage areas, wet rocks, moist cliffs, pool edges, gravelly stream banks, serpentine outcrops, roadsides and roadcuts, low pastures, riparian woodlands, blue oak woodlands, chaparral, grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepage areas" />
        <character name="habitat" value="wet rocks" />
        <character name="habitat" value="moist cliffs" />
        <character name="habitat" value="pool edges" />
        <character name="habitat" value="gravelly stream banks" />
        <character name="habitat" value="serpentine outcrops" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="roadcuts" />
        <character name="habitat" value="low pastures" />
        <character name="habitat" value="riparian woodlands" />
        <character name="habitat" value="blue oak woodlands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80–900(–1100) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="80" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1100" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>