<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Rydberg" date="1900" rank="species">cystopteridifolia</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 365. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species cystopteridifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Fern-leaved lousewort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–50 cm.</text>
      <biological_entity id="o30570" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 2–10, blade elliptic to lanceolate, 20–90 x 5–15 mm, 2 (or 3) -pinnatifid, margins of adjacent lobes nonoverlapping to extensively overlapping distally, serrate, surfaces glabrous;</text>
      <biological_entity id="o30571" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o30572" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
      <biological_entity id="o30573" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="90" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o30574" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="nonoverlapping" modifier="distally" name="arrangement" src="d0_s1" to="extensively overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30575" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o30576" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o30574" id="r2319" name="part_of" negation="false" src="d0_s1" to="o30575" />
    </statement>
    <statement id="d0_s2">
      <text>cauline 2–8, blade triangular to lanceolate, 20–120 x 5–20 mm, 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping or extensively overlapping distally, serrate, surfaces glabrous or unevenly hispid to tomentose.</text>
      <biological_entity id="o30577" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o30578" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o30579" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o30580" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="nonoverlapping" value_original="nonoverlapping" />
        <character is_modifier="false" modifier="extensively; distally" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30581" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o30582" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="unevenly hispid" name="pubescence" src="d0_s2" to="tomentose" />
      </biological_entity>
      <relation from="o30580" id="r2320" name="part_of" negation="false" src="d0_s2" to="o30581" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes simple, 1–2, exceeding basal leaves, each 10–40-flowered;</text>
      <biological_entity id="o30583" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="10-40-flowered" value_original="10-40-flowered" />
      </biological_entity>
      <biological_entity constraint="basal" id="o30584" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o30583" id="r2321" name="exceeding" negation="false" src="d0_s3" to="o30584" />
    </statement>
    <statement id="d0_s4">
      <text>bracts trullate to obtrullate or subulate to trullate, 10–25 x 2–5 mm, undivided or 1-auricled or 2-auricled, sometimes 1-pinnatifid, proximal margins entire, distal entire or serrate, surfaces tomentose.</text>
      <biological_entity id="o30585" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="trullate" name="shape" src="d0_s4" to="obtrullate or subulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-auricled" value_original="1-auricled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-auricled" value_original="2-auricled" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="1-pinnatifid" value_original="1-pinnatifid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30586" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30587" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30588" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–3 mm.</text>
      <biological_entity id="o30589" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 8–12 mm, tomentose, lobes 5, narrowly triangular, 3–4 mm, apex entire, ciliate;</text>
      <biological_entity id="o30590" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o30591" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o30592" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30593" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 20–26 mm, tube red or pink, 13–15 mm;</text>
      <biological_entity id="o30594" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o30595" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s7" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30596" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>galea red or pink, 7–11 mm, beakless, margins entire medially, 1-toothed distally, apex arching over abaxial lip;</text>
      <biological_entity id="o30597" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o30598" name="galea" name_original="galea" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity id="o30599" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="1-toothed" value_original="1-toothed" />
      </biological_entity>
      <biological_entity id="o30600" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="over abaxial lip" constraintid="o30601" is_modifier="false" name="orientation" src="d0_s8" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30601" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip red or pink, 6–7.5 mm. 2n = 32.</text>
      <biological_entity id="o30602" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o30603" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30604" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pedicularis cystopteridifolia occurs only in the Rocky Mountains of Montana and Wyoming. As the specific epithet implies, the leaves strongly resemble those of the fern Cystopteris fragilis. Although not sympatric, this species could easily be misidentified as P. sudetica subsp. scopulorum, which has flowers of a similar shape and color, and leaves that are also two-pinnatifid. The secondary leaf lobes of P. cystopteridifolia, however, are much larger, longer, more deeply incised, and more heavily toothed, making them appear more finely dissected than the linear to deltate secondary and smaller toothed lobes of P. sudetica. Many of the adjacent leaf lobes of P. cystopteridifolia also overlap, whereas the lobes of P. sudetica are more widely spaced and therefore not overlapping. The galea of P. cystopteridifolia is also more highly domed and broader, and the leaves are a paler shade of green in contrast to the dark green leaves of P. sudetica subsp. scopulorum.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky alpine tundras, meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky alpine" />
        <character name="habitat" value="meadows" modifier="tundras" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>