<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="species">multiflora</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 22. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species multiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphyllon</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">multiflorum</taxon_name>
    <taxon_hierarchy>genus aphyllon;species multiflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myzorrhiza</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="unknown" rank="species">multiflora</taxon_name>
    <taxon_hierarchy>genus myzorrhiza;species multiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orobanche</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">ludoviciana</taxon_name>
    <taxon_name authority="(Nuttall) L. T. Collins ex H. L. White &amp; W. C. Holmes" date="unknown" rank="subspecies">multiflora</taxon_name>
    <taxon_hierarchy>genus orobanche;species ludoviciana;subspecies multiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ludoviciana</taxon_name>
    <taxon_name authority="(Nuttall) Beck" date="unknown" rank="variety">multiflora</taxon_name>
    <taxon_hierarchy>genus o.;species ludoviciana;variety multiflora</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Many-flower broomrape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple or few-branched, 7–27 cm, relatively slender, base usually not enlarged.</text>
      <biological_entity id="o4831" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="27" to_unit="cm" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4832" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually not" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots usually inconspicuous, slender, unbranched or branched.</text>
      <biological_entity id="o4833" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves numerous, appressed;</text>
      <biological_entity id="o4834" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate or broadly triangular, 3–10 mm, margins entire, sometimes ciliate, apex acute, surfaces glabrous.</text>
      <biological_entity id="o4835" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4836" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o4837" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4838" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemes, sometimes thyrsoid, pale-yellow, white, or tan proximally, purple or pale lavender distally, sometimes branched, densely glandular-pubescent, appearing whitish or canescent, sometimes with axillary branches;</text>
      <biological_entity constraint="inflorescences" id="o4839" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="thyrsoid" value_original="thyrsoid" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale lavender" value_original="pale lavender" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s4" value="pale lavender" value_original="pale lavender" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o4840" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <relation from="o4839" id="r427" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o4840" />
    </statement>
    <statement id="d0_s5">
      <text>flowers numerous;</text>
      <biological_entity id="o4841" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts erect to reflexed, ± lanceolate, 11–20 mm, apex acute or acuminate, densely glandular-pubescent.</text>
      <biological_entity id="o4842" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="reflexed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4843" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0–5 mm, much shorter than plant axis;</text>
      <biological_entity id="o4844" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character constraint="than plant axis" constraintid="o4845" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="plant" id="o4845" name="axis" name_original="axis" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 2.</text>
      <biological_entity id="o4846" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx pallid externally, purple internally, weakly bilaterally symmetric, 15–21 mm, deeply divided into 5 lobes, lobes lanceolate-attenuate, densely glandular-pubescent;</text>
      <biological_entity id="o4847" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4848" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="coloration" src="d0_s9" value="pallid" value_original="pallid" />
        <character is_modifier="false" modifier="internally" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="weakly bilaterally" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="21" to_unit="mm" />
        <character constraint="into lobes" constraintid="o4849" is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o4849" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4850" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate-attenuate" value_original="lanceolate-attenuate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla 22–36 mm, tube white to pallid or cream, sometimes pinkish or light purplish tinged, rarely light yellow distally, sometimes with purple veins, constricted above ovary, only slightly bent forward, densely pubescent;</text>
      <biological_entity id="o4851" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4852" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s10" to="36" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4853" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pallid or cream" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="light purplish" value_original="light purplish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="tinged" value_original="tinged" />
        <character is_modifier="false" modifier="rarely; distally" name="coloration" src="d0_s10" value="light yellow" value_original="light yellow" />
        <character constraint="above ovary" constraintid="o4855" is_modifier="false" name="size" notes="" src="d0_s10" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="only slightly; forward" name="shape" notes="" src="d0_s10" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4854" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o4855" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <relation from="o4853" id="r428" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o4854" />
    </statement>
    <statement id="d0_s11">
      <text>palatal folds prominent, yellow, densely pubescent;</text>
      <biological_entity id="o4856" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4857" name="fold" name_original="folds" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lips externally white to pallid or cream, sometimes pinkish or light purplish tinged, internally pink or purple, sometimes white with purple veins, rarely light yellow, abaxial lip spreading, 5–9 mm, lobes broadly lanceolate, apex obtuse or rounded, adaxial lip erect or reflexed, 6–12 mm, lobes oblong, apex rounded;</text>
      <biological_entity id="o4858" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4859" name="lip" name_original="lips" src="d0_s12" type="structure">
        <character char_type="range_value" from="externally white" name="coloration" src="d0_s12" to="pallid or cream" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light purplish" value_original="light purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tinged" value_original="tinged" />
        <character is_modifier="false" modifier="internally" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light purplish" value_original="light purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tinged" value_original="tinged" />
        <character is_modifier="false" modifier="internally" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character constraint="with veins" constraintid="o4860" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" notes="" src="d0_s12" value="light yellow" value_original="light yellow" />
      </biological_entity>
      <biological_entity id="o4860" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4861" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4862" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o4863" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4864" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4865" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o4866" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments pilose with ring of hairs at insertion, anthers included, woolly.</text>
      <biological_entity id="o4867" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4868" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="with ring" constraintid="o4869" is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o4869" name="ring" name_original="ring" src="d0_s13" type="structure" />
      <biological_entity id="o4870" name="hair" name_original="hairs" src="d0_s13" type="structure" />
      <biological_entity id="o4871" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <relation from="o4869" id="r429" name="part_of" negation="false" src="d0_s13" to="o4870" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid, 8–12 mm.</text>
      <biological_entity id="o4872" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.4–0.5 mm. 2n = 48.</text>
      <biological_entity id="o4873" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4874" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orobanche multiflora is parasitic mainly on Gutierrezia and occasionally Heterotheca (Asteraceae). A population in southern Texas is parasitic on Varilla texana (Asteraceae).</discussion>
  <discussion>There is considerable confusion about what constitutes Orobanche multiflora. It has been interpreted as both a variety and subspecies of O. ludoviciana. The much larger flowers (22–36 mm versus 14–20 mm) set it apart from O. ludoviciana. P. A. Munz (1930) described four varieties of O. multiflora: vars. arenosa, multiflora (as typica), pringlei, and xanthocroa. Variety xanthocroa was based on a specimen of Conopholis. Variety arenosa is treated in synonymy under O. ludoviciana. Variety pringlei was based on a few specimens from northeastern Mexico that appear to represent an undescribed species.</discussion>
  <discussion>The distribution range is not well defined in the literature. The authors consider Orobanche multiflora to have a much more restricted range than previously indicated, occurring mostly in Colorado, New Mexico, and adjacent northwestern Texas, with disjunct populations in southern Texas that are apparently this species. Specimen data indicate that the species distribution is most likely east of the Continental Divide; plants from west of the continental divide with this binomial are probably misidentified.</discussion>
  <discussion>Plants from five counties in extreme southern Texas, near the Mexican border, appear to be a disjunct variant of Orobanche multiflora that flowers in spring. The flowers are about the same length, but the corolla tube is much narrower. Margins of the petals are often undulate and are an unusual shade of blue-purple. The pubescence at the base of the filaments is also reduced. Host and habitat differences also raise questions about the classification of these plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arid grasslands, semideserts, open woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arid grasslands" />
        <character name="habitat" value="semideserts" />
        <character name="habitat" value="woodlands" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>