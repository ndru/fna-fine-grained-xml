<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>David C. Michener, Noel H. Holmgren</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 203. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella</taxon_hierarchy>
    <other_info_on_name type="etymology">For David Daniels Keck, 1903–1995, California botanist, and ella, honor</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Lemaire" date=" 1862" rank="genus">Lepidostemon</taxon_name>
    <place_of_publication>
      <publication_title>Ill. Hort.</publication_title>
      <place_in_publication>9: plate 315. 1862</place_in_publication>
      <other_info_on_pub>not Hooker f. &amp; Thomson 1861 [Brassicaceae]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus lepidostemon</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs.</text>
      <biological_entity id="o20865" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, sometimes climbing, glabrous or hairy.</text>
      <biological_entity id="o20867" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves drought-deciduous, cauline, opposite, subopposite, or whorled;</text>
      <biological_entity id="o20868" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="drought-deciduous" value_original="drought-deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20869" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present or absent;</text>
      <biological_entity id="o20870" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade fleshy or not, leathery or not, margins entire or toothed, sometimes relatively small, distal leaf-blade not needlelike or scalelike.</text>
      <biological_entity id="o20871" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character name="texture" src="d0_s4" value="not" value_original="not" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
        <character name="texture" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o20872" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="sometimes relatively" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20873" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character is_modifier="false" name="shape" src="d0_s4" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, panicles, thyrses, corymbs, or spikelike racemes;</text>
      <biological_entity id="o20874" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o20875" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o20876" name="thyrse" name_original="thyrses" src="d0_s5" type="structure" />
      <biological_entity id="o20877" name="corymb" name_original="corymbs" src="d0_s5" type="structure" />
      <biological_entity id="o20878" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts present.</text>
      <biological_entity id="o20879" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present;</text>
      <biological_entity id="o20880" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles present, sometimes much reduced.</text>
      <biological_entity id="o20881" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes much" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o20882" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, basally connate, caly× weakly bilaterally symmetric, short-tubular, lobes lanceolate to ovate, sometimes oblanceolate;</text>
      <biological_entity id="o20883" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="caly×weakly bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="short-tubular" value_original="short-tubular" />
      </biological_entity>
      <biological_entity id="o20884" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla red, reddish orange, pink, white, cream, yellow, brownish yellow, or purplish brown, sometimes with red-purple nectar guides, bilaterally symmetric, strongly bilabiate, ± tubular, tube base not spurred or gibbous, lobes 5, abaxial 3, adaxial 2, adaxial lip hooded;</text>
      <biological_entity id="o20885" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish yellow" value_original="brownish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o20886" name="guide" name_original="guides" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s11" value="red-purple" value_original="red-purple" />
      </biological_entity>
      <biological_entity constraint="tube" id="o20887" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o20888" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20889" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20890" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20891" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
      </biological_entity>
      <relation from="o20885" id="r1587" modifier="sometimes" name="with" negation="false" src="d0_s11" to="o20886" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, basally adnate to corolla, didynamous, filaments basally eglandular-hairy, pollen-sacs explanate;</text>
      <biological_entity id="o20892" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o20893" is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o20893" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o20894" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s12" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity id="o20895" name="pollen-sac" name_original="pollen-sacs" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="explanate" value_original="explanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 1, filamentous;</text>
      <biological_entity id="o20896" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="texture" src="d0_s13" value="filamentous" value_original="filamentous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectary a hypogynous disc;</text>
      <biological_entity id="o20897" name="nectary" name_original="nectary" src="d0_s14" type="structure" />
      <biological_entity constraint="hypogynous" id="o20898" name="disc" name_original="disc" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o20899" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate.</text>
      <biological_entity id="o20900" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, dehiscence septicidal, sometimes also loculicidal distally, not densely packed with white-membranous hairs.</text>
      <biological_entity constraint="fruits" id="o20901" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" modifier="sometimes; distally" name="dehiscence" src="d0_s17" value="loculicidal" value_original="loculicidal" />
        <character constraint="with hairs" constraintid="o20902" is_modifier="false" modifier="not densely" name="arrangement" src="d0_s17" value="packed" value_original="packed" />
      </biological_entity>
      <biological_entity id="o20902" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="texture" src="d0_s17" value="white-membranous" value_original="white-membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 20–100, brown, ovoid, wings essentially absent.</text>
      <biological_entity id="o20903" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s18" to="100" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>x = 8.</text>
      <biological_entity id="o20904" name="wing" name_original="wings" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o20905" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7 (7 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Keckiella is nearly endemic to the California Floristic Province (P. H. Raven and D. I. Axelrod 1978). In the flora area, taxa range beyond California into Arizona and Nevada; two occur in Mexico (Baja California Peninsula).</discussion>
  <discussion>All taxa of Keckiella were first described in Penstemon, where they were long recognized as related to each other and anomalous within Penstemon. Keckiella taxa were treated as Keckia Straw before the prior use of that name for a fossil alga was noticed. The relationship of Keckiella and Penstemon with the rest of Cheloneae was clarified by A. D. Wolfe et al. (2002). This confirmed the classification implied by D. D. Keck (1936) and proposed by R. M. Straw (1966). The two genera are now recognized as distinct evolutionary lineages based on morphology, rust fungi relationships, phytochemistry, and DNA evidence (D. B. O. Savile 1968b, 1979; D. C. Michener 1982; O. Mistretta and R. Scogin 1989; Wolfe et al.; C. E. Freeman et al. 2003).</discussion>
  <discussion>Among Keckiella species, gross morphological and anatomical characteristics of leaf and wood correlate with habitat; floral characters, other than the distinctive nectary structure that helps define Keckiella, reflect the pollination syndrome (D. C. Michener 1981, 1982). Leaf morphology is typical of drought-deciduous plants (Michener 1981). Varieties are recognized in part on foliage and caly× indument. Leaf primordia of all species are (weakly) pubescent with glandular and non-glandular hairs, making indument a challenging character for interpretation.</discussion>
  <discussion>Pollination biology and habitat tolerances appear to have been important in species diversification. Bee pollination is fundamental in Keckiella, with floral size among the species scaled to a range of native bees; hummingbird pollination is polyphyletic in parallel with Penstemon (D. C. Michener 1982; C. E. Freeman et al. 2003; Paul Wilson et al. 2007). Natural hybrids have been found repeatedly for two species pairs (K. antirrhinoides var. antirrhinoides and K. cordifolia; K. breviflora var. glabrisepala and K. lemmonii) where the ranges overlap. The pollination syndromes are not effective barriers to extraneous cross pollination: even the hummingbird-pollinated species are visited by small pollen-foraging bees that may promote some pollination or hybridization separate from the hummingbirds (Michener). Leaf flavonoids do not support the hummingbird-pollinated species being each other’s closest relatives (O. Mistretta and R. Scogin 1989).</discussion>
  <discussion>Species of Keckiella are widespread but only locally common in their habitat. They usually occur in unstable sites such as slopes, scree, and crevices in rocky faces from near sea level to 3000 m. These local micro-sites may be less fire prone than the immediately surrounding communities. Fire ecology may be important in the evolution of the species; root gnarls found only in K. antirrhinoides var. antirrhinoides and K. cordifolia may help these species resprout following the fires typical of their low-elevation habitats (D. C. Michener 1981).</discussion>
  <discussion>The eco-evolutionary segregation of Keckiella from its relatives may have been centered on the Klamath region during the Tertiary (A. D. Wolfe et al. 2002). C. E. Freeman et al. (2003) recognized three clades within Keckiella: the basal-most K. rothrockii lineage, a northern clade (K. breviflora, K. corymbosa, K. lemmonii, and K. ternata), ranging from southern to northern California and through almost the entire 3000 m elevational range of the genus, and a southern clade (K. antirrhinoides and K. cordifolia) nearly restricted to lower elevations in southern California and Baja California, Mexico. They argued that speciation in these clades likely reflects repeated regional and elevational migration during the Pleistocene, presumably including the ecological scenarios that led to the modern species in fire-dominated vegetation types.</discussion>
  <references>
    <reference>Freeman, C. E. et al. 2003. Inferred phylogeny in Keckiella (Scrophulariaceae) based on noncoding chloroplast and nuclear ribosomal DNA sequences. Syst. Bot. 28: 782–790.</reference>
    <reference>Michener, D. C. 1982. Studies on the Evolution of Keckiella (Scrophulariaceae). Ph.D. dissertation. Claremont Graduate School.</reference>
    <reference>Mistretta, O. and R. Scogin. 1989. Foliar flavonoid aglycones of the genus Keckiella (Scrophulariaceae). Biochem. Syst. &amp; Ecol. 17: 455–457.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas pink, red, or reddish orange, tubes plus indistinct throats 16–25 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves whorled (3s), sometimes opposite; calyces 3.8–7.2 mm; corolla adaxial lips 5.5–9.5 mm.</description>
      <determination>5. Keckiella ternata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves opposite; calyces 6.4–13 mm; corolla adaxial lips 9–21 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf bases rounded, truncate, or cordate; pollen sacs 1.1–1.5 mm.</description>
      <determination>3. Keckiella cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf bases wedge-shaped; pollen sacs 0.9–1 mm.</description>
      <determination>7. Keckiella corymbosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas white, cream, yellow, brownish yellow, or purplish brown, tubes plus distinct throats 4–11 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Corollas 10–15 mm, tubes plus throats longer than adaxial lips, adaxial lips 2.6–6 mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences spikelike racemes; staminodes glabrous, usually included; stems densely short-hairy when young, not glaucous.</description>
      <determination>1. Keckiella rothrockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences panicles; staminodes densely yellow-hairy, exserted; stems glabrous when young, glaucous.</description>
      <determination>6. Keckiella lemmonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Corollas (12–)15–23 mm, tubes plus throats shorter than adaxial lips, adaxial lips 8–15 mm.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Staminodes densely yellow-hairy; corollas yellow; pollen sacs 1.1–1.8 mm.</description>
      <determination>2. Keckiella antirrhinoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Staminodes glabrous; corollas white or cream, lobes sometimes rose tinged; pollen sacs 0.6–0.8 mm.</description>
      <determination>4. Keckiella breviflora</determination>
    </key_statement>
  </key>
</bio:treatment>