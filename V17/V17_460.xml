<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Eastwood" date="1905" rank="species">anguineus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>32: 208. 1905</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species anguineus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>140.</number>
  <other_name type="common_name">Siskiyou beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o17762" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (10–) 30–80 cm, glabrous, not glaucous.</text>
      <biological_entity id="o17763" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 30–150 × 6–40 mm, blade ovate to elliptic or oval, base tapered, margins serrate to dentate, sometimes denticulate or entire, apex obtuse to acute;</text>
      <biological_entity id="o17764" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o17765" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o17766" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic or oval" />
      </biological_entity>
      <biological_entity id="o17767" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o17768" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrate to dentate" value_original="serrate to dentate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17769" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 2–4 pairs, sessile or proximals short-petiolate, 11–90 × 5–50 mm, blade oblanceolate to oblong or triangular-ovate, base tapered to cordate-clasping, margins serrate to denticulate, rarely entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o17770" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17771" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17772" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="oblong or triangular-ovate" />
      </biological_entity>
      <biological_entity id="o17773" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o17774" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s4" to="denticulate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17775" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, sometimes continuous, cylindric, (4–) 8–40 cm, axis glandular-pubescent, verticillasters 3–10, cymes (2 or) 3–15-flowered, 2 per node;</text>
      <biological_entity id="o17776" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17777" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o17778" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o17779" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-15-flowered" value_original="3-15-flowered" />
        <character constraint="per node" constraintid="o17780" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o17780" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts ovate to triangular, 10–85 × 7–40 mm, margins serrate or entire;</text>
      <biological_entity constraint="proximal" id="o17781" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="triangular" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="85" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17782" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels ascending, glandular-pubescent.</text>
      <biological_entity id="o17783" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o17784" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes lanceolate, 4–6.5 × 0.9–2 mm, glandular-pubescent;</text>
      <biological_entity id="o17785" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o17786" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla light blue to lavender, purple, or violet, with violet nectar guides, ampliate, 13–18 mm, glandular-pubescent externally, glabrous or sparsely white-lanate internally abaxially, tube 5–7 mm, throat gradually inflated, 4–6 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o17787" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17788" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s9" to="lavender purple or violet" />
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s9" to="lavender purple or violet" />
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s9" to="lavender purple or violet" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o17789" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o17790" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17791" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o17788" id="r1376" name="with" negation="false" src="d0_s9" to="o17789" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, 0.8–1.2 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o17792" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17793" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o17794" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o17795" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o17796" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17797" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 10–13 mm, exserted, 0.2–0.4 mm diam., tip straight to slightly recurved, distal 2–6 mm glabrous or sparsely pilose, hairs yellow, to 0.6 mm;</text>
      <biological_entity id="o17798" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17799" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17800" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17801" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o17802" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 10–13 mm.</text>
      <biological_entity id="o17803" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17804" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–7 × 3–4 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o17805" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17806" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon anguineus occurs primarily in the Cascade, Klamath, and North Coast ranges from southwestern Oregon (Curry, Douglas, Jackson, Josephine, and Klamath counties) south into northwestern California (Del Norte, El Dorado, Glenn, Humboldt, Mendocino, Siskiyou, Tehama, and Trinity counties).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, clayey, or humus soils, openings in coniferous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" modifier="rocky clayey or humus" />
        <character name="habitat" value="openings" constraint="in coniferous forests" />
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>