<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="Lindley" date="1836" rank="species">heterophyllus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">heterophyllus</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species heterophyllus;variety heterophyllus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>209a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous, rarely retrorsely hairy.</text>
      <biological_entity id="o31961" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: axillary fascicles present, sometimes absent;</text>
      <biological_entity id="o31962" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 10–55 × 0.5–6 mm.</text>
      <biological_entity id="o31963" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o31964" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses: axis glabrous.</text>
      <biological_entity id="o31965" name="thyrse" name_original="thyrses" src="d0_s3" type="structure" />
      <biological_entity id="o31966" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx lobes obovate to oblanceolate, rarely lanceolate.</text>
      <biological_entity id="o31967" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="calyx" id="o31968" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety heterophyllus occurs through much of western California from Humboldt County to San Diego County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, chaparral, sagebrush shrublands, pine woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="woodlands" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>