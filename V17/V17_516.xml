<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Greene" date="1906" rank="species">pratensis</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 165. 1906</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species pratensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>180.</number>
  <other_name type="common_name">White-flower beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o23851" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 15–40 (–65) cm, glabrous, not glaucous.</text>
      <biological_entity id="o23852" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="65" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline (23–) 40–90 (–120) × 6–14 (–24) mm, blade spatulate to elliptic, base tapered, margins entire, apex rounded to obtuse or acute;</text>
      <biological_entity id="o23853" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o23854" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o23855" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="elliptic" />
      </biological_entity>
      <biological_entity id="o23856" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o23857" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23858" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–6 pairs, sessile or proximals short to long-petiolate, 24–115 × 4–15 mm, blade oblanceolate to lanceolate, base tapered to truncate or clasping, margins entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o23859" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o23860" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character char_type="range_value" from="24" from_unit="mm" name="length" src="d0_s4" to="115" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23861" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o23862" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o23863" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23864" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, cylindric, 6–15 cm, axis glabrous, verticillasters 2–6, cymes 3–11-flowered, 2 per node;</text>
      <biological_entity id="o23865" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23866" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23867" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o23868" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-11-flowered" value_original="3-11-flowered" />
        <character constraint="per node" constraintid="o23869" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23869" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 8–50 × 1–8 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o23870" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="50" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23871" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels erect, glabrous.</text>
      <biological_entity id="o23872" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23873" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate to lanceolate, 3–5.5 × 1.2–2.1 mm, apex acute to short-caudate, glabrous;</text>
      <biological_entity id="o23874" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o23875" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23876" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="short-caudate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla white or lavender, without nectar guides, funnelform, 10–15 mm, glabrous externally, moderately yellow or white-lanate internally abaxially, tube 4–5 mm, throat slightly inflated, 3–4 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o23877" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23878" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o23879" name="guide" name_original="guides" src="d0_s9" type="structure" />
      <biological_entity id="o23880" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23881" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o23878" id="r1835" name="without" negation="false" src="d0_s9" to="o23879" />
    </statement>
    <statement id="d0_s10">
      <text>stamens: longer pair reaching orifice or slightly exserted, pollen-sacs opposite, navicular, 0.6–0.8 mm, dehiscing completely, connective splitting, sides glabrous, sutures smooth;</text>
      <biological_entity id="o23882" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o23883" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o23884" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s10" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o23885" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o23886" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23887" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o23882" id="r1836" name="reaching" negation="false" src="d0_s10" to="o23883" />
    </statement>
    <statement id="d0_s11">
      <text>staminode 6–7 mm, included, 0.3–0.4 mm diam., tip straight, distal 0.5–3 mm moderately pilose, hairs yellow, to 0.8 mm, rarely glabrous;</text>
      <biological_entity id="o23888" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o23889" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23890" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="position_or_shape" src="d0_s11" value="distal" value_original="distal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o23891" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 7–8 mm.</text>
      <biological_entity id="o23892" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o23893" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–7 × 3–4 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 32.</text>
      <biological_entity id="o23894" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23895" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon pratensis is known from the northern Central Great Basin and Owyhee Desert regions of the northern Great Basin. The species is documented in Owyhee County, Idaho, Elko and Eureka counties, Nevada, and Harney and Malheur counties, Oregon. In Elko County, Nevada, where it occurs with P. rydbergii var. oreocharis, P. pratensis appears to occur mostly in the East Humboldt Range and Ruby Mountains, south and east of the Humboldt River. Most populations of P. rydbergii var. oreocharis in that county occur north and west of the Humboldt River in the Independence and Jarbidge mountains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, stream banks, aspen woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="woodlands" modifier="aspen" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>