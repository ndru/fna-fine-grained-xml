<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">54</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CALLITRICHE</taxon_name>
    <taxon_name authority="Linnaeus" date="1755" rank="species">hermaphroditica</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Pl. I,</publication_title>
      <place_in_publication>31. 1755</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus callitriche;species hermaphroditica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Callitriche</taxon_name>
    <taxon_name authority="(Linnaeus) Morong" date="unknown" rank="species">bifida</taxon_name>
    <taxon_hierarchy>genus callitriche;species bifida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="variety">bifida</taxon_name>
    <taxon_hierarchy>genus c.;species palustris;variety bifida</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Autumnal or northern water-starwort</other_name>
  <other_name type="common_name">callitriche hermaphrodiate</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves not connate at base, ± linear, tapering from base, (7–) 7.6–11.5 × 0.9–1.4 mm, 1-veined.</text>
      <biological_entity id="o3024" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at base" constraintid="o3025" is_modifier="false" modifier="not" name="fusion" src="d0_s0" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" notes="" src="d0_s0" value="linear" value_original="linear" />
        <character constraint="from base" constraintid="o3026" is_modifier="false" name="shape" src="d0_s0" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" notes="" src="d0_s0" to="7.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.6" from_unit="mm" name="length" notes="" src="d0_s0" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" notes="" src="d0_s0" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o3025" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o3026" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stem and leaf scales absent.</text>
      <biological_entity id="o3027" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o3028" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: bracts absent.</text>
      <biological_entity id="o3029" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o3030" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 0 mm in fruit.</text>
      <biological_entity id="o3031" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character constraint="in fruit" constraintid="o3032" name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
      <biological_entity id="o3032" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers solitary;</text>
      <biological_entity id="o3033" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>styles erect, later recurved;</text>
      <biological_entity id="o3034" name="style" name_original="styles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="later" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen colorless.</text>
      <biological_entity id="o3035" name="pollen" name_original="pollen" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="colorless" value_original="colorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Schizocarps 1.1–1.9 × 1.2–2.4 mm, shorter than or as long as wide, sometimes longer than wide;</text>
      <biological_entity id="o3036" name="schizocarp" name_original="schizocarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s7" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="2.4" to_unit="mm" />
        <character constraint="than or" constraintid="" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s7" value="wide" value_original="wide" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="sometimes longer than wide" value_original="sometimes longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mericarps dark-brown, not swollen, winged throughout, wings straight, 0.2–0.8 mm wide, as wide as or wider than mericarp body.</text>
      <biological_entity id="o3037" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="mericarp" id="o3040" name="body" name_original="body" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="mericarp" id="o3039" name="body" name_original="body" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wider" value_original="wider" />
      </biological_entity>
      <relation from="o3038" id="r280" name="as wide as" negation="false" src="d0_s8" to="o3040" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 6.</text>
      <biological_entity id="o3038" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3041" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Infraspecific divisions have been based on the size of fruit (for example, R. V. Lansdown 2006); assignment of North American populations to these taxa would be premature.</discussion>
  <discussion>Callitriche autumnalis Linnaeus is a superfluous renaming of C. hermaphroditica, with which it is homotypic (R. V. Lansdown and C. E. Jarvis 2004).</discussion>
  <discussion>Callitriche fassettii, C. hermaphroditica, and C. stenoptera are morphologically very close; however, C. hermaphroditica always has sessile fruit, whereas the fruit of both the other species may be pedicellate. The mericarp wing of C. stenoptera is consistently very narrow and that of C. fassettii is slightly wider than that of C. hermaphroditica, which is generally wide, particularly at the apex.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lakes, ponds, backwaters, ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="backwaters" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Nfld.), N.W.T., Ont., Que., Sask.; Alaska, Colo., Idaho, Mont., Nebr., N.J., N.Y., Oreg., Utah, Vt., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>