<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Bentham in W. J. Hooker" date="1838" rank="species">contorta</taxon_name>
    <taxon_name authority="R. N. Reese" date="1984" rank="variety">rubicunda</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>36: 63, figs. 1–3. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species contorta;variety rubicunda</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>9c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: bracts 2–4 mm wide, proximal margins glabrous.</text>
      <biological_entity id="o39384" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o39385" name="bract" name_original="bracts" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o39386" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx reddish with purple spots;</text>
      <biological_entity id="o39387" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o39388" name="calyx" name_original="calyx" src="d0_s1" type="structure">
        <character constraint="with purple spots" is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>corolla: tube pink to pinkish purple;</text>
      <biological_entity id="o39389" name="corolla" name_original="corolla" src="d0_s2" type="structure" />
      <biological_entity id="o39390" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s2" to="pinkish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>galea pink to pinkish purple;</text>
      <biological_entity id="o39391" name="corolla" name_original="corolla" src="d0_s3" type="structure" />
      <biological_entity id="o39392" name="galea" name_original="galea" src="d0_s3" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s3" to="pinkish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>abaxial lip pink to pinkish purple.</text>
      <biological_entity id="o39393" name="corolla" name_original="corolla" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 16.</text>
      <biological_entity constraint="abaxial" id="o39394" name="lip" name_original="lip" src="d0_s4" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s4" to="pinkish purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39395" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reese noted that the center of distribution of var. rubicunda was Clearwater County, Idaho, and adjacent Ravalli County, Montana. Intermediates are common between var. contorta and var. rubicunda in areas of sympatry where the same species of Bombus pollinate both varieties during foraging (Reese).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open subalpine forests, meadows, dry rocky exposures, deep loam soils, rocky well-drained soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open subalpine forests" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="dry rocky exposures" />
        <character name="habitat" value="deep loam soils" />
        <character name="habitat" value="rocky well-drained soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>