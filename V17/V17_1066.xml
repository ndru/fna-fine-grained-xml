<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">607</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(A. Heller) T. I. Chuang &amp; Heckard" date="1991" rank="species">exserta</taxon_name>
    <taxon_name authority="(A. Heller) J. M. Egger" date="2008" rank="variety">venusta</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>90: 70. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species exserta;variety venusta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="A. Heller" date="1906" rank="species">venustus</taxon_name>
    <place_of_publication>
      <publication_title>Muhlenbergia</publication_title>
      <place_in_publication>2: 141. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species venustus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">exserta</taxon_name>
    <taxon_name authority="(A. Heller) T. I. Chuang &amp; Heckard" date="unknown" rank="subspecies">venusta</taxon_name>
    <taxon_hierarchy>genus castilleja;species exserta;subspecies venusta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">ornatus</taxon_name>
    <taxon_hierarchy>genus o.;species purpurascens;variety ornatus</taxon_hierarchy>
  </taxon_identification>
  <number>32c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs 0.4–2.3 dm.</text>
      <biological_entity id="o39057" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s0" to="2.3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences 2–12.5 cm;</text>
      <biological_entity id="o39058" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="12.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracts: distal apices pink, light purple, or yellow or orange-spotted, 4–5.5 mm, (3–) 5 (–7) -lobed;</text>
      <biological_entity id="o39059" name="bract" name_original="bracts" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o39060" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="light purple" value_original="light purple" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="orange-spotted" value_original="orange-spotted" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="light purple" value_original="light purple" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="orange-spotted" value_original="orange-spotted" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="(3-)5(-7)-lobed" value_original="(3-)5(-7)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>lobes linear to narrowly spatulate, 2 × 0.8–1.2 mm.</text>
      <biological_entity id="o39061" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o39062" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly spatulate" />
        <character name="length" src="d0_s3" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyces 16–19 mm;</text>
      <biological_entity id="o39063" name="calyx" name_original="calyces" src="d0_s4" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s4" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial clefts 6.5–9.5 mm, adaxial 9–13 mm, clefts 40–70% of calyx length, lateral 5.5–8 mm, 34–45% of calyx length.</text>
      <biological_entity constraint="abaxial" id="o39064" name="cleft" name_original="clefts" src="d0_s5" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s5" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39065" name="cleft" name_original="clefts" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39066" name="cleft" name_original="clefts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="40-70%" name="position" src="d0_s5" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 20–27 mm;</text>
      <biological_entity id="o39067" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tube 13–15 mm;</text>
      <biological_entity id="o39068" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak 7–10 mm;</text>
      <biological_entity id="o39069" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip deep red-purple proximally, distal 1/4 bright-yellow to yellow-orange, becoming orange or red after anthesis, 4–7 mm.</text>
      <biological_entity constraint="abaxial" id="o39070" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="proximally" name="coloration_or_density" src="d0_s9" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s9" value="1/4" value_original="1/4" />
        <character char_type="range_value" from="bright-yellow" name="coloration" src="d0_s9" to="yellow-orange" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character constraint="after anthesis" is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety venusta is endemic to the western Mojave Desert region of southern California. Typical var. exserta is found east and west of it, in less arid grasslands. In var. venusta, the corolla is consistently different in color pattern and intensity from var. exserta.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–May(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sand, desert washes, creosote scrub, saltbush flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sand" />
        <character name="habitat" value="desert washes" />
        <character name="habitat" value="creosote scrub" />
        <character name="habitat" value="flats" modifier="saltbush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>