<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">348</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">SCROPHULARIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERBASCUM</taxon_name>
    <taxon_name authority="Boissier" date="1844" rank="species">bombyciferum</taxon_name>
    <place_of_publication>
      <publication_title>Diagn. Pl. Orient.</publication_title>
      <place_in_publication>1(4): 52. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family scrophulariaceae;genus verbascum;species bombyciferum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Turkish or silver mullein</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials.</text>
      <biological_entity id="o14821" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 60–200 cm, densely and persistently tomentose, eglandular.</text>
      <biological_entity id="o14822" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s1" to="200" to_unit="cm" />
        <character is_modifier="false" modifier="densely; persistently" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: surfaces densely and persistently tomentose, eglandular;</text>
      <biological_entity id="o14823" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14824" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline with petiole 15–40 mm;</text>
      <biological_entity id="o14825" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character constraint="with petiole" constraintid="o14826" is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o14826" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly elliptic to lanceolate-oblong, 25–35 × 15–25 cm, base attenuate;</text>
      <biological_entity id="o14827" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14828" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="lanceolate-oblong" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14829" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline slightly auriculate-clasping, gradually smaller distally, base not decurrent, margins obscurely crenate or entire, apex of distal cauline and floral bracts acute.</text>
      <biological_entity id="o14830" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline distal and floral" id="o14831" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o14832" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o14833" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14834" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="cauline distal and floral" id="o14835" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o14834" id="r1162" name="part_of" negation="false" src="d0_s5" to="o14835" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences unbranched or branched from proximal nodes, narrowly cylindric, flowers loosely overlapping, in clusters of 2–8;</text>
      <biological_entity id="o14836" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character constraint="from proximal nodes" constraintid="o14837" is_modifier="false" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s6" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14837" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o14838" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>rachis densely and persistently tomentose, eglandular;</text>
      <biological_entity id="o14839" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts ovate to lanceolate-triangular, 7–12 mm, base not decurrent, apex acuminate, densely and persistently tomentose, eglandular.</text>
      <biological_entity id="o14840" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate-triangular" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14841" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o14842" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="densely; persistently" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels free, 2–5 mm;</text>
      <biological_entity id="o14843" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2.</text>
      <biological_entity id="o14844" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: calyx 6–10 mm, densely and persistently tomentose, eglandular, lobes lanceolate to narrowly lanceolate;</text>
      <biological_entity id="o14845" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14846" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="densely; persistently" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o14847" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla yellow, (20–) 30–40 mm diam., pellucid glands absent;</text>
      <biological_entity id="o14848" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14849" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s12" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="diameter" src="d0_s12" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14850" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="pellucid" value_original="pellucid" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments villous, hairs yellowish to yellowish white, or 2 proximal glabrous distally or completely;</text>
      <biological_entity id="o14851" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14852" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o14853" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s13" to="yellowish white" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14854" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="distally; distally; completely" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma spatulate, base decurrent.</text>
      <biological_entity id="o14855" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o14856" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o14857" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid to subglobular, 5–8 mm, tomentose.</text>
      <biological_entity id="o14858" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="subglobular" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Verbascum bombyciferum is naturalized in Sonoma County, escaped from ornamental plantings in 1976 at a residence on the Pepperwood Preserve (F. Hrusa et al. 2002). Photos of the population (http://www.calflora.org) show plants (intermixed with typical V. thapsus) with a dense, persistent, bright white vestiture, spikes unbranched or proximally few-branched and 1–2 m, the floral clusters thick and somewhat remotely arranged, yellow corollas with yellowish to yellowish white filament hairs, and broadly elliptic, basally attenuate leaves densely and persistently tomentose on both surfaces. Internet photos confirm the identification as V. bombyciferum and indicate that the Calflora photos show plants just beginning to flower, as the plants potentially elongate proximally and the spikes may develop lateral branches, although the central one usually remains dominant.</discussion>
  <discussion>Verbascum bombyciferum of Sonoma County has been identified previously (F. Hrusa et al. 2002) as V. olympicum Boissier, and that name has correspondingly been registered in other literature. Verbascum bombyciferum (as well as V. olympicum) is endemic in native range to Mount Olympus (now known as Uludağ) in northwestern Turkey.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy, rocky benches, streambeds.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy" />
        <character name="habitat" value="rocky benches" />
        <character name="habitat" value="streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Asia (Turkey); introduced also in Europe (England, Germany).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Asia (Turkey)" establishment_means="native" />
        <character name="distribution" value=" also in Europe (England)" establishment_means="introduced" />
        <character name="distribution" value=" also in Europe (Germany)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>