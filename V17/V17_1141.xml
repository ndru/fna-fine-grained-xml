<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">643</other_info_on_meta>
    <other_info_on_meta type="mention_page">582</other_info_on_meta>
    <other_info_on_meta type="mention_page">589</other_info_on_meta>
    <other_info_on_meta type="mention_page">635</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Pennell" date="1941" rank="species">peckiana</taxon_name>
    <place_of_publication>
      <publication_title>Notul. Nat. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>74: 9. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species peckiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>85.</number>
  <other_name type="common_name">Peck’s paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (1.8–) 2.4–6 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a stout taproot.</text>
      <biological_entity id="o14155" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.8" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2.4" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14156" name="taproot" name_original="taproot" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o14155" id="r1107" name="with" negation="false" src="d0_s2" to="o14156" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, erect or ascending, often branched distally, sometimes unbranched, sometimes with short axillary shoots, proximal hairs retrorse to appressed, short to moderately long, distal hairs spreading, longer, soft, often mixed with short-stipitate-glandular ones.</text>
      <biological_entity id="o14157" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o14158" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14159" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="retrorse" name="orientation" src="d0_s3" to="appressed" />
        <character is_modifier="false" name="size_or_length" src="d0_s3" value="short to moderately" value_original="short to moderately" />
        <character is_modifier="false" modifier="moderately" name="character" src="d0_s3" value="length" value_original="length" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14160" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with ones" constraintid="o14161" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o14161" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o14157" id="r1108" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o14158" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, linear-lanceolate, rarely broadly lanceolate, (1.2–) 2.5–8 (–9) cm, not fleshy, margins plane, flat or involute, 0 (–3) -lobed, apex acute to rounded;</text>
      <biological_entity id="o14162" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o14163" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0(-3)-lobed" value_original="0(-3)-lobed" />
      </biological_entity>
      <biological_entity id="o14164" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending-spreading, narrowly lanceolate to linear, apex acute or obtuse.</text>
      <biological_entity id="o14165" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o14166" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (2–) 4–17 × 1.5–3 cm;</text>
      <biological_entity id="o14167" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="17" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, distally red, orange-red, or orange, proximal sometimes lanceolate, distal broadly lanceolate to ovate, (0–) 3 (–7) -lobed;</text>
      <biological_entity id="o14168" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14169" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14170" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(0-)3(-7)-lobed" value_original="(0-)3(-7)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading to ascending, linear to lanceolate, long, arising near or below mid length, central lobe apex rounded, lateral ones acute.</text>
      <biological_entity id="o14171" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="below mid below mid lobes length" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o14172" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces proximally pale-yellow or greenish, distally colored as bracts, (15–) 18–28 mm;</text>
      <biological_entity id="o14173" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character constraint="as bracts" constraintid="o14174" is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14174" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 6–12 mm, 40–45% of calyx length, deeper than laterals, lateral 2.5–8 mm, 20–30% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o14175" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="than laterals" constraintid="o14176" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="40-45%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14176" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes linear to narrowly lanceolate, apex acute to acuminate.</text>
      <biological_entity id="o14177" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o14178" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, 23–30 (–35) mm;</text>
      <biological_entity id="o14179" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="35" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 12–20 mm;</text>
      <biological_entity id="o14180" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, adaxially green, 8–12 (–14) mm;</text>
      <biological_entity id="o14181" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip pale to deep green, reduced, rounded, 0.5–1.5 mm, 10–20% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o14182" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s15" to="deep green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14183" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o14182" id="r1109" modifier="10-20%" name="as long as" negation="false" src="d0_s15" to="o14183" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect to incurved, green, (0.4–) 0.7–1.2 (–2) mm.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 72, 96, ca. 120.</text>
      <biological_entity id="o14184" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s16" to="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14185" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="72" value_original="72" />
        <character name="quantity" src="d0_s17" value="96" value_original="96" />
        <character name="quantity" src="d0_s17" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <discussion>When describing Castilleja peckiana, Pennell noted that its variation approached C. hispida on one hand and C. miniata on the other, and it is likely of allopolyploid derivation. L. R. Heckard (1968) found chromosome numbers of n = 36, 48, and ca. 60. He hypothesized that C. chromosa, C. hispida var. acuta, and C. miniata were likely involved in its ancestry, and possibly C. pruinosa as well. Heckard suggested subsequent introgression among the derived forms introduced further complexity. Though complex, these forms are self-perpetuating and appear morphologically stable within their range.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open conifer forests, sagebrush slopes, riparian meadows, shores.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open conifer forests" />
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="meadows" modifier="riparian" />
        <character name="habitat" value="shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>