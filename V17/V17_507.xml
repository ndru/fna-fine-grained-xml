<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1935" rank="species">laxiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 229. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species laxiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>171.</number>
  <other_name type="common_name">Nodding beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o9128" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 25–65 (–70) cm, puberulent or retrorsely hairy, rarely also glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o9129" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="65" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely; distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal often withering by anthesis, not leathery, glabrous or retrorsely hairy to puberulent;</text>
      <biological_entity id="o9130" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 25–90 × 8–25 mm, blade spatulate, oblanceolate, or ovate, base tapered, rarely truncate, margins subentire or serrate to dentate, apex rounded to obtuse or acute;</text>
      <biological_entity constraint="basal" id="o9131" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="often" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="retrorsely hairy" name="pubescence" src="d0_s2" to="puberulent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o9132" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o9133" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o9134" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o9135" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o9136" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline (3–) 5–7 pairs, short-petiolate or sessile, (20–) 30–90 (–110) × (2–) 5–22 mm, blade lanceolate to oblanceolate, base tapered to cordate-clasping, margins subentire or serrate to dentate, apex acute.</text>
      <biological_entity constraint="cauline" id="o9137" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="110" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9138" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o9139" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o9140" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to dentate" value_original="serrate to dentate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o9141" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, conic, 5–26 (–32) cm, axis retrorsely hairy and ± glandular-pubescent, verticillasters 3–7, cymes 2–6-flowered, branches of each cyme usually elongating, of equal length, 2 per node;</text>
      <biological_entity id="o9142" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="32" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="26" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9143" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o9144" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o9145" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
      <biological_entity id="o9146" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s5" value="elongating" value_original="elongating" />
        <character constraint="per node" constraintid="o9148" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9147" name="cyme" name_original="cyme" src="d0_s5" type="structure" />
      <biological_entity id="o9148" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o9146" id="r731" name="part_of" negation="false" src="d0_s5" to="o9147" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 4–28 × 1–6 mm, margins entire or ± serrulate;</text>
      <biological_entity constraint="proximal" id="o9149" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="28" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9150" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels spreading or ascending, puberulent or retrorsely hairy and, usually, glandular-pubescent.</text>
      <biological_entity id="o9151" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o9152" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate to lanceolate, 2.5–5.5 × 2–3 mm, sparsely puberulent and glandular-pubescent;</text>
      <biological_entity id="o9153" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o9154" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla white to light lavender, sometimes tinged pink, with reddish purple nectar guides, tubular, 20–28 (–30) mm, glandular-pubescent externally, moderately white or yellow-lanate internally abaxially, tube 5–7 mm, throat abruptly inflated, 4–8 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o9155" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9156" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="light lavender" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="tinged pink" value_original="tinged pink" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="28" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="moderately" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="yellow-lanate" value_original="yellow-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o9157" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o9158" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9159" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o9156" id="r732" name="with" negation="false" src="d0_s9" to="o9157" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, 1–1.3 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o9160" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9161" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o9162" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o9163" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o9164" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9165" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 15–20 mm, exserted, 0.6–0.8 mm diam., tip straight to slightly recurved, distal 8–10 mm densely pilose, hairs yellow, to 1.5 mm;</text>
      <biological_entity id="o9166" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9167" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9168" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9169" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9170" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 14–20 mm.</text>
      <biological_entity id="o9171" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9172" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 8–10 × 6–7 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o9173" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9174" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon laxiflorus is a species of the central and western Gulf Coastal Plain and southern Interior Lowland. Pennell cited one specimen each from Florida and Georgia; specimens from those states have not been confirmed. The species shares many features with P. australis, which occurs farther east along the southern Atlantic Coastal Plain and eastern Gulf Coastal Plain. Penstemon laxiflorus usually can be distinguished from P. australis by stem vestiture. Penstemon laxiflorus has stems with only short, retrorse hairs or, if glandular hairs also are present, they are sparse and occur just below the inflorescences. By contrast, P. australis has stems with a distinct mix of short, eglandular hairs and much longer, glandular hairs.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or rocky open woods, tallgrass prairies, sand barrens.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky open woods" />
        <character name="habitat" value="tallgrass prairies" />
        <character name="habitat" value="sand barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., La., Miss., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>