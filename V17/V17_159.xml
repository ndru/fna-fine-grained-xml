<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CALLITRICHE</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="species">pedunculosa</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>5: 140. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus callitriche;species pedunculosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Nuttall’s water-starwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves connate at base, obovate-oblanceolate to spatulate, (2.2–) 2.7–5.7 × 0.7–2 mm, 2+-veined.</text>
      <biological_entity id="o29828" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at base" constraintid="o29829" is_modifier="false" name="fusion" src="d0_s0" value="connate" value_original="connate" />
        <character char_type="range_value" from="obovate-oblanceolate" name="shape" notes="" src="d0_s0" to="spatulate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="atypical_length" src="d0_s0" to="2.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s0" to="5.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s0" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="2+-veined" value_original="2+-veined" />
      </biological_entity>
      <biological_entity id="o29829" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stem and leaf scales present.</text>
      <biological_entity id="o29830" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o29831" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: bracts absent.</text>
      <biological_entity id="o29832" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o29833" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 0–5 mm in fruit.</text>
      <biological_entity id="o29834" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o29835" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29835" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers usually 1 staminate and 1 pistillate in 1 axil opposed by 1 pistillate, rarely both pairs at an axil with 1 staminate and 1 pistillate;</text>
      <biological_entity id="o29836" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="in axil" constraintid="o29837" is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29837" name="axil" name_original="axil" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="by 1 pistillate" is_modifier="false" name="position" src="d0_s4" value="opposed" value_original="opposed" />
      </biological_entity>
      <biological_entity id="o29838" name="axil" name_original="axil" src="d0_s4" type="structure">
        <character modifier="with 1 staminate" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o29836" id="r2268" modifier="rarely" name="at" negation="false" src="d0_s4" to="o29838" />
    </statement>
    <statement id="d0_s5">
      <text>styles loosely ascending or ± reflexed;</text>
      <biological_entity id="o29839" name="style" name_original="styles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen yellow.</text>
      <biological_entity id="o29840" name="pollen" name_original="pollen" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Schizocarps 0.8–1 × 0.9–1.3 mm, shorter than wide;</text>
      <biological_entity id="o29841" name="schizocarp" name_original="schizocarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s7" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s7" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter than wide" value_original="shorter than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mericarps black, not swollen, winged throughout, wings curled, giving appearance of thickened margins, 0.06–0.15 mm wide.</text>
      <biological_entity id="o29842" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o29844" name="appearance" name_original="appearance" src="d0_s8" type="structure" />
      <biological_entity id="o29845" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o29843" id="r2269" name="giving" negation="false" src="d0_s8" to="o29844" />
      <relation from="o29843" id="r2270" name="part_of" negation="false" src="d0_s8" to="o29845" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 20.</text>
      <biological_entity id="o29843" name="wing" name_original="wings" src="d0_s8" type="structure" constraint="margin" constraint_original="margin; margin">
        <character is_modifier="false" name="shape" src="d0_s8" value="curled" value_original="curled" />
        <character char_type="range_value" from="0.06" from_unit="mm" name="width" src="d0_s8" to="0.15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29846" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Callitriche nuttallii Torrey is an illegitimate, superfluous name that applies here. Torrey proposed it as a replacement for C. pedunculosa Nuttall based on the mistaken belief that the name by Nuttall was a later homonym of one by Arnott; the purported name by Arnott does not exist.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun(Sep–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet sandy soils, in or shaded by woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" modifier="or shaded by" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>