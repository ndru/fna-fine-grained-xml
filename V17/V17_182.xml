<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="species">verna</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 190, plate 9. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species verna</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Collinsia</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">bicolor</taxon_name>
    <taxon_hierarchy>genus collinsia;species bicolor</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 10–30 cm.</text>
      <biological_entity id="o17526" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o17527" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate to elliptic or lanceolate, base cuneate to subcordate, margins shallowly and coarsely serrate.</text>
      <biological_entity id="o17528" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="elliptic or lanceolate" />
      </biological_entity>
      <biological_entity id="o17529" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="subcordate" />
      </biological_entity>
      <biological_entity id="o17530" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences ± glandular, scaly-hairy;</text>
      <biological_entity id="o17531" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scaly-hairy" value_original="scaly-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes 1–6 (–8) -flowered;</text>
      <biological_entity id="o17532" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-6(-8)-flowered" value_original="1-6(-8)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers not crowded proximally, often crowded distally;</text>
      <biological_entity id="o17533" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="often; distally" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distalmost bracts linear, 5–6 (+) mm.</text>
      <biological_entity constraint="distalmost" id="o17534" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="average_some_measurement" src="d0_s6" to="6" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending to reflexed, usually longer than calyx, visible, glandular abaxially, glabrate adaxially.</text>
      <biological_entity id="o17535" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="reflexed" />
        <character constraint="than calyx" constraintid="o17536" is_modifier="false" name="length_or_size" src="d0_s7" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o17536" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: caly× lobes deltate, surpassing capsule, ape× acute to acuminate;</text>
      <biological_entity id="o17537" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o17538" name="capsule" name_original="capsule" src="d0_s8" type="structure" />
      <relation from="o17537" id="r1361" name="surpassing" negation="false" src="d0_s8" to="o17538" />
    </statement>
    <statement id="d0_s9">
      <text>corolla blue, banner white to pale lilac, base yellow with small maroon spots, wings and keel bluish, 8–15 mm, keel sparsely glandular;</text>
      <biological_entity id="o17539" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17540" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o17541" name="banner" name_original="banner" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale lilac" />
      </biological_entity>
      <biological_entity id="o17542" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="with spots" constraintid="o17543" is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17543" name="spot" name_original="spots" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="small maroon" value_original="small maroon" />
      </biological_entity>
      <biological_entity id="o17544" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bluish" value_original="bluish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17545" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bluish" value_original="bluish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17546" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>banner length (0.7–) 0.8–1 times wings, lobe base without folds;</text>
      <biological_entity id="o17547" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17548" name="banner" name_original="banner" src="d0_s10" type="structure">
        <character constraint="wing" constraintid="o17549" is_modifier="false" name="length" src="d0_s10" value="(0.7-)0.8-1 times wings" value_original="(0.7-)0.8-1 times wings" />
      </biological_entity>
      <biological_entity id="o17549" name="wing" name_original="wings" src="d0_s10" type="structure" />
      <biological_entity constraint="lobe" id="o17550" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o17551" name="fold" name_original="folds" src="d0_s10" type="structure" />
      <relation from="o17550" id="r1362" name="without" negation="false" src="d0_s10" to="o17551" />
    </statement>
    <statement id="d0_s11">
      <text>wings widely obovate, notched less than 0.1 times whole length;</text>
      <biological_entity id="o17552" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17553" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="notched" value_original="notched" />
        <character is_modifier="false" name="length" src="d0_s11" value="0-0.1 times whole length" value_original="0-0.1 times whole length" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>throat angled to tube, longer than diam., pouch arched, slightly expanded;</text>
      <biological_entity id="o17554" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17555" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character constraint="to tube" constraintid="o17556" is_modifier="false" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s12" value="longer than diam" value_original="longer than diam" />
      </biological_entity>
      <biological_entity id="o17556" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <biological_entity id="o17557" name="pouch" name_original="pouch" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s12" value="arched" value_original="arched" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s12" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens: abaxial filaments glabrous or sparsely hairy at base, adaxials hairy, basal spur 0.</text>
      <biological_entity id="o17558" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity constraint="abaxial" id="o17559" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o17560" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17560" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o17561" name="adaxial" name_original="adaxials" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17562" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules without red blotches.</text>
      <biological_entity id="o17563" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2–4, oblong to oval, 2–3 mm, margins thickened, inrolled.</text>
      <biological_entity id="o17564" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="4" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="oval" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 14.</text>
      <biological_entity id="o17565" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s15" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17566" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ark., Ill., Ind., Iowa, Kans., Ky., Mich., Mo., N.Y., Ohio, Okla., Pa., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>