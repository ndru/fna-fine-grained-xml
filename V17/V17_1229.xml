<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">678</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">CORDYLANTHUS</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">wrightii</taxon_name>
    <taxon_name authority="(Pennell) T. I. Chuang &amp; Heckard" date="1986" rank="subspecies">tenuifolius</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>10: 86. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus cordylanthus;species wrightii;subspecies tenuifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cordylanthus</taxon_name>
    <taxon_name authority="Pennell" date="1940" rank="species">tenuifolius</taxon_name>
    <place_of_publication>
      <publication_title>Notul. Nat. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>43: 9. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cordylanthus;species tenuifolius</taxon_hierarchy>
  </taxon_identification>
  <number>13c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems scabrous.</text>
      <biological_entity id="o16019" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s0" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3–7-lobed.</text>
      <biological_entity id="o16020" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="3-7-lobed" value_original="3-7-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikes 5–12-flowered, 25–30 mm;</text>
      <biological_entity id="o16021" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-12-flowered" value_original="5-12-flowered" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts 4–10.</text>
      <biological_entity id="o16022" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels: bracteoles 20–25 mm, margins toothed distally.</text>
      <biological_entity id="o16023" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o16024" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16025" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: corolla pale-yellow with purple markings, 20–25 mm. 2n = 26.</text>
      <biological_entity id="o16026" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16027" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character constraint="with markings" constraintid="o16028" is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16028" name="marking" name_original="markings" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16029" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In areas where their distributions overlap, subspp. tenuifolius and wrightii intergrade in the shape of the bracteoles and overall indument.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky sites, pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>