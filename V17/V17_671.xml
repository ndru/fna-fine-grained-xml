<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in J. Lindley" date="1836" rank="genus">SOPHRONANTHE</taxon_name>
    <taxon_name authority="(Michaux) Small" date="1903" rank="species">pilosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1067, 1338. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus sophronanthe;species pilosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gratiola</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">pilosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 7. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gratiola;species pilosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pilosa</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="variety">epilis</taxon_name>
    <taxon_hierarchy>genus g.;species pilosa;variety epilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tragiola</taxon_name>
    <taxon_name authority="(Michaux) Small &amp; Pennell" date="unknown" rank="species">pilosa</taxon_name>
    <taxon_hierarchy>genus tragiola;species pilosa</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Shaggy hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 20–65 cm, villous, rarely glabrous.</text>
      <biological_entity id="o13121" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade lanceolate to ovate, 8–20 × 4–13 mm, base clasping, rounded to subcordate, margins entire or serrate, not or slightly revolute, apex rounded to obtuse or acute, surfaces villous, rarely glabrous, papillate, distinctly punctate abaxially.</text>
      <biological_entity id="o13122" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13123" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13124" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="subcordate" />
      </biological_entity>
      <biological_entity id="o13125" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="not; slightly" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o13126" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="obtuse or acute" />
      </biological_entity>
      <biological_entity id="o13127" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s1" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="distinctly; abaxially" name="coloration_or_relief" src="d0_s1" value="punctate" value_original="punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels 0–3 (–5) mm;</text>
      <biological_entity id="o13128" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracteoles 6–9 (–11) mm.</text>
      <biological_entity id="o13129" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx lobes 5–7 × 0.5–1.5 mm;</text>
      <biological_entity id="o13130" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="calyx" id="o13131" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla tubular, 5–9 mm, tube yellow, veins bluish purple, limb white.</text>
      <biological_entity id="o13132" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o13133" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13134" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13135" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="bluish purple" value_original="bluish purple" />
      </biological_entity>
      <biological_entity id="o13136" name="limb" name_original="limb" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules conic, 4–5 × 2–2.8 mm.</text>
      <biological_entity id="o13137" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds 0.2–0.4 mm. 2n = 22.</text>
      <biological_entity id="o13138" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13139" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants with glabrous stems and leaves have been named Gratiola pilosa var. epilis. The combination for that taxon in Sophronanthe has not been made.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pinelands, bogs, swamps, cypress-gum depressions, pine savannas, wet flatwoods.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pinelands" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="cypress-gum depressions" />
        <character name="habitat" value="savannas" modifier="pine" />
        <character name="habitat" value="wet flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ky., La., Md., Miss., N.J., N.C., Okla., S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>