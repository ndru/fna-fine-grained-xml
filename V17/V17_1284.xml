<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) N. S. Fraga" date="2012" rank="species">barbata</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 35. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species barbata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greene" date="1884" rank="species">barbatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 9. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species barbatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">deflexus</taxon_name>
    <taxon_hierarchy>genus m.;species deflexus</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Bearded monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, taprooted.</text>
      <biological_entity id="o4933" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched from basal nodes, 2–14.5 cm, sparsely glandular-pubescent, internodes elongate, distinct.</text>
      <biological_entity id="o4934" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from basal nodes" constraintid="o4935" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="14.5" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4935" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o4936" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal not persistent;</text>
      <biological_entity id="o4937" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4938" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0 mm;</text>
      <biological_entity id="o4939" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined or palmately 3-veined (in broader ones), linear to lanceolate, 5–15 × 0.5–2 mm, base attenuate, margins entire, apex acute to obtuse, surfaces sparsely glandular-pubescent.</text>
      <biological_entity id="o4940" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4941" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o4942" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4943" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o4944" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–30, from distal or medial to distal nodes.</text>
      <biological_entity id="o4946" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o4945" id="r436" name="from" negation="false" src="d0_s5" to="o4946" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels ascending to often spreading horizontally, (5–) 9–25 mm.</text>
      <biological_entity id="o4947" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o4945" id="r437" name="fruiting" negation="false" src="d0_s6" to="o4947" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces sometimes red-dotted on ribs, campanulate, 3–6 mm, margins distinctly toothed or lobed, glabrous, sparsely glandular-pubescent, ribs weak, lobes pronounced, erect, margins glabrous.</text>
      <biological_entity id="o4945" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="30" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending to often" value_original="ascending to often" />
        <character is_modifier="false" modifier="often; horizontally" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4948" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o4949" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="red-dotted" value_original="red-dotted" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4950" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o4951" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s7" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o4952" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o4953" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4945" id="r438" name="fruiting" negation="false" src="d0_s7" to="o4948" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas bicolored (abaxial limb yellow with red spots, adaxial maroon-purple) or yellow, bilaterally symmetric, strongly bilabiate;</text>
      <biological_entity id="o4954" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric, (5–) 8–15 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o4955" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_distance" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="distance" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o4956" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o4955" id="r439" name="exserted beyond" negation="false" src="d0_s9" to="o4956" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 6–15 mm, lateral lobes 2-fid, palate bearded.</text>
      <biological_entity id="o4957" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4958" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o4959" name="palate" name_original="palate" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o4960" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o4961" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included or equal to calyx, 3–5 mm.</text>
      <biological_entity id="o4962" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character constraint="to calyx" constraintid="o4963" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4963" name="calyx" name_original="calyx" src="d0_s13" type="structure" />
    </statement>
  </description>
  <discussion>Erythranthe barbata previously has been placed in synonymy with E. montioides but differs from it in having each corolla lobe deeply notched and a consistently bearded palate. In E. montioides, each corolla lobe is entire or shallowly notched, and the palate is glabrous or sparsely bearded. Erythranthe barbata is most abundant in Tulare County but also occurs in immediately adjacent Inyo and Kern counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas in pine forests, edges of meadows and ephemeral streams.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" constraint="in pine forests , edges of meadows and ephemeral streams" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="edges" constraint="of meadows and ephemeral streams" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="ephemeral streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>