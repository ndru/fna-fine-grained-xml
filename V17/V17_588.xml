<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="species">sepalulus</taxon_name>
    <place_of_publication>
      <publication_title>New Man. Bot. Rocky Mt.,</publication_title>
      <place_in_publication>449. 1909</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species sepalulus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">azureus</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="variety">ambiguus</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 272. 1878</place_in_publication>
      <other_info_on_pub>(as Pentstemon), not P. ambiguus Torrey 1827</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species azureus;variety ambiguus</taxon_hierarchy>
  </taxon_identification>
  <number>223.</number>
  <other_name type="common_name">Little-cup beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs.</text>
      <biological_entity id="o32578" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 40–90 cm, glabrous, glaucous.</text>
      <biological_entity id="o32580" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite, glabrous, glaucous;</text>
      <biological_entity id="o32581" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 6–14 pairs, sessile, 30–85 (–102) × 3–13 mm, blade elliptic, base tapered, margins entire, apex acute.</text>
      <biological_entity constraint="cauline" id="o32582" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="14" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="85" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="102" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="85" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32583" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o32584" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o32585" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o32586" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, cylindric, 6–30 cm, axis glabrous, verticillasters (5–) 8–14, cymes 1 (or 2) -flowered, 2 per node;</text>
      <biological_entity id="o32587" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32588" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32589" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="14" />
      </biological_entity>
      <biological_entity id="o32590" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
        <character constraint="per node" constraintid="o32591" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o32591" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts narrowly elliptic to linear, 8–45 × 1–8 mm;</text>
      <biological_entity constraint="proximal" id="o32592" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="arrangement" src="d0_s5" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glabrous.</text>
      <biological_entity id="o32593" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32594" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes obovate to ovate or elliptic, 1.8–3.2 × 1.5–2.2 mm, glabrous or minutely ciliolate distally;</text>
      <biological_entity id="o32595" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o32596" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="ovate or elliptic" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; distally" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla light lavender to violet or purple, with violet nectar guides, weakly ventricose, 22–26 (–28) mm, glabrous externally, glabrous internally, tube 7–9 mm, throat gradually inflated, 7–9 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o32597" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o32598" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="light lavender" name="coloration" src="d0_s8" to="violet or purple" />
        <character is_modifier="false" modifier="weakly" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="28" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s8" to="26" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o32599" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o32600" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32601" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o32598" id="r2474" name="with" negation="false" src="d0_s8" to="o32599" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included or longer pair exserted, filaments glabrous, pollen-sacs parallel to slightly divergent, 1.5–2 mm, distal 2/3 indehiscent, sides glabrous, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o32602" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32603" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o32604" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32605" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character char_type="range_value" from="parallel" name="arrangement" src="d0_s9" to="slightly divergent" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o32606" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32607" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o32608" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 15–16 mm, included, 0.5–0.6 mm diam., glabrous;</text>
      <biological_entity id="o32609" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32610" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s10" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 16–20 mm.</text>
      <biological_entity id="o32611" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32612" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–10 × 4.5–5.5 mm.</text>
      <biological_entity id="o32613" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s12" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon sepalulus is found in the Wasatch Mountains in Juab, Sevier, Utah, and Washington counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky to gravelly and talus slopes, Gambel oak, maple, and aspen woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky to gravelly" />
        <character name="habitat" value="rocky to talus slopes" />
        <character name="habitat" value="gambel oak" />
        <character name="habitat" value="maple" />
        <character name="habitat" value="aspen" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>