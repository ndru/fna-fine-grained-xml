<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Caespitosi</taxon_name>
    <taxon_name authority="A. Nelson" date="1899" rank="species">crandallii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 354. 1899</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section caespitosi;species crandallii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Crandall’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate, decumbent, ascending, or erect, 2–28 cm, retrorsely hairy or pubescent.</text>
      <biological_entity id="o34262" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="28" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves not leathery, glabrous, glabrate, scabrous, or pubescent, hairs pointed;</text>
      <biological_entity id="o34263" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o34264" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline (1 or) 2–4 pairs, petiolate, (2–) 4–47 × 0.5–8 mm, blade elliptic to obovate, oblanceolate, or linear, base tapered, apex acute to mucronate.</text>
      <biological_entity constraint="cauline" id="o34265" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="47" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34266" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="obovate oblanceolate or linear" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="obovate oblanceolate or linear" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="obovate oblanceolate or linear" />
      </biological_entity>
      <biological_entity id="o34267" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o34268" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses continuous, ± secund, 1–8 (–16) cm, axis retrorsely hairy, hairs pointed and, usually, sparsely glandular-pubescent, verticillasters (1 or) 2–8, cymes 1–4-flowered, 1 or 2 per node;</text>
      <biological_entity id="o34269" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="16" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34270" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o34271" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="usually; sparsely" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o34272" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity id="o34273" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-4-flowered" value_original="1-4-flowered" />
        <character name="quantity" src="d0_s3" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o34274" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts oblanceolate to linear, 10–25 × 0.5–4 mm;</text>
      <biological_entity constraint="proximal" id="o34275" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels ascending to erect, retrorsely hairy and, usually, sparsely glandular-pubescent.</text>
      <biological_entity id="o34276" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually; sparsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o34277" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually; sparsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate, 4–7 (–7.5) × 0.9–1.8 (–2.2) mm, broadly scarious-margined, apex acuminate to caudate, retrorsely puberulent, hairs pointed, and sparsely glandular-pubescent;</text>
      <biological_entity id="o34278" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o34279" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s6" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s6" value="scarious-margined" value_original="scarious-margined" />
      </biological_entity>
      <biological_entity id="o34280" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s6" to="caudate" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o34281" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla blue to violet or lavender, lined internally abaxially with violet or reddish purple nectar guides, funnelform, 14–20 mm, sparsely to densely yellow-lanate or yellow-pilose internally abaxially, tube 6–8 mm, throat gradually inflated, 6–8 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o34282" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o34283" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s7" to="violet or lavender" />
        <character constraint="with nectar guides" constraintid="o34284" is_modifier="false" modifier="internally abaxially" name="architecture" src="d0_s7" value="lined" value_original="lined" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s7" value="yellow-lanate" value_original="yellow-lanate" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s7" value="yellow-pilose" value_original="yellow-pilose" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o34284" name="guide" name_original="guides" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="violet" value_original="violet" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o34285" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34286" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens: longer pair reaching orifice, filaments glabrous, pollen-sacs opposite, navicular to subexplanate, 0.9–1.4 mm, sutures papillate;</text>
      <biological_entity id="o34287" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o34288" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <biological_entity id="o34289" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34290" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
        <character constraint="to subexplanate , 0.9-1.4 mm" is_modifier="false" name="shape" src="d0_s8" value="navicular" value_original="navicular" />
      </biological_entity>
      <biological_entity id="o34291" name="suture" name_original="sutures" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o34287" id="r2616" name="reaching" negation="false" src="d0_s8" to="o34288" />
    </statement>
    <statement id="d0_s9">
      <text>staminode 11–14 mm, reaching orifice or exserted, flattened distally, 0.3–0.4 mm diam., tip straight to recurved, distal 7–10 mm densely pilose, hairs golden yellow, to 1 mm;</text>
      <biological_entity id="o34292" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o34293" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34294" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o34295" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o34296" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o34293" id="r2617" name="reaching" negation="false" src="d0_s9" to="o34294" />
    </statement>
    <statement id="d0_s10">
      <text>style 13–15 mm.</text>
      <biological_entity id="o34297" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o34298" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 7–8 × 3.5–4.5 mm.</text>
      <biological_entity id="o34299" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon crandallii comprises five partly sympatric, sometimes intergrading varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves glabrous or sparsely puberulent, especially along margins, blades obovate to elliptic; stems prostrate or decumbent; Colorado.</description>
      <determination>18d. Penstemon crandallii var. procumbens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves glabrate or scabrous to pubescent, blades linear, oblanceolate, or obovate; stems decumbent, ascending, or erect; Colorado, New Mexico, Utah.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades obovate or oblanceolate; stems decumbent to ascending.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves glabrate or scabrous, blades oblanceolate; Colorado, Utah.</description>
      <determination>18a. Penstemon crandallii var. crandallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves ± scabrous, blades obovate or oblanceolate; Utah.</description>
      <determination>18b. Penstemon crandallii var. atratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades linear to oblanceolate; stems ascending to erect.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems retrorsely hairy or puberulent, ascending to erect; Colorado, New Mexico.</description>
      <determination>18c. Penstemon crandallii var. glabrescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems puberulent to pubescent, erect; Colorado.</description>
      <determination>18e. Penstemon crandallii var. ramaleyi</determination>
    </key_statement>
  </key>
</bio:treatment>