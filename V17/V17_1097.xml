<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">598</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="mention_page">649</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="species">lindheimeri</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2(1): 298. 1878</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species lindheimeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="(Nuttall) G. Don" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(A. Gray) Shinners" date="unknown" rank="variety">lindheimeri</taxon_name>
    <taxon_hierarchy>genus castilleja;species purpurea;variety lindheimeri</taxon_hierarchy>
  </taxon_identification>
  <number>59.</number>
  <other_name type="common_name">Lindheimer’s paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1.5–3 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o32227" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o32228" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o32227" id="r2443" name="with" negation="false" src="d0_s2" to="o32228" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several, erect or ascending, branched or unbranched, sometimes with axillary tufts of leaves, hairs spreading to ± appressed, ± matted, short, soft, eglandular.</text>
      <biological_entity id="o32229" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o32230" name="tuft" name_original="tufts" src="d0_s3" type="structure" />
      <biological_entity id="o32231" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o32232" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="more or less appressed" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s3" value="matted" value_original="matted" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o32229" id="r2444" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o32230" />
      <relation from="o32230" id="r2445" name="part_of" negation="false" src="d0_s3" to="o32231" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green to purple, linear to narrowly lanceolate, 1.3–7 cm, not fleshy, margins plane, sometimes ± wavy, involute, 3–5-lobed, apex acute to obtuse;</text>
      <biological_entity id="o32233" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o32234" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o32235" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending, linear to narrowly lanceolate, apex acute.</text>
      <biological_entity id="o32236" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o32237" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 4.5–18 × 1.5–3.5 cm;</text>
      <biological_entity id="o32238" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally pale greenish to dull greenish brown, distally orange, reddish orange, or pale orange, sometimes reddish, lanceolate to oblong, 3 (–5) -lobed;</text>
      <biological_entity id="o32239" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally pale greenish" name="coloration" src="d0_s7" to="dull greenish brown distally orange reddish orange or pale orange" />
        <character char_type="range_value" from="proximally pale greenish" name="coloration" src="d0_s7" to="dull greenish brown distally orange reddish orange or pale orange" />
        <character char_type="range_value" from="proximally pale greenish" name="coloration" src="d0_s7" to="dull greenish brown distally orange reddish orange or pale orange" />
        <character char_type="range_value" from="proximally pale greenish" name="coloration" src="d0_s7" to="dull greenish brown distally orange reddish orange or pale orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending to spreading, linear to broadly lanceolate or oblanceolate, proximal ones usually arising below mid length, rarely at or above mid length, apex obtuse to acute.</text>
      <biological_entity id="o32240" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="broadly lanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o32241" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character constraint="below mid lobes" constraintid="o32242" is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o32242" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s8" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o32243" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" modifier="rarely" name="length" src="d0_s8" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces proximally greenish or pale, distally colored as bracts, 23–33 mm;</text>
      <biological_entity id="o32244" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character constraint="as bracts" constraintid="o32245" is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="33" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32245" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 10–15 mm, 40–50% of calyx length, deeper than laterals, lateral 7–11 mm, 20–35% of calyx length;</text>
      <biological_entity constraint="calyx" id="o32246" name="cleft" name_original="clefts" src="d0_s10" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o32248" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32247" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o32248" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o32246" id="r2446" modifier="40-50%" name="part_of" negation="false" src="d0_s10" to="o32247" />
    </statement>
    <statement id="d0_s11">
      <text>lobes oblong to broadly linear, central lobe apex obtuse to rounded, lateral ones acute to rounded.</text>
      <biological_entity id="o32249" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="broadly linear" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o32250" name="apex" name_original="apex" src="d0_s11" type="structure" constraint_original="central lobe">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="rounded" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas ± curved, 30–40 mm;</text>
      <biological_entity id="o32251" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s12" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 20–27 mm;</text>
      <biological_entity id="o32252" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak, sometimes teeth of abaxial lip, exserted;</text>
      <biological_entity id="o32253" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity id="o32254" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o32255" name="lip" name_original="lip" src="d0_s14" type="structure" />
      <relation from="o32254" id="r2447" name="part_of" negation="false" src="d0_s14" to="o32255" />
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially green to yellowish, 8–15 mm;</text>
      <biological_entity id="o32256" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s15" to="yellowish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip green, reduced, 2–3.5 mm, 20–25% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o32257" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s16" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32258" name="beak" name_original="beak" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>teeth prominent, petaloid, spreading to erect, colored as in distal portion of bracts, 0.5–3 mm.</text>
      <biological_entity id="o32259" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s17" to="erect" />
        <character constraint="in distal portion" constraintid="o32260" is_modifier="false" name="coloration" src="d0_s17" value="colored" value_original="colored" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32260" name="portion" name_original="portion" src="d0_s17" type="structure" />
      <biological_entity id="o32261" name="bract" name_original="bracts" src="d0_s17" type="structure" />
      <relation from="o32260" id="r2448" name="part_of" negation="false" src="d0_s17" to="o32261" />
    </statement>
  </description>
  <discussion>Castilleja lindheimeri is endemic to the Edwards Plateau region. Unlike its close relatives, C. citrina and C. purpurea, most plants of C. lindheimeri have orange to reddish orange inflorescences, with smaller numbers varying to red. The leaves are also often less divided than in either C. citrina or C. purpurea.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, ridges, grasslands, pastures, open forests, roadsides, sometimes over limestone or granite.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>