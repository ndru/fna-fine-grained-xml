<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">oliganthus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 172. 1913</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species oliganthus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Crosswhite" date="unknown" rank="species">pseudoparvus</taxon_name>
    <taxon_hierarchy>genus penstemon;species pseudoparvus</taxon_hierarchy>
  </taxon_identification>
  <number>176.</number>
  <other_name type="common_name">Apache beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o19608" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (6–) 10–45 cm, retrorsely hairy, also glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o19609" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, proximals usually retrorsely hairy proximally, especially along midveins and margins, mostly glabrate distally, distals retrorsely hairy and, sometimes, sparsely glandular-pubescent, sometimes ± glaucescent;</text>
      <biological_entity id="o19610" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o19611" name="proximal" name_original="proximals" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually retrorsely; proximally" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="mostly; distally" name="pubescence" notes="" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o19612" name="midvein" name_original="midveins" src="d0_s2" type="structure" />
      <biological_entity id="o19613" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <relation from="o19611" id="r1506" modifier="especially" name="along" negation="false" src="d0_s2" to="o19612" />
      <relation from="o19611" id="r1507" modifier="especially" name="along" negation="false" src="d0_s2" to="o19613" />
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 15–75 × (2–) 5–19 mm, blade spatulate to oblanceolate or elliptic, base tapered, margins entire, apex rounded to obtuse or acute, sometimes mucronate;</text>
      <biological_entity id="o19614" name="distal" name_original="distals" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes more or less" name="coating" src="d0_s2" value="glaucescent" value_original="glaucescent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o19615" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o19616" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate or elliptic" />
      </biological_entity>
      <biological_entity id="o19617" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o19618" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19619" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse or acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 2–5 pairs, sessile, 11–70 × (1–) 4–15 mm, blade of proximal leaves oblanceolate to lanceolate, distal leaves lanceolate, base tapered to clasping, margins entire, rarely ± denticulate distally, apex acute to acuminate.</text>
      <biological_entity constraint="cauline" id="o19620" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19621" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19622" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o19623" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o19624" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o19625" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely more or less; distally" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o19626" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <relation from="o19621" id="r1508" name="part_of" negation="false" src="d0_s4" to="o19622" />
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, secund, (1–) 3–24 (–30) cm, axis glandular-pubescent, verticillasters (2 or) 3–7, cymes 1–4-flowered, (1 or) 2 per node;</text>
      <biological_entity id="o19627" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="24" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="24" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19628" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o19629" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o19630" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-4-flowered" value_original="1-4-flowered" />
        <character constraint="per node" constraintid="o19631" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o19631" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate to linear, 7–38 (–60) × 1–7 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o19632" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="38" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="38" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19633" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels erect, glandular-pubescent.</text>
      <biological_entity id="o19634" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o19635" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes lanceolate, 2.8–6 × 0.9–2 mm, glandular-pubescent;</text>
      <biological_entity id="o19636" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o19637" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla blue to violet-blue, with reddish purple nectar guides, tubular to tubular-funnelform, 11–20 mm, glandular-pubescent externally, sparsely to moderately white-villous to white-lanate internally abaxially, tube 5–7 mm, throat slightly inflated, 3–6 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o19638" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19639" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="violet-blue" />
        <character char_type="range_value" from="tubular" name="shape" notes="" src="d0_s9" to="tubular-funnelform" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character char_type="range_value" from="white-villous" modifier="sparsely to moderately; moderately; internally abaxially; abaxially" name="pubescence" src="d0_s9" to="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o19640" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o19641" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19642" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o19639" id="r1509" name="with" negation="false" src="d0_s9" to="o19640" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, 0.8–1.2 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o19643" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19644" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o19645" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o19646" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o19647" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19648" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 10–12 mm, included or slightly exserted, 0.3–0.4 mm diam., tip straight to slightly recurved, distal 3–6 mm densely pilose, hairs golden, to 2 mm;</text>
      <biological_entity id="o19649" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19650" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19651" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19652" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o19653" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden" value_original="golden" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 9–13 mm.</text>
      <biological_entity id="o19654" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19655" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–7 × 3–4.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o19656" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19657" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon oliganthus is known from the Mogollon Rim (Coconino County) and White Mountains of east-central Arizona (Apache and Greenlee counties) and the Jemez and San Mateo mountains of northwestern New Mexico (McKinley, Sandoval, and Valencia counties). Crosswhite described P. pseudoparvus from five specimens from the Magdalena and San Mateo mountains in Socorro County, New Mexico, and more than 24 specimens of P. oliganthus from Arizona and New Mexico. He separated the two species based on stem indument (obscurely puberulent in P. oliganthus versus obviously puberulent in P. pseudoparvus), and flower orientation and shape (horizontal or more usually drooping and little inflated in P. oliganthus versus ascending and not inflated in P. pseudoparvus). More than 60 collections referable to these species were examined for this treatment, including the types, specimens annotated by Crosswhite, and 20 specimens from the San Mateo and Magdalena mountains in Socorro County; the two taxa appear to be indistinguishable.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane meadows, ciénagas, clearings in pine and spruce-fir forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane meadows" />
        <character name="habitat" value="ciénagas" />
        <character name="habitat" value="pine" modifier="clearings in" />
        <character name="habitat" value="spruce-fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–3500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>