<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">608</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Nuttall) G. Don" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species angustifolia;variety angustifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: hairs sparse to moderately dense, spreading to retrorse, long, soft to stiff, usually mixed with short-glandular ones.</text>
      <biological_entity id="o22686" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o22687" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s0" to="moderately dense" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="retrorse" />
        <character is_modifier="false" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character constraint="with short-glandular ones" constraintid="o22688" is_modifier="false" modifier="usually" name="arrangement" src="d0_s0" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="short-glandular" id="o22688" name="one" name_original="ones" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Bracts distally pink or pink-purple, sometimes pale orange, yellowish, or white, 3–5-lobed.</text>
      <biological_entity id="o22689" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s1" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="pink-purple" value_original="pink-purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s1" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Calyces 13–20 mm;</text>
      <biological_entity id="o22690" name="calyx" name_original="calyces" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>abaxial clefts 3–6 mm, adaxial 5–8 mm.</text>
      <biological_entity constraint="abaxial" id="o22691" name="cleft" name_original="clefts" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22692" name="cleft" name_original="clefts" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Corollas 18–25 mm.</text>
      <biological_entity id="o22693" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety angustifolia has pink to purple inflorescences over most of its range from southeastern Oregon and southern Idaho to southwestern Montana and northwestern Wyoming. At the eastern edge of its distribution, the colors vary more, possibly representing hybridization or contact with the more eastern var. dubia, which usually has yellow inflorescences. Plants of the western slope of the Big Horn Mountains, Wyoming, are morphologically intermediate between these two varieties. In the same area, var. angustifolia occasionally hybridizes with Castilleja flava var. flava.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sagebrush slopes and flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="dry sagebrush" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Oreg., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>