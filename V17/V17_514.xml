<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">pallidus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1060, 1337. 1903</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species pallidus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>178.</number>
  <other_name type="common_name">Pale beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o16100" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 25–55 (–65) cm, retrorsely hairy and glandular-villous proximally, retrorsely hairy and glandular-pubescent distally, glandular-hairs sometimes sparse, not glaucous.</text>
      <biological_entity id="o16101" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="65" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="55" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o16102" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="count_or_density" src="d0_s1" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal often withering by anthesis, not leathery, sparsely to densely pubescent, sometimes also with scattered glandular-hairs, abaxially, sparsely pubescent adaxially;</text>
      <biological_entity id="o16103" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o16105" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o16104" id="r1257" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o16105" />
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 20–120 (–180) × 5–35 (–40) mm, blade spatulate to obovate or ovate, base tapered, margins subentire or ± serrate or dentate, apex rounded to obtuse or acute;</text>
      <biological_entity constraint="basal" id="o16104" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="often" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="abaxially; sparsely; adaxially" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o16106" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o16107" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="obovate or ovate" />
      </biological_entity>
      <biological_entity id="o16108" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o16109" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" value_original="subentire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16110" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 4–8 pairs, sessile, 22–100 × 4–24 mm, blade ovate to lanceolate, base truncate to clasping, margins subentire or ± serrate or dentate, apex acute to acuminate.</text>
      <biological_entity constraint="cauline" id="o16111" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="22" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16112" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o16113" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="architecture" src="d0_s4" to="clasping" />
      </biological_entity>
      <biological_entity id="o16114" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16115" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, conic, 5–26 (–30) cm, axis sparsely to densely glandular-pubescent, verticillasters 3–8, cymes 2–8 (–16) -flowered, 2 per node;</text>
      <biological_entity id="o16116" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="26" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16117" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o16118" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
      <biological_entity id="o16119" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-8(-16)-flowered" value_original="2-8(-16)-flowered" />
        <character constraint="per node" constraintid="o16120" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16120" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 7–72 × 2–22 mm, margins entire or ± dentate;</text>
      <biological_entity constraint="proximal" id="o16121" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="72" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16122" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels ascending, sparsely to densely glandular-pubescent.</text>
      <biological_entity id="o16123" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o16124" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate to lanceolate, (2–) 3–5 × 1–3 mm, sparsely glandular-pubescent;</text>
      <biological_entity id="o16125" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o16126" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla white, sometimes tinged lavender or blue, with reddish purple nectar guides, tubular to tubular-funnelform, 16–22 mm, glandular-pubescent externally, moderately whitish or yellowish lanate internally abaxially, tube 4–6 mm, throat slightly inflated, 4–7 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o16127" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16128" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="tinged lavender" value_original="tinged lavender" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character char_type="range_value" from="tubular" name="shape" notes="" src="d0_s9" to="tubular-funnelform" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s9" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="moderately" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o16129" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o16130" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16131" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o16128" id="r1258" name="with" negation="false" src="d0_s9" to="o16129" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, (0.8–) 1–1.2 mm, dehiscing completely, connective splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o16132" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16133" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o16134" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_distance" src="d0_s10" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o16135" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o16136" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16137" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 10–12 mm, exserted, 0.2–0.3 mm diam., tip straight to slightly recurved, distal 8–9 mm moderately to densely villous, hairs yellow or golden yellow, to 1.5 mm;</text>
      <biological_entity id="o16138" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16139" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16140" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16141" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o16142" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 12–14 mm.</text>
      <biological_entity id="o16143" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16144" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–7 × 3–5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o16145" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16146" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon pallidus is concentrated in the Central Lowlands and Ozark Plateau of the central United States. F. W. Pennell (1935) believed it to be native there, having spread eastward as a result of human activities, as in Ontario.</discussion>
  <discussion>Penstemon brevisepalus, which has been synonymized with P. pallidus by many authors, is treated here as a distinct species. Characters distinguishing the species are discussed under 147. P. brevisepalus.</discussion>
  <discussion>Some specimens from north-central Arkansas and north-central Missouri are morphologically intermediate between Penstemon pallidus and P. arkansanus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tallgrass prairie, limestone and sandstone glades, barrens, rocky oak-hickory woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tallgrass prairie" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone glades" />
        <character name="habitat" value="rocky oak-hickory woodlands" modifier="barrens" />
        <character name="habitat" value="barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>