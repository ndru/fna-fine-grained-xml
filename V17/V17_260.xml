<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1935" rank="section">Baccharifolii</taxon_name>
    <taxon_name authority="Hooker" date="1852" rank="species">baccharifolius</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>78: plate 4627. 1852</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section baccharifolii;species baccharifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Baccharis-leaf beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 12–50 cm.</text>
      <biological_entity id="o5155" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: cauline 5–15 pairs (distal ones much-reduced), 5–30 (–46) × 4–15 mm, blade base tapered to acute to cuneate, margins sharply dentate, especially distally, or entire, apex obtuse to acute.</text>
      <biological_entity id="o5156" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="cauline" id="o5157" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="15" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="46" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="blade" id="o5158" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s1" to="acute" />
      </biological_entity>
      <biological_entity id="o5159" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="especially distally; distally" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5160" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Thyrses (2–) 5–15 (–40) cm, verticillasters (2 or) 3–7 (–12), cymes 1-flowered or 2 (or 3) -flowered;</text>
      <biological_entity id="o5161" name="thyrse" name_original="thyrses" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5162" name="verticillaster" name_original="verticillasters" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="12" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o5163" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal bracts elliptic to lanceolate, 2–5 × 1–3 mm;</text>
      <biological_entity constraint="proximal" id="o5164" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o5165" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o5166" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes elliptic to ovate, 3.5–5 × 1.5–2.2 mm;</text>
      <biological_entity id="o5167" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o5168" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla unlined internally, 22–27 mm, glandular-pubescent internally, tube 4–5 mm, throat 6–7 mm diam.;</text>
      <biological_entity id="o5169" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5170" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" modifier="internally" name="some_measurement" src="d0_s6" to="27" to_unit="mm" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o5171" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5172" name="throat" name_original="throat" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pollen-sacs 1.2–1.5 mm;</text>
      <biological_entity id="o5173" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5174" name="pollen-sac" name_original="pollen-sacs" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminode 16–19 mm;</text>
      <biological_entity id="o5175" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5176" name="staminode" name_original="staminode" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 18–25 mm.</text>
      <biological_entity id="o5177" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5178" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 7–10 × 5–7 mm.</text>
      <biological_entity id="o5179" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon baccharifolius is known from the Edwards Plateau and eastern Trans-Pecos of Texas, from Bandera and Medina counties west to extreme eastern Presidio County. It also occurs in the mountains of northern Coahuila, Mexico. I. Méndez Larios and J. L. Villaseñor Ríos (2001) listed it for the state of San Luis Potosí, Mexico.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs, ledges, hills.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="hills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, San Luis Potosí).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>