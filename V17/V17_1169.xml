<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">657</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="mention_page">579</other_info_on_meta>
    <other_info_on_meta type="mention_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">598</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">sessiliflora</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 738. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species sessiliflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>106.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–4 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a branching, woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o30740" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o30741" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o30740" id="r2327" name="with" negation="false" src="d0_s2" to="o30741" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, ascending to erect, often decumbent at base, unbranched, sometimes branched, hairs spreading, sometimes matted, short to medium length, ± soft, eglandular, often with a layer of minute-glandular hairs, sometimes woolly.</text>
      <biological_entity id="o30742" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character constraint="at base" constraintid="o30743" is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o30743" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o30744" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s3" value="matted" value_original="matted" />
        <character is_modifier="false" name="length" src="d0_s3" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s3" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o30745" name="layer" name_original="layer" src="d0_s3" type="structure" />
      <biological_entity constraint="minute-glandular" id="o30746" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o30744" id="r2328" modifier="often" name="with" negation="false" src="d0_s3" to="o30745" />
      <relation from="o30745" id="r2329" name="part_of" negation="false" src="d0_s3" to="o30746" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green to purple, or grayish with dust and hairs, linear to narrowly lanceolate, (1–) 2–5 (–6) cm, not fleshy, margins plane, involute, (0–) 3–5-lobed, apex acuminate to acute;</text>
      <biological_entity id="o30747" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple or grayish" />
        <character char_type="range_value" constraint="with hairs" constraintid="o30748" from="green" name="coloration" src="d0_s4" to="purple or grayish" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30748" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o30749" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3-5-lobed" value_original="(0-)3-5-lobed" />
      </biological_entity>
      <biological_entity id="o30750" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes divergent, spreading, linear, apex acute.</text>
      <biological_entity id="o30751" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o30752" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–18 × 2.5–6.5 cm;</text>
      <biological_entity id="o30753" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s6" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts green to purplish throughout, sometimes reddish-brown, pink, or lavender throughout, or distally white or pale-yellow, sometimes distally dull pink, pink, salmon, orangish, pale pink-orange, buff, or cream, lanceolate, similar to distal leaves, 3 (–5) -lobed;</text>
      <biological_entity id="o30754" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" modifier="throughout" name="coloration" src="d0_s7" to="purplish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes distally" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="salmon" value_original="salmon" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orangish" value_original="orangish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale pink-orange" value_original="pale pink-orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="buff" value_original="buff" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="buff" value_original="buff" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30755" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o30754" id="r2330" name="to" negation="false" src="d0_s7" to="o30755" />
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading, linear-lanceolate, long, arising at or below mid length, apex acute to acuminate, sometimes obtuse.</text>
      <biological_entity id="o30756" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character constraint="at or below mid lobes, apex" constraintid="o30757, o30758" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="mid" id="o30757" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o30758" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, sometimes proximally white, 20–40 mm;</text>
      <biological_entity id="o30759" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o30760" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="sometimes proximally" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30760" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 12–20 mm, 40–60% of calyx length, deeper than laterals, lateral 5–15 mm, 10–25% of calyx length;</text>
      <biological_entity constraint="calyx" id="o30761" name="cleft" name_original="clefts" src="d0_s10" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character constraint="than laterals" constraintid="o30763" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30762" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o30763" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o30761" id="r2331" modifier="40-60%" name="part_of" negation="false" src="d0_s10" to="o30762" />
    </statement>
    <statement id="d0_s11">
      <text>lobes linear, apex acute to acuminate.</text>
      <biological_entity id="o30764" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o30765" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas strongly curved distally, 35–55 mm;</text>
      <biological_entity id="o30766" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly; distally" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s12" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 24–45 mm;</text>
      <biological_entity id="o30767" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s13" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>abaxial lip, beak, and distal portion of tube exserted;</text>
      <biological_entity constraint="abaxial" id="o30768" name="lip" name_original="lip" src="d0_s14" type="structure" />
      <biological_entity id="o30769" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity constraint="distal" id="o30770" name="portion" name_original="portion" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o30771" name="tube" name_original="tube" src="d0_s14" type="structure" />
      <relation from="o30770" id="r2332" name="part_of" negation="false" src="d0_s14" to="o30771" />
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially green, yellow, pinkish, purplish, or whitish, 9–15 mm;</text>
      <biological_entity id="o30772" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip green, pale green, or purple, protruding, shelflike, 4–8 mm, 50–70% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o30773" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="protruding" value_original="protruding" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="shelflike" value_original="shelflike" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30774" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o30773" id="r2333" modifier="50-70%" name="as long as" negation="false" src="d0_s16" to="o30774" />
    </statement>
    <statement id="d0_s17">
      <text>teeth spreading, white, pale-yellow, pink, or purple, 3–4 mm. 2n = 24.</text>
      <biological_entity id="o30775" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purple" value_original="purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30776" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja sessiliflora ranges across the Great Plains from southern Canada to northern Mexico, where it is apparently rare. In Texas and northern Mexico, its range overlaps with the similar C. mexicana. Most populations of C. sessiliflora, especially north of Texas, have white to pale yellow inflorescences; in southwestern Texas they are more variable in color, with pink-purple plants often predominating locally. Those plants with pink-purple inflorescences were named forma purpurina by F. W. Pennell. In the limestone deserts of southern New Mexico and southeastern Arizona, the inflorescences are often a pale pink-orange, but these are intermingled with more typical greenish white plants. Occasional hybrids between C. angustifolia var. dubia and C. sessiliflora are known from northeastern Wyoming.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry mixed grass and shortgrass prairies, prairie sandhills, sandsage plains, sand prairies, rocky or sandy slopes, bluffs, open forests, or desert scrub, limestone, sandstone, gypsum, granite, other bedrock types.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry mixed grass and shortgrass prairies" />
        <character name="habitat" value="prairie sandhills" />
        <character name="habitat" value="sandsage plains" />
        <character name="habitat" value="sand prairies" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="desert scrub" modifier="or" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="gypsum" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="other bedrock types" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Ariz., Colo., Ill., Iowa, Kans., Minn., Mo., Mont., Nebr., N.Mex., N.Dak., Okla., S.Dak., Tex., Wis., Wyo.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>