<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">eriantherus</taxon_name>
    <taxon_name authority="(Piper) A. Nelson" date="1912" rank="variety">whitedii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>54: 148. 1912</place_in_publication>
      <other_info_on_pub>(as Pentstemon erianthera)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species eriantherus;variety whitedii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Piper" date="1896" rank="species">whitedii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>22: 490. 1896</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species whitedii</taxon_hierarchy>
  </taxon_identification>
  <number>61e.</number>
  <other_name type="common_name">Whited’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrate or retrorsely hairy, rarely glandular-villous, proximally, glandular-villous distally.</text>
      <biological_entity id="o37428" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" modifier="proximally; distally" name="pubescence" src="d0_s0" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves glabrous or glabrate abaxially, glabrate or villous to glandular-villous adaxially.</text>
      <biological_entity id="o37429" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="glabrate or villous" modifier="adaxially" name="pubescence" src="d0_s1" to="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: corolla lavender to violet or blue, ventricose-ampliate, 17–22 mm, not constricted at orifice;</text>
      <biological_entity id="o37430" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o37431" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s2" to="violet or blue" />
        <character is_modifier="false" name="size" src="d0_s2" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s2" to="22" to_unit="mm" />
        <character constraint="at orifice" constraintid="o37432" is_modifier="false" modifier="not" name="size" src="d0_s2" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o37432" name="orifice" name_original="orifice" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>pollen-sacs navicular, 1.3–1.6 mm;</text>
      <biological_entity id="o37433" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o37434" name="pollen-sac" name_original="pollen-sacs" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="distance" src="d0_s3" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>staminode: distal 3–6 mm moderately lanate, hairs yellow, to 2 mm;</text>
      <biological_entity id="o37435" name="staminode" name_original="staminode" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o37436" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>style 9–13 mm.</text>
      <biological_entity id="o37437" name="staminode" name_original="staminode" src="d0_s5" type="structure" />
      <biological_entity id="o37438" name="style" name_original="style" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety whitedii is known from the foothills of the Cascade Range and on the plains and in valleys in the Columbia Basin. The variety has been documented in Chelan, Douglas, Franklin, Kittitas, Klickitat, and Lincoln counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky, bitterbrush and rabbitbrush shrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>