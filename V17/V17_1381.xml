<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="G. L. Nesom" date="2013" rank="species">cusickioides</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2013-65: 6, figs. 3, 4. 2013</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species cusickioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o27173" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to erect-ascending, (10–) 30–240 (–350) mm, distal internodes 1–5 mm, densely glandular-villous.</text>
      <biological_entity id="o27174" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="erect-ascending" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="240" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="350" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s1" to="240" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27175" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline or usually cauline, relatively even-sized or gradually larger distally;</text>
      <biological_entity id="o27176" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s2" value="equal-sized" value_original="even-sized" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s2" value="larger" value_original="larger" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent, proximal base short petiolelike, 1–5 mm;</text>
      <biological_entity id="o27177" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27178" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s3" value="petiole-like" value_original="petiolelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to broadly elliptic-ovate, (10–) 15–25 (–35) × 4–16 mm, margins entire, plane, apex abruptly acuminate, surfaces densely glandular-villous.</text>
      <biological_entity id="o27179" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly elliptic-ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27180" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o27181" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o27182" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–1.5 mm in fruit.</text>
      <biological_entity id="o27183" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o27184" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27184" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o27185" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o27186" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o27186" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o27187" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o27188" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o27187" id="r2093" name="on" negation="false" src="d0_s6" to="o27188" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, not inflated in fruit, 7–12 mm, glabrous or minutely stipitate-glandular, lobes unequal, apex linear-acuminate, sharp-pointed, ribs green distally, intercostal areas whitish.</text>
      <biological_entity id="o27189" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o27190" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o27191" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o27190" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o27191" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o27192" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o27193" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-acuminate" value_original="linear-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
      <biological_entity id="o27194" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o27195" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas magenta or rose-purple, tube yellow, throat usually yellow, throat and distal tube red-spotted on floor, palate ridges yellow, tube-throat 13–16 (–19) mm, limb 14–26 mm diam., bilabiate.</text>
      <biological_entity id="o27196" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose-purple" value_original="rose-purple" />
      </biological_entity>
      <biological_entity id="o27197" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o27198" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o27199" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character constraint="on floor" constraintid="o27201" is_modifier="false" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27200" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character constraint="on floor" constraintid="o27201" is_modifier="false" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
      </biological_entity>
      <biological_entity id="o27201" name="floor" name_original="floor" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o27202" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o27203" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s8" to="19" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="distance" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27204" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="diameter" src="d0_s8" to="26" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous or sparsely hirsutulous.</text>
      <biological_entity id="o27205" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles puberulent, at least on distal 1/2.</text>
      <biological_entity id="o27206" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27207" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <relation from="o27206" id="r2094" modifier="at-least" name="on" negation="false" src="d0_s10" to="o27207" />
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes subequal (herkogamous).</text>
      <biological_entity id="o27208" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o27209" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 10–17 mm.</text>
      <biological_entity id="o27210" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus cusickioides occurs in western Idaho, eastern Oregon, Klickitat County, Washington, and apparently in a disjunct population system in Modoc County, California, on the east side of the Warner Mountains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lava formations, steep slopes, roadsides, volcanic gravels, scree, ash.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lava formations" />
        <character name="habitat" value="steep slopes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="volcanic gravels" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(400–)600–1500(–2000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>