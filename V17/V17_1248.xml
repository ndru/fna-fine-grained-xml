<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">685</other_info_on_meta>
    <other_info_on_meta type="mention_page">592</other_info_on_meta>
    <other_info_on_meta type="mention_page">680</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="genus">TRIPHYSARIA</taxon_name>
    <taxon_name authority="(Bentham) T. I. Chuang &amp; Heckard" date="1991" rank="species">eriantha</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>16: 660. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus triphysaria;species eriantha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="Bentham" date="1835" rank="species">erianthus</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>12. 1835</place_in_publication>
      <other_info_on_pub>(as eriantha)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species erianthus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Johnny-tuck</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or with 1–10 ascending branches proximally, 2–37 cm, glabrous proximally, puberulent to glandular-puberulent distally.</text>
      <biological_entity id="o4263" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="with 1-10 ascending branches" value_original="with 1-10 ascending branches" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="37" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="puberulent" modifier="distally" name="pubescence" src="d0_s0" to="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o4264" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="10" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o4263" id="r391" name="with" negation="false" src="d0_s0" to="o4264" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent to glandular-puberulent;</text>
      <biological_entity id="o4265" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal cauline: blade linear, 5–25 mm;</text>
      <biological_entity constraint="cauline proximal" id="o4266" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4267" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline: blade ± ovate or obovate, 8–50 mm, base sessile, margins pinnatifid, rarely bipinnatifid, lateral lobes 2–11.</text>
      <biological_entity constraint="cauline" id="o4268" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4269" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4270" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o4271" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="bipinnatifid" value_original="bipinnatifid" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4272" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelike racemes interrupted, dense distally, 1–24 cm;</text>
      <biological_entity id="o4273" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="distally" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="24" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle absent;</text>
      <biological_entity id="o4274" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts pinnatifid, rarely bipinnatifid, ± ovate, 2–30 mm, lateral lobes 2–8.</text>
      <biological_entity id="o4275" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s6" value="bipinnatifid" value_original="bipinnatifid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4276" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.3–0.5 mm, glabrous.</text>
      <biological_entity id="o4277" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 6–13 mm, puberulent to glandular-puberulent, tube 4–7 mm, lobes triangular to narrowly lanceolate, 1–4 × 1–1.5 mm;</text>
      <biological_entity id="o4278" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4279" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s8" to="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o4280" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4281" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla yellow, rarely yellow-and-white, or white, fading to rose-pink, 10–25 mm, densely hairy, beak dark purple, not hooked, abaxial lobes spreading, 2–5 mm, throat abruptly indented, forming a fold under abaxial corolla lip, adaxial lobes projecting;</text>
      <biological_entity id="o4282" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4283" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s9" to="rose-pink" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s9" to="rose-pink" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s9" to="rose-pink" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4284" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4285" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4286" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="indented" value_original="indented" />
      </biological_entity>
      <biological_entity id="o4287" name="fold" name_original="fold" src="d0_s9" type="structure" />
      <biological_entity constraint="corolla abaxial" id="o4288" name="lip" name_original="lip" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o4289" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o4286" id="r392" name="forming a" negation="false" src="d0_s9" to="o4287" />
      <relation from="o4286" id="r393" name="under" negation="false" src="d0_s9" to="o4288" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sac yellow, 1.5–2 mm, glabrous, dehiscing longitudinally;</text>
      <biological_entity id="o4290" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4291" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o4292" name="pollen-sac" name_original="pollen-sac" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 12–22 mm, glabrous;</text>
      <biological_entity id="o4293" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4294" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma capitate.</text>
      <biological_entity id="o4295" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4296" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–8 × 2.5–4.5 mm, glabrous.</text>
      <biological_entity id="o4297" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 30–100, ovoid, 0.5–1 mm.</text>
      <biological_entity id="o4298" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s14" to="100" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas yellow, rarely yellow and white.</description>
      <determination>1a. Triphysaria eriantha subsp. eriantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas white, fading to rose pink.</description>
      <determination>1b. Triphysaria eriantha subsp. rosea</determination>
    </key_statement>
  </key>
</bio:treatment>