<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="Lindley" date="1827" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Reg.</publication_title>
      <place_in_publication>13: plate 1082. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Collinsia</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">grandiflora</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">pusilla</taxon_name>
    <taxon_hierarchy>genus collinsia;species grandiflora;variety pusilla</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Small-flowered blue-eyed Mary</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 3–40 cm.</text>
      <biological_entity id="o5554" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o5555" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ± linear-lanceolate, obovate, or narrowly elliptic, margins subentire.</text>
      <biological_entity id="o5556" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o5557" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences glabrous or sparsely and finely glandular;</text>
      <biological_entity id="o5558" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s3" value="sparsely" value_original="sparsely" />
        <character is_modifier="false" modifier="finely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal nodes 1-flowered, distals 3–5 (–7) -flowered;</text>
      <biological_entity constraint="proximal" id="o5559" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
      <biological_entity id="o5560" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-5(-7)-flowered" value_original="3-5(-7)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers not crowded proximally, sometimes crowded distally;</text>
      <biological_entity id="o5561" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="sometimes; distally" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distalmost bracts linear, 5–6 mm.</text>
      <biological_entity constraint="distalmost" id="o5562" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending to reflexed, longer than calyx, visible.</text>
      <biological_entity id="o5563" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="reflexed" />
        <character constraint="than calyx" constraintid="o5564" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o5564" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: caly× lobes ± deltate, equal to capsule, ape× sharply acute to acuminate;</text>
      <biological_entity id="o5565" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character constraint="to capsule" constraintid="o5566" is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character char_type="range_value" from="acute" modifier="ape×sharply" name="shape" notes="" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity id="o5566" name="capsule" name_original="capsule" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>corolla blue, banner whitish or blue-tipped, 4–8 mm, glabrous;</text>
      <biological_entity id="o5567" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5568" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o5569" name="banner" name_original="banner" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="blue-tipped" value_original="blue-tipped" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>banner length 0.8–1 times wings, lobe base without folds;</text>
      <biological_entity id="o5570" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5571" name="banner" name_original="banner" src="d0_s10" type="structure">
        <character constraint="wing" constraintid="o5572" is_modifier="false" name="length" src="d0_s10" value="0.8-1 times wings" value_original="0.8-1 times wings" />
      </biological_entity>
      <biological_entity id="o5572" name="wing" name_original="wings" src="d0_s10" type="structure" />
      <biological_entity constraint="lobe" id="o5573" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o5574" name="fold" name_original="folds" src="d0_s10" type="structure" />
      <relation from="o5573" id="r476" name="without" negation="false" src="d0_s10" to="o5574" />
    </statement>
    <statement id="d0_s11">
      <text>banner lobes and wings blue, sometimes purplish, oblong, 1 (–3) mm wide;</text>
      <biological_entity id="o5575" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="banner" id="o5576" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="banner" id="o5577" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>throat barely angled to tube, tube and throat white, narrowed to lips, pouch angular, ± hidden by calyx;</text>
      <biological_entity id="o5578" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5579" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character constraint="to throat" constraintid="o5582" is_modifier="false" modifier="barely" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character constraint="to lips" constraintid="o5583" is_modifier="false" name="shape" notes="" src="d0_s12" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o5581" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <biological_entity id="o5582" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o5583" name="lip" name_original="lips" src="d0_s12" type="structure" />
      <biological_entity id="o5584" name="pouch" name_original="pouch" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="angular" value_original="angular" />
        <character constraint="by calyx" constraintid="o5585" is_modifier="false" modifier="more or less" name="prominence" src="d0_s12" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o5585" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens: filaments glabrous, basal spur 0.</text>
      <biological_entity id="o5586" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o5587" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5588" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds (3 or) 4, oblong, 2–2.5 mm, margins thickened, inrolled.</text>
      <biological_entity id="o5589" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 14, 28, 42.</text>
      <biological_entity id="o5590" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s14" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5591" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collinsia parviflora is the closest relative of C. grandiflora and is primarily a plant of moist montane habitats with well-drained, rocky or sandy soil. However, it occurs in a wide range of habitats across its entire range. The species is also the most widespread taxon within Collinsia. Some plants from the western coastal ranges may be difficult to separate from C. grandiflora.</discussion>
  <discussion>Collinsia parviflora is frequently confused with C. wrightii. The corollas of C. wrightii are distinctly purplish; those of C. parviflora are bright blue. The acute to acuminate sepals of C. parviflora contrast with the blunt, rounded tips of sepals of C. wrightii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forests, grasslands, meadows, eroded banks, bedrock depressions, scree slopes, shrublands, shaded shorelines.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="banks" modifier="eroded" />
        <character name="habitat" value="bedrock depressions" />
        <character name="habitat" value="scree slopes" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="shaded shorelines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.S., Ont., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mass., Mich., Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., Pa., S.Dak., Utah, Vt., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>