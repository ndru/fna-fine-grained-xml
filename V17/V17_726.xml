<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">SYNTHYRIS</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1933" rank="species">missurica</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>85: 89. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus synthyris;species missurica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="Rafinesque" date="1818" rank="species">missurica</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>2: 175. 1818</place_in_publication>
      <other_info_on_pub>based on V. reniformis Pursh, Fl. Amer. Sept. 1: 10. 1813, not Rafinesque 1808</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus veronica;species missurica</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Mountain kittentail</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, some withering in 2d year as new leaves expand;</text>
      <biological_entity id="o3483" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character constraint="as leaves" constraintid="o3484" is_modifier="false" name="life_cycle" src="d0_s0" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o3484" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade orbiculate to reniform, 25+ mm wide, not leathery, base cordate to lobate, margins ± incised-crenate to dentate, teeth apices rounded to acute or acuminate, surfaces glabrous, rarely sparsely hairy;</text>
      <biological_entity id="o3485" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s1" to="reniform" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o3486" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="lobate" />
      </biological_entity>
      <biological_entity id="o3487" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="less incised-crenate" name="shape" src="d0_s1" to="dentate" />
      </biological_entity>
      <biological_entity constraint="teeth" id="o3488" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="acute or acuminate" />
      </biological_entity>
      <biological_entity id="o3489" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal veins extending into distal 1/2 of blade, lateral-veins 2–4 on each side of midvein.</text>
      <biological_entity constraint="basal" id="o3490" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o3491" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o3492" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o3493" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o3494" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o3494" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o3495" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <relation from="o3490" id="r316" name="extending into" negation="false" src="d0_s2" to="o3491" />
      <relation from="o3491" id="r317" name="part_of" negation="false" src="d0_s2" to="o3492" />
      <relation from="o3494" id="r318" name="part_of" negation="false" src="d0_s2" to="o3495" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes erect, to 33 cm in fruit;</text>
      <biological_entity id="o3496" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="in fruit" constraintid="o3497" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="33" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3497" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sterile bracts 3+, ovate-spatulate, largest 1+ cm;</text>
      <biological_entity id="o3498" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-spatulate" value_original="ovate-spatulate" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers 12–100, loosely aggregated.</text>
      <biological_entity id="o3499" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s5" to="100" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s5" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sepals 4.</text>
      <biological_entity id="o3500" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Petals (3 or) 4 (or 5), apex entire or erose;</text>
      <biological_entity id="o3501" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o3502" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue, ± regular, campanulate, much longer than calyx, glabrous or puberulent, tube conspicuous.</text>
      <biological_entity id="o3503" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character constraint="than calyx" constraintid="o3504" is_modifier="false" name="length_or_size" src="d0_s8" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3504" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity id="o3505" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Stamens epipetalous.</text>
      <biological_entity id="o3506" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="epipetalous" value_original="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ovaries: ovules 10–16.</text>
      <biological_entity id="o3507" name="ovary" name_original="ovaries" src="d0_s10" type="structure" />
      <biological_entity id="o3508" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules glabrous or glabrescent.</text>
      <biological_entity id="o3509" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes usually 20–100-flowered, to 33 cm at end of flowering; ovaries glabrous; capsules glabrous.</description>
      <determination>9b. Synthyris missurica subsp. major</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes usually 12–45-flowered, to 25 cm at end of flowering; ovaries sparsely hairy, especially along margins; capsules glabrescent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins regularly toothed; racemes to 20 cm at end of flowering.</description>
      <determination>9a. Synthyris missurica subsp. missurica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins regularly to irregularly toothed; racemes to 25 cm at end of flowering.</description>
      <determination>9c. Synthyris missurica subsp. stellata</determination>
    </key_statement>
  </key>
</bio:treatment>