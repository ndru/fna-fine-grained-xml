<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="2012" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 28. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species bolanderi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 381. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species bolanderi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="species">platylaemus</taxon_name>
    <taxon_hierarchy>genus m.;species platylaemus</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Bolander’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o36787" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 20–900 (–1200) mm, usually glandular-pubescent and viscid.</text>
      <biological_entity id="o36788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="900" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="1200" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="900" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal in rosette, cauline gradually reduced distally;</text>
      <biological_entity id="o36789" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o36790" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o36791" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o36792" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o36790" id="r2823" name="in" negation="false" src="d0_s2" to="o36791" />
    </statement>
    <statement id="d0_s3">
      <text>petiole present proximally, usually absent distally;</text>
      <biological_entity id="o36793" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually; distally" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, obovate, elliptic, or oblanceolate, 5–60 (–85) × 1.5–25 (–32) mm, margins entire or largest irregularly serrate, plane, apex rounded to obtuse, surfaces: proximals nearly glabrous, distals glandular-pubescent.</text>
      <biological_entity id="o36794" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="85" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="32" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36795" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="largest" value_original="largest" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o36796" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o36797" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o36798" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o36799" name="distal" name_original="distals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 2–5 (–8) mm in fruit.</text>
      <biological_entity id="o36800" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o36801" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36801" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant (1 per node on depauperate plants), chasmogamous.</text>
      <biological_entity id="o36802" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o36803" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o36803" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o36804" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o36805" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o36804" id="r2824" name="on" negation="false" src="d0_s6" to="o36805" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, inflated in fruit, (7–) 9–25 (–27) mm, glandular-pubescent and viscid, lobes often spreading in fruit, unequal if calyx large, apex acute to attenuate, ribs prominent, purplish, intercostal areas white.</text>
      <biological_entity id="o36806" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o36807" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o36808" is_modifier="false" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="27" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coating" src="d0_s7" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o36807" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o36808" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o36809" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o36810" is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o36810" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o36811" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o36812" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="attenuate" />
      </biological_entity>
      <biological_entity id="o36813" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o36814" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas magenta, throat often darkening near floor, without dark spots, palate ridges or floor white, usually magenta-speckled, tube-throat 10–30 mm, limb 6–20 mm diam., not bilabiate.</text>
      <biological_entity id="o36815" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
      </biological_entity>
      <biological_entity id="o36816" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character constraint="near " constraintid="o36818" is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="darkening" value_original="darkening" />
      </biological_entity>
      <biological_entity constraint="palate" id="o36817" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark spots" value_original="dark spots" />
      </biological_entity>
      <biological_entity id="o36818" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="true" modifier="usually" name="coloration" src="d0_s8" value="magenta-speckled" value_original="magenta-speckled" />
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36819" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o36820" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent.</text>
      <biological_entity id="o36821" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes unequal, abaxial 2–3 times adaxial.</text>
      <biological_entity id="o36822" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o36823" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o36824" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character constraint="lobe" constraintid="o36825" is_modifier="false" name="size_or_quantity" src="d0_s11" value="2-3 times adaxial lobes" value_original="2-3 times adaxial lobes" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o36825" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 8–20 mm. 2n = 16.</text>
      <biological_entity id="o36826" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36827" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus bolanderi is scattered and uncommon through most of California (with concentrations in the southern North Coast Ranges and Central Sierra) and apparently is disjunct in Jackson County, Oregon.</discussion>
  <discussion>Diplacus bolanderi is highly variable in height, leaf size, corolla size, and calyx size. Identity is unmistakable in larger plants; the inflated calyx with lobes strongly unequal in length usually is helpful in identification of smaller plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granite outcrops, slopes with decomposed granite soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granite" />
        <character name="habitat" value="decomposed granite soils" modifier="outcrops slopes with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(100–)300–1700(–2000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>