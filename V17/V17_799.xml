<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">471</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="Smith" date="1797" rank="species">minor</taxon_name>
    <place_of_publication>
      <publication_title>Engl. Bot.</publication_title>
      <place_in_publication>6: plate 422. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species minor</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orobanche</taxon_name>
    <taxon_name authority="H. St. John &amp; English" date="unknown" rank="species">columbiana</taxon_name>
    <taxon_hierarchy>genus orobanche;species columbiana</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Common or lesser broomrape</other_name>
  <other_name type="common_name">hellroot</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple, (8–) 12–55 (–70) cm, slender, base sometimes abruptly enlarged.</text>
      <biological_entity id="o39845" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o39846" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes abruptly" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots usually conspicuous (often forming a globular mass), very slender, usually branched.</text>
      <biological_entity id="o39847" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves several to numerous, loosely ascending to spreading, imbricate only near stem base;</text>
      <biological_entity id="o39848" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="several" name="quantity" src="d0_s2" to="numerous" />
        <character char_type="range_value" from="loosely ascending" name="orientation" src="d0_s2" to="spreading" />
        <character constraint="near stem base" constraintid="o39849" is_modifier="false" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity constraint="stem" id="o39849" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to oblong-ovate or triangular-ovate, 6–20 mm, margins entire, apex acute to acuminate, surfaces moderately to densely glandular-pubescent.</text>
      <biological_entity id="o39850" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblong-ovate or triangular-ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39851" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o39852" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o39853" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences spikes, reddish-brown to purple or yellow, simple, sparsely to densely glandular-pubescent;</text>
      <biological_entity constraint="inflorescences" id="o39854" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s4" to="purple or yellow" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers numerous, axis visible between flowers;</text>
      <biological_entity id="o39855" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o39856" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character constraint="between flowers" constraintid="o39857" is_modifier="false" name="prominence" src="d0_s5" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o39857" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts slightly reflexed, narrowly lanceolate, 6–17 mm, apex attenuate, glandular-pubescent.</text>
      <biological_entity id="o39858" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39859" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0–0.8 mm (rarely to 30 mm in proximalmost flowers);</text>
      <biological_entity id="o39860" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 0.</text>
      <biological_entity id="o39861" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx yellow or brownish red to brownish purple, strongly bilaterally symmetric, (6–) 8–12 mm, deeply divided into 2 lateral lobes (rarely with an additional vestigial abaxial lobe), lobes entire or asymmetrically divided into 2 teeth or short lobes, these much shorter than tube, lanceolate to subulate-attenuate, ± glandular-villous;</text>
      <biological_entity id="o39862" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o39863" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="brownish red" name="coloration" src="d0_s9" to="brownish purple" />
        <character is_modifier="false" modifier="strongly bilaterally" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character constraint="into lateral lobes" constraintid="o39864" is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o39864" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o39865" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character constraint="into teeth, lobes" constraintid="o39866, o39867" is_modifier="false" modifier="asymmetrically" name="shape" src="d0_s9" value="divided" value_original="divided" />
        <character constraint="than tube" constraintid="o39868" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s9" value="much shorter" value_original="much shorter" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="subulate-attenuate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
      <biological_entity id="o39866" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o39867" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o39868" name="tube" name_original="tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>corolla 10–19 mm, tube white to pale-yellow, not or only slightly constricted above ovary, ± curved, glandular-puberulent;</text>
      <biological_entity id="o39869" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o39870" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39871" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pale-yellow" />
        <character constraint="above ovary" constraintid="o39872" is_modifier="false" modifier="not; only slightly" name="size" src="d0_s10" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="more or less" name="course" notes="" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o39872" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>palatal folds prominent, yellow to nearly white, usually glabrous;</text>
      <biological_entity id="o39873" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o39874" name="fold" name_original="folds" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="nearly white" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lips similar in color to tube, more commonly purplish tinged and/or veined, sometimes more strongly so externally, abaxial lip spreading abruptly from base, 3–4 mm, lobes broadly ovate to ± semiorbiculate (this sometimes difficult to observe because of the crinkled, erose-crenulate margins and overlapping sinuses), apex rounded or shallowly emarginate, adaxial lip erect or curved outward at tip, 3–5 mm, lobes shallow, ± semiorbiculate, apex broadly rounded;</text>
      <biological_entity id="o39875" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o39876" name="lip" name_original="lips" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="commonly" name="coloration" notes="" src="d0_s12" value="purplish tinged" value_original="purplish tinged" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o39877" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <biological_entity constraint="abaxial" id="o39878" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character constraint="from base" constraintid="o39879" is_modifier="false" modifier="sometimes; strongly; externally" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39879" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o39880" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s12" to="more or less semiorbiculate" />
      </biological_entity>
      <biological_entity id="o39881" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s12" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39882" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character constraint="at tip" constraintid="o39883" is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39883" name="tip" name_original="tip" src="d0_s12" type="structure" />
      <biological_entity id="o39884" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="shallow" value_original="shallow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="semiorbiculate" value_original="semiorbiculate" />
      </biological_entity>
      <biological_entity id="o39885" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o39876" id="r3078" name="to" negation="false" src="d0_s12" to="o39877" />
    </statement>
    <statement id="d0_s13">
      <text>filaments sparsely pubescent, distal hairs gland-tipped, anthers included, glabrous or tomentulose.</text>
      <biological_entity id="o39886" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o39887" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o39888" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o39889" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid to oblong-ovoid, 5–9 mm.</text>
      <biological_entity id="o39890" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="oblong-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.2–0.4 mm. 2n = 38.</text>
      <biological_entity id="o39891" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39892" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orobanche minor has been documented most frequently parasitizing introduced clovers (mainly Trifolium arvense and T. repens), and collected rarely on Crotalaria (J. W. Thieret 1971) and Vicia. It also has been recorded, at least historically, on a variety of cultivated hosts in the region, including hemp, carrots (Daucus carota), tobacco, geraniums (Pelargonium spp.), and Petunia spp. Allegedly, the species is toxic to livestock (Thieret). The sole specimen from Idaho (J. A. Allen s.n., 1875, NY) lacks locality data; if the provenance is correct, the elevational range would be extended upward.</discussion>
  <discussion>European authors have recognized a number of infrataxa and segregates; for example, F. J. Rumsey and S. L. Jury (1991) provisionally accepted four varieties of Orobanche minor as occurring in the British Isles. However, they noted that little is known about cytological and morphological variation within the complex. Thus, it seems inappropriate to apply an infraspecific classification to the North American plants.</discussion>
  <discussion>A single historical specimen (J. C. Nelson 3337, 25 August 1920, GH) collected from ship’s ballast in the Linnton area of Portland, Oregon, is an unusually stout plant with apparently pale corollas and filaments relatively densely pubescent toward their bases. This plant may represent a record of Orobanche loricata Reichenbach, a European species that parasitizes mainly Picris and other Asteraceae, and does not affect any crop plants. However, specimen condition precludes definitive determination, and the label does not list a host species. Other materials from Oregon have the typical morphology of O. minor.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old fields, forest margins, woodland openings, railroad embankments, roadsides, pastures, crop fields, orchards, gardens, lawns, disturbed areas, greenhouses.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old fields" />
        <character name="habitat" value="forest margins" />
        <character name="habitat" value="openings" modifier="woodland" />
        <character name="habitat" value="railroad embankments" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="crop fields" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="greenhouses" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Del., D.C., Fla., Ga., Idaho, Md., N.J., N.Y., N.C., Oreg., Pa., S.C., Tex., Vt., Va., Wash., W.Va.; Eurasia; n Africa</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>