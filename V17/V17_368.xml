<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="A. Heller" date="1895" rank="species">triflorus</taxon_name>
    <taxon_name authority="(Pennell) Cory" date="1936" rank="variety">integrifolius</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>38: 407. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species triflorus;variety integrifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triflorus</taxon_name>
    <taxon_name authority="Pennell" date="1935" rank="subspecies">integrifolius</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 251. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species triflorus;subspecies integrifolius</taxon_hierarchy>
  </taxon_identification>
  <number>78b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Cauline leaves: blade lanceolate to linear, margins entire or obscurely, rarely sharply, serrate.</text>
      <biological_entity constraint="cauline" id="o2747" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o2748" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s0" to="linear" />
      </biological_entity>
      <biological_entity id="o2749" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s0" value="obscurely" value_original="obscurely" />
        <character is_modifier="false" modifier="rarely sharply; sharply" name="architecture_or_shape" src="d0_s0" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx lobes 6.5–8.5 × 2.4–3.5 mm;</text>
      <biological_entity id="o2750" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity constraint="calyx" id="o2751" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s1" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>corolla 22–30 mm.</text>
      <biological_entity id="o2752" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o2753" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety integrifolius has been documented in Concho, Crockett, Edwards, Irion, Reagan, Schleicher, Sutton, Tom Green, and Val Verde counties. Specimens from Concho and Tom Green counties with margins of cauline leaves serrate as in var. triflorus and smaller flowers as in var. integrifolius are included here.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone prairies and bluffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" modifier="limestone" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>