<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Glabri</taxon_name>
    <taxon_name authority="(Pennell) S. L. Clark" date="1976" rank="species">longiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>35: 434. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section glabri;species longiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">cyananthus</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="subspecies">longiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>20: 353. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species cyananthus;subspecies longiflorus</taxon_hierarchy>
  </taxon_identification>
  <number>113.</number>
  <other_name type="common_name">Long-flower beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, (11–) 20–60 cm, ± puberulent proximally, glabrous distally, not glaucous.</text>
      <biological_entity id="o19170" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="11" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="more or less; proximally" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, proximals ± puberulent, distals glabrous, not glaucous;</text>
      <biological_entity id="o19171" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o19172" name="proximal" name_original="proximals" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline (24–) 40–90 (–120) × 6–20 (–30) mm, blade obovate, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o19173" name="distal" name_original="distals" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o19174" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o19175" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o19176" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o19177" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19178" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–5 pairs, sessile, (25–) 35–70 × 8–23 mm, blade ovate to lanceolate or elliptic, base tapered to clasping, apex acute, rarely obtuse.</text>
      <biological_entity constraint="cauline" id="o19179" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_length" src="d0_s3" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19180" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate or elliptic" />
      </biological_entity>
      <biological_entity id="o19181" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o19182" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, secund, 7–21 cm, axis glabrous, verticillasters 3–6, cymes 1-flowered or 2 (or 3) -flowered, 2 per node;</text>
      <biological_entity id="o19183" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="21" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19184" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19185" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o19186" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-flowered" value_original="2-flowered" />
        <character constraint="per node" constraintid="o19187" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o19187" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to lanceolate, 5–28 × 2.5–24 mm;</text>
      <biological_entity constraint="proximal" id="o19188" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="28" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glabrous, peduncles to 26 mm, pedicels 1–8 (–12) mm.</text>
      <biological_entity id="o19189" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19190" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19191" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19192" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, 4.5–7 × 2.5–3 mm, apex acute to acuminate, glabrous;</text>
      <biological_entity id="o19193" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o19194" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19195" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue to violet, without nectar guides, ventricose, (20–) 24–30 (–32) mm, glabrous externally, glabrous internally, tube 8–11 mm, throat gradually inflated, not constricted at orifice, 8–10 (–12) mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o19196" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19197" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="24" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="32" to_unit="mm" />
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o19198" name="guide" name_original="guides" src="d0_s8" type="structure" />
      <biological_entity id="o19199" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19200" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o19201" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="diameter" notes="" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" notes="" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o19201" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o19197" id="r1489" name="without" negation="false" src="d0_s8" to="o19198" />
    </statement>
    <statement id="d0_s9">
      <text>stamens: longer pair reaching orifice, pollen-sacs divergent, rarely nearly opposite, sigmoid, 1.8–2.4 (–2.8) mm, dehiscing incompletely, proximal 1/5 indehiscent, connective not splitting, sides sparsely hispid, hairs white, to 0.3 mm, sutures denticulate, teeth to 0.1 mm;</text>
      <biological_entity id="o19202" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o19203" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o19204" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="rarely nearly" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="course_or_shape" src="d0_s9" value="sigmoid" value_original="sigmoid" />
        <character char_type="range_value" from="2.4" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s9" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="distance" src="d0_s9" to="2.4" to_unit="mm" />
        <character is_modifier="false" modifier="incompletely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s9" value="1/5" value_original="1/5" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o19205" name="connective" name_original="connective" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s9" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o19206" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o19207" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19208" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o19209" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o19202" id="r1490" name="reaching" negation="false" src="d0_s9" to="o19203" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 14–16 mm, included, 0.6–1 mm diam., tip straight to recurved, distal 0.5–1 mm moderately pilose, hairs yellow, to 0.8 mm, remaining distal 3–4 mm with scattered yellow hairs;</text>
      <biological_entity id="o19210" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o19211" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19212" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o19213" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
        <character char_type="range_value" constraint="with hairs" constraintid="o19215" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19214" name="hair" name_original="hairs" src="d0_s10" type="structure" />
      <biological_entity id="o19215" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o19213" id="r1491" name="remaining" negation="false" src="d0_s10" to="o19214" />
    </statement>
    <statement id="d0_s11">
      <text>style 18–22 mm.</text>
      <biological_entity id="o19216" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o19217" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 8–14 × 6–10 mm. 2n = 16.</text>
      <biological_entity id="o19218" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19219" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon longiflorus is known from Beaver, Juab, Millard, Piute, Sevier, and Tooele counties (E. C. Neese and N. D. Atwood 2003). The species is most often confused with the partially sympatric P. cyananthus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, Gambel oak and mountain mahogany woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="gambel oak" />
        <character name="habitat" value="mountain mahogany" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>