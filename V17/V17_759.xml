<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Wallich in W. Roxburgh" date="1820" rank="species">undulata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ind.</publication_title>
      <place_in_publication>1: 147. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species undulata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Wavy-leaved water speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials.</text>
      <biological_entity id="o29599" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or prostrate basally, 10–100 cm, glabrous at least proximally, glandular-hairy distally.</text>
      <biological_entity id="o29601" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="at-least proximally; proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0–5 mm;</text>
      <biological_entity id="o29602" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o29603" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to ovate, sometimes ovate-oblong or linear-lanceolate, rarely lanceolate, 20–50 (–100) × 5–20 (–25) mm, 2.5–4 times as long as wide, base attenuate, upwards amplexicaul, margins subentire or crenate to serrate or ± undulate, apex acute to obtuse, surfaces glabrous.</text>
      <biological_entity id="o29604" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o29605" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2.5-4" value_original="2.5-4" />
      </biological_entity>
      <biological_entity id="o29606" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="upwards" name="architecture_or_shape" src="d0_s3" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o29607" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="serrate or more or less undulate" />
      </biological_entity>
      <biological_entity id="o29608" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o29609" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Racemes 6–25, axillary, 50–220 mm, 10–15 mm diam., 10–100-flowered, axis sparsely glandular-hairy, rarely glabrous;</text>
      <biological_entity id="o29610" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="25" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s4" to="220" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="10-100-flowered" value_original="10-100-flowered" />
      </biological_entity>
      <biological_entity id="o29611" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicels 2–6 per cm;</text>
      <biological_entity id="o29612" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per cm" constraintid="o29613" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o29613" name="cm" name_original="cm" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts linear to lanceolate, 3–6 mm, apex acute.</text>
      <biological_entity id="o29614" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29615" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels patent, 3–5 mm, equal to subtending bract, sparsely glandular-hairy.</text>
      <biological_entity id="o29616" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="patent" value_original="patent" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity id="o29617" name="bract" name_original="bract" src="d0_s7" type="structure" />
      <relation from="o29616" id="r2246" name="subtending" negation="false" src="d0_s7" to="o29617" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes 1.5–2 (–3) mm, apex acute, sparsely glandular-hairy, rarely glabrous;</text>
      <biological_entity id="o29618" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o29619" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29620" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pale blue or pale lilac, rarely white, 2.5–5 mm diam.;</text>
      <biological_entity id="o29621" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29622" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale blue" value_original="pale blue" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale lilac" value_original="pale lilac" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 2 mm;</text>
      <biological_entity id="o29623" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29624" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.9–2 mm.</text>
      <biological_entity id="o29625" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o29626" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules slightly compressed in cross-section, obcordiform to globular, 2–3 × 2–3 mm, apex rounded or ± emarginate, sparsely glandular-hairy.</text>
      <biological_entity id="o29627" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character constraint="in cross-section" constraintid="o29628" is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="obcordiform" name="shape" notes="" src="d0_s12" to="globular" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29628" name="cross-section" name_original="cross-section" src="d0_s12" type="structure" />
      <biological_entity id="o29629" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 30–40, ochre, broadly ellipsoid, ± flat, convex on both sides, 0.5–0.6 × 0.4–0.5 mm, thickness and texture unknown.</text>
      <biological_entity id="o29631" name="side" name_original="sides" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 54 (Asia).</text>
      <biological_entity id="o29630" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="40" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="ochre" value_original="ochre" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character constraint="on sides" constraintid="o29631" is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="texture" notes="" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="texture" notes="" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29632" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Veronica undulata was introduced near ports (Mobile, Alabama, and Portland, Oregon) before 1900 via ship ballast from trade with Asia, did not spread much, and may not have persisted.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, wet places, ditches, or swamps.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet places" modifier="disturbed" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Oreg., Wash.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>