<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Greenman" date="1906" rank="species">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>42: 146. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species purpurascens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>93.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (1.5–) 2–4 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with branched, woody roots.</text>
      <biological_entity id="o18933" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18934" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="true" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o18933" id="r1466" name="with" negation="false" src="d0_s2" to="o18934" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few or several, ascending to erect, branched or unbranched, shiny proximally, glabrous proximally, hairy distally, hairs spreading, long, soft, eglandular, sometimes sparsely glandular.</text>
      <biological_entity id="o18935" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="proximally" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18936" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deep purple to green, linear-lanceolate to narrowly lanceolate, rarely broadly lanceolate, 1.5–4.5 cm, not fleshy, margins plane, flat or slightly involute, 0 (–3) -lobed, apex usually acuminate or acute;</text>
      <biological_entity id="o18937" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="deep" value_original="deep" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s4" to="green" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o18938" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0(-3)-lobed" value_original="0(-3)-lobed" />
      </biological_entity>
      <biological_entity id="o18939" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes upright or ascending, lanceolate, apex acute.</text>
      <biological_entity id="o18940" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="upright" value_original="upright" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o18941" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–12 × 2–6 cm;</text>
      <biological_entity id="o18942" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts deep reddish or crimson, rarely magenta or dull orange, or proximally greenish near base, distally colored as above, oblong to broadly lanceolate, often spreading from base and exposing calyces, 0–3 (–5) -lobed;</text>
      <biological_entity id="o18943" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="crimson" value_original="crimson" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="crimson" value_original="crimson" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="crimson" value_original="crimson" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character constraint="near base" constraintid="o18944" is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character constraint="as above" is_modifier="false" modifier="distally" name="coloration" notes="" src="d0_s7" value="colored" value_original="colored" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="broadly lanceolate" />
        <character constraint="from base" constraintid="o18945" is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s7" value="0-3(-5)-lobed" value_original="0-3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o18944" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o18945" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o18946" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <relation from="o18943" id="r1467" name="exposing" negation="false" src="d0_s7" to="o18946" />
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending to erect, linear, short to medium length, arising near or above mid length, apex acute to obtuse.</text>
      <biological_entity id="o18947" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="length" src="d0_s8" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o18948" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, 18–28 mm;</text>
      <biological_entity id="o18949" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o18950" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18950" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial clefts 5–6 mm, adaxial 8–15 mm, clefts 28–50% of calyx length, deeper than laterals, lateral 1.5–6 mm, 5–25% of calyx length;</text>
      <biological_entity constraint="abaxial" id="o18951" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18952" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18953" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character constraint="than laterals" constraintid="o18954" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="28-50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18954" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes long-triangular, apex acute.</text>
      <biological_entity id="o18955" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="long-triangular" value_original="long-triangular" />
      </biological_entity>
      <biological_entity id="o18956" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight to slightly curved, 20–37 mm;</text>
      <biological_entity id="o18957" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s12" to="slightly curved" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="37" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 12–21 mm;</text>
      <biological_entity id="o18958" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>abaxial lip sometimes exserted, beak exserted;</text>
      <biological_entity constraint="abaxial" id="o18959" name="lip" name_original="lip" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o18960" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially green, 10–16 mm;</text>
      <biological_entity id="o18961" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip green, sometimes yellow, usually reduced, protruding through abaxial cleft, sometimes a little pouched, 3-lobed, 2–6 mm, 17–45% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o18962" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s16" value="reduced" value_original="reduced" />
        <character constraint="through abaxial lip" constraintid="o18963" is_modifier="false" name="prominence" src="d0_s16" value="protruding" value_original="protruding" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s16" value="pouched" value_original="pouched" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18963" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="through" name="position" src="d0_s16" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o18964" name="beak" name_original="beak" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>teeth erect, green, whitish, or yellow, 1–1.5 mm. 2n = 48.</text>
      <biological_entity id="o18965" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18966" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja purpurascens is a species of gravelly flood plains and riverbanks at moderate elevations in the vicinity of the Kicking Horse River in Yoho National Park, British Columbia, and in immediately adjacent Alberta. While this tetraploid species may be derived from hybridization between C. miniata and C. rhexiifolia, its combination of traits is unique, and the species forms morphologically consistent populations limited to a habitat not particularly favored by either putative parent species. Very occasional hybrids are known with both C. lutescens and C. miniata.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravel river flats, moist thickets.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel river flats" />
        <character name="habitat" value="moist thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>