<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HIPPURIS</taxon_name>
    <taxon_name authority="Ledebour e× Reichenbach" date="1823" rank="species">montana</taxon_name>
    <place_of_publication>
      <publication_title>Iconogr. Bot. Pl. Crit.</publication_title>
      <place_in_publication>1: 71, plate 86, fig. 181. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus hippuris;species montana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 15–100 mm.</text>
      <biological_entity id="o25472" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s0" to="100" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes 1 mm diam.</text>
      <biological_entity id="o25473" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character name="diameter" src="d0_s1" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves on mid portions of emergent shoots in whorls of 5–7, linear, 2–10 × 0.2–0.5 mm, midvein often conspicuous, lateral-veins absent, ape× acute, tip translucent, callous, not curled in dried plants.</text>
      <biological_entity id="o25474" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="of 5-7" name="arrangement_or_course_or_shape" notes="" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="mid" id="o25475" name="portion" name_original="portions" src="d0_s2" type="structure" />
      <biological_entity id="o25476" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="emergent" value_original="emergent" />
      </biological_entity>
      <biological_entity id="o25477" name="whorl" name_original="whorls" src="d0_s2" type="structure" />
      <biological_entity id="o25478" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o25479" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o25480" name="tip" name_original="tip" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s2" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="texture" src="d0_s2" value="callous" value_original="callous" />
        <character constraint="in plants" constraintid="o25481" is_modifier="false" modifier="not" name="shape" src="d0_s2" value="curled" value_original="curled" />
      </biological_entity>
      <biological_entity id="o25481" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="dried" value_original="dried" />
      </biological_entity>
      <relation from="o25474" id="r1930" name="on" negation="false" src="d0_s2" to="o25475" />
      <relation from="o25475" id="r1931" name="part_of" negation="false" src="d0_s2" to="o25476" />
      <relation from="o25475" id="r1932" name="in" negation="false" src="d0_s2" to="o25477" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers unisexual, staminate in leaf whorls proximal to pistillate;</text>
      <biological_entity id="o25482" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s3" value="unisexual" value_original="unisexual" />
        <character constraint="in leaf whorls" constraintid="o25483" is_modifier="false" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o25483" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>filaments longer than anthers.</text>
      <biological_entity id="o25484" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character constraint="than anthers" constraintid="o25485" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o25485" name="anther" name_original="anthers" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Drupes 1.2 × 1 mm. 2n = 16.</text>
      <biological_entity id="o25486" name="drupe" name_original="drupes" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s5" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25487" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hippuris montana differs from other species of Hippuris by its diminutive size and the tendency for the plants to be woven into the moss carpet; it is probably often overlooked by collectors.</discussion>
  <discussion>The single occurrence reported by N. N. Tzvelev (1980) in the Russian Far East (lower Amur River) of an otherwise North American endemic has not been confirmed.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow streams, stream banks, bogs, seeps, upper montane and alpine zones.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow streams" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="upper montane" />
        <character name="habitat" value="alpine zones" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>