<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="Eastwood" date="1893" rank="species">moffatii</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>4: 9. 1893</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species moffatii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">moffatii</taxon_name>
    <taxon_name authority="(Pennell) D. D. Keck" date="unknown" rank="subspecies">paysonii</taxon_name>
    <taxon_hierarchy>genus penstemon;species moffatii;subspecies paysonii</taxon_hierarchy>
  </taxon_identification>
  <number>72.</number>
  <other_name type="common_name">Moffat’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, (3–) 7–30 cm, retrorsely hairy.</text>
      <biological_entity id="o15370" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, retrorsely hairy;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, 15–45 (–65) × 3–20 (–25) mm, blade spatulate to oblanceolate or ovate, base tapered, margins entire or obscurely dentate distally, apex rounded to obtuse;</text>
      <biological_entity id="o15371" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o15372" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o15373" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="oblanceolate or ovate" />
      </biological_entity>
      <biological_entity id="o15374" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o15375" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely; distally" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o15376" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline (1 or) 2–4 pairs, sessile, 16–55 × 4–10 (–15) mm, blade lanceolate to oblanceolate or linear, base tapered to clasping, margins entire or obscurely dentate distally, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o15377" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s3" to="55" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15378" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblanceolate or linear" />
      </biological_entity>
      <biological_entity id="o15379" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o15380" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely; distally" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o15381" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous to ± interrupted, cylindric, 1–12 cm, axis glandular-pubescent, verticillasters (1–) 3–7, cymes (1 or) 2–4-flowered, 2 per node;</text>
      <biological_entity id="o15382" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character char_type="range_value" from="continuous" name="architecture" src="d0_s4" to="more or less interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15383" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15384" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
      <biological_entity id="o15385" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
        <character constraint="per node" constraintid="o15386" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15386" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to lanceolate, 8–34 × 2–16 mm;</text>
      <biological_entity constraint="proximal" id="o15387" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="34" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o15388" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15389" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate, 4.8–6.2 × 1.2–1.9 mm, glandular-pubescent;</text>
      <biological_entity id="o15390" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o15391" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="length" src="d0_s7" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue to violet, with reddish violet nectar guides, bilabiate, tubular-funnelform, 15–20 (–22) mm, sparsely white-lanate or glabrous internally abaxially, tube 4–6 mm, throat gradually inflated, not constricted at orifice, 4–7 mm diam., rounded abaxially;</text>
      <biological_entity id="o15392" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15393" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="violet" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="22" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="white-lanate" value_original="white-lanate" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o15394" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish violet" value_original="reddish violet" />
      </biological_entity>
      <biological_entity id="o15395" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15396" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o15397" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" notes="" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15397" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o15393" id="r1204" name="with" negation="false" src="d0_s8" to="o15394" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs opposite, navicular, 1.1–1.4 mm, dehiscing completely, sutures papillate;</text>
      <biological_entity id="o15398" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15399" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o15400" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o15401" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 7–9 mm, included or reaching orifice, 0.3–0.5 mm diam., tip straight to recurved, distal 4–5 mm sparsely pubescent, hairs yellow, to 0.8 mm;</text>
      <biological_entity id="o15402" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15403" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character name="position" src="d0_s10" value="reaching orifice" value_original="reaching orifice" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15404" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15405" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15406" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 10–12 mm.</text>
      <biological_entity id="o15407" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15408" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–10 × 4–6 mm.</text>
      <biological_entity id="o15409" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon moffatii is known from Delta, Grand, Mesa, and Montrose counties, Colorado, and Garfield, Grand, and Wayne counties, Utah. Plants with oblanceolate basal leaves and sessile cauline leaves from Montrose County, Colorado, have been distinguished as subsp. paysonii. Plants with these characteristics are found sporadically throughout the range of P. moffatii, sometimes in populations with plants referable to subsp. moffatii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shale or gravelly mesas and slopes, sagebrush shrublands, pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shale" />
        <character name="habitat" value="gravelly mesas" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>