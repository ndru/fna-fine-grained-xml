<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">601</other_info_on_meta>
    <other_info_on_meta type="mention_page">570</other_info_on_meta>
    <other_info_on_meta type="mention_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">592</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Bentham) T. I. Chuang &amp; Heckard" date="1991" rank="species">densiflora</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>16: 656. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species densiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="Bentham" date="1835" rank="species">densiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>13. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species densiflorus</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Dense-flowered owl’s-clover</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 0.7–4.7 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with fibrous-roots.</text>
      <biological_entity id="o27470" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s0" to="4.7" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o27471" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure" />
      <relation from="o27470" id="r2111" name="with" negation="false" src="d0_s1" to="o27471" />
    </statement>
    <statement id="d0_s2">
      <text>Stems solitary, erect, branched, sometimes unbranched, glabrous or glabrate proximally, pubescent distally, hairs moderately dense, spreading, short to long, soft, eglandular, often mixed with short-stipitate-glandular ones (except var. obispoënsis).</text>
      <biological_entity id="o27472" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27473" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size_or_length" src="d0_s2" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character constraint="with ones" constraintid="o27474" is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o27474" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves pale green, linear to broadly lanceolate, linear-lanceolate, or ovate, 1.4–9 cm, not fleshy, margins plane, flat or slightly involute, (0–) 3–5-lobed, apex acuminate;</text>
      <biological_entity id="o27475" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="broadly lanceolate linear-lanceolate or ovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="broadly lanceolate linear-lanceolate or ovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="broadly lanceolate linear-lanceolate or ovate" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s3" to="9" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o27476" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="(0-)3-5-lobed" value_original="(0-)3-5-lobed" />
      </biological_entity>
      <biological_entity id="o27477" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lobes ascending, linear to narrowly to sometimes broadly lanceolate, apex acuminate to acute.</text>
      <biological_entity id="o27478" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character constraint="narrowly to apex" constraintid="o27479" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o27479" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–16 (–20) × 2.5–4 cm;</text>
      <biological_entity id="o27480" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts proximally greenish to deep purple, distally white, or pink to pink-purple or reddish purple on apices, if white sometimes aging pink, lanceolate, 3–5-lobed;</text>
      <biological_entity id="o27481" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s6" to="deep purple distally white or pink" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s6" to="deep purple distally white or pink" />
        <character char_type="range_value" from="proximally greenish" name="coloration" src="d0_s6" to="deep purple distally white or pink" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="pink-purple" value_original="pink-purple" />
        <character constraint="on apices" constraintid="o27482" is_modifier="false" name="coloration" src="d0_s6" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="life_cycle" src="d0_s6" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o27482" name="apex" name_original="apices" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lobes ascending, linear to oblanceolate, long, arising below mid length, apex acute to acuminate.</text>
      <biological_entity id="o27483" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="oblanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o27484" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="arising" value_original="arising" />
        <character is_modifier="true" modifier="below" name="position" src="d0_s7" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o27485" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="length" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyces colored as bracts, 5–20 mm;</text>
      <biological_entity id="o27486" name="calyx" name_original="calyces" src="d0_s8" type="structure">
        <character constraint="as bracts" constraintid="o27487" is_modifier="false" name="coloration" src="d0_s8" value="colored" value_original="colored" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27487" name="bract" name_original="bracts" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial and adaxial clefts 4.7–15 mm, 33–90% of calyx length, slightly deeper than laterals or all 4 clefts subequal, lateral 3–8 mm, 33–60% of calyx length;</text>
      <biological_entity constraint="calyx" id="o27488" name="cleft" name_original="clefts" src="d0_s9" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o27490" is_modifier="false" name="length" src="d0_s9" value="slightly deeper" value_original="slightly deeper" />
      </biological_entity>
      <biological_entity id="o27489" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o27490" name="lateral" name_original="laterals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o27491" name="cleft" name_original="clefts" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o27488" id="r2112" modifier="33-90%" name="part_of" negation="false" src="d0_s9" to="o27489" />
    </statement>
    <statement id="d0_s10">
      <text>lobes linear to narrowly oblanceolate, apex acute.</text>
      <biological_entity id="o27492" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="narrowly oblanceolate" />
      </biological_entity>
      <biological_entity id="o27493" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Corollas straight, 14–29 mm;</text>
      <biological_entity id="o27494" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tube expanded distally;</text>
      <biological_entity id="o27495" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s12" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>abaxial lip and beak exserted from or equal to calyx;</text>
      <biological_entity constraint="abaxial" id="o27496" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character constraint="to calyx" constraintid="o27498" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27497" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character constraint="to calyx" constraintid="o27498" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o27498" name="calyx" name_original="calyx" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>beak straight, adaxially pink, purple, or white, if white often aging light pink, (4–) 5–7 mm, densely puberulent, hairs often stipitate-glandular;</text>
      <biological_entity id="o27499" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="life_cycle" src="d0_s14" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light pink" value_original="light pink" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o27500" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s14" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip proximally white or pink to deep purple, expanded part white throughout, or proximally white or purple, or green becoming light pink with age, distally white or yellow (sometimes becoming orange with age), purple or maroon spots or blotches on each lobe, inflated, lobes 3, pouches gradually (to abruptly) widened, 4–6 mm wide, 2–3 mm deep, deeper than tall, 3–7 mm, 80–100% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o27501" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s15" to="deep purple" />
      </biological_entity>
      <biological_entity id="o27502" name="part" name_original="part" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="green becoming light pink" value_original="green becoming light pink" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character constraint="with age" constraintid="o27503" is_modifier="false" name="coloration" src="d0_s15" value="green becoming light pink" value_original="green becoming light pink" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="maroon spots" value_original="maroon spots" />
        <character constraint="on lobe" constraintid="o27504" is_modifier="false" name="coloration" src="d0_s15" value="blotches" value_original="blotches" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o27503" name="age" name_original="age" src="d0_s15" type="structure" />
      <biological_entity id="o27504" name="lobe" name_original="lobe" src="d0_s15" type="structure" />
      <biological_entity id="o27505" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o27506" name="pouch" name_original="pouches" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="gradually" name="width" src="d0_s15" value="widened" value_original="widened" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration_or_size" src="d0_s15" value="deeper than tall" value_original="deeper than tall" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27507" name="beak" name_original="beak" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect, pink, white (often turning pink with age), chartreuse, or purple, sometimes with deep purple spot at base, 1–2.5 mm.</text>
      <biological_entity id="o27508" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="chartreuse" value_original="chartreuse" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="chartreuse" value_original="chartreuse" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purple" value_original="purple" />
        <character constraint="with" is_modifier="false" modifier="sometimes" name="depth" src="d0_s16" value="deep" value_original="deep" />
        <character constraint="at base" constraintid="o27509" is_modifier="false" name="coloration" src="d0_s16" value="purple spot" value_original="purple spot" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27509" name="base" name_original="base" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Filaments glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 24.</text>
      <biological_entity id="o27510" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27511" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja densiflora is often confused with C. exserta, and both species are broadly overlapping in both range and habitat, often occurring in close proximity. However, intermediates are remarkably rare. The two are most easily separated by the structure and pubescence of the corollas. In addition to the characters mentioned in the key, C. densiflora usually has a bilobed stigma that is exserted from the apex of the corolla with a more or less vertical orientation, while that of C. exserta emerges horizontally and is capitate. These differences are remarkably consistent.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts distally white, rarely pale yellow; coastal grasslands; San Luis Obispo County, sc California.</description>
      <determination>26c. Castilleja densiflora var. obispoënsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts distally pink to pink-purple to red-purple, rarely white; near-coastal and interior grasslands; widespread in w California.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Abaxial lips of corollas appearing slightly inflated, pouches widening gradually, longer than deep, 3–7 mm; calyces 8–20 mm.</description>
      <determination>26a. Castilleja densiflora var. densiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Abaxial lips of corollas appearing moderately inflated, pouches widening abruptly, deeper than long, 4–5 mm; calyces 5–11 mm.</description>
      <determination>26b. Castilleja densiflora var. gracilis</determination>
    </key_statement>
  </key>
</bio:treatment>