<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker*,Brian G. Marcks*,J. Richard Carter *</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">154</other_info_on_meta>
    <other_info_on_meta type="mention_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">Cyperus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 44. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 26. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus Cyperus</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kupeiros, name for Eurasian Cyperus longus Linnaeus</other_info_on_name>
    <other_info_on_name type="fna_id">109010</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial or less often annual, cespitose or not, rhizomatous, stoloniferous, rarely tuberous.</text>
      <biological_entity id="o13100" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="less often" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or not, trigonous or round, glabrous or scabridulous with extrorse or antrorse (rarely retrorse) prickles.</text>
      <biological_entity id="o13101" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="with prickles" constraintid="o13102" is_modifier="false" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o13102" name="prickle" name_original="prickles" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="extrorse" value_original="extrorse" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="antrorse" value_original="antrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually basal;</text>
      <biological_entity id="o13103" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13104" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules absent;</text>
      <biological_entity id="o13105" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades keeled abaxially, flat, V, or inversely W-shaped in cross-section.</text>
      <biological_entity id="o13106" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character constraint="in cross-section" constraintid="o13107" is_modifier="false" modifier="inversely" name="shape" src="d0_s4" value="w--shaped" value_original="w--shaped" />
      </biological_entity>
      <biological_entity id="o13107" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, rarely pseudolateral, 1st order subumbellate to capitate, 2d order with spicate or digitately arranged spikelets, rarely a solitary spikelet;</text>
      <biological_entity id="o13108" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s5" value="pseudolateral" value_original="pseudolateral" />
        <character char_type="range_value" from="subumbellate" name="architecture" src="d0_s5" to="capitate" />
      </biological_entity>
      <biological_entity id="o13109" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
        <character is_modifier="true" modifier="digitately" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o13110" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o13108" id="r1618" name="with" negation="false" src="d0_s5" to="o13109" />
    </statement>
    <statement id="d0_s6">
      <text>spikelets 1–150;</text>
    </statement>
    <statement id="d0_s7">
      <text>1st order rays unequal (rarely equal) in length, produced singly from the axils of inflorescence bracts;</text>
      <biological_entity id="o13111" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="150" />
      </biological_entity>
      <biological_entity id="o13112" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character is_modifier="false" name="length" src="d0_s7" value="unequal" value_original="unequal" />
        <character constraint="from axils" constraintid="o13113" is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o13113" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <biological_entity constraint="inflorescence" id="o13114" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <relation from="o13113" id="r1619" name="part_of" negation="false" src="d0_s7" to="o13114" />
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts 1–22, spirally arranged at culm apex, spreading to erect, leaflike.</text>
      <biological_entity id="o13115" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="22" />
        <character constraint="at culm apex" constraintid="o13116" is_modifier="false" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s8" to="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity constraint="culm" id="o13116" name="apex" name_original="apex" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets: scales to 76, distichous, each subtending flower, cylindric to compressed, borne spicately or digitately at ends of rays (occasionally proliferous).</text>
      <biological_entity id="o13117" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o13118" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="76" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="distichous" value_original="distichous" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="compressed" />
      </biological_entity>
      <biological_entity id="o13119" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity id="o13120" name="end" name_original="ends" src="d0_s9" type="structure" />
      <biological_entity id="o13121" name="ray" name_original="rays" src="d0_s9" type="structure" />
      <relation from="o13118" id="r1620" name="subtending" negation="false" src="d0_s9" to="o13119" />
      <relation from="o13118" id="r1621" name="borne" negation="false" src="d0_s9" to="o13120" />
      <relation from="o13118" id="r1622" name="part_of" negation="false" src="d0_s9" to="o13121" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual [rarely unisexual], in axils of distichous floral scales, bases often decurrent onto rachilla as ± hyaline wings;</text>
      <biological_entity id="o13122" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o13123" name="axil" name_original="axils" src="d0_s10" type="structure" />
      <biological_entity constraint="floral" id="o13124" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="distichous" value_original="distichous" />
      </biological_entity>
      <biological_entity id="o13125" name="base" name_original="bases" src="d0_s10" type="structure">
        <character constraint="onto rachilla" constraintid="o13126" is_modifier="false" modifier="often" name="shape" src="d0_s10" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o13126" name="rachillum" name_original="rachilla" src="d0_s10" type="structure" />
      <biological_entity id="o13127" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o13122" id="r1623" name="in" negation="false" src="d0_s10" to="o13123" />
      <relation from="o13123" id="r1624" name="part_of" negation="false" src="d0_s10" to="o13124" />
      <relation from="o13126" id="r1625" name="as" negation="false" src="d0_s10" to="o13127" />
    </statement>
    <statement id="d0_s11">
      <text>perianth absent;</text>
      <biological_entity id="o13128" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 1–3;</text>
      <biological_entity id="o13129" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles linear, 2–3-fid, base deciduous or persistent;</text>
      <biological_entity id="o13130" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2-3-fid" value_original="2-3-fid" />
      </biological_entity>
      <biological_entity id="o13131" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas 2–3.</text>
      <biological_entity id="o13132" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes biconvex, flattened, or trigonous.</text>
      <biological_entity id="o13133" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s15" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s15" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pantemperate and tropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pantemperate and tropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Umbrella-sedge</other_name>
  <other_name type="common_name">flatsedge</other_name>
  <discussion>Species ca. 600 (96 in the flora).</discussion>
  <references>
    <reference>Carter, J. R. 1984. A Systematic Study of the New World Species of Section Umbellati of Cyperus. Ph.D. dissertation. Vanderbilt University.</reference>
    <reference>Denton, M. F. 1978b. The Luzulae group of Cyperus (Cyperaceae). Contr. Univ. Michigan Herb. 11: 197–271.</reference>
    <reference>Horvat, M. L. 1941. A revision of the subgenus Mariscus found in the United States. Catholic Univ. Amer., Biol. Ser. 33: 1–147.</reference>
    <reference>Marcks, B. G. 1972. Population Studies of North American Cyperus Section Laxiglumi (Cyperaceae). Ph.D. dissertation. University of Wisconsin.</reference>
    <reference>McGivney, M. V. 1938. A revision of the subgenus Eucyperus found in the United States. Catholic Univ. Amer., Biol. Ser. 26: 1–74.</reference>
    <reference>Tucker, G. C. 1983. The taxonomy of Cyperus (Cyperaceae) in Costa Rica and Panama. Syst. Bot. Mongr. 2: 1–85.</reference>
    <reference>Tucker, G. C. 1994. A revision of the Mexican species of Cyperus L. (Cyperaceae). Syst. Bot. Monogr. 43: 1–214.</reference>
    <reference>Tucker, G. C. and R. McVaugh. 1993. Cyperus. In: R. McVaugh and W. R. Anderson, eds. 1974+. Flora Novo-Galiciana: A Descriptive Account of the Vascular Plants of Western Mexico. 8+ vols. Ann Arbor. Vol. 3, pp. 270–344.</reference>
    <reference>Corcoran, M. L. 1941. A revision of the subgenus Pycreus in North and South America. Catholic Univ. Amer., Biol. Ser. 37: 1–68.</reference>
    <reference>Marcks, B. G. 1974. Preliminary reports on the flora of Wisconsin, no. 66. Cyperaceae II—Sedge family II. The genus Cyperus—the umbrella sedges. Trans. Wisconsin Acad. Sci. 62: 261–284.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stigmas 2 (3 in C. serotinus); achenes biconvex.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stigmas 3; achenes trigonous, plano-convex, or terete.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Achenes laterally flattened, borne with edge toward rachilla.</description>
      <determination>15b Cyperus subg. Pycreus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Achenes dorsiventrally flattened, borne with face toward rachilla.</description>
      <determination>15c Cyperus subg. Juncellus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spikelets borne in digitate clusters (rarely singly) or in umbellate or glomerulate heads.</description>
      <determination>15a Cyperus subg. Pycnostachys</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spikelets borne in spikes on conspicuous rachis.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rachilla continuous or articulate only at base.</description>
      <determination>15d Cyperus subg. Cyperus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rachilla articulate at base of each scale, mature spikelet disarticulating into segments consisting of scale, internode, and rachilla wings.</description>
      <determination>15e Cyperus subg. Diclidium</determination>
    </key_statement>
  </key>
</bio:treatment>