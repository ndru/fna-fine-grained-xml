<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="C. B. Clarke in J. D. Hooker" date="1893" rank="subgenus">pycnostachys</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1816" rank="species">cuspidatus</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>1: 204. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus pycnostachys;species cuspidatus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200026673</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose.</text>
      <biological_entity id="o24215" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 1–6 (–25), trigonous to roundly trigonous, (0.5–) 2–5 (–9) cm × 0.2–0.4– (0.6) mm, glabrous.</text>
      <biological_entity id="o24216" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="25" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="6" />
        <character char_type="range_value" from="trigonous" name="shape" src="d0_s1" to="roundly trigonous" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character name="atypical_width" src="d0_s1" unit="mm" value="0.6" value_original="0.6" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s1" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1–6, V-shaped, 1–6 cm × 0.3–1 mm.</text>
      <biological_entity id="o24217" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="false" name="shape" src="d0_s2" value="v--shaped" value_original="v--shaped" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: heads digitate, loosely ovoid, 3–18 × 3–26 mm;</text>
      <biological_entity id="o24218" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o24219" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="digitate" value_original="digitate" />
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays (0–) 1–6 (–8), 2.5–20 (–30) mm;</text>
      <biological_entity id="o24220" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o24221" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s4" to="1" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 2–3 (–5), horizontal to ascending at 45 (–60) °, V-shaped, 2–60 × 0.3–1.2 mm.</text>
      <biological_entity id="o24222" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o24223" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character char_type="range_value" from="horizontal" modifier="45(-60)°" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="v--shaped" value_original="v--shaped" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets (3–) 10–20 (–30), greenish to reddish-brown, narrowly ellipsoid to oblong, 3–11 × (0.8–) 1.2–1.5 mm;</text>
      <biological_entity id="o24224" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s6" to="10" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="30" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s6" to="reddish-brown" />
        <character char_type="range_value" from="narrowly ellipsoid" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_width" src="d0_s6" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral scales (3–) 6–20 (–28), laterally greenish to reddish-brown, glossy, medially greenish to light reddish-brown, strongly recurved, laterally ribless, medially strongly 3-ribbed, oblongovate, (0.8–) 1–1.3 × 0.8–1 (–1.2) mm, apex emarginate, cusp 0.6–1.2 mm.</text>
      <biological_entity constraint="floral" id="o24225" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="6" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="28" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="laterally greenish" name="coloration" src="d0_s7" to="reddish-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="glossy" value_original="glossy" />
        <character char_type="range_value" from="medially greenish" name="coloration" src="d0_s7" to="light reddish-brown" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s7" value="ribless" value_original="ribless" />
        <character is_modifier="false" modifier="medially strongly" name="architecture_or_shape" src="d0_s7" value="3-ribbed" value_original="3-ribbed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_length" src="d0_s7" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24226" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o24227" name="cusp" name_original="cusp" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: stamens 2 or 3;</text>
      <biological_entity id="o24228" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24229" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s8" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers oblong-ovoid, 0.1 (–0.2) mm;</text>
      <biological_entity id="o24230" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24231" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles (0.2–) 0.4–0.6 mm;</text>
      <biological_entity id="o24232" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24233" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 0.3–0.5 mm.</text>
      <biological_entity id="o24234" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24235" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes brown, oblong-obovoid, (0.5–) 0.6 × (0.3–) 0.4 mm, base cuneate, apex blunt, surfaces coarsely papillose.</text>
      <biological_entity id="o24236" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-obovoid" value_original="oblong-obovoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_length" src="d0_s12" to="0.6" to_inclusive="false" to_unit="mm" />
        <character name="length" src="d0_s12" unit="mm" value="0.6" value_original="0.6" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s12" to="0.4" to_inclusive="false" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
      <biological_entity id="o24237" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24238" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o24239" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, disturbed soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed soils" modifier="damp" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.; Mexico; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  
</bio:treatment>