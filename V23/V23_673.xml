<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lisa A. Standley,Jacques Cayouette,Leo Bruederle</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Phacocystis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>146. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Phacocystis</taxon_hierarchy>
    <other_info_on_name type="fna_id">302722</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(J. Carey) H. Christ" date="unknown" rank="section">Acutae</taxon_name>
    <taxon_hierarchy>genus Carex;section Acutae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(Tuckerman ex L. H. Bailey) Mackenzie" date="unknown" rank="section">Cryptocarpae</taxon_name>
    <taxon_hierarchy>genus Carex;section Cryptocarpae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(Rafinesque) Kreczetovicz" date="unknown" rank="section">Temnemis</taxon_name>
    <taxon_hierarchy>genus Carex;section Temnemis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or not, short to long rhizomatous.</text>
      <biological_entity id="o18349" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms brown or redbrown at base.</text>
      <biological_entity id="o18350" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="at base" constraintid="o18351" is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o18351" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths fibrous or not;</text>
      <biological_entity id="o18352" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o18353" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character name="texture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts sometimes dotted purple or red, sometimes prominently veined proximally and becoming ladder-fibrillose, membranous;</text>
      <biological_entity id="o18354" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o18355" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dotted purple" value_original="dotted purple" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" modifier="sometimes prominently; proximally" name="architecture" src="d0_s3" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="becoming" name="architecture" src="d0_s3" value="ladder-fibrillose" value_original="ladder-fibrillose" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped or M-shaped in cross-section when young, glabrous, sometimes papillose.</text>
      <biological_entity id="o18356" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18357" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
        <character constraint="in cross-section" constraintid="o18358" is_modifier="false" name="shape" src="d0_s4" value="m--shaped" value_original="m--shaped" />
      </biological_entity>
      <biological_entity id="o18358" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with 2–8 (–10) spikes;</text>
      <biological_entity id="o18359" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o18360" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
      <relation from="o18359" id="r2216" name="with" negation="false" src="d0_s5" to="o18360" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts scalelike or leaflike, sheathless or short-sheathing;</text>
      <biological_entity constraint="proximal" id="o18361" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sheathless" value_original="sheathless" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-sheathing" value_original="short-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate, occasionally androgynous, or the distal 1–3 staminate, pedunculate or subsessile, prophyllate, at least 2 times as long as wide;</text>
      <biological_entity constraint="lateral" id="o18362" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
      </biological_entity>
      <biological_entity constraint="distal lateral" id="o18363" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>terminal spikes staminate or, rarely, gynecandrous.</text>
      <biological_entity constraint="terminal" id="o18364" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character name="architecture" src="d0_s8" value="," value_original="," />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="gynecandrous" value_original="gynecandrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales with apex obtuse to acuminate or, at least the proximal, long-awned.</text>
      <biological_entity constraint="proximal" id="o18365" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s9" value="long-awned" value_original="long-awned" />
      </biological_entity>
      <biological_entity id="o18366" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acuminate" />
        <character name="shape" src="d0_s9" value="," value_original="," />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18367" name="scale" name_original="scales" src="d0_s9" type="structure" />
      <relation from="o18365" id="r2217" name="with" negation="false" src="d0_s9" to="o18366" />
    </statement>
    <statement id="d0_s10">
      <text>Perigynia erect or ascending, rarely spreading, veined or veinless on faces, with 2 prominent marginal veins, sessile or stipitate, elliptic to ovate or obovate, planoconvex or biconvex in cross-section, base rounded or tapering, apex rounded or tapering to beak or not, usually papillose or, sometimes, smooth;</text>
      <biological_entity id="o18368" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character constraint="on faces" constraintid="o18369" is_modifier="false" name="architecture" src="d0_s10" value="veinless" value_original="veinless" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="ovate or obovate planoconvex or biconvex" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o18371" from="elliptic" name="shape" src="d0_s10" to="ovate or obovate planoconvex or biconvex" />
      </biological_entity>
      <biological_entity id="o18369" name="face" name_original="faces" src="d0_s10" type="structure" />
      <biological_entity constraint="marginal" id="o18370" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o18371" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o18372" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18373" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character constraint="to beak" constraintid="o18374" is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not; usually" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
        <character name="relief" src="d0_s10" value="," value_original="," />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18374" name="beak" name_original="beak" src="d0_s10" type="structure" />
      <relation from="o18368" id="r2218" name="with" negation="false" src="d0_s10" to="o18370" />
    </statement>
    <statement id="d0_s11">
      <text>beak orifice entire, emarginate, or shallowly bidentate.</text>
      <biological_entity constraint="beak" id="o18375" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 2.</text>
      <biological_entity id="o18376" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes biconvex, smaller than bodies of perigynia;</text>
      <biological_entity id="o18377" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character constraint="than bodies" constraintid="o18378" is_modifier="false" name="size" src="d0_s13" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o18378" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o18379" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o18378" id="r2219" name="part_of" negation="false" src="d0_s13" to="o18379" />
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 27–48.</text>
      <biological_entity id="o18380" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="x" id="o18381" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character char_type="range_value" from="27" name="quantity" src="d0_s15" to="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26r.</number>
  <discussion>Species 70–90 (31 in the flora).</discussion>
  <discussion>Carex sect. Phacocystis, while fairly cohesive, contains several distinct groups of species. Members of the Carex acuta group (C. lenticularis, C. nigra, C. rufina, and C. eleusinoides) are slender, cespitose plants that have amphistomatous leaves, bracts longer than the inflorescences, veined, stipitate perigynia distended by the base of the achenes, and chromosome numbers of 2n = 82–86. Members of the C. stricta group (C. schottii, C. senta, C. nudata, and C. angustata) are moderately robust plants with hypostomatous leaves and have scabrous, ladder-fibrillose proximal sheaths, bracts shorter than the inflorescence, and veined perigynia. Several additional groups of two or three species show strong morphologic, anatomical, and chromosomal similarities. This section was previously separated into two sections. Species formerly placed in sect. Cryptocarpae have three-veined, awned scales, pendent spikes, and large achenes that in many species are constricted. Species formerly placed in sect. Acutae have one-veined scales, erect spikes, and smaller achenes that are not constricted. The distinctions do not hold up when variation among all species is considered.</discussion>
  <discussion>Among the most common groups of wetland sedges in North America, species of sect. Phacocystis usually occur in wetlands, on shores, marshes, or wet tundra.</discussion>
  <references>
    <reference>Cayouette, J. and P. Morisset. 1985. Chromosome studies on natural hybrids between maritime species of Carex (sections Phacocystis and Cryptocarpae) in northeastern North America, and their taxonomic implications. Canad. J. Bot. 63: 1957–1982.</reference>
    <reference>Cayouette, J. and P. Morisset. 1986. Chromosome studies on Carex paleacea Wahl., C. nigra (L.) Reichard, and C. aquatilis Wahl. in northeastern North America. Cytologia 51: 857–883.</reference>
    <reference>Cayouette, J. and P. Morisset. 1986b. Chromosome studies on the Carex salina complex (Cyperaceae, section Cryptocarpae) in northeastern North America. Cytologia 51: 817–856.</reference>
    <reference>Faulkner, J. S. 1973. Experimental hybridization of north-west European species in Carex section Acutae (Cyperaceae). Bot. J. Linn. Soc. 67: 233–253.</reference>
    <reference>Standley, L. A. 1983. A clarification of the status of Carex crinita and C. gynandra (Cyperaceae). Rhodora 85: 229–241.</reference>
    <reference>Standley, L. A. 1985. Systematics of the Acutae group of Carex (Cyperaceae) in the Pacific Northwest. Syst. Bot. Monogr. 7: 1–106.</reference>
    <reference>Standley, L. A. 1987. Anatomical and chromosomal studies of Carex section Phacocystis in eastern North America. Bot. Gaz. 148: 507–518.</reference>
    <reference>Standley, L. A. 1990. Allozyme evidence for the hybrid origin of the maritime species Carex salina and Carex recta (Cyperaceae) in eastern North America. Syst. Bot. 15: 182–191.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate scales with prominent, scabrous awn on at least the proximal scales.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate scales with apex acute, acuminate, or mucronate, lacking prominent, scabrous awn.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades involute, 1–2 mm wide.</description>
      <determination>169 Carex subspathacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades not involute, the widest more than 2 mm wide.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia veinless.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia veined.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spikes usually erect; proximal sheaths not ladder-fibrillose.</description>
      <determination>175 Carex recta</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spikes usually pendent; proximal sheaths ladder-fibrillose.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sheaths glabrous; perigynia obovoid; apex of pistillate scales retuse.</description>
      <determination>176 Carex crinita</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sheaths scabrous; perigynia broadly ovoid to ellipsoid; apex of pistillate scales truncate to acuminate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia ovoid to ellipsoid, not papillose over surface; achenes constricted; apex of pistillate scales acuminate.</description>
      <determination>177 Carex gynandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia ovoid, papillose over entire surface; achenes not constricted; apex of pistillate scales truncate.</description>
      <determination>178 Carex mitchelliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal bract often spathelike, enclosing spike; achenes glossy, deeply constricted on 1 edge.</description>
      <determination>170 Carex salina</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Proximal bract not spathelike, not enclosing spike; achenes not glossy, constricted or not.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Beak of perigynium bidentate, apical teeth to 0.5 mm; achenes not constricted.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Beak of perigynium entire, apical teeth not more than 0.3 mm; achenes constricted on 1 or both edges.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sheaths ladder-fibrillose; proximal bract much longer than inflorescence.</description>
      <determination>179 Carex barbarae</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sheaths not ladder-fibrillose; proximal bract equaling inflorescence.</description>
      <determination>181 Carex nebrascensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Perigynia thick-walled, leathery.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Perigynia thin-walled, not leathery.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Spikes pendent, 5–13 mm thick.</description>
      <determination>173 Carex paleacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Spikes erect, 4–7 mm thick.</description>
      <determination>171 Carex ramenskii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Scales dark purple-brown.</description>
      <determination>174 Carex vacillans</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Scales brown to reddish brown.</description>
      <determination>175 Carex recta</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Proximal bract longer than inflorescence (usually at least 1.5 times as long).</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Proximal bract shorter than or equal to inflorescence.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Sheaths ladder-fibrillose.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Sheaths not ladder-fibrillose.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Widest leaf blades not more than 7 mm wide; perigynia veinless on faces.</description>
      <determination>180 Carex obnupta</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Widest leaf blades 10–15 mm wide; perigynia 5–7-veined on faces.</description>
      <determination>186 Carex schottii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Spikes pendent; perigynia yellow-brown, thick-walled, indistinctly veined; scales longer than perigynia, apex acuminate.</description>
      <determination>172 Carex lyngbyei</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Spikes erect or pendent; perigynia green, pale brown, red- or purple-brown, thin-walled, veinless or veined; scales shorter or longer than perigynia, apex not acuminate.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Perigynia veinless; achenes glossy.</description>
      <determination>197 Carex aquatilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Perigynia veined; achenes dull.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Terminal spike staminate.</description>
      <determination>187 Carex lenticularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Terminal spike gynecandrous.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Scales reddish brown, pale; body and beak of perigynium pale green or pale brown.</description>
      <determination>187 Carex lenticularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Scales purple-brown or black; beak of perigynium reddish brown or black, darker than bodies.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Perigynia 5–7-veined on each face, ovoid; stipe to 0.5 mm.</description>
      <determination>187 Carex lenticularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Perigynia 3–5-veined on each face, ellipsoid; stipe not more than 0.2 mm.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Perigynia prominently veined, apex acute; basal sheaths brown.</description>
      <determination>188 Carex rufina</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Perigynia indistinctly veined, apex round; basal sheaths reddish brown.</description>
      <determination>189 Carex eleusinoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Proximal sheaths ladder-fibrillose.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Proximal sheaths not ladder-fibrillose.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Scales pale red-brown.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Scales dark red-brown or black.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Scales longer than perigynia; perigynia olive-green, veinless, inflated, obovoid.</description>
      <determination>192 Carex haydenii</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Scales shorter than or equaling perigynia; perigynia pale brown, 0–5-veined on each face, not inflated, ovoid.</description>
      <determination>184 Carex stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Perigynia veinless.</description>
      <determination>198 Carex scopulorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Perigynia veined.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Perigynia 1–3-veined on each face.</description>
      <determination>182 Carex angustata</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Perigynia 3–9-veined on each face.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Perigynia broadly ellipsoid or ovoid, 2–2.2 mm wide, somewhat thick-walled and leathery; scales red-brown; plants not cespitose.</description>
      <determination>185 Carex senta</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Perigynia narrowly ovoid or ellipsoid, 1.2–1.8 mm wide, thin-walled and leathery; scales red-brown; plants cespitose.</description>
      <determination>183 Carex nudata</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Perigynia veined.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Perigynia veinless.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Scales black; perigynia with purple-brown spots distally; apex of proximal sheaths truncate.</description>
      <determination>190 Carex nigra</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Scales pale red-brown; perigynia green; apex of proximal sheaths prolonged.</description>
      <determination>193 Carex emoryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Perigynia with apex tapering, flat, triangular, twisted; beak orifice obliquely bidentate.</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Perigynia with apex round or acute, not twisted; beak orifice obliquely bidentate or entire.</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Perigynia orange-brown, obovoid; scales black.</description>
      <determination>196 Carex endlichii</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Perigynia green, ovoid; scales red- to purple-brown.</description>
      <determination>195 Carex torta</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Perigynia distended by achenes, fragile, often splitting; beak obliquely bidentate.</description>
      <determination>194 Carex interrupta</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Perigynia not distended or split by achenes; beak orifice entire.</description>
      <next_statement_id>33</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Perigynia inflated, obovoid; scales longer than perigynia, apex acute or acuminate.</description>
      <determination>191 Carex aperta</determination>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Perigynia flat, ellipsoid or obovoid; scales shorter than or equaling perigynia, apex acute or obtuse.</description>
      <next_statement_id>34</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>34</statement_id>
      <description type="morphology">Leaf blades 3.5–5 mm wide; pistillate spikes 4–5 mm thick; perigynia ellipsoid or obovoid, 2.5–4 mm.</description>
      <determination>198 Carex scopulorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>34</statement_id>
      <description type="morphology">Leaf blades 2–3.5 mm wide; pistillate spikes 3–4 mm thick; perigynia ellipsoid, 2–3 mm.</description>
      <determination>199 Carex bigelowii</determination>
    </key_statement>
  </key>
</bio:treatment>