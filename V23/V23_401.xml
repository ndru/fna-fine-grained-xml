<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">228</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="(Chapman) Small" date="unknown" rank="species">globularis</taxon_name>
    <taxon_name authority="(Britton &amp; Small) Gale" date="1944" rank="variety">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>46: 248. 1944</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species globularis;variety pinetorum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357886</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Britton &amp; Small" date="unknown" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Man. S.E. Fl.,</publication_title>
      <place_in_publication>183, 1503. 1933 (as Rynchospora)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rhynchospora;species pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–80 cm.</text>
      <biological_entity id="o6458" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms arching, stiff or lax, slender.</text>
      <biological_entity id="o6459" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikelets dark to pale-brown, broadly ovoid to ovoidlanceoloid, mostly 2–2.5 mm.</text>
      <biological_entity id="o6460" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark" name="coloration" src="d0_s2" to="pale-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: perianth bristles rarely reach 2/3 length of fruit body.</text>
      <biological_entity id="o6461" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="perianth" id="o6462" name="bristle" name_original="bristles" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Fruit body indistinctly crossridged, reticulate with broad, often isodiametric alveolae or lattices;</text>
      <biological_entity constraint="fruit" id="o6463" name="body" name_original="body" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="relief" src="d0_s4" value="crossridged" value_original="crossridged" />
        <character constraint="with lattices" constraintid="o6465" is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s4" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o6464" name="alveola" name_original="alveolae" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="true" modifier="often" name="architecture_or_shape" src="d0_s4" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o6465" name="lattex" name_original="lattices" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>tubercle depressedconic to patelliform, apiculate, nearly as broad as truncate summit of fruit body.</text>
      <biological_entity id="o6466" name="tubercle" name_original="tubercle" src="d0_s5" type="structure" />
      <biological_entity id="o6467" name="summit" name_original="summit" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="patelliform" value_original="patelliform" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
        <character is_modifier="true" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o6468" name="body" name_original="body" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="patelliform" value_original="patelliform" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
        <character is_modifier="true" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <relation from="o6466" id="r769" modifier="nearly; nearly; of fruit" name="to" negation="false" src="d0_s5" to="o6467" />
      <relation from="o6466" id="r770" modifier="nearly; nearly; of fruit" name="to" negation="false" src="d0_s5" to="o6468" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall or all year (south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="south" to="fall" from="spring" constraint=" -year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy savannas, clearings in pine flatwoods, moist sandy swales, bog margins, ponds, lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy savannas" />
        <character name="habitat" value="clearings" constraint="in pine flatwoods" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="sandy swales" modifier="moist" />
        <character name="habitat" value="bog margins" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.; West Indies (Antilles); Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies (Antilles)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45b.</number>
  <discussion>Rhynchospora globularis var. pinetorum frequently shares habitat with var. globularis in Georgia and northern Florida and, apart from a slightly taller and stiffer habit, and slightly longer and (sometimes) paler spikelets, var. pinetorum looks like var. globularis in the field. An inspection of fruit reveals significant differences. Of similar length, shape, and thickness, the fruit body of var. pinetorum has a distinct, shallow surface network of subisodiametric lattices nearly level with frame, or alveolae more sunken and frame more raised. While the tubercle of most plants of var. globularis is evenly conic, the slightly longer tubercle of those in var. pinetorum is concavesided and sharpertipped.</discussion>
  
</bio:treatment>