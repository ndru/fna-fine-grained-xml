<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="Britton in J. K. Small" date="1903" rank="species">ravenelii</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Fl. S.E. U.S.,</publication_title>
      <place_in_publication>184, 1327. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species ravenelii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242357781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="M. C. Johnston" date="unknown" rank="species">austrotexana</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species austrotexana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, densely tufted;</text>
      <biological_entity id="o13400" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes mostly hidden by aerial shoots and roots, not long, 2–3 mm thick, hard, cortex persistent?, internodes very short, scales persistent, 4–7 mm, membranous, slightly fibrous.</text>
      <biological_entity id="o13401" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character constraint="by roots" constraintid="o13403" is_modifier="false" modifier="mostly" name="prominence" src="d0_s1" value="hidden" value_original="hidden" />
        <character is_modifier="false" modifier="not" name="length_or_size" notes="" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o13402" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
      </biological_entity>
      <biological_entity id="o13403" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o13404" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o13405" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o13406" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms terete, when dry with to 12 blunt ribs, 20–55 cm × 0.5–1.1 mm, soft to firm, internally mostly hollow with complete transverse septa 2–4 mm apart, evident only on sectioning culm.</text>
      <biological_entity id="o13407" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="55" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="soft" modifier="when dry with 0-12 blunt ribs" name="texture" src="d0_s2" to="firm" />
        <character constraint="with septa" constraintid="o13408" is_modifier="false" modifier="internally mostly" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
        <character constraint="on culm" constraintid="o13409" is_modifier="false" name="prominence" src="d0_s2" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o13408" name="septum" name_original="septa" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="complete" value_original="complete" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="transverse" value_original="transverse" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13409" name="culm" name_original="culm" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, not splitting, proximally dark red, distally stramineous or reddish, thinly papery, apex often red to brown, obtuse to subtruncate, slightly callose, tooth present, 0.5–1 (–3.7) mm.</text>
      <biological_entity id="o13410" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o13411" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s3" value="dark red" value_original="dark red" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o13412" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s3" to="brown" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="subtruncate" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="callose" value_original="callose" />
      </biological_entity>
      <biological_entity id="o13413" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets lanceoloid, 5–13 × 2–2.5 mm, apex acute to obtuse;</text>
      <biological_entity id="o13414" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13415" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale amplexicaulous, entire;</text>
      <biological_entity constraint="proximal" id="o13416" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="amplexicaulous" value_original="amplexicaulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale empty;</text>
      <biological_entity constraint="subproximal" id="o13417" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="empty" value_original="empty" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral scales appressed in fruit, 10–100, 10–12 per mm of rachilla, medium brown to stramineous, midrib regions often greenish, ovate, 1–1.5 × 1 mm, entire, apex rounded to subacute, carinate in distal part of spikelet.</text>
      <biological_entity constraint="floral" id="o13418" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o13419" is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="medium" value_original="medium" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s7" to="stramineous" />
      </biological_entity>
      <biological_entity id="o13419" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="100" />
        <character char_type="range_value" constraint="per mm" constraintid="o13420" from="10" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <biological_entity id="o13420" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o13421" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o13422" name="region" name_original="regions" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="1.5" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13423" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="subacute" />
        <character constraint="in distal part" constraintid="o13424" is_modifier="false" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13424" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o13425" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <relation from="o13420" id="r1653" name="part_of" negation="false" src="d0_s7" to="o13421" />
      <relation from="o13424" id="r1654" name="part_of" negation="false" src="d0_s7" to="o13425" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles 5–6, pale-brown, stout, equaling achene;</text>
      <biological_entity id="o13426" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o13427" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o13428" name="achene" name_original="achene" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o13429" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13430" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers brown, 0.4–0.9 mm;</text>
      <biological_entity id="o13431" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13432" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid or a few 2-fid in the same spikelet.</text>
      <biological_entity id="o13433" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13434" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="quantity" src="d0_s11" value="few" value_original="few" />
        <character constraint="in spikelet" constraintid="o13435" is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o13435" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Achenes falling with scales, green or medium or dark-brown, obpyriform, compressedtrigonous or some biconvex in same spikelet, angles prominent, 0.6–0.8 × 0.5–0.6 mm, neck short or absent, smooth or very finely reticulate at 20–30X.</text>
      <biological_entity id="o13436" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character constraint="with scales" constraintid="o13437" is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s12" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyriform" value_original="obpyriform" />
        <character constraint="in spikelet" constraintid="o13438" is_modifier="false" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity id="o13437" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o13438" name="spikelet" name_original="spikelet" src="d0_s12" type="structure" />
      <biological_entity id="o13439" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s12" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13440" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="at 20-30x" is_modifier="false" modifier="very finely" name="architecture_or_coloration_or_relief" src="d0_s12" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Tubercles brown, depressed-pyramidal, 0.1–0.2 × 0.2–0.3 mm.</text>
      <biological_entity id="o13441" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="depressed-pyramidal" value_original="depressed-pyramidal" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s13" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh, wet to damp, seasonally wet depressions, flatwoods, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh" />
        <character name="habitat" value="wet to damp" />
        <character name="habitat" value="wet depressions" modifier="seasonally" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (San Luis Potosí).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eleocharis ravenelii is apparently very uncommon in North America. It is often mistaken for very slender-stemmed E. montana, which differs in its spikelets with floral scales 1.5 mm or more, 100–500 per spikelet and 15–40 per mm of rachilla, its mostly larger and biconvex achenes, and its culm septa usually evident without sectioning the culm. Although the holotype of E. ravenelii (NY), from Corpus Christi, Texas, lacks culm bases and leaf sheaths, its culms, spikelets, floral scales, and achenes are typical of the later E. austrotexana M. C. Johnston.</discussion>
  
</bio:treatment>