<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="P. J. Bergius" date="1765" rank="genus">scleria</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">ciliata</taxon_name>
    <taxon_name authority="(Chapman) Fairey" date="1967" rank="variety">glabra</taxon_name>
    <place_of_publication>
      <publication_title>Castanea</publication_title>
      <place_in_publication>32: 48. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus scleria;species ciliata;variety glabra</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357978</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scleria</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="variety">glabra</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>532. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scleria;species pauciflora;variety glabra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scleria</taxon_name>
    <taxon_name authority="Core" date="unknown" rank="species">brittonii</taxon_name>
    <taxon_hierarchy>genus Scleria;species brittonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms slender, glabrous.</text>
      <biological_entity id="o12760" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves short, 2 mm wide, glabrous.</text>
      <biological_entity id="o12761" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character name="width" src="d0_s1" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts glabrous.</text>
      <biological_entity id="o12762" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Staminate spikelets many flowered, clusters more than 1 cm.</text>
      <biological_entity id="o12763" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Tubercles entire or 2-lobed.</text>
      <biological_entity id="o12764" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Achenes 2–3 mm.</text>
      <biological_entity id="o12765" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry sandy soil of thickets, pinelands, and disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry sandy soil" constraint="of thickets , pinelands , and disturbed areas" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.; West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10c.</number>
  <discussion>Scleria ciliata var. glabra is restricted to the Atlantic and Gulf coastal plains; it grows primarily on the outer Coastal Plain.</discussion>
  
</bio:treatment>