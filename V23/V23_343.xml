<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown in J. H. Tuckey" date="1818" rank="genus">lipocarpha</taxon_name>
    <taxon_name authority="(Nees) G. C. Tucker" date="1987" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>68: 410. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus lipocarpha;species drummondii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357852</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemicarpha</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. P. von Martius et al., Fl. Bras.</publication_title>
      <place_in_publication>2(1): 62. 1842 (as drummondi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemicarpha;species drummondii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemicarpha</taxon_name>
    <taxon_name authority="(Vahl) Pax" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_name authority="(Nees) Friedland" date="unknown" rank="variety">drummondii</taxon_name>
    <taxon_hierarchy>genus Hemicarpha;species micrantha;variety drummondii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 2–12 cm × (0.2–) 0.3–0.5 mm.</text>
      <biological_entity id="o25960" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s0" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s0" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1–3 cm × 0.3–0.6 mm;</text>
      <biological_entity id="o25961" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s1" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>longest involucral leaf ± erect.</text>
      <biological_entity constraint="longest" id="o25962" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s2" value="involucral" value_original="involucral" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes 1–2 (–3), ovoid (to globose), 1–5 (–6) × 1–2 mm, not bristly;</text>
      <biological_entity id="o25963" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o25964" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 1–2, longest erect, 0.5–2 cm × 0.3–0.5 mm.</text>
      <biological_entity id="o25965" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o25966" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="length" src="d0_s4" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets: floral scales 2;</text>
      <biological_entity id="o25967" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity constraint="floral" id="o25968" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>1st scale light-brown to reddish-brown, with greenish midvein, obtrullate, 0.9–1.3 × 0.3–0.6 mm, widest above mid length, long-acuminate;</text>
      <biological_entity id="o25969" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity id="o25970" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s6" to="reddish-brown" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="obtrullate" value_original="obtrullate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s6" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s6" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="length" src="d0_s6" value="widest" value_original="widest" />
        <character is_modifier="false" name="shape" src="d0_s6" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o25971" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <relation from="o25970" id="r3190" name="with" negation="false" src="d0_s6" to="o25971" />
    </statement>
    <statement id="d0_s7">
      <text>2d scale reddish-brown distally, with 2–4 reddish veins, oblong, 0.5–0.8 × 0.2 mm;</text>
      <biological_entity id="o25972" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o25973" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s7" to="0.8" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o25974" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o25973" id="r3191" name="with" negation="false" src="d0_s7" to="o25974" />
    </statement>
    <statement id="d0_s8">
      <text>3d scale absent;</text>
      <biological_entity id="o25975" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o25976" name="scale" name_original="scale" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamen 1;</text>
      <biological_entity id="o25977" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o25978" name="stamen" name_original="stamen" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anther 0.2 mm;</text>
      <biological_entity id="o25979" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o25980" name="anther" name_original="anther" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 2.</text>
      <biological_entity id="o25981" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o25982" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes obovoid, terete, 0.5–0.75 × 0.25–0.35 mm, 1.5–2.5 times long as wide.</text>
      <biological_entity id="o25983" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s12" to="0.75" to_unit="mm" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="width" src="d0_s12" to="0.35" to_unit="mm" />
        <character constraint="as wide" is_modifier="false" name="length_or_size" src="d0_s12" value="1.5-2.5 times long" value_original="1.5-2.5 times long" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Emergent shorelines, usually sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="emergent shorelines" />
        <character name="habitat" value="sandy soils" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Ill., Ind., Kans., Mo., Nebr., N.Mex., Ohio, Okla., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>