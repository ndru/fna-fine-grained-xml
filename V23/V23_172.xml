<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="T. V. Egorova" date="1981" rank="subgenus">zinserlingia</taxon_name>
    <taxon_name authority="(Hartmann) O. Schwarz" date="1949" rank="species">quinqueflora</taxon_name>
    <place_of_publication>
      <publication_title>Mitt. Thüring. Bot. Ges.</publication_title>
      <place_in_publication>1: 89. 1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus zinserlingia;species quinqueflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101140</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Hartmann" date="unknown" rank="species">quinqueflorus</taxon_name>
    <place_of_publication>
      <publication_title>Primae Lin. Inst. Bot. ed.</publication_title>
      <place_in_publication>2, 85. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species quinqueflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="(Svenson) Á. Löve" date="unknown" rank="species">fernaldii</taxon_name>
    <taxon_hierarchy>genus E.;species fernaldii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="(Lightfoot) Link" date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_hierarchy>genus E.;species pauciflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_name authority="Svenson" date="unknown" rank="variety">fernaldii</taxon_name>
    <taxon_hierarchy>genus E.;species pauciflora;variety fernaldii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">quinqueflora</taxon_name>
    <taxon_name authority="(Svenson) Hultén" date="unknown" rank="subspecies">fernaldii</taxon_name>
    <taxon_hierarchy>genus E.;species quinqueflora;subspecies fernaldii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o11282" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 0.2–1 mm thick, scales persistent or fugaceous, 2–4 (–7) mm, thinly membranous, not fibrous;</text>
      <biological_entity id="o11283" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>resting buds often present on rhizomes or among culm bases, broadly to narrowly ovoid, 3–6 (–10) × 2–5 mm;</text>
      <biological_entity id="o11284" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s1" value="fugaceous" value_original="fugaceous" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o11285" name="bud" name_original="buds" src="d0_s2" type="structure" />
      <biological_entity constraint="culm" id="o11286" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o11284" id="r1397" name="resting" negation="false" src="d0_s2" to="o11285" />
    </statement>
    <statement id="d0_s3">
      <text>caudices absent, rarely present, soft or rarely hard, 0.5 mm thick.</text>
      <biological_entity id="o11287" name="caudex" name_original="caudices" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="rarely" name="texture" src="d0_s3" value="hard" value_original="hard" />
        <character name="thickness" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms erect, not spirally twisted, not contracted near spikelet, when dry usually with several blunt to acute ridges and sulcate, subterete to slightly compressed, to 2 times wider than thick, 5–35 cm × 0.2–0.5 (–1.2) mm, soft to hard;</text>
      <biological_entity id="o11288" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not spirally" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character constraint="near spikelet" constraintid="o11289" is_modifier="false" modifier="not" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o11289" name="spikelet" name_original="spikelet" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when dry with several blunt to acute ridges and; when dry with several blunt to acute ridges and" name="architecture" src="d0_s4" value="sulcate" value_original="sulcate" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s4" to="slightly compressed" />
        <character is_modifier="false" name="width_or_width" src="d0_s4" value="0-2 times wider than thick" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="soft" name="texture" src="d0_s4" to="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>culm tufts often proximally bulbous (if bulbous then tunicated by papery-fibrous scales).</text>
      <biological_entity constraint="culm" id="o11290" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often proximally" name="architecture" src="d0_s5" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves: distal leaf-sheaths stramineous to brown or reddish proximally, green to stramineous or brown distally, membranous to papery, apex often reddish, subtruncate to acute.</text>
      <biological_entity id="o11291" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o11292" name="sheath" name_original="leaf-sheaths" src="d0_s6" type="structure">
        <character char_type="range_value" from="stramineous" modifier="distally" name="coloration" src="d0_s6" to="brown or reddish proximally" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s6" to="papery" />
      </biological_entity>
      <biological_entity id="o11293" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="subtruncate" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 3–8 × 1.5–4 mm;</text>
      <biological_entity id="o11294" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal scale with a flower, seldom empty, 2–5 mm, 1/2 or more as long as spikelet;</text>
      <biological_entity constraint="proximal" id="o11295" name="scale" name_original="scale" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="seldom" name="architecture" notes="" src="d0_s8" value="empty" value_original="empty" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character constraint="as-long-as spikelet" constraintid="o11297" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o11296" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o11297" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
      <relation from="o11295" id="r1398" name="with" negation="false" src="d0_s8" to="o11296" />
    </statement>
    <statement id="d0_s9">
      <text>floral scales 3–10 per spikelet, 2.5–6 × 1.5–2.5 mm.</text>
      <biological_entity constraint="floral" id="o11298" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o11299" from="3" name="quantity" src="d0_s9" to="10" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" notes="" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11299" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth bristles (0–) 3–6, often unequal, rudimentary to equaling tubercle, stout to slender, spinules dense to apparently absent;</text>
      <biological_entity id="o11300" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o11301" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o11302" name="tubercle" name_original="tubercle" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character char_type="range_value" from="stout" name="size" notes="" src="d0_s10" to="slender" />
      </biological_entity>
      <biological_entity id="o11303" name="spinule" name_original="spinules" src="d0_s10" type="structure">
        <character is_modifier="false" name="density" src="d0_s10" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="apparently" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.5–2.7 (–3.5) mm.</text>
      <biological_entity id="o11304" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11305" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes stramineous to medium brown or graybrown, equilaterally trigonous to compressedtrigonous, rarely some biconvex, obpyriform (to obovoid), 1.6–2.3 × 0.7–1.3 mm, beak variable.</text>
      <biological_entity id="o11306" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s12" to="medium brown" />
        <character name="coloration" src="d0_s12" value="graybrown" value_original="graybrown" />
        <character constraint="to compressedtrigonous" constraintid="o11307" is_modifier="false" modifier="equilaterally" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s12" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyriform" value_original="obpyriform" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s12" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11307" name="compressedtrigonou" name_original="compressedtrigonous" src="d0_s12" type="structure" />
      <biological_entity id="o11308" name="beak" name_original="beak" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Tubercles rarely absent, 0.3–0.4 × 0.2–0.3 mm.</text>
      <biological_entity id="o11309" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s13" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting (spring–)summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="atypical_range" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fens, wet meadows, seeps, springs, hot springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fens" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="hot springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., Ohio, Oreg., Pa., Utah, Vt., Wash., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <other_name type="common_name">Few-flowered spike-rush</other_name>
  <other_name type="common_name">éléocharide à cinq fleurs</other_name>
  <discussion>The chromosome numbers for Eleocharis quinqueflora reported for North America (2n = 80) are in doubt because vouchers and other information are lacking. The often-cited n = 10 is probably erroneous. S.-O. Strandhede and R. M. T. Dahlgren (1968) gave 2n = 132 and 134 from Scandinavia. Recognition of infraspecific taxa within E. quinqueflora is premature pending a worldwide revision of subg. Zinserlingia. It has been reported from North Dakota, although I have not seen specimens. About five varieties and subspecies of E. quinqueflora have been described worldwide.</discussion>
  <discussion>Most specimens from eastern North America and some from the West can be placed in Eleocharis quinqueflora subsp. fernaldii (Svenson) Hultén, which is characterized by its small size (culms to 15 cm × 0.5 mm) and small bulbs. Specimens of E. quinqueflora from 2000–3600 m in California, which are atypical, especially in that the proximal scales of the spikelets do not subtend flowers, may deserve taxonomic recognition. Those plants are also small, with culms only to 15 cm × 0.5 mm; hard caudices are often present at the culm-tuft bases; small, narrowly ovoid bulbs are sometimes present; and perianth bristles are absent or rudimentary. Very few specimens of E. quinqueflora are intermediate with E. suksdorfiana.</discussion>
  
</bio:treatment>