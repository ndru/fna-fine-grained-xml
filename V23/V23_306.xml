<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">cyperus</taxon_name>
    <taxon_name authority="Torrey &amp; Hooker" date="1836" rank="species">cephalanthus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York.</publication_title>
      <place_in_publication>3: 431. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus cyperus;species cephalanthus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357643</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="species">laetus</taxon_name>
    <taxon_name authority="(Torrey &amp; Hooker) Kükenthal" date="unknown" rank="variety">cephalanthus</taxon_name>
    <taxon_hierarchy>genus Cyperus;species laetus;variety cephalanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous.</text>
      <biological_entity id="o8220" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms trigonous, 40–70 (–100) cm × 2–3 mm, scabrid or hirtellate, either immediately proximal to apex or over distal 1/2 of culm.</text>
      <biological_entity id="o8221" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="length" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="scabrid" value_original="scabrid" />
        <character name="pubescence_or_relief" src="d0_s1" value="hirtellate" value_original="hirtellate" />
        <character constraint="to " constraintid="o8223" is_modifier="false" modifier="either immediately" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o8222" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o8223" name="1/2" name_original="1/2" src="d0_s1" type="structure" />
      <biological_entity id="o8224" name="culm" name_original="culm" src="d0_s1" type="structure" />
      <relation from="o8223" id="r982" name="part_of" negation="false" src="d0_s1" to="o8224" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat, 30–80 cm × 4–10 mm, scabrid on margins, ribs on abaxial surface.</text>
      <biological_entity id="o8225" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s2" to="80" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character constraint="on margins" constraintid="o8226" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <biological_entity id="o8226" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity id="o8227" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o8228" name="surface" name_original="surface" src="d0_s2" type="structure" />
      <relation from="o8227" id="r983" name="on" negation="false" src="d0_s2" to="o8228" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes ovoid, 12–20 × 13–18 mm;</text>
      <biological_entity id="o8229" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o8230" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays 3–6, 4–10 (–16) cm, glabrous;</text>
      <biological_entity id="o8231" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o8232" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="16" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 3–5, ± horizontal to reflexed, 3–12 (–36) cm × 0.4–3 (–5) mm, scabrid like leaves;</text>
      <biological_entity id="o8233" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o8234" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="less horizontal" name="orientation" src="d0_s5" to="reflexed" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="36" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8235" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s5" value="scabrid" value_original="scabrid" />
        <character is_modifier="true" name="shape" src="d0_s5" value="like" value_original="like" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachilla deciduous, essentially wingless or wings inconspicuous, hyaline, 0.1 (–0.2) mm wide.</text>
      <biological_entity id="o8236" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o8237" name="rachillum" name_original="rachilla" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="essentially" name="architecture" src="d0_s6" value="wingless" value_original="wingless" />
      </biological_entity>
      <biological_entity id="o8238" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="0.2" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 25–50 (–80), oblong, quadrangular-compressed, 5–10 × 2.5–3.5 mm;</text>
      <biological_entity id="o8239" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="80" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s7" to="50" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrangular-compressed" value_original="quadrangular-compressed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral scales deciduous, 4–10 (–14), marginally clear, laterally brownish to clear distally, or light-brown or reddish-brown, blunt, laterally 3 (–4) -ribbed, ovatelanceolate, 2.5–3 × (1.2–) 1.4–2 mm, often erose apically, apex acute to obtuse, entire or emarginate.</text>
      <biological_entity constraint="floral" id="o8240" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="14" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" modifier="marginally" name="coloration" src="d0_s8" value="clear" value_original="clear" />
        <character char_type="range_value" from="laterally brownish" name="coloration" src="d0_s8" to="clear distally" />
        <character is_modifier="false" name="shape" src="d0_s8" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" src="d0_s8" value="3(-4)-ribbed" value_original="3(-4)-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_width" src="d0_s8" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="often; apically" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o8241" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: anthers 0.4–0.6 mm;</text>
      <biological_entity id="o8242" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8243" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 0.8–1.2 mm;</text>
      <biological_entity id="o8244" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8245" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 2 mm.</text>
      <biological_entity id="o8246" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8247" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes brown to reddish-brown, ± stipitate, ellipsoid, 1.2 × 0.5–0.6 mm, apex obtuse, scarcely to distinctly apiculate, surfaces puncticulate.</text>
      <biological_entity id="o8248" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s12" to="reddish-brown" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s12" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8249" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="scarcely distinctly apiculate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="scarcely distinctly apiculate" />
      </biological_entity>
      <biological_entity id="o8250" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s12" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Okla., Tex.; South America (Argentina, Brazil, Paraguay, Uruguay).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Paraguay)" establishment_means="native" />
        <character name="distribution" value="South America (Uruguay)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>71.</number>
  <discussion>The combination of spreading floral scales and scabrid culms separates this rather uncommon species from any others occurring in its range. The scarcity of Cyperus cephalanthus in the United States and its disjunct distribution suggest it might be naturalized rather than native. It was collected early in the nineteenth century in Louisiana, and the amphitropical distribution is not without parallel in the genus (cf. 12. C. eragrostis).</discussion>
  <discussion>Cyperus cephalanthus has been treated as a variety of the widespread, polymorphic South American C. laetus (G. Kükenthal 1935–1936). Recognition of C. cephalanthus as a species follows recent American floristic practice.</discussion>
  
</bio:treatment>