<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter W. Ball</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="(Ascherson &amp; Graebner) Rouy in G. Rouy et al." date="1912" rank="section">Hallerianae</taxon_name>
    <place_of_publication>
      <publication_title>in G. Rouy et al., Fl. France</publication_title>
      <place_in_publication>13: 439. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Hallerianae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302700</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely to loosely cespitose, short-rhizomatous.</text>
      <biological_entity id="o6505" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely to loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms brown or red or purple-brown at base, sometimes fibrous.</text>
      <biological_entity id="o6506" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character constraint="at base" constraintid="o6507" is_modifier="false" name="coloration" src="d0_s1" value="purple-brown" value_original="purple-brown" />
        <character is_modifier="false" modifier="sometimes" name="texture" notes="" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o6507" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheath fronts membranous;</text>
      <biological_entity id="o6508" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="sheath" id="o6509" name="front" name_original="fronts" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades V-shaped or M-shaped in cross-section when young, proximal leaf-blade usually with 2 lateral-veins more prominent than midvein on adaxial side, papillose or hairy.</text>
      <biological_entity id="o6510" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6511" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="v--shaped" value_original="v--shaped" />
        <character constraint="in cross-section" constraintid="o6512" is_modifier="false" name="shape" src="d0_s3" value="m--shaped" value_original="m--shaped" />
      </biological_entity>
      <biological_entity id="o6512" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o6513" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character constraint="than midvein" constraintid="o6515" is_modifier="false" name="prominence" src="d0_s3" value="more prominent" value_original="more prominent" />
        <character is_modifier="false" name="relief" notes="" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6514" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character constraint="than midvein" constraintid="o6515" is_modifier="false" name="prominence" src="d0_s3" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o6515" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o6516" name="side" name_original="side" src="d0_s3" type="structure" />
      <relation from="o6513" id="r777" modifier="when young" name="with" negation="false" src="d0_s3" to="o6514" />
      <relation from="o6515" id="r778" name="on" negation="false" src="d0_s3" to="o6516" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemose, with 2–6 spikes;</text>
      <biological_entity id="o6517" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o6518" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <relation from="o6517" id="r779" name="with" negation="false" src="d0_s4" to="o6518" />
    </statement>
    <statement id="d0_s5">
      <text>proximal nonbasal bracts with well-developed blades, sheathless or sheath less than 5 mm;</text>
      <biological_entity constraint="proximal nonbasal" id="o6519" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="sheathless" value_original="sheathless" />
      </biological_entity>
      <biological_entity id="o6520" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="development" src="d0_s5" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity id="o6521" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o6519" id="r780" name="with" negation="false" src="d0_s5" to="o6520" />
    </statement>
    <statement id="d0_s6">
      <text>lateral spikes pistillate, sometimes basal, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o6522" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s6" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal spike staminate, rarely gynecandrous.</text>
      <biological_entity constraint="terminal" id="o6523" name="spike" name_original="spike" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s7" value="gynecandrous" value_original="gynecandrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Proximal pistillate scales hyaline, green, or brown, 3–7-veined, apex subobtuse to acuminate, sometimes awned.</text>
      <biological_entity constraint="proximal" id="o6524" name="spike" name_original="spikes" src="d0_s8" type="structure" />
      <biological_entity id="o6525" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
      <biological_entity id="o6526" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="subobtuse" name="shape" src="d0_s8" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perigynia ascending, 12–30-veined with 2, strong marginal veins, stipitate, obovate or ovate, trigonous in cross-section, less than 10 mm, base ± tapering, usually with spongy tissue, apex abruptly contracted or tapering to beak, hairy at least distally;</text>
      <biological_entity id="o6527" name="perigynium" name_original="perigynia" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character constraint="with marginal veins" constraintid="o6528" is_modifier="false" name="architecture" src="d0_s9" value="12-30-veined" value_original="12-30-veined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character constraint="in cross-section" constraintid="o6529" is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6528" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="fragility" src="d0_s9" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o6529" name="cross-section" name_original="cross-section" src="d0_s9" type="structure" />
      <biological_entity id="o6530" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6531" name="tissue" name_original="tissue" src="d0_s9" type="structure">
        <character is_modifier="true" name="texture" src="d0_s9" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o6532" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character constraint="to beak" constraintid="o6533" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="at-least distally; distally" name="pubescence" notes="" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6533" name="beak" name_original="beak" src="d0_s9" type="structure" />
      <relation from="o6530" id="r781" modifier="usually" name="with" negation="false" src="d0_s9" to="o6531" />
    </statement>
    <statement id="d0_s10">
      <text>beak straight or bent, to 0.6 mm, emarginate.</text>
      <biological_entity id="o6534" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="bent" value_original="bent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas 3.</text>
      <biological_entity id="o6535" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes trigonous, usually nearly as large as bodies of perigynia;</text>
      <biological_entity id="o6536" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity id="o6537" name="body" name_original="bodies" src="d0_s12" type="structure" />
      <biological_entity id="o6538" name="perigynium" name_original="perigynia" src="d0_s12" type="structure" />
      <relation from="o6536" id="r782" modifier="usually nearly; nearly" name="as large as" negation="false" src="d0_s12" to="o6537" />
      <relation from="o6537" id="r783" name="part_of" negation="false" src="d0_s12" to="o6538" />
    </statement>
    <statement id="d0_s13">
      <text>style deciduous.</text>
      <biological_entity id="o6539" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s North America to Central America (e Guatemala), s Europe, sw Asia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s North America to Central America (e Guatemala)" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26ll.</number>
  <discussion>Species 5 or more (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perigynia 3–4.5 mm, proximal shorter than subtending scales; culms often with basal spikes.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perigynia 4–6 mm, all exceeding subtending scales; culms without basal spikes.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Faces of perigynia with veins less than 0.1 mm wide, most distinctly narrower than marginal veins; perigynium beak straight or slightly bent; achenes obtuse to truncate at apex.</description>
      <determination>348 Carex planostachys</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Faces of perigynia with most veins ca. 0.1 mm wide or wider, about as wide as marginal veins; perigynium beak mostly abruptly bent; achenes retuse at apex.</description>
      <determination>349 Carex lativena</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes 3–3.5 × 1.9–2 mm, almost filling bodies of perigynia; hairs on distal part of perigynium short, not exceeding 0.2 mm.</description>
      <determination>350 Carex tenax</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes 2.5–2.6 × 1.3–1.7 mm, filling only distal 1/2 of bodies of perigynia; hairs on distal part of perigynium long, many exceeding 0.3 mm.</description>
      <determination>351 Carex dasycarpa</determination>
    </key_statement>
  </key>
</bio:treatment>