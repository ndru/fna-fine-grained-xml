<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="(Palisot de Beauvois ex T. Lestiboudois) Torrey" date="1836" rank="subgenus">limnochloa</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">elongata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>515. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus limnochloa;species elongata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357751</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o9709" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1–1.5 mm thick, soft, longer internodes 2–3 cm, scales 5–14 mm, tubers absent.</text>
      <biological_entity id="o9710" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="longer" id="o9711" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9712" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9713" name="tuber" name_original="tubers" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms obscurely trigonous to terete;</text>
    </statement>
    <statement id="d0_s3">
      <text>spikelet-bearing culms 16–80 cm × 0.5–1.5 mm;</text>
      <biological_entity id="o9714" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="obscurely trigonous" name="shape" src="d0_s2" to="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>when submersed plants often forming numerous, filiform flaccid culms without spikelets, sometimes with whorls of slender branches, 0.1–0.3 mm wide, soft;</text>
      <biological_entity id="o9716" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="true" name="texture" src="d0_s4" value="flaccid" value_original="flaccid" />
      </biological_entity>
      <biological_entity id="o9717" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity id="o9718" name="whorl" name_original="whorls" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" notes="alterIDs:o9718" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o9719" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <relation from="o9715" id="r1179" modifier="when submersed plants often forming numerous" name="forming" negation="false" src="d0_s4" to="o9716" />
      <relation from="o9715" id="r1180" modifier="when submersed plants often forming numerous" name="without" negation="false" src="d0_s4" to="o9717" />
      <relation from="o9715" id="r1181" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o9718" />
      <relation from="o9718" id="r1182" name="part_of" negation="false" src="d0_s4" to="o9719" />
    </statement>
    <statement id="d0_s5">
      <text>sometimes septate-nodulose when aquatic, internally spongy, transverse septa incomplete.</text>
      <biological_entity id="o9715" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s3" to="80" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="when aquatic" name="shape" src="d0_s5" value="septate-nodulose" value_original="septate-nodulose" />
        <character is_modifier="false" modifier="internally" name="texture" src="d0_s5" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o9720" name="septum" name_original="septa" src="d0_s5" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s5" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="incomplete" value_original="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves: distal leaf-sheaths persistent or decaying, membranous, apex acute, often prolonged into translucent portion to 1 mm.</text>
      <biological_entity id="o9721" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o9722" name="sheath" name_original="leaf-sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character name="duration" src="d0_s6" value="decaying" value_original="decaying" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o9723" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character constraint="into portion" constraintid="o9724" is_modifier="false" modifier="often" name="length" src="d0_s6" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o9724" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets not proliferous, (6–) 9–24 × 1.4–2.2 mm;</text>
      <biological_entity id="o9725" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s7" value="proliferous" value_original="proliferous" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s7" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s7" to="24" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachilla joints bearing prominent winglike remnants of floral scales;</text>
      <biological_entity constraint="scale" id="o9726" name="joint" name_original="joints" src="d0_s8" type="structure" constraint_original="scale rachilla; scale" />
      <biological_entity id="o9727" name="remnant" name_original="remnants" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="winglike" value_original="winglike" />
      </biological_entity>
      <biological_entity constraint="floral" id="o9728" name="scale" name_original="scales" src="d0_s8" type="structure" />
      <relation from="o9726" id="r1183" name="bearing" negation="false" src="d0_s8" to="o9727" />
      <relation from="o9726" id="r1184" name="part_of" negation="false" src="d0_s8" to="o9728" />
    </statement>
    <statement id="d0_s9">
      <text>proximal scale with a flower, amplexicaulous, 2.5–4.1 mm;</text>
      <biological_entity constraint="proximal" id="o9729" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="amplexicaulous" value_original="amplexicaulous" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9730" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <relation from="o9729" id="r1185" name="with" negation="false" src="d0_s9" to="o9730" />
    </statement>
    <statement id="d0_s10">
      <text>floral scales 7–26, 1–2 per mm of rachilla, green to stramineous or pale-brown, often minutely dotted reddish, usually with conspicuous dark-brown to blackish submarginal band, narrowly ovate, 3.5–4.5 × 2 mm, thickly papery, membranous toward margins.</text>
      <biological_entity constraint="floral" id="o9731" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="26" />
        <character char_type="range_value" constraint="per mm" constraintid="o9732" from="1" name="quantity" src="d0_s10" to="2" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s10" to="stramineous or pale-brown" />
        <character is_modifier="false" modifier="often minutely; minutely" name="coloration" src="d0_s10" value="dotted reddish" value_original="dotted reddish" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="4.5" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" modifier="thickly" name="texture" src="d0_s10" value="papery" value_original="papery" />
        <character constraint="toward margins" constraintid="o9735" is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o9732" name="mm" name_original="mm" src="d0_s10" type="structure" />
      <biological_entity id="o9733" name="rachillum" name_original="rachilla" src="d0_s10" type="structure" />
      <biological_entity constraint="submarginal" id="o9734" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="conspicuous" value_original="conspicuous" />
        <character char_type="range_value" from="dark-brown" is_modifier="true" name="coloration" src="d0_s10" to="blackish" />
      </biological_entity>
      <biological_entity id="o9735" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <relation from="o9732" id="r1186" name="part_of" negation="false" src="d0_s10" to="o9733" />
      <relation from="o9731" id="r1187" modifier="usually" name="with" negation="false" src="d0_s10" to="o9734" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: perianth bristles 6–7, whitish to stramineous or pale reddish-brown, proximally slightly flattened, unequal, exceeding or rarely shorter than achene, 0.7–1.9 mm, retrorsely spinulose;</text>
      <biological_entity id="o9736" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="perianth" id="o9737" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="7" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s11" to="stramineous or pale reddish-brown" />
        <character is_modifier="false" modifier="proximally slightly" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="position_relational" src="d0_s11" value="exceeding" value_original="exceeding" />
        <character constraint="than achene" constraintid="o9738" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="rarely shorter" value_original="rarely shorter" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="retrorsely" name="architecture_or_shape" src="d0_s11" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o9738" name="achene" name_original="achene" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers yellow to reddish, 1.7–1.9 mm;</text>
      <biological_entity id="o9739" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9740" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="reddish" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 3-fid.</text>
      <biological_entity id="o9741" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9742" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes whitish, stramineous, or pale green, obovoid to obpyriform, compressed trigonous with adaxial face broadest, or biconvex, 0.65–1.4 × 0.5–0.8 mm, clearly sculptured at 10–15X, each face with 10–13 rows of rectangular, transversely elongated cells, apex constricted to short neck 0.2–0.25 (–0.3) mm wide, wider at tubercle base.</text>
      <biological_entity id="o9743" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s14" to="obpyriform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character constraint="with adaxial face" constraintid="o9744" is_modifier="false" name="shape" src="d0_s14" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="biconvex" value_original="biconvex" />
        <character char_type="range_value" from="0.65" from_unit="mm" name="length" src="d0_s14" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="0.8" to_unit="mm" />
        <character constraint="at face" constraintid="o9745" is_modifier="false" modifier="clearly" name="relief" src="d0_s14" value="sculptured" value_original="sculptured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9744" name="face" name_original="face" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o9745" name="face" name_original="face" src="d0_s14" type="structure" />
      <biological_entity id="o9746" name="row" name_original="rows" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s14" to="13" />
      </biological_entity>
      <biological_entity id="o9747" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" modifier="transversely" name="length" src="d0_s14" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o9748" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="constricted" name="size" src="d0_s14" to="short" />
      </biological_entity>
      <biological_entity id="o9749" name="neck" name_original="neck" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.25" from_inclusive="false" from_unit="mm" name="width" src="d0_s14" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s14" to="0.25" to_unit="mm" />
        <character constraint="at tubercle base" constraintid="o9750" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="tubercle" id="o9750" name="base" name_original="base" src="d0_s14" type="structure" />
      <relation from="o9745" id="r1188" name="with" negation="false" src="d0_s14" to="o9746" />
      <relation from="o9746" id="r1189" name="part_of" negation="false" src="d0_s14" to="o9747" />
    </statement>
    <statement id="d0_s15">
      <text>Tubercles dark-brown, pyramidal, 0.2–0.5 × 0.2–0.4 mm.</text>
      <biological_entity id="o9751" name="tubercle" name_original="tubercles" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="pyramidal" value_original="pyramidal" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s15" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sometimes drying ponds, lakeshores, marshes, creeks, canals, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" modifier="sometimes drying" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="canals" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., N.C., Tex.; Mexico; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>61.</number>
  <discussion>Eleocharis elongata sometimes grows with E. robbinsii; no intermediates are known.</discussion>
  
</bio:treatment>