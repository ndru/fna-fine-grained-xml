<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter W. Ball</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Porocystis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>147. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Porocystis</taxon_hierarchy>
    <other_info_on_name type="fna_id">302727</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(Kunth) Mackenzie" date="unknown" rank="section">Virescentes</taxon_name>
    <taxon_hierarchy>genus Carex;section Virescentes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, short-rhizomatous.</text>
      <biological_entity id="o26589" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms redbrown at base, rough distally.</text>
      <biological_entity id="o26590" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o26591" is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" notes="" src="d0_s1" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o26591" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths fibrous;</text>
      <biological_entity id="o26592" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o26593" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o26594" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o26595" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, not more than 8 mm wide, usually pubescent, at least at junction of blade and sheath, sometimes very sparsely.</text>
      <biological_entity id="o26596" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26597" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o26598" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o26598" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" modifier="when young; not" name="width" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o26599" name="junction" name_original="junction" src="d0_s4" type="structure" />
      <biological_entity id="o26600" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o26601" name="sheath" name_original="sheath" src="d0_s4" type="structure" />
      <relation from="o26598" id="r3286" modifier="at-least; sometimes very; very sparsely; sparsely" name="at" negation="false" src="d0_s4" to="o26599" />
      <relation from="o26599" id="r3287" name="part_of" negation="false" src="d0_s4" to="o26600" />
      <relation from="o26599" id="r3288" name="part_of" negation="false" src="d0_s4" to="o26601" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, spikes 2–6;</text>
      <biological_entity id="o26602" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o26603" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal nonbasal bracts filiform or leaflike, sheathless;</text>
      <biological_entity constraint="proximal nonbasal" id="o26604" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sheathless" value_original="sheathless" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate, with not more than 40 (–50) perigynia, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o26605" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
      <biological_entity id="o26606" name="perigynium" name_original="perigynia" src="d0_s7" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" modifier="not" name="quantity" src="d0_s7" upper_restricted="false" />
        <character is_modifier="true" modifier="not" name="quantity" src="d0_s7" value="50]" value_original="50]" />
      </biological_entity>
      <relation from="o26605" id="r3289" name="with" negation="false" src="d0_s7" to="o26606" />
    </statement>
    <statement id="d0_s8">
      <text>terminal spike gynecandrous or staminate.</text>
      <biological_entity constraint="terminal" id="o26607" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="gynecandrous" value_original="gynecandrous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales with apex obtuse to acuminate or awned, glabrous, sometimes margins ciliate.</text>
      <biological_entity constraint="proximal" id="o26608" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26609" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acuminate or awned" />
      </biological_entity>
      <biological_entity id="o26610" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o26608" id="r3290" name="with" negation="false" src="d0_s9" to="o26609" />
    </statement>
    <statement id="d0_s10">
      <text>Perigynia spreading, abaxial face veined, adaxial face veined or veinless, with 2, strong, marginal veins, sometimes stipitate, sometimes inflated, elliptic to broadly obovate, rounded-trigonous to round in cross-section, 1.7–4 mm, base rounded or tapering, apex rounded or tapering, beakless or, rarely, beaked, glabrous or pubescent;</text>
      <biological_entity id="o26611" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26612" name="face" name_original="face" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26613" name="face" name_original="face" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veinless" value_original="veinless" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="broadly obovate rounded-trigonous" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o26615" from="elliptic" name="shape" src="d0_s10" to="broadly obovate rounded-trigonous" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o26614" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="fragility" src="d0_s10" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o26615" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o26616" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o26617" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="beakless" value_original="beakless" />
        <character name="architecture" src="d0_s10" value="," value_original="," />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o26613" id="r3291" name="with" negation="false" src="d0_s10" to="o26614" />
    </statement>
    <statement id="d0_s11">
      <text>beak 0.2–0.5 (–1) mm, orifice entire or subentire.</text>
      <biological_entity id="o26618" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26619" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3.</text>
      <biological_entity id="o26620" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous, almost as large as bodies of perigynia;</text>
      <biological_entity id="o26621" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity id="o26622" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o26623" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o26621" id="r3292" modifier="almost" name="as large as" negation="false" src="d0_s13" to="o26622" />
      <relation from="o26622" id="r3293" name="part_of" negation="false" src="d0_s13" to="o26623" />
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
      <biological_entity id="o26624" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions of the Northern Hemisphere, Central America, and South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions of the Northern Hemisphere" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="and South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26ii.</number>
  <discussion>Species 10 (8 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Terminal spike staminate or, rarely, gynecandrous, then not more than 1/4 of flowers pistillate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Terminal spike gynecandrous, at least 1/3 of flowers pistillate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perigynia 1–1.5 mm wide; beak absent; pistillate scales ovate, 2 times as long as wide.</description>
      <determination>343 Carex pallescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perigynia 1.5–2.2 mm wide; beak 0.2–0.5(–1) mm; pistillate scales ovate-circular, not more than 1.5 times as long as wide.</description>
      <determination>344 Carex torreyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia densely pilose; widest lateral spikes 2–4 mm wide; ligules longer than wide.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia usually glabrous or pubescent, sometimes sparsely pilose; widest lateral spikes (3.5–)4–11 mm wide; ligules usually not longer than wide.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Terminal spike (5–)11–20 mm; anthers 0.7–1.3(–1.6) mm.</description>
      <determination>337 Carex swanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Terminal spike 20–35(–40) mm; anthers (1–)1.6–2(–2.8) mm.</description>
      <determination>338 Carex virescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistillate scales awned to 0.5–2 mm; perigynia papillose, pubescent.</description>
      <determination>342 Carex bushii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistillate scales awnless or awned to 0.5(–1) mm; perigynia not papillose, glabrous or sparsely pilose.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Mature perigynia nearly circular in cross section, spreading.</description>
      <determination>341 Carex caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Mature perigynia ± trigonous in cross section, ascending.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades pilose on both surfaces.</description>
      <determination>339 Carex hirsutella</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades glabrescent, at least abaxially, sometimes persistently pilose proximally.</description>
      <determination>340 Carex complanata</determination>
    </key_statement>
  </key>
</bio:treatment>