<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="(H. Lindberg) H. Lindberg in I. Dörfler" date="unknown" rank="species">mamillata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">mamillata</taxon_name>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species mamillata;subspecies mamillata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242357762</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mat-forming;</text>
      <biological_entity id="o17621" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes evident, long, 0.7–1 mm thick, soft, cortex persistent, longer internodes 2–6 cm, scales often fugaceous, 5–9 mm, membranous, not fibrous.</text>
      <biological_entity id="o17622" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o17623" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="longer" id="o17624" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17625" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s1" value="fugaceous" value_original="fugaceous" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms terete, often with 8–14 blunt ridges when dry, 10–50 cm × 0.5–3 mm, very soft, internally spongy.</text>
      <biological_entity id="o17626" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o17627" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s2" to="14" />
        <character is_modifier="true" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="when dry; very" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="internally" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <relation from="o17626" id="r2134" modifier="often" name="with" negation="false" src="d0_s2" to="o17627" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, often splitting abaxially, mostly reddish proximally, green to stramineous distally, not inflated, not callose, membranous, apex obtuse, tooth absent.</text>
      <biological_entity id="o17628" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o17629" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="often; abaxially" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="mostly; proximally" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="green" modifier="distally" name="coloration" src="d0_s3" to="stramineous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="callose" value_original="callose" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o17630" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17631" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets ovoid, 5–20 × 3–5 mm, acute;</text>
      <biological_entity id="o17632" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale clasping 2/3 of culm, entire;</text>
      <biological_entity constraint="proximal" id="o17633" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character constraint="of culm" constraintid="o17634" name="quantity" src="d0_s5" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17634" name="culm" name_original="culm" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale empty;</text>
      <biological_entity constraint="subproximal" id="o17635" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="empty" value_original="empty" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral scales often spreading in fruit, 15–80, 5 per mm of rachilla, medium brown, midrib regions stramineous or greenish, ovate, 2.5–4 × 1.5–2 mm, entire, apex acute to obtuse, sometimes carinate in distal part of spikelet.</text>
      <biological_entity constraint="floral" id="o17636" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o17637" is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o17637" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="80" />
        <character constraint="per mm" constraintid="o17638" name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o17638" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o17639" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o17640" name="region" name_original="regions" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17641" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse" />
        <character constraint="in distal part" constraintid="o17642" is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17642" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o17643" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <relation from="o17638" id="r2135" name="part_of" negation="false" src="d0_s7" to="o17639" />
      <relation from="o17642" id="r2136" name="part_of" negation="false" src="d0_s7" to="o17643" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles (4–) 5–6 (–8), brown, slender to stout, mostly exceeding tubercle, to 2 times longer than achene;</text>
      <biological_entity id="o17644" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o17645" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="8" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="slender" name="size" src="d0_s8" to="stout" />
        <character constraint="achene" constraintid="o17647" is_modifier="false" name="length_or_size" src="d0_s8" value="0-2 times longer than achene" />
      </biological_entity>
      <biological_entity id="o17646" name="tubercle" name_original="tubercle" src="d0_s8" type="structure" />
      <biological_entity id="o17647" name="achene" name_original="achene" src="d0_s8" type="structure" />
      <relation from="o17645" id="r2137" modifier="mostly" name="exceeding" negation="false" src="d0_s8" to="o17646" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o17648" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17649" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers stramineous, 0.9–1.5 mm;</text>
      <biological_entity id="o17650" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17651" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 2-fid.</text>
      <biological_entity id="o17652" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17653" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes not persistent, yellow maturing to stramineous, biconvex, angles obscure, obovoid (to obpyriform), 1–1.4 × 0.9–1.3 mm, apex rounded, neck absent, smooth at 30X.</text>
      <biological_entity id="o17654" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s12" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity id="o17655" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17656" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o17657" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character constraint="at 30x" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Tubercles brown to partly whitish, pyramidal to mamillate, lower than to as high as wide, 0.2–0.6 × 0.4–0.6 mm. 2n = 16.</text>
      <biological_entity id="o17658" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="partly whitish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="relief" src="d0_s13" value="mamillate" value_original="mamillate" />
        <character is_modifier="false" name="position" src="d0_s13" value="lower than to as-high-as wide" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17659" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh lakeshores, shallow ponds, streams, floating mats, bogs, fens, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh lakeshores" />
        <character name="habitat" value="shallow ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="floating mats" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Ont., Que., Sask., Yukon; Alaska, Minn., Wash., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <other_name type="common_name">Éléocharide à tétons</other_name>
  <discussion>Eleocharis mamillata has been confused in North America with E. macrostachya and E. palustris. In addition to the perianth bristle and achene differences as given in the key, E. mamillata differs from E. palustris in culm stomate shape and distance between epidermal collenchyma strands (S. M. Walters 1953b; S.-O. Strandhede and R. Dahlgren 1968). Eleocharis mamillata subsp. mamillata, with the tubercle mamillate, usually shorter than wide, and subsp. austriaca (Hayek) Strandhede, with the tubercle conic, longer than wide, are recognized in Europe; in North America only E. mamillata subsp. mamillata is thus far known. The stamen filaments usually remain attached to the shed achenes, and together with the bristles they keep the achenes in ball-like aggregates that drift with winds and water currents (S.-O. Strandhede 1966).</discussion>
  
</bio:treatment>