<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="treatment_page">180</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">cyperus</taxon_name>
    <taxon_name authority="(Linnaeus) Torrey in J. Carey" date="1847" rank="species">retrofractus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Carey, Carices North. U.S.,</publication_title>
      <place_in_publication>519. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus cyperus;species retrofractus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357709</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">retrofractus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 50. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species retrofractus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">dipsaciformis</taxon_name>
    <taxon_hierarchy>genus Cyperus;species dipsaciformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Linnaeus) Vahl" date="unknown" rank="species">retrofractus</taxon_name>
    <taxon_name authority="(Fernald) Kükenthal" date="unknown" rank="variety">dipsaciformis</taxon_name>
    <taxon_hierarchy>genus Cyperus;species retrofractus;variety dipsaciformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mariscus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">retrofractus</taxon_name>
    <taxon_hierarchy>genus Mariscus;species retrofractus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose.</text>
      <biological_entity id="o30077" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms with cormlike bases, sharply trigonous, 25–90 cm, glabrous proximally, minutely scabridulous distally.</text>
      <biological_entity id="o30078" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" notes="" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; distally" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o30079" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="cormlike" value_original="cormlike" />
      </biological_entity>
      <relation from="o30078" id="r3714" name="with" negation="false" src="d0_s1" to="o30079" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves V-shaped, 14–40 cm × 3.5–8 mm, (pubescent on adaxial side of midrib).</text>
      <biological_entity id="o30080" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="v--shaped" value_original="v--shaped" />
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes loose, obtrulloid to obdeltoid (broadest near apex, nearly as long as wide), 20–28 (–34) × (12–) 15–30 mm;</text>
      <biological_entity id="o30081" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o30082" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character char_type="range_value" from="obtrulloid" name="shape" src="d0_s3" to="obdeltoid" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="34" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="28" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays 4–13, (1–) 4–24 cm (longest ray shorter than bracts), glabrous;</text>
      <biological_entity id="o30083" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o30084" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="13" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="24" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 3–12, ascending at 30–45°, flat, 3–25 cm × 3–7 mm;</text>
      <biological_entity id="o30085" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o30086" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="12" />
        <character is_modifier="false" modifier="30-45°" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachilla persistent, wings 0.4–0.6 mm wide, covering 3/4 of mature achene.</text>
      <biological_entity id="o30087" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o30088" name="rachillum" name_original="rachilla" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o30089" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s6" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="position_relational" src="d0_s6" value="covering" value_original="covering" />
        <character constraint="of achene" constraintid="o30090" name="quantity" src="d0_s6" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o30090" name="achene" name_original="achene" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 30–50, proximal one reflexed, distal ones divaricate, linear-lanceoloid, ± terete, (6.5–) 8–10 (–17) × 0.5–0.7 mm;</text>
      <biological_entity id="o30091" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" to="50" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30092" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30093" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceoloid" value_original="linear-lanceoloid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="atypical_length" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral scales persistent, 3–6 (–8), appressed, stramineous (suffused with reddish purple), 4–5-ribbed laterally, ovatelanceolate, (4–) 4.4–4.9 (–5.4) × 1.3–1.5 mm;</text>
      <biological_entity constraint="floral" id="o30094" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" src="d0_s8" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s8" to="4.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="5.4" to_unit="mm" />
        <character char_type="range_value" from="4.4" from_unit="mm" name="length" src="d0_s8" to="4.9" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal scale not spinose, apex straight, excurved mucronate, or cuspidate.</text>
      <biological_entity constraint="terminal" id="o30095" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s9" value="spinose" value_original="spinose" />
      </biological_entity>
      <biological_entity id="o30096" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: anthers 0.7–1.2 mm;</text>
      <biological_entity id="o30097" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o30098" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 1.5–2 mm;</text>
      <biological_entity id="o30099" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o30100" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 2–2.5 mm.</text>
      <biological_entity id="o30101" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30102" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes brown to brownish black, narrowly oblong, (2.2–) 2.5–3 × 0.6–0.7 mm, base cuneate, apex obtuse, apiculate, surfaces puncticulate.</text>
      <biological_entity id="o30103" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="brownish black" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="atypical_length" src="d0_s13" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30104" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30105" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o30106" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s13" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, pastures, disturbed sites in sandy or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="disturbed sites" constraint="in sandy or clayey soils" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., D.C., Ga., Ky., Mo., N.J., N.C., Ohio, Pa., S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>72.</number>
  <other_name type="common_name">Rough flatsedge</other_name>
  <discussion>For information concerning nomenclature of Cyperus retrofractus, see J. R. Carter and C. E. Jarvis (1986).</discussion>
  
</bio:treatment>