<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">cyperus</taxon_name>
    <taxon_name authority="Nees ex Steudel" date="1855" rank="species">oxylepis</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl. Glumac.</publication_title>
      <place_in_publication>2: 25. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus cyperus;species oxylepis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357695</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, coarse, (culms, leaves, bracts, and rays viscid).</text>
      <biological_entity id="o15906" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms roundly trigonous, 10–50 cm × 0.9–2.4 mm.</text>
      <biological_entity id="o15907" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="roundly" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s1" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: adaxial face concave, becoming flat to trigonous apically, 10–46 cm × 1.5–4 mm, margins involute.</text>
      <biological_entity id="o15908" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o15909" name="face" name_original="face" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="flat" modifier="becoming; apically" name="shape" src="d0_s2" to="trigonous" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="46" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15910" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s2" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes ovoid, 1–5.5 cm wide;</text>
      <biological_entity id="o15911" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o15912" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays usually 3–6, 0.5–5 cm, glabrous;</text>
      <biological_entity id="o15913" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o15914" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sometimes absent in small plants;</text>
      <biological_entity id="o15915" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="in plants" constraintid="o15916" is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15916" name="plant" name_original="plants" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>if absent, inflorescence a congested head of spikelets 1–3.5 cm diam.;</text>
      <biological_entity id="o15917" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o15918" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
      <biological_entity id="o15919" name="head" name_original="head" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15920" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <relation from="o15919" id="r1949" modifier="if absent" name="part_of" negation="false" src="d0_s6" to="o15920" />
    </statement>
    <statement id="d0_s7">
      <text>2d order rays 0–3, 1–3 cm;</text>
      <biological_entity id="o15921" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o15922" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="3" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3–5, vertical to ascending at 45°, 2.5–25 cm × 1.2–4 mm, margins involute;</text>
      <biological_entity id="o15923" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o15924" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="vertical" modifier="45°" name="orientation" src="d0_s8" to="ascending" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15925" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d order bracts 0–2, 5–20 mm;</text>
      <biological_entity id="o15926" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o15927" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="2" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachilla persistent, wingless.</text>
      <biological_entity id="o15928" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <biological_entity id="o15929" name="rachillum" name_original="rachilla" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="wingless" value_original="wingless" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 5–24, greenish yellow to golden brown, oblong to linear-lanceoloid, quadrangular, strongly compressed, 7–20 (–30) × 2.5–4 (–6) mm;</text>
      <biological_entity id="o15930" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="24" />
        <character char_type="range_value" from="greenish yellow" name="coloration" src="d0_s11" to="golden brown" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="linear-lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="quadrangular" value_original="quadrangular" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>floral scales 10–20 (–40), spreading, pale green to stramineous, laterally 2–3-ribbed, ovatelanceolate, 3.1–4 × 1.5–2.4 mm, apex with mucro 0.2–0.8 mm.</text>
      <biological_entity constraint="floral" id="o15931" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="40" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s12" to="stramineous" />
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" src="d0_s12" value="2-3-ribbed" value_original="2-3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15932" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o15933" name="mucro" name_original="mucro" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o15932" id="r1950" name="with" negation="false" src="d0_s12" to="o15933" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: anthers 0.6–0.8 mm;</text>
      <biological_entity id="o15934" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15935" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.8–1.6 mm;</text>
      <biological_entity id="o15936" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15937" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 1–1.4 mm.</text>
      <biological_entity id="o15938" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o15939" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light to dark-brown, rarely somewhat reddish, stipitate, ellipsoid, 2–2.4 × 0.5–0.8 mm, base cuneate, stipe whitish, spongy, 0.2–0.3 × 0.2–0.4 mm, apex acute, persistent style forming beak 0.5–1.2 mm, surfaces glabrous or finely papillose.</text>
      <biological_entity id="o15940" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s16" to="dark-brown" />
        <character is_modifier="false" modifier="rarely somewhat" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s16" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15941" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o15942" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="texture" src="d0_s16" value="spongy" value_original="spongy" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s16" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15943" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15944" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="true" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15945" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <biological_entity id="o15946" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o15944" id="r1951" name="forming" negation="false" src="d0_s16" to="o15945" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches and disturbed places in marshes, often in saline soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" constraint="in marshes" />
        <character name="habitat" value="disturbed places" constraint="in marshes" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="saline soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ark., Fla., Ga., La., Miss., S.C., Tex.; Mexico; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50.</number>
  <discussion>Cyperus oxylepis is easily recognized by its sticky leaves, culms, and bracts (in living plants), involute leaves,and golden brown spikelets. The ovate-lanceolate floral scales and the ellipsoid, brownish achene with a persistent beak distinguish C. oxylepis from other species with deciduous floral scales.</discussion>
  
</bio:treatment>