<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="Britton in J. K. Small" date="1903" rank="species">perplexa</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Fl. S.E. U.S.,</publication_title>
      <place_in_publication>197, 1328. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species perplexa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357914</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phaeocephalum</taxon_name>
    <taxon_name authority="(Britton) House" date="unknown" rank="species">perplexum</taxon_name>
    <taxon_hierarchy>genus Phaeocephalum;species perplexum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, 50–110 cm;</text>
      <biological_entity id="o28165" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o28166" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms lax, often excurved, slender, ± terete or trigonous.</text>
      <biological_entity id="o28167" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s2" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending, exceeded by culm;</text>
      <biological_entity id="o28168" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o28169" name="culm" name_original="culm" src="d0_s3" type="structure" />
      <relation from="o28168" id="r3472" name="exceeded by" negation="false" src="d0_s3" to="o28169" />
    </statement>
    <statement id="d0_s4">
      <text>blades linear, proximally flat, 1.5–2.5 mm wide, apex trigonous, subulate, tapering.</text>
      <biological_entity id="o28170" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28171" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: clusters 3–4, widely spaced, narrowly, compactly, or diffusely turbinate;</text>
      <biological_entity id="o28172" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="narrowly; compactly; diffusely" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leafy bracts exceeding proximal clusters.</text>
      <biological_entity id="o28173" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o28174" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28175" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <relation from="o28174" id="r3473" name="exceeding" negation="false" src="d0_s6" to="o28175" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets deep redbrown, ovoid to broadly ellipsoid, 2–3 mm, apex acute to acuminate;</text>
      <biological_entity id="o28176" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s7" to="broadly ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28177" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile scales broadly elliptic to obovate or orbiculate, 1.4–2 (–2.5) mm, apex rounded to notched, midrib shortexcurrent.</text>
      <biological_entity id="o28178" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s8" to="obovate or orbiculate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28179" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="notched" />
      </biological_entity>
      <biological_entity id="o28180" name="midrib" name_original="midrib" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles 0–3, vestigial when present.</text>
      <biological_entity id="o28181" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o28182" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" modifier="when present" name="prominence" src="d0_s9" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits 2–4 per spikelet, 1.5 mm;</text>
      <biological_entity id="o28183" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o28184" from="2" name="quantity" src="d0_s10" to="4" />
        <character name="some_measurement" notes="" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o28184" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>body pale-brown to brown, strongly flattened, orbicular to broadly obovoid, 1–1.3 × 0.9–1.2 mm, surfaces sharply transversely wavyrugose, intervals finely vertically striate with rows of linearrectangular alveolae;</text>
      <biological_entity id="o28185" name="body" name_original="body" src="d0_s11" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s11" to="brown" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s11" to="broadly obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28186" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sharply transversely" name="relief" src="d0_s11" value="wavyrugose" value_original="wavyrugose" />
      </biological_entity>
      <biological_entity id="o28187" name="interval" name_original="intervals" src="d0_s11" type="structure">
        <character constraint="with rows" constraintid="o28188" is_modifier="false" modifier="finely vertically" name="coloration_or_pubescence_or_relief" src="d0_s11" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o28188" name="row" name_original="rows" src="d0_s11" type="structure" />
      <biological_entity id="o28189" name="linearrectangular" name_original="linearrectangular" src="d0_s11" type="structure" />
      <biological_entity id="o28190" name="alveola" name_original="alveolae" src="d0_s11" type="structure" />
      <relation from="o28188" id="r3474" name="part_of" negation="false" src="d0_s11" to="o28189" />
      <relation from="o28188" id="r3475" name="part_of" negation="false" src="d0_s11" to="o28190" />
    </statement>
    <statement id="d0_s12">
      <text>tubercle depressed, triangular, flattened, 0.2–0.3 mm, base lunate.</text>
      <biological_entity id="o28191" name="tubercle" name_original="tubercle" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="depressed" value_original="depressed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28192" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lunate" value_original="lunate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–fall or all year (south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="south" to="fall" from="late spring" constraint=" -year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of pond and lakeshores, depressions in savannas and flatwoods, or seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of pond" />
        <character name="habitat" value="peats" constraint="of pond" />
        <character name="habitat" value="pond" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="depressions" constraint="in savannas and flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex., Va.; West Indies (Cuba, Dominican Republic); Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <discussion>In habit and in shape, size, and color of spikelet, Rhynchospora perplexa strongly resembles R. microcarpa, a species with which it is commonly associated in the Coastal Plain. An examination of the fruit shows those of R. perplexa to be flattened, with fewer and much coarser transverse ridges, the intervals with very narrow vertical alveolae. The perianth in most instances is absent or rudimentary. Fruit of R. microcarpa is biconvex with more transverse ridges (eight or more), the intervals more coarsely alveolate; its perianth bristles are six, evident, extending at least halfway up the fruit body.</discussion>
  
</bio:treatment>