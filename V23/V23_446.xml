<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="P. J. Bergius" date="1765" rank="genus">scleria</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">ciliata</taxon_name>
    <taxon_name authority="(Chapman) Fernald" date="1937" rank="variety">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>39: 392. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus scleria;species ciliata;variety elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357977</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scleria</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>531. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scleria;species elliottii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms stout, pubescent or ciliate.</text>
      <biological_entity id="o27906" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3.5–7 mm wide, pubescent or ciliate.</text>
      <biological_entity id="o27907" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts densely ciliate.</text>
      <biological_entity id="o27908" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scales pubescent;</text>
      <biological_entity id="o27909" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate scales usually with conspicuous midrib projecting as keel toward base.</text>
      <biological_entity id="o27910" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27911" name="midrib" name_original="midrib" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character constraint="as keel" constraintid="o27912" is_modifier="false" name="orientation" src="d0_s4" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o27912" name="keel" name_original="keel" src="d0_s4" type="structure" />
      <biological_entity id="o27913" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o27910" id="r3446" name="with" negation="false" src="d0_s4" to="o27911" />
      <relation from="o27912" id="r3447" name="toward" negation="false" src="d0_s4" to="o27913" />
    </statement>
    <statement id="d0_s5">
      <text>Tubercles usually strongly 2-lobed.</text>
      <biological_entity id="o27914" name="tubercle" name_original="tubercles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually strongly" name="shape" src="d0_s5" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Achenes 2.6–3.6 × 2–2.6 mm</text>
      <biological_entity id="o27915" name="achene" name_original="achenes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s6" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry sandy soil of thickets, pinelands, and disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry sandy soil" constraint="of thickets , pinelands , and disturbed areas" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex., Va.; West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10b.</number>
  <discussion>Scleria ciliata var. elliottii is practically restricted to the Atlantic and Gulf coastal plains.</discussion>
  
</bio:treatment>