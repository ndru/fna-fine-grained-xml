<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="A. Gray" date="1835" rank="species">oligantha</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>3: 212. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species oligantha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357912</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, densely cespitose, knottybased, 20–40 cm;</text>
      <biological_entity id="o29780" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="knottybased" value_original="knottybased" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o29781" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms filiform, leafy at base, wiry.</text>
      <biological_entity id="o29782" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
        <character constraint="at base" constraintid="o29783" is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o29783" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending to erect;</text>
      <biological_entity id="o29784" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades filiform, nearly terete, or channeled, sometimes compressed, nearly reaching distal inflorescence or much shorter, 0.2–0.3 mm thick, apex subulate.</text>
      <biological_entity id="o29785" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="much" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o29786" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <biological_entity id="o29787" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o29785" id="r3678" modifier="nearly" name="reaching" negation="false" src="d0_s4" to="o29786" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: spikelet clusters 2–6, simple or reduced to 1 spikelet, branches ascending to divaricate or reflexed;</text>
      <biological_entity id="o29788" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o29789" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character constraint="to spikelet" constraintid="o29790" is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o29790" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29791" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leafy bracts single per cluster, filiform, setaceous, with clusters appearing lateral to bracts.</text>
      <biological_entity id="o29792" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o29793" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character constraint="per cluster , filiform , setaceous , with clusters" is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o29794" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="lateral" value_original="lateral" />
      </biological_entity>
      <relation from="o29793" id="r3679" name="appearing" negation="false" src="d0_s6" to="o29794" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets pale redbrown, ellipsoidlanceoloid, 5–6 (–8) mm, apex acute to acuminate;</text>
      <biological_entity id="o29795" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale redbrown" value_original="pale redbrown" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29796" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile scales oblongelliptic, convex, acuminate, 3.5–5 mm, apex broadly acute, midrib forming apiculus.</text>
      <biological_entity id="o29797" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29798" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o29799" name="midrib" name_original="midrib" src="d0_s8" type="structure" />
      <biological_entity id="o29800" name="apiculu" name_original="apiculus" src="d0_s8" type="structure" />
      <relation from="o29799" id="r3680" name="forming" negation="false" src="d0_s8" to="o29800" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles 6, reaching to or slightly past tubercle base, increasingly plumose from middle to base.</text>
      <biological_entity id="o29801" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o29802" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character char_type="range_value" from="reaching" name="position_relational" src="d0_s9" to="or slightly past" />
        <character constraint="from middle" constraintid="o29804" is_modifier="false" modifier="increasingly" name="shape" notes="" src="d0_s9" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="tubercle" id="o29803" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o29804" name="middle" name_original="middle" src="d0_s9" type="structure" />
      <biological_entity id="o29805" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o29804" id="r3681" name="to" negation="false" src="d0_s9" to="o29805" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits 1–3 per spikelet, (2.5–) 2.7–3 (–3.4) mm;</text>
      <biological_entity id="o29806" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o29807" from="1" name="quantity" src="d0_s10" to="3" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s10" to="2.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s10" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29807" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>body light-brown to brown, ellipsoidobovoid, distally conspicuously necked, tumidly lenticular, 1.7–2.5 × 1.5–1.8 mm;</text>
      <biological_entity id="o29808" name="body" name_original="body" src="d0_s11" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s11" to="brown" />
        <character is_modifier="false" modifier="distally conspicuously" name="architecture" src="d0_s11" value="necked" value_original="necked" />
        <character is_modifier="false" modifier="tumidly" name="shape" src="d0_s11" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>surfaces smooth or minutely transversely rugulose;</text>
      <biological_entity id="o29809" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely transversely" name="relief" src="d0_s12" value="rugulose" value_original="rugulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tubercle conicsubulate, 0.5–0.7 mm, base flaring.</text>
      <biological_entity id="o29810" name="tubercle" name_original="tubercle" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29811" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="flaring" value_original="flaring" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of bogs, depressions in savannas, open pinelands, seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of bogs , depressions in savannas , open pinelands , seeps" />
        <character name="habitat" value="peats" constraint="of bogs , depressions in savannas , open pinelands , seeps" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="depressions" constraint="in savannas , open pinelands , seeps" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="open pinelands" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., D.C., Fla., Ga., La., Miss., N.J., N.C., S.C., Tex., Va.; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Rhynchospora oligantha is distinguished from other taxa of its complex mostly by the distinctive neck at the achene apex, a feature essentially absent in R. breviseta, its closest relative. Those two species have been heavily impacted by conversion of pine savannas to cropland or pine plantations; even with abandonment or clearing of such land, they are very slow to reoccupy the disturbed sites.</discussion>
  
</bio:treatment>