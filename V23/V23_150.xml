<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="(Nees) Bentham in G. Bentham and J. D. Hooker" date="unknown" rank="section">eleogenus</taxon_name>
    <taxon_name authority="Svenson" date="1929" rank="series">maculosae</taxon_name>
    <taxon_name authority="(Poiret) Urban" date="1903" rank="species">flavescens</taxon_name>
    <taxon_name authority="(Torrey) Gleason" date="1952" rank="variety">olivacea</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>4: 22. 1952</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleogenus;series maculosae;species flavescens;variety olivacea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357758</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">olivacea</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>3: 300. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eleocharis;species olivacea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">olivacea</taxon_name>
    <taxon_name authority="(Schuyler &amp; Ferren) Schuyler &amp; Ferren" date="unknown" rank="variety">reductiseta</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species olivacea;variety reductiseta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 28 cm.</text>
      <biological_entity id="o7479" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="28" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Floral scales brown, streaked with green on flanks.</text>
      <biological_entity constraint="floral" id="o7480" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="streaked" value_original="streaked" />
        <character constraint="on flanks" constraintid="o7481" is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o7481" name="flank" name_original="flanks" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers: perianth bristles to 2 times as long as achene, rarely shorter than achene.</text>
      <biological_entity id="o7482" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="perianth" id="o7483" name="bristle" name_original="bristles" src="d0_s2" type="structure">
        <character constraint="achene" constraintid="o7484" is_modifier="false" name="length" src="d0_s2" value="0-2 times as long as achene" />
        <character constraint="than achene" constraintid="o7485" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="rarely shorter" value_original="rarely shorter" />
      </biological_entity>
      <biological_entity id="o7484" name="achene" name_original="achene" src="d0_s2" type="structure" />
      <biological_entity id="o7485" name="achene" name_original="achene" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Achenes green to golden brown when ripe, 0.5–1.1 × 0.4–0.8 mm, apex often highly constricted proximal to tubercle.</text>
      <biological_entity id="o7486" name="achene" name_original="achenes" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" modifier="when ripe" name="coloration" src="d0_s3" to="golden brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s3" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7487" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often highly" name="size" src="d0_s3" value="constricted" value_original="constricted" />
        <character constraint="to tubercle" constraintid="o7488" is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o7488" name="tubercle" name_original="tubercle" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Tubercles 0.3–0.7 × 0.2–0.4 mm. 2n = 20.</text>
      <biological_entity id="o7489" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s4" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7490" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–winter (Jun–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="winter" from="summer" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs, cold springs, dry stream banks, lake and pond margins, maritime mud flats, marshes, moist meadows, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="cold springs" />
        <character name="habitat" value="dry stream banks" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="mud flats" modifier="maritime" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="maritime" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ark., Conn., Del., Fla., Ga., Ill., Ind., La., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tex., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39b.</number>
  <other_name type="common_name">Green spikerush</other_name>
  <other_name type="common_name">éléocharide olivâtre</other_name>
  <discussion>Eleocharis flavescens var. olivacea was reported from Hawaii; I have not seen a voucher specimen.</discussion>
  
</bio:treatment>