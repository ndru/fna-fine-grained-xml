<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jeremy J. Bruhl</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
    <other_info_on_meta type="illustrator">Amanda Humphrey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Nees in C. F. P. von Martius et al." date="1842" rank="genus">Oxycaryum</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. P. von Martius et al., Fl. Brasil.</publication_title>
      <place_in_publication>2(1): 90. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus Oxycaryum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oxys, sharp, and carya, nut</other_info_on_name>
    <other_info_on_name type="fna_id">123480</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, not cespitose, stoloniferous, aquatic.</text>
      <biological_entity id="o28245" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary, trigonous.</text>
      <biological_entity id="o28246" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o28247" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules present, ciliate;</text>
      <biological_entity id="o28248" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades flat to V-shaped in cross-section, prominently keeled on abaxial surface.</text>
      <biological_entity id="o28249" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in cross-section" constraintid="o28250" from="flat" name="shape" src="d0_s4" to="v-shaped" />
        <character constraint="on abaxial surface" constraintid="o28251" is_modifier="false" modifier="prominently" name="shape" notes="" src="d0_s4" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o28250" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o28251" name="surface" name_original="surface" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, umbellate heads or capitate;</text>
      <biological_entity id="o28252" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o28253" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucral-bracts 1–6+, spreading, leaflike.</text>
      <biological_entity id="o28254" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="6" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets: scales 5–10+, 3-ranked, spirally arranged, each subtending flower.</text>
      <biological_entity id="o28255" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o28256" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="10" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="3-ranked" value_original="3-ranked" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o28257" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <relation from="o28256" id="r3483" name="subtending" negation="false" src="d0_s7" to="o28257" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o28258" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth absent;</text>
      <biological_entity id="o28259" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3;</text>
      <biological_entity id="o28260" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 2-fid, linear, base persistent.</text>
      <biological_entity id="o28261" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o28262" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes planoconvex, margins and apex corky.</text>
      <biological_entity id="o28263" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity id="o28264" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity id="o28265" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical America and Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical America and Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Lye, K. A. 1971c. Studies in African Cyperaceae: 2. The genus Oxycaryum Nees. Bot. Not. 124: 281–286.</reference>
  </references>
  
</bio:treatment>