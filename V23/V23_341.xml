<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. TuckerTucker, Gordon C.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">195</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown in J. H. Tuckey" date="1818" rank="genus">LIPOCARPHA</taxon_name>
    <place_of_publication>
      <publication_title>in J. H. Tuckey, Narr. Exped. Zaire,</publication_title>
      <place_in_publication>459. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus LIPOCARPHA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, leipo, to fall, and carpha, chaff, referring to deciduous transparent inner secondary scale of the spikelet in many species</other_info_on_name>
    <other_info_on_name type="fna_id">118685</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nees" date="unknown" rank="genus">Hemicarpha</taxon_name>
    <taxon_hierarchy>genus Hemicarpha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual [rarely perennial], usually densely cespitose, not rhizomatous.</text>
      <biological_entity id="o14945" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="usually densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or not, terete.</text>
      <biological_entity id="o14946" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, proximal bladeless or with involute appendage, distal with distinct blade, glabrous;</text>
      <biological_entity id="o14947" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14948" name="culm" name_original="culm" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="bladeless" value_original="bladeless" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with involute appendage" value_original="with involute appendage" />
      </biological_entity>
      <biological_entity id="o14949" name="appendage" name_original="appendage" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s2" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14950" name="culm" name_original="culm" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14951" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o14948" id="r1839" name="with" negation="false" src="d0_s2" to="o14949" />
      <relation from="o14950" id="r1840" name="with" negation="false" src="d0_s2" to="o14951" />
    </statement>
    <statement id="d0_s3">
      <text>ligules absent;</text>
      <biological_entity id="o14952" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades setaceous to involute or flat.</text>
      <biological_entity id="o14953" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="setaceous" name="shape" src="d0_s4" to="involute or flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, sometimes pseudolateral, ovoid clusters of spikes, spikes 1–4;</text>
      <biological_entity id="o14954" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="pseudolateral" value_original="pseudolateral" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character constraint="of spikes" constraintid="o14955" is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o14955" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <biological_entity id="o14956" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikelets [20–] 50–150 per spike;</text>
      <biological_entity id="o14957" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s6" to="50" to_inclusive="false" />
        <character char_type="range_value" constraint="per spike" constraintid="o14958" from="50" name="quantity" src="d0_s6" to="150" />
      </biological_entity>
      <biological_entity id="o14958" name="spike" name_original="spike" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucral-bracts 1–4, spreading or erect, leaflike.</text>
      <biological_entity id="o14959" name="involucral-bract" name_original="involucral-bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets: scales (1–) 3, spirally arranged, 1 scale subtending flower, others empty.</text>
      <biological_entity id="o14960" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o14961" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14962" name="scale" name_original="scale" src="d0_s8" type="structure" />
      <biological_entity id="o14963" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o14964" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="empty" value_original="empty" />
      </biological_entity>
      <relation from="o14962" id="r1841" name="subtending" negation="false" src="d0_s8" to="o14963" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o14965" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth scales 1–2;</text>
      <biological_entity constraint="perianth" id="o14966" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 1–2;</text>
      <biological_entity id="o14967" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles linear, 2–3-fid, base deciduous or persistent, thickened or not.</text>
      <biological_entity id="o14968" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-3-fid" value_original="2-3-fid" />
      </biological_entity>
      <biological_entity id="o14969" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
        <character name="size_or_width" src="d0_s12" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous or terete.</text>
      <biological_entity id="o14970" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, pantropical and wet, warm-temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="pantropical and wet" establishment_means="native" />
        <character name="distribution" value="warm-temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Lipocarphe</other_name>
  <discussion>Species ca. 35 (6 in the flora).</discussion>
  <discussion>The morphology of the inflorescence has received various interpretations. One suggested that the inner transparent scale represents five perianth members that correspond to the bristles subtending the achenes in some species of Scirpus (S. Friedland 1941). Another proposed that Lipocarpha is a highly reduced derivative of a Cyperus-like ancestor (J. Raynal 1967); that view was accepted by G. C. Tucker (1987) and P. Goetghebeur and A. Van den Borre (1989). The shared feature of Kranz anatomy of the “Cyperus type” further strengthens Raynal’s proposal. The achene and subtending scales of Lipocarpha are thus homologous to a single spikelet of Kyllinga or Cyperus. The interpretation of the secondary scales of Lipocarpha (Hemicarpha) as reduced scales of a spikelet (J. Raynal 1967) appears more plausible than the view of the secondary scale as fused perianth members (S. Friedland 1941).</discussion>
  <discussion>Lipocarpha drummondii and L. aristulata have been frequently confused and misidentified as each other or as L. micrantha. Reports of those species, or their synonyms, should be confirmed by examining supporting specimens. Lipocarpha squarrosa (Linnaeus) Goetghebeur, a native of southern Asia, is known as an adventive from the Gulf Coast of Florida; it is not included in the species count. It has subtrigonous achenes and three stigmas, and it would seem to key to L. maculata in the following key; however, it is a smaller plant, at most 20 cm tall, with anthers only 0.15–0.2 mm and achenes 0.4–0.55 mm.</discussion>
  <references>
    <reference>Friedland, S. 1941. The American species of Hemicarpha. Amer. J. Bot. 28: 855–861.</reference>
    <reference>Goetghebeur, P. and A. Van den Borre. 1989. Studies in Cyperaceae: 8. A revision of Lipocarpha, including Hemicarpha and Rikliella. Wageningen Agric. Univ. Pap. 89(1): 1–87.</reference>
    <reference>Koyama, T. 1982. The genus Lipocarpha R. Br., its morphology and systematic position in the family Cyperaceae. Acta Phytotax. Geobot. 33: 218–226.</reference>
    <reference>Raynal, J. 1967. Notes cypérologiques: 7. Sur quelques Lipocarpha Africains. Adansonia, n.s. 7: 81–87.</reference>
    <reference>Tucker, G. C. 1993. Lipocarpha. In: J. C. Hickman, ed. 1993. The Jepson Manual. Higher Plants of California. Berkeley, Los Angeles, and London. P. 1144.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikes 2.5–10(–12) mm; anthers 0.5 mm; stigmas 3.</description>
      <determination>6 Lipocarpha maculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikes 1–5(–8) mm; anthers 0.10–0.25 mm; stigmas 2.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Culms 7–35 cm; longest involucral bract spreading to reflexed; achenes 3.5–5 times long as wide; Alabama, Florida.</description>
      <determination>5 Lipocarpha microcephala</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Culms 1–10(–20) cm; longest involucral bract ± erect; achenes 1.5–2.5 times long as wide; widespread.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer (1st) scale widest proximal to mid length; spike with bristly appearance; Pacific Coast states.</description>
      <determination>4 Lipocarpha occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Outer (1st) scale widest at or distal to mid length; spike not bristly; widespread.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inner (2d) scale 0.1–0.2 mm or absent.</description>
      <determination>3 Lipocarpha micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Outer (1st) scale widest at mid length; 2d scale veinless (or occasionally 2–4 reddish veins).</description>
      <determination>1 Lipocarpha aristulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Outer (1st) scale widest distal to mid length; 2d scale reddish brown distally.</description>
      <determination>2 Lipocarpha drummondii</determination>
    </key_statement>
  </key>
</bio:treatment>