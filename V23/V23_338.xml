<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottbøll" date="1773" rank="genus">kyllinga</taxon_name>
    <taxon_name authority="Vahl" date="1805" rank="species">odorata</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>2: 382. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus kyllinga;species odorata</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357849</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Torrey) Mattfeld &amp; Kükenthal" date="unknown" rank="species">sesquiflorus</taxon_name>
    <taxon_hierarchy>genus Cyperus;species sesquiflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kyllinga</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">sesquiflora</taxon_name>
    <taxon_hierarchy>genus Kyllinga;species sesquiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, densely cespitose, rhizomatous.</text>
      <biological_entity id="o19904" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (5–) 10–25 (–45) cm, smooth.</text>
      <biological_entity id="o19905" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat to slightly folded, (6–) 10–20 (–30) cm × 2–3 (–4) mm.</text>
      <biological_entity id="o19906" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="slightly folded" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_length" src="d0_s2" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes 1–3 (–4), whitish, ovoid to ellipsoid, 6–12 (–18) × 4–8 mm;</text>
      <biological_entity id="o19907" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o19908" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s3" to="ellipsoid" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts (2–) 3–4, horizontal, flat, (2–) 3–4, 1–7 (–9) cm × (0.5–) 1–3 (–4.5) mm.</text>
      <biological_entity id="o19909" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o19910" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s4" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets (50–) 75–150, ovate to ovatelanceolate, (1.8–) 2.3–2.8 (–3) × 1–1.3 mm;</text>
      <biological_entity id="o19911" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s5" to="75" to_inclusive="false" />
        <character char_type="range_value" from="75" name="quantity" src="d0_s5" to="150" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovatelanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_length" src="d0_s5" to="2.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s5" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral scales whitish, laterally 2–3-veined, broadly ovate, 2–2.5 × (1.3–) 1.8–2.6 mm;</text>
      <biological_entity constraint="floral" id="o19912" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s6" value="2-3-veined" value_original="2-3-veined" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_width" src="d0_s6" to="1.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 2;</text>
      <biological_entity id="o19913" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers (0.4–) 0.6–0.8 (–1) mm;</text>
      <biological_entity id="o19914" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 0.6–1 mm;</text>
      <biological_entity id="o19915" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 0.3–0.6 mm.</text>
      <biological_entity id="o19916" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Achenes reddish-brown to dark-brown, broadly ellipsoid, 1.2–1.5 × 0.7–0.8 (–0.9) mm, base whitish, stipe to 0.1 mm, apex obtuse, apiculate, puncticulate.</text>
      <biological_entity id="o19917" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s11" to="dark-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19918" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o19919" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19920" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s11" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" modifier="damp" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.Mex., N.C., Okla., S.C., Tex.; Mexico; West Indies; Central America; South America; Asia; Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Whitehead sedge</other_name>
  
</bio:treatment>