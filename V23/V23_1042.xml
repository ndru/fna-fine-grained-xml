<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Debra A. Dunlop</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="(Tuckerman) Kükenthal in H. G. A. Engler" date="1909" rank="section">Scirpinae</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>20[IV,38]: 81. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Scirpinae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302733</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="Tuckerman" date="unknown" rank="unranked">Scirpinae</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Meth. Caric.,</publication_title>
      <place_in_publication>8. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carex;unranked Scirpinae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Pax" date="unknown" rank="section">Scirpoideae</taxon_name>
    <taxon_hierarchy>section Scirpoideae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="V. I. Kreczetowicz" date="unknown" rank="section">Trysanolepis</taxon_name>
    <taxon_hierarchy>section Trysanolepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually cespitose, short to long rhizomatous, sometimes inconspicuously rhizomatous.</text>
      <biological_entity id="o23486" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes inconspicuously" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms redbrown at base.</text>
      <biological_entity id="o23487" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o23488" is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o23488" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths fibrous or not, persistent sheaths usually absent;</text>
      <biological_entity id="o23489" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o23490" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character name="texture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o23491" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous, puberulent;</text>
      <biological_entity id="o23492" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o23493" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, glabrous or puberulent.</text>
      <biological_entity id="o23494" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23495" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o23496" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o23496" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence usually 1 spike, rarely short second spike;</text>
      <biological_entity id="o23498" name="spike" name_original="spike" src="d0_s5" type="structure" />
      <biological_entity id="o23499" name="spike" name_original="spike" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bractless or bract filiform, sheathless, prophyllate;</text>
      <biological_entity id="o23497" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" modifier="rarely" name="height_or_length_or_size" notes="" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bractless" value_original="bractless" />
      </biological_entity>
      <biological_entity id="o23500" name="bract" name_original="bract" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sheathless" value_original="sheathless" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes unisexual, mostly staminate and pistillate spikes on different plants.</text>
      <biological_entity id="o23501" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23502" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23503" name="plant" name_original="plants" src="d0_s7" type="structure" />
      <relation from="o23502" id="r2868" name="on" negation="false" src="d0_s7" to="o23503" />
    </statement>
    <statement id="d0_s8">
      <text>Proximal pistillate scales with apex obtuse to acute, ciliate.</text>
      <biological_entity constraint="proximal" id="o23504" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o23505" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <relation from="o23504" id="r2869" name="with" negation="false" src="d0_s8" to="o23505" />
    </statement>
    <statement id="d0_s9">
      <text>Perigynia erect, veinless or obscurely veined, with 2 prominent marginal veins, stipitate, lanceolate to ovate or obovate, rounded-trigonous in cross-section, less than 10 mm, base tapering or rounded, apex tapering or rounded to beak, pubescent;</text>
      <biological_entity id="o23506" name="perigynium" name_original="perigynia" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="veinless" value_original="veinless" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="ovate or obovate" />
        <character constraint="in cross-section" constraintid="o23508" is_modifier="false" name="shape" src="d0_s9" value="rounded-trigonous" value_original="rounded-trigonous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o23507" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o23508" name="cross-section" name_original="cross-section" src="d0_s9" type="structure" />
      <biological_entity id="o23509" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o23510" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character constraint="to beak" constraintid="o23511" is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23511" name="beak" name_original="beak" src="d0_s9" type="structure" />
      <relation from="o23506" id="r2870" name="with" negation="false" src="d0_s9" to="o23507" />
    </statement>
    <statement id="d0_s10">
      <text>beak 0.1–0.5 mm, emarginate or shortly bidentate, teeth less than 0.8 mm.</text>
      <biological_entity id="o23512" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s10" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o23513" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas (2–) 3 (–4).</text>
      <biological_entity id="o23514" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes usually trigonous, smaller than bodies of perigynia;</text>
      <biological_entity id="o23515" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
        <character constraint="than bodies" constraintid="o23516" is_modifier="false" name="size" src="d0_s12" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o23516" name="body" name_original="bodies" src="d0_s12" type="structure" />
      <biological_entity id="o23517" name="perigynium" name_original="perigynia" src="d0_s12" type="structure" />
      <relation from="o23516" id="r2871" name="part_of" negation="false" src="d0_s12" to="o23517" />
    </statement>
    <statement id="d0_s13">
      <text>style deciduous.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 29, 31.</text>
      <biological_entity id="o23518" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="x" id="o23519" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="29" value_original="29" />
        <character name="quantity" src="d0_s14" value="31" value_original="31" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w and n North America, Europe (Norway), and Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w and n North America" establishment_means="native" />
        <character name="distribution" value="Europe (Norway)" establishment_means="native" />
        <character name="distribution" value="and Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26eee.</number>
  <discussion>Species 3 (3 in the flora).</discussion>
  <references>
    <reference>Dunlop, D. A. and G. E. Crow. (1999). The taxonomy of Carex section Scirpinae (Cyperaceae). Rhodora 101: 163–199.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Culms 5–35(–40) cm; achenes tightly enveloped by perigynia, occupying full width and at least 3/4 length of perigynia; North America, Europe, Asia.</description>
      <determination>448 Carex scirpoidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Culms (25–)35–91 cm; achenes loosely enveloped by perigynia, occupying 1/3–2/3 width and 1/3–3/4 length of perigynia; restricted to North America.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perigynia tawny to red-brown, distal 3/4 hirsute; leaves sparsely pilose adaxially at 20X; n Arizona and s Utah.</description>
      <determination>449 Carex curatorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perigynia purple-black, distal 1/3 sparsely hairy; leaves glabrous adaxially; sw Oregon and nw California.</description>
      <determination>450 Carex scabriuscula</determination>
    </key_statement>
  </key>
</bio:treatment>