<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="Fernald" date="1906" rank="species">nitida</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>8: 129. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species nitida;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357772</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mat-forming;</text>
      <biological_entity id="o1634" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes evident, 0.3–0.5 mm thick, hard, cortex persistent, longer internodes 2 mm, scales persistent or fugaceous, 2–3 mm, membranous to papery, slightly fibrous.</text>
      <biological_entity id="o1635" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="thickness" src="d0_s1" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o1636" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="longer" id="o1637" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1638" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s1" value="fugaceous" value_original="fugaceous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s1" to="papery" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms terete, 2–15 cm × 0.15–0.3 mm, firm to soft.</text>
      <biological_entity id="o1639" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="width" src="d0_s2" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="firm" name="texture" src="d0_s2" to="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, not splitting, proximally stramineous to reddish, distally green or stramineous to reddish, membranous, apex often red, obtuse to acute, tooth absent.</text>
      <biological_entity id="o1640" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o1641" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character char_type="range_value" from="proximally stramineous" name="coloration" src="d0_s3" to="reddish distally green or stramineous" />
        <character char_type="range_value" from="proximally stramineous" name="coloration" src="d0_s3" to="reddish distally green or stramineous" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o1642" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o1643" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets ovoid, 1–4 × 1–2 mm, obtuse to acute;</text>
      <biological_entity id="o1644" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale amplexicaulous, apex entire;</text>
      <biological_entity constraint="proximal" id="o1645" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="amplexicaulous" value_original="amplexicaulous" />
      </biological_entity>
      <biological_entity id="o1646" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale with flower;</text>
      <biological_entity constraint="subproximal" id="o1647" name="scale" name_original="scale" src="d0_s6" type="structure" />
      <biological_entity id="o1648" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <relation from="o1647" id="r205" name="with" negation="false" src="d0_s6" to="o1648" />
    </statement>
    <statement id="d0_s7">
      <text>floral scales spreading in fruit, 5–30, 8 per mm of rachilla, medium to very dark-brown, midrib region often pale or greenish, broadly ovate, 1–1.3 × 1 mm, apex rounded, entire, not carinate.</text>
      <biological_entity constraint="floral" id="o1649" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o1650" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="medium" value_original="medium" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s7" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o1650" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="30" />
        <character constraint="per mm" constraintid="o1651" name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o1651" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o1652" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o1653" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="1.3" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1654" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <relation from="o1651" id="r206" name="part_of" negation="false" src="d0_s7" to="o1652" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles absent;</text>
      <biological_entity id="o1655" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o1656" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o1657" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1658" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow, 0.3 mm;</text>
      <biological_entity id="o1659" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1660" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid.</text>
      <biological_entity id="o1661" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1662" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes persistent after scales fall, to dark yellow-orange or brown, broadly obpyriform, trigonous, angles evident, 0.6–0.65 × 0.5–0.55 mm, rugulose at 20–30X, 20 blunt horizontal ridges in each vertical series.</text>
      <biological_entity id="o1663" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character constraint="after scales" constraintid="o1664" is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" notes="" src="d0_s12" to="0.65" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s12" to="0.55" to_unit="mm" />
        <character constraint="at ridges" constraintid="o1666" is_modifier="false" name="relief" src="d0_s12" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o1664" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o1665" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="dark yellow-orange" value_original="dark yellow-orange" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s12" value="obpyriform" value_original="obpyriform" />
        <character is_modifier="true" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o1666" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="20" value_original="20" />
        <character is_modifier="true" name="shape" src="d0_s12" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o1667" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o1663" id="r207" name="to" negation="false" src="d0_s12" to="o1665" />
      <relation from="o1666" id="r208" name="in" negation="false" src="d0_s12" to="o1667" />
    </statement>
    <statement id="d0_s13">
      <text>Tubercles brown, greatly depressed, rudimentary, 0.05–15 × 0.15–0.3 mm.</text>
      <biological_entity id="o1668" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="greatly" name="shape" src="d0_s13" value="depressed" value_original="depressed" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="length" src="d0_s13" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring (Jun)–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late spring" from="late spring" constraint=" -summer" />
        <character name="fruiting time" char_type="atypical_range" to="Jun" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh bog pools, streams, disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh bog pools" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr., N.S., Ont., Que., Sask.; Alaska, Mich., Minn., N.H., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Neat spike-rush</other_name>
  <other_name type="common_name">éléocharide brillante</other_name>
  <discussion>Eleocharis nitida is much like E. elliptica but all structures are smaller; intermediates are unknown.</discussion>
  
</bio:treatment>