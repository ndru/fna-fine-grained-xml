<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Ehrhart" date="1790" rank="species">pubescens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">pubescens</taxon_name>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species pubescens;subspecies pubescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500263</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, narrow, to 20 m;</text>
      <biological_entity id="o19772" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s0" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk usually 1, branches ascending or spreading.</text>
      <biological_entity id="o19773" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o19774" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs usually without conspicuous resinous glands.</text>
      <biological_entity id="o19775" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <biological_entity id="o19776" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o19775" id="r2665" name="without" negation="false" src="d0_s2" to="o19776" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade broadly ovate to rhomic-ovate, 3–4 (–6) × 2–4 (–6) cm, base rounded, truncate, or cuneate, margins finely to coarsely toothed or dentate, apex acute;</text>
      <biological_entity id="o19777" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="rhomic-ovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19778" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19779" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19780" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially moderately pubescent to velutinous, especially along major veins and in vein-axils.</text>
      <biological_entity id="o19782" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o19783" name="vein-axil" name_original="vein-axils" src="d0_s4" type="structure" />
      <relation from="o19781" id="r2666" modifier="especially" name="along" negation="false" src="d0_s4" to="o19782" />
      <relation from="o19781" id="r2667" name="in" negation="false" src="d0_s4" to="o19783" />
    </statement>
    <statement id="d0_s5">
      <text>Fruiting catkins 2–3 × 0.8–1.2 cm;</text>
      <biological_entity id="o19781" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="abaxially moderately pubescent" name="pubescence" src="d0_s4" to="velutinous" />
      </biological_entity>
      <biological_entity id="o19784" name="catkin" name_original="catkins" src="d0_s5" type="structure" />
      <relation from="o19781" id="r2668" name="fruiting" negation="false" src="d0_s5" to="o19784" />
    </statement>
    <statement id="d0_s6">
      <text>scales puberulent and ciliate, lobes diverging at middle, central lobes ovate to delatate, apex acute to obtuse, lateral lobes divergent, about equal in length, several times broader.</text>
      <biological_entity id="o19785" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19786" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character constraint="at middle lobes" constraintid="o19787" is_modifier="false" name="orientation" src="d0_s6" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity constraint="middle" id="o19787" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <biological_entity constraint="central" id="o19788" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character constraint="to delatate" constraintid="o19789" is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o19789" name="delatate" name_original="delatate" src="d0_s6" type="structure" />
      <biological_entity id="o19790" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19791" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="length" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="several" value_original="several" />
        <character is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Samaras with wings somewhat broader than body, broadest near summit, extended beyond body apically.</text>
      <biological_entity id="o19793" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character constraint="than body" constraintid="o19794" is_modifier="false" name="width" src="d0_s7" value="somewhat broader" value_original="somewhat broader" />
      </biological_entity>
      <biological_entity id="o19794" name="body" name_original="body" src="d0_s7" type="structure" />
      <biological_entity id="o19795" name="summit" name_original="summit" src="d0_s7" type="structure" />
      <biological_entity id="o19796" name="body" name_original="body" src="d0_s7" type="structure" />
      <relation from="o19792" id="r2669" name="with" negation="false" src="d0_s7" to="o19793" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 56.</text>
      <biological_entity id="o19792" name="samara" name_original="samaras" src="d0_s7" type="structure">
        <character constraint="than body" constraintid="o19794" is_modifier="false" name="width" src="d0_s7" value="somewhat broader" value_original="somewhat broader" />
        <character constraint="near summit" constraintid="o19795" is_modifier="false" name="width" src="d0_s7" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o19796" is_modifier="false" name="size" notes="" src="d0_s7" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19797" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, scrubs, heaths, and open woods where native, elsewhere in abandoned plantings, moist open roadsides, swales, swampy thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="scrubs" />
        <character name="habitat" value="heaths" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="abandoned plantings" />
        <character name="habitat" value="moist open roadsides" />
        <character name="habitat" value="swales" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Conn., Ind., Maine, Mass., N.H., Ohio, Pa., Vt.; native to Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="native to Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9a.</number>
  <other_name type="common_name">European white birch</other_name>
  <other_name type="common_name">downy birch</other_name>
  <discussion>Betula pubescens subsp. pubescens is commonly cultivated in northeastern North America, where it has sometimes escaped and persisted, or become adventive but not widely naturalized. It is distinguished from other light-barked species in the East by its relatively small leaves, pubescent twigs, and brownish, mostly unpeeling bark.</discussion>
  
</bio:treatment>