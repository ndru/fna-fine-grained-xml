<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kingsley R. Stern</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">FUMARIACEAE</taxon_name>
    <taxon_hierarchy>family FUMARIACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10351</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, scapose or caulescent, from taproots, bulblets, tubers, or rhizomes;</text>
      <biological_entity id="o14722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14723" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <biological_entity id="o14724" name="bulblet" name_original="bulblets" src="d0_s0" type="structure" />
      <biological_entity id="o14725" name="tuber" name_original="tubers" src="d0_s0" type="structure" />
      <biological_entity id="o14726" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o14722" id="r2037" name="from" negation="false" src="d0_s0" to="o14723" />
      <relation from="o14722" id="r2038" name="from" negation="false" src="d0_s0" to="o14724" />
      <relation from="o14722" id="r2039" name="from" negation="false" src="d0_s0" to="o14725" />
      <relation from="o14722" id="r2040" name="from" negation="false" src="d0_s0" to="o14726" />
    </statement>
    <statement id="d0_s1">
      <text>sap clear.</text>
      <biological_entity id="o14727" name="sap" name_original="sap" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="clear" value_original="clear" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems when present leafy, erect to prostrate or climbing, simple or branching.</text>
      <biological_entity id="o14728" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when present" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and/or cauline, alternate, mostly compound, sometimes simple, without stipules, petiolate;</text>
      <biological_entity id="o14729" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o14730" name="stipule" name_original="stipules" src="d0_s3" type="structure" />
      <relation from="o14729" id="r2041" name="without" negation="false" src="d0_s3" to="o14730" />
    </statement>
    <statement id="d0_s4">
      <text>blade with 2-6 odd-pinnate orders of leaflets and/or lobes.</text>
      <biological_entity id="o14731" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o14732" name="order" name_original="orders" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="odd-pinnate" value_original="odd-pinnate" />
      </biological_entity>
      <biological_entity id="o14733" name="leaflet" name_original="leaflets" src="d0_s4" type="structure" />
      <biological_entity id="o14734" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <relation from="o14731" id="r2042" name="with" negation="false" src="d0_s4" to="o14732" />
      <relation from="o14732" id="r2043" name="part_of" negation="false" src="d0_s4" to="o14733" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, axillary, extra-axillary, or leaf-opposed, unifloral or else multifloral and thyrsoid, paniculate, racemose, or corymbose;</text>
      <biological_entity id="o14735" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s5" value="extra-axillary" value_original="extra-axillary" />
        <character is_modifier="false" name="position" src="d0_s5" value="leaf-opposed" value_original="leaf-opposed" />
        <character is_modifier="false" name="position" src="d0_s5" value="extra-axillary" value_original="extra-axillary" />
        <character is_modifier="false" name="position" src="d0_s5" value="leaf-opposed" value_original="leaf-opposed" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="unifloral" value_original="unifloral" />
        <character name="architecture" src="d0_s5" value="else" value_original="else" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="multifloral" value_original="multifloral" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thyrsoid" value_original="thyrsoid" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="corymbose" value_original="corymbose" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles present;</text>
      <biological_entity id="o14736" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o14737" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bilaterally symmetric about 1 plane or each of 2 perpendicular planes;</text>
      <biological_entity id="o14738" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character name="shape" src="d0_s8" value="each" value_original="each" />
      </biological_entity>
      <biological_entity id="o14739" name="plane" name_original="planes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <relation from="o14738" id="r2044" name="consist_of" negation="false" src="d0_s8" to="o14739" />
    </statement>
    <statement id="d0_s9">
      <text>pedicel present;</text>
      <biological_entity id="o14740" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals caducous or persistent, 2, thin;</text>
      <biological_entity id="o14741" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4, distinct or coherent basally to almost completely connate, in 2 whorls of 2;</text>
      <biological_entity id="o14742" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="coherent" value_original="coherent" />
        <character is_modifier="false" modifier="basally to almost completely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o14743" name="whorl" name_original="whorls" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o14742" id="r2045" name="in" negation="false" src="d0_s11" to="o14743" />
    </statement>
    <statement id="d0_s12">
      <text>outer petals alike or dissimilar, 1 or both sometimes swollen or spurred basally;</text>
      <biological_entity constraint="outer" id="o14744" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="alike" value_original="alike" />
        <character name="variability" src="d0_s12" value="dissimilar" value_original="dissimilar" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character name="quantity" src="d0_s12" value="both" value_original="both" />
        <character is_modifier="false" name="shape" src="d0_s12" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s12" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner petals alike, apically connate, clawed, with somewhat hollow, membranous, wrinkled, abaxial median crests;</text>
      <biological_entity constraint="inner" id="o14745" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="alike" value_original="alike" />
        <character is_modifier="false" modifier="apically" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity constraint="abaxial median" id="o14746" name="crest" name_original="crests" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="somewhat" name="architecture" src="d0_s13" value="hollow" value_original="hollow" />
        <character is_modifier="true" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="relief" src="d0_s13" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <relation from="o14745" id="r2046" name="with" negation="false" src="d0_s13" to="o14746" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 6, in 2 bundles of 3 each, opposite outer petals;</text>
      <biological_entity id="o14747" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o14748" name="bundle" name_original="bundles" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o14749" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="true" name="arrangement" src="d0_s14" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o14747" id="r2047" name="in" negation="false" src="d0_s14" to="o14748" />
      <relation from="o14748" id="r2048" name="part_of" negation="false" src="d0_s14" to="o14749" />
    </statement>
    <statement id="d0_s15">
      <text>filaments of each bundle partially to completely connate, sometimes basally adnate to petals, with basal nectariferous tissue often in form of spur;</text>
      <biological_entity id="o14750" name="filament" name_original="filaments" src="d0_s15" type="structure" constraint="bundle" constraint_original="bundle; bundle">
        <character is_modifier="false" modifier="partially to completely" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character constraint="to petals" constraintid="o14752" is_modifier="false" modifier="sometimes basally" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o14751" name="bundle" name_original="bundle" src="d0_s15" type="structure" />
      <biological_entity id="o14752" name="petal" name_original="petals" src="d0_s15" type="structure" />
      <biological_entity constraint="basal nectariferous" id="o14753" name="tissue" name_original="tissue" src="d0_s15" type="structure" />
      <biological_entity id="o14754" name="form" name_original="form" src="d0_s15" type="structure" />
      <biological_entity id="o14755" name="spur" name_original="spur" src="d0_s15" type="structure" />
      <relation from="o14750" id="r2049" name="part_of" negation="false" src="d0_s15" to="o14751" />
      <relation from="o14750" id="r2050" name="with" negation="false" src="d0_s15" to="o14753" />
      <relation from="o14753" id="r2051" name="in" negation="false" src="d0_s15" to="o14754" />
      <relation from="o14754" id="r2052" name="part_of" negation="false" src="d0_s15" to="o14755" />
    </statement>
    <statement id="d0_s16">
      <text>anthers connivent, adhering to stigma, median anthers 2-locular, lateral anthers 1-locular;</text>
      <biological_entity id="o14756" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o14757" name="stigma" name_original="stigma" src="d0_s16" type="structure" />
      <biological_entity constraint="median" id="o14758" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="2-locular" value_original="2-locular" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14759" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <relation from="o14756" id="r2053" name="adhering to" negation="false" src="d0_s16" to="o14757" />
    </statement>
    <statement id="d0_s17">
      <text>pistil 1, 2-carpellate;</text>
      <biological_entity id="o14760" name="pistil" name_original="pistil" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary 1-locular;</text>
      <biological_entity id="o14761" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s18" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>placentae parietal;</text>
      <biological_entity id="o14762" name="placenta" name_original="placentae" src="d0_s19" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s19" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>style threadlike, rigid;</text>
      <biological_entity id="o14763" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="thread-like" value_original="threadlike" />
        <character is_modifier="false" name="texture" src="d0_s20" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma 1, compressed, with 2 lobes or apical horns, and/or 2-8 papillar stigmatic surfaces.</text>
      <biological_entity id="o14764" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s21" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o14765" name="lobe" name_original="lobes" src="d0_s21" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="apical" id="o14766" name="horn" name_original="horns" src="d0_s21" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s21" to="8" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o14767" name="surface" name_original="surfaces" src="d0_s21" type="structure">
        <character is_modifier="true" name="shape" src="d0_s21" value="papillar" value_original="papillar" />
      </biological_entity>
      <relation from="o14764" id="r2054" name="with" negation="false" src="d0_s21" to="o14765" />
      <relation from="o14764" id="r2055" name="with" negation="false" src="d0_s21" to="o14766" />
    </statement>
    <statement id="d0_s22">
      <text>Fruits capsular, indehiscent or dehiscent and valvate.</text>
      <biological_entity id="o14768" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s22" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s22" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s22" value="valvate" value_original="valvate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds 1-many, small, elaiosome (oil-bearing appendage) often present.</text>
      <biological_entity id="o14769" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s23" to="many" />
        <character is_modifier="false" name="size" src="d0_s23" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o14770" name="elaiosome" name_original="elaiosome" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Fumitory Family</other_name>
  <discussion>Genera 19, species ca. 450 (4 genera, 23 species in the flora).</discussion>
  <discussion>The genera of Fumariaceae are distributed mostly in the Old World and primarily in temperate Eurasia. One acaulescent species of Dicentra occurs in Siberia, Kamchatka, and Japan; a caulescent species is found in western China and northern Burma; and nine climbing species are distributed throughout the Himalayan area and Burma. More than 400 taxa of Corydalis and 50 of Fumaria, distributed primarily throughout temperate, often montane, regions of Eurasia and Africa, have been described. Adlumia comprises only two species, which are quite similar morphologically, one from North America and the other from East Asia.</discussion>
  <discussion>Most European and some American systematists treat Fumariaceae as a subfamily of Papaveraceae. However, although a few taxa are morphologically intermediate, the members of Fumariaceae generally are quite distinct from those of Papaveraceae in several respects, including floral symmetry, sap character, and stamen number and fusion.</discussion>
  <references>
    <reference>Ernst, W. R. 1962. The genera of Papaveraceae and Fumariaceae in the southeastern United States. J. Arnold Arbor. 43: 315-343.</reference>
    <reference>Fedde, F. 1936. Papaveraceae. In: H. G. A. Engler et al., eds. 1924+. Die natürlichen Pflanzenfamilien, ed. 2. 26+ vols. Leipzig and Berlin. Vol. 17b, pp. 5-145.</reference>
    <reference>Gunn, C. R. 1980. Seeds and fruits of Papaveraceae and Fumariaceae. Seed Sci. Technol. 8: 3-58.</reference>
    <reference>Hutchinson, J. 1921. The genera of the Fumariaceae and their distribution. Bull. Misc. Inform. Kew 1921: 97-115.</reference>
    <reference>Lidén, M. 1986. Synopsis of Fumarioideae (Papaveraceae) with a monograph of the tribe Fumarieae. Opera Bot. 88: 1-133.</reference>
    <reference>Rachelle, L. D. 1974. Pollen morphology of the Papaveraceae of the northeastern U.S. and Canada. Bull. Torr. Bot. Club. 101: 152-159.</reference>
    <reference>Ryberg, M. 1960. Studies in the Morphology and Taxonomy of the Fumariaceae. Uppsala.</reference>
    <reference>Ryberg, M. 1960b. A morphological study of the Fumariaceae and the taxonomic significance of the characters examined. Acta Hort. Berg. 19(4): 121-248.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals almost completely connate, spongy; plants climbing, petiolules and reduced leaflets twining and tendril-like.</description>
      <determination>2 Adlumia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals coherent or connate only basally, not spongy; plants not climbing.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Both outer petals swollen or spurred basally.</description>
      <determination>1 Dicentra</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Only 1 outer petal swollen or spurred basally.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruit an elongate, dehiscent capsule; seeds more than 1, with elaiosome.</description>
      <determination>3 Corydalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruit a ± globose, indehiscent capsule; seeds 1, without elaiosome.</description>
      <determination>4 Fumaria</determination>
    </key_statement>
  </key>
</bio:treatment>