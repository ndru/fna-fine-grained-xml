<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">actaea</taxon_name>
    <taxon_name authority="(Aiton) Willdenow" date="1809" rank="species">rubra</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>1: 561. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus actaea;species rubra</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500024</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">spicata</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="variety">rubra</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 221. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Actaea;species spicata;variety rubra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">arguta</taxon_name>
    <taxon_hierarchy>genus Actaea;species arguta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">eburnea</taxon_name>
    <taxon_hierarchy>genus Actaea;species eburnea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="Gillman" date="unknown" rank="species">rubra</taxon_name>
    <taxon_name authority="(Nuttall) Hultén" date="unknown" rank="subspecies">arguta</taxon_name>
    <taxon_hierarchy>genus Actaea;species rubra;subspecies arguta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">neglecta</taxon_name>
    <taxon_hierarchy>genus Actaea;species neglecta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rubra</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="variety">dissecta</taxon_name>
    <taxon_hierarchy>genus Actaea;species rubra;variety dissecta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actaea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">viridiflora</taxon_name>
    <taxon_hierarchy>genus Actaea;species viridiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blade: leaflets abaxially glabrous or pubescent.</text>
      <biological_entity id="o2984" name="leaf-blade" name_original="leaf-blade" src="d0_s0" type="structure" />
      <biological_entity id="o2985" name="leaflet" name_original="leaflets" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences at anthesis often as long as wide, pyramidal.</text>
      <biological_entity id="o2986" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="as-long-as wide" name="shape" src="d0_s1" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: petals acute to obtuse at apex;</text>
      <biological_entity id="o2987" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o2988" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="at apex" constraintid="o2989" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
      <biological_entity id="o2989" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stigma nearly sessile, 0.7-1.2 mm diam. during anthesis, much narrower than ovary.</text>
      <biological_entity id="o2990" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o2991" name="stigma" name_original="stigma" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" constraint="during ovary" constraintid="o2993" from="0.7" from_unit="mm" name="diameter" src="d0_s3" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2993" name="ovary" name_original="ovary" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="much" name="width" src="d0_s3" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o2992" name="ovary" name_original="ovary" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="much" name="width" src="d0_s3" value="narrower" value_original="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Berries red or white, widely ellipsoid, 5-11 mm;</text>
      <biological_entity id="o2994" name="berry" name_original="berries" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s4" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel dull green or brown, slender, 0.3-0.7 mm diam., thinner than axis of raceme.</text>
      <biological_entity id="o2995" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s5" to="0.7" to_unit="mm" />
        <character constraint="than axis" constraintid="o2996" is_modifier="false" name="width" src="d0_s5" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o2996" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o2997" name="raceme" name_original="raceme" src="d0_s5" type="structure" />
      <relation from="o2996" id="r379" name="part_of" negation="false" src="d0_s5" to="o2997" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds 2.9-3.6 mm. 2n = 16.</text>
      <biological_entity id="o2998" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s6" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2999" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly deciduous forests, also mixed coniferous forests, open pine or spruce woodlands, swales, stream banks, and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous forests" modifier="mostly" />
        <character name="habitat" value="mixed coniferous forests" modifier="also" />
        <character name="habitat" value="woodlands" modifier="open pine or spruce" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Utah, Vt., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Actée rouge</other_name>
  <discussion>The "eye" formed by the persistent stigma in Actaea rubra is smaller than that in A.pachypoda.</discussion>
  <discussion>Actaea rubra is part of a circumboreal complex and is very similar to the black-fruited European species A. spicata Linnaeus, with which it is sometimes considered conspecific. The western North American plants of A. rubra have been called A. arguta and were distinguished on the basis of their smaller berries, more pubescent leaves, and narrow, more dissected leaflets. Those distinctions, however, are weak; specimens from the West often have fruits and leaves similar to those of plants from the East. A thorough study of A.spicata in the broad sense, on a worldwide scale, is needed to resolve the delimitation of taxa within this complex.</discussion>
  <discussion>Plants with white fruit, sometimes distinguished as Actaea rubra forma neglecta (Gillman) H. Robinson, are frequent and are more common than the red-fruited form in many localities.</discussion>
  <discussion>Native Americans used various preparations made from the roots of Actaea rubra medicinally to treat coughs and colds, sores, hemorrhages, stomachaches, syphilis, and emaciations; preparations from the entire plant as a purgative; and infusions from the stems to increase milk flow. It was also used in various ceremonies (D. E. Moerman 1986).</discussion>
  
</bio:treatment>