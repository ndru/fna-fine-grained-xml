<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Spach" date="1839" rank="section">Meconella</taxon_name>
    <taxon_name authority="Rottbfll" date="1770" rank="species">radicatum</taxon_name>
    <place_of_publication>
      <publication_title>Skr. Kifbenhavnske Selsk. Laerd. Elsk.</publication_title>
      <place_in_publication>10: 455. 1770</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section meconella;species radicatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500854</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely to densely cespitose, to 1.5 dm.</text>
      <biological_entity id="o8956" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely to densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 12 cm;</text>
      <biological_entity id="o8957" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 2/3 length of leaf;</text>
      <biological_entity id="o8958" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character name="length" src="d0_s2" value="2/3 length of leaf" value_original="2/3 length of leaf" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green on both surfaces, not glaucous, lanceolate, 1-2×-lobed with 2-3 (-4) pairs of primary lateral lobes;</text>
      <biological_entity id="o8959" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="on surfaces" constraintid="o8960" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character constraint="with pairs" constraintid="o8961" is_modifier="false" name="shape" src="d0_s3" value="1-2×-lobed" value_original="1-2×-lobed" />
      </biological_entity>
      <biological_entity id="o8960" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o8961" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity constraint="primary lateral" id="o8962" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o8961" id="r1264" name="part_of" negation="false" src="d0_s3" to="o8962" />
    </statement>
    <statement id="d0_s4">
      <text>primary lobes broadly lanceolate or strap-shaped, apex obtuse to acute.</text>
      <biological_entity constraint="primary" id="o8963" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="strap--shaped" value_original="strap--shaped" />
      </biological_entity>
      <biological_entity id="o8964" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scapes erect or bowed and decumbent, less than 15 cm, sparsely to densely hispid.</text>
      <biological_entity id="o8965" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o8966" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="bowed" value_original="bowed" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s5" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers to 6.5 cm diam.;</text>
      <biological_entity id="o8967" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s6" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals yellow or white, rarely pink tinged, or brick-red;</text>
      <biological_entity id="o8968" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink tinged" value_original="pink tinged" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brick-red" value_original="brick-red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink tinged" value_original="pink tinged" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brick-red" value_original="brick-red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pink tinged" value_original="pink tinged" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brick-red" value_original="brick-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow;</text>
      <biological_entity id="o8969" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas 4-7, disc convex.</text>
      <biological_entity id="o8970" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <biological_entity id="o8971" name="disc" name_original="disc" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules obovoid to subglobose, 1-2.5 times longer than broad, strigose, trichomes light to dark-brown or black.</text>
      <biological_entity id="o8972" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s10" to="subglobose" />
        <character is_modifier="false" name="length_or_size_or_width" src="d0_s10" value="1-2.5 times longer than broad" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o8973" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s10" to="dark-brown or black" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Nfld. and Labr. (Nfld.), Que., Yukon; Alaska, Colo., Idaho, N.Mex., Utah, Wyo.; Arctic and alpine North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Arctic and alpine North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Arctic poppy</other_name>
  <discussion>Subspecies numerous (4 in the flora).</discussion>
  <discussion>Many infraspecific taxa have been named from throughout the extensive range of this extremely variable species. Within North America, the following broadly circumscribed subspecies are generally, but not always, distinguishable.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Scapes densely hispid with dark trichomes; capsules with dark brown trichomes.</description>
      <determination>8c Papaver radicatum subsp. polare</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Scapes hispid with light-colored trichomes (sometimes dark brown in subsp. alaskanum); capsules with light to dark brown trichomes.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 2 cm diam. or less; capsules ellipsoid-subglobose to oblong-obconic; Rocky Mountain system from Alaska and Yukon southward.</description>
      <determination>8d Papaver radicatum subsp. kluanense</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers mostly greater than 2 cm diam.; capsules broadly obovoid to ellipsoid; Aleutian Islands n, e across arctic Alaska and Canada to Greenland.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsule trichomes generally with abruptly thickened bases; Aleutian Islands, islands of the Bering Strait and w coast of Alaska.</description>
      <determination>8b Papaver radicatum subsp. alaskanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsule trichomes generally without thickened bases; w, n Alaska e to Canada and Greenland.</description>
      <determination>8a Papaver radicatum subsp. radicatum</determination>
    </key_statement>
  </key>
</bio:treatment>