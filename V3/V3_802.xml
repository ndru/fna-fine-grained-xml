<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 983. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species nana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500254</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, sprawling, creeping, or upright, to 1 m.</text>
      <biological_entity id="o20002" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray to dark-brown, smooth, close;</text>
      <biological_entity id="o20003" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s1" to="dark-brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="close" value_original="close" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lenticels inconspicuous, unexpanded.</text>
      <biological_entity id="o20004" name="lenticel" name_original="lenticels" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="size" src="d0_s2" value="unexpanded" value_original="unexpanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Twigs without taste and odor of wintergreen, glabrous to sparsely or moderately pubescent, with or without heavy resinous coating, sometimes covered with warty resinous glands.</text>
      <biological_entity id="o20005" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous" name="odor" src="d0_s3" to="sparsely or moderately pubescent" />
      </biological_entity>
      <biological_entity id="o20006" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s3" value="warty" value_original="warty" />
        <character is_modifier="true" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o20005" id="r2697" modifier="sometimes" name="covered with" negation="false" src="d0_s3" to="o20006" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade broadly orbiculate or obovate-orbiculate to reniform, with 2–6 pairs of lateral-veins, often broader than long, base rounded to nearly cordate, margins deeply crenate, apex rounded;</text>
      <biological_entity id="o20007" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate-orbiculate" name="shape" src="d0_s4" to="reniform" />
        <character is_modifier="false" name="width" notes="" src="d0_s4" value="often broader than long" value_original="often broader than long" />
      </biological_entity>
      <biological_entity id="o20008" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o20009" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure" />
      <biological_entity id="o20010" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="nearly cordate" />
      </biological_entity>
      <biological_entity id="o20011" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o20012" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o20007" id="r2698" name="with" negation="false" src="d0_s4" to="o20008" />
      <relation from="o20008" id="r2699" name="part_of" negation="false" src="d0_s4" to="o20009" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially glabrous to sparsely or moderately pubescent.</text>
    </statement>
    <statement id="d0_s6">
      <text>Staminate and pistillate catkins produced season before flowering but retained in buds during winter, expanding along with new growth in spring.</text>
      <biological_entity id="o20013" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="abaxially glabrous" name="pubescence" src="d0_s5" to="sparsely or moderately pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20014" name="catkin" name_original="catkins" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="along-with growth" constraintid="o20017" is_modifier="false" name="size" src="d0_s6" value="expanding" value_original="expanding" />
      </biological_entity>
      <biological_entity id="o20015" name="season" name_original="season" src="d0_s6" type="structure" />
      <biological_entity id="o20016" name="bud" name_original="buds" src="d0_s6" type="structure" />
      <biological_entity id="o20017" name="growth" name_original="growth" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="new" value_original="new" />
      </biological_entity>
      <relation from="o20014" id="r2700" name="produced" negation="false" src="d0_s6" to="o20015" />
      <relation from="o20014" id="r2701" name="retained in" negation="false" src="d0_s6" to="o20016" />
    </statement>
    <statement id="d0_s7">
      <text>Infructescences erect, nearly cylindric, shattering with fruits in fall.</text>
      <biological_entity id="o20018" name="infructescence" name_original="infructescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o20019" name="fruit" name_original="fruits" src="d0_s7" type="structure" />
      <relation from="o20018" id="r2702" name="shattering with" negation="false" src="d0_s7" to="o20019" />
    </statement>
    <statement id="d0_s8">
      <text>Samaras with wings much narrower than body, broadest near center, not extended beyond body apically.</text>
      <biological_entity id="o20020" name="samara" name_original="samaras" src="d0_s8" type="structure">
        <character constraint="than body" constraintid="o20022" is_modifier="false" name="width" src="d0_s8" value="much narrower" value_original="much narrower" />
        <character constraint="near center" constraintid="o20023" is_modifier="false" name="width" src="d0_s8" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o20024" is_modifier="false" modifier="not" name="size" notes="" src="d0_s8" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o20021" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character constraint="than body" constraintid="o20022" is_modifier="false" name="width" src="d0_s8" value="much narrower" value_original="much narrower" />
      </biological_entity>
      <biological_entity id="o20022" name="body" name_original="body" src="d0_s8" type="structure" />
      <biological_entity id="o20023" name="center" name_original="center" src="d0_s8" type="structure" />
      <biological_entity id="o20024" name="body" name_original="body" src="d0_s8" type="structure" />
      <relation from="o20020" id="r2703" name="with" negation="false" src="d0_s8" to="o20021" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Sask., Yukon; Alaska; Subarctic and arctic of North America, Europe, and Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Subarctic and arctic of North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="and Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Arctic dwarf birch</other_name>
  <other_name type="common_name">bouleau nain</other_name>
  <discussion>Subspecies 3 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Young twigs pubescent, not covered with conspicuous resinous layer; subarctic and arctic ne Canada, s Greenland.</description>
      <determination>17a Betula nana subsp. nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Young twigs glabrous or only puberulent, covered with thick resinous coating; Alaska, Yukon, n Asia.</description>
      <determination>17b Betula nana subsp. exilis</determination>
    </key_statement>
  </key>
</bio:treatment>