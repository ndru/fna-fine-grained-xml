<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">argemone</taxon_name>
    <taxon_name authority="Durand &amp; Hilgard" date="1854" rank="species">munita</taxon_name>
    <taxon_name authority="G. B. Ownbey" date="1958" rank="subspecies">argentea</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>21: 83. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus argemone;species munita;subspecies argentea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500134</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Argemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">munita</taxon_name>
    <taxon_name authority="(G. B. Ownbey) Shinners" date="unknown" rank="variety">argentea</taxon_name>
    <taxon_hierarchy>genus Argemone;species munita;variety argentea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems greenish white, 4-8 dm, with 100-300 prickles per cm⊃2;</text>
      <biological_entity id="o4091" name="prickle" name_original="prickles" src="d0_s0" type="structure">
        <character char_type="range_value" from="100" is_modifier="true" name="quantity" src="d0_s0" to="300" />
      </biological_entity>
      <biological_entity id="o4092" name="cm" name_original="cm" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4090" id="r572" name="with" negation="false" src="d0_s0" to="o4091" />
      <relation from="o4091" id="r573" name="per" negation="false" src="d0_s0" to="o4092" />
    </statement>
    <statement id="d0_s1">
      <text>on main-stem below capsule, longest prickles 7-10 mm.</text>
      <biological_entity id="o4090" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o4093" name="capsule" name_original="capsule" src="d0_s1" type="structure" />
      <biological_entity constraint="longest" id="o4094" name="prickle" name_original="prickles" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <relation from="o4090" id="r574" name="below" negation="false" src="d0_s1" to="o4093" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades: surfaces usually densely prickly on veins and intervein areas.</text>
      <biological_entity id="o4095" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure" />
      <biological_entity id="o4096" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character constraint="on veins; on veins and intervein areas" constraintid="o4098, o4099" is_modifier="false" modifier="usually densely" name="architecture" src="d0_s2" value="prickly" value_original="prickly" />
      </biological_entity>
      <biological_entity id="o4097" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity id="o4098" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="intervein" id="o4099" name="area" name_original="areas" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bud prickles simple.</text>
      <biological_entity id="o4100" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="bud" id="o4101" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsules densely prickly, surface partially obscured, prickles unequal, longest 7-10 mm. 2n = 28.</text>
      <biological_entity id="o4102" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s4" value="prickly" value_original="prickly" />
      </biological_entity>
      <biological_entity id="o4103" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="partially" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o4104" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="length" src="d0_s4" value="longest" value_original="longest" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4105" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring; fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry desert ranges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry desert ranges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-850 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="850" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7d.</number>
  
</bio:treatment>