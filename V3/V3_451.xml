<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">berberis</taxon_name>
    <taxon_name authority="Torrey in W. H. Emory" date="1859" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory,  Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 30. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus berberis;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500229</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mahonia</taxon_name>
    <taxon_name authority="(Torrey) Fedde" date="unknown" rank="species">fremontii</taxon_name>
    <taxon_hierarchy>genus Mahonia;species fremontii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs evergreen, 1-4.5 m.</text>
      <biological_entity id="o15155" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="4.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± dimorphic, with elongate primary and short or somewhat elongate axillary shoots.</text>
      <biological_entity id="o15156" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o15157" name="primary" name_original="primary" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o15158" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o15156" id="r2112" name="with" negation="false" src="d0_s1" to="o15157" />
    </statement>
    <statement id="d0_s2">
      <text>Bark of 2d-year stems light-brown or grayish purple, glabrous.</text>
      <biological_entity id="o15159" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish purple" value_original="grayish purple" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15160" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o15159" id="r2113" name="part_of" negation="false" src="d0_s2" to="o15160" />
    </statement>
    <statement id="d0_s3">
      <text>Bud-scales 2-4 mm, deciduous.</text>
      <biological_entity id="o15161" name="bud-scale" name_original="bud-scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines absent.</text>
      <biological_entity id="o15162" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves 5-9 (-11) -foliolate;</text>
      <biological_entity id="o15163" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-9(-11)-foliolate" value_original="5-9(-11)-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petioles 0.2-0.8 (-3) cm.</text>
      <biological_entity id="o15164" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s6" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaflet blades thick and rigid;</text>
      <biological_entity constraint="leaflet" id="o15165" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially dull, papillose, adaxially dull, glaucous;</text>
      <biological_entity id="o15166" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet stalked in most or all leaves, blade 1-2.6 (-4) × 0.7-1.8 (-2.5) cm, 1-2.5 times as long as wide;</text>
      <biological_entity constraint="terminal" id="o15167" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character constraint="in leaves" constraintid="o15168" is_modifier="false" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o15168" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o15169" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s9" to="2.6" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s9" to="1.8" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1-2.5" value_original="1-2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflet blades elliptic to ovate or orbiculate, 1-3-veined from base, base obtuse or truncate, margins strongly crispate, toothed or lobed, with 2-5 teeth 2-6 mm high tipped with spines to 0.8-2.2 × 0.2-0.3 mm, apex obtuse to acuminate.</text>
      <biological_entity constraint="leaflet" id="o15170" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lateral leaflet">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="ovate or orbiculate" />
        <character constraint="from base" constraintid="o15171" is_modifier="false" name="architecture" src="d0_s10" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o15171" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o15172" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o15173" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character constraint="with spines" constraintid="o15175" is_modifier="false" name="architecture" src="d0_s10" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o15174" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character char_type="range_value" from="2" from_unit="mm" name="height" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15175" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s10" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15176" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <relation from="o15173" id="r2114" name="with" negation="false" src="d0_s10" to="o15174" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences racemose, lax, 3-6-flowered, 2.5-6.5 cm;</text>
      <biological_entity id="o15177" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-6-flowered" value_original="3-6-flowered" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s11" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles membranous, apex acuminate.</text>
      <biological_entity id="o15178" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o15179" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers: anther-filaments with distal pair of recurved lateral teeth.</text>
      <biological_entity id="o15180" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15181" name="anther-filament" name_original="anther-filaments" src="d0_s13" type="structure" />
      <biological_entity constraint="distal" id="o15182" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="lateral" id="o15183" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o15181" id="r2115" name="with" negation="false" src="d0_s13" to="o15182" />
      <relation from="o15182" id="r2116" name="part_of" negation="false" src="d0_s13" to="o15183" />
    </statement>
    <statement id="d0_s14">
      <text>Berries yellow or red to brown, ± glaucous, spheric, 12-18 mm, dry, inflated.</text>
      <biological_entity id="o15184" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s14" to="brown" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s14" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s14" value="dry" value_original="dry" />
        <character is_modifier="false" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes and flats in desert grassland and pinyon-juniper woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" constraint="in desert" />
        <character name="habitat" value="flats" constraint="in desert" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="pinyon-juniper woodland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-2400(-3400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3400" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Berberis fremontii is susceptible to infection by Puccinia graminis.</discussion>
  <discussion>The Apache Indians used Berberis fremontii for ceremonial purposes; the Hopi used it medicinally to heal gums (D. E. Moermann 1986).</discussion>
  
</bio:treatment>