<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="Rydberg" date="1902" rank="species">alpestre</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 146. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species alpestre;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500470</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">ramosum</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber" date="unknown" rank="variety">alpestre</taxon_name>
    <taxon_hierarchy>genus Delphinium;species ramosum;variety alpestre;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5-25 cm;</text>
      <biological_entity id="o14485" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base green, puberulent.</text>
      <biological_entity id="o14486" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 5-20, on proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o14487" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="20" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14488" name="1/5" name_original="1/5" src="d0_s2" type="structure" />
      <biological_entity id="o14489" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o14487" id="r1999" modifier="at anthesis" name="on" negation="false" src="d0_s2" to="o14488" />
      <relation from="o14488" id="r2000" name="part_of" negation="false" src="d0_s2" to="o14489" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-10 cm.</text>
      <biological_entity id="o14490" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to pentagonal, 1.5-5 × 2-5 cm, puberulent;</text>
      <biological_entity id="o14491" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="pentagonal" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 3-15, width 2-11 mm.</text>
      <biological_entity constraint="ultimate" id="o14492" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2-8-flowered;</text>
      <biological_entity id="o14493" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel 1-4 cm, puberulent;</text>
      <biological_entity id="o14494" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1-3 mm from flowers, green, linear-lanceolate, 6-10 mm, puberulent.</text>
      <biological_entity id="o14495" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o14496" from="1" from_unit="mm" name="location" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o14496" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals dark blue, apex rounded, puberulent, lateral sepals spreading to forward pointing, 11-14 × 5-7 mm, spurs straight except usually slightly downcurved at apex, varying from 20° above to 20° below horizontal, 8-12 mm;</text>
      <biological_entity id="o14497" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14498" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark blue" value_original="dark blue" />
      </biological_entity>
      <biological_entity id="o14499" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14500" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading to forward" value_original="spreading to forward" />
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s9" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14501" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character constraint="at apex" constraintid="o14502" is_modifier="false" modifier="usually slightly" name="orientation" src="d0_s9" value="downcurved" value_original="downcurved" />
        <character constraint="below horizontal" is_modifier="false" modifier="0-20°" name="variability" notes="" src="d0_s9" value="varying" value_original="varying" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14502" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lower petal blades ± covering stamens, 4-6 mm, clefts 2-4 mm;</text>
      <biological_entity id="o14503" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o14504" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14505" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o14506" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o14504" id="r2001" modifier="more or less" name="covering" negation="false" src="d0_s10" to="o14505" />
    </statement>
    <statement id="d0_s11">
      <text>hairs sparse, mostly near base of cleft, centered on inner lobes, white.</text>
      <biological_entity id="o14507" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14508" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s11" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o14509" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o14510" name="cleft" name_original="cleft" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o14511" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o14508" id="r2002" modifier="mostly" name="near" negation="false" src="d0_s11" to="o14509" />
      <relation from="o14509" id="r2003" name="part_of" negation="false" src="d0_s11" to="o14510" />
      <relation from="o14508" id="r2004" name="centered on" negation="false" src="d0_s11" to="o14511" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits 7-12 mm, 3.5-4 times longer than wide, puberulent.</text>
      <biological_entity id="o14512" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="3.5-4" value_original="3.5-4" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds unwinged;</text>
      <biological_entity id="o14513" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cells elongate, surface roughened.</text>
      <biological_entity constraint="seed-coat" id="o14514" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o14515" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid-late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed talus slopes on high peaks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" modifier="exposed" constraint="on high peaks" />
        <character name="habitat" value="high peaks" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(3400-)3800 m and above</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="3800" from_unit="m" constraint=" and above" />
        <character name="elevation" char_type="atypical_range" to="3800" to_unit="m" from="3400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Alpine larkspur</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium alpestre is very similar to D. ramosum, possibly divergent from that taxon only since the most recent glaciation of North America, during which ancestors of D. alpestre might have survived on peaks above the ice, while ancestors of D. ramosum survived in valleys below the ice. Since glaciation, D. ramosum apparently has migrated upslope, near but not adjoining populations of D. alpestre.</discussion>
  
</bio:treatment>