<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Ewan) N. I. Malyutin" date="1987" rank="subsection">Echinata</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>72: 689. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection echinata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302008</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Echinatae Ewan" date="unknown" rank="species">series</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>69: 139. 1942</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species series;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 3-9 (-20) -branched, 3-8 (-30) cm, fascicled, dry to fleshy, ± succulent, thin threadlike segments restricted to termini of major segment;</text>
      <biological_entity id="o13858" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="3-9(-20)-branched" value_original="3-9(-20)-branched" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="fascicled" value_original="fascicled" />
        <character char_type="range_value" from="dry" name="texture" src="d0_s0" to="fleshy" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o13859" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="shape" src="d0_s0" value="thread-like" value_original="threadlike" />
      </biological_entity>
      <biological_entity id="o13860" name="terminus" name_original="termini" src="d0_s0" type="structure" />
      <biological_entity id="o13861" name="segment" name_original="segment" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="major" value_original="major" />
      </biological_entity>
      <relation from="o13859" id="r1920" name="restricted to" negation="false" src="d0_s0" to="o13860" />
      <relation from="o13859" id="r1921" name="restricted to" negation="false" src="d0_s0" to="o13861" />
    </statement>
    <statement id="d0_s1">
      <text>buds minute.</text>
      <biological_entity id="o13862" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (-2) per root, unbranched, elongation delayed 2-10 weeks after leaf initiation;</text>
      <biological_entity id="o13863" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character constraint="per root" constraintid="o13864" name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o13864" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o13865" name="elongation" name_original="elongation" src="d0_s2" type="structure" />
      <biological_entity id="o13866" name="week" name_original="weeks" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o13867" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <relation from="o13865" id="r1922" name="delayed" negation="false" src="d0_s2" to="o13866" />
      <relation from="o13865" id="r1923" name="after" negation="false" src="d0_s2" to="o13867" />
    </statement>
    <statement id="d0_s3">
      <text>base usually not narrowed, firmly attached to root;</text>
      <biological_entity id="o13868" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to root" constraintid="o13869" is_modifier="false" modifier="firmly" name="fixation" src="d0_s3" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o13869" name="root" name_original="root" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal internodes much shorter than those of midstem.</text>
      <biological_entity constraint="proximal" id="o13870" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline, largest near base of stem, others often abruptly smaller on distal portion of stem;</text>
      <biological_entity id="o13871" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character constraint="near base" constraintid="o13872" is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o13872" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o13873" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o13874" name="other" name_original="others" src="d0_s5" type="structure">
        <character constraint="on distal portion" constraintid="o13875" is_modifier="false" modifier="often abruptly" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13875" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o13876" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o13872" id="r1924" name="part_of" negation="false" src="d0_s5" to="o13873" />
      <relation from="o13875" id="r1925" name="part_of" negation="false" src="d0_s5" to="o13876" />
    </statement>
    <statement id="d0_s6">
      <text>basal petioles spreading, cauline petioles ascending;</text>
      <biological_entity constraint="basal" id="o13877" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13878" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades of basal leaves more rounded and with fewer, wider lobes than cauline leaves.</text>
      <biological_entity id="o13879" name="blade" name_original="blades" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13880" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="wider" id="o13882" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13881" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o13879" id="r1926" name="part_of" negation="false" src="d0_s7" to="o13880" />
      <relation from="o13879" id="r1927" name="with" negation="false" src="d0_s7" to="o13882" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually with 2-11 flowers per 5 cm, dense to open, cylindric, spurs rarely intersecting rachis;</text>
      <biological_entity id="o13883" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="per 5 cm" name="density" notes="" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o13884" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
      <biological_entity id="o13885" name="spur" name_original="spurs" src="d0_s8" type="structure" />
      <biological_entity id="o13886" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <relation from="o13883" id="r1928" name="with" negation="false" src="d0_s8" to="o13884" />
      <relation from="o13885" id="r1929" modifier="rarely" name="intersecting" negation="false" src="d0_s8" to="o13886" />
    </statement>
    <statement id="d0_s9">
      <text>pedicel ascending, usually less than 2 cm, rachis to midpedicel angle more than 30°;</text>
      <biological_entity id="o13887" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13888" name="rachis" name_original="rachis" src="d0_s9" type="structure" />
      <biological_entity constraint="midpedicel" id="o13889" name="angle" name_original="angle" src="d0_s9" type="structure">
        <character name="degree" src="d0_s9" value="30+°" value_original="30+°" />
      </biological_entity>
      <relation from="o13888" id="r1930" name="to" negation="false" src="d0_s9" to="o13889" />
    </statement>
    <statement id="d0_s10">
      <text>bracts markedly smaller and fewer lobed than leaves.</text>
      <biological_entity id="o13890" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="markedly" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13891" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits erect.</text>
      <biological_entity id="o13892" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds ± rectangular, 1.5-2.5 × 0.8-1.5 mm, not ringed at proximal end, ± wing-margined;</text>
      <biological_entity id="o13893" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
        <character constraint="at proximal end" constraintid="o13894" is_modifier="false" modifier="not" name="relief" src="d0_s12" value="ringed" value_original="ringed" />
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s12" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13894" name="end" name_original="end" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>seed-coats without wavy ridges (sometimes with prism-shaped structures in D. hansenii), cells ± brick-shaped, cell margins straight or undulate.</text>
      <biological_entity id="o13895" name="seed-coat" name_original="seed-coats" src="d0_s13" type="structure" />
      <biological_entity id="o13896" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o13897" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="brick--shaped" value_original="brick--shaped" />
      </biological_entity>
      <biological_entity constraint="cell" id="o13898" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s13" value="undulate" value_original="undulate" />
      </biological_entity>
      <relation from="o13895" id="r1931" name="without" negation="false" src="d0_s13" to="o13896" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.7.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal petioles glabrous to puberulent with short (less than 0.5 mm) appressed and/or arched hairs.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal petioles with long (more than 0.5 mm), straight spreading hairs.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepal color and/or stem pubescence different from that of other individuals with which they occur; stem base puberulent with short arched hairs.</description>
      <determination>39c Delphinium hesperium subsp. pallescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepal color and stem pubescence similar to that of other individuals with which they occur; stem base glabrous to pubescent with short straight hairs.</description>
      <determination>39 Delphinium hesperium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Seeds echinate, appearing fuzzy to naked eye; lateral sepals 13 mm or shorter; flowers usually more than 12 per main inflorescence branch.</description>
      <determination>42 Delphinium hansenii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Seeds not echinate, appearing ± smooth to naked eye; lateral sepals 10 mm or longer; flowers usually fewer than 12 per main inflorescence branch.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem base longitudinally ridged.</description>
      <determination>39 Delphinium hesperium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem base not longitudinally ridged.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Margins of lower petals ciliate; bracteoles usually 7 mm or less from sepals; sepal spur straight.</description>
      <determination>40 Delphinium variegatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Margins of lower petals glabrous; bracteoles usually 7 mm or more from sepals; sepal spur often downcurved for more than 3 mm at apex.</description>
      <determination>41 Delphinium hutchinsoniae</determination>
    </key_statement>
  </key>
</bio:treatment>