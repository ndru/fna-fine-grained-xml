<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="(de Candolle) Gray" date="1822" rank="genus">CONSOLIDA</taxon_name>
    <taxon_name authority="(Linnaeus) Schur" date="1853" rank="species">ajacis</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Mitth. Siebenbürg. Vereins Naturwiss. Hermannstadt</publication_title>
      <place_in_publication>4: 47. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus consolida;species ajacis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200007763</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ajacis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 531. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species ajacis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 3-8 (-10) dm, glabrous to sparsely puberulent.</text>
      <biological_entity id="o10908" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s0" to="sparsely puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5-20 or more.</text>
      <biological_entity id="o10909" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade orbiculate, 12-60-lobed or more, 1-5 cm wide, glabrous to puberulent, lobes less than 1.5 mm wide.</text>
      <biological_entity id="o10910" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="12-60-lobed" value_original="12-60-lobed" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="puberulent" />
      </biological_entity>
      <biological_entity id="o10911" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 6-30 (-75) -flowered, simple or with 3 or fewer branches;</text>
      <biological_entity id="o10912" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="6-30(-75)-flowered" value_original="6-30(-75)-flowered" />
        <character constraint="with 3 or fewerbranches" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="with 3 or fewerbranches" value_original="with 3 or fewerbranches" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts (at least lowermost 2) with 5 or more lobes;</text>
      <biological_entity id="o10913" name="bract" name_original="bracts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pedicel ascending-spreading, 1-3 (-5) cm, ± puberulent;</text>
      <biological_entity id="o10914" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles not touching sepals, 4-20mm from flower, ± linear, 1-3mm, ± puberulent.</text>
      <biological_entity id="o10915" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="from flower" constraintid="o10917" from="4" from_unit="mm" name="location" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" notes="" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o10916" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
      <biological_entity id="o10917" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <relation from="o10915" id="r1520" name="touching" negation="false" src="d0_s6" to="o10916" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals blue to purple, rarely pink or white, nearly glabrous, lower sepal 8-18 × 4-8mm, lateral sepals 8-18 × 6-14 mm, spur 12-20 mm;</text>
      <biological_entity id="o10918" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10919" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s7" to="purple rarely pink or white" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s7" to="purple rarely pink or white" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10920" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10921" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10922" name="spur" name_original="spur" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals of same color as sepals or whiter, lateral lobes 3-6mm, terminal lobes 5-8 × 2-4 mm, sinus 0.2-1 mm.</text>
      <biological_entity id="o10923" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10924" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity id="o10925" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o10926" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="whiter" value_original="whiter" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o10927" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10928" name="sinus" name_original="sinus" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o10924" id="r1521" name="as" negation="false" src="d0_s8" to="o10925" />
      <relation from="o10924" id="r1522" name="as" negation="false" src="d0_s8" to="o10926" />
    </statement>
    <statement id="d0_s9">
      <text>Follicles 12-25 mm, puberulent.</text>
      <biological_entity id="o10929" name="follicle" name_original="follicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, old homesites, drainage ditches, roadsides, and railroads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="old homesites" />
        <character name="habitat" value="drainage ditches" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Man., Ont.; Ala., Ariz., Ark., Calif., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., N.C., N.Dak., N.J., N.Y., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.; native to Europe and Africa; introduced in Asia and Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="native to Europe and Africa" establishment_means="native" />
        <character name="distribution" value="in Asia and Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In many floras the names Consolida ambigua (Linnaeus) Ball &amp; Heywood and Delphinium ambiguum Linnaeus have been misapplied to this taxon.</discussion>
  <discussion>Consolida ajacis has escaped and become more or less naturalized in many temperate and subtropical parts of the world. It is by far the most commonly encountered species of Consolida in North America.</discussion>
  <discussion>The Cherokee used Consolida ajacis medicinally in infusions to treat heart problems (D. E. Moerman 1986, as Delphinium ajacis).</discussion>
  
</bio:treatment>