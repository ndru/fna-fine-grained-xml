<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="unknown" rank="section">Elatopsis</taxon_name>
    <taxon_name authority="W. T. Wang" date="1962" rank="subsection">Elata</taxon_name>
    <place_of_publication>
      <publication_title>Acta Bot. Sin.</publication_title>
      <place_in_publication>10: 81. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section elatopsis;subsection elata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302010</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 20-50 cm, twisted fibrous, dry, 5-11-branched;</text>
      <biological_entity id="o4170" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s0" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="5-11-branched" value_original="5-11-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds more than 3 mm, usually present throughout dormant season.</text>
      <biological_entity id="o4171" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" upper_restricted="false" />
        <character constraint="throughout dormant season" constraintid="o4172" is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4172" name="season" name_original="season" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="throughout" name="life_cycle" src="d0_s1" value="dormant" value_original="dormant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 2-8 per root, usually unbranched, elongation commencing within 2 weeks of leaf initiation;</text>
      <biological_entity id="o4173" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per root" constraintid="o4174" from="2" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o4174" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o4175" name="elongation" name_original="elongation" src="d0_s2" type="structure" />
      <biological_entity id="o4176" name="week" name_original="weeks" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4177" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4175" id="r587" name="commencing within" negation="false" src="d0_s2" to="o4176" />
      <relation from="o4175" id="r588" name="commencing within" negation="false" src="d0_s2" to="o4177" />
    </statement>
    <statement id="d0_s3">
      <text>base not narrowed, firmly attached to root.</text>
      <biological_entity id="o4178" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to root" constraintid="o4179" is_modifier="false" modifier="firmly" name="fixation" src="d0_s3" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o4179" name="root" name_original="root" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves largest at or slightly below middle of stem;</text>
      <biological_entity id="o4180" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="at leaves" constraintid="o4181" is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o4181" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="slightly below" name="position" src="d0_s4" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o4182" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <relation from="o4181" id="r589" name="part_of" negation="false" src="d0_s4" to="o4182" />
    </statement>
    <statement id="d0_s5">
      <text>petiole ascending;</text>
      <biological_entity id="o4183" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade shape and lobing similar throughout.</text>
      <biological_entity id="o4184" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s6" value="lobing" value_original="lobing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences ± dense, usually 3-8 flowers per 5 cm, narrowly pyramidal to cylindric;</text>
      <biological_entity id="o4185" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character char_type="range_value" from="3" modifier="usually" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o4186" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly pyramidal" modifier="per 5 cm" name="shape" src="d0_s7" to="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel spreading-ascending, usually less than 3 cm;</text>
      <biological_entity id="o4187" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts ± similar to leaves but smaller.</text>
      <biological_entity id="o4188" name="bract" name_original="bracts" src="d0_s9" type="structure" />
      <biological_entity id="o4189" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o4188" id="r590" name="to" negation="false" src="d0_s9" to="o4189" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits erect.</text>
      <biological_entity id="o4190" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds rectangular to crescent-shaped, 1.5-3.5 × 1.2-2.5 mm, not ringed at proximal end, wing-margined;</text>
      <biological_entity id="o4191" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s11" to="crescent-shaped" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
        <character constraint="at proximal end" constraintid="o4192" is_modifier="false" modifier="not" name="relief" src="d0_s11" value="ringed" value_original="ringed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4192" name="end" name_original="end" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>seed-coats with small wavy ridges, cells elongate, margins straight.</text>
      <biological_entity id="o4193" name="seed-coat" name_original="seed-coats" src="d0_s12" type="structure" />
      <biological_entity id="o4194" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s12" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o4195" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o4196" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o4193" id="r591" name="with" negation="false" src="d0_s12" to="o4194" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16a.1.</number>
  <discussion>Species ca. 30 (1 in the flora).</discussion>
  
</bio:treatment>