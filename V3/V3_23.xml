<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="de Candolle" date="1817" rank="species">menziesii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">menziesii</taxon_name>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species menziesii;subspecies menziesii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500519</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">menziesii</taxon_name>
    <taxon_name authority="(Ewan) C. L. Hitchcock" date="unknown" rank="variety">pyramidalis</taxon_name>
    <taxon_hierarchy>genus Delphinium;species menziesii;variety pyramidalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">oreganum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species oreganum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 35-70 cm.</text>
      <biological_entity id="o19985" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: sepals blue to purple, 13-20 × 5-11 mm;</text>
      <biological_entity id="o19986" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o19987" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s1" to="purple" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spurs 11-17 mm. 2n = 16.</text>
      <biological_entity id="o19988" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o19989" name="spur" name_original="spurs" src="d0_s2" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19990" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, open woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52a.</number>
  <other_name type="common_name">Menzies' larkspur</other_name>
  <discussion>Delphinium menziesii subsp. menziesii hybridizes with D. trolliifolium and D. nuttallii.</discussion>
  
</bio:treatment>