<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Susan E. Meyer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="genus">ARCTOMECON</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont,  Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>312. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus ARCTOMECON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek arktos, bear, alluding to the long-pilose pubescence, and mekon, poppy</other_info_on_name>
    <other_info_on_name type="fna_id">102491</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, evergreen, cespitose, scapose or very short-caulescent and subscapose, from taproots.</text>
      <biological_entity id="o19896" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s0" value="short-caulescent" value_original="short-caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19897" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <relation from="o19896" id="r2690" name="from" negation="false" src="d0_s0" to="o19897" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (caudices) when present leafy, erect, branching, covered with marcescent leaf-bases.</text>
      <biological_entity id="o19898" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when present" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o19899" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
      <relation from="o19898" id="r2691" name="covered with" negation="false" src="d0_s1" to="o19899" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves all or mostly basal and rosulate, petiolate;</text>
      <biological_entity id="o19900" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19901" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade cuneate to fan-shaped, 1×-lobed distally, glaucous, long-pilose, hairs flexuous, barbed;</text>
      <biological_entity id="o19902" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="fan-shaped" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="1×-lobed" value_original="1×-lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-pilose" value_original="long-pilose" />
      </biological_entity>
      <biological_entity id="o19903" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="barbed" value_original="barbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire;</text>
      <biological_entity id="o19904" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes acute, long-bristled.</text>
      <biological_entity id="o19905" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-bristled" value_original="long-bristled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, cymiform, simple or branching;</text>
      <biological_entity id="o19906" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts present;</text>
      <biological_entity id="o19907" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>buds nodding.</text>
      <biological_entity id="o19908" name="bud" name_original="buds" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 2 or 3, distinct;</text>
      <biological_entity id="o19909" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19910" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s9" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals persistent, 4 or 6;</text>
      <biological_entity id="o19911" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19912" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s10" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s10" unit="or" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens many;</text>
      <biological_entity id="o19913" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19914" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s11" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistil 3-6-carpellate;</text>
      <biological_entity id="o19915" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19916" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-6-carpellate" value_original="3-6-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 1-locular;</text>
      <biological_entity id="o19917" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19918" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style 1 and short, or absent;</text>
      <biological_entity id="o19919" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19920" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 1 per carpel, connate, capitate, cordately 2-lobed.</text>
      <biological_entity id="o19921" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19922" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character constraint="per carpel" constraintid="o19923" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="cordately" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o19923" name="carpel" name_original="carpel" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules erect, 3-6-valved, dehiscing from apex leaving persistent placental ribs unseparated at apex, valve tips recurving.</text>
      <biological_entity id="o19924" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-6-valved" value_original="3-6-valved" />
        <character constraint="from apex" constraintid="o19925" is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscing" value_original="dehiscing" />
        <character constraint="at apex" constraintid="o19928" is_modifier="false" name="fusion" src="d0_s16" value="unseparated" value_original="unseparated" />
      </biological_entity>
      <biological_entity id="o19925" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o19926" name="placental" name_original="placental" src="d0_s16" type="structure">
        <character is_modifier="true" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o19927" name="rib" name_original="ribs" src="d0_s16" type="structure">
        <character is_modifier="true" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o19928" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity constraint="valve" id="o19929" name="tip" name_original="tips" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="recurving" value_original="recurving" />
      </biological_entity>
      <relation from="o19925" id="r2692" name="leaving" negation="false" src="d0_s16" to="o19926" />
      <relation from="o19925" id="r2693" name="leaving" negation="false" src="d0_s16" to="o19927" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds several to many, dark-brown, shining, ovoid, 1.5-3 mm, aril present.</text>
      <biological_entity id="o19930" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="several" name="quantity" src="d0_s17" to="many" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shining" value_original="shining" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 12.</text>
      <biological_entity id="o19931" name="aril" name_original="aril" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o19932" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America (Mojave Desert).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America (Mojave Desert)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Desert bearclaw-poppy</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <references>
    <reference>Janish, J. R. 1977. Nevada's vanishing bear-poppies. Mentzelia 3: 2-5.</reference>
    <reference>Mozingo, H. N. and Margaret Williams. 1980. Threatened and Endangered Plants of Nevada: An Illustrated Manual. [Washington.]</reference>
    <reference>Nelson, D. R. and S. L. Welsh. 1993. Taxonomic revision of Arctomecon Torr. &amp; Frem. Rhodora 95: 197-213.</reference>
    <reference>Raynie, D. E., D. R. Nelson, and K. T. Harper. 1991. Alkaloidal relationships in the genus Arctomecon (Papaveraceae) and herbivory in A. humilis. Great Basin Naturalist 51: 397-403.</reference>
    <reference>Welsh, S. L. and K. L. Thorne. 1979. Illustrated Manual of Proposed Endangered and Threatened Plants of Utah. [Washington.]</reference>
    <reference>Nelson, D. R. and K. T. Harper. 1991. Site characteristics and habitat requirements of the endangered dwarf bear-claw poppy (Arctomecon humilis Coville, Papaveraceae). Great Basin Naturalist 51: 167-175.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 4, 1.2–2.5 cm, marcescent; leaf blades minutely hirsute but only sparsely long-pilose.</description>
      <determination>1 Arctomecon humilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 6, 2.5–4 cm, caducous; leaf blades copiously long-pilose.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences 1(–6)-flowered; flower buds long-pilose; petals white.</description>
      <determination>2 Arctomecon merriamii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences 3–20-flowered; flower buds glabrous; petals deep yellow.</description>
      <determination>3 Arctomecon californica</determination>
    </key_statement>
  </key>
</bio:treatment>