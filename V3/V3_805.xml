<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Leila M. Schultz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">ulmaceae</taxon_name>
    <taxon_name authority="Loureiro" date="1790" rank="genus">TREMA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Cochinch.</publication_title>
      <place_in_publication>2: 562. 1790</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ulmaceae;genus TREMA</taxon_hierarchy>
    <other_info_on_name type="fna_id">133312</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, spindly, to 15 m;</text>
      <biological_entity id="o9717" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_shape" src="d0_s0" value="spindly" value_original="spindly" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="growth_form_or_shape" src="d0_s0" value="spindly" value_original="spindly" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>crowns variable.</text>
      <biological_entity id="o9719" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark dark-brown or gray brown, smooth, shallowly furrowed, sometimes sometimes appearing warty with large, raised lenticels.</text>
      <biological_entity id="o9720" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray brown" value_original="gray brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s2" value="furrowed" value_original="furrowed" />
        <character constraint="with large" is_modifier="false" modifier="sometimes sometimes; sometimes" name="pubescence_or_relief" src="d0_s2" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity id="o9721" name="lenticel" name_original="lenticels" src="d0_s2" type="structure" />
      <relation from="o9720" id="r1364" name="raised" negation="false" src="d0_s2" to="o9721" />
    </statement>
    <statement id="d0_s3">
      <text>Branches unarmed, stout;</text>
      <biological_entity id="o9722" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>twigs hoary tomentose.</text>
      <biological_entity id="o9723" name="twig" name_original="twigs" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hoary" value_original="hoary" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: stipules ephemeral.</text>
      <biological_entity id="o9724" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9725" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade: ovate, base oblique to cordate or truncate, margins crenate to serrate;</text>
      <biological_entity id="o9726" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o9727" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblique" name="shape" src="d0_s6" to="cordate or truncate" />
      </biological_entity>
      <biological_entity id="o9728" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s6" to="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation palmate at base, pinnate on remainder of blade.</text>
      <biological_entity id="o9729" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character constraint="at base" constraintid="o9730" is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
        <character constraint="on remainder" constraintid="o9731" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o9730" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o9731" name="remainder" name_original="remainder" src="d0_s7" type="structure" />
      <biological_entity id="o9732" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <relation from="o9731" id="r1365" name="part_of" negation="false" src="d0_s7" to="o9732" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences cymes, compact to lax, 12-20-flowered.</text>
      <biological_entity constraint="inflorescences" id="o9733" name="cyme" name_original="cymes" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_architecture" src="d0_s8" value="compact to lax" value_original="compact to lax" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="12-20-flowered" value_original="12-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers mostly unisexual, usually staminate and pistillate on same plants, appearing after leaves on new stems, in 1 series, pedicellate;</text>
      <biological_entity id="o9734" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o9735" is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o9735" name="plant" name_original="plants" src="d0_s9" type="structure" />
      <biological_entity id="o9736" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o9737" name="stem" name_original="stems" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o9738" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o9734" id="r1366" name="appearing after" negation="false" src="d0_s9" to="o9736" />
      <relation from="o9734" id="r1367" name="appearing after" negation="false" src="d0_s9" to="o9737" />
      <relation from="o9734" id="r1368" name="in" negation="false" src="d0_s9" to="o9738" />
    </statement>
    <statement id="d0_s10">
      <text>calyx 5-parted.</text>
      <biological_entity id="o9739" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="5-parted" value_original="5-parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Bisexual flowers, if present: pedicel present;</text>
      <biological_entity id="o9740" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9741" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries ± globose;</text>
      <biological_entity id="o9742" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9743" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles persistent, 2, glabrous;</text>
      <biological_entity id="o9744" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9745" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas 2, unbranched.</text>
      <biological_entity id="o9746" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9747" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers: nearly sessile;</text>
      <biological_entity id="o9748" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistillodes present.</text>
      <biological_entity id="o9749" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9750" name="pistillode" name_original="pistillodes" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pistillate flowers: pedicel present;</text>
      <biological_entity id="o9751" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9752" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>staminodes absent.</text>
      <biological_entity id="o9753" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9754" name="staminode" name_original="staminodes" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits drupes, globose, fleshy.</text>
      <biological_entity constraint="fruits" id="o9755" name="drupe" name_original="drupes" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character is_modifier="false" name="texture" src="d0_s19" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Stones thick walled.</text>
      <biological_entity id="o9756" name="stone" name_original="stones" src="d0_s20" type="structure">
        <character is_modifier="false" name="width" src="d0_s20" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="walled" value_original="walled" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical regions, North America (Fla.), Mexico, West Indies, Central America, South America (to n Argentina), Asia, and Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="North America (Fla.)" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (to n Argentina)" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="and Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Trema</other_name>
  <other_name type="common_name">nettletree</other_name>
  <discussion>Species ca. 15 (2 in the flora).</discussion>
  <discussion>Trema cannabina Loureiro and T. orientalis (Linnaeus) Blume, sometimes reported for North America, are Old World species occasionally planted but not known to have escaped from cultivation.</discussion>
  <discussion>Trema species are fast-growing pioneer trees with economically important alkaloids.</discussion>
  <discussion>Trema is a member of the subfamily Celtoideae. Species are locally called nettletrees in reference to their superficial resemblance to members of the Urticaceae. Further studies of variation in this group are needed in the field and in the laboratory, giving special consideration to the morphologic variants within Trema micrantha.</discussion>
  <references>
    <reference>Baehni, C. 1937. Trema. In: J. F. MacBride et al., ed. 1936+. Flora of Peru. 6 +parts. Chicago. Part 2(2), pp. 269-270.</reference>
    <reference>Gardiner, R. C. 1965. Studies in the Leaf Anatomy and Geographic Distribution of Trema. M.S. thesis. University of Connecticut.</reference>
    <reference>Small, J. K. 1903. Flora of the Southeastern United States.... New York.</reference>
    <reference>Nevling, L. I. Jr. 1960. Trema. In: R. E. Woodson Jr. and R. W. Schery et al., eds. 1943-1981. Flora of Panama. 41 fasc. St. Louis. [Ann. Missouri Bot. Gard. 47: 108-110.]</reference>
    <reference>Standley, P. C. 1922-1926. Trees and Shrubs of Mexico. 5 parts. Washington. Part 2, p. 199. [Contr. U.S. Natl. Herb. 23.]</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade mostly longer than 5 cm, abaxially velvety white-pubescent, veins conspicuous but scarcely raised; fruits bright red-orange to yellow.</description>
      <determination>1 Trema micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade mostly shorter than 4 cm, harshly pubescent on both surfaces, venation abaxially pronounced, raised; fruits pink.</description>
      <determination>2 Trema lamarckiana</determination>
    </key_statement>
  </key>
</bio:treatment>