<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">virescens</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">carolinianum</taxon_name>
    <taxon_name authority="(Nuttall) R. E. Brooks" date="1982" rank="subspecies">virescens</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>60: 8. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection virescens;species carolinianum;subspecies virescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">233500489</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">virescens</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 14. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species virescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virescens</taxon_name>
    <taxon_name authority="(Rydberg) Cory" date="unknown" rank="variety">macroseratilis</taxon_name>
    <taxon_hierarchy>genus Delphinium;species virescens;variety macroseratilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virescens</taxon_name>
    <taxon_name authority="(Huth) L. M. Perry" date="unknown" rank="variety">penardii</taxon_name>
    <taxon_hierarchy>genus Delphinium;species virescens;variety penardii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots without elongate central axis;</text>
      <biological_entity constraint="central" id="o11830" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o11829" id="r1642" name="without" negation="false" src="d0_s0" to="o11830" />
    </statement>
    <statement id="d0_s1">
      <text>fascicles 2-8, ± horizontal.</text>
      <biological_entity id="o11829" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="8" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 50-80 cm.</text>
      <biological_entity id="o11831" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline at anthesis;</text>
      <biological_entity id="o11832" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11833" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distalmost petioles more than 5 mm.</text>
      <biological_entity constraint="distalmost" id="o11834" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade with 5 or more major divisions;</text>
      <biological_entity id="o11835" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure" />
      <biological_entity id="o11836" name="division" name_original="divisions" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
      <relation from="o11835" id="r1643" name="with" negation="false" src="d0_s5" to="o11836" />
    </statement>
    <statement id="d0_s6">
      <text>ultimate lobes of midcauline leaf-blades 5-15, width 2-6 mm.</text>
      <biological_entity constraint="ultimate" id="o11837" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11838" name="midcauline" name_original="midcauline" src="d0_s6" type="structure" />
      <biological_entity id="o11839" name="leaf-blade" name_original="leaf-blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <relation from="o11837" id="r1644" name="consist_of" negation="false" src="d0_s6" to="o11838" />
      <relation from="o11837" id="r1645" name="consist_of" negation="false" src="d0_s6" to="o11839" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals white to very pale blue.</text>
      <biological_entity id="o11840" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 16.</text>
      <biological_entity id="o11841" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s7" value="pale blue" value_original="pale blue" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11842" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, sandy, loamy, or clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="loamy" modifier="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>250-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="250" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man.; Colo., Iowa, Kans., Minn., Mo., Nebr., N.Dak., Okla., S.Dak., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36d.</number>
  <other_name type="common_name">Plains larkspur</other_name>
  <other_name type="common_name">white larkspur</other_name>
  <other_name type="common_name">Penard's larkspur</other_name>
  <discussion>Delphinium carolinianum subsp. virescens is known to hybridize with D. madrense and with D. wootonii.</discussion>
  
</bio:treatment>