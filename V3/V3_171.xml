<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="genus">corydalis</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1867" rank="species">curvisiliqua</taxon_name>
    <place_of_publication>
      <publication_title>Manual ed.</publication_title>
      <place_in_publication>5, 62. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus corydalis;species curvisiliqua</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500437</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corydalis</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">aurea</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">curvisiliqua</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>15: 75. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Corydalis;species aurea;variety curvisiliqua;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual or perhaps biennial, glaucous, from somewhat succulent roots.</text>
      <biological_entity id="o23984" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" modifier="perhaps" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23985" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="somewhat" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
      </biological_entity>
      <relation from="o23984" id="r3273" name="from" negation="false" src="d0_s0" to="o23985" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1-several, erect to ascending, 1-4 dm.</text>
      <biological_entity id="o23986" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves compound;</text>
      <biological_entity id="o23987" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 2-3 orders of leaflets and lobes;</text>
      <biological_entity id="o23988" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o23989" name="order" name_original="orders" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o23990" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o23991" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o23988" id="r3274" name="with" negation="false" src="d0_s3" to="o23989" />
      <relation from="o23989" id="r3275" name="part_of" negation="false" src="d0_s3" to="o23990" />
      <relation from="o23989" id="r3276" name="part_of" negation="false" src="d0_s3" to="o23991" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes oblong, elliptic, or obovate, margins sometimes incised, apex obtuse or rounded.</text>
      <biological_entity constraint="ultimate" id="o23992" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o23993" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o23994" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, 6-18-flowered, primary racemes usually exceeding leaves, more numerous than secondary, fewer-flowered racemes;</text>
      <biological_entity id="o23995" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="6-18-flowered" value_original="6-18-flowered" />
      </biological_entity>
      <biological_entity constraint="primary" id="o23996" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character constraint="than secondary" constraintid="o23998" is_modifier="false" name="quantity" src="d0_s5" value="more numerous" value_original="more numerous" />
      </biological_entity>
      <biological_entity id="o23997" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o23998" name="secondary" name_original="secondary" src="d0_s5" type="structure" />
      <biological_entity id="o23999" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="fewer-flowered" value_original="fewer-flowered" />
      </biological_entity>
      <relation from="o23996" id="r3277" modifier="usually" name="exceeding" negation="false" src="d0_s5" to="o23997" />
    </statement>
    <statement id="d0_s6">
      <text>bracts ovate, to 15 × 6 mm, proximal bracts sometimes leaflike, distal usually slightly to greatly reduced.</text>
      <biological_entity id="o24000" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24001" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24002" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly to greatly" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers spreading, often strongly curved;</text>
      <biological_entity id="o24003" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often strongly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel stout, 1-3 mm;</text>
      <biological_entity id="o24004" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to attenuate-ovate, ca. 1 mm, margins often sinuate or dentate;</text>
      <biological_entity id="o24005" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="attenuate-ovate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24006" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals bright-yellow;</text>
      <biological_entity id="o24007" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>spurred petal 15-18 mm, spur not appreciably incurved, 7-9 mm, apex blunt, somewhat globose, crest well developed to absent, wrinkled or dentate, marginal wing well developed, unspurred outer petal bent, 12-15 mm, crest similar to that of spurred petal;</text>
      <biological_entity id="o24008" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24009" name="spur" name_original="spur" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not appreciably" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24010" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o24011" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s11" value="developed" value_original="developed" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o24012" name="wing" name_original="wing" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s11" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24013" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="unspurred" value_original="unspurred" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bent" value_original="bent" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24014" name="crest" name_original="crest" src="d0_s11" type="structure" />
      <biological_entity id="o24015" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <relation from="o24014" id="r3278" name="part_of" negation="false" src="d0_s11" to="o24015" />
    </statement>
    <statement id="d0_s12">
      <text>inner petals oblanceolate, 9-12 mm, claw slender, nearly 1/2 petal length;</text>
      <biological_entity constraint="inner" id="o24016" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24017" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character modifier="nearly" name="quantity" src="d0_s12" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o24018" name="petal" name_original="petal" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>nectariferous spur clavate, 4-6 mm, bent near apex;</text>
      <biological_entity constraint="nectariferous" id="o24019" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character constraint="near apex" constraintid="o24020" is_modifier="false" name="shape" src="d0_s13" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o24020" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style ca. 5mm;</text>
      <biological_entity id="o24021" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma rectangular, 2-lobed, 1/2 as long as wide, with 8 papillae.</text>
      <biological_entity id="o24022" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o24023" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="8" value_original="8" />
      </biological_entity>
      <relation from="o24022" id="r3279" name="with" negation="false" src="d0_s15" to="o24023" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules erect, linear, slender, straight to strongly incurved, 20-35 mm.</text>
      <biological_entity id="o24024" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s16" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ca. 2 mm diam., appearing distinctly roughened or faintly reticulate under magnification, marginal ring present or essentially absent.</text>
      <biological_entity id="o24025" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character name="diameter" src="d0_s17" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24026" name="magnification" name_original="magnification" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="distinctly" name="relief" src="d0_s17" value="roughened" value_original="roughened" />
        <character is_modifier="true" modifier="faintly" name="relief" src="d0_s17" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o24027" name="ring" name_original="ring" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o24025" id="r3280" name="appearing" negation="false" src="d0_s17" to="o24026" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill., Iowa, Kans., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules mostly 25–35 mm, usually abruptly acute at apex; petals with no or only moderately developed crest; seeds appearing distinctly reticulate under magnification.</description>
      <determination>9a Corydalis curvisiliqua subsp. curvisiliqua</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules mostly 20–25 mm, attenuate to apex; petals with well-developed crest; seeds appearing only faintly reticulate under magnification.</description>
      <determination>9b Corydalis curvisiliqua subsp. grandibracteata</determination>
    </key_statement>
  </key>
</bio:treatment>