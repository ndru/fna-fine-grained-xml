<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Behr &amp; Kellogg" date="1884" rank="species">grayi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 5. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species grayi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500061</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">quinquefolia</taxon_name>
    <taxon_name authority="(Behr &amp; Kellogg) Jepson" date="unknown" rank="variety">grayi</taxon_name>
    <taxon_hierarchy>genus Anemone;species quinquefolia;variety grayi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">quinquefolia</taxon_name>
    <taxon_name authority="(Eastwood) Munz" date="unknown" rank="variety">minor</taxon_name>
    <taxon_hierarchy>genus Anemone;species quinquefolia;variety minor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots (3-) 10-30 (-40) cm, from rhizomes, rhizomes horizontal, occasionally ascending.</text>
      <biological_entity id="o12579" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12580" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o12581" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o12579" id="r1756" name="from" negation="false" src="d0_s0" to="o12580" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves usually absent, occasionally 1, ternate;</text>
      <biological_entity constraint="basal" id="o12582" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character modifier="occasionally" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (1.5-) 2.5-20 (-25) cm;</text>
      <biological_entity id="o12583" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet sessile or petiolulate, ovate to rhombic or oblanceolate, (1-) 1.5-4.5 (-5) × (0.6-) 1-2.5 (-3.5) cm, base cuneate, margins crenate, sometimes coarsely serrate, on distal 1/2-2/3, apex narrowly acute to acute, surfaces white-puberulous to pilose to nearly glabrous;</text>
      <biological_entity constraint="terminal" id="o12584" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolulate" value_original="petiolulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="rhombic or oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s3" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_width" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12585" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12586" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sometimes coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12587" name="1/2-2/3" name_original="1/2-2/3" src="d0_s3" type="structure" />
      <biological_entity id="o12588" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o12589" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="white-puberulous" name="pubescence" src="d0_s3" to="pilose" />
      </biological_entity>
      <relation from="o12586" id="r1757" name="on" negation="false" src="d0_s3" to="o12587" />
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets unlobed, rarely scarcely 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o12590" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="rarely scarcely" name="shape" src="d0_s4" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 4-12 mm wide.</text>
      <biological_entity constraint="ultimate" id="o12591" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o12592" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle proximally glabrous, distally villous or pilose;</text>
      <biological_entity id="o12593" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts 3, 1-tiered, ternate, ±similar to basal leaves, bases distinct;</text>
      <biological_entity id="o12594" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="ternate" value_original="ternate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12595" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o12596" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o12594" id="r1758" modifier="more or less" name="to" negation="false" src="d0_s8" to="o12595" />
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet sessile to petiolulate, ovate to rhombic or oblanceolate, (1-) 1.5-4.5 (-5) × (0.6-) 1-2.5 (-3.5) cm, bases cuneate, margins crenate, sometimes coarsely serrate, on distal 1/2-2/3, apex narrowly acute to acute, surfaces white-puberulous to pilose to nearly glabrous;</text>
      <biological_entity constraint="terminal" id="o12597" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s9" to="petiolulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="rhombic or oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s9" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s9" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_width" src="d0_s9" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s9" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s9" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12598" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12599" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sometimes coarsely" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12600" name="1/2-2/3" name_original="1/2-2/3" src="d0_s9" type="structure" />
      <biological_entity id="o12601" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
      <biological_entity id="o12602" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character char_type="range_value" from="white-puberulous" name="pubescence" src="d0_s9" to="pilose" />
      </biological_entity>
      <relation from="o12599" id="r1759" name="on" negation="false" src="d0_s9" to="o12600" />
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets unlobed, rarely scarcely 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o12603" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="rarely scarcely" name="shape" src="d0_s10" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ultimate lobes 4-12 mm wide.</text>
      <biological_entity constraint="ultimate" id="o12604" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals 5-6, white or blue, elliptic to obovate, rarely ovate, 7-15 × 4-8 mm, glabrous;</text>
      <biological_entity id="o12605" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12606" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="blue" value_original="blue" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="obovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 25-40.</text>
      <biological_entity id="o12607" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o12608" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Heads of achenes nearly spheric;</text>
      <biological_entity id="o12609" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s14" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o12610" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <relation from="o12609" id="r1760" name="part_of" negation="false" src="d0_s14" to="o12610" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel (0.5-) 3-10 cm in fruit.</text>
      <biological_entity id="o12611" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s15" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o12612" from="3" from_unit="cm" name="some_measurement" src="d0_s15" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12612" name="fruit" name_original="fruit" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Achenes: body elliptic, flat, 3-4 × 1.5-2 mm, not winged, puberulous to pilose;</text>
      <biological_entity id="o12613" name="achene" name_original="achenes" src="d0_s16" type="structure" />
      <biological_entity id="o12614" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character char_type="range_value" from="puberulous" name="pubescence" src="d0_s16" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak curved or recurved, 0.6-1 mm, glabrous.</text>
      <biological_entity id="o12615" name="achene" name_original="achenes" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n=16.</text>
      <biological_entity id="o12616" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12617" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–early summer (Feb–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="winter" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded woods, wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded woods" />
        <character name="habitat" value="wooded slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Western windflower</other_name>
  <discussion>The rhizomes of Anemone grayi tend to be knobby rather than filiform as in A. lyallii and A. oregana. Investigation of the populations in which these three species overlap and appear to intergrade in southern Oregon and northern California is needed to clarify the taxonomic relationships among them.</discussion>
  
</bio:treatment>