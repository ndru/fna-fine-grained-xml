<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce A. Ford</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ACTAEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 504. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 222. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ACTAEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, aktea, ancient name for the elder, probably for leaf similarity</other_info_on_name>
    <other_info_on_name type="fna_id">100421</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from caudices ca. 1 cm thick.</text>
      <biological_entity id="o18662" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18663" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character name="thickness" src="d0_s0" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o18662" id="r2498" name="from" negation="false" src="d0_s0" to="o18663" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline, alternate, petiolate.</text>
      <biological_entity id="o18664" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade 1-3-ternately or pinnately compound;</text>
      <biological_entity id="o18665" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets ovate to narrowly elliptic, unlobed to 3-lobed, margins sharply cleft, irregularly dentate.</text>
      <biological_entity id="o18666" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="narrowly elliptic unlobed" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="narrowly elliptic unlobed" />
      </biological_entity>
      <biological_entity id="o18667" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or axillary, 25 (-more) -flowered racemes, 2-17 cm;</text>
      <biological_entity id="o18668" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o18669" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="25-flowered" value_original="25-flowered" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts leaflike, sometimes present between leaves and inflorescence, bracteoles 1-2, at base of each pedicel, not forming involucre.</text>
      <biological_entity id="o18670" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character constraint="between leaves, inflorescence" constraintid="o18671, o18672" is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18671" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o18672" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure" />
      <biological_entity id="o18673" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o18674" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18675" name="pedicel" name_original="pedicel" src="d0_s5" type="structure" />
      <biological_entity id="o18676" name="involucre" name_original="involucre" src="d0_s5" type="structure" />
      <relation from="o18673" id="r2499" name="at" negation="false" src="d0_s5" to="o18674" />
      <relation from="o18674" id="r2500" name="part_of" negation="false" src="d0_s5" to="o18675" />
      <relation from="o18673" id="r2501" name="forming" negation="true" src="d0_s5" to="o18676" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o18677" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals not persistent in fruit, 3-5, whitish green, plane, orbiculate, 2-4.5 mm;</text>
      <biological_entity id="o18678" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o18679" is_modifier="false" modifier="not" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="whitish green" value_original="whitish green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s7" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18679" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 4-10, distinct, cream colored, plane, spatulate to obovate, clawed, 2-4.5 mm;</text>
      <biological_entity id="o18680" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream colored" value_original="cream colored" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s8" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary absent;</text>
      <biological_entity id="o18681" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 15-50;</text>
      <biological_entity id="o18682" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments filiform;</text>
      <biological_entity id="o18683" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o18684" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character constraint="between stamens, pistils" constraintid="o18685, o18686" is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18685" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o18686" name="pistil" name_original="pistils" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pistil 1, simple;</text>
      <biological_entity id="o18687" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules many per pistil;</text>
      <biological_entity id="o18688" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character constraint="per pistil" constraintid="o18689" is_modifier="false" name="quantity" src="d0_s14" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o18689" name="pistil" name_original="pistil" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style very short or absent.</text>
      <biological_entity id="o18690" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits berries, solitary, sessile, broadly ellipsoid to nearly globose, sides smooth;</text>
      <biological_entity constraint="fruits" id="o18691" name="berry" name_original="berries" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s16" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s16" to="nearly globose" />
      </biological_entity>
      <biological_entity id="o18692" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak a wart, terminal, to 1 mm.</text>
      <biological_entity id="o18693" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18694" name="wart" name_original="wart" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds dark-brown to reddish-brown, obconic to wedge-shaped, rugulose.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 8.</text>
      <biological_entity id="o18695" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s18" to="reddish-brown" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s18" to="wedge-shaped" />
        <character is_modifier="false" name="relief" src="d0_s18" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="x" id="o18696" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate to cool forests throughout Northern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate to cool forests throughout Northern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Baneberry</other_name>
  <other_name type="common_name">necklaceweed</other_name>
  <other_name type="common_name">cohosh</other_name>
  <other_name type="common_name">actée</other_name>
  <discussion>Species ca. 8 (2 in the flora).</discussion>
  <discussion>The two species in North America are similar to each other vegetatively and differ primarily in flower and fruit characteristics.</discussion>
  <references>
    <reference>Hultén, E. 1971. The circumpolar plants. II. Dicotyledons. Kongl. Svenska Vetenskapsakad. Handl., ser. 4, 13: 1-463.</reference>
    <reference>Keener, C.S. 1977. Studies in the Ranunculaceae of the southeastern United States. VI. Miscellaneous genera. Sida 7: 1-12.</reference>
    <reference>Pellmyr, O. 1985. The pollination biology of Actaea pachypoda and A. rubra (including A. erythrocarpa) in northern Michigan and Finland. Bull. Torrey Bot. Club 112: 265-273.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicel in fruit bright red, stout, (0.7–)0.9–2.2(–3) mm diam., ± as thick as axis of raceme; stigma at anthesis as broad as or broader than ovary, 1.5–2.8 mm diam. in flower and fruit; petals truncate or cleft, often antherlike at apex; berries white, very rarely red.</description>
      <determination>1 Actaea pachypoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicel in fruit dull green or brown, slender, 0.3–0.7 mm diam., thinner than axis of raceme; stigma at anthesis narrower than ovary, 0.7–1.2 mm diam. in flower and fruit; petals acute or obtuse, not cleft or antherlike at apex; berries red or white.</description>
      <determination>2 Actaea rubra</determination>
    </key_statement>
  </key>
</bio:treatment>