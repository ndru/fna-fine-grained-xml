<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="species">occidentalis</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="variety">brevistylis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 14. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species occidentalis;variety brevistylis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501180</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="subspecies">insularis</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species occidentalis;subspecies insularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to reclining, 2-3 mm thick, pilose or glabrous.</text>
      <biological_entity id="o16071" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="reclining" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaf-blades 3-parted, ultimate segments elliptic or narrowly elliptic, margins dentate.</text>
      <biological_entity constraint="basal" id="o16072" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16073" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o16074" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 5, 5-7 mm;</text>
      <biological_entity id="o16075" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o16076" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="5" value_original="5" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals 5-6, 8-12 × 3-8 mm.</text>
      <biological_entity id="o16077" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o16078" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="6" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Achenes 3-3.4 × 2.6-3 mm, glabrous;</text>
      <biological_entity id="o16079" name="achene" name_original="achenes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>beak lanceolate, curved, 1.2-1.4 mm. 2n = 28.</text>
      <biological_entity id="o16080" name="beak" name_original="beak" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16081" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal and mountain meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9e.</number>
  <discussion>Ranunculus occidentalis var. brevistylis may be difficult to distinguish from var. occidentalis; the two have sometimes been combined. The pubescence character distinguishing them is well correlated with geography, however, so I am provisionally maintaining both of them.</discussion>
  
</bio:treatment>