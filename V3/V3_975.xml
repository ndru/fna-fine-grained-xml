<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Rydberg) N. I. Malyutin" date="1987" rank="subsection">Bicoloria</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>72: 687. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection bicoloria;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302003</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="section">Bicoloria</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Rocky Mts.,</publication_title>
      <place_in_publication>309. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;section Bicoloria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 2-9-branched at least 1 cm from stem attachment, (5-) 10-30 (-40) cm, diffuse-fibrous, ± braided, dry;</text>
      <biological_entity id="o22262" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="at least" name="architecture" src="d0_s0" value="2-9-branched" value_original="2-9-branched" />
        <character constraint="from stem attachment" constraintid="o22263" name="location" src="d0_s0" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s0" value="diffuse-fibrous" value_original="diffuse-fibrous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s0" value="braided" value_original="braided" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s0" value="dry" value_original="dry" />
      </biological_entity>
      <biological_entity constraint="stem" id="o22263" name="attachment" name_original="attachment" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>buds minute.</text>
      <biological_entity id="o22264" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (-2) per root, usually unbranched, elongation delayed 2-6 (-10) weeks after leaf initiation;</text>
      <biological_entity id="o22265" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character constraint="per root" constraintid="o22266" name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o22266" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o22267" name="elongation" name_original="elongation" src="d0_s2" type="structure" />
      <biological_entity id="o22268" name="week" name_original="weeks" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o22269" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <relation from="o22267" id="r3048" name="delayed" negation="false" src="d0_s2" to="o22268" />
      <relation from="o22267" id="r3049" name="after" negation="false" src="d0_s2" to="o22269" />
    </statement>
    <statement id="d0_s3">
      <text>base ± narrowed, ± tenuously attached to root;</text>
      <biological_entity id="o22270" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to root" constraintid="o22271" is_modifier="false" modifier="more or less tenuously" name="fixation" src="d0_s3" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o22271" name="root" name_original="root" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal internodes much shorter than those of midstem.</text>
      <biological_entity constraint="proximal" id="o22272" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline, largest leaves near base of stem, others often abruptly smaller on distal portion of stem;</text>
      <biological_entity id="o22273" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="largest" id="o22274" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o22275" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o22276" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o22277" name="other" name_original="others" src="d0_s5" type="structure">
        <character constraint="on distal portion" constraintid="o22278" is_modifier="false" modifier="often abruptly" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22278" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o22279" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o22274" id="r3050" name="near" negation="false" src="d0_s5" to="o22275" />
      <relation from="o22275" id="r3051" name="part_of" negation="false" src="d0_s5" to="o22276" />
      <relation from="o22278" id="r3052" name="part_of" negation="false" src="d0_s5" to="o22279" />
    </statement>
    <statement id="d0_s6">
      <text>basal petioles spreading, cauline petioles ascending;</text>
      <biological_entity constraint="basal" id="o22280" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o22281" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade shape and lobing similar throughout.</text>
      <biological_entity id="o22282" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s7" value="lobing" value_original="lobing" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually with 2-6 flowers per 5 cm, open, ± pyramidal, seldom more than 3 times longer than wide, spurs rarely intersecting rachis;</text>
      <biological_entity id="o22283" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="per 5 cm" name="architecture" notes="" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s8" value="3+" value_original="3+" />
      </biological_entity>
      <biological_entity id="o22284" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o22285" name="spur" name_original="spurs" src="d0_s8" type="structure" />
      <biological_entity id="o22286" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <relation from="o22283" id="r3053" name="with" negation="false" src="d0_s8" to="o22284" />
      <relation from="o22285" id="r3054" modifier="rarely" name="intersecting" negation="false" src="d0_s8" to="o22286" />
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading, usually more than 1.5 cm, rachis to midpedicel angle more than 30°;</text>
      <biological_entity id="o22287" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1.5" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s9" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22288" name="rachis" name_original="rachis" src="d0_s9" type="structure" />
      <biological_entity constraint="midpedicel" id="o22289" name="angle" name_original="angle" src="d0_s9" type="structure">
        <character name="degree" src="d0_s9" value="30+°" value_original="30+°" />
      </biological_entity>
      <relation from="o22288" id="r3055" name="to" negation="false" src="d0_s9" to="o22289" />
    </statement>
    <statement id="d0_s10">
      <text>bracts markedly smaller and fewer lobed than leaves.</text>
      <biological_entity id="o22290" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="markedly" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o22291" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits spreading.</text>
      <biological_entity id="o22292" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds obpyramidal, 1.6-2.9 × 0.7-1.9 mm, ± ringed at proximal end, usually wing-margined;</text>
      <biological_entity id="o22293" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s12" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.9" to_unit="mm" />
        <character constraint="at proximal end" constraintid="o22294" is_modifier="false" modifier="more or less" name="relief" src="d0_s12" value="ringed" value_original="ringed" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s12" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22294" name="end" name_original="end" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>seed-coats lacking wavy ridges, cells elongate, cell margins straight.</text>
      <biological_entity id="o22295" name="seed-coat" name_original="seed-coats" src="d0_s13" type="structure" />
      <biological_entity id="o22296" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="shape" src="d0_s13" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o22297" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="cell" id="o22298" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.8.</number>
  <discussion>Species 6 (6 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals red or yellow.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals usually bluish, not red or yellow (sometimes maroon, white, or pink).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals red to reddish orange, not waxy.</description>
      <determination>47 Delphinium nudicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals bright yellow, appearing waxy.</description>
      <determination>48 Delphinium luteum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sepals maroon; plants distinctly different from other individuals within populations.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sepals blue or pink; plants usually similar to other individuals within populations.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf segments more than 5, 5 mm or less wide.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf segments 5 or fewer, more than 5 mm wide.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal portion of stem hairy.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal portion of stem glabrous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves mostly above proximal 1/3 of stem; flowers usually more than 15 per main inflorescence axis.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves mostly on proximal 1/3 of stem; flowers usually fewer than 20 per main inflorescence axis.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Sepals pinkish; plants distinctly different from other individuals within populations.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Sepals dark blue to white; plants usually similar to other individuals within populations.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants in populations with red flowers.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants in populations with blue flowers.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves ± succulent, clustered on proximal portion of stem.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves not succulent, clustered on proximal portion of stem or not.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Lateral sepals dark blue to white, spreading to reflexed.</description>
      <determination>46 Delphinium antoninum</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Lateral sepals dark blue, spreading.</description>
      <determination>44 Delphinium glareosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Lower petal blade cleft usually less than 1/3 blade length.</description>
      <determination>43 Delphinium bicolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Lower petal blade cleft more than 1/3 blade length.</description>
      <determination>45 Delphinium basalticum</determination>
    </key_statement>
  </key>
</bio:treatment>