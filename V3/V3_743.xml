<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="(Ashe) Fernald" date="1945" rank="species">uber</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>47: 325. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species uber;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500266</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Betula</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">lenta</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="variety">uber</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>20: 64. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Betula;species lenta;variety uber;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, slender, to 10 m.</text>
      <biological_entity id="o19698" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark-brown, smooth, close.</text>
      <biological_entity id="o19699" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="close" value_original="close" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs with taste and odor of wintergreen when crushed, glabrous, covered with small resinous glands.</text>
      <biological_entity id="o19700" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when crushed" name="odor" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19701" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="true" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o19700" id="r2649" name="covered with" negation="false" src="d0_s2" to="o19701" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade nearly orbiculate to broadly elliptic with 2–6 pairs of lateral-veins, 2–5 × 2–4 cm, base rounded to cordate or truncate, margins irregularly serrate or dentate, apex broadly obtuse to rounded;</text>
      <biological_entity id="o19702" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="with pairs" constraintid="o19703" from="nearly orbiculate" name="shape" src="d0_s3" to="broadly elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" notes="" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19703" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o19704" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure" />
      <biological_entity id="o19705" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o19706" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19707" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly obtuse" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <relation from="o19703" id="r2650" name="part_of" negation="false" src="d0_s3" to="o19704" />
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially glabrous to sparsely pubescent, especially along major veins and in vein-axils, often with scattered resinous glands.</text>
      <biological_entity id="o19708" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="abaxially glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity id="o19709" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o19710" name="vein-axil" name_original="vein-axils" src="d0_s4" type="structure" />
      <biological_entity id="o19711" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o19708" id="r2651" modifier="especially" name="along" negation="false" src="d0_s4" to="o19709" />
      <relation from="o19708" id="r2652" name="in" negation="false" src="d0_s4" to="o19710" />
      <relation from="o19708" id="r2653" modifier="often" name="with" negation="false" src="d0_s4" to="o19711" />
    </statement>
    <statement id="d0_s5">
      <text>Infructescences erect, ellipsoid-cylindric, 1–2 × 1–1.5 cm, shattering with fruits in fall;</text>
      <biological_entity id="o19712" name="infructescence" name_original="infructescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ellipsoid-cylindric" value_original="ellipsoid-cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19713" name="fruit" name_original="fruits" src="d0_s5" type="structure" />
      <relation from="o19712" id="r2654" name="shattering with" negation="false" src="d0_s5" to="o19713" />
    </statement>
    <statement id="d0_s6">
      <text>scales glabrous, lobes diverging distal to middle, central lobe ascending, shorter than lateral lobes.</text>
      <biological_entity id="o19714" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19715" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="diverging" value_original="diverging" />
        <character char_type="range_value" from="distal" name="position" src="d0_s6" to="middle" />
      </biological_entity>
      <biological_entity constraint="central" id="o19716" name="lobe" name_original="lobe" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character constraint="than lateral lobes" constraintid="o19717" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19717" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Samaras with wings narrower than to as wide as body, broadest near summit, extended beyond body apically.</text>
      <biological_entity id="o19718" name="samara" name_original="samaras" src="d0_s7" type="structure">
        <character constraint="than to as-wide-as body" constraintid="o19720" is_modifier="false" name="width" src="d0_s7" value="narrower" value_original="narrower" />
        <character constraint="near summit" constraintid="o19721" is_modifier="false" name="width" src="d0_s7" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o19722" is_modifier="false" name="size" notes="" src="d0_s7" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o19719" name="wing" name_original="wings" src="d0_s7" type="structure" />
      <biological_entity id="o19720" name="body" name_original="body" src="d0_s7" type="structure" />
      <biological_entity id="o19721" name="summit" name_original="summit" src="d0_s7" type="structure" />
      <biological_entity id="o19722" name="body" name_original="body" src="d0_s7" type="structure" />
      <relation from="o19718" id="r2655" name="with" negation="false" src="d0_s7" to="o19719" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks and adjacent flood plains in rich mesic forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="adjacent flood plains" />
        <character name="habitat" value="rich mesic forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Virginia roundleaf birch</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Betula uber, described in 1918, was not seen again until its widely celebrated rediscovery in 1974 (P. M. Mazzeo 1974; C. F. Reed 1975; D. W. Ogle and P. M. Mazzeo 1976; D. J. Preston 1976). It is apparently allied to B. lenta (W. J. Hayden and S. M. Hayden 1984; T. L. Sharik and R. H. Ford 1984); whether it constitutes a separate species or simply mutant individuals of B. lenta is a matter of controversy. Seeds obtained from the original single extant population of 17 trees and grown at the U.S. National Arboretum have produced an apparent hybrid swarm of offspring varying in leaf characteristics from those of B. uber to those of B. lenta (with which it occurs).</discussion>
  
</bio:treatment>