<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">alnus</taxon_name>
    <taxon_name authority="(Chaix) de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="species">viridis</taxon_name>
    <taxon_name authority="(Regel) A. Löve &amp; D. Löve" date="1965" rank="subspecies">sinuata</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. Biol.</publication_title>
      <place_in_publication>17: 20. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus alnus;species viridis;subspecies sinuata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500044</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alnus</taxon_name>
    <taxon_name authority="(Aiton) Pursh" date="unknown" rank="species">viridis</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="variety">(d) sinuata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>38(3): 422. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Alnus;species viridis;variety (d) sinuata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alnus</taxon_name>
    <taxon_name authority="(Regel) Rydberg" date="unknown" rank="species">crispa</taxon_name>
    <taxon_name authority="(Regel) Hultén" date="unknown" rank="subspecies">sinuata</taxon_name>
    <taxon_hierarchy>genus Alnus;species crispa;subspecies sinuata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alnus</taxon_name>
    <taxon_name authority="(Regel) Sargent" date="unknown" rank="species">sinuata</taxon_name>
    <taxon_hierarchy>genus Alnus;species sinuata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sitchensis</taxon_name>
    <taxon_hierarchy>genus Alnus;species sitchensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, spreading, to 5 (–10) m.</text>
      <biological_entity id="o6747" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark light gray to reddish-brown;</text>
      <biological_entity id="o6748" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="light gray" name="coloration" src="d0_s1" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lenticels inconspicuous.</text>
      <biological_entity id="o6749" name="lenticel" name_original="lenticels" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade light or yellowish green, narrowly to broadly ovate, 4–10 × 3–8 cm, thin, papery, base rounded to cordate, margins flat, sharply and coarsely doubly serrate, apex acuminate;</text>
      <biological_entity id="o6750" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o6751" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
      <biological_entity id="o6752" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sharply; coarsely doubly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6753" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially glabrous to sparsely pubescent, lightly to moderately resin-coated.</text>
      <biological_entity id="o6754" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="abaxially glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
        <character is_modifier="false" modifier="lightly to moderately" name="coating" src="d0_s4" value="resin-coated" value_original="resin-coated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: staminate catkins 2.5–13 cm.</text>
      <biological_entity id="o6755" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o6756" name="catkin" name_original="catkins" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s5" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Infructescences 1.5–2.5 × 0.8–1.3 cm;</text>
      <biological_entity id="o6757" name="infructescence" name_original="infructescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s6" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles 1–3 cm. 2n = 28.</text>
      <biological_entity id="o6758" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6759" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along gravelly or rocky stream banks, lakeshores, and coasts, on moist rocky slopes, outcrops, in open coniferous woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="along" />
        <character name="habitat" value="rocky stream banks" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="coasts" constraint="on moist rocky slopes" />
        <character name="habitat" value="moist rocky slopes" />
        <character name="habitat" value="outcrops" constraint="in open coniferous" />
        <character name="habitat" value="open coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Calif., Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7c.</number>
  <other_name type="common_name">Sitka alder</other_name>
  <other_name type="common_name">mountain alder</other_name>
  <discussion>Alnus viridis subsp. sinuata is one of the first successional taxa to appear in the northwestern mountains following disruption of the mature vegetation. It often forms dense thickets on avalanche and talus slopes. Sitka alder differs from the two previous subspecies in its paper-thin, light or yellowish green, doubly serrate leaves.</discussion>
  <discussion>The Bella Coola used Alnus viridis subsp. sinuata medicinally although D. E. Moerman (1986) did not specify the nature of the remedies.</discussion>
  
</bio:treatment>