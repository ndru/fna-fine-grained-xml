<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">aristolochiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">asarum</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 294. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aristolochiaceae;genus asarum;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500171</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes horizontal, shallow, internodes (0.6-) 1-4 cm.</text>
      <biological_entity id="o3095" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o3096" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (3.5-) 6-26 cm, sparsely crisped-hirsute.</text>
      <biological_entity id="o3097" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o3098" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="26" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="crisped-hirsute" value_original="crisped-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade not variegate, cordate to almost reniform, 4-6.5 × 7-10.5 cm, apex broadly rounded-acute to rounded;</text>
      <biological_entity id="o3099" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly rounded-acute" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o3100" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="cordate to almost" value_original="cordate to almost" />
        <character is_modifier="true" modifier="almost" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="4" from_unit="cm" is_modifier="true" name="length" src="d0_s2" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" is_modifier="true" name="width" src="d0_s2" to="10.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3101" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="cordate to almost" value_original="cordate to almost" />
        <character is_modifier="true" modifier="almost" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="4" from_unit="cm" is_modifier="true" name="length" src="d0_s2" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" is_modifier="true" name="width" src="d0_s2" to="10.5" to_unit="cm" />
      </biological_entity>
      <relation from="o3099" id="r401" name="variegate" negation="false" src="d0_s2" to="o3100" />
      <relation from="o3099" id="r402" name="variegate" negation="false" src="d0_s2" to="o3101" />
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially sparsely appressed-hirsute, adaxially glabrous or sparsely hirsute along veins, marginal hairs perpendicular to margin or curved toward apex.</text>
      <biological_entity id="o3102" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s3" value="appressed-hirsute" value_original="appressed-hirsute" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="along veins" constraintid="o3103" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o3103" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity constraint="marginal" id="o3104" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character constraint="to " constraintid="o3106" is_modifier="false" name="orientation" src="d0_s3" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o3105" name="margin" name_original="margin" src="d0_s3" type="structure" />
      <biological_entity id="o3106" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers descending;</text>
      <biological_entity id="o3107" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="descending" value_original="descending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle 2-3.</text>
      <biological_entity id="o3108" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx-tube cylindric, externally reddish, at least in part, glabrous or sparsely hirsute, internally white, rarely with longitudinal red stripes, with white or purple hairs;</text>
      <biological_entity id="o3109" name="calyx-tube" name_original="calyx-tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="externally" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="internally" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o3110" name="part" name_original="part" src="d0_s6" type="structure" />
      <biological_entity id="o3111" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o3109" id="r403" modifier="at-least" name="in" negation="false" src="d0_s6" to="o3110" />
      <relation from="o3109" id="r404" modifier="with longitudinal red stripes" name="with" negation="false" src="d0_s6" to="o3111" />
    </statement>
    <statement id="d0_s7">
      <text>distal portion of sepal strongly reflexed at anthesis, 4-8 mm, apex acute to apiculate or short-acuminate, abaxially purple, glabrous or sparsely hirsute, adaxially red, puberulent with crisped pale or purple hairs;</text>
      <biological_entity constraint="sepal" id="o3112" name="portion" name_original="portion" src="d0_s7" type="structure" constraint_original="sepal distal; sepal">
        <character constraint="at anthesis" is_modifier="false" modifier="strongly" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3113" name="sepal" name_original="sepal" src="d0_s7" type="structure" />
      <biological_entity id="o3114" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="apiculate or short-acuminate" />
        <character is_modifier="false" modifier="abaxially" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character constraint="with hairs" constraintid="o3115" is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3115" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="crisped" value_original="crisped" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o3112" id="r405" name="part_of" negation="false" src="d0_s7" to="o3113" />
    </statement>
    <statement id="d0_s8">
      <text>pollen-sacs 1-1.5 mm, sterile tip of connective on inner stamens purple or brown, 0.5-1 mm, shorter than pollen-sacs.</text>
      <biological_entity id="o3116" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3118" name="connective" name_original="connective" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o3119" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o3120" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure" />
      <relation from="o3117" id="r406" name="part_of" negation="false" src="d0_s8" to="o3118" />
      <relation from="o3117" id="r407" name="on" negation="false" src="d0_s8" to="o3119" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 26.</text>
      <biological_entity id="o3117" name="tip" name_original="tip" src="d0_s8" type="structure" constraint="connective" constraint_original="connective; connective">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
        <character constraint="than pollen-sacs" constraintid="o3120" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3121" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet places, usually near creeks, in understory of conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet places" />
        <character name="habitat" value="near creeks" modifier="usually" />
        <character name="habitat" value="understory" constraint="of conifer forests" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Asarum lemmonii is endemic to the Sierra Nevada.</discussion>
  
</bio:treatment>