<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="T. Lestiboudois ex Dumortier" date="unknown" rank="family">platanaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">platanus</taxon_name>
    <taxon_name authority="S. Watson" date="1875" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 349. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family platanaceae;genus platanus;species wrightii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500964</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 25 m;</text>
      <biological_entity id="o6608" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="25" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks straight and erect to inclined or basally reclining or prostrate, to 1.2 (-2) m diam., lower branches becoming thick, contorted.</text>
      <biological_entity id="o6609" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="inclined or basally reclining or prostrate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="1.2" to_unit="m" />
      </biological_entity>
      <biological_entity constraint="lower" id="o6610" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="becoming" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules entire to somewhat dentate.</text>
      <biological_entity id="o6611" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6612" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s2" value="entire to somewhat" value_original="entire to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade dark green, deeply 3-5 (-7) -lobed, 9-25 × 9-30 cm, rather thick;</text>
      <biological_entity id="o6613" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="width" src="d0_s3" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="rather" name="width" src="d0_s3" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lobes of blade much longer than wide, basal lobes usually smaller and spreading, not reflexed, sinuses broad and very deeply concave, depth of distal sinuses equal or greater than distance from sinus to base of blade, terminal leaf lobe about 2/3 or more length of blade;</text>
      <biological_entity id="o6614" name="lobe" name_original="lobes" src="d0_s4" type="structure" constraint="blade" constraint_original="blade; blade">
        <character constraint="than wide , basal lobes" constraintid="o6616" is_modifier="false" name="length_or_size" src="d0_s4" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o6615" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o6616" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s4" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o6617" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="very deeply" name="shape" src="d0_s4" value="concave" value_original="concave" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character constraint="from sinus" constraintid="o6619" is_modifier="false" name="size" src="d0_s4" value="greater" value_original="greater" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6618" name="sinuse" name_original="sinuses" src="d0_s4" type="structure" />
      <biological_entity id="o6619" name="sinus" name_original="sinus" src="d0_s4" type="structure" />
      <biological_entity id="o6620" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o6621" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o6622" name="lobe" name_original="lobe" src="d0_s4" type="structure" constraint_original="terminal leaf">
        <character name="quantity" src="d0_s4" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o6623" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <relation from="o6614" id="r922" name="part_of" negation="false" src="d0_s4" to="o6615" />
      <relation from="o6617" id="r923" name="part_of" negation="false" src="d0_s4" to="o6618" />
      <relation from="o6619" id="r924" name="to" negation="false" src="d0_s4" to="o6620" />
      <relation from="o6620" id="r925" name="part_of" negation="false" src="d0_s4" to="o6621" />
    </statement>
    <statement id="d0_s5">
      <text>margins entire to serrulate, apex acute to acuminate;</text>
      <biological_entity id="o6624" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="entire to serrulate" value_original="entire to serrulate" />
      </biological_entity>
      <biological_entity id="o6625" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially and adaxially glabrescent.</text>
      <biological_entity id="o6626" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate inflorescences: heads (1-) 2-4;</text>
      <biological_entity id="o6627" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6628" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fruiting heads to 20 mm diam., lateral heads sessile or pedunculate, peduncle often eventually obscured by maturing achenes;</text>
      <biological_entity id="o6629" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6630" name="head" name_original="heads" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o6631" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o6632" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character constraint="by achenes" constraintid="o6633" is_modifier="false" modifier="often eventually" name="prominence" src="d0_s8" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o6633" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="maturing" value_original="maturing" />
      </biological_entity>
      <relation from="o6629" id="r926" name="fruiting" negation="false" src="d0_s8" to="o6630" />
    </statement>
    <statement id="d0_s9">
      <text>fruiting rachis to 25 cm.</text>
      <biological_entity id="o6634" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6635" name="rachis" name_original="rachis" src="d0_s9" type="structure" />
      <relation from="o6634" id="r927" name="fruiting" negation="false" src="d0_s9" to="o6635" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes 5-8 mm, basal hairs about 2/3 or equal to length of achenes.</text>
      <biological_entity id="o6636" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6638" name="achene" name_original="achenes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 42.</text>
      <biological_entity constraint="basal" id="o6637" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2/3" value_original="2/3" />
        <character constraint="to " constraintid="o6638" is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6639" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring; fruiting late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="late fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Often abundant in riparian forests, especially in montane canyons, extending into deserts along streams and near springs, and cultivated</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian forests" modifier="often abundant in" />
        <character name="habitat" value="montane canyons" />
        <character name="habitat" value="deserts" constraint="along streams and near springs" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="near springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-2000+m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000+" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Sinaloa, and Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Arizona sycamore</other_name>
  <discussion>L. D. Benson (1943) reported intermediates between Platanus wrightii and P. racemosa in southern California. He reduced P. wrightii to P. racemosa var. wrightii (S. Watson) L. Benson. Most authors have retained that taxon at the specific level because of its more deeply lobed, more glabrate leaves and its often pedunculate fruiting heads. Very low yields of germinable seeds were obtained from crosses of P. wrightii and P. racemosa with P. occidentalis (F. S. Santamour Jr. 1972b).</discussion>
  
</bio:treatment>