<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="species">inamoenus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 91. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species inamoenus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501165</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 5-33 cm, pilose or glabrous, each with 3-7 flowers.</text>
      <biological_entity id="o1302" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="33" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1303" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s0" to="7" />
      </biological_entity>
      <relation from="o1302" id="r160" name="with" negation="false" src="d0_s0" to="o1303" />
    </statement>
    <statement id="d0_s1">
      <text>Roots slender, 0.6-1.2 mm thick.</text>
      <biological_entity id="o1304" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="thickness" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves persistent, blades ovate, obovate or orbiculate, rarely reniform, undivided or innermost with 2 clefts or partings near apex, 1-3.7 × 1.1-3.5 cm, base acute to rounded, margins entire, apex rounded.</text>
      <biological_entity constraint="basal" id="o1305" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o1306" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o1307" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s2" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" notes="" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1308" name="cleft" name_original="clefts" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1309" name="parting" name_original="partings" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1310" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o1311" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o1312" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1313" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o1307" id="r161" name="with" negation="false" src="d0_s2" to="o1308" />
      <relation from="o1307" id="r162" name="with" negation="false" src="d0_s2" to="o1309" />
      <relation from="o1308" id="r163" name="near" negation="false" src="d0_s2" to="o1310" />
      <relation from="o1309" id="r164" name="near" negation="false" src="d0_s2" to="o1310" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: pedicels appressed-pubescent;</text>
      <biological_entity id="o1314" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1315" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>receptacle pilose or glabrous;</text>
      <biological_entity id="o1316" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1317" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 3-5 × 2-3 mm, abaxially pilose, hairs colorless;</text>
      <biological_entity id="o1318" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1319" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o1320" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="colorless" value_original="colorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5, 4-9 × 2-5 mm;</text>
      <biological_entity id="o1321" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1322" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectary scale glabrous.</text>
      <biological_entity id="o1323" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="nectary" id="o1324" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads of achenes cylindric, 7-17 × 5-8 mm;</text>
      <biological_entity id="o1325" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1326" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <relation from="o1325" id="r165" name="part_of" negation="false" src="d0_s8" to="o1326" />
    </statement>
    <statement id="d0_s9">
      <text>achenes 1.5-2 × 1.3-1.8 mm, canescent or glabrous;</text>
      <biological_entity id="o1327" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak subulate, straight or hooked, 0.4-2 mm.</text>
      <biological_entity id="o1328" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Ariz., Colo., Idaho, Mont., N.Mex., Nebr., Nev., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <discussion>The Navaho-Ramah considered Ranunculus inamoenus to be an effective hunting medicine, used to protect hunters from their prey (D. E. Moerman 1986).</discussion>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 3–5 mm; beaks of achenes 0.4–0.9 mm.</description>
      <determination>37a Ranunculus inamoenus var. inamoenus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 5–7 mm; beaks of achenes 1.4–2 mm.</description>
      <determination>37b Ranunculus inamoenus var. subaffinis</determination>
    </key_statement>
  </key>
</bio:treatment>