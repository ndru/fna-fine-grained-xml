<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="(Bunge) L. D. Benson" date="1940" rank="subgenus">Oxygraphis</taxon_name>
    <taxon_name authority="de Candolle" date="1817" rank="species">kamtschaticus</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 302. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus oxygraphis;species kamtschaticus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501169</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxygraphis</taxon_name>
    <taxon_name authority="(Fischer ex de Candolle) Bunge" date="unknown" rank="species">glacialis</taxon_name>
    <taxon_hierarchy>genus Oxygraphis;species glacialis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect from short caudices or rhizomes, not rooting nodally, glabrous, not bulbous-based.</text>
      <biological_entity id="o18367" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from rhizomes" constraintid="o18369" is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not; nodally" name="architecture" notes="" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o18368" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o18369" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Tuberous roots absent.</text>
      <biological_entity id="o18370" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades circular to ovate or obovate, undivided, 0.8-2.6 × 0.6-1.4 cm, margins entire or serrulate, apex obtuse or rounded.</text>
      <biological_entity constraint="basal" id="o18371" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="circular" name="shape" src="d0_s2" to="ovate or obovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s2" to="2.6" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s2" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18372" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o18373" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o18374" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o18375" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals spreading, 4-8 × 2-4 mm, glabrous;</text>
      <biological_entity id="o18376" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o18377" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals yellow, 5-12 × 1-3 mm.</text>
      <biological_entity id="o18378" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18379" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes depressed-hemispheric or conic, 3-4 × 5 mm;</text>
      <biological_entity id="o18380" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="depressed-hemispheric" value_original="depressed-hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18381" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o18380" id="r2469" name="part_of" negation="false" src="d0_s6" to="o18381" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2-2.6 × 1-1.4 mm, glabrous;</text>
      <biological_entity id="o18382" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, lanceolate, straight, 0.8-1.2 mm. 2n = 16.</text>
      <biological_entity id="o18383" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18384" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra on moist slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" constraint="on moist slopes" />
        <character name="habitat" value="moist slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>70.</number>
  
</bio:treatment>