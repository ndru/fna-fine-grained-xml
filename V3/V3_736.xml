<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Smith in J. Sibthorp &amp; J. E. Smith" date="1809" rank="genus">nuphar</taxon_name>
    <taxon_name authority="Durand in G. W. Clinton" date="1866" rank="species">variegata</taxon_name>
    <place_of_publication>
      <publication_title>in G. W. Clinton,  Rep. (Annual) Regents Univ. State New York State Cab. Nat. Hist.</publication_title>
      <place_in_publication>19: 73. 1866 (as variegatum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nuphar;species variegata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500821</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="Provancher" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Nuphar;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="(G. S. Miller &amp; Standley) Standley" date="unknown" rank="species">fraterna</taxon_name>
    <taxon_hierarchy>genus Nuphar;species fraterna;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="(Linnaeus) Smith" date="unknown" rank="species">lutea</taxon_name>
    <taxon_name authority="(Durand) E. O. Beal" date="unknown" rank="subspecies">variegata</taxon_name>
    <taxon_hierarchy>genus Nuphar;species lutea;subspecies variegata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="(Provancher) G. S. Miller &amp; Standley" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Nymphaea;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="G. S. Miller &amp; Standley" date="unknown" rank="species">fraterna</taxon_name>
    <taxon_hierarchy>genus Nymphaea;species fraterna;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes 2.5-7 cm diam.</text>
      <biological_entity id="o20778" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s0" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly floating, occasionally submersed;</text>
      <biological_entity id="o20779" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
        <character is_modifier="false" modifier="occasionally" name="location" src="d0_s1" value="submersed" value_original="submersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole adaxially flattened, with median ridge, winged along margins.</text>
      <biological_entity id="o20780" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character constraint="along margins" constraintid="o20782" is_modifier="false" name="architecture" notes="" src="d0_s2" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="median" id="o20781" name="ridge" name_original="ridge" src="d0_s2" type="structure" />
      <biological_entity id="o20782" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <relation from="o20780" id="r2803" name="with" negation="false" src="d0_s2" to="o20781" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially and adaxially green, sometimes abaxially purple-tinged, broadly ovate to oblong, 7-35 5-25 cm, 1.2-1.6 times as long as wide, sinus 1/3-1/2 length of midrib, lobes approximate to overlapping;</text>
      <biological_entity id="o20783" name="blade-leaf" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes abaxially" name="coloration" src="d0_s3" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="oblong" />
        <character char_type="range_value" from="7" from_unit="cm" name="distance" src="d0_s3" to="35-5-25" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1.2-1.6" value_original="1.2-1.6" />
      </biological_entity>
      <biological_entity id="o20784" name="sinus" name_original="sinus" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/3 length of midrib" name="length" src="d0_s3" to="1/2 length of midrib" />
      </biological_entity>
      <biological_entity id="o20785" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="approximate" name="arrangement" src="d0_s3" to="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces glabrous.</text>
      <biological_entity id="o20786" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 2.5-5 cm diam.;</text>
      <biological_entity id="o20787" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals mostly 6, abaxially green to yellow, adaxially usually with red or maroon toward base;</text>
      <biological_entity id="o20788" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character char_type="range_value" from="abaxially green" name="coloration" src="d0_s6" to="yellow" />
        <character is_modifier="false" modifier="adaxially usually; usually" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character constraint="toward base" constraintid="o20789" is_modifier="false" name="coloration" src="d0_s6" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity id="o20789" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>petals oblong, thick;</text>
      <biological_entity id="o20790" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3-9 mm, longer than filaments.</text>
      <biological_entity id="o20791" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character constraint="than filaments" constraintid="o20792" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o20792" name="filament" name_original="filaments" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruit mostly purple-tinged, ovoid, 2-4.3 2-3.5 cm, strongly ribbed, slightly constricted below stigmatic disk;</text>
      <biological_entity id="o20793" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s9" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="4.3-2-3.5" to_unit="cm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character constraint="below stigmatic, disk" constraintid="o20794, o20795" is_modifier="false" modifier="slightly" name="size" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o20794" name="stigmatic" name_original="stigmatic" src="d0_s9" type="structure" />
      <biological_entity id="o20795" name="disk" name_original="disk" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigmatic disk green, rarely reddened, 8-20 mm diam., entire to deeply crenate;</text>
      <biological_entity constraint="stigmatic" id="o20796" name="disk" name_original="disk" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="reddened" value_original="reddened" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s10" to="deeply crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmatic rays 7-28, linear to narrowly lanceolate, terminating 0-1 (-1.5) mm from margin of disk.</text>
      <biological_entity constraint="stigmatic" id="o20797" name="ray" name_original="rays" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="28" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2.5-5 mm. 2n = 34.</text>
      <biological_entity id="o20798" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20799" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ponds, lakes, sluggish streams, and ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="sluggish streams" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Conn., Del., Idaho, Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., Mont., Nebr., N.H., N.J., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Grand nénuphar jaune</other_name>
  <discussion>Nuphar variegata is distinct throughout most of its range. The leaves are characteristically floating, being emergent only under low-water conditions. Intermediates between N. variegata and N. microphylla, probably of hybrid origin, are treated as N. rubrodisca. Some intergrading of characteristics occurs where the range overlaps with N. advena (E. O. Beal 1956). This can be observed in the mid-Atlantic region. Intermediates between N. variegata and N. polysepala occur in eastern British Columbia where the two species are sympatric. Authorship and typification of this name were discussed by E. G. Voss (1965).</discussion>
  
</bio:treatment>