<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="(de Candolle) H. Hara" date="1958" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>J. Jap. Bot.</publication_title>
      <place_in_publication>33: 271. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500049</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="Chaix" date="unknown" rank="species">triloba</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="variety">americana</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 216. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hepatica;species triloba;variety americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="(de Candolle) Ker-Gawler" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Hepatica;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">nobilis</taxon_name>
    <taxon_name authority="(Pursh) Steyermark" date="unknown" rank="variety">obtusa</taxon_name>
    <taxon_hierarchy>genus Hepatica;species nobilis;variety obtusa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triloba</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="variety">obtusa</taxon_name>
    <taxon_hierarchy>genus Hepatica;species triloba;variety obtusa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 5-18 cm, from rhizomes, rhizomes ascending to horizontal.</text>
      <biological_entity id="o3268" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3269" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o3270" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="horizontal" />
      </biological_entity>
      <relation from="o3268" id="r425" name="from" negation="false" src="d0_s0" to="o3269" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3-15, often purplish abaxially, simple, deeply divided;</text>
      <biological_entity constraint="basal" id="o3271" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="15" />
        <character is_modifier="false" modifier="often; abaxially" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s1" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 5-20 cm;</text>
      <biological_entity id="o3272" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaf-blade widely orbiculate, 1.5-7 × 2-10 cm, base cordate, margins entire, apex rounded, surfaces strongly villous to glabrescent;</text>
      <biological_entity id="o3273" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3274" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o3275" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3276" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3277" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="strongly villous" name="pubescence" src="d0_s3" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lobes 3 (-4), widely ovate, 1-4 cm wide;</text>
      <biological_entity id="o3278" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>middle lobe 50-70 (-75) % of total blade length.</text>
      <biological_entity constraint="middle" id="o3279" name="lobe" name_original="lobe" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered, villous to pilose;</text>
      <biological_entity id="o3280" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s6" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>involucral-bracts 3, 1-tiered, simple, dissimilar to basal leaves, sessile, calyxlike, closely subtending flower, ovate or elliptic, 0.65-1.8 × 0.5-1.2 cm, bases distinct, cuneate, margins entire, apex obtuse, strongly villous to glabrescent.</text>
      <biological_entity id="o3281" name="involucral-bract" name_original="involucral-bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="calyxlike" value_original="calyxlike" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.65" from_unit="cm" name="length" src="d0_s7" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3282" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o3283" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <biological_entity id="o3284" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o3285" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3286" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="strongly villous" name="pubescence" src="d0_s7" to="glabrescent" />
      </biological_entity>
      <relation from="o3281" id="r426" name="to" negation="false" src="d0_s7" to="o3282" />
      <relation from="o3281" id="r427" name="closely subtending" negation="false" src="d0_s7" to="o3283" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 5-12, white to pink or bluish, ovate to obovate 7.5-14.5 × 3.5-7.7 mm, glabrous;</text>
      <biological_entity id="o3287" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3288" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pink or bluish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="length" src="d0_s8" to="14.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s8" to="7.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals absent;</text>
      <biological_entity id="o3289" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3290" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10-30.</text>
      <biological_entity id="o3291" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3292" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Heads of achenes spheric;</text>
      <biological_entity id="o3293" name="head" name_original="heads" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o3294" name="achene" name_original="achenes" src="d0_s11" type="structure" />
      <relation from="o3293" id="r428" name="part_of" negation="false" src="d0_s11" to="o3294" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel 0.1-0.4 cm.</text>
      <biological_entity id="o3295" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s12" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes: body narrowly ovoid, 3.5-5 × 1.2-1.6 mm, slightly winged, hispid, gradually tapering;</text>
      <biological_entity id="o3296" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity id="o3297" name="body" name_original="body" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak indistinct.</text>
      <biological_entity id="o3298" name="achene" name_original="achenes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n=14.</text>
      <biological_entity id="o3299" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3300" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed woods, often in association with both conifers and deciduous trees</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed woods" />
        <character name="habitat" value="association" modifier="often" constraint="with both conifers and deciduous trees" />
        <character name="habitat" value="both conifers" />
        <character name="habitat" value="deciduous trees" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Round-lobed hepatica</other_name>
  <other_name type="common_name">anémone d'Amérique</other_name>
  <other_name type="common_name">hépatique d'Amérique</other_name>
  <discussion>Anemone americana is found in habitats similar to those of A. acutiloba but usually in drier sites with more acid soils.</discussion>
  
</bio:treatment>