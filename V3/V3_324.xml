<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">patens</taxon_name>
    <taxon_name authority="Ewan" date="1945" rank="subspecies">hepaticoideum</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. D, Phys. Sci.</publication_title>
      <place_in_publication>2: 103. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species patens;subspecies hepaticoideum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500540</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal leaves usually present at anthesis;</text>
      <biological_entity id="o20407" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o20408" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal and proximal cauline leaves rarely cleft more than 4/5 radius of blade.</text>
      <biological_entity id="o20409" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal and proximal cauline" id="o20410" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s1" value="cleft" value_original="cleft" />
        <character name="quantity" src="d0_s1" value="4" value_original="4" />
        <character constraint="of blade" constraintid="o20411" name="quantity" src="d0_s1" value="/5" value_original="/5" />
      </biological_entity>
      <biological_entity id="o20411" name="blade" name_original="blade" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade: ultimate lobes 3-5, width more than 15 mm.</text>
      <biological_entity id="o20412" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o20413" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: pedicel usually glabrous.</text>
      <biological_entity id="o20414" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o20415" name="pedicel" name_original="pedicel" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: lateral sepals 11-17 mm, spur 10-18 mm;</text>
      <biological_entity id="o20416" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="lateral" id="o20417" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20418" name="spur" name_original="spur" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower petal blades 5-8 mm;</text>
      <biological_entity id="o20419" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="petal" id="o20420" name="blade" name_original="blades" src="d0_s5" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hairs symmetrically distributed.</text>
      <biological_entity id="o20421" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 16.</text>
      <biological_entity id="o20422" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="symmetrically" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20423" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded ravines, near streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded ravines" />
        <character name="habitat" value="near streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60b.</number>
  <discussion>Although Delphinium patens has not been reported outside California, subspp. hepaticoideum and/or montanum may grow in mountains of northern Baja California, Mexico.</discussion>
  <discussion>Although it hybridizes with Delphinium parryi and D. umbraculorum, D. patens subsp. hepaticoideum usually flowers early enough not to overlap in any given site with flowering time of those species.</discussion>
  <discussion>Likely to be confused only with Delphinium umbraculorum, D. patens subsp. hepaticoideum may be distinguished from that species by its ringed, unwinged seeds, recurved fruits, and lack of arched hairs. Contrasting features of D. umbraculorum are presence of unringed, winged seeds, erect fruits, and arched hairs.</discussion>
  
</bio:treatment>