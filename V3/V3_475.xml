<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 386. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species lancifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500062</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">quinquefolia</taxon_name>
    <taxon_name authority="(Pursh) Fosberg" date="unknown" rank="variety">lancifolia</taxon_name>
    <taxon_hierarchy>genus Anemone;species quinquefolia;variety lancifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 10-40 cm, from rhizomes, rhizomes horizontal.</text>
      <biological_entity id="o9600" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9601" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o9602" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o9600" id="r1350" name="from" negation="false" src="d0_s0" to="o9601" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 1, ternate;</text>
      <biological_entity constraint="basal" id="o9603" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 10-25 cm;</text>
      <biological_entity id="o9604" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet sessile, oblanceolate to ovate, (3.5-) 4-7 (-8) × 2.5-4 (-6) cm, base broadly cuneate, margins coarsely serrate on distal 1/2-2/3, apex acuminate, surfaces ±glabrous;</text>
      <biological_entity constraint="terminal" id="o9605" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9606" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o9607" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="on distal 1/2-2/3" constraintid="o9608" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9608" name="1/2-2/3" name_original="1/2-2/3" src="d0_s3" type="structure" />
      <biological_entity id="o9609" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o9610" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets unlobed or occasionally 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o9611" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 8-25 mm wide.</text>
      <biological_entity constraint="ultimate" id="o9612" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o9613" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle ±glabrous;</text>
      <biological_entity id="o9614" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts 3, 1-tiered, ternate, ±similar to basal leaves, bases distinct;</text>
      <biological_entity id="o9615" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="ternate" value_original="ternate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9616" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o9617" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o9615" id="r1351" modifier="more or less" name="to" negation="false" src="d0_s8" to="o9616" />
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet sessile, oblanceolate to ovate, (2-) 3-8.7 × 0.8-3 cm, bases narrowly cuneate to cuneate, margins coarsely serrate on distal 1/2 -2/3, apex acuminate, surfaces ± glabrous;</text>
      <biological_entity constraint="terminal" id="o9618" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s9" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s9" to="8.7" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9619" name="base" name_original="bases" src="d0_s9" type="structure">
        <character char_type="range_value" from="narrowly cuneate" name="shape" src="d0_s9" to="cuneate" />
      </biological_entity>
      <biological_entity id="o9620" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character constraint="on distal 1/2-2/3" constraintid="o9621" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9621" name="1/2-2/3" name_original="1/2-2/3" src="d0_s9" type="structure" />
      <biological_entity id="o9622" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o9623" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets unlobed or occasionally 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o9624" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s10" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ultimate lobes 15-30 mm wide.</text>
      <biological_entity constraint="ultimate" id="o9625" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals (4-) 5 (-7), white, oblong to elliptic, (13-) 15-20 (-25) × 5-10 mm, glabrous;</text>
      <biological_entity id="o9626" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9627" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="7" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="elliptic" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" src="d0_s12" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 50-70.</text>
      <biological_entity id="o9628" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9629" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Heads of achenes nearly spheric;</text>
      <biological_entity id="o9630" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s14" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o9631" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <relation from="o9630" id="r1352" name="part_of" negation="false" src="d0_s14" to="o9631" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel (3-) 4-8 (-10) cm.</text>
      <biological_entity id="o9632" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s15" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s15" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s15" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes: body elliptic, flat, 3.5-5 × 1-1.5 mm, not winged, puberulous;</text>
      <biological_entity id="o9633" name="achene" name_original="achenes" src="d0_s16" type="structure" />
      <biological_entity id="o9634" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s16" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulous" value_original="puberulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak straight or slightly curved, 1-1.5 mm, ±glabrous, not plumose.</text>
      <biological_entity id="o9635" name="achene" name_original="achenes" src="d0_s17" type="structure" />
      <biological_entity id="o9636" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, rich woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" modifier="damp" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Lance-leaved anemone</other_name>
  
</bio:treatment>