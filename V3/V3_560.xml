<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="de Candolle" date="1817" rank="variety">monantha</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 213. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species narcissiflora;variety monantha</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500070</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="subspecies">alaskana</taxon_name>
    <taxon_hierarchy>genus Anemone;species narcissiflora;subspecies alaskana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="subspecies">interior</taxon_name>
    <taxon_hierarchy>genus Anemone;species narcissiflora;subspecies interior;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="(Linnaeus) Hultén" date="unknown" rank="subspecies">sibirica</taxon_name>
    <taxon_hierarchy>genus Anemone;species narcissiflora;subspecies sibirica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="variety">uniflora</taxon_name>
    <taxon_hierarchy>genus Anemone;species narcissiflora;variety uniflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 10-50 cm.</text>
      <biological_entity id="o13772" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3-5;</text>
      <biological_entity constraint="basal" id="o13773" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (2-) 4-7 cm;</text>
      <biological_entity id="o13774" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets puberulous to moderately villous and glabrate;</text>
      <biological_entity id="o13775" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="puberulous" name="pubescence" src="d0_s3" to="moderately villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets 1-2×-lobed;</text>
      <biological_entity constraint="lateral" id="o13776" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="1-2×-lobed" value_original="1-2×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 3-6 mm wide.</text>
      <biological_entity constraint="ultimate" id="o13777" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2-3 (-5) -flowered umbels or flowers solitary;</text>
      <biological_entity id="o13778" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o13779" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3(-5)-flowered" value_original="2-3(-5)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o13780" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3(-5)-flowered" value_original="2-3(-5)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle puberulous to villous to nearly glabrous;</text>
      <biological_entity id="o13781" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="puberulous" name="pubescence" src="d0_s7" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts (2-) 3, obtriangular, 3-cleft, (1-) 1.5-2.5 cm, surfaces puberulous to moderately villous and glabrate;</text>
      <biological_entity id="o13782" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtriangular" value_original="obtriangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="3-cleft" value_original="3-cleft" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13783" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="puberulous" name="pubescence" src="d0_s8" to="moderately villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral segments unlobed.</text>
      <biological_entity constraint="lateral" id="o13784" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5-8, white, or abaxially white or blue and adaxially white;</text>
      <biological_entity id="o13785" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13786" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue and adaxially white" value_original="blue and adaxially white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially; adaxially" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="blue and adaxially white" value_original="blue and adaxially white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 40-60.</text>
      <biological_entity id="o13787" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13788" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s11" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels (4.5-) 7-14 (-18.5) cm in fruit.</text>
      <biological_entity id="o13789" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="18.5" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o13790" from="7" from_unit="cm" name="some_measurement" src="d0_s12" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13790" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Achenes: body 6-9 mm.</text>
      <biological_entity id="o13791" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity id="o13792" name="body" name_original="body" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist slopes, grassy areas, thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist slopes" />
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  
</bio:treatment>