<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. Richard ex Kunth" date="unknown" rank="family">juglandaceae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">carya</taxon_name>
    <taxon_name authority="(Poiret) Nuttall" date="1818" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 221. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juglandaceae;genus carya;species tomentosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220002408</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juglans</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>4: 504. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Juglans;species tomentosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 36 m.</text>
      <biological_entity id="o19091" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="36" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark gray, fissured or ridged.</text>
      <biological_entity id="o19092" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="relief" src="d0_s1" value="fissured" value_original="fissured" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs reddish-brown, stout, hirsute and scaly.</text>
      <biological_entity id="o19093" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s2" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Terminal buds tan (after early loss of outer scales), broadly ovoid, 8-20 mm, tomentose;</text>
      <biological_entity constraint="terminal" id="o19094" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bud-scales imbricate;</text>
      <biological_entity id="o19095" name="bud-scale" name_original="bud-scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary buds protected by bracteoles fused into hood.</text>
      <biological_entity constraint="axillary" id="o19096" name="bud" name_original="buds" src="d0_s5" type="structure" />
      <biological_entity id="o19097" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure" />
      <biological_entity id="o19098" name="hood" name_original="hood" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="fused" value_original="fused" />
      </biological_entity>
      <relation from="o19096" id="r2565" name="protected by" negation="false" src="d0_s5" to="o19097" />
      <relation from="o19096" id="r2566" name="protected by" negation="false" src="d0_s5" to="o19098" />
    </statement>
    <statement id="d0_s6">
      <text>Leaves 3-5 dm;</text>
      <biological_entity id="o19099" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s6" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole 3-12 cm, petiole and rachis hirsute, with conspicuous large and small round peltate scales.</text>
      <biological_entity id="o19100" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19101" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o19102" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o19103" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="peltate" value_original="peltate" />
      </biological_entity>
      <relation from="o19101" id="r2567" name="with" negation="false" src="d0_s7" to="o19103" />
      <relation from="o19102" id="r2568" name="with" negation="false" src="d0_s7" to="o19103" />
    </statement>
    <statement id="d0_s8">
      <text>Leaflets (5-) 7-9, lateral petiolules 0-2 mm, terminal petiolules 2-13 mm;</text>
      <biological_entity id="o19104" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19105" name="petiolule" name_original="petiolules" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o19106" name="petiolule" name_original="petiolules" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades ovate to elliptic or obovate, not falcate, 4-19 × 2-8 cm, margins finely to coarsely serrate, apex acute, rarely acuminate;</text>
      <biological_entity id="o19107" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="elliptic or obovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s9" to="19" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s9" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19108" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="finely to coarsely" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o19109" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>surfaces abaxially hirsute with unicellular, 2-8-rayed fasciculate and multiradiate hairs, and with large and small round peltate scales abundant, adaxially hirsute along midrib and major veins, puberulent with fasciculate hairs and scales in spring.</text>
      <biological_entity id="o19110" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character constraint="with hairs" constraintid="o19111" is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
        <character constraint="along veins" constraintid="o19114" is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s10" value="hirsute" value_original="hirsute" />
        <character constraint="with scales" constraintid="o19116" is_modifier="false" name="pubescence" notes="" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o19111" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="unicellular" value_original="unicellular" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s10" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="multiradiate" value_original="multiradiate" />
      </biological_entity>
      <biological_entity id="o19112" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="large" value_original="large" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s10" value="round" value_original="round" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o19113" name="midrib" name_original="midrib" src="d0_s10" type="structure" />
      <biological_entity id="o19114" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o19115" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s10" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity id="o19116" name="scale" name_original="scales" src="d0_s10" type="structure" />
      <relation from="o19110" id="r2569" name="with" negation="false" src="d0_s10" to="o19112" />
    </statement>
    <statement id="d0_s11">
      <text>Staminate catkins pedunculate, to 14 cm, stalks and bracts hirsute, scaly, apex of each bract with coarse hairs;</text>
      <biological_entity id="o19117" name="catkin" name_original="catkins" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pedunculate" value_original="pedunculate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s11" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19118" name="stalk" name_original="stalks" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s11" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o19119" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s11" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o19120" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o19121" name="bract" name_original="bract" src="d0_s11" type="structure" />
      <biological_entity id="o19122" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="relief" src="d0_s11" value="coarse" value_original="coarse" />
      </biological_entity>
      <relation from="o19120" id="r2570" name="part_of" negation="false" src="d0_s11" to="o19121" />
      <relation from="o19120" id="r2571" name="with" negation="false" src="d0_s11" to="o19122" />
    </statement>
    <statement id="d0_s12">
      <text>anthers hirsute.</text>
      <biological_entity id="o19123" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits reddish-brown, finely mottled, spheric to ellipsoid or obovoid, not compressed to compressed, 3-5 × 3-5 cm;</text>
      <biological_entity id="o19124" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="finely" name="coloration" src="d0_s13" value="mottled" value_original="mottled" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s13" to="ellipsoid or obovoid not compressed" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s13" to="ellipsoid or obovoid not compressed" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s13" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s13" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>husks rough, 4-10 mm thick, dehiscing to middle or nearly to base, sutures smooth;</text>
      <biological_entity id="o19125" name="husk" name_original="husks" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="rough" value_original="rough" />
        <character char_type="range_value" from="4" from_unit="mm" name="thickness" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position" src="d0_s14" value="middle" value_original="middle" />
        <character name="position" src="d0_s14" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o19126" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o19127" name="suture" name_original="sutures" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o19125" id="r2572" name="to" negation="false" src="d0_s14" to="o19126" />
    </statement>
    <statement id="d0_s15">
      <text>nuts tan, spheric to ellipsoid, compressed, prominently to faintly 4-angled, rugulose;</text>
      <biological_entity id="o19128" name="nut" name_original="nuts" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s15" to="ellipsoid compressed" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s15" to="ellipsoid compressed" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s15" to="ellipsoid compressed" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>shells thick.</text>
      <biological_entity id="o19129" name="shell" name_original="shells" src="d0_s16" type="structure">
        <character is_modifier="false" name="width" src="d0_s16" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds sweet.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 64.</text>
      <biological_entity id="o19130" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="taste" src="d0_s17" value="sweet" value_original="sweet" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19131" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Well-drained sandy soils, rolling hills and rocky hillsides, occasionally on limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="well-drained sandy soils" />
        <character name="habitat" value="rolling hills" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="limestone outcrops" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Mockernut hickory</other_name>
  <discussion>Both the mockernut hickory and the shagbark hickory were formerly known as Carya alba (Linnaeus) K. Koch [or Hicoria alba (Linnaeus) Britton], based on Juglans alba of Linnaeus. A. J. Rehder (1945) pointed out that the original circumscription included two taxa, and C. alba (J. alba) should therefore be rejected as ambiguous in favor of C. tomentosa and C. ovata, respectively.</discussion>
  <discussion>Carya tomentosa hybridizes with C. texana (C. ×collina Laughlin) and is reported to hybridize with the diploid C. illinoinensis (C. ×schneckii Sargent).</discussion>
  <discussion>Cherokee Indians used Carya tomentosa medicinally as an analgesic, especially as an aid for polio, and as an oral and a cold aid; the Delaware, as a gynecological aid and a tonic (D. E. Moerman 1986).</discussion>
  
</bio:treatment>