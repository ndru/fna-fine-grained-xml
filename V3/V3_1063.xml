<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">myosurus</taxon_name>
    <taxon_name authority="Gay" date="1845" rank="species">apetalus</taxon_name>
    <taxon_name authority="(G. R. Campbell) Whittemore" date="1994" rank="variety">montanus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>4: 78. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus myosurus;species apetalus;variety montanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500785</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myosurus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">minimus</taxon_name>
    <taxon_name authority="G. R. Campbell" date="unknown" rank="subspecies">montanus</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>2: 394. 1952</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Myosurus;species minimus;subspecies montanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: sepals faintly 3-veined, scarious margins narrow.</text>
      <biological_entity id="o895" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o896" name="sepal" name_original="sepals" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s0" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o897" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="size_or_width" src="d0_s0" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads of achenes 11-26 mm;</text>
      <biological_entity id="o898" name="head" name_original="heads" src="d0_s1" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s1" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o899" name="achene" name_original="achenes" src="d0_s1" type="structure" />
      <relation from="o898" id="r105" name="part_of" negation="false" src="d0_s1" to="o899" />
    </statement>
    <statement id="d0_s2">
      <text>outer face of achene narrowly rhombic.</text>
      <biological_entity id="o901" name="achene" name_original="achene" src="d0_s2" type="structure" />
      <relation from="o900" id="r106" name="part_of" negation="false" src="d0_s2" to="o901" />
    </statement>
    <statement id="d0_s3">
      <text>2n=16.</text>
      <biological_entity constraint="achene" id="o900" name="face" name_original="face" src="d0_s2" type="structure" constraint_original="outer achene; achene">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="rhombic" value_original="rhombic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o902" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" constraint="May-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows, vernal pools and sloughs, bogs, muddy shores of lakes and streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="sloughs" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="muddy shores" constraint="of lakes and streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Ariz., Calif., Colo., Mont., Nev., N.Mex., N.Dak., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <discussion>The illegitimate name Myosurus aristatus subsp. montanus (G.R. Campbell) H.Mason has been used for this taxon.</discussion>
  
</bio:treatment>