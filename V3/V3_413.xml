<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Michael A. Vincent</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">62</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Blume" date="unknown" rank="family">SCHISANDRACEAE</taxon_name>
    <taxon_hierarchy>family SCHISANDRACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10803</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen or deciduous, scandent or twining, glabrous.</text>
      <biological_entity id="o10248" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scandent" value_original="scandent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="twining" value_original="twining" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, simple, without stipules, petiolate.</text>
      <biological_entity id="o10249" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o10250" name="stipule" name_original="stipules" src="d0_s1" type="structure" />
      <relation from="o10249" id="r1426" name="without" negation="false" src="d0_s1" to="o10250" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade often fragrant (especially when bruised or crushed), translucent-dotted, ovate to lanceolate, pinnately veined, thin to leathery, margins entire to remotely dentate.</text>
      <biological_entity id="o10251" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="odor" src="d0_s2" value="fragrant" value_original="fragrant" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="translucent-dotted" value_original="translucent-dotted" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s2" value="veined" value_original="veined" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o10252" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s2" to="remotely dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers unisexual, staminate and pistillate on same [different] plants, axillary, solitary, pedunculate;</text>
      <biological_entity id="o10253" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s3" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o10254" is_modifier="false" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o10254" name="plant" name_original="plants" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle bracteolate;</text>
      <biological_entity id="o10255" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth hypogynous;</text>
      <biological_entity id="o10256" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 5-20, imbricate, distinct, in 2 [-3] series, all similar but innermost more petaloid.</text>
      <biological_entity id="o10257" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="20" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o10258" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o10259" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <relation from="o10257" id="r1427" name="in" negation="false" src="d0_s6" to="o10258" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: stamens 4-80, hypogynous, distinct or connate partially or completely into fleshy, globose or discoid mass, in 1-several series;</text>
      <biological_entity id="o10260" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10261" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="80" />
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="into mass" constraintid="o10262" is_modifier="false" modifier="partially" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o10262" name="mass" name_original="mass" src="d0_s7" type="structure">
        <character is_modifier="true" name="texture" src="d0_s7" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="shape" src="d0_s7" value="globose" value_original="globose" />
        <character is_modifier="true" name="shape" src="d0_s7" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o10263" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="several" />
      </biological_entity>
      <relation from="o10261" id="r1428" name="in" negation="false" src="d0_s7" to="o10263" />
    </statement>
    <statement id="d0_s8">
      <text>anthers basifixed, 4-locular, longitudinally dehiscent;</text>
      <biological_entity id="o10264" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10265" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s8" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s8" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pollen 3-aperturate or 6-aperturate;</text>
      <biological_entity id="o10266" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10267" name="pollen" name_original="pollen" src="d0_s9" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="3-aperturate" value_original="3-aperturate" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="6-aperturate" value_original="6-aperturate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil absent.</text>
      <biological_entity id="o10268" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10269" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: pistils simple, 6-300, distinct, closely-set in few-to-many series on globose to elongate axis, attached obliquely;</text>
      <biological_entity id="o10270" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10271" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="simple" value_original="simple" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="300" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character constraint="in series" constraintid="o10272" is_modifier="false" name="arrangement" src="d0_s11" value="closely-set" value_original="closely-set" />
        <character is_modifier="false" modifier="obliquely" name="fixation" notes="" src="d0_s11" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o10272" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few-to-many" value_original="few-to-many" />
      </biological_entity>
      <biological_entity id="o10273" name="axis" name_original="axis" src="d0_s11" type="structure">
        <character char_type="range_value" from="globose" is_modifier="true" name="shape" src="d0_s11" to="elongate" />
      </biological_entity>
      <relation from="o10272" id="r1429" name="on" negation="false" src="d0_s11" to="o10273" />
    </statement>
    <statement id="d0_s12">
      <text>placentation marginal to pendulous;</text>
      <biological_entity id="o10274" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="placentation" src="d0_s12" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules [1-] 2-3 [-10] per locule;</text>
      <biological_entity id="o10275" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10276" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="10" />
        <character char_type="range_value" constraint="per locule" constraintid="o10277" from="2" name="quantity" src="d0_s13" to="3" />
      </biological_entity>
      <biological_entity id="o10277" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stigmatic surface adaxial;</text>
      <biological_entity id="o10278" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o10279" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="adaxial" value_original="adaxial" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens absent.</text>
      <biological_entity id="o10280" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10281" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits aggregates of berries, produced on elongate axis.</text>
      <biological_entity constraint="fruits" id="o10282" name="aggregate" name_original="aggregates" src="d0_s16" type="structure" />
      <biological_entity id="o10283" name="berry" name_original="berries" src="d0_s16" type="structure" />
      <biological_entity id="o10284" name="axis" name_original="axis" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o10282" id="r1430" name="part_of" negation="false" src="d0_s16" to="o10283" />
      <relation from="o10282" id="r1431" name="produced on" negation="false" src="d0_s16" to="o10284" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds [1-] 2-3 [-10] per berry, flattened;</text>
      <biological_entity id="o10285" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="10" />
        <character char_type="range_value" constraint="per berry" constraintid="o10286" from="2" name="quantity" src="d0_s17" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o10286" name="berry" name_original="berry" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>endosperm copious, oily;</text>
      <biological_entity id="o10287" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="copious" value_original="copious" />
        <character is_modifier="false" name="coating" src="d0_s18" value="oily" value_original="oily" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>embryo small.</text>
      <biological_entity id="o10288" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="size" src="d0_s19" value="small" value_original="small" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, chiefly e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="chiefly e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Star-vine Family</other_name>
  <discussion>Genera 2, species 47 (1 genus, 1 species in the flora).</discussion>
  <discussion>The Chinese drug wu-wei-zi and its substitutes are obtained from species of Schisandra. The drug is considered a potential source of expectorants, immune response boosters, and anti-ulcer compounds. Several lignan compounds from species of the genus are used as a treatment for hepatitis and as central nervous-system depressants (S. Foster 1989).</discussion>
  <discussion>Schisandraceae were considered closely allied to Illiciaceae by A. C. Smith (1947). Embryologic studies (R. N. Kapil and S. Jalan 1964) were interpreted as showing that the former are considerably removed from the latter. In his monograph, A. C. Smith also postulated a close relationship between Schisandraceae and Magnoliaceae. J. Hutchinson (1973) stated that Schisandraceae were probably derived from Magnoliaceae. Studies of fossil pollen led J. W. Walker and A. G. Walker (1984) to conclude that Schisandraceae and Illiceaceae are closely allied with Winteraceae. Based on analysis of nucleotide sequences from the plastid gene rbcL, however, M. W. Chase et al. (1993) and Qiu Y. L. et al. (1993) concluded that Schisandraceae and Illiciaceae are closely allied and closely related to Austrobaileyaceae but distant from Winteraceae.</discussion>
  <references>
    <reference>Hutchinson, J. 1973. The Families of Flowering Plants, ed. 3. Oxford. Pp. 161-162.</reference>
    <reference>Smith, A. C. 1947. The families Illiciaceae and Schisandraceae. Sargentia 7: 1-224.</reference>
  </references>
  
</bio:treatment>