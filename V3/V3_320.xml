<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(Greene) B. Boivin" date="1944" rank="section">Leucocoma</taxon_name>
    <taxon_name authority="Fischer &amp; Avé Lallemant in Fischer" date="1842" rank="species">dasycarpum</taxon_name>
    <place_of_publication>
      <publication_title>in Fischer,  C. A. Meyer &amp; Avé-Lallemant, Index Sem. Hort. Petrop.</publication_title>
      <place_in_publication>8: 72. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section leucocoma;species dasycarpum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dasycarpum</taxon_name>
    <taxon_name authority="(Rydberg) B. Boivin" date="unknown" rank="variety">hypoglaucum</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species dasycarpum;variety hypoglaucum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, stout, 40-150 (-200) cm.</text>
      <biological_entity id="o20864" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves chiefly cauline;</text>
      <biological_entity id="o20865" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o20866" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="chiefly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, distal cauline leaves sessile or nearly so;</text>
      <biological_entity constraint="basal and proximal cauline" id="o20867" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="cauline distal" id="o20868" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s2" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles and rachises glabrous or occasionally pubescent and/or stipitate-glandular.</text>
      <biological_entity id="o20869" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20870" name="rachis" name_original="rachises" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade: basal and proximal cauline 3-5×-ternately compound;</text>
      <biological_entity id="o20871" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="3-5×-ternately" name="architecture" src="d0_s4" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets brownish green to dark green or bright green, ovate to cuneate-obovate, apically undivided or 2-3 (-5) -lobed, 15-60 × 8-45 mm, length 0.9-2.6 times width, usually leathery with veins prominent abaxially, margins often revolute, lobe margins entire, surfaces abaxially usually pubescent and/or papillose (i.e., with very minute sessile glands).</text>
      <biological_entity id="o20872" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure" />
      <biological_entity id="o20873" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="brownish green" name="coloration" src="d0_s5" to="dark green or bright green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="cuneate-obovate apically undivided or 2-3(-5)-lobed" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="cuneate-obovate apically undivided or 2-3(-5)-lobed" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="45" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="0.9-2.6" value_original="0.9-2.6" />
        <character constraint="with veins" constraintid="o20874" is_modifier="false" modifier="usually" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o20874" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o20875" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o20876" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20877" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially usually" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences panicles, apically ± acutely pyramidal, many flowered;</text>
      <biological_entity constraint="inflorescences" id="o20878" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="apically more or less acutely" name="shape" src="d0_s6" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels usually glabrous, rarely pubescent or stipitate-glandular.</text>
      <biological_entity id="o20879" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20880" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers usually unisexual, staminate and pistillate on different plants;</text>
      <biological_entity id="o20881" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o20882" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20882" name="plant" name_original="plants" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4 (-6), whitish, lanceolate, 3-5 mm;</text>
      <biological_entity id="o20883" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="6" />
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments white to purplish, filiform, scarcely dilated distally, 2-6.5 mm, flexible;</text>
      <biological_entity id="o20884" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="purplish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="scarcely; distally" name="shape" src="d0_s10" value="dilated" value_original="dilated" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1-3.6 (-4) mm, usually strongly apiculate.</text>
      <biological_entity id="o20885" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3.6" to_unit="mm" />
        <character is_modifier="false" modifier="usually strongly" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes numerous, sessile or nearly sessile;</text>
      <biological_entity id="o20886" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stipe 0-1.1 mm;</text>
      <biological_entity id="o20887" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>body ovoid to fusiform, 2-4.6 mm, prominently veined, usually pubescent and/or glandular;</text>
      <biological_entity id="o20888" name="body" name_original="body" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="fusiform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.6" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak often dehiscent as fruit matures, ± straight, filiform, 1.5-4.7 (-6) mm, about as long as achene body.</text>
      <biological_entity id="o20889" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character constraint="as fruit" constraintid="o20890" is_modifier="false" modifier="often" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="more or less" name="course" notes="" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="4.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20890" name="fruit" name_original="fruit" src="d0_s15" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s15" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity constraint="achene" id="o20891" name="body" name_original="body" src="d0_s15" type="structure" />
      <relation from="o20889" id="r2815" name="as long as" negation="false" src="d0_s15" to="o20891" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (May-late Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="late Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous, riparian woods, damp thickets, swamps, wet meadows, and prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="riparian woods" />
        <character name="habitat" value="thickets" modifier="damp" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Que., Sask., Yukon; Ala., Ariz., Ark., Colo., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Mich., Minn., Miss., Mo., Mont., Nebr., N.Mex., N.Y., N.Dak., Ohio, Okla., Pa., S.Dak., Tenn., Tex., Utah., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Purple meadow-rue</other_name>
  <discussion>Thalictrum dasycarpum is a variable species similar to, and possibly intergrading with, T. pubescens. Glabrous variants of T. dasycarpum have been treated as T. dasycarpum var. hypoglaucum. Glabrous and glandular (stipitate and papillate) forms are found throughout the range of the species and occur together in some populations.</discussion>
  <discussion>Native Americans used Thalictrum dasycarpum medicinally to reduce fever, cure cramps, as a stimulant for horses, and as a love charm (D. E. Moerman 1986).</discussion>
  
</bio:treatment>