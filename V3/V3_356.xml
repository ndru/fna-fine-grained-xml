<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey &amp; A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 22. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species occidentalis;</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501179</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to reclining, not rooting nodally, hirsute or sometimes pilose or glabrous, base not bulbous.</text>
      <biological_entity id="o17676" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="reclining" />
        <character is_modifier="false" modifier="not; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character name="pubescence" src="d0_s0" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17677" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots never tuberous.</text>
      <biological_entity id="o17678" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades broadly ovate to semicircular or reniform in outline, 3-parted or foliolate, 1.5-5.3 × 2.2-8 cm, segments usually again 1 (-2) ×-lobed, ultimate segments oblong or elliptic to lanceolate or oblanceolate, margins dentate (sometimes dentate-lobulate or entire), apex acute to rounded-obtuse.</text>
      <biological_entity constraint="basal" id="o17679" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in outline" from="broadly ovate" name="shape" src="d0_s2" to="semicircular or reniform" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-parted" value_original="3-parted" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliolate" value_original="foliolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="5.3" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17680" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually again" name="shape" src="d0_s2" value="1(-2)×-lobed" value_original="1(-2)×-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17681" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="lanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o17682" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o17683" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="rounded-obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o17684" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o17685" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reflexed 2-3 mm above base, 4-7 (-9) × 2-4 mm, hirsute;</text>
      <biological_entity id="o17686" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o17687" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="above base" constraintid="o17688" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o17688" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5-14, yellow, 5-13 × 1.5-8 mm.</text>
      <biological_entity id="o17689" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o17690" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="14" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes hemispheric, 3-7 × 5-9 mm;</text>
      <biological_entity id="o17691" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17692" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o17691" id="r2401" name="part_of" negation="false" src="d0_s6" to="o17692" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2.6-3.6 (-4.8) × 1.8-3 (-3.2) mm, glabrous, rarely hispid, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o17693" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s7" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o17694" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17695" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o17694" id="r2402" name="forming" negation="false" src="d0_s7" to="o17695" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, lanceolate to lance-subulate, straight or curved, 0.4-2.2 mm.</text>
      <biological_entity id="o17696" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="lance-subulate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Calif., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Varieties 7</discussion>
  <discussion>The seeds of Ranunculus occidentalis were eaten by some Californian Indians. D. E. Moerman (1986) identified this taxon as an Aleut poison: juice of the flowers could be slipped into food to poison the person who ate it.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 8-14; Queen Charlotte Islands, B.C.</description>
      <determination>9f Ranunculus occidentalis var. hexasepalus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 5-6; widespread.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem 4-8 mm thick; beak of achene 1.8-2.4 mm, curved; coastal Alaska.</description>
      <determination>9g Ranunculus occidentalis var. nelsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem 1-3(-4) mm thick; beak of achene either 0.4-1.4 mm and curved, or 1.2-2.2 mm and straight; widespread.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Beak of achene straight, 1.2-2.2 mm; Oregon and northernmost California.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Beak of achene curved, 0.4-1.4 mm; widespread.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ultimate segments of leaves lanceolate to oblanceolate.</description>
      <determination>9c Ranunculus occidentalis var. dissectus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ultimate segments of leaves elliptic.</description>
      <determination>9d Ranunculus occidentalis var. howellii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Petals 1.5-2.5 mm wide; beak of achene 0.4-1.2 mm; stems ± reclining; Sierra Nevada, above 1000m.</description>
      <determination>9b Ranunculus occidentalis var. ultramontanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Petals 3-8 mm wide; beak of achene (0.6-)1-1.4 mm; stems erect or reclining; widespread.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems pilose or glabrous; Alaska to c British Columbia and Alberta.</description>
      <determination>9e Ranunculus occidentalis var. brevistylis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems hirsute, sometimes glabrous; California to sw British Columbia.</description>
      <determination>9a Ranunculus occidentalis var. occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>