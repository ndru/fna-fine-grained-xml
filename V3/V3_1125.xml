<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Gaudichaud Beaupré" date="1830" rank="genus">pouzolzia</taxon_name>
    <taxon_name authority="(Linnaeus) Bennett &amp; R. Br. in J. J. Bennett et al." date="1838" rank="species">zeylanica</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Bennett et al.,  Pl. Jav. Rar.,</publication_title>
      <place_in_publication>67. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus pouzolzia;species zeylanica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500994</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parietaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">zeylanica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1052. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Parietaria;species zeylanica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1-20 dm, sparsely to moderately covered on all parts with appressed and spreading hairs.</text>
      <biological_entity id="o27913" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27914" name="part" name_original="parts" src="d0_s0" type="structure" />
      <biological_entity id="o27915" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s0" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o27913" id="r3752" modifier="sparsely to moderately" name="covered on all" negation="false" src="d0_s0" to="o27914" />
      <relation from="o27913" id="r3753" modifier="sparsely to moderately" name="covered on all" negation="false" src="d0_s0" to="o27915" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1-5 × 0.6-2 cm, base rounded to broadly cuneate, apex acute or nearly acuminate.</text>
      <biological_entity id="o27916" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27917" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o27918" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s1" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Staminate flowers ca. 3 mm across;</text>
      <biological_entity id="o27919" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="staminate" value_original="staminate" />
        <character name="some_measurement" src="d0_s2" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>filaments longer than tepals;</text>
      <biological_entity id="o27920" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character constraint="than tepals" constraintid="o27921" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o27921" name="tepal" name_original="tepals" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>pistillode elongate, knoblike.</text>
      <biological_entity id="o27922" name="pistillode" name_original="pistillode" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="knoblike" value_original="knoblike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers ca. 1-1.5 mm, tepals ribbed.</text>
      <biological_entity id="o27923" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27924" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Achenes ca.</text>
    </statement>
    <statement id="d0_s7">
      <text>(0.9-) 1.2 × ca. 0.6 mm, base truncate.</text>
      <biological_entity id="o27925" name="achene" name_original="achenes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_length" src="d0_s7" to="1.2" to_inclusive="false" to_unit="mm" />
        <character name="length" src="d0_s7" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s7" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
      <biological_entity id="o27926" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, old fields, waste places, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Pouzolzia zeylanica was misidentified as Parietaria officinalis Linnaeus by J. K. Small.</discussion>
  
</bio:treatment>