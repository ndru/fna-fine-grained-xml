<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. Meyer" date="unknown" rank="family">saururaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">SAURURUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 341. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 159. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saururaceae;genus SAURURUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek sauros, lizard, and oura, tail</other_info_on_name>
    <other_info_on_name type="fna_id">129337</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, nodes more than 2.</text>
      <biological_entity id="o21413" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21414" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline.</text>
      <biological_entity id="o21415" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences opposite leaves and/or terminal, racemes, lax, spikelike, not subtended by petaloid bracts.</text>
      <biological_entity id="o21416" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o21417" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21418" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o21419" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <relation from="o21418" id="r2906" name="subtended by" negation="true" src="d0_s2" to="o21419" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers 175-350, not coalescent;</text>
      <biological_entity id="o21420" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="175" name="quantity" src="d0_s3" to="350" />
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="coalescent" value_original="coalescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens (3-) 6 (-8), hypogynous or epigynous;</text>
      <biological_entity id="o21421" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character name="quantity" src="d0_s4" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s4" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s4" value="epigynous" value_original="epigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pistil 1, (3-) 4-5 (-7) -carpellate, connate only at base;</text>
      <biological_entity id="o21423" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>placentation marginal;</text>
      <biological_entity id="o21422" name="pistil" name_original="pistil" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(3-)4-5(-7)-carpellate" value_original="(3-)4-5(-7)-carpellate" />
        <character constraint="at base" constraintid="o21423" is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
        <character is_modifier="false" name="placentation" src="d0_s6" value="marginal" value_original="marginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovules 2 per placenta (only 1 developing into seed).</text>
      <biological_entity id="o21424" name="ovule" name_original="ovules" src="d0_s7" type="structure">
        <character constraint="per placenta" constraintid="o21425" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21425" name="placenta" name_original="placenta" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruits schizocarps;</text>
      <biological_entity constraint="fruits" id="o21426" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>mericarps dry, indehiscent.</text>
      <biological_entity id="o21427" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s9" value="dry" value_original="dry" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed 1 per mericarp.</text>
      <biological_entity id="o21429" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>x = 11.</text>
      <biological_entity id="o21428" name="seed" name_original="seed" src="d0_s10" type="structure">
        <character constraint="per mericarp" constraintid="o21429" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="x" id="o21430" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Lizard's-tail</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  <references>
    <reference>Baldwin, J. T. and B. M. Speese. 1949. Cytogeography of Saururus cernuus. Bull. Torrey Bot. Club 76: 213-216.</reference>
    <reference>Hall, T. F. 1940. The biology of Saururus cernuus L. Amer. Midl. Naturalist 24: 253-260.</reference>
    <reference>Holm, T. 1926. Saururus cernuus L. A morphological study. Amer. J. Sci., ser. 5, 12: 162-168.</reference>
    <reference>Johnson, D. S. 1900. On the development of Saururus cernuus L. Bull. Torrey Bot. Club 27: 365-372.</reference>
    <reference>Tucker, S. C. 1975. Floral development in Saururus cernuus (Saururaceae). 1. Floral initiation and stamen development. Amer. J. Bot. 62: 993-1007.</reference>
    <reference>Tucker, S. C. 1976. Floral development in Saururus cernuus (Saururaceae). 2. Carpel initiation and floral vasculature. Amer. J. Bot. 63: 289-301.</reference>
    <reference>Tucker, S. C. 1979. Ontogeny of the inflorescence of Saururus cernuus (Saururaceae). Amer. J. Bot. 66: 227-236.</reference>
  </references>
  
</bio:treatment>