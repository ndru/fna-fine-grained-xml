<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1807" rank="genus">coptis</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1838" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1:28. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus coptis;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500425</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysocoptis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>J.Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7:8. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysocoptis;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes pale-brown.</text>
      <biological_entity id="o23712" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade ternate;</text>
      <biological_entity id="o23713" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o23714" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets long-petiolulate, blade widely ovate, incised with 2-3 lobes divided ca. 1/2 length to base, margins sharply serrate-denticulate.</text>
      <biological_entity id="o23715" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23716" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="long-petiolulate" value_original="long-petiolulate" />
      </biological_entity>
      <biological_entity id="o23717" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character constraint="with lobes" constraintid="o23718" is_modifier="false" name="shape" src="d0_s2" value="incised" value_original="incised" />
        <character constraint="to base" constraintid="o23719" name="length" src="d0_s2" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o23718" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="shape" src="d0_s2" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o23719" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o23720" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s2" value="serrate-denticulate" value_original="serrate-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2-3 (-5) -flowered, often shorter than leaves at anthesis, 10-25 cm, elongating to 32 cm in fruit.</text>
      <biological_entity id="o23721" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-3(-5)-flowered" value_original="2-3(-5)-flowered" />
        <character constraint="than leaves" constraintid="o23722" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="often shorter" value_original="often shorter" />
        <character char_type="range_value" from="10" from_unit="cm" modifier="at anthesis" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
        <character constraint="in fruit" constraintid="o23723" is_modifier="false" name="length" src="d0_s3" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o23722" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23723" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers erect;</text>
      <biological_entity id="o23724" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals spreading to reflexed, linear-lanceolate, 7-11× 0.4-1 mm;</text>
      <biological_entity id="o23725" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals linear-lanceolate, nectary nearly basal, blade flattened, narrowly ligulate at apex;</text>
      <biological_entity id="o23726" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o23727" name="nectary" name_original="nectary" src="d0_s6" type="structure" />
      <biological_entity id="o23728" name="nectary" name_original="nectary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="nearly" name="position" src="d0_s6" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o23729" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
        <character constraint="at apex" constraintid="o23730" is_modifier="false" modifier="narrowly" name="architecture" src="d0_s6" value="ligulate" value_original="ligulate" />
      </biological_entity>
      <biological_entity id="o23730" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 10-35.</text>
      <biological_entity id="o23731" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Follicles 5-15;</text>
      <biological_entity id="o23732" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stipe slightly shorter than body;</text>
      <biological_entity id="o23733" name="stipe" name_original="stipe" src="d0_s9" type="structure">
        <character constraint="than body" constraintid="o23734" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o23734" name="body" name_original="body" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>body oblong, 7.5-14 mm;</text>
      <biological_entity id="o23735" name="body" name_original="body" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak recurved, less than 1 mm.</text>
      <biological_entity id="o23736" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2.2-2.6 mm.</text>
      <biological_entity id="o23737" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, coniferous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="coniferous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>