<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Britton" date="1895" rank="species">allegheniensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>22: 224. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species allegheniensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501113</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or nearly erect, 10-50 cm, glabrous, each with 9-40 flowers.</text>
      <biological_entity id="o22101" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22102" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s0" to="40" />
      </biological_entity>
      <relation from="o22101" id="r3019" name="with" negation="false" src="d0_s0" to="o22102" />
    </statement>
    <statement id="d0_s1">
      <text>Roots slender, 0.2-0.8 mm thick.</text>
      <biological_entity id="o22103" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s1" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves persistent, blades reniform, outer undivided, inner 3-lobed to 3-foliolate, 1-3.5 × 1.5-4.5 cm, base truncate or cordate, margins crenate, apex rounded or obtuse.</text>
      <biological_entity constraint="basal" id="o22104" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o22105" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
      </biological_entity>
      <biological_entity constraint="outer" id="o22106" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity constraint="inner" id="o22107" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22108" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o22109" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o22110" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: pedicels pubescent or glabrous;</text>
      <biological_entity id="o22111" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o22112" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>receptacle sparsely pilose;</text>
      <biological_entity id="o22113" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o22114" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 2-3 × 1-2 mm, abaxially sparsely hispid, hairs colorless;</text>
      <biological_entity id="o22115" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o22116" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o22117" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="colorless" value_original="colorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5, 1-2 × 0.5-1 mm;</text>
      <biological_entity id="o22118" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22119" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectary scale glabrous.</text>
      <biological_entity id="o22120" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="nectary" id="o22121" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads of achenes globose to ovoid, 3-7 × 3-5 mm;</text>
      <biological_entity id="o22122" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s8" to="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22123" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <relation from="o22122" id="r3020" name="part_of" negation="false" src="d0_s8" to="o22123" />
    </statement>
    <statement id="d0_s9">
      <text>achenes 1.5-2 × 1.4-1.8 mm, glabrous;</text>
      <biological_entity id="o22124" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak slender, strongly curved, 0.6-1 mm. 2n = 16.</text>
      <biological_entity id="o22125" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22126" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods and pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., Ky., Md., Mass., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>46.</number>
  
</bio:treatment>