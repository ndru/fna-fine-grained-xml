<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="genus">corydalis</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) A. Gray" date="1886" rank="species">micrantha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">micrantha</taxon_name>
    <taxon_hierarchy>family fumariaceae;genus corydalis;species micrantha;subspecies micrantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500443</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: racemes of chasmogamous flowers not greatly exceeding leaves, often short;</text>
      <biological_entity id="o753" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o754" name="raceme" name_original="racemes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o755" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s0" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o756" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <relation from="o754" id="r85" name="part_of" negation="false" src="d0_s0" to="o755" />
      <relation from="o754" id="r86" modifier="not greatly" name="exceeding" negation="false" src="d0_s0" to="o756" />
    </statement>
    <statement id="d0_s1">
      <text>petal spur ± globose at apex.</text>
      <biological_entity id="o757" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity constraint="petal" id="o758" name="spur" name_original="spur" src="d0_s1" type="structure">
        <character constraint="at apex" constraintid="o759" is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o759" name="apex" name_original="apex" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Capsules often stout, commonly 10-15 mm. 2n = 16.</text>
      <biological_entity id="o760" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="10" from_unit="mm" modifier="commonly" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o761" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid-late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bluffs, rocky hills, open woods, and river banks, often in disturbed soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="rocky hills" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="river banks" />
        <character name="habitat" value="disturbed soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Iowa, Kans., Minn., Mo., Nebr., N.C., Okla., S.Dak., Tenn., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8a.</number>
  
</bio:treatment>