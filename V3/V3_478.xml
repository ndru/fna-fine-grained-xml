<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">Thalictrum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">minus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 546. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section thalictrum;species minus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200008217</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">minus</taxon_name>
    <taxon_name authority="(Fries) Hultén" date="unknown" rank="subspecies">kemense</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species minus;subspecies kemense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">minus</taxon_name>
    <taxon_name authority="(C. A. Meyer) Tamura" date="unknown" rank="variety">stipellatum</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species minus;variety stipellatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, nearly cespitose or rhizomatous, 15-150 cm, glabrous or somewhat glandular.</text>
      <biological_entity id="o22356" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal, 7-30 cm.</text>
      <biological_entity id="o22357" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade 3-4-ternate;</text>
      <biological_entity id="o22358" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-4-ternate" value_original="3-4-ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets nearly orbiculate or broadly ovate, irregularly 2-3-lobed or margins dentate in distal 1/2, 15-30 mm, surfaces glabrous to glandular.</text>
      <biological_entity id="o22359" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="2-3-lobed" value_original="2-3-lobed" />
      </biological_entity>
      <biological_entity id="o22360" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in distal 1/2 , 15-30 mm" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o22361" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s3" to="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences panicles with long branches, many flowered.</text>
      <biological_entity constraint="inflorescences" id="o22362" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" notes="" src="d0_s4" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o22363" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
      </biological_entity>
      <relation from="o22362" id="r3060" name="with" negation="false" src="d0_s4" to="o22363" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: pedicels not recurved in fruit;</text>
      <biological_entity id="o22364" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o22365" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in fruit" constraintid="o22366" is_modifier="false" modifier="not" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o22366" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sepals yellowish green, ovate, 3-4 mm;</text>
      <biological_entity id="o22367" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22368" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 10-15;</text>
      <biological_entity id="o22369" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22370" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellowish, 2-3 mm.</text>
      <biological_entity id="o22371" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22372" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 3-15, sessile;</text>
      <biological_entity id="o22373" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>body broadly ovoid to narrowly oblong-ovoid, 2.5-4 mm, ± weakly veined.</text>
      <biological_entity id="o22374" name="body" name_original="body" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly ovoid" name="shape" src="d0_s10" to="narrowly oblong-ovoid" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="more or less weakly" name="architecture" src="d0_s10" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steppe meadows, shrub thickets, forest margins, and forest meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steppe meadows" />
        <character name="habitat" value="shrub thickets" />
        <character name="habitat" value="forest margins" />
        <character name="habitat" value="forest meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Thalictrum minus has been reported from mainland Alaska (E. Hultén 1968); we have been able to confirm its occurrence only in the Aleutian Islands.</discussion>
  <discussion>Initially pendent, the flowers become erect. The beak is 0.75-1 mm, much shorter than the achene, and not fimbriate.</discussion>
  
</bio:treatment>