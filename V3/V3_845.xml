<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. Meyer" date="unknown" rank="family">saururaceae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1841" rank="genus">ANEMOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>390. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saururaceae;genus ANEMOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek anemone, the windflower, and opsis, appearance</other_info_on_name>
    <other_info_on_name type="fna_id">101739</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple, nodes 1 (-2).</text>
      <biological_entity id="o16430" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o16431" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s0" to="2" />
        <character name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal.</text>
      <biological_entity id="o16432" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16433" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences terminal, spikes, compact, conic, subtended by petaloid bracts.</text>
      <biological_entity id="o16434" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16435" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
        <character is_modifier="false" name="shape" src="d0_s2" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o16436" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <relation from="o16435" id="r2256" name="subtended by" negation="false" src="d0_s2" to="o16436" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers 75-150, coalescent;</text>
      <biological_entity id="o16437" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s3" to="150" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="coalescent" value_original="coalescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens epigynous, 6 (-8);</text>
      <biological_entity id="o16438" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="epigynous" value_original="epigynous" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character name="quantity" src="d0_s4" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pistil 1, 3 (-4) -carpellate;</text>
      <biological_entity id="o16439" name="pistil" name_original="pistil" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3(-4)-carpellate" value_original="3(-4)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>placentae 3 (-4), parietal;</text>
      <biological_entity id="o16440" name="placenta" name_original="placentae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="4" />
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovules 6-10 per placenta;</text>
      <biological_entity id="o16441" name="ovule" name_original="ovules" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per placenta" constraintid="o16442" from="6" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <biological_entity id="o16442" name="placenta" name_original="placenta" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>styles and stigmas 3 (-4), distinct.</text>
      <biological_entity id="o16443" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o16444" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits capsules, dehiscent.</text>
      <biological_entity constraint="fruits" id="o16445" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 18-40.</text>
    </statement>
    <statement id="d0_s11">
      <text>x = 11.</text>
      <biological_entity id="o16446" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
      <biological_entity constraint="x" id="o16447" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Yerba mansa</other_name>
  <discussion>Species 1</discussion>
  <references>
    <reference>Holm, T. 1905. Anemiopsis [sic] californica (Nutt.) H. and A. An anatomical study. Amer. J. Sci., ser. 4, 19: 76-82.</reference>
    <reference>Kelso, L. 1932. A note on Anemopsis californica. Amer. Midl. Naturalist 13: 110-113.</reference>
    <reference>Nozeran, R. 1955. Contribution à l'étude de quelques structures florales. (Essai de morphologie florale comparée). Ann. Sci. Nat., Bot., sér. 11, 16: 1-224.</reference>
    <reference>Quibell, C. H. 1941. Floral anatomy and morphology of Anemopsis californica. Bot. Gaz. 102: 749-758.</reference>
  </references>
  
</bio:treatment>