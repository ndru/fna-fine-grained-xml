<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">annonaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">asimina</taxon_name>
    <taxon_name authority="(Linnaeus) Dunal" date="1817" rank="species">triloba</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Anonac.,</publication_title>
      <place_in_publication>83. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family annonaceae;genus asimina;species triloba</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220001231</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Annona</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">triloba</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 537. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Annona;species triloba;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1.5-11 (-14) m;</text>
      <biological_entity id="o19949" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="14" to_unit="m" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="11" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="14" to_unit="m" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="11" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks slender, to 20 (-30) cm diam.;</text>
      <biological_entity id="o19951" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark shallowly furrowed in larger trees.</text>
      <biological_entity id="o19952" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character constraint="in larger trees" constraintid="o19953" is_modifier="false" modifier="shallowly" name="architecture" src="d0_s2" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity constraint="larger" id="o19953" name="tree" name_original="trees" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Branches spreading-ascending, slender;</text>
      <biological_entity id="o19954" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>new shoots moderately to copiously brown-hairy apically, aging glabrous.</text>
      <biological_entity id="o19955" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="new" value_original="new" />
        <character is_modifier="false" modifier="moderately to copiously; apically" name="pubescence" src="d0_s4" value="brown-hairy" value_original="brown-hairy" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: petiole 5-10 mm.</text>
      <biological_entity id="o19956" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o19957" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade oblong-obovate to oblanceolate, 15-30 cm, membranous, base narrowly cuneate, margins scarcely or not revolute, apex acute to acuminate;</text>
      <biological_entity id="o19958" name="blade-leaf" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="cm" name="distance" src="d0_s6" to="30" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o19959" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19960" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o19961" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>surfaces abaxially densely hairy, later sparsely so on veins, adaxially sparsely appressed-pubescent on veins, becoming glabrous.</text>
      <biological_entity id="o19962" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character constraint="on veins" constraintid="o19963" is_modifier="false" modifier="sparsely" name="condition" src="d0_s7" value="later" value_original="later" />
        <character constraint="on veins" constraintid="o19964" is_modifier="false" modifier="adaxially sparsely" name="pubescence" notes="" src="d0_s7" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" modifier="becoming" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19963" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o19964" name="vein" name_original="veins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences from previous years shoots before or during new leaf emergence;</text>
      <biological_entity id="o19965" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity constraint="previous" id="o19966" name="year" name_original="years" src="d0_s8" type="structure" />
      <biological_entity constraint="previous" id="o19967" name="shoot" name_original="shoots" src="d0_s8" type="structure" />
      <biological_entity id="o19968" name="leaf" name_original="leaf" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="new" value_original="new" />
      </biological_entity>
      <relation from="o19965" id="r2694" name="from" negation="false" src="d0_s8" to="o19966" />
      <relation from="o19965" id="r2695" name="from" negation="false" src="d0_s8" to="o19967" />
      <relation from="o19965" id="r2696" name="during" negation="false" src="d0_s8" to="o19968" />
    </statement>
    <statement id="d0_s9">
      <text>peduncle nodding, (1-) 1.5-2 (-2.5) cm, densely hairy, hairs dark-brown to redbrown;</text>
      <biological_entity id="o19969" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19970" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s9" to="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 1-2, basal, usually ovate-triangular, rarely over 2-3 mm, hairy.</text>
      <biological_entity id="o19971" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="false" name="position" src="d0_s10" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" modifier="over 2-3 mm" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers maroon, fetid, 2-4 (-5) cm diam.;</text>
      <biological_entity id="o19972" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="odor" src="d0_s11" value="fetid" value_original="fetid" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s11" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s11" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals triangular-deltate, 8-12 mm, abaxially densely pilose;</text>
      <biological_entity id="o19973" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular-deltate" value_original="triangular-deltate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outer petals excurved, oblongelliptic, 1.5-2.5 cm, abaxially puberulent on veins;</text>
      <biological_entity constraint="outer" id="o19974" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s13" to="2.5" to_unit="cm" />
        <character constraint="on veins" constraintid="o19975" is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o19975" name="vein" name_original="veins" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>inner petals elliptic, 1/3-1/2 length of outer petals, base saccate, apex recurved, surfaces abaxially glabrate, veins impressed adaxially, corrugate nectary zone distinct;</text>
      <biological_entity constraint="inner" id="o19976" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1/3 length of outer petals" name="length" src="d0_s14" to="1/2 length of outer petals" />
      </biological_entity>
      <biological_entity id="o19977" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o19978" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o19979" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s14" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o19980" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s14" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity constraint="nectary" id="o19981" name="zone" name_original="zone" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement_or_relief" src="d0_s14" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 3-7 (-12).</text>
      <biological_entity id="o19982" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="12" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Berries yellow-green, 5-15 cm.</text>
      <biological_entity id="o19983" name="berry" name_original="berries" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s16" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds brown to chestnut-brown, 1.5-2.5 cm.</text>
      <biological_entity id="o19984" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="chestnut-brown" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s17" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic woods, often alluvial sites, fencerows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic woods" />
        <character name="habitat" value="alluvial sites" modifier="often" />
        <character name="habitat" value="fencerows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mich., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Pawpaw</other_name>
  <other_name type="common_name">dog-banana</other_name>
  <other_name type="common_name">Indian-banana</other_name>
  <other_name type="common_name">aciminier</other_name>
  <discussion>A. pendula Salisbury; Asimina glabra Horta ex C. Koch; Orchidocarpum arietinum Michaux; Porcelia triloba (Linnaeus) Persoon; Uvaria triloba (Linnaeus) Torrey &amp; A. Gray</discussion>
  
</bio:treatment>