<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="1903" rank="species">texanum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>446. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species texanum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501275</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">debile</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">texanum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1: 18. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thalictrum;species debile;variety texanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots becoming black when dry, tuberous, not ribbed, irregular.</text>
      <biological_entity id="o13067" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s0" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s0" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 10-45 cm, rigid, glabrous.</text>
      <biological_entity id="o13068" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: distal cauline long-petiolate.</text>
      <biological_entity id="o13069" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal cauline" id="o13070" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="long-petiolate" value_original="long-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 2×-ternately compound;</text>
      <biological_entity id="o13071" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="2×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets cuneate to reniform, undivided, cleft, or lobed, 2-7 mm wide, margins entire or sometimes weakly crenate, surfaces glabrous, somewhat glaucous.</text>
      <biological_entity id="o13072" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="reniform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13073" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o13074" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemes, several flowered.</text>
      <biological_entity id="o13075" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o13076" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals lanceolate to obovate, in staminate flowers 1.7-3 mm, in pistillate flowers 0.7-1.5 mm;</text>
      <biological_entity id="o13077" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13078" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="obovate" />
      </biological_entity>
      <biological_entity id="o13079" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13080" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o13078" id="r1818" name="in" negation="false" src="d0_s6" to="o13079" />
      <relation from="o13078" id="r1819" name="in" negation="false" src="d0_s6" to="o13080" />
    </statement>
    <statement id="d0_s7">
      <text>filaments colored, not white, ca. 1.5 mm;</text>
      <biological_entity id="o13081" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13082" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1.4-2 mm;</text>
      <biological_entity id="o13083" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13084" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma color unknown.</text>
      <biological_entity id="o13085" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13086" name="stigma" name_original="stigma" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes few, not reflexed, nearly sessile;</text>
      <biological_entity id="o13087" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.1-0.3 mm;</text>
      <biological_entity id="o13088" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body ovoid, not laterally compressed, adaxial surface 2.7-3.7 × 1.4-1.6 mm, glabrous, prominently 6-8-veined, veins not anastomosing-reticulate;</text>
      <biological_entity id="o13089" name="body" name_original="body" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="not laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13090" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s12" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s12" value="6-8-veined" value_original="6-8-veined" />
      </biological_entity>
      <biological_entity id="o13091" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_coloration_or_relief" src="d0_s12" value="anastomosing-reticulate" value_original="anastomosing-reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>beak 0.5-1 mm.</text>
      <biological_entity id="o13092" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring (Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" constraint="Mar-Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins or openings of mesic woodlands or forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of mesic woodlands or forests" />
        <character name="habitat" value="openings" constraint="of mesic woodlands or forests" />
        <character name="habitat" value="mesic woodlands" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Somewhat difficult to locate in the field, Thalictrum texanum is currently known from only two populations. It is closely related to T. arkansanum and T. debile and sometimes treated as a variety of the latter.</discussion>
  <discussion>Thalictrum confine is quite similar to T. occidentale and T. venulosum; it has been treated as a variety or synonym of the latter (R. S. Mitchell 1988).</discussion>
  
</bio:treatment>