<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray in A. Gray et al." date="1895" rank="subgenus">Cyrtorhyncha</taxon_name>
    <taxon_name authority="(A. Gray) L. D. Benson" date="1940" rank="section">Halodes</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>27: 805. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus cyrtorhyncha;section halodes;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302016</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Halodes A. Gray" date="unknown" rank="section">Ranunculus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>21: 366. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Ranunculus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or sparsely hirsute.</text>
      <biological_entity id="o12076" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems dimorphic, with erect flowering-stems and prostrate stolons, not bulbous-based, without bulbils.</text>
      <biological_entity id="o12077" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s1" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o12078" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o12079" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <biological_entity id="o12080" name="bulbil" name_original="bulbils" src="d0_s1" type="structure" />
      <relation from="o12077" id="r1670" name="with" negation="false" src="d0_s1" to="o12078" />
      <relation from="o12077" id="r1671" name="with" negation="false" src="d0_s1" to="o12079" />
      <relation from="o12077" id="r1672" name="without" negation="false" src="d0_s1" to="o12080" />
    </statement>
    <statement id="d0_s2">
      <text>Roots basal and nodal, never tuberous.</text>
      <biological_entity id="o12081" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="nodal" value_original="nodal" />
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o12082" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal, proximal cauline, and stolon leaves similar, petiolate, base broadly rounded to truncate, margins crenate or crenate-serrate;</text>
      <biological_entity constraint="basal" id="o12083" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline proximal" id="o12084" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="stolon" id="o12085" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o12086" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s4" to="truncate" />
      </biological_entity>
      <biological_entity id="o12087" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cauline leaves much reduced, scalelike, simple and undivided.</text>
      <biological_entity constraint="distal cauline" id="o12088" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="undivided" value_original="undivided" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-5-flowered cymes.</text>
      <biological_entity id="o12089" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o12090" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers pedicellate;</text>
      <biological_entity id="o12091" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals deciduous soon after anthesis, 5;</text>
      <biological_entity id="o12092" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, yellow;</text>
      <biological_entity id="o12093" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectary scale attached basally, forming flap over nectary, glabrous, free margin entire;</text>
      <biological_entity constraint="nectary" id="o12094" name="scale" name_original="scale" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="fixation" src="d0_s10" value="attached" value_original="attached" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12095" name="flap" name_original="flap" src="d0_s10" type="structure" />
      <biological_entity id="o12096" name="nectary" name_original="nectary" src="d0_s10" type="structure" />
      <biological_entity id="o12097" name="margin" name_original="margin" src="d0_s10" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o12094" id="r1673" name="forming" negation="false" src="d0_s10" to="o12095" />
      <relation from="o12094" id="r1674" name="over" negation="false" src="d0_s10" to="o12096" />
    </statement>
    <statement id="d0_s11">
      <text>style present.</text>
      <biological_entity id="o12098" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits achenes, 1-locular;</text>
      <biological_entity constraint="fruits" id="o12099" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>achene body discoid to flattened-lenticular, oblong to obovate in outline, 2-4 times as wide as thick, not prolonged beyond seed;</text>
      <biological_entity constraint="achene" id="o12100" name="body" name_original="body" src="d0_s13" type="structure">
        <character char_type="range_value" from="discoid" name="shape" src="d0_s13" to="flattened-lenticular oblong" />
        <character char_type="range_value" constraint="in outline" constraintid="o12101" from="discoid" name="shape" src="d0_s13" to="flattened-lenticular oblong" />
        <character is_modifier="false" name="width_or_width" notes="" src="d0_s13" value="2-4 times as wide as thick" />
        <character constraint="beyond seed" constraintid="o12102" is_modifier="false" modifier="not" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o12101" name="outline" name_original="outline" src="d0_s13" type="structure" />
      <biological_entity id="o12102" name="seed" name_original="seed" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>achene wall papery, not loose, longitudinally ribbed, glabrous;</text>
      <biological_entity constraint="achene" id="o12103" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="not" name="architecture_or_fragility" src="d0_s14" value="loose" value_original="loose" />
        <character is_modifier="false" modifier="longitudinally" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>margin prominent narrow ridge;</text>
      <biological_entity id="o12104" name="margin" name_original="margin" src="d0_s15" type="structure" />
      <biological_entity id="o12105" name="ridge" name_original="ridge" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak much shorter than achene body.</text>
      <biological_entity id="o12106" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character constraint="than achene body" constraintid="o12107" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="achene" id="o12107" name="body" name_original="body" src="d0_s16" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.2.</number>
  <discussion>Species ca. 10 (1 in the flora).</discussion>
  
</bio:treatment>