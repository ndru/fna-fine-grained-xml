<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="de Candolle" date="1821" rank="genus">achlys</taxon_name>
    <taxon_name authority="(Smith) de Candolle" date="1821" rank="species">triphylla</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 35. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus achlys;species triphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500013</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leontice</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">triphylla</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>20: [5]. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leontice;species triphylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants, 2-4 dm.</text>
      <biological_entity id="o25974" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1-3 cm.</text>
      <biological_entity id="o25975" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o25976" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Central leaflet blade 4-11 × 4-8 cm, proximal margins entire, distal margins (1-) 3-4 (-8) -lobed.</text>
      <biological_entity constraint="leaflet" id="o25977" name="blade" name_original="blade" src="d0_s2" type="structure" constraint_original="central leaflet">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25978" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25979" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="(1-)3-4(-8)-lobed" value_original="(1-)3-4(-8)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2.5-5 cm excluding peduncle.</text>
      <biological_entity id="o25980" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25981" name="peduncle" name_original="peduncle" src="d0_s3" type="structure" />
      <relation from="o25980" id="r3524" name="excluding" negation="false" src="d0_s3" to="o25981" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: stamens 3-4 mm;</text>
      <biological_entity id="o25982" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o25983" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ovaries 1-1.5 mm.</text>
      <biological_entity id="o25984" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25985" name="ovary" name_original="ovaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Follicles red-purple, 3-4.5 mm. 2n = 12.</text>
      <biological_entity id="o25986" name="follicle" name_original="follicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="red-purple" value_original="red-purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25987" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountain regions in Cascade Range and Coast Range of California in coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountain regions" constraint="in cascade range" />
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Deer-foot</other_name>
  <other_name type="common_name">vanilla-leaf</other_name>
  <discussion>Medicinally, Native Americans used preparations of the leaves of Achlys triphylla to treat tuberculosis, for a hair wash, and as an emetic (D. E. Moermann 1986).</discussion>
  
</bio:treatment>