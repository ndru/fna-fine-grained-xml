<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Muhlenberg ex J. M. Bigelow" date="1814" rank="species">fascicularis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Boston.,</publication_title>
      <place_in_publication>137. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species fascicularis;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501139</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fascicularis</taxon_name>
    <taxon_name authority="(Greene) Fernald" date="unknown" rank="variety">apricus</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species fascicularis;variety apricus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending, never rooting nodally, strigose or spreading-strigose, base not bulbous.</text>
      <biological_entity id="o16491" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="never; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="spreading-strigose" value_original="spreading-strigose" />
      </biological_entity>
      <biological_entity id="o16492" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots always both filiform and tuberous on same stem.</text>
      <biological_entity id="o16493" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character constraint="on stem" constraintid="o16494" is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o16494" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades ovate to broadly ovate in outline, 3-5-foliolate, 2.1-4.7 × 1.9-4.5 cm, leaflets undivided or 1×-lobed or parted, ultimate segments oblanceolate or obovate, margins entire or with few teeth, apex rounded-acute to rounded-obtuse.</text>
      <biological_entity constraint="basal" id="o16495" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o16496" from="ovate" name="shape" src="d0_s2" to="broadly ovate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="3-5-foliolate" value_original="3-5-foliolate" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="length" src="d0_s2" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="1.9" from_unit="cm" name="width" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16496" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity id="o16497" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1×-lobed" value_original="1×-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16498" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o16499" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with few teeth" value_original="with few teeth" />
      </biological_entity>
      <biological_entity id="o16500" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o16501" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded-acute" name="shape" src="d0_s2" to="rounded-obtuse" />
      </biological_entity>
      <relation from="o16499" id="r2260" name="with" negation="false" src="d0_s2" to="o16500" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle hispid or glabrous;</text>
      <biological_entity id="o16502" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o16503" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals spreading or sometimes reflexed from base, 5-7 × 2-3 mm, hispid or glabrous;</text>
      <biological_entity id="o16504" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16505" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character constraint="from base" constraintid="o16506" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16506" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5 (-7), yellow, 8-14 × 3-6 mm.</text>
      <biological_entity id="o16507" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16508" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="7" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes globose or ovoid, 5-9 × 5-8 mm;</text>
      <biological_entity id="o16509" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16510" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o16509" id="r2261" name="part_of" negation="false" src="d0_s6" to="o16510" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2-2.8 × 1.8-2.2 mm, glabrous, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o16511" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16512" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16513" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o16512" id="r2262" name="forming" negation="false" src="d0_s7" to="o16513" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, filiform, straight, 1.2-2.8 mm. 2n = 32.</text>
      <biological_entity id="o16514" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16515" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–spring (Jan–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="winter" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassland or deciduous forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassland or deciduous forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont.; Ala., Ark., Conn., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Ranunculus fascicularis is very similar to R. hispidus var. hispidus, and herbarium specimens without underground parts may be difficult to identify. Ranunculus fascicularis grows in drier habitats; segments of its leaves are commonly oblanceolate and blunt, with few or no marginal teeth; and its petals are widest at or below the middle. Ranunculus hispidus var. hispidus is usually larger in all its parts (leaves, flowers, heads of achenes); leaf segments are variable in shape but their apices are normally sharper and their marginal teeth more numerous, and petals are widest above the middle.</discussion>
  
</bio:treatment>