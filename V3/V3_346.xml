<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Blume" date="1826" rank="genus">lithocarpus</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Rehder in L. H. Bailey" date="1917" rank="species">densiflorus</taxon_name>
    <place_of_publication>
      <publication_title>in L. H. Bailey, Stand. Cycl. Hort.</publication_title>
      <place_in_publication>6: 3569. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus lithocarpus;species densiflorus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500750</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">densiflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>391. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Quercus;species densiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 20 (-45) m.</text>
      <biological_entity id="o5832" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="45" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="45" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray or brown, smooth or deeply furrowed.</text>
      <biological_entity id="o5834" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs densely yellowish tomentose.</text>
      <biological_entity id="o5835" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade adaxially convex, to 60-120 mm, leathery to brittle, margins often revolute, regularly toothed, teeth prominent to obscure;</text>
      <biological_entity id="o5836" name="blade-leaf" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s3" value="convex" value_original="convex" />
        <character char_type="range_value" from="60" from_unit="mm" name="distance" src="d0_s3" to="120" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity id="o5837" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="regularly" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o5838" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="prominent" name="prominence" src="d0_s3" to="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially prominently and densely woolly, often glabrate at maturity, revealing gray or bluish green waxy surface, veins often distally impressed.</text>
      <biological_entity id="o5839" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="woolly" value_original="woolly" />
        <character constraint="at surface" constraintid="o5840" is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o5840" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="bluish green" value_original="bluish green" />
        <character is_modifier="true" name="texture" src="d0_s4" value="ceraceous" value_original="waxy" />
      </biological_entity>
      <biological_entity id="o5841" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often distally" name="prominence" src="d0_s4" value="impressed" value_original="impressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits: cup scales subulate, spreading to strongly recurved, hooked;</text>
      <biological_entity id="o5842" name="fruit" name_original="fruits" src="d0_s5" type="structure" />
      <biological_entity constraint="cup" id="o5843" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="strongly recurved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>nut yellowish-brown, globose to cylindric-tapered, to 15-35 mm, extremely hard, densely tomentose, eventually glabrate.</text>
      <biological_entity id="o5844" name="fruit" name_original="fruits" src="d0_s6" type="structure" />
      <biological_entity id="o5845" name="nut" name_original="nut" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="cylindric-tapered" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="extremely" name="texture" src="d0_s6" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="eventually" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.; only in the flora.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="only in the flora" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Tanoak</other_name>
  <other_name type="common_name">tanbark-oak</other_name>
  <discussion>Varieties 2: only in the flora.</discussion>
  <discussion>Sterile specimens of Lithocarpus densiflorus are often confused with Chrysolepis and vice versa. Nonfruiting material of L. densiflorus is recognizable by the loose tomentose pubescence of the leaves and inflorescences (although the leaves are often glabrate with age). Chrysolepis lacks this tomentose pubescence and has only a tight vestiture of glandular-peltate trichomes, except for some stellate and straight simple trichomes associated with the flowers.</discussion>
  <discussion>The Costanoan used infusions prepared from the bark of Lithocarpus densiflora (no varieties specified) as a wash for facial sores and to tighten loose teeth (D. E. Moerman 1986).</discussion>
  <discussion>Varieties 2</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trees, 20(–45) m at maturity; leaves to 120 mm, convex adaxially, secondary veins conspicuous and strongly impressed adaxially.</description>
      <determination>1a Lithocarpus densiflorus var. densiflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, 3 m or less at maturity; leaves 60 mm or less, flat, secondary veins inconspicuous and not strongly impressed adaxially.</description>
      <determination>1b Lithocarpus densiflorus var. echinoides</determination>
    </key_statement>
  </key>
</bio:treatment>