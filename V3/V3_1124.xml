<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Webb) Rouy &amp; Foucaud" date="1893" rank="section">Flammula</taxon_name>
    <taxon_name authority="Geyer ex Bentham" date="1849" rank="species">alismifolius</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">alismifolius</taxon_name>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section flammula;species alismifolius;variety alismifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501108</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20-70 cm × 3.5-8 mm, glabrous.</text>
      <biological_entity id="o19082" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s0" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots not or scarcely fusiform-thickened proximally.</text>
      <biological_entity id="o19083" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely; proximally" name="size_or_width" src="d0_s1" value="fusiform-thickened" value_original="fusiform-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole glabrous.</text>
      <biological_entity id="o19084" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19085" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade lanceolate, 5.8-14.1 × 1.2-2.9 cm, base acuminate or sometimes acute, margins serrulate.</text>
      <biological_entity id="o19086" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5.8" from_unit="cm" name="length" src="d0_s3" to="14.1" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s3" to="2.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19087" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o19088" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals 4-6, 7-11 × 4-8 mm.</text>
      <biological_entity id="o19089" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19090" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows, bogs, shallow water of streams and ponds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="shallow water" constraint="of streams and ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ponds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50a.</number>
  
</bio:treatment>