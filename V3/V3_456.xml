<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">alnus</taxon_name>
    <taxon_name authority="Nuttall" date="1842" rank="species">rhombifolia</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Sylv.</publication_title>
      <place_in_publication>1: 49. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus alnus;species rhombifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500038</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rhombifolia</taxon_name>
    <taxon_name authority="Munz &amp; I. M. Johnston" date="unknown" rank="variety">bernardina</taxon_name>
    <taxon_hierarchy>genus Alnus;species rhombifolia;variety bernardina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 35 m;</text>
      <biological_entity id="o9689" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="35" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks often several, crowns spreading, open.</text>
      <biological_entity id="o9690" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s1" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o9691" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark light gray, smooth, becoming darker and breaking into scales in age;</text>
      <biological_entity id="o9692" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light gray" value_original="light gray" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o9693" name="scale" name_original="scales" src="d0_s2" type="structure" />
      <relation from="o9692" id="r1360" name="breaking into" negation="false" src="d0_s2" to="o9693" />
    </statement>
    <statement id="d0_s3">
      <text>lenticels inconspicuous.</text>
      <biological_entity id="o9694" name="lenticel" name_original="lenticels" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Winter buds stipitate, ellipsoid to obovoid, 3–9 mm, apex rounded;</text>
      <biological_entity id="o9695" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="true" name="season" src="d0_s4" value="winter" value_original="winter" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s4" to="obovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9696" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stalks 3–5 mm;</text>
      <biological_entity id="o9697" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales 2, equal, valvate, sometimes incompletely covering underlying leaves, moderately to heavily resin-coated.</text>
      <biological_entity id="o9698" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s6" value="valvate" value_original="valvate" />
        <character is_modifier="false" modifier="moderately to heavily" name="coating" src="d0_s6" value="resin-coated" value_original="resin-coated" />
      </biological_entity>
      <biological_entity constraint="underlying" id="o9699" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o9698" id="r1361" modifier="sometimes incompletely" name="covering" negation="false" src="d0_s6" to="o9699" />
    </statement>
    <statement id="d0_s7">
      <text>Leaf-blade narrowly elliptic to rhombic, rarely ovate, 4–9 × 2–5 cm, base cuneate to rounded, margins flat, finely serrate or serrulate, sometimes slightly lobed, without noticeably larger secondary teeth, apex acute or obtuse to rounded;</text>
      <biological_entity id="o9700" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s7" to="rhombic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9701" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
      <biological_entity id="o9702" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o9703" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="noticeably" name="size" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o9704" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
      <relation from="o9702" id="r1362" name="without" negation="false" src="d0_s7" to="o9703" />
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially sparsely pubescent to villous.</text>
      <biological_entity id="o9705" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="abaxially sparsely pubescent" name="pubescence" src="d0_s8" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences formed season before flowering and exposed during winter;</text>
      <biological_entity id="o9706" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character constraint="during winter" is_modifier="false" name="prominence" src="d0_s9" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o9707" name="season" name_original="season" src="d0_s9" type="structure" />
      <relation from="o9706" id="r1363" name="formed" negation="false" src="d0_s9" to="o9707" />
    </statement>
    <statement id="d0_s10">
      <text>staminate catkins in 1 or more clusters of 3–7, 3–10 cm, stamens 2, or 4 with 2 reduced in size;</text>
      <biological_entity id="o9708" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="of 3-7" name="some_measurement" src="d0_s10" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9709" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character constraint="with" name="quantity" src="d0_s10" value="4" value_original="4" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s10" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate catkins in 1 or more clusters of 2–6.</text>
    </statement>
    <statement id="d0_s12">
      <text>Flowering before new growth in spring.</text>
      <biological_entity id="o9710" name="catkin" name_original="catkins" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character constraint="before growth" constraintid="o9711" is_modifier="false" name="life_cycle" src="d0_s12" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o9711" name="growth" name_original="growth" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Infructescences ovoid to nearly cylindric, 1–2.2 × 0.7–1 cm;</text>
      <biological_entity id="o9712" name="infructescence" name_original="infructescences" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="nearly cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s13" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s13" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peduncles 1–10 mm.</text>
      <biological_entity id="o9713" name="peduncle" name_original="peduncles" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Samaras broadly elliptic, wings narrower than body, irregular in shape, leathery.</text>
      <biological_entity id="o9714" name="samara" name_original="samaras" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9715" name="wing" name_original="wings" src="d0_s15" type="structure">
        <character constraint="than body" constraintid="o9716" is_modifier="false" name="width" src="d0_s15" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="shape" src="d0_s15" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o9716" name="body" name_original="body" src="d0_s15" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky stream banks and adjacent (often rather dry) slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky stream banks" modifier="open" />
        <character name="habitat" value="adjacent ( dry ) slopes" modifier="often rather" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">White alder</other_name>
  <other_name type="common_name">California alder</other_name>
  <discussion>Alnus rhombifolia is the common alder throughout the dry Mediterranean climatic zone of coastal western United States. Mexican populations are not known, but because A. rhombifolia has been collected as far south as San Diego, California, it should be expected in adjacent Baja California.</discussion>
  <discussion>Native Americans used various parts of Alnus rhombifolia medicinally for diarrhea, consumption, and burns, as a blood purifier, an emetic, and a wash for babies with skin diseases, and to facilitate childbirth (D. E. Moerman 1986).</discussion>
  
</bio:treatment>