<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">lobatae</taxon_name>
    <taxon_name authority="Michaux" date="1801" rank="species">falcata</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Chênes Amér., no.</publication_title>
      <place_in_publication>16, plate 28. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section lobatae;species falcata</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501030</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Sudworth" date="unknown" rank="species">digitata</taxon_name>
    <taxon_hierarchy>genus Quercus;species digitata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">falcata</taxon_name>
    <taxon_name authority="(Michaux) Nuttall" date="unknown" rank="variety">triloba</taxon_name>
    <taxon_hierarchy>genus Quercus;species falcata;variety triloba;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, deciduous, to 30 m.</text>
      <biological_entity id="o6540" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark-brown to black, narrowly fissured with scaly ridges, inner bark orange.</text>
      <biological_entity id="o6541" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s1" to="black" />
        <character constraint="with ridges" constraintid="o6542" is_modifier="false" modifier="narrowly" name="relief" src="d0_s1" value="fissured" value_original="fissured" />
      </biological_entity>
      <biological_entity id="o6542" name="ridge" name_original="ridges" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6543" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs reddish-brown, (1-) 1.5-3.5 (-4.5) mm diam., pubescent.</text>
      <biological_entity id="o6544" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s2" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Terminal buds light reddish-brown, ovoid, 4-8 mm, puberulent throughout.</text>
      <biological_entity constraint="terminal" id="o6545" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light reddish-brown" value_original="light reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 20-60 mm, glabrous to sparsely pubescent.</text>
      <biological_entity id="o6546" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6547" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade ovate to elliptic or obovate, 100-300 × 60-160 mm, base rounded or U-shaped, margins with 3-7 deep lobes and 6-20 awns, terminal lobe often long-acuminate, much longer than lateral lobes, apex acute;</text>
      <biological_entity id="o6548" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic or obovate" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s5" to="300" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="width" src="d0_s5" to="160" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6549" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="u--shaped" value_original="u--shaped" />
      </biological_entity>
      <biological_entity id="o6550" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o6551" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="true" name="depth" src="d0_s5" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o6552" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="true" name="depth" src="d0_s5" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6553" name="lobe" name_original="lobe" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
        <character constraint="than lateral lobes" constraintid="o6554" is_modifier="false" name="length_or_size" src="d0_s5" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6554" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity id="o6555" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o6550" id="r907" name="with" negation="false" src="d0_s5" to="o6551" />
      <relation from="o6550" id="r908" name="with" negation="false" src="d0_s5" to="o6552" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially sparsely to uniformly tawny-pubescent, adaxially glossy and glabrous or puberulent along midrib, secondary-veins raised on both surfaces.</text>
      <biological_entity id="o6556" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely to uniformly" name="pubescence" src="d0_s6" value="tawny-pubescent" value_original="tawny-pubescent" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="along midrib" constraintid="o6557" is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o6557" name="midrib" name_original="midrib" src="d0_s6" type="structure" />
      <biological_entity id="o6558" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure" />
      <biological_entity id="o6559" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o6558" id="r909" name="raised on both" negation="false" src="d0_s6" to="o6559" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns biennial;</text>
      <biological_entity id="o6560" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup saucer-shaped to cupshaped, 3-7 mm high × 9-18 mm wide, covering 1/3-1/2 nut, outer surface puberulent, inner surface pubescent, scale tips tightly appressed, acute;</text>
      <biological_entity id="o6561" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character char_type="range_value" from="saucer-shaped" name="shape" src="d0_s8" to="cupshaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="×9-18" value_original="×9-18" />
      </biological_entity>
      <biological_entity id="o6562" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o6563" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6564" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="scale" id="o6565" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o6561" id="r910" name="covering" negation="false" src="d0_s8" to="o6562" />
    </statement>
    <statement id="d0_s9">
      <text>nut subglobose, 9-16 × 8-15 mm, often striate, puberulent, scar diam. 5-10 mm.</text>
      <biological_entity id="o6566" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="coloration_or_pubescence_or_relief" src="d0_s9" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o6567" name="scar" name_original="scar" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diam" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or sandy upland sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="sandy upland sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., D.C., Fla., Ga., Ill., Ind., Ky., La., Md., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Southern red oak</other_name>
  <other_name type="common_name">Spanish oak</other_name>
  <other_name type="common_name">chêne rouge</other_name>
  <discussion>Native Americans used Quercus falcata in various ways to treat indigestion, chronic dysentery, sores, chapped skin, chills and fevers, lost voice, asthma, milky urine, and as an antiseptic, a tonic, and an emetic (D. E. Moerman 1986).</discussion>
  <discussion>Quercus falcata reportedly hybridizes with Q. ilicifolia (= Q. ×caesariensis Moldenke), Q. imbricaria, Q. incana, Q. laevis, Q. laurifolia (= Q. ×beaumontiana Sargent), and Q. marilandica (E. J. Palmer 1948); with Q. nigra, and Q. pagoda (S. A. Ware 1967; R. J. Jensen 1989); and with Q. phellos, Q. shumardii, Q. hemisphaerica, and Q. velutina.</discussion>
  
</bio:treatment>