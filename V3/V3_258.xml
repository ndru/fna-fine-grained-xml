<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="genus">corydalis</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="species">aurea</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>2: 740. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus corydalis;species aurea</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500427</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Capnodes</taxon_name>
    <taxon_name authority="(Willdenow) Kuntze" date="unknown" rank="species">aureum</taxon_name>
    <taxon_hierarchy>genus Capnodes;species aureum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual or biennial, glaucous, from ± branched caudices.</text>
      <biological_entity id="o2098" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2099" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o2098" id="r259" name="from" negation="false" src="d0_s0" to="o2099" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 10-50, prostrate-ascending, 2-3.5 dm.</text>
      <biological_entity id="o2100" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s1" to="50" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate-ascending" value_original="prostrate-ascending" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="3.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves compound;</text>
      <biological_entity id="o2101" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 3 orders of leaflets and lobes;</text>
      <biological_entity id="o2102" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o2103" name="order" name_original="orders" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2104" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o2105" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o2102" id="r260" name="with" negation="false" src="d0_s3" to="o2103" />
      <relation from="o2103" id="r261" name="part_of" negation="false" src="d0_s3" to="o2104" />
      <relation from="o2103" id="r262" name="part_of" negation="false" src="d0_s3" to="o2105" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes elliptic, 1.5 times or more longer than wide, margins incised, apex subapiculate.</text>
      <biological_entity constraint="ultimate" id="o2106" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="length_or_size_or_width" src="d0_s4" value="1.5 times or more longer than wide" />
      </biological_entity>
      <biological_entity id="o2107" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o2108" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="subapiculate" value_original="subapiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, 10-20 (-30) -flowered, primary racemes shorter than to slightly exceeding leaves, secondary racemes fewer flowered;</text>
      <biological_entity id="o2109" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="10-20(-30)-flowered" value_original="10-20(-30)-flowered" />
      </biological_entity>
      <biological_entity constraint="primary" id="o2110" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o2111" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="secondary" id="o2112" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
      <relation from="o2110" id="r263" modifier="slightly" name="exceeding" negation="false" src="d0_s5" to="o2111" />
    </statement>
    <statement id="d0_s6">
      <text>bracts elliptic to linear, 4-10 × 1-2 mm, rarely larger, margins often denticulate toward apex, distal bracts usually much reduced.</text>
      <biological_entity id="o2113" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="arrangement" src="d0_s6" to="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s6" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o2114" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="toward apex" constraintid="o2115" is_modifier="false" modifier="often" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o2115" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o2116" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually much" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers at first erect, later reflexed;</text>
      <biological_entity id="o2117" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="later" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 5-10 mm;</text>
      <biological_entity id="o2118" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to attenuate-ovate, to 1-3 mm, margins often sinuate or dentate;</text>
      <biological_entity id="o2119" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="attenuate-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2120" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pale to bright-yellow;</text>
      <biological_entity id="o2121" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s10" to="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>spurred petal 13-16 mm, spur straight or slightly incurved, 4-5 mm, apex subglobose, crest low and incised or absent, marginal wing moderately to well developed, unspurred outer petal 9-11 mm, crest same as that of spurred petal;</text>
      <biological_entity id="o2122" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2123" name="spur" name_original="spur" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2124" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
      </biological_entity>
      <biological_entity id="o2125" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s11" value="incised" value_original="incised" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o2126" name="wing" name_original="wing" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately to well" name="development" src="d0_s11" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2127" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="unspurred" value_original="unspurred" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2128" name="crest" name_original="crest" src="d0_s11" type="structure" />
      <biological_entity id="o2129" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <relation from="o2128" id="r264" name="part_of" negation="false" src="d0_s11" to="o2129" />
    </statement>
    <statement id="d0_s12">
      <text>inner petals oblanceolate, 8-10 mm, blade wider than claw and more prominently winged toward apex, claw 3.5-4.5 mm;</text>
      <biological_entity constraint="inner" id="o2130" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2131" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character constraint="than claw" constraintid="o2132" is_modifier="false" name="width" src="d0_s12" value="wider" value_original="wider" />
        <character constraint="toward apex" constraintid="o2133" is_modifier="false" modifier="prominently" name="architecture" notes="" src="d0_s12" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o2132" name="claw" name_original="claw" src="d0_s12" type="structure" />
      <biological_entity id="o2133" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o2134" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectariferous spur 2-3 mm;</text>
      <biological_entity constraint="nectariferous" id="o2135" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style ca. 3 mm;</text>
      <biological_entity id="o2136" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma 2-lobed, 1/2 as long as wide, with 8 papillae.</text>
      <biological_entity id="o2137" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o2138" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="8" value_original="8" />
      </biological_entity>
      <relation from="o2137" id="r265" name="with" negation="false" src="d0_s15" to="o2138" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules erect to pendent at maturity, linear, often torulose, slender to somewhat stout, straight to moderately incurved, 12-24 (-30) mm.</text>
      <biological_entity id="o2139" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="at maturity , linear , often torulose , slender to somewhat somewhat stout , straight to moderately incurved , 12-24(-30) mm" from="erect" name="orientation" src="d0_s16" to="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds nearly 2 mm diam., appearing essentially smooth under magnification, narrow marginal ring present or absent.</text>
      <biological_entity id="o2140" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character name="diameter" src="d0_s17" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2141" name="magnification" name_original="magnification" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="essentially" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o2142" name="ring" name_original="ring" src="d0_s17" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s17" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o2140" id="r266" name="appearing" negation="false" src="d0_s17" to="o2141" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Ill., Kans., Mich., Minn., Mo., Mont., N.Dak., N.H., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., S.Dak., Tex., Utah, Vt., Wash., Wis., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Golden corydalis</other_name>
  <other_name type="common_name">corydalis dorée</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>The Navaho used Corydalis aurea medicinally for a variety of ailments, including rheumatism, diarrhea, sores on the hands, stomachaches, menstrual problems, and sore throats, and as a general disinfectant (D. E. Moerman 1986, no subspecies cited).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules slender, pendent or spreading at maturity, usually 18–24 mm; seeds without marginal ring; leaves generally exceeding racemes.</description>
      <determination>10a Corydalis aurea subsp. aurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules stout, erect at maturity, 12–20 mm; seeds with narrow marginal ring; racemes generally exceeding leaves.</description>
      <determination>10b Corydalis aurea subsp. occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>