<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="genus">meconella</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont,  Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>312. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus meconella;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500778</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Meconella</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">oregana</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) Jepson" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus Meconella;species oregana;variety californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.3-1.8 dm.</text>
      <biological_entity id="o1163" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s0" to="1.8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o1164" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3-25 mm;</text>
      <biological_entity id="o1165" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal with blade spatulate;</text>
      <biological_entity constraint="basal" id="o1166" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1167" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <relation from="o1166" id="r139" name="with" negation="false" src="d0_s3" to="o1167" />
    </statement>
    <statement id="d0_s4">
      <text>proximal cauline whorled, blade linear-spatulate;</text>
      <biological_entity constraint="proximal cauline" id="o1168" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="whorled" value_original="whorled" />
      </biological_entity>
      <biological_entity id="o1169" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-spatulate" value_original="linear-spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal usually opposite, blade broadly linear;</text>
      <biological_entity constraint="distal" id="o1170" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o1171" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole to 17 mm;</text>
      <biological_entity id="o1172" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins entire or rarely denticulate.</text>
      <biological_entity id="o1173" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences: peduncle 3-8 cm.</text>
      <biological_entity id="o1174" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o1175" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: receptacle shorter than broad, expanded into small ring beneath calyx;</text>
      <biological_entity id="o1176" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1177" name="receptacle" name_original="receptacle" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter than broad" value_original="shorter than broad" />
        <character constraint="into ring" constraintid="o1178" is_modifier="false" name="size" src="d0_s9" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o1178" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o1179" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <relation from="o1178" id="r140" name="beneath" negation="false" src="d0_s9" to="o1179" />
    </statement>
    <statement id="d0_s10">
      <text>petals white to cream colored or outer sometimes yellow, alternately obovate and narrowly ovate, 2-7 × 1-5 mm, apex rounded;</text>
      <biological_entity id="o1180" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1181" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="cream colored" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1182" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="alternately" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1183" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens ca.</text>
      <biological_entity id="o1184" name="flower" name_original="flowers" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>(6-) 12, in 2 series;</text>
      <biological_entity id="o1185" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s12" to="12" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="12" value_original="12" />
      </biological_entity>
      <biological_entity id="o1186" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outer filaments shorter than inner;</text>
      <biological_entity constraint="outer" id="o1187" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="than inner filaments" constraintid="o1188" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1188" name="filament" name_original="filaments" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers ovoid to globose, much shorter than filaments, usually broader.</text>
      <biological_entity id="o1189" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="globose" />
        <character constraint="than filaments" constraintid="o1190" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" modifier="usually" name="width" src="d0_s14" value="broader" value_original="broader" />
      </biological_entity>
      <biological_entity id="o1190" name="filament" name_original="filaments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules to 50 × 1.5 mm. 2n = 16.</text>
      <biological_entity id="o1191" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s15" to="50" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1192" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, sunny slopes in open grassy areas in grasslands, oak or pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sunny slopes" modifier="moist" constraint="in open grassy areas in grasslands , oak or pine woodlands" />
        <character name="habitat" value="open grassy areas" constraint="in grasslands , oak or pine woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>The yellow color that occurs in the outer petals of some plants fades at night and returns in daylight.</discussion>
  
</bio:treatment>