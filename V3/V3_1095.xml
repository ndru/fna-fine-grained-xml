<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1838" rank="species">pitcheri</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 10. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species pitcheri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500406</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Britton" date="unknown" rank="species">pitcheri</taxon_name>
    <taxon_hierarchy>genus Viorna;species pitcheri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems viny, to 4 m, very sparsely short-pilose, sometimes nearly glabrous.</text>
      <biological_entity id="o26574" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s0" value="short-pilose" value_original="short-pilose" />
        <character is_modifier="false" modifier="sometimes nearly" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade mostly 1-2 pinnate, many leaves simple;</text>
      <biological_entity id="o26575" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o26576" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>primary leaflets 2-8 plus additional tendril-like terminal leaflet, deeply 2-5-lobed or unlobed or 3-foliolate, leaflets or major lobes lanceolate to broadly ovate, 1-11 × 1-6 cm, leathery (thin in var. pitcheri), ± prominently reticulate adaxially;</text>
      <biological_entity constraint="primary" id="o26577" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="2-5-lobed" value_original="2-5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26578" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="additional" value_original="additional" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="tendril-like" value_original="tendril-like" />
      </biological_entity>
      <biological_entity id="o26579" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="broadly ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="more or less prominently; adaxially" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o26580" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="broadly ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="more or less prominently; adaxially" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially nearly glabrous to densely pubescent, not glaucous.</text>
      <biological_entity id="o26581" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="abaxially nearly glabrous" name="pubescence" src="d0_s3" to="densely pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 1-7-flowered.</text>
      <biological_entity id="o26582" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers ovoid to urn-shaped;</text>
      <biological_entity id="o26583" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s5" to="urn-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals pale to dark bluish or reddish purple, sometimes whitish toward tip, ovatelanceolate, 1.2-3 (-4) cm (larger sepals mostly in w part of range), margins narrowly expanded distally to about 1 mm wide, thin, crispate toward tip, tomentose, tips acuminate, recurved, abaxially sparsely to densely appressed-puberulent.</text>
      <biological_entity id="o26584" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s6" to="dark bluish or reddish purple" />
        <character constraint="toward tip" constraintid="o26585" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26585" name="tip" name_original="tip" src="d0_s6" type="structure" />
      <biological_entity id="o26586" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="size" src="d0_s6" value="expanded" value_original="expanded" />
        <character constraint="to about 1 mm" is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character constraint="toward tip" constraintid="o26587" is_modifier="false" name="shape" src="d0_s6" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o26587" name="tip" name_original="tip" src="d0_s6" type="structure" />
      <biological_entity id="o26588" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="abaxially sparsely; sparsely to densely" name="pubescence" src="d0_s6" value="appressed-puberulent" value_original="appressed-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Achenes: bodies appressed-pubescent;</text>
      <biological_entity id="o26589" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <biological_entity id="o26590" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak 1-3 cm, nearly glabrous to ± appressed-pubescent or silky.</text>
      <biological_entity id="o26591" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o26592" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="nearly glabrous" name="pubescence" src="d0_s8" to="more or less appressed-pubescent or silky" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Ind., Iowa, Kans., Ky., Mo., N.Mex., Nebr., Okla., Tenn., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Bellflower clematis</other_name>
  <other_name type="common_name">Pitcher's clematis</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Clematis pitcheri is highly variable, notably in the size and thickness of the leaflets, the external sepal color and internal color of the recurved tips, and the amount of pubescence of the beaks. Additional varieties might be recognized, as some authors have done in the past, but the extent of intergradation and the lack of correlation among varying traits tend to make recognition of additional varieties impractical (W. M. Dennis 1976). The two varieties recognized here show very extensive intergradation in the western part of the range of the species.</discussion>
  <discussion>Although otherwise similar to Clemitis reticulata, C. pitcheri differs distinctly in its more coarsely reticulate leaves, with the smallest closed areoles mostly over 2 mm long, and its scarcely raised tertiary and quaternary veins.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets mostly 4–11 cm, thin; stamens with filaments and extended connectives usually pubescent.</description>
      <determination>22a Clematis pitcheri var. pitcheri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets mostly 3–4 cm, somewhat leathery; stamens with filaments and extended connectives usually glabrous or nearly so.</description>
      <determination>22b Clematis pitcheri var. dictyota</determination>
    </key_statement>
  </key>
</bio:treatment>