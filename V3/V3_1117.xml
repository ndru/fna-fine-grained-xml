<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Spach" date="1839" rank="section">Meconella</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="species">macounii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 247. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section meconella;species macounii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500847</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, with persistent leaf-bases.</text>
      <biological_entity id="o23459" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23460" name="leaf-base" name_original="leaf-bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o23459" id="r3210" name="with" negation="false" src="d0_s0" to="o23460" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole to 3/4 length of leaf;</text>
      <biological_entity id="o23461" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o23462" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0 length of leaf" name="length" src="d0_s1" to="3/4 length of leaf" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate, 1-2×-lobed;</text>
      <biological_entity id="o23463" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23464" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-2×-lobed" value_original="1-2×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>primary lobes generally lanceolate, sometimes strap-shaped, apex acute or obtuse.</text>
      <biological_entity id="o23465" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="primary" id="o23466" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="generally" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="strap--shaped" value_original="strap--shaped" />
      </biological_entity>
      <biological_entity id="o23467" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: scapes erect.</text>
      <biological_entity id="o23468" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o23469" name="scape" name_original="scapes" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsules more than 4 times longer than broad.</text>
      <biological_entity id="o23470" name="capsule" name_original="capsules" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size_or_width" src="d0_s5" value="4+ times longer than broad" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Russia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Russia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants robust; persistent leaf bases numerous, firm, acuminate; leaf blades green on both surfaces, hispid.</description>
      <determination>6a Papaver macounii subsp. macounii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants slender; persistent leaf bases few, flexible, lanceolate; leaf blades light green or glaucous abaxially, dark green adaxially, glabrate to densely pilose.</description>
      <determination>6b Papaver macounii subsp. discolor</determination>
    </key_statement>
  </key>
</bio:treatment>