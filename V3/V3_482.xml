<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Ewan) N. I. Malyutin" date="1987" rank="subsection">echinata</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">hesperium</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>12: 53. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection echinata;species hesperium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500509</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (11-) 40-80 (-120) cm;</text>
      <biological_entity id="o5641" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually reddish, longitudinally ridged, puberulent.</text>
      <biological_entity id="o5642" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="longitudinally" name="shape" src="d0_s1" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o5643" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5644" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-3 at anthesis;</text>
      <biological_entity constraint="basal" id="o5645" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 3-8 at anthesis;</text>
      <biological_entity constraint="cauline" id="o5646" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="3" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5-7 cm, petioles of proximal leaves glabrous to puberulent.</text>
      <biological_entity id="o5647" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5648" name="petiole" name_original="petioles" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s5" to="puberulent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5649" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o5648" id="r794" name="part_of" negation="false" src="d0_s5" to="o5649" />
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 1-4 × 2-6 cm, usually puberulent, especially abaxially;</text>
      <biological_entity id="o5650" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-14, width 3-8 mm (basal), 2-5 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o5651" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="14" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (5-) 15-30 (-100) -flowered, moderately open;</text>
      <biological_entity id="o5652" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(5-)15-30(-100)-flowered" value_original="(5-)15-30(-100)-flowered" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel (0.5-) 1-2.5 (-7.5) cm, puberulent;</text>
      <biological_entity id="o5653" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-6 (-12) mm from flowers, green to blue, margins often white, linear-lanceolate, 3-7 (-12) mm, puberulent.</text>
      <biological_entity id="o5654" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o5655" from="2" from_unit="mm" name="location" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s10" to="blue" />
      </biological_entity>
      <biological_entity id="o5655" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5656" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark blue to white, puberulent, lateral sepals spreading, 7-13 (-16) × 3-10 mm, spurs straight to upcurved, ascending 30-45° above horizontal, 9-18 mm;</text>
      <biological_entity id="o5657" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5658" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s11" to="white" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5659" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5660" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="upcurved" value_original="upcurved" />
        <character constraint="above horizontal" is_modifier="false" modifier="30-45°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades slightly elevated, ± exposing stamens, 3-8 mm, clefts 1-5 mm;</text>
      <biological_entity id="o5661" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o5662" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5663" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o5664" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o5662" id="r795" modifier="more or less" name="exposing" negation="false" src="d0_s12" to="o5663" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered, denser on inner lobes near base of cleft, white.</text>
      <biological_entity id="o5665" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o5666" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character constraint="on inner lobes" constraintid="o5667" is_modifier="false" name="density" src="d0_s13" value="denser" value_original="denser" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5667" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o5668" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o5667" id="r796" name="near" negation="false" src="d0_s13" to="o5668" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 8-18 mm, 2.2-3 (-4.2) times longer than wide, sparse puberulent.</text>
      <biological_entity id="o5669" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="2.2-3(-4.2)" value_original="2.2-3(-4.2)" />
        <character is_modifier="false" name="count_or_density" src="d0_s14" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds not echinate, appearing ± smooth to naked eye;</text>
      <biological_entity id="o5670" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="echinate" value_original="echinate" />
      </biological_entity>
      <biological_entity id="o5671" name="eye" name_original="eye" src="d0_s15" type="structure">
        <character char_type="range_value" from="less smooth" is_modifier="true" name="architecture" src="d0_s15" to="naked" />
      </biological_entity>
      <relation from="o5670" id="r797" name="appearing" negation="false" src="d0_s15" to="o5671" />
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cells with margins straight, surfaces smooth or roughened.</text>
      <biological_entity constraint="seed-coat" id="o5672" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity id="o5673" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5674" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o5672" id="r798" name="with" negation="false" src="d0_s16" to="o5673" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>Delphinium hesperium is thought to be poisonous to cattle (D. E. Moerman 1986, no subspecies specified). It is often confused with D. hansenii. See discussion under that species for distinguishing features.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral sepals 4(–5) mm wide or less; inflorescence dense (individual flowers often touching in pressed specimens).</description>
      <determination>39b Delphinium hesperium subsp. cuyamacae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral sepals more than 4 mm wide; inflorescence open (individual flowers seldom touching in pressed specimens).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals dark blue-purple.</description>
      <determination>39a Delphinium hesperium subsp. hesperium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals white to pinkish, rarely light blue.</description>
      <determination>39c Delphinium hesperium subsp. pallescens</determination>
    </key_statement>
  </key>
</bio:treatment>