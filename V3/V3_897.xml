<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1833" rank="genus">dicentra</taxon_name>
    <taxon_name authority="Engelmann" date="1881" rank="species">ochroleuca</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>6: 223. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus dicentra;species ochroleuca</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500578</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Lants perennial, caulescent, glaucous, from stout taproots.</text>
      <biological_entity id="o12263" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o12264" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o12263" id="r1704" name="from" negation="false" src="d0_s0" to="o12264" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 or more, rigidly erect, 10-20 (-40) dm, 2-3 cm or more diam. at base.</text>
      <biological_entity id="o12265" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" unit="or more" value="1" value_original="1" />
        <character is_modifier="false" modifier="rigidly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
        <character char_type="range_value" constraint="at base" constraintid="o12266" from="2" from_unit="cm" name="diam" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12266" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves (12-) 25-35 (-50) × (6-) 10-20 (-35) cm;</text>
      <biological_entity id="o12267" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="atypical_length" src="d0_s2" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_width" src="d0_s2" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 4 orders of leaflets and lobes;</text>
      <biological_entity id="o12268" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o12269" name="order" name_original="orders" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o12270" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o12271" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o12268" id="r1705" name="with" negation="false" src="d0_s3" to="o12269" />
      <relation from="o12269" id="r1706" name="part_of" negation="false" src="d0_s3" to="o12270" />
      <relation from="o12269" id="r1707" name="part_of" negation="false" src="d0_s3" to="o12271" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate leaflets (10-) 20-30 (-60) × (5-) 8-15 (-30) mm, ultimate lobes linear-lanceolate.</text>
      <biological_entity constraint="ultimate" id="o12272" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12273" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, paniculate, 5-many-flowered, subglobose;</text>
      <biological_entity id="o12274" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-many-flowered" value_original="5-many-flowered" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts nearly round to ovate, 4-8 × 3-7 mm, margins entire.</text>
      <biological_entity id="o12275" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="nearly round" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12276" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect;</text>
      <biological_entity id="o12277" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel rigid, 2-10 mm;</text>
      <biological_entity id="o12278" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="rigid" value_original="rigid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals nearly round to ovate, (5-) 7-10 (-12) × 3-7 (-9) mm;</text>
      <biological_entity id="o12279" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="nearly round" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals dull to yellowish white;</text>
      <biological_entity id="o12280" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s10" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish white" value_original="yellowish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer petals purple-tipped, (15-) 22-26 (-30) mm, reflexed portion 5-12 mm;</text>
      <biological_entity constraint="outer" id="o12281" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="purple-tipped" value_original="purple-tipped" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="22" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s11" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12282" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner petals (15-) 20-22 (-24) mm, claw 6-9 mm and 2/3 width of blade, crest 3-5 mm diam., exceeding apex by 2-4 mm;</text>
      <biological_entity constraint="inner" id="o12283" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="24" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12284" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character name="width" src="d0_s12" value="2/3 width of blade" value_original="2/3 width of blade" />
      </biological_entity>
      <biological_entity id="o12285" name="crest" name_original="crest" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12286" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <relation from="o12285" id="r1708" name="exceeding" negation="false" src="d0_s12" to="o12286" />
    </statement>
    <statement id="d0_s13">
      <text>filaments of each bundle connate from base to shortly below anthers, rarely distinct from near base;</text>
      <biological_entity id="o12287" name="filament" name_original="filaments" src="d0_s13" type="structure" constraint="bundle" constraint_original="bundle; bundle">
        <character constraint="from base" constraintid="o12289" is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character constraint="from base" constraintid="o12291" is_modifier="false" modifier="rarely" name="fusion" notes="" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o12288" name="bundle" name_original="bundle" src="d0_s13" type="structure" />
      <biological_entity id="o12289" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o12290" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <biological_entity id="o12291" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o12287" id="r1709" name="part_of" negation="false" src="d0_s13" to="o12288" />
      <relation from="o12289" id="r1710" name="shortly below" negation="false" src="d0_s13" to="o12290" />
    </statement>
    <statement id="d0_s14">
      <text>nectariferous tissue borne at base of median filament, not projecting into outer petal;</text>
      <biological_entity constraint="filament" id="o12292" name="tissue" name_original="tissue" src="d0_s14" type="structure" constraint_original="filament nectariferous; filament">
        <character constraint="into outer petal" constraintid="o12295" is_modifier="false" modifier="not" name="orientation" src="d0_s14" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o12293" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="median" id="o12294" name="filament" name_original="filament" src="d0_s14" type="structure" />
      <biological_entity constraint="outer" id="o12295" name="petal" name_original="petal" src="d0_s14" type="structure" />
      <relation from="o12292" id="r1711" name="borne at" negation="false" src="d0_s14" to="o12293" />
      <relation from="o12292" id="r1712" name="part_of" negation="false" src="d0_s14" to="o12294" />
    </statement>
    <statement id="d0_s15">
      <text>stigma shallowly 2-horned, ca. 2 times wider than long.</text>
      <biological_entity id="o12296" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s15" value="2-horned" value_original="2-horned" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s15" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ovoid, attenuate at both ends, (10-) 20-30 (-35) × 5-8 mm.</text>
      <biological_entity id="o12297" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character constraint="at ends" constraintid="o12298" is_modifier="false" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o12298" name="end" name_original="ends" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" notes="alterIDs:o12298" src="d0_s16" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o12298" src="d0_s16" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="alterIDs:o12298" src="d0_s16" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="alterIDs:o12298" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds slightly reniform, ca. 1.3 (rarely to 2) mm diam., densely covered with tiny protuberances, elaiosome absent.</text>
      <biological_entity id="o12299" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="reniform" value_original="reniform" />
        <character name="diameter" src="d0_s17" unit="mm" value="1.3" value_original="1.3" />
      </biological_entity>
      <biological_entity id="o12300" name="protuberance" name_original="protuberances" src="d0_s17" type="structure">
        <character is_modifier="true" name="size" src="d0_s17" value="tiny" value_original="tiny" />
      </biological_entity>
      <relation from="o12299" id="r1713" modifier="densely" name="covered with" negation="false" src="d0_s17" to="o12300" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 32.</text>
      <biological_entity id="o12301" name="elaiosome" name_original="elaiosome" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12302" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry gravelly hillsides, gullies, and disturbed areas, often invading after fire</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry gravelly hillsides" />
        <character name="habitat" value="gullies" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="fire" modifier="often invading after" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>15-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="15" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">White eardrops</other_name>
  <discussion>The seeds of Dicentra ochroleuca usually do not germinate unless desiccated or seared by fire.</discussion>
  
</bio:treatment>