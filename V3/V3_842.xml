<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 61. 1857 ["1856"]</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500384</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or sprawling, 0.1-0.6 m, short pubescent-pilose, sometimes sparsely so.</text>
      <biological_entity id="o22810" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent-pilose" value_original="pubescent-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 1-2 (-3) -pinnate;</text>
      <biological_entity id="o22811" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-2(-3)-pinnate" value_original="1-2(-3)-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>primary leaflets 7-11, mostly deeply 2-several-lobed, leaflets and larger lobes mostly ovate, 0.8-3.5 × 0.5-1.5 cm (lateral lobes nearly as wide as central portion), thin, not prominently reticulate;</text>
      <biological_entity constraint="primary" id="o22812" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s2" to="11" />
        <character is_modifier="false" modifier="mostly deeply" name="shape" src="d0_s2" value="2-several-lobed" value_original="2-several-lobed" />
      </biological_entity>
      <biological_entity id="o22813" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not prominently" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="larger" id="o22814" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not prominently" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces glabrous, somewhat glaucous.</text>
      <biological_entity id="o22815" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, 1-flowered.</text>
      <biological_entity id="o22816" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers broadly urn to bell-shaped;</text>
      <biological_entity id="o22817" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o22818" name="urn" name_original="urn" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="bell--shaped" value_original="bell--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals purple, lanceolate, 1.5-3 cm, margins narrowly expanded distally to ca. 1 mm wide, thin, crispate, tomentose, tips acuminate, spreading, abaxially sparsely pubescent.</text>
      <biological_entity id="o22819" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22820" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly; distally" name="size" src="d0_s6" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o22821" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Achenes: bodies appressed-long-pubescent;</text>
      <biological_entity id="o22822" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <biological_entity id="o22823" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="appressed-long-pubescent" value_original="appressed-long-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak 2-3 cm, glabrous or inconspicuously appressed-pubescent, sparsely so toward tip.</text>
      <biological_entity id="o22824" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o22825" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="inconspicuously" name="pubescence" src="d0_s8" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o22826" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <relation from="o22825" id="r3114" modifier="sparsely" name="toward" negation="false" src="d0_s8" to="o22826" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountain slopes, moist sites in canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountain slopes" />
        <character name="habitat" value="moist sites" constraint="in canyons" />
        <character name="habitat" value="canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Bigelow's clematis</other_name>
  <discussion>Clematis bigelovii is locally common and is restricted to New Mexico and a few sites in Arizona. Although usually grouped with C. hirsutissima because of its scarcely viny habit, this species appears to represent the extreme expression of clinal variation including C. pitcheri var. dictyota and, at the other extreme, eastern populations of C. pitcheri var. pitcheri; it might well be treated as C. pitcheri var. bigelovii (Torrey) B.L. Robinson.</discussion>
  <discussion>Clematis palmeri Rose is represented by a small number of specimens from New Mexico and one from eastern Arizona, and its distinctness as a species has been questioned. The few published descriptions scarcely suffice to indicate its distinguishing features and are sometimes at variance with each other and with specimens so identified by R.O. Erickson (1943) or others. Almost all specimens were found where C. bigelovii was reported nearby. Pending further studies, it seems likely that C. palmeri comprises somewhat aberrant specimens of C. bigelovii, C. hirsutissima var. scottii, and/or C. pitcheri var. dictyota, perhaps also some herbarium sheets of flowering and fruiting material inadvertently collected from different species, and/or hybrids involving the species named above in one or more combinations.</discussion>
  
</bio:treatment>