<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David Whetstone, Daniel D. Spaulding, T.A. Atkinson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="C. Morren &amp; Decaisne" date="1834" rank="genus">VANCOUVERIA</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 2: 351. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus VANCOUVERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for George Vancouver (1757-1798), English navigator and explorer</other_info_on_name>
    <other_info_on_name type="fna_id">134345</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, evergreen or deciduous, 1-5 dm, glabrous, glandular-pubescent, or sparsely hairy.</text>
      <biological_entity id="o16516" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes extensive, creeping, nodose, producing 3 or more foliage leaves and flowering shoots per year.</text>
      <biological_entity id="o16517" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="extensive" value_original="extensive" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="shape" src="d0_s1" value="nodose" value_original="nodose" />
      </biological_entity>
      <biological_entity id="o16518" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o16519" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o16520" name="year" name_original="year" src="d0_s1" type="structure" />
      <relation from="o16517" id="r2263" name="producing" negation="false" src="d0_s1" to="o16518" />
      <relation from="o16519" id="r2264" name="per" negation="false" src="d0_s1" to="o16520" />
    </statement>
    <statement id="d0_s2">
      <text>Aerial stems absent.</text>
      <biological_entity id="o16521" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, alternate, 2-3-ternately compound (sometimes 3-5-foliolate in V. chrysantha);</text>
      <biological_entity id="o16522" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="2-3-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole long, slender.</text>
      <biological_entity id="o16523" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade deltate in overall outline;</text>
      <biological_entity id="o16524" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character constraint="in outline" constraintid="o16525" is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o16525" name="outline" name_original="outline" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis without pulvinae;</text>
      <biological_entity id="o16526" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o16527" name="pulvina" name_original="pulvinae" src="d0_s6" type="structure" />
      <relation from="o16526" id="r2265" name="without" negation="false" src="d0_s6" to="o16527" />
    </statement>
    <statement id="d0_s7">
      <text>leaflet blades rhomboid or rounded-pentagonal to ovate to oblong, shallowly 3-lobed, margins entire to sinuate;</text>
      <biological_entity constraint="leaflet" id="o16528" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded-pentagonal" name="shape" src="d0_s7" to="ovate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s7" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation palmate.</text>
      <biological_entity id="o16529" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s7" to="sinuate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, racemes or panicles, open.</text>
      <biological_entity id="o16530" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16531" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o16532" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 3-merous, 6-14 mm;</text>
      <biological_entity id="o16533" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-merous" value_original="3-merous" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracteoles 6-9, sepaloid;</text>
      <biological_entity id="o16534" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="9" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sepaloid" value_original="sepaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 6, white to yellow;</text>
      <biological_entity id="o16535" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 6, white to yellow, hooded with tip reflexed or flat, bearing nectar;</text>
      <biological_entity id="o16536" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="yellow" />
        <character constraint="with tip" constraintid="o16537" is_modifier="false" name="architecture" src="d0_s13" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o16537" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o16538" name="nectar" name_original="nectar" src="d0_s13" type="structure" />
      <relation from="o16536" id="r2266" name="bearing" negation="false" src="d0_s13" to="o16538" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 6;</text>
      <biological_entity id="o16539" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dehiscing by 2 apically hinged flaps;</text>
      <biological_entity id="o16540" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character constraint="by flaps" constraintid="o16541" is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o16541" name="flap" name_original="flaps" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pollen exine striate;</text>
      <biological_entity constraint="pollen" id="o16542" name="exine" name_original="exine" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s16" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovaries ellipsoid;</text>
    </statement>
    <statement id="d0_s18">
      <text>placentation marginal;</text>
      <biological_entity id="o16543" name="ovary" name_original="ovaries" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="placentation" src="d0_s18" value="marginal" value_original="marginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>style lateral.</text>
      <biological_entity id="o16544" name="style" name_original="style" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits follicles, brown, asymmetric, generally elliptic, dehiscing by 2 valves.</text>
      <biological_entity constraint="fruits" id="o16545" name="follicle" name_original="follicles" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="generally" name="arrangement_or_shape" src="d0_s20" value="elliptic" value_original="elliptic" />
        <character constraint="by valves" constraintid="o16546" is_modifier="false" name="dehiscence" src="d0_s20" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o16546" name="valve" name_original="valves" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds 4-7, black to reddish-brown;</text>
      <biological_entity id="o16547" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s21" to="7" />
        <character char_type="range_value" from="black" name="coloration" src="d0_s21" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>aril whitish, covering ca. 1/2-2/3 of seed.</text>
      <biological_entity id="o16549" name="seed" name_original="seed" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>x = 6.</text>
      <biological_entity id="o16548" name="aril" name_original="aril" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="position_relational" src="d0_s22" value="covering" value_original="covering" />
        <character char_type="range_value" constraint="of seed" constraintid="o16549" from="1/2" name="quantity" src="d0_s22" to="2/3" />
      </biological_entity>
      <biological_entity constraint="x" id="o16550" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Inside-out flower</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>The fruits of Vancouveria are thin-walled follicles that are green or greenish brown at the time of dehiscence. The follicles dehisce by means of two valves that begin below the style and open to the base. The two valves recurve, exposing the seeds downward. In V. hexandra the follicle opens before the seeds are mature. The green seeds continue to grow and ripen in the open follicle. The appendage or aril on Vancouveria seeds has been shown to elaiosome (R. Y. Berg 1972). Ants carry the seeds to their nests and harvest the appendage as a food source.</discussion>
  <references>
    <reference>Berg, R. Y. 1972. Dispersal ecology of Vancouveria (Berberidaceae). Amer. J. Bot. 59: 109-122.</reference>
    <reference>Stearn, W. T. 1938. Epimedium and Vancouveria, a monograph. J. Linn. Soc., Bot. 51: 409-535.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet margins not conspicuously thickened; leaves falling when fruits maturing; pedicels lacking glands.</description>
      <determination>1 Vancouveria hexandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet margins conspicuously thickened; leaves persistent; pedicels stipitate-glandular.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals yellow, petal apex reflexed; follicles densely stipitate-glandular.</description>
      <determination>2 Vancouveria chrysantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals white, sometimes lavender-tinged, petal apex not reflexed; follicles lacking glands.</description>
      <determination>3 Vancouveria planipetala</determination>
    </key_statement>
  </key>
</bio:treatment>