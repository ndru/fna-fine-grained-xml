<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">aconitum</taxon_name>
    <taxon_name authority="de Candolle" date="1817" rank="species">delphiniifolium</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 380. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus aconitum;species delphiniifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500017</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aconitum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">delphiniifolium</taxon_name>
    <taxon_name authority="(Reichenbach) Hultén" date="unknown" rank="subspecies">chamissonianum</taxon_name>
    <taxon_hierarchy>genus Aconitum;species delphiniifolium;subspecies chamissonianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aconitum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">delphiniifolium</taxon_name>
    <taxon_name authority="(Reichenbach) Hultén" date="unknown" rank="subspecies">paradoxum</taxon_name>
    <taxon_hierarchy>genus Aconitum;species delphiniifolium;subspecies paradoxum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aconitum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">delphiniifolium</taxon_name>
    <taxon_name authority="(Reichenbach) S. L. Welsh" date="unknown" rank="variety">paradoxum</taxon_name>
    <taxon_hierarchy>genus Aconitum;species delphiniifolium;variety paradoxum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots tuberous, tuber ca. 10-15 × 5 mm, with 1 (rarely more) contiguous daughter tuber.</text>
      <biological_entity id="o18753" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o18754" name="tuber" name_original="tuber" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s0" to="15" to_unit="mm" />
        <character name="width" src="d0_s0" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="daughter" id="o18755" name="tuber" name_original="tuber" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="1" value_original="1" />
        <character is_modifier="true" name="arrangement" src="d0_s0" value="contiguous" value_original="contiguous" />
      </biological_entity>
      <relation from="o18754" id="r2512" name="with" negation="false" src="d0_s0" to="o18755" />
    </statement>
    <statement id="d0_s1">
      <text>Stem erect, slender, 1-10 dm.</text>
      <biological_entity id="o18756" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: blade 3-divided to base, (1-) 7 (-13) cm wide, occasionally with up to 2 (-4) mm of leaf tissue between deepest sinus and base of blade, 2 lateral segments each deeply 2-parted;</text>
      <biological_entity constraint="cauline" id="o18757" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="between sinus and base" constraintid="o18761-o18762" id="o18758" name="blade" name_original="blade" src="d0_s2" type="structure" constraint_original="between  sinus and  base, ">
        <character constraint="to base" constraintid="o18759" is_modifier="false" name="shape" src="d0_s2" value="3-divided" value_original="3-divided" />
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18759" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="alterIDs:o18759" src="d0_s2" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="width" notes="alterIDs:o18759" src="d0_s2" to="13" to_unit="cm" />
        <character name="width" notes="alterIDs:o18759" src="d0_s2" unit="cm" value="7" value_original="7" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o18760" name="tissue" name_original="tissue" src="d0_s2" type="structure" />
      <biological_entity constraint="deepest" id="o18761" name="sinus" name_original="sinus" src="d0_s2" type="structure" />
      <biological_entity constraint="deepest" id="o18762" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o18763" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o18764" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="2-parted" value_original="2-parted" />
      </biological_entity>
      <relation from="o18758" id="r2513" modifier="with 0-2(-4) mm" name="part_of" negation="false" src="d0_s2" to="o18760" />
      <relation from="o18758" id="r2514" name="part_of" negation="false" src="d0_s2" to="o18763" />
    </statement>
    <statement id="d0_s3">
      <text>margins cleft into narrow, linear-oblong lobes.</text>
      <biological_entity constraint="cauline" id="o18765" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18766" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="into lobes" constraintid="o18767" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o18767" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="shape" src="d0_s3" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences open racemes or panicles.</text>
      <biological_entity id="o18768" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18769" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
      <biological_entity id="o18770" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
      <relation from="o18768" id="r2515" name="open" negation="false" src="d0_s4" to="o18769" />
      <relation from="o18768" id="r2516" name="open" negation="false" src="d0_s4" to="o18770" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers blue, purple, occasionally greenish purple, yellowish, or white, 20-40 mm from tips of pendent sepals to top of hood;</text>
      <biological_entity id="o18771" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s5" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" constraint="from tips" constraintid="o18772" from="20" from_unit="mm" name="location" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18772" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity id="o18773" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o18774" name="hood" name_original="hood" src="d0_s5" type="structure" />
      <relation from="o18772" id="r2517" name="part_of" negation="false" src="d0_s5" to="o18773" />
      <relation from="o18772" id="r2518" name="top of" negation="false" src="d0_s5" to="o18774" />
    </statement>
    <statement id="d0_s6">
      <text>pendent sepals 9-20 mm;</text>
      <biological_entity id="o18775" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hood low, crescent-shaped to hemispheric to conic-hemispheric, 10-24 mm high receptacle to top of hood, 13-30 mm broad from receptacle to beak apex.</text>
      <biological_entity id="o18776" name="hood" name_original="hood" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="low" value_original="low" />
        <character char_type="range_value" from="crescent-shaped" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18778" name="top" name_original="top" src="d0_s7" type="structure" />
      <biological_entity id="o18779" name="hood" name_original="hood" src="d0_s7" type="structure" />
      <biological_entity id="o18780" name="receptacle" name_original="receptacle" src="d0_s7" type="structure" />
      <biological_entity constraint="beak" id="o18781" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o18777" id="r2519" name="to" negation="false" src="d0_s7" to="o18778" />
      <relation from="o18778" id="r2520" name="top of" negation="false" src="d0_s7" to="o18779" />
      <relation from="o18780" id="r2521" name="to" negation="false" src="d0_s7" to="o18781" />
    </statement>
    <statement id="d0_s8">
      <text>2n=16.</text>
      <biological_entity id="o18777" name="receptacle" name_original="receptacle" src="d0_s7" type="structure">
        <character is_modifier="true" name="height" src="d0_s7" value="high" value_original="high" />
        <character char_type="range_value" constraint="from receptacle" constraintid="o18780" from="13" from_unit="mm" name="width" notes="" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18782" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (mid Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="mid Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, along creeks, thickets, woods, rocky slopes, and alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" constraint="along creeks , thickets , woods , rocky slopes , and alpine tundra" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="alpine tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska; Asia (Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Aconitum delphiniifolium is variable and was divided into three intergrading subspecies by E. Hultén (1941-1950, vol. 4) on basis of plant size, flower size, number of flowers, petal morphology, and narrow versus somewhat broader leaf lobing. Because of the "paucity of definitive criteria," J. A. Calder and R. L. Taylor (1968, vol.1) and S. L. Welsh (1974) chose not to recognize infraspecific taxa within A. delphiniifolium. Similarly, we have deferred formal recognition of infraspecific taxa within this species pending population studies. The most distinctive group within A. delphiniifolium has one to several large flowers with shallow nectaries borne on diminutive plants. These are sometimes distinguished as subsp. paradoxum, and they exhibit a different pattern of variation from that found in A. columbianum, in which large flower size is associated almost invariably with large, robust plants and deep nectaries.</discussion>
  <discussion>More typical forms of Aconitum delphiniifolium are similar to A. columbianum except that leaves tend to be more deeply divided and the hood tends to be lower, i.e., more crescent-shaped and less conic. Some plants and populations of A. columbianum also have leaves divided almost to the base and low hoods. Intergradation between the two species should be investigated more fully.</discussion>
  <discussion>The Salishan used Aconitum delphiniifolium for unspecified medicinal purposes (D. E. Moerman 1986).</discussion>
  
</bio:treatment>