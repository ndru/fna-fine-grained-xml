<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Wernischeck" date="1763" rank="genus">cimicifuga</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 316. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus cimicifuga;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500375</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cimicifuga</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">cordifolia</taxon_name>
    <taxon_hierarchy>genus Cimicifuga;species cordifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cimicifuga</taxon_name>
    <taxon_name authority="(Linnaeus) Nuttall" date="unknown" rank="species">racemosa</taxon_name>
    <taxon_name authority="(Pursh) A. Gray" date="unknown" rank="variety">cordifolia</taxon_name>
    <taxon_hierarchy>genus Cimicifuga;species racemosa;variety cordifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 60-250 cm, usually glabrous, rarely sparsely pubescent.</text>
      <biological_entity id="o26838" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole rounded abaxially, 8-16 cm, broadly and shallowly grooved adaxially, glabrous, rarely with a few lax hairs in groove.</text>
      <biological_entity id="o26839" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o26840" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="broadly; shallowly; adaxially" name="architecture" src="d0_s1" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26841" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o26842" name="groove" name_original="groove" src="d0_s1" type="structure" />
      <relation from="o26840" id="r3616" modifier="rarely" name="with" negation="false" src="d0_s1" to="o26841" />
      <relation from="o26841" id="r3617" name="in" negation="false" src="d0_s1" to="o26842" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade (2-) 3-ternately compound;</text>
      <biological_entity id="o26843" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="(2-)3-ternately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 32-100;</text>
      <biological_entity id="o26844" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="32" name="quantity" src="d0_s3" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>terminal leaflet of central segment ovate to oblong, incisely 3-lobed, 6-15 × 4-14 cm, with 3 veins arising basally, base cuneate to somewhat cordate, margins dentate to serrate, apex acute to acuminate, surfaces glabrous or glabrate;</text>
      <biological_entity constraint="segment" id="o26845" name="leaflet" name_original="leaflet" src="d0_s4" type="structure" constraint_original="segment terminal; segment">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong" />
        <character is_modifier="false" modifier="incisely" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s4" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o26846" name="segment" name_original="segment" src="d0_s4" type="structure" />
      <biological_entity id="o26847" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s4" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o26848" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate to somewhat" value_original="cuneate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o26849" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="dentate to serrate" value_original="dentate to serrate" />
      </biological_entity>
      <biological_entity id="o26850" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <relation from="o26845" id="r3618" name="part_of" negation="false" src="d0_s4" to="o26846" />
      <relation from="o26845" id="r3619" name="with" negation="false" src="d0_s4" to="o26847" />
    </statement>
    <statement id="d0_s5">
      <text>other leaflets 3-15 × 3-10 cm.</text>
      <biological_entity id="o26851" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o26852" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ± lax panicles of 3-10 racemelike branches, 10-50 cm, densely short-pubescent and granular;</text>
      <biological_entity id="o26853" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o26854" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26855" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="true" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
      </biological_entity>
      <relation from="o26854" id="r3620" name="consist_of" negation="false" src="d0_s6" to="o26855" />
    </statement>
    <statement id="d0_s7">
      <text>bract 1, subtending pedicel, broadly triangular, acute, 1-3 mm;</text>
      <biological_entity id="o26856" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26857" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <relation from="o26856" id="r3621" name="subtending" negation="false" src="d0_s7" to="o26857" />
    </statement>
    <statement id="d0_s8">
      <text>pedicel to 20 mm, granular, bracteoles present along most of pedicel, narrowly triangular, acute.</text>
      <biological_entity id="o26858" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief_or_texture" src="d0_s8" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity id="o26859" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character constraint="of pedicel" constraintid="o26860" is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o26860" name="pedicel" name_original="pedicel" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, 3 outer white tinged red, 2 inner yellowish green;</text>
      <biological_entity id="o26861" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26862" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26863" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white tinged" value_original="white tinged" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o26864" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually 2, rarely more, sessile, body yellowish with white lobes, ovate, 3-6 mm;</text>
      <biological_entity id="o26865" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26866" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o26867" name="body" name_original="body" src="d0_s10" type="structure">
        <character constraint="with lobes" constraintid="o26868" is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26868" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary basal;</text>
      <biological_entity id="o26869" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26870" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 40-70;</text>
      <biological_entity id="o26871" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26872" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 6-10 mm;</text>
      <biological_entity id="o26873" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o26874" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistils 3-8, short-stipitate, body glabrous, stipe granular;</text>
      <biological_entity id="o26875" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o26876" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="short-stipitate" value_original="short-stipitate" />
      </biological_entity>
      <biological_entity id="o26877" name="body" name_original="body" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26878" name="stipe" name_original="stipe" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence_or_relief_or_texture" src="d0_s14" value="granular" value_original="granular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style subulate, often curved;</text>
      <biological_entity id="o26879" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o26880" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma minute, 0.2 mm wide.</text>
      <biological_entity id="o26881" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o26882" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="minute" value_original="minute" />
        <character name="width" src="d0_s16" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Follicles 2-4, stipitate, obovate, laterally compressed, 8-17 mm, membranous walled, glabrous.</text>
      <biological_entity id="o26883" name="follicle" name_original="follicles" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="17" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="walled" value_original="walled" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds pale-brown, lenticular, ca. 3.5 mm, covered with whitish, broad, lacerate scales but not apprearing cylindric.</text>
      <biological_entity id="o26884" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="shape" src="d0_s18" value="lenticular" value_original="lenticular" />
        <character name="some_measurement" src="d0_s18" unit="mm" value="3.5" value_original="3.5" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s18" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o26885" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s18" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="width" src="d0_s18" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s18" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o26884" id="r3622" name="covered with" negation="false" src="d0_s18" to="o26885" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Aug–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, rich, rocky and boulder-strewn, wooded slopes and coves</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="slopes" modifier="boulder-strewn wooded" />
        <character name="habitat" value="coves" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="wooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., Ill., Ky., Md., N.C., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">American bugbane</other_name>
  <discussion>Much confusion has developed over the name Cimicifuga cordifolia Pursh. Although F. Pursh (1814, vol. 2, pp. 372-373) considered it synonymous with C. americana, several other authors have variously misapplied the name C. cordifolia to C. racemosa and C. rubifolia.</discussion>
  
</bio:treatment>