<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan T. Whittemore</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">RANUNCULUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 548. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 243. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus RANUNCULUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin rana, frog, unculus, little, allusion to the wet habitats in which some species grow</other_info_on_name>
    <other_info_on_name type="fna_id">127971</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, from tuberous roots, caudices, rhizomes, stolons, or bulbous stem-bases.</text>
      <biological_entity id="o9438" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9439" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o9440" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o9441" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o9442" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o9443" name="stem-base" name_original="stem-bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
      <relation from="o9438" id="r1326" name="from" negation="false" src="d0_s0" to="o9439" />
      <relation from="o9438" id="r1327" name="from" negation="false" src="d0_s0" to="o9440" />
      <relation from="o9438" id="r1328" name="from" negation="false" src="d0_s0" to="o9441" />
      <relation from="o9438" id="r1329" name="from" negation="false" src="d0_s0" to="o9442" />
      <relation from="o9438" id="r1330" name="from" negation="false" src="d0_s0" to="o9443" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal, cauline, or both, simple, variously lobed or parted, or compound, all petiolate or distal leaves sessile;</text>
      <biological_entity id="o9444" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="parted" value_original="parted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9445" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate (rarely a distal pair opposite in Ranunculus sect. Flammula).</text>
      <biological_entity constraint="cauline" id="o9446" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade reniform to linear, margins entire, crenate, or toothed.</text>
      <biological_entity id="o9447" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o9448" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or axillary, 2-50-flowered cymes to 25 cm or solitary flowers;</text>
      <biological_entity id="o9449" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o9450" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-50-flowered" value_original="2-50-flowered" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9451" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts present or absent, small or large and leaflike, not forming involucre.</text>
      <biological_entity id="o9452" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s5" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o9453" name="involucre" name_original="involucre" src="d0_s5" type="structure" />
      <relation from="o9452" id="r1331" name="forming" negation="true" src="d0_s5" to="o9453" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o9454" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals sometimes persistent in fruit, 3-5 (-6), green or sometimes purple, yellow, or white, plane (base saccate in R. ficaria), oblong to elliptic, ovate, or lanceolate, 1-15 mm;</text>
      <biological_entity id="o9455" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o9456" is_modifier="false" modifier="sometimes" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="green" value_original="green" />
        <character name="coloration" src="d0_s7" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic ovate or lanceolate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic ovate or lanceolate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic ovate or lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9456" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 0-22 (-150), distinct, yellow, rarely white, red, or green, plane, linear to orbiculate, 1-26 mm;</text>
      <biological_entity id="o9457" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="22" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="150" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="22" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="orbiculate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary present, usually covered by scale;</text>
      <biological_entity id="o9458" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9459" name="scale" name_original="scale" src="d0_s9" type="structure" />
      <relation from="o9458" id="r1332" modifier="usually" name="covered by" negation="false" src="d0_s9" to="o9459" />
    </statement>
    <statement id="d0_s10">
      <text>stamens (5-) 10-many;</text>
      <biological_entity id="o9460" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="(5-)10-many" value_original="(5-)10-many" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments filiform;</text>
      <biological_entity id="o9461" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o9462" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character constraint="between stamens, pistils" constraintid="o9463, o9464" is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9463" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o9464" name="pistil" name_original="pistils" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pistils 4-250, simple;</text>
      <biological_entity id="o9465" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="250" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovule 1 per ovary;</text>
      <biological_entity id="o9466" name="ovule" name_original="ovule" src="d0_s14" type="structure">
        <character constraint="per ovary" constraintid="o9467" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9467" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style present or absent.</text>
      <biological_entity id="o9468" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits achenes, rarely utricles, aggregate, sessile, discoid, lenticular, globose, obovoid, or cylindric, sides sometimes veined;</text>
      <biological_entity constraint="fruits" id="o9469" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s16" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s16" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o9470" name="utricle" name_original="utricles" src="d0_s16" type="structure" />
      <biological_entity id="o9471" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak present or absent, terminal, straight or curved, 0-4.5 mm. x = 7, 8.</text>
      <biological_entity id="o9472" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o9473" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide except lowland tropics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide except lowland tropics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Buttercup</other_name>
  <other_name type="common_name">crowfoot</other_name>
  <other_name type="common_name">renoncule</other_name>
  <discussion>Species about 300 (76 in the flora).</discussion>
  <discussion>Most Ranunculus species are poisonous to stock; when abundant, they may be troublesome to ranchers. A few species with acrid juice were formerly used as vesicatories. The genus is badly in need of biosystematic work. Apomixis and interspecific hybridization occur in several Old World groups of buttercups; some of the taxonomic complexity of the New World species probably results from these processes.</discussion>
  <discussion>Considerable disagreement exists among authors on the proper generic and infrageneric classification of Ranunculus. Most of the subgenera accepted here have been treated as separate genera at one time or another. All recent studies have been based on local or continental floras, however, and classifications proposed for one region may not work for the plants of other regions. Like most North American workers, I have followed the generic and infrageneric classification of L. D. Benson (1948), who gave by far the most thorough and best documented study of the problem. The genus and its subdivisions should be studied on a worldwide basis.</discussion>
  <references>
    <reference>Benson, L. D. 1948. A treatise on the North American Ranunculi. Amer. Midl. Naturalist 40: 1-261.</reference>
    <reference>Benson, L. D. 1954. Supplement to a treatise on the North American Ranunculi. Amer. Midl. Naturalist 52: 328-369.</reference>
    <reference>Cook, C. D. K. 1966. A monographic study of Ranunculus subgenus Batrachium (DC.) A. Gray. Mitt. Bot. Staatssamml. München 6: 47-237.</reference>
    <reference>Duncan, T. 1980. A taxonomic study of the Ranunculus hispidus Michaux complex in the Western Hemisphere. Univ. Calif. Publ. Bot. 77: 1-125.</reference>
    <reference>Nesom, G. L. 1993. Ranunculus (Ranunculaceae) in Nuevo León, with comments on the R. petiolaris group. Phytologia 75: 391-398.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">All leaves simple and unlobed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Some or all leaves simple and lobed, or compound.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves absent or scalelike; sepals deciduous or persistent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves present, well developed; sepals deciduous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals 5; achene beak 0.1-0.2 mm; plants stoloniferous, without caudices.</description>
      <determination>2b2 Ranunculus subg. Cyrtorhyncha sect. Halodes (Ranunculus cymbalaria)</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals 8-18; achene beak 0.8-1.4 mm; plants not stoloniferous, stems erect from short caudices.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades undivided, margins entire or serrulate; sepals persistent in fruit; petals 7-12 mm.</description>
      <determination>2d Ranunculus subg. Oxygraphis (Ranunculus kamtschaticus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades shallowly lobed, margins crenate; sepals deciduous; petals 2-4 mm</description>
      <determination>2b4 Ranunculus subg. Cyrtorhyncha sect. Pseudaphanostemma (Ranunculus hystriculus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tuberous roots present; flowers yellow; sepals 3.</description>
      <determination>2h Ranunculus subg. Ficaria (Ranunculus ficaria)</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tuberous roots absent (roots thickened proximally in some 5-sepaled species of sect. Flammula); flowers yellow, white, or pink.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Achenes 4.2-5.2 mm, achene body prolonged beyond seed as corky distal appendage; sepals 3, 6-10 mm; petals white to pink.</description>
      <determination>2f Ranunculus subg. Pallasiantha (Ranunculus pallasii)</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Achenes 0.8-2.8 mm, achene body not prolonged beyond seed; sepals (3-)4-5, 1.5-12 mm; petals yellow.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Sepals covered with dense brown pubescence; distal leaves and bracts apically 3-crenate or shallowly 3-lobed, otherwise undivided.</description>
      <determination>2a3 Ranunculus subg. Ranunculus sect. Epirotes (Ranunculus macauleyi)</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Sepals glabrous or with colorless hairs; distal leaves simple and undivided.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achene wall papery, longitudinally ribbed; leaf apex broadly rounded to truncate, margins crenate.</description>
      <determination>2b2 Ranunculus subg. Cyrtorhyncha sect. Halodes (Ranunculus cymbalaria)</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achene wall thick, not ornamented, smooth (sometimes pubescent); leaf apex acuminate to rounded-obtuse, margins entire or finely toothed.</description>
      <determination>2a4 Ranunculus sect. Flammula</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leafy stems creeping and rooting at nodes or floating in water, then rootless.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leafy stems erect or if decumbent rooting only at base, never floating.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves 3-foliolate.</description>
      <determination>2a1 Ranunculus sect. Ranunculus</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves simple, lobed to filiform-dissected or occasionally undivided.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Achene body prolonged beyond seed as corky distal appendage; sepals 3, petals 5-11.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Achene body not prolonged beyond seed; either sepals 5 or sepals 3-4 and petals also 3-4.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade as wide as long, ternately divided to base.</description>
      <determination>2g Ranunculus subg. Coptidium (Ranunculus lapponicus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade much longer than wide, unlobed or lobed.</description>
      <determination>2f Ranunculus subg. Pallasiantha (Ranunculus pallasii)</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals white or white with yellow claws; achenes with strong coarse wrinkles.</description>
      <determination>2e Ranunculus subg. Batrachium</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals yellow; achenes smooth (faintly wrinkled in R. sceleratus var. sceleratus).</description>
      <determination>2a5 Ranunculus sect. Hecatonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals pure red, or white when immature; fruits winged achenes or utricles.</description>
      <determination>2c Ranunculus subg. Crymodes</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals yellow, rarely also with some red pigmentation abaxially, or greenish yellow; fruits achenes, rarely winged.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Cauline leaves absent or scalelike, tuberous roots absent; leaves sometimes deeply parted or dissected, never compound.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Cauline leaves present, simple, lobed or dissected, or compound (rarely reduced to scales in R. fascicularis with tuberous roots and 3-5-foliolate leaves).</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Plants villous; sepals 3-6 × 1-2 mm, persistent in fruit; fruit wall firm, smooth, beak much longer than achene body.</description>
      <determination>2i Ranunculus subg. Ceratocephala (Ranunculus testiculatus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Plants glabrous; sepals 6-13 × 3-7 mm, deciduous in fruit; fruit wall thin, veined, beak much shorter than achene body.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves shallowly 5-7-lobed; petals inconspicuous, 2-4 mm.</description>
      <determination>2b4 Ranunculus subg. Cyrtorhyncha sect. Pseudaphanostemma (Ranunculus hystriculus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves 3-5-parted; petals showy, 8-12 mm.</description>
      <determination>2b3 Ranunculus subg. Cyrtorhyncha Sect. Arcteranthis (Ranunculus cooleyae)</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Style absent, stigma sessile; achene margins thick and corky; emergent aquatic, sometimes also found on very wet soil.</description>
      <determination>2a5 Ranunculus subg. Ranunculus Sect. Hecatonia (Ranunculus sceleratus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Style present; achene margins not corky; in various habitats but rarely aquatic.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Achene wall thin, longitudinally striate; scale of nectary reduced to low ridge, not covering nectary.</description>
      <determination>2b1 Ranunculus subg. Cyrtorhyncha Sect. Cyrtorhyncha (Ranunculus ranunculinus)</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Achene wall thick, smooth, papillose, or spiny; scale of nectary well-developed flap or pocket completely covering nectary.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Achenes thick-lenticular or asymmetrically thick-lenticular to compressed-globose, 1.2-2 times as wide as thick; nectary scale joined with petal on 3 sides, forming pocket enclosing nectary (sometimes with apex free, forming flap shorter than pocket); basal leaves various, unlobed to deeply divided, margins entire to crenate but never at all serrate.</description>
      <determination>2a3 Ranunculus sect. Epirotes</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Achenes strongly flattened, at least 3-15 times as wide as thick; nectary scale free from petal for at least 1/2 its length, thus forming free scale over nectary (scale sometimes free for less than 1/2 its length in R. recurvatus, with serrate to crenate-serrate leaf margins); basal leaves always deeply lobed or compound (except sometimes in R. marginatus and R. orthorhynchus), margins various.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Achenes papillose or spiny (sometimes smooth in R. sardous); flowers small, petals 1-6 mm, scarcely longer than sepals, sometimes absent (larger and much longer than sepals in R. sardous).</description>
      <determination>2a2 Ranunculus sect. Echinella</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Achenes smooth, glabrous or pubescent; flowers small to large, petals always present, 2-22 mm.</description>
      <determination>2a1 Ranunculus sect. Ranunculus</determination>
    </key_statement>
  </key>
</bio:treatment>