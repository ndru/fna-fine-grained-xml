<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Webb) Rouy &amp; Foucaud" date="1893" rank="section">Flammula</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="species">gormanii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 91. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section flammula;species gormanii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501153</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate, sometimes rooting nodally, glabrous.</text>
      <biological_entity id="o26817" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="sometimes; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots thickened basally, glabrous.</text>
      <biological_entity id="o26818" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Proximal cauline leaf-blades narrowly to broadly ovate, 1.2-4 × 0.7-2 cm, base rounded, truncate or sometimes obtuse, margins entire or denticulate, apex obtuse or acute.</text>
      <biological_entity constraint="proximal cauline" id="o26819" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26820" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o26821" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o26822" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts ovate or sometimes lanceolate.</text>
      <biological_entity id="o26823" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o26824" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o26825" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o26826" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 5, spreading or reflexed from near base, 2-4 × 1-3 mm, glabrous;</text>
      <biological_entity id="o26827" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26828" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character constraint="from base" constraintid="o26829" is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" notes="" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26829" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petals 5-6, 4-6 × 2-4 mm;</text>
      <biological_entity id="o26830" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26831" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectary scales glabrous.</text>
      <biological_entity id="o26832" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="nectary" id="o26833" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads of achenes hemispheric, 2-3 × 3-4 mm;</text>
      <biological_entity id="o26834" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26835" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <relation from="o26834" id="r3615" name="part_of" negation="false" src="d0_s8" to="o26835" />
    </statement>
    <statement id="d0_s9">
      <text>achenes 1.2-2 × 1.2-1.4 mm, glabrous;</text>
      <biological_entity id="o26836" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak lanceolate to subulate, straight or curved, 0.6-0.8 mm.</text>
      <biological_entity id="o26837" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="subulate" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp soil of meadows and stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="damp" constraint="of meadows and stream banks" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <discussion>Ranunculus gormanii is restricted to middle elevations in the Klamath and southern Cascade Mountains.</discussion>
  
</bio:treatment>