<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1833" rank="genus">dicentra</taxon_name>
    <taxon_name authority="S. Watson" date="1880" rank="species">pauciflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>2: 429. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus dicentra;species pauciflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500579</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, scapose, from rhizomes or clusters of spindle-shaped tubers, bulblets often present at proximal ends of tubers or along rhizomes.</text>
      <biological_entity id="o3317" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3318" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o3319" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="spindle--shaped" value_original="spindle--shaped" />
        <character is_modifier="true" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o3320" name="bulblet" name_original="bulblets" src="d0_s0" type="structure" />
      <biological_entity constraint="proximal" id="o3321" name="end" name_original="ends" src="d0_s0" type="structure" />
      <biological_entity constraint="proximal" id="o3322" name="tuber" name_original="tubers" src="d0_s0" type="structure" />
      <biological_entity constraint="proximal" id="o3323" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o3317" id="r432" name="from" negation="false" src="d0_s0" to="o3318" />
      <relation from="o3317" id="r433" name="from" negation="false" src="d0_s0" to="o3319" />
      <relation from="o3320" id="r434" modifier="often" name="present" negation="false" src="d0_s0" to="o3321" />
      <relation from="o3320" id="r435" modifier="often" name="present" negation="false" src="d0_s0" to="o3322" />
      <relation from="o3320" id="r436" modifier="often" name="present" negation="false" src="d0_s0" to="o3323" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves (7-) 9-13 (-16) × 3-7 (-10) cm;</text>
      <biological_entity id="o3324" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_length" src="d0_s1" to="9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s1" to="13" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (2-) 4-7 (-10) cm;</text>
      <biological_entity id="o3325" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 3-4 orders of leaflets and lobes;</text>
      <biological_entity id="o3326" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o3327" name="order" name_original="orders" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o3328" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o3329" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o3326" id="r437" name="with" negation="false" src="d0_s3" to="o3327" />
      <relation from="o3327" id="r438" name="part_of" negation="false" src="d0_s3" to="o3328" />
      <relation from="o3327" id="r439" name="part_of" negation="false" src="d0_s3" to="o3329" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes linear-lanceolate, (2-) 7-13 (-18) × 1.5-3 mm, occasionally irregular, minutely apiculate.</text>
      <biological_entity constraint="ultimate" id="o3330" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s4" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, 1-3-flowered, barely exceeding leaves;</text>
      <biological_entity id="o3331" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity id="o3332" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o3331" id="r440" modifier="barely" name="exceeding" negation="false" src="d0_s5" to="o3332" />
    </statement>
    <statement id="d0_s6">
      <text>bracts ovate, 4-5 × 2-3 mm.</text>
      <biological_entity id="o3333" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect to nodding;</text>
      <biological_entity id="o3334" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 5-25 mm;</text>
      <biological_entity id="o3335" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate to lanceolate, 5-8 × 2-4 mm;</text>
      <biological_entity id="o3336" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer petals white to pink, (15-) 18-22 (-25) × 3-6 mm, reflexed portion (5-) 7-8 (-11) mm;</text>
      <biological_entity constraint="outer" id="o3337" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s10" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3338" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner petals purple, (15-) 18-22 (-24) mm, blade spoon-shaped, 2-3 mm, claw obovate-elliptic, ca. 10 × 3-4 mm, crest absent;</text>
      <biological_entity constraint="inner" id="o3339" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="24" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3340" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spoon--shaped" value_original="spoon--shaped" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3341" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character name="length" src="d0_s11" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3342" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments of each bundle connate at base and near apex, distinct in between, distinct portion of median filament forming loop that almost doubles back to its proximal end;</text>
      <biological_entity id="o3343" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="bundle" constraint_original="bundle; bundle">
        <character constraint="at base" constraintid="o3345" is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o3344" name="bundle" name_original="bundle" src="d0_s12" type="structure" />
      <biological_entity id="o3345" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o3346" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o3347" name="portion" name_original="portion" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="median" id="o3348" name="filament" name_original="filament" src="d0_s12" type="structure" />
      <biological_entity id="o3349" name="loop" name_original="loop" src="d0_s12" type="structure" />
      <biological_entity id="o3350" name="back" name_original="back" src="d0_s12" type="structure" />
      <biological_entity constraint="proximal" id="o3351" name="end" name_original="end" src="d0_s12" type="structure" />
      <relation from="o3343" id="r441" name="part_of" negation="false" src="d0_s12" to="o3344" />
      <relation from="o3343" id="r442" name="near" negation="false" src="d0_s12" to="o3346" />
      <relation from="o3347" id="r443" name="part_of" negation="false" src="d0_s12" to="o3348" />
      <relation from="o3347" id="r444" modifier="almost" name="forming" negation="false" src="d0_s12" to="o3349" />
      <relation from="o3349" id="r445" name="doubles" negation="false" src="d0_s12" to="o3350" />
      <relation from="o3349" id="r446" name="to" negation="false" src="d0_s12" to="o3351" />
    </statement>
    <statement id="d0_s13">
      <text>nectariferous tissue borne at lowermost point of loop and often extending to base of median filament;</text>
      <biological_entity constraint="nectariferous" id="o3352" name="tissue" name_original="tissue" src="d0_s13" type="structure" />
      <biological_entity constraint="lowermost" id="o3353" name="point" name_original="point" src="d0_s13" type="structure" />
      <biological_entity constraint="lowermost" id="o3354" name="loop" name_original="loop" src="d0_s13" type="structure" />
      <biological_entity id="o3355" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity constraint="median" id="o3356" name="filament" name_original="filament" src="d0_s13" type="structure" />
      <relation from="o3352" id="r447" name="borne at" negation="false" src="d0_s13" to="o3353" />
      <relation from="o3352" id="r448" name="borne at" negation="false" src="d0_s13" to="o3354" />
      <relation from="o3352" id="r449" modifier="often" name="extending to" negation="false" src="d0_s13" to="o3355" />
      <relation from="o3352" id="r450" modifier="often" name="extending to" negation="false" src="d0_s13" to="o3356" />
    </statement>
    <statement id="d0_s14">
      <text>style 7-11 mm;</text>
      <biological_entity id="o3357" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma 2-lobed, much reduced, ca. 2 times wider than style.</text>
      <biological_entity id="o3358" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character constraint="style" constraintid="o3359" is_modifier="false" name="width" src="d0_s15" value="2 times wider than style" />
      </biological_entity>
      <biological_entity id="o3359" name="style" name_original="style" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules spindle-shaped to ovoid, 10-15 × 4-6 mm.</text>
      <biological_entity id="o3360" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="spindle-shaped" name="shape" src="d0_s16" to="ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s16" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds reniform, ca. 2 mm diam., smooth, elaiosome present.</text>
      <biological_entity id="o3361" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="reniform" value_original="reniform" />
        <character name="diameter" src="d0_s17" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 16.</text>
      <biological_entity id="o3362" name="elaiosome" name_original="elaiosome" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3363" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in coniferous forests, in volcanic and granitic soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in coniferous forests" />
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="granitic soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>